<?php namespace Hummingbird\Core;

class HummingbirdCMS
{
    /**
     * The HummingbirdCMS version.
     * @var string
     */
    const VERSION = '3.6.2';
}
