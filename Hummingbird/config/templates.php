<?php
/*
|--------------------------------------------------------------------------
| Templates
|--------------------------------------------------------------------------
| Configuration values for website templates.
*/
   
return [

    /*
    |--------------------------------------------------------------------------
    | Page side navigation settings
    |--------------------------------------------------------------------------
    | 
    */

    'side-navigation' => [
        'template' => 'HummingbirdBase::public.partials.side-navigation',
        'wrapper' => [
            'id' => 'sidenav',
            'classes' => '',
        ],
        'list' => [
            'classes' => '',
            'classes_active' => 'active'
        ],
        'list-items' => [
            'classes' => '',
            'classes_active' => 'active'
        ]
    ]
];
