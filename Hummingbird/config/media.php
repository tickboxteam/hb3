<?php

return [

    /*
    |--------------------------------------------------------------------------
    | WEBP Settings
    |--------------------------------------------------------------------------
    | Are we going to allow the CMS to support WEBP images. If so, we should
    | change the settings to allow us to do this. We can allow:
    | * images to be converted to WEBP
    | * the compression ratio
    | * whether we should replace the original image or not
    | * How do we deliver this
    */

    'webp_active' => env('media_webp_activate') ?: false,

    /*
    |--------------------------------------------------------------------------
    | WEBP Media compression
    |--------------------------------------------------------------------------
    |
    |
    */

    'webp_compression' => env('webp_compression') ?: 90,

    /*
    |--------------------------------------------------------------------------
    | WEBP Media: Replace original
    |--------------------------------------------------------------------------
    |
    |
    */

    'webp_replace_original' => env('webp_replace_original') ?: false,

    /*
    |--------------------------------------------------------------------------
    | WEBP Media: Delivery method
    |--------------------------------------------------------------------------
    |
    |
    */

    'webp_delivery_method' => env('webp_delivery_method') ?: 'picture'
];
