<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Page configuration settings
    |--------------------------------------------------------------------------
    | 
    | This option allows control over the default page template to be used when
    | new pages are created.
    | 
    | Leave empty for default "internal" - which all themes should have as a
    | minimum for them to work/load.
    |
    */
   'default_template' => env('DEFAULT_PAGE_TEMPLATE', NULL)
];
