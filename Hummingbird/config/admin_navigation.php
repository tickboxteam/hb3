<?php

return array(
    'items' => [
        'dashboard' => [
            'label' => 'Dashboard',
            'icon'  => 'fa-home',
            'url'   => 'hummingbird.dashboard',
            'order' => 0,
            'parent' => NULL
        ],
        'pages' => [
            'label' => 'Pages',
            'icon'  => 'fa-edit',
            'url'   => 'hummingbird.pages.index',
            'order' => 32,
            'parent' => 'content',
            'permissions' => [
                'page.read'
            ]
        ],
        'modules' => [
            'label' => 'Modules',
            'icon'  => 'fa-angle-right',
            'url'   => 'hummingbird.modules.index',
            'order' => 8,
            'parent' => 'content.children.modules-and-templates',
            'permissions' => [
                'modules.read'
            ]
        ],
        'module-templates' => [
            'label' => 'Module templates',
            'icon'  => 'fa-angle-right',
            'url'   => 'hummingbird.module-templates.index',
            'order' => 16,
            'parent' => 'content.children.modules-and-templates',
            'permissions' => [
                'modules.templates.read'
            ]
        ],
        'users' => [
            'label' => 'Users',
            'icon'  => 'fa-users',
            'url'   => 'hummingbird.users.index',
            'order' => 2,
            'parent' => 'users',
            'permissions' => [
                'user.read'
            ]
        ],
        'roles' => [
            'label' => 'Roles',
            'icon'  => 'fa-eye-slash',
            'url'   => 'hummingbird.roles.index',
            'order' => 1,
            'parent' => 'users',
            'permissions' => [
                'role.read'
            ]
        ],
        'plugins' => [
            'label' => 'Plugins',
            'icon'  => 'fa-plug',
            'url'   => 'hummingbird.plugins.index',
            'order' => 1,
            'parent' => 'settings',
            'permissions' => [],
            'roles'  => ['superadmin']
        ],
        'media' => [
            'label' => 'Media centre',
            'icon'  => 'fa-image',
            'url'   => 'hummingbird.media.index',
            'order' => 1,
            'parent' => 'content',
            'permissions' => [
                'media.read'
            ]
        ],
        'menus' => [
            'label' => 'Menus',
            'icon'  => 'fa-sitemap',
            'url'   => 'hummingbird.menus.index',
            'order' => 2,
            'parent' => 'content',
            'permissions' => [
                'menu.read'
            ]
        ],
        'website-templates' => [
            'label' => 'Website templates',
            'icon'  => 'fa-briefcase',
            'url'   => 'hummingbird.templates.index',
            'order' => 64,
            'parent' => 'content',
            'permissions' => [
                'template.read'
            ]
        ],
        'errors' => [
            'label' => 'Error log',
            'icon'  => 'fa-warning',
            'url'   => 'hummingbird.errors.index',
            'order' => 2,
            'parent' => 'settings',
            'permissions' => [
                'error.read'
            ]
        ],
        'activities' => [
            'label' => 'Activity log',
            'icon'  => 'fa-history',
            'url'   => 'hummingbird.activity-logs.index',
            'order' => 4,
            'parent' => 'settings',
            'permissions' => [
                'activitylog.read'
            ]
        ],
        'redirects' => [
            'label' => 'Redirects',
            'icon'  => 'fa-repeat',
            'url'   => 'hummingbird.redirections.index',
            'order' => 8,
            'parent' => 'settings',
            'permissions' => [
                'redirections.read'
            ]
        ],
        'themes' => [
            'label' => 'Themes',
            'icon'  => 'fa-newspaper-o',
            'url'   => 'hummingbird.themes.index',
            'order' => 16,
            'parent' => 'settings',
            'permissions' => [],
            'roles'  => ['superadmin']
        ],
        'tags' => [
            'label' => 'Tags',
            'icon'  => 'fa-tags',
            'url'   => 'hummingbird.tags.index',
            'order' => 2,
            'parent' => 'taxonomy',
            'permissions' => [
                'tags.read'
            ]
        ],
        'categories' => [
            'label' => 'Categories',
            'icon'  => 'fa-sitemap',
            'url'   => 'hummingbird.categories.index',
            'order' => 1,
            'parent' => 'taxonomy',
            'permissions' => [
                'categories.read'
            ]
        ],
        'system-settings' => [
            'label' => 'Settings',
            'icon'  => 'fa-cog',
            'url'   => 'hummingbird.settings.index',
            'order' => 1,
            'parent' => 'settings',
            'permissions' => [],
            'roles'  => ['superadmin']
        ]
    ],
    'groups' => [
        'content' => [
            'label' => 'CONTENT',
            'order' => 1
        ],
        'settings' => [
            'label' => 'SYSTEM',
            'order' => 2
        ],
        'taxonomy' => [
            'label' => 'TAXONOMY',
            'order' => 4
        ],
        'users' => [
            'label' => 'USER MANAGEMENT',
            'order' => 8
        ],
        'modules-and-templates' => [
            'label' => 'Modules &amp; Templates',
            'order' => 4,
            'parent' => 'content',
            'icon'  => 'fa-cubes'
        ]
    ]
);
