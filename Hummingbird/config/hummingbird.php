<?php

return array(
    /*
    |--------------------------------------------------------------------------
    | Site name
    |--------------------------------------------------------------------------
    |
    | The site-wide title
    |
    */
    'site_name' => env('APP_NAME', ''),

    /*
    |--------------------------------------------------------------------------
    | Maintenance
    |--------------------------------------------------------------------------
    |
    | Website is in maintenance mode
    |
    */
    'maintenance' => FALSE,
    
    /*
    |--------------------------------------------------------------------------
    | Install URI
    |--------------------------------------------------------------------------
    |
    | The HB URI for installation
    |
    */
    'installURI' => 'installing',

    /*
    |--------------------------------------------------------------------------
    | CMS Backend URI
    |--------------------------------------------------------------------------
    |
    | A specific, installation URI, that can be used to define where the 
    | backend system will sit. This is set at the installation stage and can be 
    | changed by editing this URL. A new route is given for this URI.
    |
    */
    'backendURI' => env('APP_BACKEND_URI', 'hummingbird'),


    /*
    |--------------------------------------------------------------------------
    | SSL Enabled Field
    |--------------------------------------------------------------------------
    |
    | A specific, installation URI, that can be used to define where the 
    | backend system will sit. This is set at the installation stage and can be 
    | changed by editing this URL. A new route is given for this URI.
    |
    */
    'ssl' => false,

    /*
    |--------------------------------------------------------------------------
    | Default plugins to install
    |--------------------------------------------------------------------------
    |
    | Specify plugin codes which will be automatically installed.
    |
    */
    'defaultPlugins' => [],

    /*
    |--------------------------------------------------------------------------
    | Sepcific plugins to disable
    |--------------------------------------------------------------------------
    |
    | Specify plugin codes which will always be disabled in the application.
    |
    */
    'disablePlugins' => [],

    /*
    |--------------------------------------------------------------------------
    | Prevents application updates
    |--------------------------------------------------------------------------
    |
    | Stop HB from updating any area of the core.
    |
    */

    'disableCoreUpdates' => false,

    /*
    |--------------------------------------------------------------------------
    | Prevents plugin updates
    |--------------------------------------------------------------------------
    |
    | Stop HB from updating any plugins.
    |
    */

    'disablePluginUpdates' => false,

    /*
    |--------------------------------------------------------------------------
    | Prevents Theme updates
    |--------------------------------------------------------------------------
    |
    | Stop HB from updating any themes.
    |
    */

    'disableThemeUpdates' => false,

    /*
    |--------------------------------------------------------------------------
    | Page Time To Live Mapping
    |--------------------------------------------------------------------------
    |
    | Caching (in minutes) a page before the new version is loaded. Any page 
    | updates (published) must have the cache cleared. 
    |
    */

    'pageTTL' => 5,

    /*
    |--------------------------------------------------------------------------
    | Plugin directory
    |--------------------------------------------------------------------------
    |
    | The directory in which HB: Plugins will sit, relative to the application 
    | root.
    |
    */

    'pluginsDir' => '/plugins',

    /*
    |--------------------------------------------------------------------------
    | Themes directory
    |--------------------------------------------------------------------------
    |
    | The directory in which HB: Themes will sit, relative to the application 
    | root.
    |
    */

    'themesDir' => '/themes/public',

    /*
    |--------------------------------------------------------------------------
    | Uploads directory
    |--------------------------------------------------------------------------
    |
    | The general upload directory where documents, images and media items will 
    | sit. This directory is relative to the application root. Can changed to 
    | something else in the installation process.
    |
    */

    'uploadsDir' => '/uploads',
    
    /*
    |--------------------------------------------------------------------------
    | Installation
    |--------------------------------------------------------------------------
    |
    | Detecting whether the installation is complete or is a new installation.
    | 
    | Options:
    |   False - Start the install process
    |   True - Installation complete   
    |
    */
    'install' => env('APP_READY', false),
    
    /*
    |--------------------------------------------------------------------------
    | Default pages - after installation
    |--------------------------------------------------------------------------
    |
    | The following pages will be created during the installation and can not 
    | be removed. They are the:
    |
    | Homepage, Error 404
    |
    */
    'installPages' => array(
        array(
            'Name' => 'Homepage',
            'URL' => '/',
            'locked' => true
        ),
        array(
            'Name' => '404',
            'URL' => '/404/',
            'locked' => true
        )
    ),
    
    /*
    |--------------------------------------------------------------------------
    | Password strength
    |--------------------------------------------------------------------------
    |
    |
    */
    'passwordstrength' => '0',
    
    /*
    |--------------------------------------------------------------------------
    | Passwords not to be used
    |--------------------------------------------------------------------------
    |
    |
    */
    'bad_passwords' => array('123', 'password', 'letmein', 'test',
        'guest', 'info', 'adm', 'mysql', 'user', 'oracle'),
    
    /*
    |--------------------------------------------------------------------------
    | Usernames not to be used
    |--------------------------------------------------------------------------
    |
    |
    */
    'bad_usernames' => array('admin', 'administrator', 'root', '123456',
        'abc123', 'qwerty', 'monkey')
);
