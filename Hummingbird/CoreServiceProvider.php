<?php namespace Hummingbird;

use Request;
use Validator;
use Hummingbird\Listeners\Auth\PasswordResetListener;
use Hummingbird\Libraries\ArrayHelper;
use Hummingbird\Libraries\BackendMenu;
use Hummingbird\Libraries\General;
use Hummingbird\Libraries\Installer;
use Hummingbird\Models\Setting;
use Hummingbird\Validators\HummingbirdValidators;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rules\Password;


class CoreServiceProvider extends ServiceProvider {
    protected $files = array('events.php', 'routes.php');

    /**
     * Bootstrap Hummingbird.
     *
     * @return void
     */
    public function boot()
    {
        // Registering the Plugin Services
        if( Installer::isInstalled() ) {
            $this->app->register('Hummingbird\PluginManagerServiceProvider');
        }

        // Main routes, events and filters for Hummingbird
        foreach($this->files as $file) {
            if(File::exists(__DIR__ . "/{$file}")) {
                 require __DIR__ . "/{$file}";
            }
        }

        // Register commands
        // $this->registerCommands();
        $this->registerValidators();

        Password::defaults(function () {
            $SystemSettings = Setting::where('key', 'system')->firstOrFail();
            $Settings = $SystemSettings->value;

            $PasswordLength = (isset($Settings['pass_length']) && is_numeric($Settings['pass_length']) && $Settings['pass_length'] >= 8) ? $Settings['pass_length'] : 8;
            $Letters  = ( isset($Settings['letters']) && filter_var( $Settings['letters'], FILTER_VALIDATE_BOOLEAN) );
            $MixedCase  = ( isset($Settings['mixed_case']) && filter_var( $Settings['mixed_case'], FILTER_VALIDATE_BOOLEAN) );
            $Numbers    = ( isset($Settings['pass_numbers']) && filter_var( $Settings['pass_numbers'], FILTER_VALIDATE_BOOLEAN) );
            $Symbols    = ( isset($Settings['pass_symbols']) && filter_var( $Settings['pass_symbols'], FILTER_VALIDATE_BOOLEAN) );
            $Uncompromised = ( isset($Settings['pass_compromised']) && filter_var( $Settings['pass_compromised'], FILTER_VALIDATE_BOOLEAN) );

            $PasswordRule = Password::min( $PasswordLength );

            if( $Letters ) {
                $PasswordRule->letters();
            }

            if( $MixedCase ) {
                $PasswordRule->mixedCase();
            }

            if( $Numbers ) {
                $PasswordRule->numbers();
            }

            if( $Symbols ) {
                $PasswordRule->symbols();
            }

            if( $Uncompromised ) {
                $PasswordRule->uncompromised();
            }

            return $this->app->isProduction()
                        ? $PasswordRule
                        : Password::min(8);
        });

        // Register Components
        // $this->appRegisterComponents();

        Event::listen(PasswordReset::class, PasswordResetListener::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $corePath = base_path() . '/Hummingbird';

        $this->loadViewsFrom("{$corePath}/Views", 'HummingbirdBase');

        // Include all configuration files
        foreach (glob( $corePath . '/config/*.php' ) as $CoreConfig ) {
            $ConfigInfo = pathinfo($CoreConfig);

            $this->mergeConfigFrom( $corePath . "/config/{$ConfigInfo['basename']}", "HummingbirdBase::{$ConfigInfo['filename']}");
        }

        /*
         * Define path constants
         */
        if (!defined('HUMMINGBIRD_PATH')) define('HUMMINGBIRD_PATH', $corePath);
        // if (!defined('HUMMINGBIRD_PLUGIN_PATH')) define('HUMMINGBIRD_PLUGIN_PATH', base_path() . '/plugins');

        // Include our helper files
        foreach (glob( $corePath . '/Helpers/*.php') as $file) {
            require_once($file);
        }

        $this->app->alias(General::class, 'general');
        $this->app->alias(ArrayHelper::class, 'array.helper');
        
        // TODO: Can/Should be removed, as I don't think we need it
        $this->app->singleton('backend_url', function($app) {
            $General = $app->make('general');
            return $General->backend_url();
        });

        // Register commands
        $this->registerCommands();
        // $this->registerValidators();

        // Register Components
        $this->appRegisterComponents();

        if( is_link( public_path("uploads") ) === false ) {
            symlink( storage_path( "app/public/uploads" ), public_path( "uploads" ) );
        }

        $adminThemeAssetsPath = public_path( 'themes/admin' );

        if( !file_exists($adminThemeAssetsPath) ) {
            app(Filesystem::class)->makeDirectory($adminThemeAssetsPath, 0755, true);
        }
        
        // Create target symlink public theme assets directory if required
        if( !file_exists( public_path() . "/themes/admin/hummingbird" ) ) {
            symlink( base_path( "themes/admin/hummingbird" ), public_path( "themes/admin/hummingbird" ) );
        }
    }

    public function appRegisterComponents() {
        if( Request::segment(1) == 'hummingbird' ) {
            // Set the forgotten password view
            config()->set('auth.password.email', 'HummingbirdBase::emails.backend.auth.admin-forgotten');
            
            $this->app->singleton('cms.navigation', function(){
                $BackendMenu = new BackendMenu;
                
                $BackendMenu->registerBackendGroups( config('hb3-admin_navigation.groups') );
                $BackendMenu->registerBackendNavigationItems( config('hb3-admin_navigation.items') );

                return $BackendMenu;
            });
        }
    }

    public function registerCommands() {
        $this->commands( [
            "Hummingbird\\Commands\\CleanSearchCommand",
            "Hummingbird\\Commands\\ConvertExistingMediaItemsToWEBP",
            "Hummingbird\\Commands\\HB3AutoloadCommand",
            "Hummingbird\\Commands\\HB3UpdateCommand",
            "Hummingbird\\Commands\\RemoveEmptyCollectionCommand",
            "Hummingbird\\Commands\\PageVersionsPurgeCommand",
            "Hummingbird\\Commands\\SEO\\GenerateSitemap",
            "Hummingbird\\Commands\\Taxonomy\\CleanTaxonomyCategoryPermalinks",
            "Hummingbird\\Commands\\Taxonomy\\CleanTaxonomyTagPermalinks",
            "Hummingbird\\Commands\\Taxonomy\\CleanTaxonomyRelationship",
            "Hummingbird\\Commands\\Encryption\\EncryptionKeyRotation",
            "Hummingbird\\Commands\\Users\\RegenerateUsernames"
        ] );
    }


    public function registerValidators() {
        Validator::extend('google_recaptcha', 'Hummingbird\Validators\GoogleReCaptcha@validate');

        Validator::resolver(function($translator, $data, $rules, $messages) {
            return new HummingbirdValidators($translator, $data, $rules, $messages);
        });
    }
}