<?php namespace Hummingbird\Interfaces;

interface Searchable
{
    /**
     * Return whether this element is searchable
     *
     * @return Boolean
     */
    public function isSearchable();


    /**
     * Return the id of the searchable subject.
     *
     * @return string
     */
    public function getSearchableId();


    /**
     * Return the title of the searchable subject
     *
     * @return string
     */
    public function getSearchableTitle();


    /**
     * Return the slug of the searchable subject
     *
     * @return string
     */
    public function getSearchableSlug();


    /**
     * Returns the article summary that must be indexed
     *
     * @return array
     */
    public function getSearchableSummary();


    /**
     * Returns the article body that must be indexed
     *
     * @return array
     */
    public function getSearchableBody();


    /**
     * Return the type of the searchable subject.
     *
     * @return string
     */
    public function getSearchableType();


    /**
     * Return the type of the searchable subject.
     *
     * @return string
     */
    public function getSearchableWeight();


    /**
     * Return the searchable_image if there is one set
     *
     * @return string
     */
    public function getSearchableImage();


    /**
     * Check if the resource has a searchable_image
     *
     * @return Boolean
     */
    public function hasSearchableImage();
}