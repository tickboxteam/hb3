<?php namespace Hummingbird\Interfaces;

/**
 * The interface that external imports will
 * hook into to make sure migrations can
 * run correctly
 */
interface HB3MigratorInterface {
    /**
     * Import all elements
     */
    public function process_import();

    /**
     * Process request to all the elements we want to import
     */
    public function process_elements();
}