<?php namespace Hummingbird\Interfaces;

/**
 *
 */
interface PluginServiceProviderInterface {
    
    /**
     * Set the plugin name
     *
     * @return void
     */
    public function setPluginName();


    /**
     * Set the package name
     *
     * @return void
     */
    public function setPackageName();


    /**
     * Set the directory for our views
     *
     * @return void
     */
    public function setViewDirectory();
}
