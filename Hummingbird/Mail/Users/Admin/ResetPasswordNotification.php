<?php namespace Hummingbird\Mail\Users\Admin;

use Hummingbird\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordNotification extends Mailable
{
    use Queueable, SerializesModels;
 
    /**
     * The user we're sending too
     *
     * @var String
     */
    public $user;

    /**
     * The token for password resets
     *
     * @var String
     */
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;

        config()->set('auth.passwords.users.email', 'HummingbirdBase::emails.backend.auth.admin-forgotten');
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view( config()->get('auth.passwords.users.email') )->subject( 'Reset your password' );
    }
}
