<?php namespace Hummingbird\Middleware\Admin;

use Closure;
use Illuminate\Support\Facades\Auth;

class Force2FASetup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if( 
            Auth::user() && 
            Auth::user()->isAdmin() && 
            $request->is('hummingbird', 'hummingbird/*') &&
            !in_array($request->route()->getName(), ['hummingbird.profile', 'password.confirm', 'password.confirmation', 'password.confirm.store', 'two-factor.enable', 'two-factor.confirm', 'two-factor.disable']) && 
            ( !Auth::user()->two_factor_secret && !Auth::user()->two_factor_confirmed_at && is_2fa_forced() )
        ) {
            return redirect()->route('hummingbird.profile');
        }

        return $next($request);
    }
}
