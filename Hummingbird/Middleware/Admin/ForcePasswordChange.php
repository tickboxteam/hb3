<?php namespace Hummingbird\Middleware\Admin;

use Auth;
use Closure;
use Redirect;
use Illuminate\Contracts\Auth\Guard;

class ForcePasswordChange {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if( Auth::user() && Auth::user()->force_password_reset && $request->url() != Route('hummingbird.profile') ) {
            return Redirect::route('hummingbird.profile');
        }

        return $next($request);
    }
}
