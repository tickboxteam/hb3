<?php namespace Hummingbird\Middleware\Admin;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // If authenticated, then return to the base route (depending on what you were accessing before)
        if( !Auth::guard($guard)->guest() ) {
            if( $request->is('hummingbird/*') ) {
                return redirect()->to('hummingbird');
            }

            return redirect()->to('/');
        }

        return $next($request);
    }
}
