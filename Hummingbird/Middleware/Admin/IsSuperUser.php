<?php namespace Hummingbird\Middleware\Admin;

use Auth;
use Closure;
use Redirect;
use Illuminate\Contracts\Auth\Guard;

class IsSuperUser {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if( !Auth::user()->isSuperUser() ) {
            return Redirect::route('hummingbird.forbidden');
        }

        return $next($request);
    }
}
