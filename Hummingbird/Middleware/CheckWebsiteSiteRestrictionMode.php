<?php namespace Hummingbird\Middleware;

use Closure;
use DB;
use Redirect;
use Hummingbird\Models\Setting;
use Illuminate\Contracts\Auth\Guard;

class CheckWebsiteSiteRestrictionMode {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if( config()->get('hb3-hummingbird.install') && DB::connection() ) {
            if( is_site_restricted() ):
                $RestrictedURL = (env('APP_RESTRICTED_REDIRECT_TO') != '' && filter_var(env('APP_RESTRICTED_REDIRECT_TO'), FILTER_VALIDATE_URL) ) ? env('APP_RESTRICTED_REDIRECT_TO') : NULL;

                try {
                    $SystemSettings = Setting::where('key', 'system')->firstOrFail();
                    $Settings = $SystemSettings->value;

                    if( isset($Settings['APP_RESTRICTED_REDIRECT_TO']) ):
                        $RestrictedURL = ( $Settings['APP_RESTRICTED_REDIRECT_TO'] != '' && filter_var($Settings['APP_RESTRICTED_REDIRECT_TO'], FILTER_VALIDATE_URL) ) ? $Settings['APP_RESTRICTED_REDIRECT_TO'] : $RestrictedURL;
                    endif;
                }
                catch(\Exception $e) {
                    // silence - no key or no entries
                }

                if( !is_null($RestrictedURL) ):
                    return Redirect::to( $RestrictedURL );
                endif;
                
                abort(403, 'Access denied');
            endif;
        }

        return $next($request);
    }
}
