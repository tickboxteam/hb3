<?php namespace Hummingbird\Middleware;

use Closure;
use DB;
use Illuminate\Contracts\Auth\Guard;

class CheckWebsiteAppURL {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if( config()->get('hb3-hummingbird.install') && DB::connection() ) {
            validate_app_url();
        }

        return $next($request);
    }
}
