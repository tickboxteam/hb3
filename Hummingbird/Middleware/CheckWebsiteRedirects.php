<?php namespace Hummingbird\Middleware;

use Closure;
use DB;
use Event;
use HB3Meta;
use Redirect;
use Hummingbird\Models\Redirections;
use Illuminate\Contracts\Auth\Guard;

class CheckWebsiteRedirects {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if( config()->get('HummingbirdBase::hummingbird.install') && DB::connection() && !$request->is('hummingbird*') ) {
            try {
                $redirect = Redirections::where('from', clean_and_sluggify_url( $request->getRequestUri() ))->firstOrFail();
                
                event( new \Hummingbird\Jobs\Redirects\RedirectTriggered($redirect) );
            
                if( !$redirect->isMeta() ) {
                    return redirect()->to( $redirect->to, $redirect->type, [
                        'Cache-Control' => 'public, max-age=1'
                    ]);
                }

                HB3Meta::addMetaRefresh($redirect->to);
            }
            catch(\Exception $e) {
                // nothing - silence
            }
        }

        return $next($request);
    }

}
