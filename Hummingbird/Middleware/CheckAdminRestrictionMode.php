<?php namespace Hummingbird\Middleware;

use Closure;
use DB;
use Redirect;
use Illuminate\Contracts\Auth\Guard;

class CheckAdminRestrictionMode {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if( config()->get('HummingbirdBase::hummingbird.install') && DB::connection() ) {
            try {
                if( $request->is('hummingbird', 'hummingbird/*') && is_backend_restricted() ):
                    return Redirect::to( '/' );
                endif;
            }
            catch(\Exception $e) {
                // nothing - silence
            }
        }

        return $next($request);
    }
}
