<?php namespace Hummingbird\Middleware;

use Closure;
use Log;
use Response;
use View;
use Hummingbird\Libraries\HtmlHelper;
use Hummingbird\Libraries\Themes;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * The "down" Artisan command gives you the ability to put an application 
 * into maintenance mode. Here, you will define what is displayed back
 * to the user if maintenance mode is in effect for the application.
 */
class HummingbirdCheckForMaintenanceMode extends CheckForMaintenanceMode {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( !site_is_in_debug_mode() && $this->app->isDownForMaintenance() ) {
            Log::info('Application currently in maintenance mode');

            /* Do we have a mainteance file for the theme */
            $theme = Themes::activeTheme();
            
            View::addLocation(base_path()."/themes/public/$theme/");
            View::addNamespace('theme', base_path() . '/themes/public/' . $theme . '/');
            if(View::exists('maintenance')) return Response::make(View::make('maintenance'), 503);

            /* Do we have a maintenance mode function enabled? */
            if(class_exists(HtmlHelper::class)) return Response::make(HtmlHelper::maintenanceMode(), 503);
            
            /* Standard return */
            throw new HttpException(503);
        }

        return $next($request);
    }
}
