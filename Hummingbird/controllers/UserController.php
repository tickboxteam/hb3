<?php namespace Hummingbird\Controllers;

use App;
use ArrayHelper;
use Auth;
use Event;
use General;
use Redirect;
use Session;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Libraries\RoleManager;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Permission;
use Hummingbird\Models\PermissionGroup;
use Hummingbird\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Validation\Rules\Password;
use Laravel\Fortify\Actions\DisableTwoFactorAuthentication;
use Laravel\Fortify\Actions\EnableTwoFactorAuthentication;
use Laravel\Fortify\Contracts\TwoFactorDisabledResponse;
use Laravel\Fortify\Contracts\TwoFactorEnabledResponse;

class UserController extends CmsbaseController
{
    public $preview_user = false;    
    public $code_location = 'users';
    protected $roles = array();

    public function __construct( Request $Request, RoleManager $RoleManager)
    {
        parent::__construct($Request);

        $this->RoleManager = $RoleManager;

        if( !Breadcrumbs::exists('hummingbird/users') ):
            Breadcrumbs::for('hummingbird/users', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Users', url('hummingbird/users'));
            });
        endif;

        $this->activateSearch();
    }

    public function index()
    {
        if($this->user->userAbility('user.read'))
        {
            $this->data['tag'] = 'Manage Users';
            $this->data['users'] = User::cmsSearch($this->Request->get('s'))->admin()->exceptMe()->status( $this->Request->get('status') );

            if(!empty($this->Request->filled('filter_by_role')) && $this->Request->get('filter_by_role') > 0) {
                $this->data['users']->where('role_id', $this->Request->get('filter_by_role'));
            }

            $orderByDirection = $this->Request->get('order_by_direction');

            if(!empty($this->Request->filled('order_by_fields'))) {
               $this->data['users']->orderBy($this->Request->get('order_by_fields'), $orderByDirection);
            } else {
                $this->data['users']->orderBy('name', 'ASC');
            }

            $this->data['users'] = $this->data['users']->paginate($this->pagination);
            $this->data['roles'] = $this->RoleManager->getRoles();

            return View::make('HummingbirdBase::cms.users.index', $this->data);
        }

        return $this->forbidden(true);
    }
    
    public function show($id)
    {
        if($this->user->userAbility('user.update'))
        {
            if($this->user->id == (int) $id) {
                return Redirect::route('hummingbird.profile');
            }

            try {
                $this->data['user'] = User::admin()->where('id', $id)->firstOrFail();

                if( !Auth::user()->isSuperUser() && $this->data['user']->isSuperUser() ) {
                    return Redirect::route( 'hummingbird.users.index' )->with('error', 'Unable to edit administrator. Insufficient priviledges.');
                }

                Breadcrumbs::for("hummingbird/users/{$this->data['user']->id}", function ($breadcrumbs)  {
                    $breadcrumbs->parent('hummingbird/users');
                    $breadcrumbs->push('Edit: ' . $this->data['user']->name, url("hummingbird/users/{$this->data['user']->id}"));
                });

                $this->data['tag'] = 'Edit '.$this->data['user']->username;
                $this->data['help'] = 'We are going to be editing this user. Simply fill in their details';
                $this->data['roles'] = $this->RoleManager->getRoles();

                $FilterByParentRole = NULL;

                if($this->data['user']->role) {
                    $FilterByParentRole = ($this->data['user']->role->isSuperUser() || ($this->data['user']->role->parentrole && $this->data['user']->role->parentrole->isSuperUser())) ? NULL : $this->data['user']->role->parentrole->id;
                }

                $this->data['statuses'] = User::get_statuses();
                $this->data['PermissionGroups']     = ($this->data['user']->role) ? PermissionGroup::filterPermissionsByRole($FilterByParentRole)->get() : [];
                $this->data['AllowedPermissions']   = NULL;

                if($this->data['user']->role) {
                    $this->data['AllowedPermissions']   = ($this->data['user']->role->isSuperUser() || ($this->data['user']->role->parentrole && $this->data['user']->role->parentrole->isSuperUser())) ? Permission::pluck('name')->all() : $this->data['user']->role->parentrole->perms->pluck('name')->all();
                }

                return View::make('HummingbirdBase::cms.users.edit', $this->data);
            }
            catch(\Exception $e) {
                // silence is golden
            }

            return Redirect::route( 'hummingbird.users.index' )->with('error', 'Unable to find user.');
        }

        return $this->forbidden(true);
    }

    public function update($id) {
        if($this->user->userAbility('user.update')) {
            $Validator = Validator::make( $this->Request->all(), [
                'firstname' => 'required',
                'surname' => 'required',
                'username' => "required|unique:users,id,{$id}",
                'email' => "required|email|unique:users,id,{$id}",
                'password' => ['nullable', Password::defaults(), 'confirmed'],
                'role_id' => 'required',
                'status' => 'required|in:inactive,active,banned'
            ]);

            if( $Validator->fails() ):
                return Redirect::route( 'hummingbird.users.show', $id )
                    ->withInput()
                    ->withErrors( $Validator )
                    ->with('error', 'There was a problem with your inputs. Please review.');
            endif;

            $name = $this->Request->get('firstname') . ' ' . $this->Request->get('surname');
            $this->Request->merge(['name' => $name]);

            $input = $this->Request->except('perms', 'password_confirmation');
            $user = User::find($id);

            if( !Auth::user()->isSuperUser() && $user->isSuperUser() ) {
                return Redirect::route( 'hummingbird.users.index' )->with('error', 'Unable to edit administrator. Insufficient priviledges.');
            }

            $old_role = $user->role ?: NULL;
            
            if( $this->Request->get('password') == '' ):
                unset( $input['password'] );
            endif;

            $user->fill($input);
            $user->role_id = $this->Request->get('role_id');
            $user->force_password_reset = ( $this->Request->filled('password') ) ? 1 : NULL;
            $user->save();

            $user->load('role');

            $permissions = ($this->Request->filled('selected_perms')) ? $this->Request->get('selected_perms'):[];
            
            // update perms depending on whether role has changed
            if($this->Request->filled('role_id') && (empty($old_role) || $this->Request->get('role_id') != $old_role->id)) {
                $user_permissions = ($user->role) ? $user->role->perms()->pluck('permission_id')->all() : [];

                // role has changed, get role perms and activated perms to merge them in
                $permissions = array_unique(array_merge($user_permissions, $permissions));
            }

            $user->permissions()->sync($permissions);

            Event::dispatch('UserWasUpdated', array($user));

            Activitylog::log([
                'action' => 'UPDATED',
                'type' => get_class($user),
                'link_id' => $user->id,
                'description' => 'Updated user',
                'notes' => Auth::user()->username . ' updated ' . $user->name . '\'s account'
            ]);

            return Redirect::route( 'hummingbird.users.show', $id )->with('success', 'User account updated');
        }

        return $this->forbidden(true);
    }


    /**
     * Updating the user status
     *
     * @param integer $id
     */  
    public function changeStatus( $id )
    {
        if( $this->user->userAbility('user.update') )
        {
            try {
                $user = User::findOrFail($id);
                $user->toggleStatus();

                Activitylog::log([
                    'action' => 'UPDATED',
                    'type' => get_class($user),
                    'link_id' => $user->id,
                    'description' => ( $user->status == User::APPROVED) ? 'User account has been activated.' : 'User account has been deactivated.',
                    'notes' => Auth::user()->username . ( $user->status == User::APPROVED) ? " has activated &quot;{$user->firstname} account." : " has deactivated &quot;{$user->firstname} account."
                ]);

                return Redirect::route( 'hummingbird.users.index' )->with('success', ( $user->status == User::APPROVED) ? 'User account has been activated.' : 'User account has been deactivated.');
            }
            catch(\Exception $e)
            {
                //silence is golden
            }

            return Redirect::route( 'hummingbird.users.index' )->with('error', 'Unable to update the status of user.');
        }
        
        return $this->forbidden();
    }
    
    /**
     *
     * 
     *
     *
     */ 
    public function store() {
        if( $this->user->userAbility('user.create') ) {
            $Validator = Validator::make( $this->Request->all(), [
                'firstname' => 'required',
                'surname' => 'required',
                'username' => 'required|unique:users',
                'email' => 'required|email|unique:users'
            ], [] );

            if( $Validator->fails() ):
                return Redirect::route( 'hummingbird.users.index' )
                    ->withInput()
                    ->withErrors( $Validator )
                    ->with('error', 'There was a problem creating that account')
                    ->with('UserDuplicationError', ( $Validator->errors()->first('email') == 'The email has already been taken.' ) ? User::where('email', $this->Request->get('email'))->withTrashed()->first()->id : NULL );
            endif;

            $name = $this->Request->get('firstname') . ' ' . $this->Request->get('surname');
            $this->Request->merge(['name' => $name]);
                        
            $input = $this->Request->except('_token', 'add', 'password_confirmation');
            
            $user = (new User)->fill($input);
            $user->is_admin = 1;
            $user->password = str_random(32);
            $user->status = User::APPROVED;
            $user->force_password_reset = 1;
            $user->remember_token = hash('sha256', time().rand(0,10000));
            $user->save();

            // Store permissions
            $user->permissions()->sync($user->role->perms()->pluck('permission_id')->all());

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($user),
                'link_id' => $user->id,
                'description' => 'Created user',
                'notes' => Auth::user()->username . ' created an account for ' . $user->name
            ]);

            Event::dispatch('Backend.User.Added', array(['User' => $user]));

            return Redirect::route( 'hummingbird.users.show', $user->id );
        }

        return $this->forbidden(true);
    }
    
    public function destroy($id)
    {
        if($this->user->userAbility('user.delete')) {
            try {        
                $user = User::exceptMe()->where('id', $id)->firstOrFail();
                
                Activitylog::log([
                    'action' => 'DELETED',
                    'type' => get_class($user),
                    'link_id' => $user->id,
                    'description' => 'Deleted user',
                    'notes' => Auth::user()->username . ' deleted ' . $user->name
                ]);

                if( $user->isSuperUser() && !Auth::user()->isSuperUser() ) {
                    throw new \Exception( "Super administrator can not be removed" );
                }

                $user->delete();

                return Redirect::route( 'hummingbird.users.index' )->with('success', 'Administrator has been removed.');
            }
            catch(ModelNotFoundException $e) {
                return Redirect::route( 'hummingbird.users.index' )->with('error', 'Administrator could not be found.');
            }
            catch(\Exception $e) {
                return Redirect::route( 'hummingbird.users.index' )->with('error', 'Unable to delete administrator.');
            }
        }

        return $this->forbidden(true);
    }

    public function profile()
    {
        if(null !== $this->user)
        {
            if($this->user->previewMode())
            {
                // in user preview mode
                return Redirect::route('hummingbird.users.preview.destroy', ['referrer' => route('hummingbird.profile')]);
            }
            else if($this->user->role->previewMode())
            {
                //in role preview mode
                return Redirect::route('hummingbird.roles.preview', [NULL, 'action' => 'remove']);
            }

            if( $this->Request->isMethod('POST') ) {
                $Validator = Validator::make( $this->Request->all(), [
                    'firstname' => 'required',
                    'surname' => 'required',
                    'username' => "required|unique:users,id,{$this->user->id}",
                    'email' => "required|email|unique:users,id,{$this->user->id}",
                    'password' => ['nullable', 'sometimes', Password::defaults(), 'confirmed']
                ]);

                if( $Validator->fails() ):
                    return Redirect::route("hummingbird.profile")
                        ->withErrors( $Validator )
                        ->withInput()
                        ->with('error', 'There was a problem updating your profile');
                endif;

                $name = $this->Request->get('firstname') . ' ' . $this->Request->get('surname');
                $this->Request->merge(['name' => $name]);
                $input = $this->Request->except('_token', 'add');

                if( $this->Request->get('password') == '' ):
                    unset( $input['password'] );
                    unset( $input['password_confirmation'] ); // ensure blank passwords aren't stored
                endif;

                $this->user->fill( $input );

                if( !empty($input['password']) AND !empty($input['password_confirmation']) ):
                    $this->user->force_password_reset = NULL;
                endif;

                $this->user->save();

                Activitylog::log([
                    'action' => 'UPDATED',
                    'type' => get_class($this->user),
                    'link_id' => $this->user->id,
                    'description' => 'Updated profile',
                    'notes' => Auth::user()->name . ' updated their profile '
                ]);

                return Redirect::route('hummingbird.profile')->with('success', 'Profile updated.');
            }

            $this->data['user'] = $this->user;

            return View::make('HummingbirdBase::cms.users.profile', $this->data);
        }
    }

    /**
     * Enable two factor authentication for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laravel\Fortify\Actions\EnableTwoFactorAuthentication  $enable
     * @return \Laravel\Fortify\Contracts\TwoFactorEnabledResponse
     */
    public function enable2fa(Request $request, EnableTwoFactorAuthentication $enable)
    {
        $enable($request->user(), $request->boolean('force', false));

        // TODO: Confirm password before this is enabled
        $request->user()->two_factor_confirmed_at = now();
        $request->user()->save();

        return redirect()->back()->with(['success' => '2FA has been setup', 'newlyEnabled' => true]);
    }

    /**
     * Disable two factor authentication for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laravel\Fortify\Actions\DisableTwoFactorAuthentication  $disable
     * @return \Laravel\Fortify\Contracts\TwoFactorDisabledResponse
     */
    public function disable2fa(Request $request, DisableTwoFactorAuthentication $disable)
    {
        $disable($request->user());

        // TODO: Confirm password before this is disabled
        $request->user()->two_factor_confirmed_at = NULL;
        $request->user()->save();

        return redirect()->back()->with('success', '2FA has been removed from your account');
    }

    /**
     * View the QR recovery codes for this user
     */
    public function show2FARecoveryCodes() {
        // Confirm password before access
        // Show recovery codes on password confirmation and 2FA setup.
    }



    
    public function getExport()
    {
        return parent::export_model('User');
    }
    
    public function postExport()
    {
        return parent::run_export('User', 'user_export.csv');        
    }

    /**
     * Allowing administrators to preview and access other administrators
     * 
     * @param  Integer $user_id
     * @return Redirect
     */
    public function preview($user_id = null) {
        if( $this->user->userAbility('user.*') ) {
            try {
                $this->preview_destroy();

                if($User = User::admin()->where('id', $user_id)->firstOrFail()) {
                    if( $User->isSuperUser() && !Auth::user()->isSuperUser() ) {
                        throw new \Exception("Unable to preview as you have insufficient priviledges.");
                    }

                    if( $User->isSuperUser() && Auth::user()->isSuperUser() ) {
                        throw new \Exception("You are a SuperUser. You can't preview the same role.");
                    }

                    Session::put('original_user_id', Auth::user()->id);
                    Session::put('view_user_perms', $user_id);
                    Auth::login($User, true);
                }
            }
            catch(ModelNotFoundException $e) {
                return route('hummingbird.users.index')->with('error', 'Unable to preview user. Please try again.');
            }
            catch(\Exception $e) {
                return route('hummingbird.users.index')->with('error', $e->getMessage());
            }

            return Redirect::to(General::backend_url());
        }

        return $this->forbidden();
    }

    /**
     * Remove previewing permissions
     * 
     * @return Redirect
     */
    public function preview_remove() {
        $this->preview_destroy();

        if( $this->Request->filled('referrer') ) {
            return Redirect::to( $this->Request->get('referrer') )->with('success', 'Preview removed');
        }

        return Redirect::route('hummingbird.dashboard')->with('success', 'Preview removed');
    }

    /**
     * Destroy the preview we've currently got open
     */
    protected function preview_destroy() {
        if( Session::has('view_user_perms') ) {
            Session::forget('view_user_perms');
            Auth::logout();
        }

        if( Session::has('original_user_id') ) {
            Auth::loginUsingId(Session::get('original_user_id'));
            Session::forget('original_user_id');
        }
    }


    /**
     * Reinstate users - of whom are deleted (and are only Administrators)
     * @param  Integer $user_id
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function reinstate($user_id) {
        if($this->user->userAbility('user.create')) {
            try {
                $User = User::where('id', $user_id)->onlyTrashed()->firstOrFail();
                $User->is_admin = 1;
                $User->role_id  = ( !empty($User->role_id) ) ? $User->role_id : 1;
                $User->reinstate();
                $User->save();

                return Redirect::route('hummingbird.users.index')->with('success', 'User has been reinstated.');
            }
            catch(\Exception $e) {
                return Redirect::route('hummingbird.users.index')->with('error', 'User may not exist, can not be reinstated (non-admin) or has not been previously deleted.');
            }

            return Redirect::route('hummingbird.users.index');
        }

        return $this->forbidden(true);
    }

    public function userPasswordForceReset($id) {
        if($this->user->isSuperUser() ) {
            try {
                $User = User::admin()->where('id', $id)->firstOrFail();
                $User->force_password_reset = 1;
                $User->save();

                Activitylog::log([
                    'action' => 'UPDATED',
                    'type' => get_class($this->user),
                    'link_id' => $this->user->id,
                    'description' => 'Force user password change on login',
                    'notes' => Auth::user()->name . ' requested for password update on ' . $User->name
                ]);

                return Redirect::back()->with('success', "{$User->firstname} will be required to change their password when they next login.");   
            }

            catch(\Exception $e) {
                return Redirect::route('hummingbird.users.index')->with('error', 'Unable to request password changes');
            }

            return Redirect::route('hummingbird.users.index')->with('error', 'Something went wrong, please try again.');
        }

       return $this->forbidden(true); 
    }

    public function usersPasswordForceReset()
    {
        if($this->user->isSuperUser() ) {
            try {
                $PasswordRequests = 0;

                if( $this->Request->filled('user-selected') && count($this->Request->get('user-selected')) > 0 ) {
                    $Users = User::admin()->find( $this->Request->get('user-selected') );

                    foreach( $Users as $User ) {

                        try {
                            $User->force_password_reset = 1;
                            $User->save();

                            $PasswordRequests++;

                            Activitylog::log([
                                'action' => 'UPDATED',
                                'type' => get_class($User),
                                'link_id' => null,
                                'description' => 'Force user password change on login',
                                'notes' => Auth::user()->username . " requested a password change on next login for &quot;$User->name&quot;"
                            ]);

                        }
                        catch(\Exception $e) {
                            Log::info('Unable to force password change password');
                            Log::error( $e->getMessage() );
                            $mb->add("{$User->id}", "Unable to force password change password: " . $e->getMessage());
                        }
                    }
                }

                return Redirect::back()->with('success', ( $PasswordRequests > 0 ) ? 'Successfully requested password changes for ' . $PasswordRequests . General::singular_or_plural($PasswordRequests, ' user') : NULL);               
            }

            catch(\Exception $e) {
                return Redirect::route('hummingbird.users.index')->with('error', 'Unable to request password changes');
            }

            return Redirect::route('hummingbird.users.index')->with('error', 'Something went wrong, please try again.');
        }

       return $this->forbidden(true); 
    }






    public function userConfirmPassword($request)
    {
        $this->data['user'] = $this->user;

        return View::make('HummingbirdBase::cms.users.auth.confirm-password', $this->data);
    }
}