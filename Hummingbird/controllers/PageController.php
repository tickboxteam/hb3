<?php namespace Hummingbird\Controllers;

use App;
use ArrayHelper;
use Auth;
use Config;
use DB;
use General;
use Log;
use Redirect;
use Response;
use SearchIndex;
use Session;
use SplTempFileObject;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use League\Csv\Writer;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Page;
use Hummingbird\Models\PageVersion;
use Hummingbird\Models\Setting;
use Hummingbird\Models\Template;
use Hummingbird\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class PageController extends CmsbaseController 
{
    public $code_location = 'pages';
    public $pagination = 15;
    public $reset_positions = FALSE;

    /**
     * Inject the models.
     * @param Post $postEdit
     * @param User $user
     */
    public function __construct(Request $Request, Page $page, User $user)
    {
        parent::__construct($Request);

        $this->page = $page;
        $this->user = Auth::user();

        if($this->reset_positions)
        {
            $this->processUpdates();
        }

        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Pages',
            'url' => '/hummingbird/pages/'
        ); //breadcrumb manager 

        if( !Breadcrumbs::exists('hummingbird/pages') ):
            Breadcrumbs::for('hummingbird/pages', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Pages', url('hummingbird/pages'));
            });
        endif;

        $this->activateSearch();
    }

    /*
    * Start with the homepage - run an update on all the internal pages, and assign them a position
    */
    public function processUpdates($parent_id = 1)
    {
        $pages = Page::where('parentpage_id', '=', $parent_id)->get();

        $position = 0;
        foreach($pages as $page)
        {
            $position++;
            $page->position = $position;
            $page->save();

            /* Update all page versions */
            $page_versions = PageVersion::where('page_id', '=', $page->id)->get();
            foreach($page_versions as $version)
            {
                $version->position = $position;
                $version->save();
            }

            $this->processUpdates($page->id);
        }
    }

    public function index($pagelist = '')
    {
        if($this->user->userAbility('page.read')) {
            $this->data['tag'] = 'Manage Pages';
            $this->data['page_list'] = (new Page)->cms_list_pages();
            
            $this->data['pages'] = Page::objectPermissions()
                ->cmsSearch( $this->Request->get('s') )
                ->notTrashed()
                ->parent( $this->Request->get('filter') )
                ->status( $this->Request->get('status') )
                ->paginate($this->pagination);

            return View::make('HummingbirdBase::cms.pages', $this->data);
        }
        
        return $this->forbidden();
    }
    
    public function getTrash()
    {
        if($this->user->userAbility('page.read')) {

            $this->data['tag'] = 'Manage Pages';
            $this->data['page_list'] = (new Page)->cms_list_pages();

            $this->data['pages'] = Page::objectPermissions()->onlyTrashed()
                                       ->cmsSearch($this->Request->get('s'))
                                       ->paginate($this->pagination);

             Breadcrumbs::for("hummingbird/pages/trash", function ($breadcrumbs)  {
                $breadcrumbs->parent('hummingbird/pages');
                $breadcrumbs->push('Trash', url('/hummingbird/pages/trash'));
            });

            return View::make('HummingbirdBase::cms.pages', $this->data);
        }
        
        return $this->forbidden();
    }

    public function emptyTrash()
    {
        if($this->user->userAbility('page.delete')) {
            // TODO: Tidy this function to be nicer.
            ini_set('set_time_limit', -1); // We need to set this whilst we delete whatever versions we have

            foreach(Page::onlyTrashed()->get() as $page) {
                $page->forceDelete();
            }

            return Redirect::route('hummingbird.pages.index')->with('success', 'Trash has been emptied');
        }
        
        return $this->forbidden();
    }

    /**
     * [getRestore description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function restorePage($id)
    {
        try {
            $page           = Page::objectPermissions()->withTrashed()->where('id', $id)->firstOrFail();
            $page->restore();

            return redirect()->route('hummingbird.pages.index', $page->id);
        }
        catch(\Exception $e) {
            Log::error("Unable to restore page {$id}. See error.");
            Log::error($e->getMessage());
        }
        
        return $this->forbidden();
    }

    public function show($id = false)
    {
        if($this->user->userAbility('page.update')) {
            $version = $this->Request->get('version') ?: NULL;
            $this->data['page_version'] = NULL;
            $this->data['page_list'] = (new Page)->cms_list_pages($id);

            if(!$id AND !$version) return Redirect::route('hummingbird.pages.index');

            if($version)
            {
                $this->data['page']         = PageVersion::where('id', '=', $version)->where('page_id', '=', $id)->first();

                if(!$this->data['page']) {
                    return $this->forbidden();
                }

                $this->data['page_id']      = $this->data['page']->page_id;
                $this->data['page_version'] = '/?version='.$this->data['page']->id;
            }

            if(!isset($this->data['page']))
            {
                $live           = $this->Request->get('live');
                $page           = Page::objectPermissions()->find($id);

                if(!$page) {
                    return $this->forbidden();
                }

                $LatestVersion = $page->latest_version();

                $this->data['out_of_date']  = (!$live && !empty($LatestVersion) && $LatestVersion->status != 'public') ? TRUE : FALSE;
                $this->data['page']         = (!$this->data['out_of_date']) ? $page : $LatestVersion;
                $this->data['page_id']      = (!$this->data['out_of_date']) ? $this->data['page']->id : $this->data['page']->page_id;
                
            }
            
            $this->data['versions'] = PageVersion::with(array('user'  => function ($q) {
                                            $q->withTrashed();
                                         }))
                                         ->where('page_id', '=', $id)->orderBy('id', 'DESC')
                                         ->get();

            $this->data['meta'] = DB::table('meta_content')->where('item_id', $this->data['page']->id)->where('item_type', get_class($this->data['page']))->pluck('value', 'key');
            $this->data['templates'] = (isset($this->data['microsite'])) ? $this->data['Microsites']->filter_get('Template', 'get_selection', true):Template::get_selection();
            $this->data['taxonomy'] =  (isset($this->data['microsite'])) ? $this->data['page']->taxonomy($this->data['Microsites']):$this->data['page']->taxonomy();
            $this->data['tag'] = 'Edit '.$this->data['page']->title;
            $this->getCommonData($id);

            $PageId = ( get_class($this->data['page']) == get_class( new PageVersion ) ) ? $this->data['page']->page_id : $this->data['page']->id;

            Breadcrumbs::for("hummingbird/pages/{$PageId}", function ($breadcrumbs) use ($PageId)  {
                $breadcrumbs->parent('hummingbird/pages');
                $breadcrumbs->push('Edit: ' . $this->data['page']->title, url("hummingbird/pages/{$PageId}"));
            });

            $this->data['themetemplates'] = (new Page)->get_templates();

            return View::make('HummingbirdBase::cms/pages-edit', $this->data);
        }
        
        return $this->forbidden();
    }
    
    public function update($id)
    {
        if($this->user->userAbility('page.update')) {
            $version = (!$this->Request->get('version')) ? false:$this->Request->get('version'); //get the version
            $old_page_url       = $this->Request->get('old_page_url');
            $old_parentpageid   = $this->Request->get('old_parentpage_id');

            $input                  = $this->Request->except('_token');
            $page                   = Page::find($id);
            $page                   = $page->fill($input);
            $page->content          = (strip_tags($input['generated_html']) != 'No contentGet started by selecting a template') ? $input['generated_html'] : '';
            $page->parentpage_id    = NULL;
            
            if($page->id > 1) {
                $page->parentpage_id    = (!is_numeric($input['parentpage_id'])) ? 1:$input['parentpage_id'];
            }

            $page->protected        = (null !== $this->Request->get('protected'));

            if($page->id > 1) {
                $page->permalink    = (!empty( $this->Request->get('url') )) ? str_slug( $this->Request->get('url') ) : str_slug($old_page_url);
                $last = ArrayHelper::cleanExplodedArray(explode("/", $page->permalink));
                $page->url          = end($last); //just in case there has been an update (not sure why we need that field anyway)
            }

            $page->template = ($this->Request->filled('template_name')) ? $this->Request->get('template_name'):NULL;
            $page->position = ( $page->position > 0 ) ? $page->position : 0; //FIXME: This doesn't feel right because of the issues with default values     
            $page->enable_comments = 0; //FIXME: This doesn't feel right because of the issues with default values

            if($input['action'] == 'publish' || $input['action'] == 'unpublish')
            {
                if($this->user->userAbility('page.publish')) {
                    $page->status =  $input['action'] == 'publish' ? 'public' : 'draft';
                    
                    if(!$page->protected)
                    {
                        $page->username = '';
                        $page->password = '';
                    }

                    /* Check if this is running */
                    if (!$page->save()) {
                        return Redirect::route('hummingbird.pages.show', $id)->withErrors($page->errors());
                    }

                    SearchIndex::insertOrUpdateSearch($page);
                }
            }

            $page_version = new PageVersion;
            $page_version = $page_version->fill($this->Request->all());
            $page_version->hash = $page->url.time();
            $page_version->page_id = $page->id;
            $page_version->parentpage_id = $page->parentpage_id;
            $page_version->permalink = $page->permalink;
            $page_version->user_id = Auth::user()->id;
            $page_version->template = $page->template;
            $page_version->content = $page->content;
            $page_version->title = $page->title;
            $page_version->url = $page->url;
            $page_version->status = $input['action'] == 'publish' ? 'public' : 'draft';

            if(!$page->protected)
            {
                $page_version->username = '';
                $page_version->password = '';
            }

            // Synchronise the (un)published version's) so we have the same date/time as the primary article
            if( in_array($input['action'], array('publish', 'unpublish')) ) {
                $page_version->created_at = $page_version->updated_at = $page->updated_at;
            }
            
            $page_version->position = ( $page->position > 0 ) ? $page->position : 0; //FIXME: This doesn't feel right because of the issues with default values
            $page_version->enable_comments = 0; //FIXME: This doesn't feel right because of the issues with default values
            $page_version->save();

            /* Store cats and tags */
            $input['taxonomy'] = (isset($input['taxonomy'])) ? $input['taxonomy']:array();

            if($input['action'] == 'publish') {
                $page->storeTaxonomy($input['taxonomy'], $page, get_class($page), (isset($this->data['Microsites']) AND null !== $this->data['Microsites']) ? $this->data['Microsites']:NULL);
                
                /* Store meta - for published articles */
                $this->setMetaData($id, $input);
            }

            /** Store the same details for versions */
            $page_version->storeTaxonomy($input['taxonomy'], $page_version, get_class($page_version), (isset($this->data['Microsites']) AND null !== $this->data['Microsites']) ? $this->data['Microsites']:NULL);

            if($page->url != $old_page_url) {
                $this->update_child_urls($page);
            }

            /* Store meta - page version (Always) */
            $this->setMetaData($id, $input, $page_version->id);


            /* Store activity */
            Activitylog::log([
                'action' => 'UPDATED',
                'type' => get_class($page),
                'link_id' => $page->id,
                'description' => 'Updated page',
                'notes' => Auth::user()->username . " has updated &quot;$page->title&quot;"
            ]);

            if($input['action'] == 'preview')
            {
                Session::put('preview', true);
            }     
            
            if($input['action'] == 'publish')
            {
                return Redirect::route('hummingbird.pages.show', $id)->with('success', 'Page has been published.');
            }

            if($input['action'] == 'unpublish')
            {
                return Redirect::route('hummingbird.pages.show', $id)->with('success', 'Page has been unpublished.');
            }

            return Redirect::route('hummingbird.pages.show', [$id, 'version' => $page_version->id])->with('success', 'Page has been saved.');
        }
        
        return $this->forbidden();
    }
    
    public function getCommonData($id = 0)
    {
        $this->data['pages'] = Page::get_selection('Select a parent page', array($id));
        $this->data['blog_feed_styles'] = Page::get_blog_feed_styles();
        $this->data['statuses'] = Page::get_statuses();
        $this->data['blocks'] = array(); // todo: get from Plugins
        $this->data['responsive'] = Setting::getWhere('key', '=', 'responsive');
    }

    public function store()
    {
        if($this->user->userAbility('page.create')) {
            $input = $this->Request->except('_token'); //remove filters

            $validator = Validator::make($input, Page::$rules);

            if($validator->fails())
            {
                return Redirect::route('hummingbird.pages.index')->withErrors($validator)->withInput();
            }

            $page = Page::create([
                'title' => $this->Request->get('title'),
                'url' => str_slug($this->Request->get('title')),
                'permalink' => str_slug($this->Request->get('title')),
                'parentpage_id' => (!$this->Request->get('parentpage_id')) ? 1 : $this->Request->get('parentpage_id'),
                'user_id' => Auth::user()->id,
                'locked' => 0
            ]);

            /* Create version */
            $page_version = (new PageVersion)->fill($input);
            $page_version['hash'] = $page->url.time();
            $page_version['page_id'] = $page->id;
            $page_version['url'] = $page->url;
            $page_version['permalink'] = $page->permalink;
            $page_version['parentpage_id'] = $page->parentpage_id;
            $page_version['user_id'] = Auth::user()->id;
            $page_version['locked'] = 0;
            $page_version['searchable'] = $page->searchable == 1 ? 1 : 0;
            $page_version['template'] = $page->template;
            $page_version->save();

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($page),
                'link_id' => $page->id,
                'description' => 'Created page',
                'notes' => Auth::user()->username . " created a new page - &quot;$page->title&quot;"
            ]);

            return Redirect::route('hummingbird.pages.show', [$page->id, 'version' => $page_version->id]);
        }
    }
    
    public function destroy($id)
    {
        if($this->user->userAbility('page.delete')) {
            $page = Page::find($id);

            if($page->locked && !Auth::user()->isSuperUser()) {
                return redirect()->back()->with('error', 'Page cannot be deleted as its locked.');
            }

            $page_title = (isset($page->title)) ? "&quot;$page->title&quot;":'';

            Activitylog::log([
                'action' => 'DELETED',
                'type' => get_class($page),
                'link_id' => null,
                'description' => 'Deleted page',
                'notes' => Auth::user()->username . " deleted the page " . $page_title
            ]);

            $page->status = 'trash';
            $page->save();
            $page->delete();

            /* Delete from the search page */
            SearchIndex::removeFromSearch($page);

            /* Redirect */
            return Redirect::route('hummingbird.pages.index')->with('success', 'Article has been moved to trash.');
        }
    }
    
    public function search()
    {
        $this->data['pages'] = parent::search_model('page');
        return View::make(General::backend_url().'/pages', $this->data);
    }


    /**
     *
     * 
     *
     *
     */  
    public function publish( $page_id, $value )
    {
        if($this->user->userAbility('page.publish')) {
            $page_id = str_replace("page:", "", $page_id); //remove this so we can get the page number

            if(is_numeric($page_id) AND is_numeric($value))
            {
                $this->page = Page::whereNull('deleted_at')->where('id', '=', $page_id)->first();

                if(null !== $this->page)
                {
                    $current_publish_val = $this->page->isLive(); //true = 1, false = 0

                    if($value != $current_publish_val)
                    {
                        $this->page->status = ($value == 1) ? 'public':'draft';
                        $this->page->save();

                        /* Update searcher */
                        SearchIndex::insertOrUpdateSearch($this->page);

                        /* Store activity */
                        $activity_text = ($value == 1) ? 'Page published':'Page unpublished';
                        $activity_desc = ($value == 1) ? " published &quot;".$this->page->title."&quot;":" unpublished &quot;".$this->page->title."&quot;";

                        Activitylog::log([
                            'action' => 'UPDATED',
                            'type' => get_class($this->page),
                            'link_id' => $this->page->id,
                            'description' => $activity_text,
                            'notes' => Auth::user()->username . " has $activity_desc"
                        ]);

                        Session::flash((($value == 1) ? 'success':'message'), "&quot;".$this->page->title."&quot; - $activity_text");
                        return Redirect::route('hummingbird.pages.index');
                    }
                    else
                    {
                        Session::flash('message', 'No changes could be made to ' . $this->page->title);
                        return Redirect::route('hummingbird.pages.index');
                    }
                }
                else
                {
                    Session::flash('error', 'Page could not be found.');
                    return Redirect::route('hummingbird.pages.index');
                }
            }
            else
            {
                Session::flash('error', 'There was a problem processing your request, please try again.');
                return Redirect::route('hummingbird.pages.index');
            }
        }
        
        return $this->forbidden();
    }

    /**
     *
     * 
     *
     *
     */  
    public function reorder($parent = null)
    {
        if($this->user->userAbility('page.update')) {

            Breadcrumbs::for("hummingbird/pages/re-order", function ($breadcrumbs)  {
                $breadcrumbs->parent('hummingbird/pages');
                $breadcrumbs->push('Re-order pages', url('/hummingbird/pages/re-order'));
            });

            if ($parent) {
                $sortable = Page::where('parentpage_id', $parent);

                Breadcrumbs::for("hummingbird/pages/re-order/{$parent}", function ($breadcrumbs) use($parent) {
                    $breadcrumbs->parent('hummingbird/pages/re-order');
                    $breadcrumbs->push($parent, url('/hummingbird/pages/re-order/' . $parent));
                });
            } else {
                $sortable = Page::where('parentpage_id', 1);
            }

            $this->data['tag'] = 'Re-ordering pages';
            $this->data['pageList'] = (new Page)->cms_list_pages();
            $this->data['sortable'] = $sortable->orderBy('position')->get();
            $this->data['current'] = $parent;

            return View::make('HummingbirdBase::cms.pages-re-order', $this->data);
        }
        
        return $this->forbidden();
    }

    public function updateOrder()
    {
        if($this->user->userAbility('page.update')) {
            if( $this->Request->filled('pages') ):
                foreach($this->Request->get('pages') as $p):
                    $page = Page::findOrFail($p['id']);
                    $page->position = $p['position'];
                    $page->save();
                endforeach;
            else:
                return Response::json(array('message' => 'No pages provided. Order not updated'));
            endif;

            return Response::json(array('message' => 'Page order updated'));
        }
        
        return Response::json(array('error' => 'Unauthorised'), 401);
    }

    /**
     * Saving the Meta Data for pages/page versions
     * @param Integer   $id
     * @param Array     $data
     * @param Mixed     $version_id
     */
    public function setMetaData($id, $data = array(), $version_id = null)
    {
        $PageID     = (!empty($version_id)) ? $version_id : $id;
        $PageClass  = (!empty($version_id)) ? get_class(new PageVersion) : get_class(new Page);

        DB::table('meta_content')->where('item_id', $PageID)->where('item_type', $PageClass)->delete();

        $insert_array = array();

        $data['meta_title'] = ( !empty($data['meta_title']) ) ? trim($data['meta_title']) : NULL;
        $data['meta_description'] = ( !empty($data['meta_description']) ) ? trim($data['meta_description']) : NULL;
        $data['meta_robots'] = ( !empty($data['meta_robots']) ) ? clean_meta_robot_tags($data['meta_robots']) : NULL;
        $data['meta_canonical'] = ( !empty($data['meta_canonical']) ) ? seo_clean_canonical_url($data['meta_canonical']) : NULL;
        $data['meta_facebook_title'] = ( !empty($data['meta_facebook_title']) ) ? trim($data['meta_facebook_title']) : NULL;
        $data['meta_facebook_description'] = ( !empty($data['meta_facebook_description']) ) ? trim($data['meta_facebook_description']) : NULL;
        $data['meta_facebook_image'] = ( !empty($data['meta_facebook_image']) ) ? $data['meta_facebook_image'] : NULL;
        $data['meta_twitter_title'] = ( !empty($data['meta_twitter_title']) ) ? trim($data['meta_twitter_title']) : NULL;
        $data['meta_twitter_description'] = ( !empty($data['meta_twitter_description']) ) ? trim($data['meta_twitter_description']) : NULL;
        $data['meta_twitter_image'] = ( !empty($data['meta_twitter_image']) ) ? trim($data['meta_twitter_image']) : NULL;
        $data['meta_twitter_card'] = ( !empty($data['meta_twitter_card']) ) ? trim($data['meta_twitter_card']) : NULL;

        if (!empty($data['meta_title']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_title', 'value' => $data['meta_title']);
        endif;

        if (!empty($data['meta_description']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_description', 'value' => $data['meta_description']);
        endif;

        if (!empty($data['meta_robots']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_robots', 'value' => $data['meta_robots']);
        endif;

        if (!empty($data['meta_canonical']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_canonical', 'value' => $data['meta_canonical']);
        endif;

        if (!empty($data['meta_facebook_title']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_facebook_title', 'value' => $data['meta_facebook_title']);
        endif;

        if (!empty($data['meta_facebook_description']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_facebook_description', 'value' => $data['meta_facebook_description']);
        endif;

        if (!empty($data['meta_facebook_image']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_facebook_image', 'value' => $data['meta_facebook_image']);
        endif;

        if (!empty($data['meta_twitter_title']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_twitter_title', 'value' => $data['meta_twitter_title']);
        endif;

        if (!empty($data['meta_twitter_description']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_twitter_description', 'value' => $data['meta_twitter_description']);
        endif;

        if (!empty($data['meta_twitter_image']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_twitter_image', 'value' => $data['meta_twitter_image']);
        endif;

        if (!empty($data['meta_twitter_card']) ):
            $insert_array[] = array('item_id' => $PageID, 'item_type' => $PageClass, 'key' => 'meta_twitter_card', 'value' => $data['meta_twitter_card']);
        endif;

        if( !empty($insert_array) ):
            DB::table('meta_content')->insert($insert_array);
        endif;
    }

    /**
     * We need to update the child URLs so that we can keep track of everything (especially children)
     * @param  Page $page
     * @return Void
     */
    public function update_child_urls($page) {
        foreach($page->active_children()->get() as $ChildPage) {
            $ChildPage->permalink = $ChildPage->url;
            $ChildPage->save();

            $this->update_child_urls($ChildPage);
        }
    }

    /**
     * Export all pages in the current structure
     */
    public function export_pages() {
        $this->export_pages_writer = Writer::createFromFileObject(new SplTempFileObject());
        $this->export_pages_writer->insertOne(array("Title", "URL", "Status"));

        $Pages = ( $this->Request->filled('filter') ) ? Page::where('id', $this->Request->get('filter')) : Page::whereNull('parentpage_id');

        $this->export_pages_loop( $Pages->orderby('position', 'ASC')->get() );

        $this->export_pages_writer->output('website_pages_export.csv');
    }
    
    /**
     * Loop over all pages and add children in after parent
     * @param  Collection $Pages
     */
    protected function export_pages_loop($Pages) {
        if( count($Pages) > 0 ) {
            foreach($Pages as $Page) {
                $this->export_pages_writer->insertOne(array(
                    $Page->title,
                    $Page->permalink,
                    $Page->status
                ));

                $this->export_pages_loop( $Page->children()->get() );
            }
        }
    }

    /**
     * Replicating the page
     * @param  Integer $page_id
     */
    public function replicate($page_id) {
        if( $this->user->userAbility('page.update') ) {
            try {
                $Page = Page::objectPermissions()->where('id', $page_id)->firstOrFail();

                $NewPage = new Page;
                $NewPage->fill( ( $Page->latest_version ) ? $Page->latest_version->toArray() : $Page->toArray() );
                $NewPage->status = 'draft';
                $NewPage->save();
                $NewPage->searchable = ( $Page->latest_version ) ? $Page->latest_version->searchable : $Page->searchable;
                $NewPage->template = ( $Page->latest_version ) ? $Page->latest_version->template : $Page->template;
                $NewPage->save();
                
                /* Create version */
                $NewPageVersion = (new PageVersion)->fill( $NewPage->toArray() );
                $NewPageVersion->hash = $NewPage->url.time();
                $NewPageVersion->save();

                Activitylog::log([
                    'action' => 'CREATED',
                    'type' => get_class($NewPage),
                    'link_id' => $NewPage->id,
                    'description' => 'Created page',
                    'notes' => Auth::user()->username . " created a new page - &quot;$NewPage->title&quot;"
                ]);

                // Clone taxonomy for Page / PageVersion
                $NewPage->storeTaxonomy( array_merge($Page->categories->lists('id'), $Page->tags->lists('id')), $NewPage, get_class($NewPage) );
                $NewPageVersion->storeTaxonomy( array_merge($Page->categories->lists('id'), $Page->tags->lists('id')), $NewPageVersion, get_class($NewPageVersion) );

                // Clone meta content for Page / PageVersion
                $Meta = DB::table('meta_content')->where('item_id', $Page->id)->where('item_type', get_class($Page))->lists('value', 'key');
                $this->setMetaData($NewPage->id, $Meta);
                $this->setMetaData($NewPage->id, $Meta, $NewPageVersion->id);

                return Redirect::action('Hummingbird\Controllers\PageController@getIndex')->with('success', 'Successfully cloned this page');
            }
            catch(\Exception $e) {
                Log::error($e->getMessage());
            }

            return Redirect::action('Hummingbird\Controllers\PageController@getIndex')->with('error', 'Unable to clone this page');
        }

        return $this->forbidden();
    }
}
