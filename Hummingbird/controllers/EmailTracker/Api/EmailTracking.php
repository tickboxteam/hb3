<?php namespace Hummingbird\Controllers\EmailTracker\Api;

use App;
use Redirect;
use App\Http\Controllers\Controller;
use Hummingbird\Jobs\EmailTracking\EmailTracker_Click;
use Hummingbird\Jobs\EmailTracking\EmailTracker_Open;
use Hummingbird\Providers\EmailTracker\Models\EmailTrack;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

/**
 * Controller API to help track and process email tracking tools
 */
class EmailTracking extends Controller {
    public function __construct(Request $Request) {
        $this->Request = $Request;
    }

    /**
     * Tracking the open email command for an email that has been sent
     * @param  Integer $tracker_id
     */
    public function track_open($tracker_id = NULL) {
        try {
            if( empty($tracker_id) ) {
                throw new \Exception('Unknown tracker ID');
            }

            if( config('emailTracker.track_email_opens') !== TRUE ) {
                throw new \Exception("The tracking of opening emails is disabled");
            }

            $Tracker = EmailTrack::hash($tracker_id)->firstOrFail();

            if( config('emailTracker.allow_admin_email_tracking') === FALSE && !empty($Tracker->is_admin) ) {
                throw new \Exception("Unable to track opens of administrator emails");
            }

            if( config('emailTracker.allow_email_tracking') === FALSE && empty($Tracker->is_admin) ) {
                throw new \Exception("Unable to track opens of website emails");
            }

            $TrackOpenJob = new EmailTracker_Open($Tracker);
            $TrackOpenJob->handle();
        }
        catch(\Exception $e) { }
        
        return Redirect::to('/');
    }

    /**
     * Tracking email link clicks
     * @param  Integer $tracker_id
     */
    public function track_click() {
        try {
            if( empty($this->Request->get('h') ) ) {
                throw new \Exception('Unknown tracker ID');
            }

            if( empty($this->Request->get('l') ) ) {
                throw new \Exception('Empty URL');
            }

            if( config('emailTracker.track_email_clicks') !== TRUE ) {
                throw new \Exception("The tracking of clicks in emails is disabled");
            }

            $Tracker = EmailTrack::hash($this->Request->get('h'))->firstOrFail();

            if( config('emailTracker.allow_admin_email_tracking') === FALSE && !empty($Tracker->is_admin) ) {
                throw new \Exception("Unable to track clicks in administrator emails");
            }

            if( config('emailTracker.allow_email_tracking') === FALSE && empty($Tracker->is_admin) ) {
                throw new \Exception("Unable to track clicks in website emails");
            }

            $TrackingLink = trim( $this->Request->get('l') );

            $TrackOpenJob = new EmailTracker_Click($Tracker, $TrackingLink);
            $TrackOpenJob->handle();

            return Redirect::to( $TrackingLink );
        }
        catch(\Exception $e) {}
        
        return Redirect::to('/');
    }
}