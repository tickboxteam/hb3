<?php namespace Hummingbird\Controllers;

use Auth;
use General;
use Redirect;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Carbon\Carbon;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\RequestNotFound;

class ErrorController extends CmsbaseController {
	/**
	 * Displaying all errors
	 *
	 * @return Response
	 */
	public function index()
	{
        if($this->user->userAbility('error.read')) {

            if( !Breadcrumbs::exists('hummingbird/errors') ):
                Breadcrumbs::for('hummingbird/errors', function ($breadcrumbs) { 
                    $breadcrumbs->parent('HummingbirdBreadcrumbs');
                    $breadcrumbs->push('Error log', url('hummingbird/errors'));
                });
            endif;

            $FromDate   = ($this->Request->filled('from')) ? Carbon::createFromFormat( 'd-m-Y', $this->Request->get('from') ) : NULL;
            $ToDate     = ($this->Request->filled('to')) ? Carbon::createFromFormat( 'd-m-Y', $this->Request->get('to') ) : NULL;

    		$this->data['tag'] = 'Manage Errors';
    		$this->data['errors'] = RequestNotFound::search()
                ->date_search($FromDate, $ToDate)
                ->type()
                ->orderBy('accessed', $this->Request->get('orderBy') ?: 'DESC')
                ->orderBy('updated_at', 'DESC')
                ->paginate(15);

            if( $this->data['errors']->count() == 0 && $this->Request->get('page') > 1 ) {
                return Redirect::route('hummingbird.errors.index', array('page' => $this->Request->get('page') - 1));
            }

    		return View::make('HummingbirdBase::cms/errors', $this->data);
        }

        return $this->forbidden(true);
    }

    /**
     * Remove this error
     *
     * @param  int  $id
     * @return Response
     */
    public function remove()
    {
        // Will destroy individual and multiple erors
        if($this->user->userAbility('error.delete')) {
            try {
                if( $this->Request->filled('delete-individual') ) {
                    $RequestNotFound = RequestNotFound::findOrFail( $this->Request->get('delete-individual') );
                    if( $RequestNotFound->delete() ) {
                        return Redirect::back()
                            ->with('message', "Successfully deleted &quot;{$RequestNotFound->url}&quot; as an error.");
                    }
                }

                if( $this->Request->filled('delete-selected') && count($this->Request->get('delete-selected')) > 0 ) {  
                    $Requests = RequestNotFound::find( $this->Request->get('delete-selected') );
                    if( RequestNotFound::destroy( $this->Request->get('delete-selected') ) ) {
                        return Redirect::back()
                            ->with('message', 'Successfully deleted ' . $Requests->count() . General::singular_or_plural($Requests->count(), ' error'));
                    }
                }
            }
            catch(\Exception $e) {
                // silence is golder
            }

            return Redirect::back()->with('message', 'Error(s) could not be removed.');
        }

        return $this->forbidden(true);
    }

    /**
     * Remove all resources
     */
    public function truncate() {
        if($this->user->userAbility('error.delete')) {
            $RequestNotFoundCount = RequestNotFound::count();

            if(RequestNotFound::count() <= 0) {
                return Redirect::route('hummingbird.errors.index')->with('error', 'There are no errors to remove.');
            }
        
            RequestNotFound::truncate();
            
            //record activity
            Activitylog::log(array(
                'action' => 'DELETED',
                'type' => get_class(new RequestNotFound),
                'link_id' => null,
                'description' => 'Cleared the error log',
                'notes' => Auth::user()->name . " cleared {$RequestNotFoundCount}" . General::singular_or_plural($RequestNotFoundCount, ' error') . " from the error log."
            ));

            return Redirect::route('hummingbird.errors.index')->with('message', 'Successfully wiped the error log.');
        }

        return $this->forbidden(true);
    }
}