<?php namespace Hummingbird\Controllers;

use App;
use ArrayHelper;
use Auth;
use Config;
use Event;
use General;
use Log;
use Redirect;
use Response;
use Session;
use Validator;
use View;
use App\Http\Controllers\Controller;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Contracts\SEO as SEOContract;
use Hummingbird\Libraries\FileHelper;
use Hummingbird\Libraries\HtmlHelper;
use Hummingbird\Libraries\ModuleParser;
use Hummingbird\Libraries\PluginManager;
use Hummingbird\Libraries\Themes;
use Hummingbird\Models\Moduletemplate;
use Hummingbird\Models\Module;
use Hummingbird\Models\Page;
use Hummingbird\Models\PageVersion;
use Hummingbird\Models\RequestNotFound;
use Hummingbird\Models\Setting;
use Hummingbird\Traits\SEO as SEOTrait;
use Illuminate\Http\Request;

class FrontendController extends Controller implements SEOContract {
    use SEOTrait;

    public $data = array();
    public $settings = array();
    public $theme;
    public $page = array();
    public $url = array();
    public $handles_children = false;
    
    /**
     * The array of parameters passed by the Router
     *
     * @var array
     */
    protected $page_parameters = [];


    public function __construct( Request $request )
    {
        $this->request = $request;

        $this->pageParametersBeforeLoad();

        // For use with the Microsite builder and manager service
        if(class_exists('\Tickbox\Microsites\Microsites'))
        {
            $this->Microsites = new \Tickbox\Microsites\Microsites;

            $this->microsite = $this->Microsites->load();
            $this->data['microsite'] = $this->microsite;

            if(null === $this->microsite) $this->Microsites->purgeDBConnection();
        }

        $this->data['path'] = parse_url( $this->request->url() . '/');

        if(!isset($this->data['path']['path'])) $this->data['path']['path'] = '/';

        $this->data['clean_path'] = ArrayHelper::cleanExplodedArray(explode('/', $this->parseURL($this->data['path'])));

        $this->settings = $this->getSiteSettings();
        $this->theme = Themes::activeTheme();
            
        View::addLocation( public_path("/themes/public/$this->theme") );
        View::addNamespace('theme', public_path("/themes/public/$this->theme") );

        $this->data['page'] = $this->loadPage($this->data['path']['path']);
        
        if( $this->data['page'] ) {
            $this->registerSEO( $this->data['page'] );
        }

        $this->data['view_location'] = $this->loadView();
        $this->template = 'internal';

        // Home
        if( !Breadcrumbs::exists('website-breadcrumb') ):
            Breadcrumbs::for('website-breadcrumb', function ($breadcrumbs) {
                $breadcrumbs->push('Home', url('/'));
            });
        endif;

        if( View::exists('theme::partials.breadcrumbs') ):
            config()->set('breadcrumbs.view', 'theme::partials.breadcrumbs');
        endif;

        $this->registerBreadcrumb($this->data['page']);

        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            
            return $next($request);
        });
    }

    /**
     * Load any parameters that the site generates.
     */
    protected function pageParametersBeforeLoad() {
        if( $this->request->route() && count($this->request->route()->parameters() ) > 0 ):
            $this->page_parameters = $this->request->route()->parameters();
        endif;
    }


    /**
     * Get a specific route parameter
     * @param  String $RouteParameterName
     * @return String
     *
     * @throws Exception
     */
    public function getRouteParameter($RouteParameterName) {
        if( isset($this->page_parameters[ $RouteParameterName ]) ) {
            return $this->page_parameters[ $RouteParameterName ];
        }

        throw new \Exception('Page parameter not found');
    }




    
    public function loadPage($slug, $page = NULL)
    {
        $page = Page::where('permalink', $slug);

        if( !$this->request->filled('version') ) {
            $page->where('status', 'public');
        }

        $page = $page->get()->first();

        if(!empty($page) && $this->request->filled('version') && ( is_numeric($this->request->get('version')) || in_array($this->request->get('version'), ['latest']) ) )
        {
            $PageVersion = ( is_numeric($this->request->get('version')) ) ? PageVersion::where('page_id', $page->id)->where('id', $this->request->get('version'))->first() : PageVersion::where('page_id', $page->id)->orderBy('id', 'DESC')->first();

            if( $PageVersion ) {
                $page->content = $PageVersion->content;
                $page->template = $PageVersion->template;
            }
        }

        if(null === $page)
        {
            // Replace with $this->request->segments() //
            $url = ArrayHelper::cleanExplodedArray(explode('/', $slug));
            array_pop($url);

            if(count($url) > 0 && $this->handles_children)
            {
                return $this->loadPage('/' . implode("/", $url) . '/');
            }

            return NULL;
        }

        return $page;
    }

    /**
     *  loadView() checks the theme directory for specific content files that help drive content
     * 
     * @return String View location 
     *
     */ 
    public function loadView()
    {
        if(!empty($this->data['page']) && null !== $this->data['page']->template && View::exists($this->data['page']->template)) return $this->data['page']->template;

        // This will return if the section has an index > about-us > hello-world.index
        if(View::exists(implode(".", $this->data['clean_path']) . '.index')) return implode(".", $this->data['clean_path']) . '.index';

        if( !empty($this->data['clean_path']) ): 
            /* 
             * Loop over a potential directory based layout and see if we have a file 
             * about-us > 123 > 456 > 789.blade (789 belongs to 456)
             * about-us > 789.blade (check root for standardised page templates)
             */
            $pop_array = $this->data['clean_path'];
            array_pop($pop_array);

            // Check the last item in the array to see if it exists
            $view = implode(".", $pop_array) . '.' . $this->data['clean_path'][count($this->data['clean_path'])-1];
                if(View::exists($view)) return $view;
            
            // Check the root directory to see if we have a general page available
            $view = $this->data['clean_path'][0] . '.' . $this->data['clean_path'][count($this->data['clean_path'])-1];
                if(View::exists($view)) return $view;

            // Pages completely added to the root, lazy
            if(View::exists($this->data['clean_path'][0])) return $this->data['clean_path'][0];

            /* 
             * Check the top level root and see if an internal page exists
             * about-us > index.blade
             */
            if(View::exists($this->data['clean_path'][0] . '.index')) return $this->data['clean_path'][0] . '.index';
        endif;
        
        // Nothing return internal - which each theme must have
        return 'internal';
    }

    public function getSiteSettings()
    {
        return Setting::all();
    }

    public function side_navGenerator($page, $args = [])
    {
        $args = array_merge(['levels' => 100, 'path' => $this->data['clean_path']], $args);
        return $page->list_all_pages_new($page, $args, true);
    }


    /**
     * Deprecated function
     * As of HB3 v1.7.7, this is not needed as we have a shortcode handler that can handle content
     */
    public function shortcodes($html, $page)
    {
        // [HB::PAGE-NAVIGATION]    - GLOBAL NAV NO LIMITS
        // [HB::PAGE-NAVIGATION(5)] - GLOBAL NAV - 5 Levels Deep
        $find_sub_nav = '#\[HB::PAGE-NAVIGATION(\(([0-9]*?)\))?\]#';
        preg_match_all($find_sub_nav, $html, $subnavs);

        foreach($subnavs[0] as $key => $selected_nav) {
            $args = (isset($subnavs[2][$key]) && is_numeric($subnavs[2][$key])) ? ['levels' => $subnavs[2][$key]] : [];
            $html = str_replace($subnavs[0][$key], $this->side_navGenerator($page, $args), $html);   
        }

        if(strpos($html, '[HB::MODULE') !== FALSE)
        {
            $modules = array();

            $find_modules_pattern = '#\[HB::MODULE\((.*?)\)(.*?)\]#';
            preg_match_all($find_modules_pattern, $html, $modules);

            if(isset($this->microsite) AND null !== $this->microsite AND $this->microsite->type == 'standard') $this->Microsites->purgeDBConnection();
            foreach($modules[1] as $key => $selected_module)
            {
                $args = [];
                $attributes = '';

                if( isset($modules[2]) && isset($modules[2][ $key ]) ):
                    $attributes = trim($modules[2][ $key ]);

                    if( $attributes != '' ):
                        // COPIED FROM Webwizo\Shortcodes\Compilers\ShortcodeCompiler::parseAttributes()

                        $pattern = '/(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';

                        if(preg_match_all($pattern, preg_replace("/[\x{00a0}\x{200b}]+/u", " ", $attributes), $matches, PREG_SET_ORDER)):

                            foreach ($matches as $m)
                            {
                                if (!empty($m[1]))
                                {
                                    $args[strtolower($m[1])] = stripcslashes($m[2]);
                                }
                                elseif (!empty($m[3]))
                                {
                                    $args[strtolower($m[3])] = stripcslashes($m[4]);
                                }
                                elseif (!empty($m[5]))
                                {
                                    $args[strtolower($m[5])] = stripcslashes($m[6]);
                                }
                                elseif (isset($m[7]) and strlen($m[7]))
                                {
                                    $args[] = stripcslashes($m[7]);
                                }
                                elseif (isset($m[8]))
                                {
                                    $args[] = stripcslashes($m[8]);
                                }
                            }
                        else:
                            $args = ltrim($text);
                        endif;
                    endif;
                endif;

                $module = Module::find($selected_module);

                if(null !== $module AND $module->template())
                {
                    $template = Moduletemplate::find($module->moduletemplate_id); //get template
                    $ModuleBuilder = new ModuleParser($template, $module, $page, $args); //build the ModuleParser
                    $html = str_replace($modules[0][$key], $ModuleBuilder->render(), $html);
                }
                else
                {
                    /* Standard HTML Modules - no template */
                    $mod_html = (null !== $module) ? $module->module_data['structure']:'';
                    $mod_html .= (null !== $module) ? "<style>{$module->module_data['css']}</style>":'';
                    $mod_html .= (null !== $module) ? "<script>{$module->module_data['js']}</script>":'';
                    $html = str_replace($modules[0][$key], $mod_html, $html, $args);
                }
            }

            $find_modules_pattern = '#\[HB::MODULETEMPLATE\((.*?)\)\]#';
            preg_match_all($find_modules_pattern, $html, $modules);
            
            foreach($modules[1] as $key => $selected_module)
            {   
                $selected_module    = (int) $selected_module;
                $modules            = Module::where('moduletemplate_id', $selected_module)->orderBy('name', 'ASC')->get();
                $html_to_store      = '';

                foreach($modules as $module)
                {
                    if(null !== $module AND $module->template())
                    {
                        $ModuleBuilder = new ModuleParser($module->moduletemplate, $module); //build the ModuleParser
                        $html_to_store .= $ModuleBuilder->render();
                    }
                }

                $html = str_replace("[HB::MODULETEMPLATE($selected_module)]", $html_to_store, $html); 
            }

            if(isset($this->microsite) AND null !== $this->microsite AND $this->microsite->type == 'standard') $this->Microsites->purgeDBConnection($this->Microsites->new_prefix);
        }
        
        $PluginManager = new PluginManager();
        $active = $PluginManager->get_active_plugins();
        if(key_exists('Tickbox\Gallery', $active)) {
            //only look for this if the plugin is installed and active
            if(strpos($html, '[HB::GALLERY') !== FALSE)
            {
                $find_modules_pattern = '#\[HB::GALLERY\((.*?)\)\]#';
                preg_match_all($find_modules_pattern, $html, $galleries);
                $html_to_store = '';
                foreach($galleries[1] as $key => $selected_gallery)
                {
                    $selected_gallery = (int) $selected_gallery;
                    
                    $gallery = \Tickbox\Gallery\Models\Gallery::find($selected_gallery);
                    if(null !== $gallery)
                    {
                        $GalleryHelper = new \Tickbox\Gallery\Classes\GalleryHelper;
                        $html_to_store = $GalleryHelper->renderLightbox($gallery);
                    }
                    
                    $html = str_replace("[HB::GALLERY($selected_gallery)]", $html_to_store, $html);
                }
            }   
        }


        return $html;
    }

    // Show a page by slug
    public function show($slug = '/', $contents = NULL)
    {
        if((boolean) config()->get('hb3-hummingbird.maintenance') === TRUE)
        {
            if(View::exists('maintenance')) return View::make('maintenance');

            return Response::make(HtmlHelper::maintenanceMode(), 503);
        } 

        /* This can be done better */
        $slug = ($slug[0] == '/') ? $slug:'/'.$slug; //add a trailing slash
        $slug = ($slug[strlen($slug)-1] == '/') ? $slug:$slug.'/'; //add a ending slash
        // $this->url = explode("/", $slug); // add this to global 

        if(null !== $contents)
        {
            $this->data['html'] = $contents;

            if(View::exists($this->template)) {
                return View::make($this->template, $this->data)->withShortcodes();
            }

            return View::make('internal', $this->data)->withShortcodes();
        }

        $page = $this->data['page'];

        if($page) {
            if($page->status == 'public' OR $this->request->filled('version'))
            {
                $this->data['html'] = (isset($this->html)) ? $this->html:$this->shortcodes($page->content, $page);

                if( $page->protected == 1 ) {
                    if( Session::has('login-' . $this->data['page']->id) ):
                        // Add an extra hour - every time we access it
                        Session::put('login-'.$this->data['page']->id, time(), 60);
                    endif;

                    if ( !Session::has('login-' . $this->data['page']->id) ) {
                        if( $this->request->isMethod('post') && $this->request->filled('login') ):
                            $Rules      = ['password' => 'required'];
                            $Messages   = ['password.required' => 'Please enter the password.'];

                            if( !empty($this->data['page']->username) ):
                                $Rules['username'] = 'required|in:' . $this->data['page']->username;
                                $Messages['username.required'] = 'Please enter the username';
                                $Messages['username.in'] = 'Username is incorrect.';
                            endif;

                            $Validator = Validator::make($this->request->all(), $Rules, $Messages);

                            if(! $Validator->passes() ):
                                return redirect()->to( $page->permalink )
                                    ->with('PasswordProtected.Error', 'There was a problem validating your inputs. Please try again.')
                                    ->with('protected-login-error', 'There was a problem signing you in. Please try again.')
                                    ->withInput()
                                    ->withErrors( $Validator );
                            endif;

                            Session::put('login-'.$this->data['page']->id, time(), 60);

                            return redirect()->to( ( session()->has('PasswordProtected.IntendedUrl') ) ? session()->pull('PasswordProtected.IntendedUrl') : $page->permalink )->with('PasswordProtected.Success', 'You\'ve successfully logged in. You\'re access is available for up to an hour');
                        endif;

                        if( !Session::has('login-' . $this->data['page']->id) ):
                            $this->data['view_location'] = ( View::exists('theme::partials.protected-page-login') ) ? 'theme::partials.protected-page-login' : 'HummingbirdBase::public.partials.protected.protected-login';
                            $this->data['html'] = NULL;
                        endif;
                    }
                }

                $ParentPage = $page->parentpage;
                
                while ($ParentPage) {
                    if($ParentPage && $ParentPage->protected == 1) {
                        if( !Session::has('login-' . $ParentPage->id) ):

                            session()->put('PasswordProtected.IntendedUrl', $page->permalink);

                            $this->data['view_location'] = ( View::exists('theme::partials.protected-page-login') ) ? 'theme::partials.protected-page-login' : 'HummingbirdBase::public.partials.protected.protected-login';
                            $this->data['html'] = NULL;
                            return redirect()->to( $ParentPage->permalink )->with('PasswordProtected.Error', 'Please login to view the page.');
                        endif;
                    }
                    $ParentPage = $ParentPage->parentpage;
                }
            }

            if( $event = Event::dispatch('frontend.page.beforeDisplay', ['data' => array_merge(['pageContent' => $this->data['html']], $this->data) ]) ) {
                $this->data['html'] = $event[0];
            }

            if( View::exists($this->data['view_location']) ) {
                $Content = $this->shortcodes( View::make($this->data['view_location'], $this->data)->withShortcodes()->render() , $page);

                return Response::make($Content);
            }
        }

        /**
         *
         * ERROR HANDLING
         *
         */
        if(count($this->data['clean_path']) == 0 OR $this->data['clean_path'][0] != 'hummingbird')
        {
            return $this->error();
        }

        App::abort(404);
    }


    /**
     *  Throw website error and store in the database for viewing
     * 
     *
     */ 
    public function error()
    {
        $FileHelper = new FileHelper;
        $URL        = $this->request->path();

        /* Find error */
        $Error      = RequestNotFound::where('url', '=', $URL)->first();

        if(null === $Error)
        {
            $Error  = new RequestNotFound;
            $Error->url = $URL;
            $Error->type = 'URL';

            /* Detect type of error */
            $path_parts = pathinfo($URL);

            if(isset($path_parts['extension']))
            {
                $path_parts['extension'] = strtolower($path_parts['extension']); //lower the extension
                $error_type = $FileHelper->getValidKeyByExt($path_parts['extension']);
                $Error->type = ($error_type != 'other') ? 'Files':ucfirst($error_type); //standard error if file not found in allowed types
            }
        }
        else
        {
            //error exists - update fields
            $Error->accessed++;
        }

        $Error->save();

        // 404 Error Page 
        $this->data['html'] = HtmlHelper::message('404');
        $this->data['page'] = null;
        $this->data['slug'] = '404';

        $page = Page::where('permalink', '/404/')->where('status', 'public')->first();

        if( $page ) {
            $this->data['html'] = $this->shortcodes($page->content, $page);
            $this->data['page'] = $page;
            $this->data['slug'] = $page->permalink;
            $this->registerBreadcrumb($this->data['page']);
            $this->registerSEO( $this->data['page'] );

            $this->data['view_location'] = $this->loadView();
        }

        return response()->view($this->data['view_location'], $this->data, 404);
    }

    public function parseURL(Array $url)
    {
        if(!isset($url['path'])) return;

        return $url['path'];
    }


    /**
     *  Checks that the page actually exists before outputting it (plugins only)
     * 
     * @return Boolean
     */ 
    public function page_exists($must_have_own_page = false)
    {
        return (isset($this->data['page']));
    }


    /**
     *  Allows plugins to naturally hook into the outputting of content
     * 
     * @param String    Theme view for the plugin
     * @param String    Plugin default view
     * @return View 
     *
     */ 
    public function view($theme_view = NULL, $default_view = NULL, $must_have_own_page = true)
    {
        if($this->page_exists($must_have_own_page)) {
            $RenderView = NULL;

            // Show the custom theme_view
            if(is_null($RenderView) && !is_null($theme_view) && View::exists($theme_view)) {
                $RenderView = $theme_view;
            }

            if(is_null($RenderView) && !is_null($default_view) && View::exists($default_view)) {
                $RenderView = $default_view;
            }

            // TODO: All views should go through show(), rather than duplication of rendering.
            // TODO: Tighten up all view outputs so that we can tie it into the core render to avoid duplicate Events firing
            if( $RenderView ) {
                $ResponseView = View::make($RenderView)
                    ->with($this->data)
                    ->with('html', $this->shortcodes($this->data['page']->content, $this->data['page']))
                    ->withShortcodes()
                    ->render();

                if( $event = Event::dispatch('frontend.page.beforeDisplay', ['data' => array_merge(['pageContent' => $ResponseView], $this->data) ]) ) {
                    $ResponseView = $event[0];
                }

                return Response::make($ResponseView, 200);
            }

            // Show the base page as we have nothing else
            return $this->show( $this->data['path']['path'] );
        }

        return $this->error();
    }


    /**
     * Updated: Filter action to make sure that a controller route, 
     * when in use, must have a page. Or be overriden when handling children.
     */
    public function hasPageFilter() {
        if( !$this->handles_children && empty($this->data['page']) ) {
            return $this->error();
        }
    }

    public function registerBreadcrumb($page = NULL) {
        if(class_exists('Breadcrumbs'))
        {
            if(null !== $page)
            {
                if( null !== $page->parentpage && $page->parentpage->id > 1 )
                    $this->registerBreadcrumb($page->parentpage);

                if( !Breadcrumbs::exists( $page->breadcrumbPath ) ):
                    Breadcrumbs::for($page->breadcrumbPath, function ($breadcrumbs) use ($page) {
                        if( empty($page->parentpage) || (!empty($page->parentpage) && $page->parentpage->id <= 1 ) ):
                            $breadcrumbs->parent('website-breadcrumb');
                        else:
                            $breadcrumbs->parent($page->parentpage->breadcrumbPath);
                        endif;

                        $breadcrumbs->push($page->breadcrumbTitle, url($page->permalink));
                    });
                endif;
            }
        }
    }

    protected function checkPageAccess() {
        if( ! Session::has('login-' . $this->data['page']->id) ) {
            if ($this->request->isMethod('post') && $this->request->filled('login') ) {
                $Rules = $Messages = [];

                if(!empty($this->data['page']->password)) {
                    $Rules      = ['password' => 'required'];
                    $Messages   = ['password.required' => 'Please enter the password.'];
                }

                if(!empty($this->data['page']->username)) {
                    $Rules['username'] = 'required|in:' . $this->data['page']->username;
                    $Messages['username.required'] = 'Please enter the username';
                    $Messages['username.in'] = 'Username is incorrect.';
                }

                $Validator = Validator::make($this->request->all(), $Rules, $Messages);

                if(! $Validator->passes() ) {
                    Session::flash('protected-login-error', 'There was a problem signing you in. Please try again.');
                    return false;
                }

                Session::put('login-'.$this->data['page']->id, time(), 60);
                return true;
            }

            return false;
        }

        return true;
    }
}
