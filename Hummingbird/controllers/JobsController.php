<?php namespace Hummingbird\Controllers;

use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Repositories\JobRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class JobsController extends CmsbaseController {
    public function __construct(Request $Request, JobRepository $JobRepository) {
        $this->JobRepository = $JobRepository;
    
        parent::__construct($Request);

        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Failed Jobs',
            'url' => route('hummingbird.failed-jobs.index')
        );
    }

    /**
     * Display resources
     */
    public function index() {
        try {
            $this->data['FailedJobs'] = $this->JobRepository->getFailedJobs()->paginate($this->pagination);

            return view('HummingbirdBase::cms.failed-jobs', $this->data);
        }
        catch(\Exception $e) {}

        return $this->forbidden(true);
    }

    /**
     * Retry selected Failed Job
     */
    public function retry($id = NULL) {
        try {
            $FailedJob = $this->JobRepository->findFailedJob( $id );

            $this->retryFailedJob( $FailedJob->id );

            return Redirect::route('hummingbird.failed-jobs.index')
                ->with('success', "Job #{$FailedJob->id} has been restarted");
        }
        catch(\Exception $e) {
            Log::error($e->getMessage());
        }

        return Redirect::route('hummingbird.failed-jobs.index')
            ->with('error', 'Unable to retry selected job');
    }

    /**
     * Retry all jobs
     */
    public function retry_all() {
        try {
            $TotalJobs = $this->JobRepository->getFailedJobs();

            if( $TotalJobs->count() > 0 ) {

                foreach($TotalJobs->get() as $FailedJob) {
                    $this->retryFailedJob( $FailedJob->id );
                }

                return Redirect::route('hummingbird.failed-jobs.index')
                    ->with('success', "{$TotalJobs->count()} failed entries have been restarted");
            }
        }
        catch(\Exception $e) {
            Log::error($e->getMessage());
        }

        return Redirect::route('hummingbird.failed-jobs.index')
            ->with('message', 'No jobs to restart');
    }

    /**
     * Delete selected Failed Job
     */
    public function destroy($id) {
        try {
            $FailedJob = $this->JobRepository->findFailedJob( $id );
            $FailedJob->delete();

            return Redirect::route('hummingbird.failed-jobs.index')
                ->with('success', "Job #{$id} has been deleted");
        }
        catch(\Exception $e) {
            Log::error($e->getMessage());
        }

        return Redirect::route('hummingbird.failed-jobs.index')
            ->with('error', 'Unable to delete selected job');
    }

    /**
     * Clean the queue
     */
    public function flush() {
        try {
            $TotalJobs = $this->JobRepository->getFailedJobs()->count();

            if( $TotalJobs > 0 ) {
                $this->JobRepository->flush();

                return Redirect::route('hummingbird.failed-jobs.index')
                    ->with('success', "{$TotalJobs} failed entries have been removed");
            }
        }
        catch(\Exception $e) {
            Log::error($e->getMessage());
        }

        return Redirect::route('hummingbird.failed-jobs.index')->with('message', "No jobs to clear");
    }

    /**
     * Retrying a specific Failed Job in the queue
     * @param  Integer $FailedJobId
     */
    protected function retryFailedJob($FailedJobId) {
        Artisan::call( "queue:retry", ['id' => $FailedJobId] );
    }
}