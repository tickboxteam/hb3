<?php namespace Hummingbird\Controllers\Frontend;

use Hummingbird\Controllers\FrontendController;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class ArchivesFrontendController extends FrontendController {
    public $handles_children = false;
    
    /**
     * Frontend Controller for allowing Archives to be loaded into views
     * Must have a page in the CMS for these to work
     */
    public function __construct(Request $Request) {
        if( $Request->segments() > 2 ) {
            $this->handles_children = true;
        }

        parent::__construct($Request);

        $this->hasPageFilter();
    }
    
    /**
     * Index: Show listings
     */
    public function index() {
        return parent::view('theme::archives');
    }

    /**
     * Index: Show category based listings
     * @param String $slug  Find all items related to this tag
     */
    public function viewByTag($slug = NULL) {
        return parent::view('theme::archives');
    }

    /**
     * Index: Show category based listings
     * @param String $slug  Find all items related to this category
     */
    public function viewByCategory($slug = NULL) {
        return parent::view('theme::archives');
    }
}
