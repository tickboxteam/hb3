<?php namespace Hummingbird\Controllers;

use App;
use Auth;
use General;
use Redirect;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Module;
use Hummingbird\Models\Moduletemplate;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class ModuleController extends CmsbaseController
{
    public function __construct(Request $Request, Module $Module)
    {
        parent::__construct($Request);

        $this->model = $Module;

        $this->data['moduletemplates'] = Moduletemplate::get_selection();  

        if( !Breadcrumbs::exists('hummingbird/modules') ):
            Breadcrumbs::for("hummingbird/modules", function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Modules', route('hummingbird.modules.index'));
            });
        endif;

        $this->activateSearch();
    }

    public function index()
    {
        if($this->user->userAbility('modules.read'))
        {
            $this->data['tag'] = 'Manage Modules';
            
            $query = Module::search($this->Request->get('s'))->microsite();

            if(!empty($this->Request->filled('filter_by_mod_template')) && $this->Request->get('filter_by_mod_template') > 0) {
                $query->where('moduletemplate_id', $this->Request->get('filter_by_mod_template'));
            }

            $orderByDirection = $this->Request->get('order_by_direction');

            if(!empty($this->Request->filled('order_by_fields'))) {
               $query->orderBy($this->Request->get('order_by_fields'), $orderByDirection);
            } else {
                $query->orderBy('name', 'ASC');
            }

            $this->data['modules'] = $query->paginate($this->pagination);

            return View::make('HummingbirdBase::cms/modules', $this->data);
        }

        return parent::forbidden();
    }
    
    public function show($id)
    {
        if($this->user->userAbility('modules.update')) {
            try {
                $this->data['module'] = Module::microsite()->where('id', $id)->firstOrFail();

                $this->data['tag'] = 'Edit '.$this->data['module']->title;

                Breadcrumbs::for("hummingbird/modules/".$this->data['module']->id, function ($breadcrumbs)  {
                    $breadcrumbs->parent("hummingbird/modules");
                    $breadcrumbs->push('Edit: ' . $this->data['module']->name, url('hummingbird.modules.show', $this->data['module']->id));
                });

                $this->data['taxonomy'] = $this->data['module']->taxonomy();

                return View::make('HummingbirdBase::cms/modules-edit', $this->data);
            }
            catch( \Exception $e) {
                // silence   
            }

            return Redirect::route('hummingbird.modules.index', ['fail' => 'noedit'])->with('error', 'Unable to edit module');
        }
        
        return parent::forbidden();
    }
    
    public function update($id)
    {   
        if($this->user->userAbility('modules.update')) {
            try {
                $input = $this->Request->except('_token');
                $module = Module::microsite()->where('id', $id)->firstOrFail();

                $validator = Validator::make($input, [
                    'name' => "required|unique:modules,name,$id|max:255"
                ], [
                    'name.required' => 'The :attribute field is required.',
                    'name.unique' => 'The :attribute must be unique.',
                    'name.max' => 'The :attribute must be shorter than 255 characters'
                ]);

                if ($validator->fails()) {
                    return Redirect::route('hummingbird.modules.show', [$id])
                        ->with('error', 'Unable to update module')
                        ->withInput()
                        ->withErrors($validator);
                }

                $module->update($input);
                
                $data = array();

                if( !$module->moduletemplate ) {
                    $data['structure'] = $this->Request->get('html');
                    $data['css'] = $this->Request->get('css');
                    $data['js'] = $this->Request->get('js');
                    $module->module_data = serialize($data);
                }
                else {
                    $data = [];

                    if( $this->Request->filled('fields') ) {
                        foreach($input['fields'] as $key => $field) {
                            $data['structure'][$key] = $field;
                        }
                    }

                    $module->module_data = serialize($data);
                }

                /* Store cats and tags */
                $input['taxonomy'] = (isset($input['taxonomy'])) ? $input['taxonomy']:array();
                $module->storeTaxonomy($input['taxonomy']);

                if($module->save()) {
                    Activitylog::log([
                        'action' => 'UPDATED',
                        'type' => get_class($module),
                        'link_id' => $module->id,
                        'description' => 'Updated module',
                        'notes' => Auth::user()->username . " updated the module - &quot;$module->name&quot;"
                    ]);

                    return Redirect::route('hummingbird.modules.show', [$id])
                        ->with('success', 'Module has been updated successfully.');
                }

                return Redirect::route('hummingbird.modules.show', [$id, 'fail' => 'nosave'])
                    ->with('error', 'Module could not be updated.');
            }
            catch(\Exception $e) {
                // silence is golden
            }

            return Redirect::to(App::make('backend_url').'/modules/?fail=no-module-found')
                ->with('error', 'Module could not be found.');
        }
        
        return parent::forbidden();
    }
    
    public function store()
    {
        if($this->user->userAbility('modules.create')) {
            $input = $this->Request->except('_token', 'add');        
            $module = (new Module)->fill($input);

            $validator = Validator::make($input, [
                'name' => "required|unique:modules|max:255"
            ], [
                'name.required' => 'The :attribute field is required.',
                'name.unique' => 'The :attribute must be unique.',
                'name.max' => 'The :attribute must be shorter than 255 characters'
            ]);

            if ($validator->fails()) {
                return Redirect::route('hummingbird.modules.index')
                    ->withErrors($validator)
                    ->withInput();
            }
            
            if(isset($this->data['Microsites']) AND null !== $this->data['Microsites'] AND null !== $this->data['microsite']) $module->microsite_id = $this->data['microsite']->id;

            if(!$module->save()) {
                return Redirect::route('hummingbird.modules.index')
                    ->withErrors( $module->errors() )
                    ->withInput();
            }

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($this->model),
                'link_id' => $module->id,
                'description' => 'Created a new module',
                'notes' => Auth::user()->username . " created a new module - &quot;$module->name&quot;"
            ]); 

            return Redirect::route('hummingbird.modules.show', [$module->id])
                ->with('success', 'Module has been created');
        }
        
        return parent::forbidden();
    }

    public function replicate($id)
    {
        if($this->user->userAbility('modules.create')) {
            try {
                $module = Module::microsite()->where('id', $id)->firstOrFail(); //get module to replicate
                $ReplicatedModule = $module->replicate();
                $ReplicatedModule->name .= ' ' . time();

                if(isset($this->data['Microsites']) AND null !== $this->data['Microsites'] AND null !== $this->data['microsite']) $ReplicatedModule->microsite_id = $this->data['microsite']->id;
                
                if( !$ReplicatedModule->save() ) {                    
                    return Redirect::route('hummingbird.modules.index', ['fail' => 'problem-replicating'])
                        ->with('error', 'There was a problem replicating this module.');
                }

                $ReplicatedModule->storeTaxonomy( array_merge($module->categories->pluck('id')->all(), $module->tags->pluck('id')->all()) );

                Activitylog::log([
                    'action' => 'CREATED',
                    'type' => get_class($this->model),
                    'link_id' => $ReplicatedModule->id,
                    'description' => 'Created a new module',
                    'notes' => Auth::user()->username . " created a new module - &quot;$ReplicatedModule->name&quot; by replicating an existing module"
                ]); 

                return Redirect::route('hummingbird.modules.show', [$ReplicatedModule->id])->with('success', 'Module cloned.');
            }
            catch(\Exception $e) {
                return Redirect::route('hummingbird.modules.index')
                    ->with('error', 'Could not replicate module.');
            }
        }
        
        return parent::forbidden();
    }

    public function destroy($id)
    {
        if($this->user->userAbility('modules.delete')) {
            try {
                $Module = Module::microsite()->where('id', $id)->firstOrFail();

                $name = $Module->name;
                $Module->delete();

                Activitylog::log([
                    'action' => 'DELETED',
                    'type' => get_class( new Module ),
                    'link_id' => $id,
                    'description' => 'Deleted module',
                    'notes' => Auth::user()->username . " deleted the module &quot;$name&quot; role"
                ]);

                return Redirect::route('hummingbird.modules.index')
                    ->with('success', 'Module deleted');
            }
            catch(\Exception $e) {
                // silence
            }

            return Redirect::route('hummingbird.modules.index')->with('error', 'Module not found.');
        }
        
        return parent::forbidden();
    }
}
