<?php namespace Hummingbird\Controllers;

use App;
use Auth;
use General;
use Redirect;
use Session;
use SplTempFileObject;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Redirections;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
/*
|--------------------------------------------------------------------------
| Site Error Controller
|--------------------------------------------------------------------------
|
| Here is where you can monitor the errors that appear on the site. Manage
| and delete wherever necessary to make sure your site maintains a healthy
| platform.
|
| Needs to INSTALL
| Needs to REGISTER NAVIGATION MENU
| Needs to REGISTER WIDGETS (CMS and FRONTEND)
| Needs to EXPORT DATA
| <iframe width="608" height="358" src="https://www.youtube.com/embed/r1lVPrYoBkA" frameborder="0" allowfullscreen></iframe>
|
*/

use League\Csv\Writer;

class RedirectionsController extends CmsbaseController
{
    public $code_location = 'dashboard';

    public function __construct(Request $Request, Redirections $Redirections)
    {
        parent::__construct($Request);

        $this->model = $Redirections;

        if( !Breadcrumbs::exists('hummingbird/redirections') ):
            Breadcrumbs::for('hummingbird/redirections', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Website redirects', url('hummingbird/redirections'));
            });
        endif;
        
        $this->activateSearch();
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if($this->user->userAbility('redirections.read')) {
            if(!$this->Request->filled('orderBy')) {
                $this->Request->merge(['orderBy' => 'from']);
            }

            if(!$this->Request->filled('order')) {
                $this->Request->merge(['order' => 'asc']);
            }

            $orderBy    = ($this->Request->filled('orderBy')) ? $this->Request->get('orderBy') : 'from';
            $order      = ($this->Request->filled('order')) ? $this->Request->get('order') : 'asc';

            $redirections = Redirections::cmsFilter($this->Request->all())->orderBy($orderBy, $order)->paginate($this->pagination);

            $this->data['tag']          = 'Manage Redirects';
            $this->data['redirects']    = $redirections;
            $this->data['searchTerm']   = $this->Request->get('s');

            return View::make('HummingbirdBase::cms.redirections', $this->data);
        }

        return parent::forbidden();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if($this->user->userAbility('redirections.create')) {
            $ExternalRedirect = isLinkExternal( $this->Request->get('to') );

            $this->Request->merge([
                'from' => ( $this->Request->filled('from') ) ? clean_and_sluggify_url( $this->Request->get('from') ) : NULL,
                'to' => clean_and_sluggify_url( $this->Request->get('to') )
            ]);

            $rules = array(
                'from' => 'required|unique:redirections,from,NULL,id,deleted_at,NULL|different:to',
                'to' => 'required',
                'type' => 'required',
                'category' => 'required',
                'custom_category' => 'required_if:category,NEW',
                'function' => 'required'
            );
            $messages = [
                'from.required' => 'Please enter the URL you are redirecting from',
                'from.unique' => 'This URL already exists, please edit that one or delete it and try again',
                'from.different' => 'Looks like you are trying to redirect to the same URL. This will cause an infinite loop on your website and has been removed',
                'to.required' => 'Please enter the desired location',
                'type.required' => 'Please select the type of redirect',
                'function.required' => 'Please select the function of this redirect'
            ];

            $validator = Validator::make(
                $this->Request->all(),
                $rules,
                $messages
            );

            /* Does it validate? */
            if($validator->fails())
            {
                return Redirect::route('hummingbird.redirections.index', $this->getReferrerParameters() )
                    ->withInput()
                    ->withErrors($validator);
            }

            $all_input  = $this->Request->except('_token');
            $input      = $this->Request->except('category', 'custom_category', '_token');
            
            $redirect = (new Redirections)->fill($input);
            $redirect->category = ( isset($all_input['category']) && $all_input['category'] == 'NEW' ) ? trim($all_input['custom_category']) : $all_input['category'];

            /* check if irl is rexternal or internal */
            if( $ExternalRedirect ) {
                $headers = @get_headers($all_input['to']);

                // Use condition to check the existence of URL
                if(!$headers || strpos( $headers[0], '200') === FALSE ) {
                    Session::flash('message', "Redirect URL curently not accessible or unavailable. Please check.");
                }
            }

            $redirect->save();

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($redirect),
                'link_id' => $redirect->id,
                'description' => 'Created new redirect',
                'notes' => Auth::user()->username . " created a new redirect"
            ]);

            return Redirect::route('hummingbird.redirections.index')
                    ->with('success', "Website redirect has been added successfully.");  
        }

        return parent::forbidden();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        if($this->user->userAbility('redirections.update')) {
            try {
                $this->data['redirect'] = Redirections::findOrFail($id);;

                Breadcrumbs::for("hummingbird/redirections/".$this->data['redirect']->id, function ($breadcrumbs)  {
                    $breadcrumbs->parent('hummingbird/redirections');
                    $breadcrumbs->push('Edit: &quot;' . $this->data['redirect']->from.'&quot;', url('/hummingbird/redirections/'.$this->data['redirect']->id.'/edit'));
                });

                return View::make('HummingbirdBase::cms/redirections-edit', $this->data);
            }
            catch(\Exception $e) {}

            return Redirect::to('hummingbird/redirections')->with('error', 'Unable to find redirect.');
        }

        return parent::forbidden();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if($this->user->userAbility('redirections.update')) {
            /* Validation rules */
            $rules = array(
                'from' => "required|unique:redirections,from,{$id},id,deleted_at,NULL|different:to",
                'to' => 'required',
                'type' => 'required',
                'category' => 'required',
                'custom_category' => 'required_if:category,NEW',
                'function' => 'required'
            );
            $messages = [
                'from.required' => 'Please enter the URL you are redirecting from',
                'from.unique' => 'This URL already exists, please edit that one or delete it and try again',
                'from.different' => 'Looks like you are trying to redirect to the same URL. This will cause an infinite loop on your website and has been removed',
                'to.required' => 'Please enter the desired location',
                'type.required' => 'Please select the type of redirect',
                'function.required' => 'Please select the function of this redirect'
            ];

            /* Inputs */
            $ExternalRedirect = isLinkExternal( $this->Request->get('to') );

            $this->Request->merge([
                'from' => ( $this->Request->filled('from') ) ? clean_and_sluggify_url( $this->Request->get('from') ) : NULL,
                'to' => clean_and_sluggify_url( $this->Request->get('to') )
            ]);

            $all_input  = $this->Request->except('_token');
            $input      = $this->Request->except('category', 'custom_category', '_token');

            $validator = Validator::make(
                $this->Request->all(),
                $rules,
                $messages
            );

            /* Does it validate? */
            if($validator->fails()) {
                return Redirect::route('hummingbird.redirections.edit', array('redirections' => $id))->withInput()->withErrors($validator);
            }

            /* Get redirect */
            $redirect = Redirections::find($id)->fill($input);
            $redirect->category = ( isset($all_input['category']) && $all_input['category'] == 'NEW' ) ? trim($all_input['custom_category']) : $all_input['category'];

            /* check if irl is rexternal or internal */
            if( $ExternalRedirect ) {
                $headers = @get_headers($all_input['to']);

                // Use condition to check the existence of URL
                if(!$headers || strpos( $headers[0], '200') === FALSE ) {
                    Session::flash('message', "Redirect URL curently not accessible or unavailable. Please check.");
                }
            }

            /* save? */
            $redirect->save();

            /* Store activity log */
            Activitylog::log([
                'action' => 'UPDATED',
                'type' => get_class($redirect),
                'link_id' => $redirect->id,
                'description' => 'Updated an existing redirect',
                'notes' => Auth::user()->username . " updated details on a current redirect"
            ]);

            Session::flash('success', 'Redirect successfully updated.');
            
            return Redirect::route('hummingbird.redirections.show', $id);
        }

        return parent::forbidden();
    }


    /**
     * Remove the specified redirect.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if($this->user->userAbility('redirections.delete')) {
            try {
                if($id == 'remove-all') {
                    $redirects = Redirections::all();

                    if(count($redirects) > 0) {
                        foreach($redirects as $redirect) {
                            $redirect->delete();
                        }

                        return Redirect::route('hummingbird.redirections.index', $this->getReferrerParameters() )->with('success', 'Successfully removed all redirections.');
                    }

                    return Redirect::route('hummingbird.redirections.index', $this->getReferrerParameters() )->with('error', 'No redirects to remove');
                }

                if( $this->Request->filled('delete-individual') ) {
                    $redirect = Redirections::findOrFail( $this->Request->get('delete-individual') );
                    $redirect->delete();

                    return Redirect::route('hummingbird.redirections.index', $this->getReferrerParameters())->with('success', 'Successfully deleted &quot;'.$redirect->from.'&quot; as a '.$redirect->type.' redirect.');
                }

                if( $this->Request->filled('delete-selected') && count($this->Request->get('delete-selected')) > 0 ) {
                    $Redirects = Redirections::find( $this->Request->get('delete-selected') );

                    if( Redirections::destroy( $this->Request->get('delete-selected') ) ) {
                        return Redirect::route('hummingbird.redirections.index', $this->getReferrerParameters())->with('success', 'Successfully deleted ' . $Redirects->count() . General::singular_or_plural($Redirects->count(), ' redirect'));
                    }
                }
            }
            catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return Redirect::route('hummingbird.redirections.index', $this->getReferrerParameters())->with('error', 'Unable to find redirect.');
            }
            catch(\Exception $e) {}

            return Redirect::route('hummingbird.redirections.index', $this->getReferrerParameters())->with('error', 'Unable to process your request.');
        }

        return parent::forbidden();
    }

    /**
     * The ability to return a CSV export of all redirects
     */
    public function export() {
        if( ! $this->user->userAbility('redirections.read') ) {
            return parent::forbidden();
        }

        $orderBy        = ($this->Request->filled('orderBy')) ? $this->Request->get('orderBy') : 'from';
        $order          = ($this->Request->filled('order')) ? $this->Request->get('order') : 'asc';
        $Redirections   = Redirections::cmsFilter($this->Request->all())->orderBy($orderBy, $order)->get();

        if( $Redirections->count() <= 0 ) {
            return Redirect::route('hummingbird.redirections.index')
                ->with('error', 'Nothing to export.');
        }

        $writer = Writer::createFromFileObject(new SplTempFileObject());

        $writer->insertOne(array("id","From","To","Type","Comment","Category","Function","Date Added","Last Used","Used"));

        foreach($Redirections as $Redirection) {
            $writer->insertOne(array(
                $Redirection->id,
                $Redirection->from,
                $Redirection->to,
                $Redirection->type,
                $Redirection->comments,
                $Redirection->category,
                $Redirection->function,
                $Redirection->created_at->format("Y-m-d H:i:s"),
                (!empty($Redirection->last_used)) ? $Redirection->last_used->format("Y-m-d H:i:s") : '',
                $Redirection->no_used
            ));
        }

        $writer->output('redirects_export.csv');
    }


    /**
     * Only show the deleted redirects in the system
     */
    public function show_deleted() {
        if($this->user->userAbility('redirections.delete')) {
            $this->data['redirects'] = Redirections::onlyTrashed()->orderBy('from')->paginate($this->pagination);

            Breadcrumbs::for("hummingbird/redirections/deleted", function ($breadcrumbs)  {
                $breadcrumbs->parent('hummingbird/redirections');
                $breadcrumbs->push('Deleted redirects', url('/hummingbird/redirections/deleted'));
            });

            if( $this->data['redirects']->count() <= 0 ) {
                return Redirect::route('hummingbird.redirections.index')->with('message', 'There are no redirects that have been deleted');
            }

            return View::make('HummingbirdBase::cms.redirections-deleted', $this->data);
        }

        return parent::forbidden();
    }

    /**
     * We're going to try and re-instate this redirect, but only 
     * after we've checked to see if the "from" link doesn't 
     * already exist and is not live.
     */
    public function reinstate($id) {
        if($this->user->userAbility('redirections.delete')) {
            try {
                $Redirect = Redirections::onlyTrashed()->where('id', $id)->firstOrFail();

                $validator = Validator::make(
                    ['from' => $Redirect->from],
                    ['from' => "unique:redirections,from,{$id},id,deleted_at,NULL"]
                );

                if($validator->fails()) {
                    return Redirect::route('hummingbird.redirections.deleted')->with('error', 'This redirect is already live. Please delete the current live redirect before reinstating this one');
                }

                $Redirect->restore();

                return Redirect::route('hummingbird.redirections.deleted')->with('success', 'Redirect has been re-instated');
            }
            catch(\Exception $e) {
                return Redirect::route('hummingbird.redirections.deleted')->with('error', 'Unable to re-instate redirect');
            }
        }

        return $this->forbidden();
    }

    // If we're re-instating all, then ALL redirects need checking to see they're unique. If not, they are ignored.
    // Force delete ALL redirects and then move on
    public function reinstate_all() {
        if($this->user->userAbility('redirections.delete')) {
            $Redirects = Redirections::onlyTrashed()->get();
            $CurrentCount = $Redirects->count();

            foreach($Redirects as $Redirect) {
                try {
                    $validator = Validator::make(
                        ['from' => $Redirect->from],
                        ['from' => "unique:redirections,from,{$Redirect->id},id,deleted_at,NULL"]
                    );

                    // No need to show validation errors, just catch it
                    if( !$validator->fails() ) {
                        $Redirect->restore();
                    }
                }
                catch(\Exception $e) {

                    // Do nothing - as we can't reinstate it
                }
            }

            if( Redirections::onlyTrashed()->count() == $CurrentCount ) {
                return Redirect::route('hummingbird.redirections.deleted')->with('error', 'None of the redirects could be reinstated');
            }

            if( Redirections::onlyTrashed()->count() > 0 ) {
                return Redirect::route('hummingbird.redirections.deleted')->with('success', 'Some, but not all redirects, were re-instated.');
            }

            return Redirect::route('hummingbird.redirections.index')->with('success', 'Redirects have been re-instated.');
        }

        return $this->forbidden();
    }

    /**
     * Deleting this resource for good
     */
    public function purge($id) {
        if($this->user->userAbility('redirections.delete')) {
            try {
                $Redirect = Redirections::onlyTrashed()->where('id', $id)->firstOrFail();

                $Redirect->forceDelete();

                return Redirect::route('hummingbird.redirections.deleted')->with('success', 'Redirect (and all statistics) have been deleted.');
            }
            catch(\Exception $e) {
                return Redirect::route('hummingbird.redirections.deleted')->with('error', 'Unable to permanently redirect');
            }
        }

        return $this->forbidden();
    }

    /**
     * Deleting ALL items that are marked as deleted
     */
    public function purge_all() {
        if($this->user->userAbility('redirections.delete')) {
            $Redirects = Redirections::onlyTrashed()->get();

            foreach($Redirects as $Redirect) {
                try {
                    $Redirect->forceDelete();
                }
                catch(\Exception $e) {
                    // Do nothing - as we can't delete it
                }
            }

            if( Redirections::onlyTrashed()->count() > 0 ) {
                return Redirect::route('hummingbird.redirections.deleted')->with('success', 'Some, but not all redirects, were deleted.');
            }

            return Redirect::route('hummingbird.redirections.index')->with('success', 'Redirects have been purged.');
        }

        return $this->forbidden();
    }
}