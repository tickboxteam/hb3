<?php namespace Hummingbird\Controllers;

use App;
use Auth;
use General;
use Redirect;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Menu;
use Hummingbird\Models\Menuitem;
use Hummingbird\Models\Page;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class MenuController extends CmsbaseController
{
    /**
     * Inject the models.
     * @param Menu $menu
     */
    public function __construct(Request $Request, Menu $menu)
    {
        parent::__construct($Request);

        $this->menu = $menu;

        if( !Breadcrumbs::exists('hummingbird/menus') ):
            Breadcrumbs::for('hummingbird/menus', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Menus', url('hummingbird/menus'));
            });
        endif;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if( Auth::user()->userAbility('menu.read') ) {
            $this->data['tag'] = 'Manage Menus';
            $this->data['menus'] = Menu::orderBy('name')->get();
            return View::make('HummingbirdBase::cms.menus', $this->data);
        }

        return parent::forbidden();
    }

    /**
     * Create a new object
     *  
     */
    public function store()
    {
        if(Auth::user()->userAbility('menu.create')) {
            $input = $this->Request->except('_token', 'add');   
            $validator = Validator::make($input, Menu::$rules);

            if ($validator->fails())
            {
                return Redirect::route('hummingbird.menus.index')->withErrors($validator->errors());
            }

            $menu = (new Menu)->fill($input);
            
            if(!$menu->save()) {
                return Redirect::route('hummingbird.menus.index')->withErrors($menu->errors());
            }

            /* Store activity */
            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($menu),
                'link_id' => $menu->id,
                'description' => 'Created new navigation menu',
                'notes' => Auth::user()->username . " has created a new menu - &quot;$menu->name&quot;"
            ]); 

            return Redirect::route('hummingbird.menus.index')->with('success', 'Menu has been created');
        }

        return $this->forbidden();
    }
    
    public function edit($id = NULL)
    {                
        if(Auth::user()->userAbility('menu.update')) {
            try {
                $this->data['menu'] = Menu::findOrFail($id);
                $this->data['tag'] = 'Edit '.$this->data['menu']->name;

                Breadcrumbs::for("hummingbird/menus/{$this->data['menu']->id}/edit", function ($breadcrumbs)  {
                    $breadcrumbs->parent('hummingbird/menus');
                    $breadcrumbs->push('Edit: ' . $this->data['menu']->name, url('/hummingbird/menus/edit/' . $this->data['menu']->id));
                });

                return View::make('HummingbirdBase::cms.menus-edit', $this->data);
            }
            catch(\Exception $e) {
                return Redirect::route('hummingbird.menus.index')->with('error', 'Menu could not be found');
            }
        }

        return $this->forbidden();
    }
    

    /**
     * Update menu and its associated objects
     *  
     */
    public function update($id)
    {
        if(Auth::user()->userAbility('menu.update')) {
            try {
                $menu = Menu::findOrFail($id);
                $input = $this->Request->except('_token', 'add');   

                if( $this->Request->filled('action') && $this->Request->get('action') == 'update' ):
                    $validator = Validator::make($input, Menu::$rules);

                    if( $validator->fails() ) {
                        return Redirect::route('hummingbird.menus.edit', $id)->withInput()->withErrors($validator->errors());
                    }

                    $menu->fill($input);
                    
                    if( !$menu->save() ) {
                        return Redirect::route('hummingbird.menus.edit', $id)->withInput()->withErrors($menu->errors());
                    }

                    if( $this->Request->filled('menu_items') ):
                        foreach($this->Request->get('menu_items') as $menuitem_id => $menuitem_details):
                            $menuitem = Menuitem::find($menuitem_id);
                            $menuitem->parent = $menuitem_details[ 'parent' ];
                            $menuitem->menu_item_name = $menuitem_details[ 'menu_item_name' ];
                            $menuitem->url = $menuitem_details[ 'url' ];
                            $menuitem->title = $menuitem_details[ 'title' ];
                            $menuitem->classes = $menuitem_details[ 'classes' ];
                            $menuitem->menu_item_id = $menuitem_details[ 'menu_item_id' ];
                            $menuitem->new_window = $menuitem_details[ 'new_window' ];
                            $menuitem->save();
                        endforeach;
                    endif;
                endif;

                if( $this->Request->filled('action') && $this->Request->get('action') == 'new-menu-items' ):
                    $Validator = Validator::make($this->Request->all(), [
                        'menu_item_type' => 'required|in:custom,page',
                        'menu_item_name' => 'required_if:menu_item_type,custom',
                        'menu_item_permalink' => 'required_if:menu_item_type,custom',
                        'menu_item_pages' => 'required_if:menu_item_type,page'
                    ], []);

                    if( $Validator->fails() ):
                        return Redirect::route('hummingbird.menus.edit', $id)->withErrors($Validator)->withInput()->with('CreateMenuItemError', 'Please review your inputs');
                    endif;

                    $Position = $menu->items()->max('order') ?: 0;

                    switch( $this->Request->get('menu_item_type') ):
                        case 'custom':
                            $Position++;

                            Menuitem::create([
                                'menu_id' => $menu->id, 
                                'menu_item_name' => $this->Request->get('menu_item_name'),
                                'parent' => $this->Request->get('parent_menu_item') ?: NULL,
                                'order' => $Position, 
                                'url' => $this->Request->get('menu_item_permalink'),
                                'title' => $this->Request->get('menu_item_title'),
                                'classes' => $this->Request->get('menu_item_classes'),
                                'menu_item_id' => $this->Request->get('menu_item_menu_id'),
                                'new_window' => $this->Request->get('menu_item_new_window')
                            ]);

                            break;
                        case 'page':
                            foreach( $this->Request->get('menu_item_pages') as $MenuItemPage ):
                                try {
                                    $FoundPage = Page::findOrFail( $MenuItemPage );

                                    Menuitem::create([
                                        'menu_id' => $menu->id, 
                                        'menu_item_name' => $FoundPage->getMenuName(), 
                                        'parent' => $this->Request->get('parent_menu_item') ?: NULL,
                                        'order' => $Position, 
                                        'url' => $FoundPage->permalink,
                                        'title' => $FoundPage->title
                                    ]);
                                }
                                catch(\Exception $e) {
                                    // silence - do nothing
                                }

                            endforeach;

                            break;
                    endswitch;
                endif;

                /* Store activity */
                Activitylog::log([
                    'action' => 'UPDATED',
                    'type' => get_class($this->menu),
                    'link_id' => $id,
                    'description' => 'Updated navigation menu',
                    'notes' => Auth::user()->username . " has just updated the - &quot;" . $menu->name . "&quot; menu"
                ]); 

                return Redirect::route('hummingbird.menus.edit', $id)->with('success', 'Menu has succesfully updated.');
            }
            catch(ModelNotFoundException $e) {
                return Redirect::route('hummingbird.menus.index')->with('error', 'Menu could not be found.');
            }
            catch(\Exception $e) {
                // silence here
            }

            return Redirect::route('hummingbird.menus.edit', $id)->with('error', 'Menu could not be saved.');
        }

        return $this->forbidden();
    }
    

    /**
     * Delete associated object
     *  
     */
    public function destroy($id)
    {
        if(Auth::user()->userAbility('menu.delete')) {
            
            $menu = Menu::find($id);

            if(null !== $menu)
            {
                $menu_name = $menu->name;
                $menu_id = $menu->id;
                
                if($menu->delete())
                {        
                    /* Store activity */
                    Activitylog::log([
                        'action' => 'DELETED',
                        'type' => get_class($this->menu),
                        'link_id' => $menu_id,
                        'description' => 'Deleted navigation menu',
                        'notes' => Auth::user()->username . " has just deleted the - &quot;$menu_name&quot; menu"
                    ]); 

                    return Redirect::route('hummingbird.menus.index')->with('success','Menu has been deleted');
                }

                return Redirect::route('hummingbird.menus.index')->with('error','Menu could not be deleted.');
            }

            return Redirect::route('hummingbird.menus.index')->with('error','Menu not found.');
        }

        return $this->forbidden();
    }
        

    /**
     * Delete object associated to Menu
     *  
     */
    public function deleteItem($id)
    {
        if(Auth::user()->userAbility('menu.update')) {
            $menuitem = Menuitem::findOrFail($id);
            
            if(null !== $menuitem) {
                $this->menu = $menuitem->menu;

                $old_link = $menuitem->url;
                $old_name = $menuitem->menu_item_name;

                if($menuitem->delete()) {
                    Activitylog::log([
                        'action' => 'UPDATED',
                        'type' => get_class($this->menu),
                        'link_id' => $this->menu->id,
                        'description' => 'Updated navigation menu',
                        'notes' => Auth::user()->username . " has just updated the - &quot;" . $this->menu->name . "&quot; menu by removing the following link " . $old_name . " (" . $old_link . ")"
                    ]);
                    
                    return Redirect::route('hummingbird.menus.edit', $this->menu->id)->with('success','Menu item has been deleted');
                }
            }

            return Redirect::route('hummingbird.menus.edit', $this->menu->id)
                ->with('error','There was a problem deleting this item. Please try again.');
        }

        return parent::forbidden();
    }

    /**
     * Display a page with all categories so that they can be easily moved around (positions and parents)
     * 
     */
    public function get_update_order($id = NULL)
    {
        if(Auth::user()->userAbility('menu.update')) {
            $this->data['menu'] = Menu::find($id);

            if( $this->data['menu']->menuitems->count() > 0 ) {
                Breadcrumbs::for("hummingbird/menus/{$this->data['menu']->id}/edit", function ($breadcrumbs)  {
                    $breadcrumbs->parent('hummingbird/menus');
                    $breadcrumbs->push('Edit: ' . $this->data['menu']->name, url("/hummingbird/menus/{$this->data['menu']->id}/edit"));
                });

                Breadcrumbs::for("hummingbird/menus/re-order/".$this->data['menu']->id, function ($breadcrumbs)  {
                    $breadcrumbs->parent("hummingbird/menus/{$this->data['menu']->id}/edit");
                    $breadcrumbs->push('Re-order');
                }); 
            
                return View::make('HummingbirdBase::cms.menus-reorder', $this->data);
            }

            return Redirect::route('hummingbird.menus.edit', $this->data['menu']->id);
        }

        return $this->forbidden();
    }



    /**
     * Process the updating of category positions
     * 
     */
    public function update_order($id = NULL)
    {
        if($this->Request->filled('SortOrder'))
        {
            $this->update_menus(json_decode($this->Request->get('SortOrder')));
        }
        $this->data['menu'] = Menu::find($id);

        return Redirect::route('hummingbird.menus.re-order', $id)->with('success', "&quot;{$this->data['menu']->name}&quot; has been reordered.");
    }

    /**
     * Recursive iteration for updating categories and category children
     *
     * @param Mixed     $SortedCategories   [List of categories (inc Children if provided)]
     * @param Integer   $counter            [The position index]
     * @param Integer   $parent_category_id [Whether we are looking at a parent category]
     */
    public function update_menus($SortedMenus = [], $counter = 1, $parent_menu_id = NULL)
    {
        if(count($SortedMenus) > 0)
        {
            foreach($SortedMenus as $SortedMenu)
            {
                $menu              = Menuitem::find($SortedMenu->id);
                $menu->order       = $counter;
                $menu->save();
                $counter++;
            }
        }
    }
}
