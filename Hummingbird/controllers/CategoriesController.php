<?php namespace Hummingbird\Controllers;

use App;
use Auth;
use Config;
use DB;
use Log;
use Redirect;
use Response;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Categories;
use Hummingbird\Repositories\TaxonomyCategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class CategoriesController extends CmsbaseController
{
    protected $list_cats = array();

    public function __construct(Request $Request, TaxonomyCategoryRepository $TaxonomyCategoryRepository)
    {
        parent::__construct($Request);

        $this->Category = $TaxonomyCategoryRepository;

        if( !Breadcrumbs::exists('hummingbird/categories') ):
            Breadcrumbs::for('hummingbird/categories', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Categories', route('hummingbird.categories.index'));
            });
        endif;

        $this->data['list_cats'] = array();
        $this->data['cats'] = Categories::objectPermissions()->type()->orderBy('SortOrder', 'ASC')->get();

        $this->data['deleted_cats'] = Categories::deletedCategories(true);

        $this->buildTree($this->data['cats']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if($this->user->userAbility('categories.read')) {

            if(null !== $this->Request->get('s') OR $this->Request->get('s') != '')
            {
                $this->data['cats'] = Categories::objectPermissions()->ordered()->whereNull('deleted_at')->type()->search($this->Request->get('s'))->orderBy('name', 'ASC')->orderby('id', 'ASC')->get();    
            }

            return View::make('HummingbirdBase::cms.taxonomy-categories', $this->data);
        }

        return $this->forbidden(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex_call()
    {
        return $this->data['list_cats'];
    }


    public function buildTree($elements, $parentId = NULL, $level = 0) 
    {
        foreach ($elements as $key => $element)
        {
            /* Top level parent */
            if ($element->parent !== $parentId) continue;

            // $element['name'] = ($level > 0) ? '&#8627;'.$element['name']:$element['name']; //append arrow to name
            $new_name = ($level > 0) ? str_repeat('&nbsp;', ($level * 3) * 2) . '<i class="fa fa-long-arrow-right"></i> ' . $element['name']: $element['name'];
            $element['name'] = $new_name;

            // $element['name'] = ($level > 0) ? '&nbsp;'.$element['name']:$element['name']; //append arrow to name
            $this->data['list_cats'][$element->id] = $element;
            $this->data['list_cats'][$element->id]['level'] = $level;
            $this->buildTree($elements, $element->id, $level + 1);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if($this->user->userAbility('categories.create')) {
            $messages = array();

            $input = $this->Request->except('_token');
            $input['name'] = trim(strip_tags($this->Request->get('name')));
            $rules = array(
                // TODO: Refactor how we validate categories. It can be much better! See below
                // 'name' => 'required|unique:taxonomy,name,NULL,id,type,category'
                'name' => 'required'
            );

            $validator = Validator::make($input, $rules, array('required' => 'The :attribute is required.'));

            if($validator->fails())
            {
                // Log error in activity log
                return Redirect::to('hummingbird/categories')->withInput()->withErrors($validator->errors());
            }
            else 
            {
                if($input['name'] != '')
                {
                    $category = new Categories;

                    if($category->exists($input['name']))
                    {
                        if(!isset($input['ajax']))
                        {
                            return Redirect::to('hummingbird/categories')->with('error', $input['name'] . " could not be created as it already exists.");
                        }
                        else
                        {
                            //returning ajax
                            $message = (object) array('state' => false, 'message' => $input['name'] . " could not be created as it already exists.");
                            return Response::json($message, 200);
                        }
                    }

                    $category->fill($input);
                    $category->parent = (!isset($input['ajax'])) ? $input['parent']:null;
                    $category->type = $category->getType();
                    
                    if($category->save())
                    {
                        /* record activity */
                        Activitylog::log([
                            'action' => 'UPDATED',
                            'type' => get_class($category),
                            'link_id' => $category->id,
                            'description' => 'Created a new category',
                            'notes' => Auth::user()->username . " created a new category - &quot;$category->name&quot;"
                        ]);

                        if(!isset($input['ajax']))
                        {
                            return Redirect::to('hummingbird/categories')->with('success', "Category &quot;$category->name&quot; created");
                        }
                        else
                        {
                            //returning ajax
                            $message = (object) array('state' => true, 'data' => $category, 'message' => "$category->name has been created");
                            return Response::json($message, 200);
                        }
                    }
                    else
                    {
                        if(!isset($input['ajax']))
                        {
                            return Redirect::to('hummingbird/categories')->with('error', 'There was a problem saving this category.');
                        }
                        else
                        {
                            //returning ajax
                            $message = (object) array('state' => false, 'message' => "$category->name could not be created");
                            return Response::json($message, 200);
                        }
                    }
                }
                else
                {
                    if(!isset($input['ajax']))
                    {
                        return View::make('HummingbirdBase::cms.taxonomy-categories', $this->data)->with('error', 'Please enter a name for a category.');
                    }
                    else
                    {
                        //returning ajax
                        $message = (object) array('state' => false, 'message' => "Please enter a name for a category");
                        return Response::json($message, 200);
                    }   
                }
            }
        }

        return $this->forbidden(true);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        if($this->user->userAbility('categories.update')) {
            if(is_numeric($id) AND $id > 0) {
                $category = Categories::objectPermissions()->whereNull('deleted_at')->where('id', '=', $id)->first();

                if(null !== $category)
                {
                    $this->data['category'] = $category;
                    $this->data['children'] = Categories::whereNull('deleted_at')->ChildOf($category->id)->get();
                    $this->data['references'] = DB::table('taxonomy_relationships')->where('term_id', '=', $id)->get();

                    /* Breadcrumb build for all children */
                    $parent = $this->data['category']->parent;
                    $parent_breadcrumb = 'hummingbird/categories';

                    if(null !== $parent)
                    {
                        $parent__category = Categories::objectPermissions()->whereNull('deleted_at')->where('id', '=', $parent)->first();
                        $parent_category_ids = array();
                        $counter = 0;

                        /* checking for the parent tree to be collected in an array */
                        while (null !== $parent__category) {
                            $parent_category_ids[$counter][$parent__category->id] = $parent__category->name;
                            $parent__category = Categories::objectPermissions()->whereNull('deleted_at')->where('id', '=', $parent__category->parent)->first();
                            $counter++;
                        }

                        /* Reverse the order of the array so that it omes in the order of parent folder/collection */
                        $parent_category_ids = array_reverse($parent_category_ids);

                        for($counter = 0; $counter < count($parent_category_ids); $counter++) {
                            $parent = $parent_category_ids[$counter];
                            foreach($parent as $key => $value) {
                                $breadcrumb_path = 'hummingbird/categories/' . $key;

                                Breadcrumbs::for($breadcrumb_path, function ($breadcrumbs) use($value, $parent_breadcrumb, $breadcrumb_path)  {
                                    $breadcrumbs->parent($parent_breadcrumb);
                                    $breadcrumbs->push($value, url($breadcrumb_path));
                                });

                                $parent_breadcrumb = 'hummingbird/categories/' . $key;
                            }
                        }
                    }

                    Breadcrumbs::for("hummingbird/categories/".$this->data['category']->id, function ($breadcrumbs) use($parent_breadcrumb)  {
                        $breadcrumbs->parent($parent_breadcrumb);
                        $breadcrumbs->push('Edit: ' . $this->data['category']->name, url('/hummingbird/categories/' . $this->data['category']->id));
                    }); 

                    return View::make('HummingbirdBase::cms.taxonomy-categories-edit', $this->data);
                }

                return Redirect::to( 'hummingbird/categories' )->with('error', 'This category no longer exists.');
            }

            return Redirect::to( 'hummingbird/categories' )->with('error', 'Please select a category to view.');
        }

        return $this->forbidden(true);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if($this->user->userAbility('categories.update')) {
            if(is_numeric($id) AND $id > 0)
            {
                $input = $this->Request->except('_token');
                $rules = array(
                    'name' => "required|unique:taxonomy,name,{$id},id,type,category"
                );

                $validator = Validator::make($input, $rules, array('required' => 'The :attribute is required.'));

                if($validator->fails()) {
                    return Redirect::route('hummingbird.categories.show', $id)
                        ->withInput()
                        ->with('error', 'There was a problem saving your request. Please see review your inputs.')
                        ->withErrors($validator->errors());
                }

                $cat = Categories::objectPermissions()->find($id);

                if(null !== $cat)
                {
                    $old_cat_slug = $cat->slug;

                    $cat->fill($input);
                    // if($old_cat_slug != $input['slug'] AND $input['slug'] != '') $cat->slug = $input['slug'];
                    $cat->image = (isset($input['featured_image']) AND $input['featured_image'] != '' AND file_exists(base_path() . $input['featured_image'])) ? $input['featured_image']:'';

                    if($cat->save())
                    {
                        /* record activity */
                        Activitylog::log([
                            'action' => 'UPDATED',
                            'type' => get_class($cat),
                            'link_id' => $cat->id,
                            'description' => 'Updated category',
                            'notes' => Auth::user()->username . " updated the cateogry - &quot;$cat->name&quot;"
                        ]);
                        return Redirect::to('hummingbird/categories/'.$cat->id);
                    }

                    return Redirect::to('hummingbird/categories')->with('error', 'There was a problem updating this category.');
                }

                return Redirect::to('hummingbird/categories')->with('error', 'This category could not be found.');
            }

            return Redirect::to('hummingbird/categories')->with('error', 'There was a problem finding this category. Please try again.');
        }

        return $this->forbidden(true);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if( $this->user->userAbility('categories.delete') ) {
            try {
                $cat = Categories::objectPermissions()->findOrFail($id);
                $cat->delete();
                
                Activitylog::log([
                    'action' => 'DELETED',
                    'type' => get_class($cat),
                    'link_id' => $cat->id,
                    'description' => "Category has been deleted",
                    'notes' => Auth::user()->username . " deleted the category &quot;{$cat->name}&quot;"
                ]);

                return Redirect::route('hummingbird.categories.index')->with('success', 'Category has been deleted.');
            }
            catch(\Exception $e) {
                return Redirect::route('hummingbird.categories.index')->with('error', 'Category could not be found. Please try again.');
            }
        }

        return $this->forbidden(true);
    }

    /**
     * Show the soft deleted items
     *
     * @param  int  $id
     * @return Response
     */
    public function showDeleted()
    {
        if($this->user->userAbility('categories.read')) {
            $this->data['categories'] = Categories::deletedCategories();

            Breadcrumbs::for('hummingbird/categories/deleted', function ($breadcrumbs)  {
                $breadcrumbs->parent('hummingbird/categories');
                $breadcrumbs->push('Trash', route('hummingbird.categories.deleted'));
            });

            return View::make('HummingbirdBase::cms.taxonomy-categories-deleted', $this->data);
        }

        return $this->forbidden(true);      
    }

    /**
     * We are going to remove all deleted items (and relationships)
     */
    public function purge_all($id = null)
    {   
        if($this->user->userAbility('categories.delete')) {
            try {
                $Categories = $this->Category->getAllDeletedCategories();

                foreach( $Categories as $Category ) {
                    /* record activity */
                    Activitylog::log([
                        'action' => 'PURGED',
                        'type' => get_class(new Categories),
                        'link_id' => $Category->id,
                        'description' => 'Permanently removed category',
                        'notes' => Auth::user()->username . " permanently deleted &quot;$Category->name&quot;"
                    ]);

                    /* Update the children categories */
                    Categories::where('parent', '=', $Category->id)->update(array('parent' => $Category->parent));
                        
                    /* Force remove the category */
                    $Category->forceDelete();
                }

                return Redirect::route('hummingbird.categories.index')->with('success', "{$Categories->count()} categories and their relationships have been purged.");
            }
            catch(\Exception $e) {
                if( Config::get('app.debug') ) {
                    Log::error($e->getMessage());
                }
            }

            return Redirect::to('hummingbird/categories/deleted')->with('error', "Unable to purge deleted categories. Please try again.");
        }

        return $this->forbidden(true);
    }


    /**
     * We are going to purge this item (and relations)
     *
     * @param  Integer  $category_id
     * @return Response
     */
    public function purge($category_id = NULL) {
        if($this->user->userAbility('categories.delete')) {
            try {
                $Category = $this->Category->getDeletedCategory( $category_id );

                /* record activity */
                Activitylog::log([
                    'action' => 'PURGED',
                    'type' => get_class(new Categories),
                    'link_id' => $Category->id,
                    'description' => 'Permanently removed category',
                    'notes' => Auth::user()->username . " permanently deleted &quot;$Category->name&quot;"
                ]);

                /* Update the children categories */
                Categories::where('parent', '=', $Category->id)->update(array('parent' => $Category->parent));
                    
                /* Force remove the category */
                $Category->forceDelete();

                return Redirect::to('hummingbird/categories/deleted')->with('success', "{$Category->name} has been removed and relationships have been purged.");
            }
            catch(\Exception $e) {
                if( Config::get('app.debug') ) {
                    Log::error($e->getMessage());
                }
            }

            return Redirect::to('hummingbird/categories/deleted')->with('error', "Unable to restore deleted category. Please try again.");
        }

        return $this->forbidden(true);
    }

    /**
     * Reinstate all the deleted categories
     *
     * @return Response
     */
    public function reinstate_all() {
        try {
            $Categories = $this->Category->getAllDeletedCategories();

            foreach( $Categories as $Category ) {
                $Category->restore();

                Activitylog::log([
                    'action'    => 'UPDATED',
                    'type'      => Categories::class,
                    'link_id'   => $Category->id,
                    'description' => 'Restored category',
                    'notes'     => Auth::user()->username . " restored the category &quot;$Category->name&quot;"
                ]);
            }

            return Redirect::route('hummingbird.categories.index')->with('success', "{$Categories->count()} categories restored");
        }
        catch(\Exception $e) {
            if( Config::get('app.debug') ) {
                Log::error($e->getMessage());
            }
        }

        return Redirect::to('hummingbird/categories/deleted')->with('error', "Unable to restore deleted categories. Please try again.");
    }


    /**
     * Reinstate the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function reinstate($category_id = NULL) {
        try {
            $Category = $this->Category->getDeletedCategory( $category_id );
            $Category->restore();

            Activitylog::log([
                'action'    => 'UPDATED',
                'type'      => Categories::class,
                'link_id'   => $Category->id,
                'description' => 'Restored category',
                'notes'     => Auth::user()->username . " restored the category &quot;$Category->name&quot;"
            ]);

            return Redirect::route('hummingbird.categories.index')->with('success', "{$Category->name} has been restored.");
        }
        catch(\Exception $e) {
            if( Config::get('app.debug') ) {
                Log::error($e->getMessage());
            }
        }

        return Redirect::to('hummingbird/categories/deleted')->with('error', "Unable to restore deleted category. Please try again.");
    }




    public function import()
    {
        $import_files = array('download_categories', 'event_cats', 'gallerycategories', 'shop_cats');

        foreach($import_files as $import_file)
        {
            DB::unprepared(file_get_contents(base_path() . '/import/sql/' . $import_file . '.sql'));

            $data = DB::table($import_file)->get();

            /* data to insert */
            $insert_data = array();
        }
    }


    public function getChildren()
    {
        $data = array();

        if(null !== $this->Request->get('category'))
        {
            $category = Categories::objectPermissions()->find($this->Request->get("category"));

            if(null !== $category)
            {
                $children = Categories::objectPermissions()->whereNull('deleted_at')->type()->childOf($category->id)->orderBy('name', 'ASC')->get();

                if(count($children) > 0)
                {
                    $data['state'] = true;
                    foreach($children as $cat)
                    {
                        $data['categories'][$cat->id] = $cat->name;
                    } 
                }
                else
                {
                    $data['state'] = false;
                    $data['message'] = 'no category found';                 
                }
            }
            else
            {
                $data['state'] = false;
                $data['message'] = 'no category found';
            }
        }
        else
        {
            $data['message'] = 'no input';
            $data['state'] = false;
        }

        return json_encode($data);
    }



    /**
     * Display a page with all categories so that they can be easily moved around (positions and parents)
     * 
     */
    public function get_update_order()
    {

        Breadcrumbs::for('hummingbird/categories/re-order', function ($breadcrumbs)  {
            $breadcrumbs->parent('hummingbird/categories');
            $breadcrumbs->push('Re-order', route('hummingbird.categories.reorder'));
        });

        return View::make('HummingbirdBase::cms.taxonomy-categories-reorder', $this->data);
    }



    /**
     * Process the updating of category positions
     * 
     */
    public function update_order()
    {
        if($this->Request->filled('SortOrder')) {
            $this->update_categories(json_decode($this->Request->get('SortOrder')));
        }

        return Redirect::route('hummingbird.categories.reorder')->with('success', 'Category orders updated');
    }

    /**
     * Recursive iteration for updating categories and category children
     *
     * @param Mixed     $SortedCategories   [List of categories (inc Children if provided)]
     * @param Integer   $counter            [The position index]
     * @param Integer   $parent_category_id [Whether we are looking at a parent category]
     */
    public function update_categories($SortedCategories = [], $counter = 1, $parent_category_id = NULL)
    {
        if(count($SortedCategories) > 0)
        {
            foreach($SortedCategories as $SortedCategory)
            {
                $category               = Categories::objectPermissions()->find($SortedCategory->id);
                $category->SortOrder    = $counter;
                $category->parent       = $parent_category_id;
                $category->save();
                $counter++;

                if(isset($SortedCategory->children) && count($SortedCategory->children) > 0)
                {
                    $this->update_categories($SortedCategory->children, $counter, $category->id);
                }
            }
        }
    }
}
