<?php namespace Hummingbird\Controllers;

use App;
use ArrayHelper;
use Auth;
use Config;
use DB;
use General;
use Log;
use Redirect;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Tags;
use Hummingbird\Repositories\TaxonomyTagRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\MessageBag;

class TagsController extends CmsbaseController
{
	public function __construct(Request $Request, TaxonomyTagRepository $TaxonomyTagRepository, MessageBag $MessageBag)
	{
		parent::__construct($Request);

        $this->MessageBag = $MessageBag;
		$this->Tag = $TaxonomyTagRepository;

		if( !Breadcrumbs::exists('hummingbird/tags') ):
			Breadcrumbs::for('hummingbird/tags', function ($breadcrumbs) { 
	            $breadcrumbs->parent('HummingbirdBreadcrumbs');
	            $breadcrumbs->push('Tags', url('hummingbird/tags'));
	        });
		endif;

		$this->checkDeletedTags(base_path().'/delete-tags.csv');
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if($this->user->userAbility('tags.read')) {
			$this->data['tags'] = Tags::type()->orderBy('name', 'ASC')->get();
			$this->data['alphabetical'] = $this->generateAlphabetical();
			$this->data['deleted_tags'] = $this->Tag->getAllDeletedTags()->count();

	        return View::make('HummingbirdBase::cms/taxonomy-tags', $this->data);
		}

		return $this->forbidden(true);
	}

	/**
	 * Return an array of A-Z listing of items
	 *
	 * @return Data (array)
	 */
	public function generateAlphabetical()
	{
		$listing = array();
		$other = array();

		if(count($this->data['tags']) > 0)
		{
			foreach($this->data['tags'] as $tag)
			{
				$first_letter = ucfirst( strtolower( $tag->name[0] ) );

				switch($first_letter)
				{
					case ctype_alpha($first_letter):
						// is alphabetical
						$listing[ $first_letter ][] = $tag;

						break;
					case is_numeric($first_letter):
						// is numeric
						$other['0-9'][] = $tag;

						break;
					default:
						// something else
						$other['Other'][] = $tag;

						break;
				}
			}
		}

		if(isset($other['0-9']))
		{
			$listing['0-9'] = $other['0-9'];
		}

		if(isset($other['Other']))
		{
			$listing['Other'] = $other['Other'];
		}

		return $listing;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if( $this->user->userAbility('tags.create') ) {
			$this->mb = new \Illuminate\Support\MessageBag();
			
			$added = [];

			$this->Request->merge(array_map('trim', $this->Request->all()));

			$Validator = Validator::make( $this->Request->all(), ['tags' => 'required|min:1'], ['tags.required' => 'Please enter at least one term to be stored as a tag', 'tags.min' => 'Please enter the tags you wish to create']);

			if( $Validator->fails() ):
				return Redirect::to( 'hummingbird/tags' )->withInput()->withErrors($Validator)->with('error', 'Please enter at least one term to be stored as a tag.');
			endif;

			$tags = ArrayHelper::cleanExplodedArray(explode(",", $this->Request->get('tags')));
			$cleaned_tags = $tags;

			if( count($tags) > 0 ) {
				foreach($tags as $tag_index => $tag_name) {
					if( !(new Tags)->exists($tag_name) ) {
						$tag = new Tags;
						$tag->name = $tag_name;

						if( $tag->save() ):
							$added[] = $tag_name;

							// we've saved this, so remove it so we can tell the user
							unset($cleaned_tags[ $tag_index ]);
						endif;
					}
				}

				$this->Request->flush(); //clean the request as we want to use the Input later on

				if( count($added) > 0 ) {
			        /* record activity */
			        Activitylog::log([
			            'action' => 'CREATED',
			            'type' => get_class(new Tags),
			            'link_id' => null,
			            'description' => ( count($added) == 1 ) ? 'Created taxonomy term' : 'Created multiple taxonomy terms',
			            'notes' => ( count($added) == 1 ) ? Auth::user()->username . " created a new tag - &quot;$tag->name&quot;" : Auth::user()->username . " created the following tags (" . implode(", ", $added) . ")"
			        ]);

			        if( count($cleaned_tags) > 0 ):
			        	$this->Request->merge(['tags' => implode(', ', $cleaned_tags) ]);
			        endif;

					return Redirect::to( 'hummingbird/tags' )->with('success', count($added) . " " . General::singular_or_plural(count($added), 'Tag') . " created")
						->with('error', ( count($cleaned_tags) > 0 ) ? 'Some tags could not be created. Please check your inputs' : NULL)
						->withInput();
				}
			}

			$this->mb = new \Illuminate\Support\MessageBag();
			$this->mb->add("tags", "No tags created as they are invalid or already exist");

			return Redirect::to( 'hummingbird/tags' )
				->with('error', 'There was a problem processing your tags.')
				->withInput()
				->withErrors($this->mb ?: NULL);
		}

		return $this->forbidden(true);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if($this->user->userAbility('tags.update')) {
			if(is_numeric($id) AND $id > 0)
			{
				$tag = Tags::whereNull('deleted_at')->where('id', '=', $id)->first();

				if(null !== $tag)
				{
					$this->data['tag'] = $tag;
					$this->data['references'] = DB::table('taxonomy_relationships')->where('term_id', '=', $id)->get();

					Breadcrumbs::for("hummingbird/tags/".$this->data['tag']->id, function ($breadcrumbs)  {
	                    $breadcrumbs->parent('hummingbird/tags');
	                    $breadcrumbs->push('Edit: ' . $this->data['tag']->name, url('/hummingbird/tags/' . $this->data['tag']->id));
	                });

					return View::make('HummingbirdBase::cms/taxonomy-tags-edit', $this->data);
				}
				
				return Redirect::to("hummingbird/tags")->with('error', 'Tag could not be found. Please try again.');
			}

			return Redirect::to('hummingbird/tags')->with('error', 'Please select a valid tag to edit.');
		}

		return $this->forbidden(true);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if($this->user->userAbility('tags.update')) {
			try {
				$tag = Tags::findOrFail( $id );
				$input = $this->Request->except('_token');

				$validator = Validator::make( $this->Request->all(), [
					'name' => 'required|min:2'
				], [
					'name.required' => 'Please enter the name for your tag',
					'name.min' => 'The name attribute should be greater than two characters'
				] );

                if( $validator->fails() ) {
                    return Redirect::route('hummingbird.tags.show', $id)
                        ->withInput()
                        ->with('error', 'There was a problem saving your request. Please see review your inputs.')
                        ->withErrors($validator->errors());
                }

				$old_tag_slug = $tag->slug;
				
				$tag->fill($input);
				if($old_tag_slug != $input['slug'] AND $input['slug'] != '') $tag->slug = $input['slug'];

				if( $tag->save() ) {
			        Activitylog::log([
			            'action' => 'UPDATED',
			            'type' => get_class($tag),
			            'link_id' => $tag->id,
			            'description' => "Tag has been updated",
			            'notes' => Auth::user()->username . " has updated the tag &quot;$tag->name&quot;"
			        ]);

					return Redirect::route('hummingbird.tags.show', $tag->id)->with('success', "Tag &quot;{$tag->name}&quot; has been updated.");
				}

                return Redirect::route('hummingbird.tags.index')->with('errors', 'Tag could not be found');
			}
			catch(\Exception $e) {
				return Redirect::route('hummingbird.tags.index')->with('errors', 'Tag could not be found');
			}

			return Redirect::route('hummingbird.tags.show', $tag->id)->with('errors', 'There was a problem updating this tag.');
		}

		return $this->forbidden(true);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if($this->user->userAbility('tags.delete')) {
			if(is_numeric($id) AND $id > 0)
			{
				$tag = Tags::find($id);
				$tag_name = $tag->name;

				if(null !== $tag)
				{
					if($tag->delete())
					{
						// successfully deleted tag - add activity
			        	/* record activity */
				        Activitylog::log([
				            'action' => 'DELETED',
				            'type' => get_class(new Tags),
				            'link_id' => $tag->id,
				            'description' => "Tag has been deleted",
				            'notes' => Auth::user()->username . " deleted the tag &quot;$tag->name&quot;"
				        ]);

				        return Redirect::to('hummingbird/tags')->with('success', 'Tag has been deleted.');
					}

                    return Redirect::to('hummingbird/tags')->with('error', 'Tag could not be deleted.');
				}

                return Redirect::to('hummingbird/tags')->with('error', 'Tag could not be found. Please try again.');
			}

			return Redirect::to('hummingbird/tags')->with('error', 'Please select a tag to delete.');
		}

		return $this->forbidden(true);
	}


	/**
	 * Show the soft deleted items
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showDeleted()
	{
		$this->data['tags'] = Tags::type()->onlyTrashed()->orderBy('name', 'ASC')->get();
		$this->data['alphabetical'] = $this->generateAlphabetical();

		Breadcrumbs::for("hummingbird/tags/deleted", function ($breadcrumbs)  {
            $breadcrumbs->parent('hummingbird/tags');
            $breadcrumbs->push('Trash', url('/hummingbird/tags/deleted'));
        });

        return View::make('HummingbirdBase::cms/taxonomy-tags-deleted', $this->data);
	}

    /**
     * We are going to remove all deleted items (and relationships)
     */
	public function purge_all()
	{
		if($this->user->userAbility('tags.delete')) {
	        try {
	            $Tags = $this->Tag->getAllDeletedTagsOrFail();

	            foreach( $Tags as $Tag ) {
                  	/* record activity */
	                Activitylog::log([
	                    'action' => 'PURGED',
	                    'type' => get_class(new Tags),
	                    'link_id' => $Tag->id,
	                    'description' => 'Permanently removed tag',
	                    'notes' => Auth::user()->username . " permanently deleted &quot;$Tag->name&quot;"
	                ]);

	                $Tag->forceDelete();
	            }

	            return Redirect::route('hummingbird.tags.index')->with('success', General::singular_or_plural($Tags->count(), "{$Tags->count()} tag") . " and their relationships have been purged.");
	        }
	        catch(\Exception $e) {
	        	if( Config::get('app.debug') ) {
	        		Log::error($e->getMessage());
	        	}
	        }

	        return Redirect::to('hummingbird/tags/deleted')->with('error', "Unable to purge deleted tags. Please try again.");
        }

        return $this->forbidden(true);
	}


    /**
     * We are going to purge this item (and relations)
     *
     * @param  Integer  $category_id
     * @return Response
     */
	public function purge($tag_id = null)
	{
		if($this->user->userAbility('tags.delete')) {
	        try {
	            $Tag = $this->Tag->getDeletedTag( $tag_id );

              	/* record activity */
                Activitylog::log([
                    'action' => 'PURGED',
                    'type' => get_class(new Tags),
                    'link_id' => $Tag->id,
                    'description' => 'Permanently removed tag',
                    'notes' => Auth::user()->username . " permanently deleted &quot;$Tag->name&quot;"
                ]);

                $Tag->forceDelete();

	            return Redirect::to('hummingbird/tags/deleted')->with('success', "{$Tag->name} has been removed and relationships have been purged.");
	        }
	        catch(\Exception $e) {
	        	if( Config::get('app.debug') ) {
	        	    dd($e->getLine(), $e->getFile(), $e->getMessage());
	        	}

	        	Log::error($e->getMessage());
	        }

	        return Redirect::to('hummingbird/tags/deleted')->with('error', "Unable to purge deleted tag. Please try again.");
	    }

		return $this->forbidden(true);
	}

    /**
     * Reinstate all the deleted tags
     *
     * @return Response
     */
	public function reinstate_all()
	{
        try {
            $Tags = $this->Tag->getAllDeletedTagsOrFail();

            foreach( $Tags as $Tag ) {
                $Tag->restore();

                Activitylog::log([
                    'action'    => 'UPDATED',
                    'type'      => Tags::class,
                    'link_id'   => $Tag->id,
                    'description' => 'Restored category',
                    'notes'     => Auth::user()->username . " restored the category &quot;$Tag->name&quot;"
                ]);
            }

            return Redirect::route('hummingbird.tags.index')->with('success', General::singular_or_plural($Tags->count(), "{$Tags->count()} tag") . " restored");
        }
        catch(\Exception $e) {
        	if( Config::get('app.debug') ) {
                dd($e->getLine(), $e->getFile(), $e->getMessage());
        	}
        	Log::error( $e->getMessage() );
        }

        return Redirect::to('hummingbird/tags/deleted')->with('error', "Unable to restore deleted tags. Please try again.");
	}


    /**
     * Reinstate the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
	public function reinstate($tag_id)
	{
        try {
            $Tag = $this->Tag->getDeletedTag( $tag_id );
            $Tag->restore();

            Activitylog::log([
                'action'    => 'UPDATED',
                'type'      => Tags::class,
                'link_id'   => $Tag->id,
                'description' => 'Restored tag',
                'notes'     => Auth::user()->username . " restored the tag &quot;$Tag->name&quot;"
            ]);

            return Redirect::route('hummingbird.tags.index')->with('success', "{$Tag->name} has been restored.");
        }
        catch(\Exception $e) {
        	if( Config::get('app.debug') ) {
        		dd($e->getLine(), $e->getFile(), $e->getMessage());
        	}

            Log::error($e->getMessage());
        }

        return Redirect::to('hummingbird/tags/deleted')->with('error', "Unable to restore deleted tag. Please try again.");
	}

	public function checkDeletedTags($filename = '', $delimiter = ",")
	{
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;
     
        $header = NULL;
        $data = array();

        ini_set('auto_detect_line_endings', true);
		if (($handle = fopen($filename, "r")) !== FALSE) {
		    while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {

                if(!$header)
                    $header = $row;
                else
                    $data[] = $row[0];
            }

            fclose($handle);
		}

        if(count($data) > 0)
        {
        	DB::table('taxonomy')
        		->whereNotIn('name', $data)
        		->where('type', '=', 'tag')
        		->delete();
        }
    }
}
