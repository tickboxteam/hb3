<?php namespace Hummingbird\Controllers;

use Auth;
use DB;
use File;
use General;
use Image;
use Redirect;
use Response;
use Route;
use Session;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Libraries\FileHelper;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Media;
use Hummingbird\Models\MediaCollection;
use Hummingbird\Models\Setting;
use Illuminate\Http\Request;

class MediaController extends CmsbaseController
{

	public function __construct(Request $Request)
	{
		parent::__construct($Request);

		$this->activateUserDefaults = false;
		$this->data['collections'] = array();
        $this->data['import_media'] = false;

        /* Get all the collections - ordered by parent_collection (Null first, n+1 last) */
        $this->data['Collections'] = MediaCollection::topLevelOnly()->orderBy('name', 'ASC')->get();

        if( !Breadcrumbs::exists('hummingbird/media') ):
	        Breadcrumbs::for('hummingbird/media', function ($breadcrumbs) { 
	            $breadcrumbs->parent('HummingbirdBreadcrumbs');
	            $breadcrumbs->push('Media centre', url('hummingbird/media'));
	        });
	    endif;

        $this->image_sizes = Setting::where('key', '=', 'media')->first();

        $this->image_sizes = (!isset($this->image_sizes->id) OR null === $this->image_sizes->id) ? false:$this->image_sizes->value;

        if(!isset($this->image_sizes['sizes']) OR count($this->image_sizes['sizes']) == 0)
        {
        	$this->image_sizes['sizes'] = array(
	        	0 => array (
	        		'name' => 'File Manager Thumbnail',
	        		'type' => 'Default',
	        		'description' => 'File Manager Thumbnails',
	        		'width' => 300
	        	)
	        );

	        (new Setting)->newSetting('media', $this->image_sizes);
        }

        if( Route::currentRouteName() == 'hummingbird.media.view' ) {
        	$this->activateSearch();
        }

        $this->init();
	}

	public function init()
	{
		$upload_path = storage_path() . "/app/public/uploads";

		/* check uploads folder exists */
		if(!File::exists($upload_path))
		{
			File::makeDirectory($upload_path, 0775); // create it if not
		}

		foreach(['images', 'documents', 'audio', 'video', 'other'] as $folder)
		{
			// check upload directory exists
			if(!File::exists($upload_path . '/' . $folder))
			{
				File::makeDirectory($upload_path . '/' . $folder, 0775); // create it if not
			}
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = null)
	{
		if($this->user->userAbility('media.read')) {
			$return = $this->checkDefaultMedia(Auth::user()->preferences, $id);

			if(null !== $return) return $return;

			$this->data['current_collection'] = (!$id) ? '':$id;
			$this->data['parent_id'] = (!$id) ? '':$id;

			if(!$id)
			{
				$this->data['media_collections'] = DB::table('media-collections')->whereNull('parent_collection')->orderBy('name')->get();
				$this->data['media_items'] = Media::search( $this->Request->get('s') )->inCollection(false)->parent(true)->get(); //just get all files		
			}
			else
			{
				$this->data['media_collections'] = DB::table('media-collections')->where('parent_collection', '=', $id)->orderBy('name')->get();
				$this->data['media_items'] = Media::search( $this->Request->get('s') )->inCollection($id)->parent(true)->get(); //just get all files	

				/* Build breadcrumb */
				$this->data['collection'] = DB::table('media-collections')->where('id', '=', $id)->first();
				$parent_collection = $this->data['collection']->parent_collection;

				$parent__collection_breadcrumb_path = 'hummingbird/media';

				if(null !== $parent_collection)
				{
					$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->first();
					$parent_collection_ids = array();
					$counter = 0;

					/* checking for the parent tree to be collected in an array */
					while (null !== $parent__collection) {
						$parent_collection_ids[$counter][$parent__collection->id] = $parent__collection->name;
						$parent__collection = DB::table('media-collections')->where('id', '=', $parent__collection->parent_collection)->first();
						$counter++;
					}

					/* Reverse the order of the array so that it omes in the order of parent folder/collection */
					$parent_collection_ids = array_reverse($parent_collection_ids);

					for($counter = 0; $counter < count($parent_collection_ids); $counter++) {
						$parent = $parent_collection_ids[$counter];
						foreach($parent as $key => $value) {
							$breadcrumb_path = 'hummingbird/media/view/' . $key;

							Breadcrumbs::for($breadcrumb_path, function ($breadcrumbs) use($value, $parent__collection_breadcrumb_path, $breadcrumb_path)  {
			                    $breadcrumbs->parent($parent__collection_breadcrumb_path);
			                    $breadcrumbs->push($value, url($breadcrumb_path));
			                });

			                $parent__collection_breadcrumb_path = 'hummingbird/media/view/' . $key;
						}
					}
				}

				Breadcrumbs::for("hummingbird/media/view/".$this->data['collection']->id, function ($breadcrumbs) use($parent__collection_breadcrumb_path) {
                    $breadcrumbs->parent($parent__collection_breadcrumb_path);
                    $breadcrumbs->push($this->data['collection']->name, url('hummingbird.media.view' . $this->data['collection']->id));
                });
				
			}

			// function to check the import folder exists
			$this->getImportFiles();

			return View::make('HummingbirdBase::cms.media', $this->data);	
		}

		return $this->forbidden(true);
	}

	// function to check the folder "import" exist
	public function getImportFiles()
	{
		// if(File::exists(base_path() . '/import')) {
		//     $files = File::allFiles(base_path() . '/import');

		// 	if(count($files) > 0)
		// 	{
		// 		$this->data['import_media'] = true;
		// 		$this->data['files'] = $files;
		// 	}
		// }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		dd("create");
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if($this->user->userAbility('media.create')) {
			/*
			* Check name has been posted
			* Check name does not exist
			* Check parent of collection to make sure
			* Insert new media collection name
			* 	Does the collection need to be stored in a parent?
			*/

			$input = $this->Request->except('_token', 'add');

			$validator = Validator::make($input, ['collection-name' => 'required|max:255'], array('collection-name.required' => 'The :attribute is required.', 'collection-name.max' => 'Collection name should not be any larger than 255 characters'));

			if($validator->fails())
			{
				// Log error in activity log
				return Redirect::route('hummingbird.media.index')->with('MediaCollectionError', 'There was a problem creating your collection.')->withInput()->withErrors($validator);
			}

			// add to collections
			// set parent (if required);
			if ($this->Request->filled('collection-name'))
			{
				$parent_collection = ($this->Request->filled('parent_collection')) ? $this->Request->get('parent_collection'):NULL;

				/* Query scope here - http://laravel.com/docs/4.2/eloquent#query-scopes */
				$collection = ( empty($parent_collection) ) ? DB::table('media-collections')->where('name', $this->Request->get('collection-name'))->first() : DB::table('media-collections')->where('name', $this->Request->get('collection-name'))->where('parent_collection', $parent_collection)->first();

				if(empty($collection))
				{
					$data = array('name' => $this->Request->get('collection-name')); //data to insert
					$data['parent_collection'] = $parent_collection; //set parent

					$data['permalink'] = str_slug($data['name']);
					$set_permalink = false;

					$counter = 1;
					while(!$set_permalink)
					{
						$perm_check = DB::table('media-collections')->where('permalink', '=', $data['permalink'])->first();

						/* We have this URL */
						if(!empty($perm_check))
						{
							$data['permalink'] = str_slug($data['name']) . $counter;
							$counter++;
						}
						else
						{
							/* No url found - break */
							$set_permalink = true;
						}
					}

					$collection_id = DB::table('media-collections')->insertGetId($data);

					if(is_numeric($collection_id) AND $collection_id > 0)
					{
				        Activitylog::log([
				            'action' => 'CREATED',
				            'type' => get_class(new Media),
				            'link_id' => $collection_id,
				            'url' => route('hummingbird.media.view', $collection_id),
				            'description' => 'Created collection',
				            'notes' => Auth::user()->username . " created a new collection &quot;".$data['name']."&quot;"
				        ]);
					}
				}
			}

			if(isset($data['parent_collection']) AND null !== $data['parent_collection']) {
				return Redirect::route('hummingbird.media.view', $data['parent_collection']);
			}

			return Redirect::route('hummingbird.media.index');
		}

		return $this->forbidden(true);
	}

	/**
	 * 
	 *
	 * @return Response
	 */
	public function postEditMediaItem($id)
	{
		$FileHelper = new FileHelper;
		$input = $this->Request->except('_token', 'add');

		$action = (null !== $input['action']) ? $input['action']:'update';

		/* needs validation */
		$media = Media::find($id);
		$media->fill($input);

		if(!$FileHelper->isImage(File::extension(storage_path() . "/app/public" . $media->location)))
		{
			$media->alt = '';
			$media->caption = '';
		}

		$media->save();

        /* Store cats and tags */
        $input['taxonomy'] = (isset($input['taxonomy'])) ? $input['taxonomy']:array();
        $media->storeTaxonomy($input['taxonomy']);

        /* lets roll all changes to children - only available to parents */
		if($action == 'update-all')
		{
			if($media->parent === NULL)
			{
				/* get all children - excluding self */
				$media_children = Media::where('parent', '=', $id)->where('id', '!=', $id)->get();
			}
			else
			{
				/* All versions - excluding self */
				$media_children = Media::orWhere(function($query) use ($media)
					{
						$query->where('id', '=', $media->parent)
							->orWhere('parent', '=', $media->parent);
					})	
					->where('id', '!=', $id)->get();
			}

			foreach($media_children as $child)
			{
				$child->fill($input);
				$child->save();
				$child->storeTaxonomy($input['taxonomy'], $child, get_class($child));
			}
		}

		/* If successful, return */
		return Redirect::route('hummingbird.media.view-media', $media->id)->with('success', 'Media item has been updated.');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(null !== $id AND is_numeric($id))
		{
			$this->data['media_items'] = Media::get(); //just get all files

			return View::make('HummingbirdBase::cms.media', $this->data);
		}

		return Redirect::route('hummingbird.media.index')->with('errors', 'That collection is not available. Please try again.');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if($this->user->userAbility('media.update')) {
			$this->data['import_media'] = false;
			$this->data['collection'] = DB::table('media-collections')->find($id);

			/* Build breadcrumb */
			$this->data['collection'] = DB::table('media-collections')->where('id', '=', $id)->first();
			$parent_collection = $this->data['collection']->parent_collection;
			$parent__collection_breadcrumb_path = 'hummingbird/media';

			if(null !== $parent_collection)
			{
				$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->first();
				$parent_collection_ids = array();
				$counter = 0;

				/* checking for the parent tree to be collected in an array */
				while (null !== $parent__collection) {
					$parent_collection_ids[$counter][$parent__collection->id] = $parent__collection->name;
					$parent__collection = DB::table('media-collections')->where('id', '=', $parent__collection->parent_collection)->first();
					$counter++;
				}

				/* Reverse the order of the array so that it omes in the order of parent folder/collection */
				$parent_collection_ids = array_reverse($parent_collection_ids);

				for($counter = 0; $counter < count($parent_collection_ids); $counter++) {
					$parent = $parent_collection_ids[$counter];
					foreach($parent as $key => $value) {
						$breadcrumb_path = 'hummingbird/media/view/' . $key;

						Breadcrumbs::for($breadcrumb_path, function ($breadcrumbs) use($value, $parent__collection_breadcrumb_path, $breadcrumb_path)  {
		                    $breadcrumbs->parent($parent__collection_breadcrumb_path);
		                    $breadcrumbs->push($value, url($breadcrumb_path));
		                });

		                $parent__collection_breadcrumb_path = 'hummingbird/media/view/' . $key;
					}
				}
			}

			Breadcrumbs::for("hummingbird/media/".$this->data['collection']->id.'/edit', function ($breadcrumbs) use($parent__collection_breadcrumb_path) {
                $breadcrumbs->parent($parent__collection_breadcrumb_path);
                $breadcrumbs->push($this->data['collection']->name, url('hummingbird/media/'.$this->data['collection']->id.'/edit'));
            });

			return View::make('HummingbirdBase::cms.media-collection', $this->data);
		}

		return $this->forbidden(true);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if($this->user->userAbility('media.update')) {
			try {
				$MediaCollection = MediaCollection::findOrFail( $id );
				$MediaCollection->fill( $this->Request->all('name', 'description', 'parent_collection') );
				$MediaCollection->save();

		        Activitylog::log([
		            'action' => 'UPDATED',
		            'type' => get_class(new MediaCollection),
		            'link_id' => $id,
		            'url' => route('hummingbird.media.edit', $MediaCollection->id),
		            'description' => 'Updated details for a collection',
		            'notes' => Auth::user()->username . " updated the details for &quot;$MediaCollection->name&quot;"
		        ]);

				return Redirect::route('hummingbird.media.edit', $MediaCollection->id)->with('success', 'Collection details have been updated.');
			}
			catch(\Exception $e) {
			}

			return Redirect::route('hummingbird.media.edit', $id)->with('error', 'Unable to update collection.');
		}

		return $this->forbidden(true);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  	Mixed  $id
	 * @return 	Response
	 * @throws 	\Illuminate\Database\Eloquent\ModelNotFoundException
	 *
	 * @since 	1.1.37
	 */
	public function destroy($id = null) {
		try {
			$MediaCollection 	= MediaCollection::findOrFail($id);
			$MediaCollectionName = $MediaCollection->name;
			$ReturnUrl 			= ( !empty($MediaCollection->parent_collection) ) ? route('hummingbird.media.view', $MediaCollection->parent_collection) : route('hummingbird.media.index');

			$MediaCollection->delete();

			Activitylog::log([
			    'action' => 'DELETED',
			    'type' => get_class(new MediaCollection),
			    'link_id' => $id,
			    'description' => 'Collection deleted',
			    'notes' => Auth::user()->username . " has deleted the collection &quot;{$MediaCollectionName}&quot;"
			]);

			return Redirect::to( $ReturnUrl )->with('success', "&quot;{$MediaCollectionName}&quot; has been deleted along with any sub-collections and items inside them.");
		}
		catch(\Exception $e) {
			return Redirect::route('hummingbird.media.index')->with('errors', 'Unable to delete collection as it\'s no longer available.');
		}

		return Redirect::route('hummingbird.media.index')->with('errors', 'Unable to delete collections. Please try again.');
	}

	/**
	 * Destroy media resource
	 *
	 * @param  	Mixed  $id
	 * @return 	Response
	 * @throws 	\Illuminate\Database\Eloquent\ModelNotFoundException
	 *
	 * @since 	1.1.37
	 */
	public function destroyMedia($id = null) {
		try {
			$MediaItem 	= Media::where('id', $id)->locked()->firstOrFail();
			$ReturnUrl = ( $MediaItem->collection ) ? route('hummingbird.media.view', $MediaItem->collection) : route('hummingbird.media.index');
			
			$MediaItem->delete();

	        Activitylog::log([
	            'action' => 'DELETED',
	            'type' => get_class($MediaItem),
	            'link_id' => $MediaItem->id,
	            'description' => 'Media item deleted',
	            'notes' => Auth::user()->username . " has deleted &quot;{$MediaItem->filename}&quot; ({$MediaItem->location}) from the media library"
	        ]);

	        if( $this->Request->get('ajax') ) {
	        	$message = (object) array('state' => true, 'message' => "{$MediaItem->location} has been removed from the media library");
	        	return Response::json($message, 200);
	        }

	        return Redirect::to( $ReturnUrl )->with('success', "&quot;{$MediaItem->filename}&quot; has been deleted.");
		}
		catch(\Exception $e) {
			return Redirect::route('hummingbird.media.index')->with('error', 'There was a problem deleting that item. The error received was: ' . $e->getMessage());
		}

        if($this->Request->get('ajax'))
        {
        	$message = (object) array('state' => false, 'message' => "Item can not be removed. Please try again.");
        	return Response::json($message, 200);
		}

		return Redirect::route('hummingbird.media.index')->with('error', 'That item is not available. Please try again.');
	}

	/**
	 * Importing multiple files via the import facility
	 *
	 * @param  return  boolean (true|false)
	 * @return Response
	 */
	public function importFiles()
	{
		dd("importing");
	}

	/**
	 * Upload specified media item to the server and insert into the database
	 *
	 * @param  return  boolean (true|false)
	 * @return Response
	 */
	public function uploadFile($ajax = true)
	{
		if(($this->Request->get('noajax') != 'noajax' OR Request::isMethod('post')) AND $this->Request->get('ajax') == 0) $ajax = false;


		// /* Upload file */ - DONE 
		// /* check upload correct */ - DONE
		// /* check upload set */ - DONE
		/* check extension - DONE */
			/* Throw error if not correct extension - DONE */
			/* Upload item if correct extension - DONE */
		/* Maximum file size check - DONE */
			/* Validate Image size - DONE */
			/* Validate Documents size - DONE */
			/* Validate Videos size - DONE */
			/* Validate Audio size - DONE */
			/* Validate Other size - DONE */
		/* Insert into correct folder */
		/* Add file to database */
		/* Add file to collection if necessary */

		/* on install create those folders */

		/*
			Image - 8MB
			Documents - 8MB
			Videos - 50MB
			Audio - 50MB
			Other - 1MB
		*/
	
		/**
		 * TODO: A REWRITE OF THE UPLOAD FACILITY SO WE CAN RE-USE IT IN VARIOUS PLACES
		 * 
		 * CONCEPT:
		 * 		Hummingbird\Uploader\File::upload($_FILES['file']);
		 * 
		 * IDEAS:
		 * 		Will need to handle collections (and where media items sit)
		 * 		Handling multiple media uploads
		 * 		Handling image versioning
		 * 
		 * EXCEPTIONS:
		 * 		No file(s)
		 * 		Invalid file type
		 * 		Invalid file size
		 * 		Path is writeable
		 */
		if ($this->Request->hasFile('file'))
		{
			if ($this->Request->file('file')->isValid())
			{
				$input = $this->Request->except('_token', 'add');

				$item = $this->Request->file('file');
				$file_ext = strtolower($item->getClientOriginalExtension());
				$file_name = $item->getClientOriginalName();

				$FileHelper = new FileHelper;

				if($FileHelper->isValidExtension($file_ext))
				{
					if(!$FileHelper->checkMediaFileSize($item, $file_ext))
					{
			            $file_type = $FileHelper->getValidKeyByExt($file_ext);
			            $save_path = "/uploads/{$file_type}/" . date("Y-m-d") . "/";

						$filename = $FileHelper->formatFileName(str_replace($file_ext, "", $file_name)) . ".{$file_ext}";
						$__isImage = $FileHelper->isImage($file_ext);

						if (File::isWritable( storage_path('app/public') . "/uploads/{$file_type}" )) {
							$counter 		= 0;
							$filename_tmp 	= $filename;

							while( File::exists( storage_path('app/public') . $save_path . $filename_tmp ) ):
								if( $counter > 0 ) {
									$filename_tmp = str_replace(".{$file_ext}", "-{$counter}.{$file_ext}", $filename_tmp);
								}

								$counter++;
							endwhile;

							$filename = $filename_tmp;

							if( $FileHelper->uploadFile($item, $filename, storage_path('app/public') . $save_path) )
							{
								/* save item */
								$media = new Media;
								$media->title = $file_name;
								$media->filename = $file_name;
								$media->location = $save_path . $filename;
									
								if($this->Request->filled('collection'))
								{
									$media->collection = $this->Request->get('collection');
								}

								$media->save();
								$media->state = true;
								$media->is_image = $__isImage;
								$media->icon = 'fa-file-' . str_slug($file_ext);

								if($media->is_image)
								{
									/* if image */
									foreach($this->image_sizes['sizes'] as $size)
									{
										$width = (isset($size['width'])) ? $size['width']:null;
										$height = (isset($size['height'])) ? $size['height']:null;

										$media_child = (new Media)->fill((array) $media);
										$media_child->title = $file_name;
										$media_child->filename = $file_name;
										$media_child->parent = $media->id;
										$media_child->mc_only = 1;
										$media_child->locked = 1;

										$FileHelper->create_image($filename, $save_path, $media_child, $width, $height);
									}
								}

								if(!$ajax)
								{
									Session::flash('success', 'We uploaded your file.');
								}
								else
								{
									return Response::json($media, 200);
								}
							}
							else
							{
								if(!$ajax)
								{
									//non-ajax request
									Session::flash('errors', 'There was a problem uploading your file.');
								}
								else
								{
				        			return Response::json("There was a problem uploading &quot;$file_name&quot;.", 400);
								}
							}
						}
						else
						{
							return Response::json("The folder $file_type is not writeable. Please try again.", 400);
						}
					}
					else
					{
						if(!$ajax)
						{
							//non-ajax request
							Session::flash('errors', 'The file uploaded ('.$file_name.') is too big.'.$FileHelper->getErrorByKey('filesize'));
						}
						else
						{
		        			return Response::json('The file uploaded ('.$file_name.') is too big.'.$FileHelper->getErrorByKey('filesize'), 400);
						}	
					}
				}
				else
				{
					$files = implode(", ", $FileHelper->getAllExtensions());

					if(!$ajax)
					{
						//non-ajax request
						Session::flash('errors', 'Please upload a valid file. Valid file types include: '.$files);
					}
					else
					{
	        			return Response::json('Please upload a valid file. Valid file types include: '.$files, 400);
					}		
				}
			}
			else
			{
				if(!$ajax)
				{
					//non-ajax request
					Session::flash('errors', 'We could not upload your file. Please try again.');
				}
				else
				{
        			return Response::json('We could not upload your file. Please try again.', 400);
				}
			}
		}
		else
		{
			$item = $this->Request->file('file');

			$error = (null === $item) ? 'There was a problem processing your request.':$this->get_error_by_code($item->getError());

			if(!$ajax)
			{
				Session::flash('errors', $error);
				return Redirect::route('hummingbird.media.index');
			}
			else
			{
				return Response::json($error, 400);
			}
		}

		if(!$ajax) return Redirect::route('hummingbird.media.index');
	}

	/**
	 *
	 */
	public function uploadFile_byFunction($file, $collection_id = null)
	{
		$FileHelper = new FileHelper;

		if(File::exists($file))
		{
			//file exists
			$file_ext = strtolower(File::extension($file));
			$file_name = basename($file);

			// if($FileHelper->isValidExtension($file_ext))
			// {
	            $file_type 	= $FileHelper->getValidKeyByExt($file_ext);
	            $save_path 	= '/uploads/'.$file_type.'/'.date("Y-m-d").'/';
				$filename 	= $FileHelper->formatFileName(str_replace($file_ext, "", $file_name)) . ".{$file_ext}";
				$__isImage 	= $FileHelper->isImage($file_ext);

				if (File::isWritable(storage_path() . '/app/public/uploads/' . $file_type)) {
                    $counter        = 0;
                    $filename_tmp   = NULL;

                    while( File::exists( storage_path() . "/app/public" . $save_path . $filename_tmp ) ):
                        $filename_tmp   = $filename;

                        if( $counter > 0 ) {
                            $filename_tmp = str_replace(".{$file_ext}", "-{$counter}.{$file_ext}", $filename_tmp);
                        }

                        $counter++;
                    endwhile;

                    $filename = $filename_tmp ?: $filename;

					if($FileHelper->uploadFile_byFunction($file, $filename,  storage_path() . "/app/public" . $save_path))
					{
						/* save item */
						$media = new Media;
						$media->save();
						$ID = $media->id;

						$media->title = $file_name;
						$media->filename = $file_name;
						$media->location = '/uploads/'.$file_type.'/'.date("Y-m-d").'/'.$filename;
							
						if(isset($collection_id) AND $collection_id > 0)
						{
							$media->collection = $collection_id;
						}

						$media->save();
						
						// $media->state = true;
						$is_image = $__isImage;
						// $icon = 'fa-file-' . str_slug($file_ext);

						if($is_image)
						{
							/* if image */
							foreach($this->image_sizes['sizes'] as $size)
							{
								$width = (isset($size['width'])) ? $size['width']:null;
								$height = (isset($size['height'])) ? $size['height']:null;

								$media_child = $media->replicate();
								// $media_child = (new Media)->fill((array) $media);
								// $media_child->title = $file_name;
								// $media_child->filename = $file_name;
								$media_child->parent = $media->id;
								$media_child->collection = NULL;
								$media_child->mc_only = 1;
								$media_child->locked = 1;

								$FileHelper->create_image($filename, $save_path, $media_child, $width, $height);
							}
						}

						return $save_path . $filename;
					}
				}
			// }
		}

		return false;
	}

	/**
	 * Media items uploaded, now update the required fields
	 *
	 * @return Response
	 */
	public function updateUploadedFiles()
	{
		$items = $this->Request->get('media');
		$collection = (null !== $this->Request->get('collection')) ? $this->Request->get('collection'):null;

		if(count($items) > 0)
		{
			$updated = array();

			foreach($items as $media_id => $media_data)
			{
				$media_data = (array) json_decode($media_data);

				$media = Media::find($media_id);
				$media->fill($media_data);
				$media->save();

				/* Do we have children */
				if($media->children->count() > 0)
				{	
					//Yes we do - update them too
					$children = Media::where('parent', '=', $media->id)->get();

					foreach($children as $child)
					{
						$child->fill($media_data);
						$child->save();
					}
				}

				$updated[] = 'Meta data updated for &quot;' . $media->filename . '&quot;';
			}	

			Session::flash('success', $updated);
		}
		else
		{
			Session::flash('errors', 'No files to update.');
		}

		if(null !== $collection)
		{
			return Redirect::route('hummingbird.media.view', $collection);
		}
		
		return Redirect::route('hummingbird.media.index');
	}

	private function checkDefaultMedia($user_pref, $id)
	{
		if($this->activateUserDefaults)
		{
			if(null !== $user_pref)
			{
				$user_prefs = unserialize($user_pref); //unserialize data

				if(null !== $user_prefs['permissions']['default-collection'])
				{
					if(is_numeric($user_prefs['permissions']['default-collection']) AND $id != $user_prefs['permissions']['default-collection'])
					{
						return Redirect::route('hummingbird.media.view', $user_prefs['permissions']['default-collection']);
					}

					$this->base_user_dir = $user_prefs['permissions']['default-collection'];
				}
			}
		}
		return;
	}

	public function getMediaModalTemplate()
	{
		$html['template'] = '<section id="redactor-modal-advanced">'
	            . '<label>Enter a text</label>'
	            . '<textarea id="mymodal-textarea" rows="6"></textarea>'
	            . '</section>';
		return json_encode($html);
	            // return $html;
	}

	public function mediaLibraryRequest()
	{
		$images = Media::type()->get(); //just get all files
		$html = '';

		foreach($images as $image)
		{
			$html .= '<div class="col-sm-3"><div class="thumbnail imagemanager-insert"><img src="' . $image['location'] . '" alt="' . $image['filename'] . '" title="' . $image['filename'] . '" /></div></div>';
		}

		return json_encode($html);
	}

	private function notAvailable()
	{
		return;
	}

	public function postEditImage($image)
	{
		ini_set("memory_limit", "1024M"); //maximum memory limit - 1GB for large files - smaller than 10MB 
		try {
			$this_image = Media::find($image);

			if( File::exists(storage_path( "/app/public" ) . $this_image->location) ) {			
				$new__image = Image::make(storage_path( "/app/public" ) . $this_image->location);

				switch($_POST["update"])
				{
					case 'rotate':
						$rotate = ($this->Request->filled('rotate')) ? $this->Request->get('rotate'):NULL;

						if(null !== $rotate)
						{
							$new__image->rotate($rotate * 45);

					        $data = base64_encode($new__image->encode());

					        return $data;
						}

						break;
					case 'scale':
						// Resize the image
				        $new__image->resize($_POST['width'], $_POST['height'], function ($constraint)
				        {
				            $constraint->aspectRatio();
				            $constraint->upsize();
				        });

				        $data = base64_encode($new__image->encode());

				        return $data;
						break;
					case 'save':
						$save_path = '/uploads/images/'.date("Y-m-d").'/';

						if($_POST['type'] == 'crop')
						{

							// dd($_POST);
							if($_POST['width'] != $new__image->width() AND $_POST['height'] != $new__image->height())
							{
								//not the same - resize
						        $new__image->resize($_POST['width'], $_POST['height'], function ($constraint)
						        {
						            $constraint->aspectRatio();
						            $constraint->upsize();
						        });
							}

							/* Now Crop */
							$new__image->crop($_POST['w'], $_POST['h'], $_POST['x'], $_POST['y']);
						}
						else if($_POST['type'] == 'scale')
						{
							// Resize the image
					        $new__image->resize($_POST['width'], $_POST['height'], function ($constraint)
					        {
					            $constraint->aspectRatio();
					            $constraint->upsize();
					        });
						}

						if(!File::isWritable(storage_path() . "/app/public" . $save_path))
						{
							$result = File::makeDirectory(storage_path() . "/app/public" . $save_path, 0775); /* folder does not exist - create it */

							while(!$result)
							{
								$result = File::makeDirectory(storage_path() . "/app/public" . $save_path, 0775, true); /* folders do not exist - create recursively */
							}
						}

						//upload
						$filename = explode(".", $this_image->filename);
						$filename[0] = $filename[0] . "[" . $new__image->width() . "x" . $new__image->height() ."]";
						$filename = implode(".", $filename);

						if($new__image->save(storage_path() . "/app/public" . $save_path . $filename, 100))
						{
					        $child_image = $this_image->replicate();
					        $child_image->title = $this_image->title;
					        $child_image->filename = $this_image->filename;
					    	$child_image->location = $save_path . $filename;
					    	$child_image->parent = $this_image->getParentId(); 
					    	$child_image->collection = NULL;
					    	$child_image->mc_only = NULL;
					    	$child_image->locked = NULL;
					    	$child_image->save();

					    	$child_image->replicateTaxonomy();

					    	return Redirect::route('hummingbird.media.view-media', $child_image->id);
						}
						break;
				}
			}
			return Redirect::route('hummingbird.media.edit-image', $image);
		}
		catch(\Exception $e) { }
	}

	public function editImage($image)
	{
		$FileHelper = new FileHelper;

		try {
			$this->data['image'] = Media::findOrFail( $image );

			if(File::exists(storage_path() . "/app/public" . $this->data['image']->location)) {
				$file_extension = $FileHelper->getFileExtension($this->data['image']->location);

				//we have an image - check it
				if($FileHelper->isImage($file_extension))
				{
					/* Build breadcrumb */
					$ImageData = $this->data['image'];
					$parent__collection = DB::table('media-collections')->where('id', '=', $ImageData->collection)->first();
					$parent__collection_breadcrumb_path = 'hummingbird/media';
					
					if(null !== $ImageData->parent)
					{
						$parent = Media::find($ImageData->parent);
						$parent__collection = DB::table('media-collections')->where('id', '=', $parent->collection)->first();		
					}

					if(null !== $parent__collection)
					{
						$parent_collection_ids = array();
						$counter = 0;

						/* checking for the parent tree to be collected in an array */
						while (null !== $parent__collection) {
							$parent_collection_ids[$counter][$parent__collection->id] = $parent__collection->name;
							$parent__collection = DB::table('media-collections')->where('id', '=', $parent__collection->parent_collection)->first();
							$counter++;
						}

						/* Reverse the order of the array so that it omes in the order of parent folder/collection */
						$parent_collection_ids = array_reverse($parent_collection_ids);

						for($counter = 0; $counter < count($parent_collection_ids); $counter++) {
							$parentCollections = $parent_collection_ids[$counter];
							foreach($parentCollections as $key => $value) {
								$breadcrumb_path = 'hummingbird/media/view/' . $key;

								Breadcrumbs::for($breadcrumb_path, function ($breadcrumbs) use($value, $parent__collection_breadcrumb_path, $breadcrumb_path)  {
				                    $breadcrumbs->parent($parent__collection_breadcrumb_path);
				                    $breadcrumbs->push($value, url($breadcrumb_path));
				                });

				                $parent__collection_breadcrumb_path = 'hummingbird/media/view/' . $key;
							}
						}
					}

					if(null !== $ImageData->parent)
					{
						Breadcrumbs::for('hummingbird/media/view-media/'.$parent->id, function ($breadcrumbs) use($parent__collection_breadcrumb_path, $parent) {
			                $breadcrumbs->parent($parent__collection_breadcrumb_path);
			                $breadcrumbs->push((($parent->title != '') ? $parent->title:$parent->filename), url('hummingbird/media/view-media/'. $parent->id));
			            });

			            $parent__collection_breadcrumb_path = 'hummingbird/media/view-media/'.$parent->id;
					}

					Breadcrumbs::for("hummingbird/media/edit-image/".$ImageData->id, function ($breadcrumbs) use($parent__collection_breadcrumb_path, $ImageData) {
		                $breadcrumbs->parent($parent__collection_breadcrumb_path);
		                $breadcrumbs->push($ImageData->title, url('hummingbird/media/edit-image/' . $ImageData->id));
		            });

					//we have an image - check it
					if($FileHelper->isImage($file_extension))
					{
						//this is an image
						return View::make('HummingbirdBase::cms.media-edit-image', $this->data);
					}

					return Redirect::route('hummingbird.media.index')->with('message', 'Can not edit this media item as it is not an image. Please try again.');			
				}
			}
		}
		catch(\Exception $e) {}

		return Redirect::route('hummingbird.media.index')->with('message', 'No image was selected.');
	}

	public function viewItem($id)
	{
		$FileHelper = new FileHelper;
		$item = Media::find($id);

		if(null !== $item)
		{
			$item->media_description = (null !== $item->description) ? "$item->filename - $item->description":$item->title;
			$this->data['media'] = $item;
			$this->data['is_image'] = ($FileHelper->isImage(File::extension(storage_path() . "/app/public" . $item->location))) ? TRUE:FALSE;
	        $this->data['taxonomy'] = $item->taxonomy();

			/* Build breadcrumb */
			$parent__collection = DB::table('media-collections')->where('id', '=', $item->collection)->first();
			$parent__collection_breadcrumb_path = 'hummingbird/media';
			
			if(null !== $item->parent)
			{
				$parent = Media::find($item->parent);
				$parent__collection = DB::table('media-collections')->where('id', '=', $parent->collection)->first();		
			}

			if(null !== $parent__collection)
			{
				$parent_collection_ids = array();
				$counter = 0;

				/* checking for the parent tree to be collected in an array */
				while (null !== $parent__collection) {
					$parent_collection_ids[$counter][$parent__collection->id] = $parent__collection->name;
					$parent__collection = DB::table('media-collections')->where('id', '=', $parent__collection->parent_collection)->first();
					$counter++;
				}

				/* Reverse the order of the array so that it omes in the order of parent folder/collection */
				$parent_collection_ids = array_reverse($parent_collection_ids);

				for($counter = 0; $counter < count($parent_collection_ids); $counter++) {
					$parentCollections = $parent_collection_ids[$counter];
					foreach($parentCollections as $key => $value) {
						$breadcrumb_path = 'hummingbird/media/view/' . $key;

						Breadcrumbs::for($breadcrumb_path, function ($breadcrumbs) use($value, $parent__collection_breadcrumb_path, $breadcrumb_path)  {
		                    $breadcrumbs->parent($parent__collection_breadcrumb_path);
		                    $breadcrumbs->push($value, url($breadcrumb_path));
		                });

		                $parent__collection_breadcrumb_path = 'hummingbird/media/view/' . $key;
					}
				}
			}

			if(null !== $item->parent)
			{
				Breadcrumbs::for('hummingbird/media/view-media/'.$parent->id, function ($breadcrumbs) use($parent__collection_breadcrumb_path, $parent) {
	                $breadcrumbs->parent($parent__collection_breadcrumb_path);
	                $breadcrumbs->push((($parent->title != '') ? $parent->title:$parent->filename), url('hummingbird/media/view-media/'. $parent->id));
	            });

	            $parent__collection_breadcrumb_path = 'hummingbird/media/view-media/'.$parent->id;
			}

			Breadcrumbs::for("hummingbird/media/view-media/".$item->id, function ($breadcrumbs) use($parent__collection_breadcrumb_path, $item) {
                $breadcrumbs->parent($parent__collection_breadcrumb_path);
                $breadcrumbs->push($item->title, url('hummingbird/media/view-media/' . $item->id));
            });

			return View::make('HummingbirdBase::cms.media-view', $this->data);
		}

		return Redirect::route('hummingbird.media.index')->with('error', 'Media item could not be found.');
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function settings()
	{
		$this->data['image_sizes'] = $this->image_sizes['sizes'];
		return View::make('HummingbirdBase::cms.media-settings', $this->data);
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function showSettings($id)
	{
		die("SHOW SETTINGS");
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function createSetting()
	{
		if( $this->user->userAbility("media.create") )
		{
			/*
			* Check name has been posted
			* Check name does not exist
			* Check parent of collection to make sure
			* Insert new media collection name
			* 	Does the collection need to be stored in a parent?
			*/

			$rules = array(
				'title' => 'required|max:255',
				'width' => 'required_without:height|Integer',
				'height' => 'required_without:width|Integer'
			);

			$validator = Validator::make($this->Request->all(), $rules, array('required' => 'The :attribute is required.', 'max' => ':attribute should not be any larger than 255 characters'));

			if($validator->fails())
			{
				$messages = $validator->messages();

				// Log error in activity log
				return Redirect::route('hummingbird.media.settings')->with('MediaError', 'Please review your inputs.')->withInput()->withErrors($messages);
			}
			else
			{
				// add to collections
				// set parent (if required);
				if ($this->Request->filled('title'))
				{
					foreach($this->image_sizes['sizes'] as $size)
					{
						if($size['name'] == $this->Request->get('title'))
						{
							$messages = new \Illuminate\Support\MessageBag;
							$messages->add('error', 'Name of image resize already exists.');
							continue;
						}
					}

					if(!isset($messages))
					{
						$data = array();
						$data['name'] = $this->Request->get('title');
						$data['type'] = (null !== $this->Request->get('type')) ? $this->Request->get('type'):'';
						$data['description'] = (null !== $this->Request->get('description')) ? $this->Request->get('description'):'';
						$data['width'] = (null !== $this->Request->get('width')) ? $this->Request->get('width'):false;
						$data['height'] = (null !== $this->Request->get('height')) ? $this->Request->get('height'):false;

						$this->image_sizes['sizes'][] = $data;

						(new Setting)->updateSettings('media', $this->image_sizes);


						/* Activity */
						if(is_numeric($data['width']) AND is_numeric($data['height']))
						{
							$activity_text = '(' . $data['width'] . 'px by ' . $data['height'] . 'px)';
						}
						else
						{
							if(is_numeric($data['width']))
							{
								$activity_text = '(Max width: ' . $data['width'] . 'px)';
							}
							else
							{
								$activity_text = '(Max height: ' . $data['height'] . 'px)';
							}
						}

				        Activitylog::log([
				            'action' => 'UPDATED',
				            'type' => get_class(new Setting),
				            'link_id' => null,
				            'url' => "",
				            'description' => 'Added a new media size',
				            'notes' => Auth::user()->username . " added a new media size " . $activity_text
				        ]);

				        return Redirect::route('hummingbird.media.settings')->with('success', 'New setting created');
					}

					return Redirect::route('hummingbird.media.settings')->with('MediaError', 'There was a problem creating that setting. Please review your inputs.')->withInput()->withErrors( (isset($messages)) ? $messages : NULL );
				}
			}
		}

		return $this->forbidden(true);
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function deleteSetting()
	{
		die("DELETE SETTINGS");
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function updateSetting($id)
	{
		die("UPDATE SETTINGS");
	}


	public function globalMediaLibrary()
	{
		$FileHelper = new FileHelper;

		$input = $this->Request->all();
		
		$type = '';
		
		$html = $is_dl = $is_additional = false;
		
		if(isset($input['is_download']) AND $input['is_download'] == true) {
			$is_dl = true;
		}
		
		if(isset($input['is_additional']) AND $input['is_additional'] == true) {
			$is_additional = true;
		}

		if(isset($input['action']) AND $input['action'] == 'item')
		{
			$this->data['image'] = DB::table('media-items')->where('id', '=', $input['item'])->first();
			// $this->data['featured'] = (null !== $input['featured'] OR isset($input['featured'])) ? $input['featured']:false;
			$this->data['featured'] = false;

			if( $this->data['image'] )
			{
				$this->data['is_image'] = false;
				$this->data['is_file'] = false;
				$this->data['is_dl'] = $is_dl;
				$this->data['is_additional'] = $is_additional;

				if(File::exists(storage_path() . "/app/public" . $this->data['image']->location))
				{
					$file_extension = $FileHelper->getFileExtension($this->data['image']->location);

					//we have an image - check it
					if($FileHelper->isImage($file_extension))
					{
						$this->data['is_image'] = true;
						$this->data['versions'] = Media::where('parent', '=', $this->data['image']->id)->get();
						$type = 'image';

						/* Build breadcrumb */
						$parent_collection = $this->data['image']->collection;
						$breadcrumbs = array(
							0 => array(
					            'classes' => '',
					            'icon' => '',
					            'title' => $this->data['image']->filename,
					            'url' => route('hummingbird.media.view-media', $this->data['image']->id)
							)
						);

						while(null !== $parent_collection)
						{
							$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->first();

							array_unshift($breadcrumbs, array
								(
									'id' => $parent__collection->id,
									'classes' => '',
						            'icon' => '',
						            'title' => $parent__collection->name,
						            'url' => route('hummingbird.media.view', $parent__collection->id)
								)
							);
							
							$parent_collection = $parent__collection->parent_collection;
						}

						foreach($breadcrumbs as $crumb)
						{
							$this->addToBreadcrumb($crumb);
						}
						
						$html = View::make('HummingbirdBase::cms.media-global-view', $this->data)->render();
						
					} else if($FileHelper->isFile($file_extension)) {
						
						$this->data['file'] = $this->data['image'];
						$this->data['is_file'] = true;
						$this->data['extension'] = $file_extension;
						$type = 'file';
						
						/* Build breadcrumb */
						$parent_collection = $this->data['file']->collection;
						$breadcrumbs = array(
							0 => array(
					            'classes' => '',
					            'icon' => '',
					            'title' => $this->data['file']->filename,
					            'url' => route('hummingbird.media.view-media', $this->data['file']->id)
							)
						);
						
						while(null !== $parent_collection)
						{
							$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->first();

							array_unshift($breadcrumbs, array
								(
									'id' => $parent__collection->id,
									'classes' => '',
						            'icon' => '',
						            'title' => $parent__collection->name,
						            'url' => route('hummingbird.media.view', $parent__collection->id)
								)
							);
							
							$parent_collection = $parent__collection->parent_collection;
						}

						foreach($breadcrumbs as $crumb)
						{
							$this->addToBreadcrumb($crumb);
						}
						
						$html = View::make('HummingbirdBase::cms.media-global-view-file', $this->data)->render();
					}
				}
			}
		}
		else
		{
			$this->data['is_additional'] = $is_additional;
			
			if(isset($input['collection']) AND $input['collection'] > 0)
			{
				$input['type'] = (!isset($input['type']) OR $input['type'] == '') ? true:$input['type'];

				$this->data['media_collections'] 	= MediaCollection::where('parent_collection', $input['collection'])->orderBy('name')->get();
				$this->data['media_items'] 			= Media::search( $this->Request->get('s') )->inCollection($input['collection'])->parent(true)->get(); //just get all files

				switch((String) $input['type'])
				{
					case 'images':
					case 'documents':
					case 'audio':
					case 'video':
						$this->data['media_items'] = (new Media)->isType($input['type'], $FileHelper, $this->data['media_items']);
						break;
					default:
						break;
				}

				/* Build breadcrumb */
				$this->data['collection'] = DB::table('media-collections')->where('id', '=', $input['collection'])->first();
				$parent_collection = $this->data['collection']->parent_collection;
				$breadcrumbs = array(
					0 => array(
						'id' => $this->data['collection']->id,
			            'classes' => '',
			            'icon' => '',
			            'title' => $this->data['collection']->name,
			            'url' => route('hummingbird.media.view', $this->data['collection']->id)
					)
				);

				while(null !== $parent_collection)
				{
					$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->first();

					array_unshift($breadcrumbs, array
						(
							'id' => $parent__collection->id,
							'classes' => '',
				            'icon' => '',
				            'title' => $parent__collection->name,
				            'url' => route('hummingbird.media.view', $parent__collection->id)
						)
					);
					
					$parent_collection = $parent__collection->parent_collection;
				}

				foreach($breadcrumbs as $crumb)
				{
					$this->addToBreadcrumb($crumb);
				}
			}
			else
			{
				$this->data['media_collections'] = $this->data['Collections'];
				$this->data['media_items'] = Media::search( $this->Request->get('s') )->inCollection(false)->parent(true)->get(); //just get all files

				if(isset($input['type']))
				{
					switch($input['type'])
					{
						case 'images':
						case 'documents':
						case 'audio':
						case 'video':
							$this->data['media_items'] = (new Media)->isType($input['type'], $FileHelper, $this->data['media_items']);
							break;
						default:
							break;
					}
				}
			}
			
			$html = View::make('HummingbirdBase::cms.media-global', $this->data)->render();
		}

		return Response::json(array('html' => $html, 'type' => $type, 'is_dl' => $is_dl, 'is_additional' => $is_additional));
	}

	public function get_error_by_code($code = '')
	{
		$errors = array(
			0 => 'There is no error, the file uploaded with success.', //
			1 => 'The uploaded file exceeds the maximum upload size. Please contact your system administrator.', //
			2 => 'The maximum file size has been exceeded.', //The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.
			3 => 'There was a problem uploading this file. The file has only been uploaded partially.', //The uploaded file was only partially uploaded.
			4 => 'No file uploaded', //No file was uploaded.
			6 => 'Can not upload the file specified as no temporary folder exists. Please contact your system administrator.', // Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.
			7 => 'Failed to write file to disk.', //Failed to write file to disk. Introduced in PHP 5.1.0.
			8 => 'A configuration issue has caused your file to stop uploading. Please contact your system administrator.' //A PHP extension stopped the file upload. Introduced in PHP 5.2.0.
		);

		return $errors[$code];
	}
}
