<?php namespace Hummingbird\Controllers;

use App;
use Auth;
use Event;
use General;
use Redirect;
use Session;
use View;
use Validator;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Libraries\RoleManager;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Permission;
use Hummingbird\Models\PermissionGroup;
use Hummingbird\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class RoleController extends CmsbaseController
{
    public $preview_role = false;
    protected $all_roles = array();
    protected $roles = array();
    protected $user_role_id = 0;

    public function __construct(Request $Request, Role $role, RoleManager $RoleManager)
    {
        parent::__construct($Request);

        if(null !== Session::get('view_role_perms'))
        {
            $this->preview_role = $this->getPreviewRole(Session::get('view_role_perms'));
        }

        $this->model = $role;

        $this->RoleManager = $RoleManager;

        if( !Breadcrumbs::exists('hummingbird/roles') ):
            Breadcrumbs::for('hummingbird/roles', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Roles', url('hummingbird/roles'));
            });
        endif;
    }

    public function index()
    {
        if($this->user->userAbility('role.read'))
        {
            $this->data['tag'] = 'Manage Roles';

            $this->data['system_roles'] = $this->RoleManager->getRoles();
            $this->data['total_system_roles'] = $this->RoleManager->getTotalAvailbleRoles();

            return View::make('HummingbirdBase::cms/roles', $this->data);
        }

        return parent::forbidden();
    }


    function buildTree($elements, $parentId = 1, $level = 0, $stop_at_parent = false) 
    {
        foreach ($elements as $key => $element) {
            /* Top level parent */
            if ($element->parentrole_id !== $parentId OR $element->parentrole_id === NULL) continue;

            /* If hidden don't show */
            if(!$element->hidden)
            {
                /* Do not show in edit mode - stops moving child into child */
                if($element->id === $stop_at_parent OR $element->parentrole_id === $stop_at_parent) $this->data['role_parents'][] = $element->id;

                // $element['name'] = ($level > 0) ? '&#8627;'.$element['name']:$element['name']; //append arrow to name
                $this->roles[$element->id] = $element;
                $this->roles[$element->id]['level'] = $level;
                $this->buildTree($elements, $element->id, $level + 1, $stop_at_parent);
            }
        }
    }

    public function show($id)
    {
        if($this->user->userAbility('role.update'))
        {
            $role = Role::find($id);
            $this->data['role'] = $role;
            $this->data['tag'] = 'Edit: '.$this->data['role']->name;
            $this->data['system_roles'] = $this->RoleManager->getRoles();
            $this->data['users'] = $this->data['role']->users()->get();
         

            Breadcrumbs::for("hummingbird/roles/{$this->data['role']->id}", function ($breadcrumbs)  {
                $breadcrumbs->parent('hummingbird/roles');
                $breadcrumbs->push('Edit: ' . $this->data['role']->name, url("/hummingbird/roles/{$this->data['role']->id}"));
            });   

            $FilterByParentRole = ($role->isSuperUser() || ($role->parentrole && $role->parentrole->isSuperUser())) ? NULL : $role->parentrole->id;
            $this->data['PermissionGroups']     = PermissionGroup::filterPermissionsByRole($FilterByParentRole)->get();
            $this->data['AllowedPermissions']   = ($role->isSuperUser() || ($role->parentrole && $role->parentrole->isSuperUser())) ? Permission::pluck('name')->all() : $role->parentrole->perms->pluck('name')->all();

            return View::make('HummingbirdBase::cms/roles-edit', $this->data);
        }
        
        return parent::forbidden();
    }
    
    public function update($id)
    {
        if($this->user->userAbility('role.update'))
        {
            $input = $this->Request->except('_token');

            $validator = Validator::make($input, [
                    'name' => "required|unique:roles,name,$id|max:255"
                ], [
                    'name.required' => 'The :attribute field is required.',
                    'name.unique' => 'The :attribute must be unique.',
                    'name.max' => 'The :attribute must be shorter than 255 characters'
                ]);

            $role = Role::find($id)->fill($input);
            $role->save();

            // Update role permissions
            $role->perms()->sync($this->Request->get('selected_perms') ?: []);

            Event::dispatch('RoleWasUpdated', array($role));

            Activitylog::log([
                'action' => 'UPDATED',
                'type' => get_class($role),
                'link_id' => $role->id,
                'description' => 'Updated role',
                'notes' => Auth::user()->username . " updated the role - &quot;$role->name&quot;"
            ]);

            return Redirect::route('hummingbird.roles.show', $id);
        }

        return parent::forbidden();
    }
    
    public function store()
    {
        if($this->user->userAbility('role.create'))
        {
            $input = $this->Request->except('_token', 'add');

            $Validator = Validator::make( $this->Request->all(), [
                'name' => 'required|between:1,128|unique:roles',
                'parentrole_id' => 'required|integer'
            ], []);

            if( $Validator->fails() ):
                return Redirect::route('hummingbird.roles.index')->withErrors( $Validator )->withInput()->withError('There was a problem validating this new role');
            endif;

            $role = (new Role)->fill($input);
            $role->save();

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($role),
                'link_id' => $role->id,
                'description' => 'Created role',
                'notes' => Auth::user()->username . " created a new role - &quot;$role->name&quot;"
            ]);
            return Redirect::route('hummingbird.roles.show', $role->id);
        }

        return parent::forbidden();
    }

    public function destroy( $RoleId ) {
        if( $this->user->userAbility('role.delete') ) {
            try {
                $role = Role::findOrFail( $RoleId );

                Activitylog::log([
                    'action' => 'DELETED',
                    'type' => get_class($role),
                    'link_id' => null,
                    'description' => 'Deleted role',
                    'notes' => Auth::user()->username . " deleted the &quot;$role->name&quot; role"
                ]);

                $role->delete();
                
                return Redirect::route('hummingbird.roles.index')->with('success', 'Role deleted');
            }
            catch(\Exception $e ) {
                // silence
            }

            return Redirect::route('hummingbird.roles.index')->with('error', 'Unable to delete role');
        }

        return parent::forbidden();
    }

    public function preview($role_id = null)
    {
        switch($this->Request->get('action'))
        {
            case 'remove':
                return $this->removePreviewPerms();
                break;
            default:
                if(is_numeric($role_id) AND null !== Role::find($role_id))
                {
                    Session::put('view_role_perms', $role_id);
                    Session::save();
                }
                break;
        }

        return Redirect::to('hummingbird');
    }

    public function removePreviewPerms()
    {
        Session::forget('view_role_perms');

        return Redirect::to('hummingbird');
    }

    public function getPreviewRole($role_id)
    {
        $role = Role::find($role_id);

        if(null === $role) return $this->removePreviewPerms();
        
        return $role->name;
    }
}