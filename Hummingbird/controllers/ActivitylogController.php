<?php namespace Hummingbird\Controllers;

use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class ActivitylogController extends CmsbaseController
{
    public $pagination = 30;

    /**
     * Inject the models.
     */
    public function __construct(Request $Request) {
        parent::__construct($Request);

        if( !Breadcrumbs::exists('hummingbird/activity-logs') ):
            Breadcrumbs::for('hummingbird/activity-logs', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Activity Logs', url('hummingbird/activity-logs'));
            });
        endif;
    }

    public function index() {
        if($this->user->userAbility('activitylog.read')) {
            $this->data['tag'] = 'Manage Activity Logs';

            $this->data['activitylogs'] = Activitylog::byuser( $this->Request->get('user') ?: NULL )->byaction( $this->Request->get('action') ?: NULL )->bytype( $this->Request->get('type') ?: NULL )->orderBy('id', 'DESC')->paginate( $this->pagination );
            
            return View::make('HummingbirdBase::cms/activitylogs', $this->data);
        }

        return $this->forbidden(true);
    }
}
