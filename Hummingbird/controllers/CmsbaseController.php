<?php namespace Hummingbird\Controllers;

use App;
use Auth;
use Event;
use General;
use Image;
use Log;
use Redirect;
use Response;
use URL;
use View;
use App\Http\Controllers\Controller;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Libraries\Themes;
use Hummingbird\Models\Setting;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class CmsbaseController extends Controller
{
    protected $model;

    public $data; // data to pass to views
    public $site_settings;
    public $search = false;
    public $pagination = 15;
    

    public function __construct(Request $Request)
    {
        $this->Request = $Request;

        if(App::bound('plugin.manager'))
            $this->data['PluginManager'] = App::make('plugin.manager');

        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            
            return $next($request);
        });


        // For use with the Microsite builder and manager service
        if(class_exists('\Tickbox\Microsites\Microsites'))
        {
            $this->data['Microsites'] = new \Tickbox\Microsites\Microsites;

            $this->data['microsite'] = $this->data['Microsites']->load();

            if(null === $this->data['microsite'] OR $this->data['Microsites']->url_allowed()) $this->data['Microsites']->purgeDBConnection();
        }

        $this->site_settings = Setting::all();

        $this->data['breadcrumbs'] = array(
            0 => array(
                'classes' => '',
                'icon' => 'fa fa-home',
                'title' => 'Home',
                'url' => '/' . General::backend_url()
            )
        ); //breadcrumb manager

        $this->data['has_search'] = FALSE;

        $this->theme = Themes::activeTheme();
        
        View::addLocation(base_path()."/themes/public/$this->theme/");
        View::addNamespace('theme', base_path() . '/themes/public/' . $this->theme . '/');

        // Admin
        if( !Breadcrumbs::exists('HummingbirdBreadcrumbs') ):
            config()->set('breadcrumbs.view', 'HummingbirdBase::cms.partials.breadcrumbs');
            
            Breadcrumbs::for('HummingbirdBreadcrumbs', function ($breadcrumbs) {
                $breadcrumbs->push('Home', url('/hummingbird'), ['icon' => 'fa-home']);
            });
        endif;


        $adminThemeAssetsPath = public_path( 'themes/admin' );

        if( !file_exists($adminThemeAssetsPath) ) {
            app(Filesystem::class)->makeDirectory($adminThemeAssetsPath, 0755, true);
        }
        
        // Create target symlink public theme assets directory if required
        if( !file_exists( public_path() . "/themes/admin/hummingbird" ) ) {
            symlink( base_path( "themes/admin/hummingbird" ), public_path( "themes/admin/hummingbird" ) );
        }

        Event::dispatch('backend.page.beforeDisplay');
    }


    public function createDatabaseBackup($contents)
    {
        $filename = 'database-backup-' . date("Y-m-d H:i:s");
        $save_path = base_path() . '/backups/databases/';

        if(File::is_writable($save_path . $filename))
        {
            $bytes_written = File::put($save_path . $filename, $contents);
            if ($bytes_written === false)
            {
                die("Error writing to file");
            }
        }
        else
        {
            die( 'unable to write' );
        }
    }
    
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }
    
    public function search_model($model_name)
    {
        $search = Search::make(array(
            'model' => $model_name, 
            'searchterm' => Request::get('search'),
            'which_end' => 'back'));
        
        $search->run();       
        
        return $search->results;
    }
    
    public function export_model($modelname)
    {
        $model = new $modelname;
        $this->data['modelname'] = strtolower($modelname);
        $this->data['fields'] = $model->export_fields;
        return View::make('HummingbirdBase::cms.export', $this->data);
    }
    
    public function run_export($modelname, $filename)
    {
        $fields = Input::except('_token');
        
        $results = $modelname::select(DB::raw(implode(",", $fields)))->get();
        
        // headings
        $output = '"' . implode('","', $fields) . '"';
        $output .= "\r\n";
 
        foreach ($results as $row) {
            $output .= '"' . implode('","',$row->toArray()) . '"' . "\r\n";
        }
        
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
        );
 
        return Response::make(rtrim($output, "\n"), 200, $headers);        
    }
    
    public function forbidden($redirect = false) {
        Log::warning("Deprecated parameter $redirect will be removed in a later version and must be removed from all instances ");

        return Response::make(View::make('HummingbirdBase::cms.errors.unauthorised')->render(), 403);
    }




    /**
     * Add a leading slash to the URL - remove trailing slash
     *
     * @param string $url URL to normalize.
     * @return string Returns normalized URL.
     */
    public static function normalizeUrl($url)
    {
        if (substr($url, 0, 1) != '/')
            $url = '/'.$url;

        if (substr($url, -1) == '/')
            $url = substr($url, 0, -1);

        if (!strlen($url))
            $url = '/';

        return $url;
    }

    /**
     * Splits an URL by segments separated by the slash symbol.
     *
     * @param string $url URL to segmentize.
     * @return array Returns the URL segments.
     */
    public static function segmentizeUrl($url)
    {
        $url = self::normalizeUrl($url);
        $segments = explode('/', $url);

        $result = array();
        foreach ($segments as $segment) {
            if (strlen($segment))
                $result[] = $segment;
        }

        return $result;
    }

    /**
     * Find controller that we need
     * If controller not found, returns CMS 404 page
     * If controller, or action, not available then returns 500 forbidden
     * @param string $url Specifies the requested page URL.
     * If the parameter is omitted, the current URL used.
     * @return string Returns the processed page content.
     */
    public function run($url = null)
    {
        //if the URL is null or blank, then we need to go to the dashboard
        if(null === $url) $url = 'dashboard';

        $url = General::backend_url() . '/' . $url;

        dd($url);
        
        $params = $this->segmentizeUrl($url);

        $action = (!isset($params[1])) ? 'read':$params[1];

    }

    public function last_db_query()
    {
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        return $last_query;
    }

    public function addToBreadcrumb($data)
    {
        $this->data['breadcrumbs'][] = $data;
    }

    public function renderBreadcrumb()
    {
        foreach($this->breadcrumbs as $crumb)
        {
            $this->addToBreadcrumb($crumb);
        }
    }

    public function error($exception = null)
    {
        $messages = array(
            'There are 320 different species of hummingbird found throughout the Americas. The smallest of all is the tiny bee hummingbird, Mellsuga helenae. They are among the smallest of birds.',
            'Their English name derives from the characteristic hum made by their rapid wing beats.',
            'Each species of hummingbird makes a different humming sound, determined by the number of wing beats per second.',
            'They can hover in mid-air by rapidly flapping their wings 12–90 times per second (depending on the species).',
            'During courtship, the wingtips of the ruby-throated hummingbird beats up to 200 times per second, as opposed to the usual wing beat of 90 times per second.  When the female appears, her partner displays by flying to and fro in a perfect arc.  The pair then dives up and down vertically, facing each other.',
            'They can also fly backwards, and are the only group of birds able to do so.',
            'They can fly at speeds exceeding 15 m/s or 54 km/h.',
            'Male hummingbirds are reported to have achieved speeds of almost 400 body lengths per second when swooping in an effort to impress females.',
            'They have feet so tiny that they cannot walk on the ground, and find it awkward to shuffle along a perch.',
            'They have a short, high-pitched squeaky call.',
            'Some are so small, they have been known to be caught by dragonflies and praying mantis, trapped in spider’s webs, snatched by frogs and stuck on thistles.',
            'The hummingbird needs to eat twice its bodyweight in food every day, and to do so they must visit hundreds of flowers daily.',
            'Hummingbirds eat nectar for the most part, although they may catch an insect now and then for a protein boost.',
            'Their super fast wing beats use up a lot of energy, so they spend most of the day sitting around resting. To save energy at night, many species go into torpor (a short-term decrease in body temperature and metabolic rate).',
            'Despite their tiny size, the ruby-throated hummingbird makes a remarkable annual migration, flying over 3000km from the eastern USA, crossing over 1000km of the Gulf of Mexico in a single journey to winter in Central America.',
            'The ruby-throated hummingbird has only approximately 940 feathers on its entire body.',
            'Before migrating, the hummingbird stores a layer of fat equal to half its body weight.',
            'The female hummingbird builds a tiny nest high up in a tree, often up to six metres from the ground. She coats the outside with lichen and small pieces of bark and lines the inside with plant material.',
            'The male takes no responsibility in rearing the young and may find another mate after the first brood hatches.',
            'Some species of hummingbird are so rare that they have been identified only from the skins exported to Europe.'
        );

        $Content = View::make('HummingbirdBase::cms.errors.404', array('exception' => $exception, 'quote' => $messages[array_rand($messages)]))->render();
        
        return Response::make($Content, 404);
    }

    /**
     * Activates whether or not to show the search field
     * 
     */
    public function activateSearch()
    {
        $this->data['has_search'] = TRUE;
    }


    /**
     * Get the referrer parameters that we've just come from (if we have a page) so we can go back to it
     * @return Array
     */
    public function getReferrerParameters() {
        $previous = parse_url( URL::previous() );
        $parameters = [];

        if( isset($previous['query']) ) {
            parse_str( $previous['query'], $parameters );
        }

        return $parameters;
    }
}
