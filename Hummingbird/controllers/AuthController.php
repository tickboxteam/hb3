<?php namespace Hummingbird\Controllers;

use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

class AuthController extends Controller
{    
    /**
     * Create a new Auth instance to allow us to logout
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    /**
    * Logout and clear data
    */
    public function logout() {
        $this->auth->logout();

        return Redirect::to( '/' );
    }
}