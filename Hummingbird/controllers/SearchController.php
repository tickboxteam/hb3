<?php namespace Hummingbird\Controllers;

use ArrayHelper;
use Event;
use SearchIndex;
use View;
use Hummingbird\Controllers\FrontendController;
use Hummingbird\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

/**
* Standard class for site searching
* 
* 
* @package    Tickbox\Hummingbird3
* @subpackage Controller
* @author     Daryl Phillips <darylp@tickboxmarketing.co.uk>
*/
class SearchController extends FrontendController 
{
	public $pagination 	= 15;
	public $ordering 	= 'DESC';
	public $filter 		= 'relevance';

	/**
	 * Initilise the controller to handle searching
	 */
	public function __construct(Request $Request)
	{
		parent::__construct($Request);

		$this->hasPageFilter();
	}


	/**
	 * Run through and search this page
	 * @return Void
	 */
	public function search()
	{
		$this->ordering = ( $this->request->filled('order') && strtoupper( $this->request->get('order') ) == 'ASC' ) ? 'ASC' : 'DESC';
		$this->filter 	= ( $this->request->filled('filter') && !empty( $this->request->get('filter') ) ) ? $this->request->get('filter') : 'relevance';
		$this->exclude	= ( $this->request->filled('exclude') ) ? ArrayHelper::cleanExplodedArray( explode("+", urlencode($this->request->get('exclude')))) : [];
		$this->include	= ( $this->request->filled('include') ) ? ArrayHelper::cleanExplodedArray( explode("+", urlencode($this->request->get('include')))) : [];
		$this->section  = ( $this->request->filled('section') && $this->isValidPage( $this->request->get('section') ) ) ? $this->request->get('section') : NULL;

		$this->data['SearchTerm'] 		= trim($this->request->get('s'));
		$this->data['SearchResults'] 	= SearchIndex::search()->section( $this->section )->exclude( $this->exclude )->include( $this->include )->ordering($this->filter, $this->ordering);

        if( $event = Event::dispatch('frontend.page.search', ['results' => $this->data['SearchResults'], 's' => $this->data['SearchTerm'], 'ordering' => $this->ordering, 'filter' => $this->filter, 'exclude' => $this->exclude, 'include' => $this->include, 'section' => $this->section]) ) {
            $this->data['SearchResults'] = $event[0];
        }
        
        $this->data['SearchResults'] = $this->data['SearchResults']->paginate( $this->pagination );

		if( isset($this->data['page']) && !empty($this->data['page']) && strpos($this->data['page']->content, "[HB::SEARCH]") !== FALSE)
		{
			$search_view = (View::exists('theme::search-template')) ? 'theme::search-template':'HummingbirdBase::public.search';
			
			$this->data['page']->content = str_replace("[HB::SEARCH]", View::make($search_view)->with($this->data)->render(), $this->data['page']->content);
		}

		return parent::view('theme::search');
	}

	/**
	 * Do we have a page to filter by?
	 * @param  String  $page
	 * @return boolean
	 *
	 * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
	 */
	protected function isValidPage( $page ) {
        try {
            Page::where('permalink', "/" . trim($page, "/") . "/")->where('status', 'public')->firstOrFail();
            return true;
        }
        catch (\Exception $e) {}

        return NULL;
	}
}
