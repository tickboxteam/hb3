<?php namespace Hummingbird\Controllers;

use View;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Libraries\DashboardWidgets;
use Hummingbird\Models\Page;

class DashboardController extends CmsbaseController {
    
    public $code_location = 'dashboard';

	public function index()
	{
        $this->data['tag'] = 'Dashboard';        
        
        $this->getCommonData();
        return View::make('HummingbirdBase::cms.dashboard', $this->data);

	}
    
    private function getCommonData()
    {
        $this->data['pages'] = Page::get_selection('Select a parent page');
        $this->data['dashboard_widgets'] = DashboardWidgets::all();
    }
}
