<?php namespace Hummingbird\Controllers;

use App;
use Auth;
use General;
use Redirect;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Moduletemplate;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class ModuleTemplateController extends CmsbaseController
{
    public function __construct(Request $Request, Moduletemplate $Moduletemplate)
    {
        parent::__construct($Request);

        $this->model = $Moduletemplate;

        if( !Breadcrumbs::exists('hummingbird/module-templates') ):
            Breadcrumbs::for('hummingbird/module-templates', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Module templates', url('hummingbird/module-templates'));
            });
        endif;

        $this->activateSearch();
    }
    
    public function index()
    {
        if($this->user->userAbility('modules.templates.read'))
        {
            $this->data['tag'] = 'Module Templates';

            $query = Moduletemplate::search($this->Request->get('s'))->microsite();

            $orderByDirection = $this->Request->get('order_by_direction');
            if(!empty($this->Request->filled('order_by_fields'))) {
               $query->orderBy($this->Request->get('order_by_fields'), $orderByDirection);
            } else {
                $query->orderBy('name', 'ASC');
            }

            $this->data['moduletemplates'] = $query->paginate($this->pagination);

            return View::make('HummingbirdBase::cms/moduletemplates', $this->data);
        }
        
        return parent::forbidden();
    }
    
    public function edit($id = NULL)
    {
        if($this->user->userAbility('modules.templates.update'))
        {
            $this->data['moduletemplate'] = Moduletemplate::microsite()->where('id', '=', $id)->first();
            
            if(null !== $this->data['moduletemplate'])
            {
                $this->data['tag'] = 'Edit '.$this->data['moduletemplate']->title;

                Breadcrumbs::for("hummingbird/module-templates/{$this->data['moduletemplate']->id}/edit", function ($breadcrumbs)  {
                    $breadcrumbs->parent('hummingbird/module-templates');
                    $breadcrumbs->push('Edit: ' . $this->data['moduletemplate']->name, url('hummingbird.module-templates.show' . $this->data['moduletemplate']->id));
                });

                return View::make('HummingbirdBase::cms/moduletemplates-edit', $this->data);
            }

            return Redirect::route('hummingbird.module-templates.index')->with('error', 'Module template could not be found');
        }
        
        return parent::forbidden();
    }
    
    public function update($id = NULL)
    {
        if($this->user->userAbility('modules.templates.update'))
        {
            $input = $this->Request->except('_token');
            $this->model = Moduletemplate::microsite()->where('id', '=', $id)->first();

            if(null !== $this->model)
            {
                if( !$this->model->editable && !Auth::user()->isSuperUser() ):
                    return redirect()->back()->with('error', 'Unable to modify template as it\'s locked and you do not have permission to update it.');
                endif;

                $messages = [
                    'name.required' => 'The :attribute field is required.',
                    'name.unique' => 'The :attribute must be unique.',
                    'name.max' => 'The :attribute must be shorter than 255 characters'
                ];

                $validator = Validator::make($input, [
                    'name' => "required|unique:moduletemplates,name,$id|max:255"
                ], $messages);

                if ($validator->fails())
                {
                    //return save error
                    return Redirect::route('hummingbird.module-templates.edit', $this->model->id)->withInput()->withErrors($validator);
                }

                $this->model->update($input);
                $this->model->save();

                Activitylog::log([
                    'action' => 'UPDATED',
                    'type' => get_class($this->model),
                    'link_id' => $this->model->id,
                    'description' => 'Updated module template',
                    'notes' => Auth::user()->username . " updated the module template - &quot;".$this->model->name."&quot;"
                ]);            
                
                return Redirect::route('hummingbird.module-templates.edit', $this->model->id)->with('success', 'Module template has been updated.');
            }

            return Redirect::route('hummingbird.module-templates.index')->with('error', 'Module template could not be found');
        }
        
        return parent::forbidden();
    }

    public function postLock($id = NULL)
    {
        if($this->user->userAbility('modules.templates.lock'))
        {
            $this->model = Moduletemplate::microsite()->where('id', '=', $id)->first();

            if(null !== $this->model)
            {
                if( $this->Request->get('lock') !== NULL ) {

                    if($this->model->editable == 0) {
                        if(Auth::user()->isSuperUser()) {
                            $this->model->editable = 1;
                        } else {
                            return Redirect::route('hummingbird.module-templates.index')->with('error', 'Unable to unlock module template.');
                        }
                    } else {
                        $this->model->editable = 0;
                    }

                    $messageTXT = ($this->Request->get('lock') == 1 ) ? "unlocked" : "locked";
                    if($this->model->save())
                    {
                        Activitylog::log([
                            'action' => $messageTXT,
                            'type' => get_class($this->model),
                            'link_id' => $this->model->id,
                            'description' => 'Module template '.$messageTXT,
                            'notes' => Auth::user()->username . " " . $messageTXT . " the module template &quot;" . $this->model->name . "&quot;"
                        ]);            
                            
                        return Redirect::route('hummingbird.module-templates.index')->with('success', 'Module template has been '.$messageTXT.'.');
                    }
                }

                return Redirect::route('hummingbird.module-templates.index')->with('error', 'Unable to lock module template.');
            }

            return Redirect::route('hummingbird.module-templates.index')->with('error', 'Unable to find module template');
        }
        
        return parent::forbidden();
    }
    
    public function store()
    {
        if($this->user->userAbility('modules.templates.create'))
        {
            $this->Request->merge(array_map('trim', $this->Request->all()));
            $input = $this->Request->except('_token', 'add');      

            $moduletemplate = (new Moduletemplate)->fill($input);

            $messages = [
                'name.required' => 'The :attribute field is required.',
                'name.unique' => 'The :attribute must be unique.',
                'name.max' => 'The :attribute must be shorter than 255 characters'
            ];

            $validator = Validator::make($input, [
                'name' => 'required|unique:moduletemplates,name|max:255'
            ], $messages);

            if ($validator->fails())
            {
                return Redirect::route('hummingbird.module-templates.index')->withErrors($validator)->withInput();
            }

            if(isset($this->data['Microsites']) AND null !== $this->data['Microsites'] AND null !== $this->data['microsite']) $moduletemplate->microsite_id = $this->data['microsite']->id;

            if(!$moduletemplate->save())
            {
                return Redirect::route('hummingbird.module-templates.index')->withErrors($moduletemplate->errors());
            }

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($this->model),
                'link_id' => $moduletemplate->id,
                'description' => 'Created a new module template',
                'notes' => Auth::user()->username . " created a new module template - &quot;$moduletemplate->name&quot;"
            ]);  

            return Redirect::route('hummingbird.module-templates.edit', $moduletemplate->id);
        }
        
        return parent::forbidden();
    }
    
    public function destroy($id = NULL) {
        if($this->user->userAbility('modules.templates.delete')) {
            $this->model = Moduletemplate::microsite()->where('id', $id)->first();

            if(null !== $this->model) {
                if( !$this->model->editable && !Auth::user()->isSuperUser() ):
                    return redirect()->back()->with('error', 'Unable to delete template as it\'s locked and you do not have permission to remove it.');
                endif;

                $name = $this->model->name;
                $ModuleCount = $this->model->modules()->count();

                $this->model->delete($id);

                Activitylog::log([
                    'action' => 'DELETED',
                    'type' => get_class($this->model),
                    'link_id' => $id,
                    'description' => 'Deleted module template',
                    'notes' => Auth::user()->username . " deleted the module template &quot;$name&quot; role"
                ]);

                return Redirect::route('hummingbird.module-templates.index')
                    ->with('success', ( $ModuleCount > 0 ? 'Module template and associated modules have been deleted.' : 'Module template deleted.'));
            }

            //return errors too
            return Redirect::route('hummingbird.module-templates.index');
        }
        
        return parent::forbidden();
    }
}
