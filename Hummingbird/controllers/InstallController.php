<?php namespace Hummingbird\Controllers;

use Redirect;
use Session;
use App\Http\Controllers\Controller;
use Hummingbird\Libraries\Installer;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

/**
 * Installing Hummingbird Controller
 *
 * @package HummingbirdBase
 * @author Daryl Phillips, Jo Cavil
 *
 */
class InstallController extends Controller 
{
    public function __construct( Request $Request )
    {
        $this->Request = $Request;
    }

    public $data = array();
    
	public function getIndex()
	{
        Session::put('install_url', Installer::uri());
        $this->data['step'] = 0;
		return view('HummingbirdBase::install.welcome', $this->data);
	}
    
	public function getDatabase()
	{
        Session::put('install_url', Installer::uri() . '/database');
        $this->data['step'] = 1;
		return view('HummingbirdBase::install.database', $this->data);
	}    
    
	public function postDatabase()
	{
        $this->Request->flash();
        $input = $this->Request->except('_token');
        $installer = new Installer;
        
        if(!$installer->setupCredentials($input)) {
            return Redirect::to(Installer::uri().'/database')->withErrors($installer->errors);
        }
        
        return Redirect::to(Installer::uri().'/run-migrations')->with('token', 'from_hb');
        
	}
    
    // this needs to be a new request so that the new .env.php file is picked up
    public function getRunMigrations()
    {
        if(Session::get('token') != 'from_hb') {
            return Redirect::to(Installer::uri().'/database')->withErrors(array('general' => 'Invalid route'));
        }
        
        $installer = new Installer;
        if (!$installer->runMigrations()) {
            return Redirect::to(Installer::uri().'/database')->withErrors($installer->errors);
        }
        
        return Redirect::to(Installer::uri().'/site');
    }
        
	public function getSite()
	{
        Session::put('install_url', Installer::uri() . '/site');
        $this->data['step'] = 2;

        if ( ! function_exists('openssl_random_pseudo_bytes'))
        {
            throw new RuntimeException('OpenSSL extension is required.');
        }

        $bytes = openssl_random_pseudo_bytes(32 * 2);

        if ($bytes === false)
        {
            throw new RuntimeException('Unable to generate random string.');
        }

        $this->data['site_key'] = substr(str_replace(array('/', '+', '='), '', base64_encode($bytes)), 0, 32);

		return view('HummingbirdBase::install.site', $this->data);
	}
    
	public function postSite()
	{
        $this->Request->flash();
        $input = $this->Request->except('_token');
        $installer = new Installer;
        
        if( !$installer->updateSiteSettings($input) ) {
            return Redirect::to(Installer::uri().'/site')->withErrors($installer->errors);
        }
        
        return Redirect::to(Installer::uri().'/permissions');
	}
    
	public function getPermissions()
	{
        Session::put('install_url', Installer::uri() . '/permissions');
        $this->data['step'] = 3;
        
		return view('HummingbirdBase::install.permissions', $this->data);
	}
    
	public function postPermissions()
	{
        $this->Request->flash();
        $input = $this->Request->except('_token');
        $installer = new Installer;
        
        if (!$installer->addAdministrator( $this->Request->except('_token', 'password_confirmation') )) {
            return Redirect::to(Installer::uri().'/permissions')->withErrors($installer->errors);
        }

        return redirect()->route('hummingbird.install.complete');
	}
    
	public function getPlugins()
	{
        // there's a big here: for some reason the session var in Installer::updateSiteSettings() isn't being persisted, and i don't know why
        if (Session::get('installtype') != 'advanced') return $this->finished(new Installer);
        
        Session::put('install_url', Installer::uri() . '/plugins');        
        $this->data['step'] = 4;
		return view('HummingbirdBase::install.plugins', $this->data);
	}
    
    public function postPlugins()
    {        
        $this->Request->flash();
        $input = $this->Request->except('_token');
        $installer = new Installer;
        
        if (!$installer->runPluginMigrations($input)) {
            return Redirect::to(Installer::uri().'/plugins')->withErrors($installer->errors);
        }
        
        return route('hummingbird.install.complete');
    }
    
    public function complete()
    {
        $installer = new Installer;
        $installer->complete();
        Session::flush();
        
        // redirect to finish page
        return view()->make('HummingbirdBase::install.done');
    }
}
