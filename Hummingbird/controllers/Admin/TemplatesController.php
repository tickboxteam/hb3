<?php namespace Hummingbird\Controllers\Admin;

use App;
use Auth;
use General;
use Redirect;
use Validator;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

/**
 * Hummingbird AuthController
 *
 * @package Hummingbird
 * @author Daryl Phillips
 *
 */
class TemplatesController extends CmsbaseController
{
    public function __construct(Request $Request) {
        parent::__construct($Request);

        if( !Breadcrumbs::exists('hummingbird/templates') ):
            Breadcrumbs::for('hummingbird/templates', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Page templates', url('hummingbird/templates'));
            });
        endif;
    }

    public function index()
    {
        if($this->user->userAbility('template.read')) {
            $this->data['tag']          = 'Manage Templates';
            $this->data['templates']    = Template::microsite()->orderBy('name')->whereNull('deleted_at')->get();
            return View::make('HummingbirdBase::cms/templates-new', $this->data);
        }

        return $this->forbidden(true);
    }
    
    public function edit($id)
    {
        if($this->user->userAbility('template.update')) {
            $this->data['template'] = Template::microsite()->where('id', $id)->first();

            if($this->data['template']) {
                if( $this->Request->get('lock') !== NULL ) {
                    $this->data['template']->locked = ($this->Request->get('lock') == 1 ) ? 1 : 0;

                    $messageTXT = ($this->Request->get('lock') == 1 ) ? "locked" : "unlocked";

                    if($this->data['template']->save())
                    {
                        Activitylog::log([
                            'action' => 'UPDATED',
                            'type' => get_class($this->data['template']),
                            'link_id' => $this->data['template']->id,
                            'description' => 'Template updated',
                            'notes' => Auth::user()->username . " has ".$messageTXT." the template &quot;" . $this->data['template']->name . "&quot;"
                        ]);

                        return Redirect::to('hummingbird/templates/')->with('success', "Template has been {$messageTXT}");
                    }

                    return Redirect::to('hummingbird/templates/')->with('error', 'There was a problem adjusting the lock of this template');
                }

                Breadcrumbs::for("hummingbird/templates/{$this->data['template']->id}/edit", function ($breadcrumbs)  {
                    $breadcrumbs->parent('hummingbird/templates');
                    $breadcrumbs->push('Edit: ' . $this->data['template']->name, url('/hummingbird/templates/edit/' . $this->data['template']->id));
                });

                $this->data['tag'] = 'Edit '.$this->data['template']->title;
                return View::make('HummingbirdBase::cms/templates-new-edit', $this->data);                
            }

            return Redirect::to('hummingbird/templates/')->with('error', 'Template does not exist.');
        }

        return $this->forbidden(true);
    }
    
    public function update($id)
    {
        if($this->user->userAbility('template.update')) {
            $input = $this->Request->all();

            try {
                $template = Template::microsite()->locked()->where('id', $id)->firstOrFail();
                $template->fill($input);
                
                if( $template->save() ) {
                    Activitylog::log([
                        'action' => 'UPDATED',
                        'type' => get_class($template),
                        'link_id' => $template->id,
                        'description' => 'Template updated',
                        'notes' => Auth::user()->username . " has updated the template &quot;" . $template->name . "&quot;"
                    ]);

                    return Redirect::route('hummingbird.templates.edit', $id)->with('success', 'Template has been updated.');
                }

                return Redirect::route('hummingbird.templates.edit', $id)->with('error', 'There was a problem updating the template');
            }
            catch(\Exception $e) {
                // silence
            }

            return Redirect::route('hummingbird.templates.index')->with('error', 'There was a problem loading the template');
        }

        return $this->forbidden(true);
    }

    public function store()
    {
        if($this->user->userAbility('template.create')) {
            $input = $this->Request->except('_token', 'add'); 

            $validator = Validator::make($input, Template::$rules);

            if($validator->fails())
            {
                return Redirect::to('hummingbird/templates')->withErrors($validator)->withInput();
            }

            $template = (new Template)->fill($input);

            if(isset($this->data['Microsites']) AND null !== $this->data['Microsites'] AND null !== $this->data['microsite']) $template->microsite_id = $this->data['microsite']->id;
            
            if(!$template->save()) return Redirect::route('hummingbird.templates.index')->withErrors($template->errors());

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($template),
                'link_id' => $template->id,
                'description' => 'A new template has been created',
                'notes' => Auth::user()->username . " created a page template"
            ]);

            return Redirect::route('hummingbird.templates.edit', $template->id);
        }

        return $this->forbidden(true);
    }
    
    public function destroy($id)
    {
        if( $this->user->userAbility('template.delete') ) {
            try {
                $template = Template::microsite()->locked()->where('id', $id)->firstOrFail();
                $template->delete();

                Activitylog::log([
                    'action' => 'DELETED',
                    'type' => get_class($template),
                    'link_id' => null,
                    'description' => 'Template deleted',
                    'notes' => Auth::user()->username . " deleted the template &quot;{$template->name}&quot;"
                ]);

                return Redirect::to('hummingbird/templates')->with('success', 'Template has been deleted.');
            }
            catch(\Exception $e ){
                // silence
            }

            return Redirect::to('hummingbird/templates')->with('error', 'Template could not be deleted.');
        }

        return $this->forbidden(true);
    }

    public function cloneTemplate($id) {
        if($this->user->userAbility('template.create')) {
            try {
                $Template = Template::findOrFail($id);
                $NewTemplate = $Template->replicate();
                $NewTemplate->locked = 0;
                $NewTemplate->name .= " (cloned)";

                if($NewTemplate->save()) {
                    Activitylog::log([
                        'action' => 'CREATED',
                        'type' => get_class($NewTemplate),
                        'link_id' => $NewTemplate->id,
                        'description' => 'Template cloned',
                        'notes' => Auth::user()->username . " cloned the page template #{$Template->id}"
                    ]);

                    return Redirect::to('hummingbird/templates/')->with('success', 'Template has been cloned.');
                }
            }
            catch (\Exception $e) {
                return Redirect::back()->with('error', 'Unable to clone this template.');
            }
        }

        return $this->forbidden(true);
    }

    public function returnTemplateHTML($id)
    {
        $data = array();

        if(is_numeric($id) AND $id > 0)
        {
            $template = Template::where('id', '=', $id)->microsite()->whereNull('deleted_at')->take(1)->get();

            if(count($template) == 1)
            {
                $data = array(
                    'state' => true,
                    'message' => $template[0]->html
                );
            }
            else
            {
                $data = array(
                    'state' => false,
                    'message' => 'Template could not be found'
                );
            }
        }
        else
        {
            $data = array(
                'state' => false,
                'message' => 'Template could not be found'
            );
        }

        return json_encode($data);
    }
}
