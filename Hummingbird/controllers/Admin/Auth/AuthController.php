<?php namespace Hummingbird\Controllers\Admin\Auth;

use App;
use Auth;
use Redirect;
use Validator;
use Carbon\Carbon;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Validation\Rules\Password as PasswordRules;

/**
 * Hummingbird AuthController
 *
 * @package Hummingbird
 * @author Daryl Phillips
 *
 */
class AuthController extends CmsbaseController {
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Display the new password reminder form
     */
    public function password_reminder() {
        return view('HummingbirdBase::cms.account.forgotten');
    }

    /**
     * Send forgotten password for Hummingbird3 administrators
     */
    public function password_reminder_process( Request $request ) {
        $Validator = Validator::make($request->all(), ['email' => 'required|email']);

        if( !$Validator->passes() ):
            return Redirect::route('hummingbird.reset-password')
                ->withInput( $request->except('password') )
                ->withErrors( $Validator->errors() )
                ->with('error', 'There was a problem requesting a password');
        endif;

        try {
            $User = User::admin()->where('email', $request->get('email'))->firstOrFail();

            // We will send the password reset link to this user. Once we have attempted
            // to send the link, we will examine the response then see the message we
            // need to show to the user. Finally, we'll send out a proper response.
            $response = $this->broker()->sendResetLink(
                $request->all('email')
            );

            switch ($response) {
                case Password::RESET_LINK_SENT:
                    return redirect()->route('hummingbird.reset-password')->with('status', trans($response))->with('success', 'If your account exists an email will be sent to you. Please check your junk if it does not arrive in your inbox.');
                    break;
                case Password::INVALID_USER:
                default:
                    return redirect()->route('hummingbird.reset-password')->withErrors(['email' => trans($response)])
                        ->with('error', 'There was a problem resetting your password. Please try again.');
            }
        }
        catch(\Exception $e) { }
        
        return Redirect::route('hummingbird.reset-password')
            ->with('error', 'There was a problem requesting a new password');
    }


    public function getNew_account($remember_token) {
        try {
            $this->data['user'] = User::where('remember_token', $remember_token)->whereNotNull('force_password_reset')->firstOrFail();

            return view('HummingbirdBase::cms/users.new-account', $this->data);
        }
        catch(\Exception $e) {
            return Redirect::route('hummingbird.login');
        }
    }

    /**
     * We're saving the new password for this account.
     * Check we can access before updating.
     * 
     * @param  String $token
     */
    public function postNew_account(Request $request, $token) {
        try {
            $user = User::where('remember_token', $token)->whereNotNull('force_password_reset')->firstOrFail();

            $Validator = Validator::make( $request->all(), [
                'firstname' => 'required',
                'surname' => 'required',
                'password' => ['required', PasswordRules::defaults(), 'confirmed']
            ]);

            if( $Validator->fails() ):
                return Redirect::route('hummingbird.new-account.setup', $token )
                    ->withInput()
                    ->withErrors( $Validator )
                    ->with('error', 'There was a problem with your inputs. Please review.');
            endif;

            $user->fill( $request->all() );
            $user->status = User::APPROVED;
            $user->remember_token = NULL;
            $user->force_password_reset = NULL;
            
            if( !$user->save() ) {
                return Redirect::route('hummingbird.new-account.setup', [$user->remember_token])
                    ->with('error', 'There was a problem with your inputs. Please review.');
            }

            return Redirect::route('hummingbird.login')->with('success', 'Password has been saved. You can now login.');
        }
        catch(\Exception $e) {
            return Redirect::route('hummingbird.login');
        }
    }
}
