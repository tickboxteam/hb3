<?php namespace Hummingbird\Controllers\Admin\Auth;

use App;
use Auth;
use Redirect;
use Validator;
use Carbon\Carbon;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\User;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Validation\Rules\Password as PasswordRules;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

/**
 * Hummingbird AuthController
 *
 * @package Hummingbird
 * @author Daryl Phillips
 *
 */
class ResetPasswordController extends CmsbaseController {
    use ResetsPasswords;

    /**
     * User has a valid key, lets set their new password
     */
    public function new_password($token = NULL) {
        if( !empty($token) ) {
            return view('HummingbirdBase::cms.account.forgotten-reset', ['token' => $token]);
        }

        return Redirect::to('/');
    }

    /**
     * Validate credentials and then store the new password
     */
    public function store_new_password( Request $request ) {
        if( !$request->filled('token') ) {
            return Redirect::to('/');
        }

        $validator = Validator::make($request->all(), ['email' => 'required|email', 'password' => ['sometimes', PasswordRules::defaults(), 'confirmed']]);

        if( $validator->passes() ) {
            try {
                // From: Illuminate\Foundation\Auth\ResetPasswords
                // Here we will attempt to reset the user's password. If it is successful we
                // will update the password on an actual user model and persist it to the
                // database. Otherwise we will parse the error and return the response.
                $response = $this->broker()->reset(
                    $this->credentials($request), function ($user, $password) {
                        $this->resetPassword($user, $password);
                    }
                );

                // If the password was successfully reset, we will redirect the user back to
                // the application's home authenticated view. If there is an error we can
                // redirect them back to where they came from with their error message.
                switch ($response) {
                    case Password::PASSWORD_RESET:
                        return redirect()->route('hummingbird.login-validate')->with('success', 'Your password has now been reset.');
                        break;
                    default:
                        return redirect()->route('hummingbird.reset-password.new-password', $request->get('token'))
                            ->withInput( $request->all('email') )
                            ->withErrors(['email' => trans($response)]);
                        break;
                }
            }
            catch(\Exception $e) {}
        }

        return Redirect::route('hummingbird.reset-password.new-password', $request->get('token'))
            ->withInput( $request->all('email') )
            ->withErrors( $validator )
            ->with('error', 'There was a problem resetting your password. Please try again.');
    }
}
