<?php namespace Hummingbird\Controllers\Admin;

use ArrayHelper;
use Redirect;
use View;
use Brotzka\DotenvEditor\DotenvEditor;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

/**
 * 
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class SystemSettingsController extends CmsbaseController {
	public function __construct(Request $Request) {
		parent::__construct($Request);

		if( !Breadcrumbs::exists('hummingbird/settings') ):
	        Breadcrumbs::for('hummingbird/settings', function ($breadcrumbs) { 
	            $breadcrumbs->parent('HummingbirdBreadcrumbs');
	            $breadcrumbs->push('Settings', url('hummingbird/settings'));
	        });
	    endif;
	}

	/**
	 * Show all the settings
	 */
	public function index() {
       	$this->data['SystemSettings'] = Setting::where(['key' => 'system'])->first();

       	if( is_null($this->data['SystemSettings']) ):
			$this->data['SystemSettings'] = Setting::create(['key' => 'system', 'value' => serialize([])]);
       	endif;

		return View::make('HummingbirdBase::cms.settings', $this->data);
	}

    /**
     * Update the system settings
     */
    public function update() {
        $Settings = [];

        // New MessageBag
        $errorMessages = new \Illuminate\Support\MessageBag;

        // Debug settings
        $APP_DEBUG = ( $this->Request->get('APP_DEBUG') == 'true' ) ? 'true' : 'false';

        // Restricted settings
        $Settings['APP_RESTRICTED'] = ( $this->Request->get('APP_RESTRICTED') == 'true' ) ? 'true' : 'false';
        $Settings['APP_RESTRICTED_IPS'] = '';

       	if( $this->Request->filled('APP_RESTRICTED_IPS') && trim($this->Request->get('APP_RESTRICTED_IPS')) != '' ) {
       		$InputRestrictIPs = ArrayHelper::cleanExplodedArray( explode(",", $this->Request->get('APP_RESTRICTED_IPS')) );
			$RestrictIPs = array_filter($InputRestrictIPs, function( $IP_address ) {
				return filter_var($IP_address, FILTER_VALIDATE_IP);
			});

			if( count($RestrictIPs) < count($InputRestrictIPs) ):
				$errorMessages->add("APP_RESTRICTED_IPS", "We've cleaned invalid IP addresses");
			endif;

			if( count($RestrictIPs) > 0 ):
				$Settings['APP_RESTRICTED_IPS'] = implode(",", array_unique($RestrictIPs));
			endif;
       	}

       	$Settings['APP_RESTRICTED_REDIRECT_TO'] = ( $this->Request->get('APP_RESTRICTED_REDIRECT_TO') != '' && filter_var($this->Request->get('APP_RESTRICTED_REDIRECT_TO'), FILTER_VALIDATE_URL) ) ? $this->Request->get('APP_RESTRICTED_REDIRECT_TO') : NULL;

        // CMS: Access Restriction settings
        $Settings['APP_BACKEND_RESTRICTED'] = ( $this->Request->get('APP_BACKEND_RESTRICTED') == 'true' ) ? 'true' : 'false';
        $Settings['APP_BACKEND_RESTRICTED_IPS'] = '';

       	if( $this->Request->filled('APP_BACKEND_RESTRICTED_IPS') && trim($this->Request->get('APP_BACKEND_RESTRICTED_IPS')) != '' ) {
       		$InputBackendRestrictIPs = ArrayHelper::cleanExplodedArray( explode(",", $this->Request->get('APP_BACKEND_RESTRICTED_IPS')) );
			$BackendRestrictIPs = array_filter($InputBackendRestrictIPs, function( $IP_address ) {
				return filter_var($IP_address, FILTER_VALIDATE_IP);
			});

			if( count($BackendRestrictIPs) < count($InputBackendRestrictIPs) ):
				$errorMessages->add("APP_BACKEND_RESTRICTED_IPS", "We've cleaned invalid IP addresses");
			endif;

			if( count($BackendRestrictIPs) > 0 ):
				$Settings['APP_BACKEND_RESTRICTED_IPS'] = implode(",", array_unique($BackendRestrictIPs));
			endif;
       	}

       	// Check / Set 2FA
       	$Settings['APP_2FA_FORCE'] = ( $this->Request->filled('APP_2FA_FORCE') && $this->Request->get('APP_2FA_FORCE') == 'true' ) ? 'true' : 'false';

       	$env = new DotenvEditor();
       	$envArr = $env->getContent();
       	if( !in_array('APP_DEBUG', $envArr) ):
       		$env->addData([
       			'APP_DEBUG' => ''
       		]);
       	endif;

        $env->changeEnv([
            'APP_DEBUG' 	=> $APP_DEBUG
        ]);

        $Settings['pass_length'] = ( $this->Request->has('pass_length') && $this->Request->get('pass_length') >= 8 ) ? $this->Request->get('pass_length') : 8;
        $Settings['letters'] = ( $this->Request->has('letters') && $this->Request->get('letters') == 'true' ) ? 'true' : 'false';
        $Settings['mixed_case'] = ( $this->Request->has('mixed_case') && $this->Request->get('mixed_case') == 'true' ) ? 'true' : 'false';
        $Settings['pass_numbers'] = ( $this->Request->has('pass_numbers') && $this->Request->get('pass_numbers') == 'true' ) ? 'true' : 'false';
        $Settings['pass_symbols'] = ( $this->Request->has('pass_symbols') && $this->Request->get('pass_symbols') == 'true' ) ? 'true' : 'false';
        $Settings['pass_compromised'] = ( $this->Request->has('pass_compromised') && $this->Request->get('pass_compromised') == 'true' ) ? 'true' : 'false';
        
        Setting::updateOrCreate([
            'key' => 'system'
        ], [
            'value' => serialize( $Settings )
        ]);

        if( !$this->Request->has('pass_length') || $this->Request->get('pass_length') < 8 ) {
            $errorMessages->add("pass_length", "Password length must be a minimum of 8 characters.");
        }

        return Redirect::route('hummingbird.settings.index', $this->getReferrerParameters())
            ->with('success', 'Website settings updated')
            ->with('error', ( count($errorMessages) ) ? 'There was a problem with some of the settings' : NULL)
            ->withErrors( $errorMessages );
  	}
}