<?php namespace Hummingbird\Controllers\Admin;

use App;
use ArrayHelper;
use Auth;
use General;
use Redirect;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

/**
 * Plugin Controller to handle the outputting and management buttons of plugins
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class PluginController extends CmsbaseController
{
	public function __construct(Request $Request)
	{
		parent::__construct($Request);

		if( !Breadcrumbs::exists('hummingbird/plugins') ):
	        Breadcrumbs::for('hummingbird/plugins', function ($breadcrumbs) { 
	            $breadcrumbs->parent('HummingbirdBreadcrumbs');
	            $breadcrumbs->push('Plugins', url('hummingbird/plugins'));
	        });
	   	endif;
	}


	/**
	 * Display index of plugins
	 *
	 * @return View
	 */
	public function index()
	{
		$this->data['tag'] = 'Manage Plugins';

		return View::make('HummingbirdBase::cms.plugins/plugins', $this->data);
	}


	/**
	 * Processing the plugin request
	 *
	 */
	public function plugin_update() 
	{
		$input 		= $this->Request->all();
		$namespace 	= $input['package'];
		$package	= ArrayHelper::cleanExplodedArray(explode("\\", $namespace));
		$vendor		= $package[0];
		$package 	= array_pop($package);

		try
		{
			switch($input['action'])
			{
				case 'activate':
					$this->data['PluginManager']->install($vendor, $package);
					
					return Redirect::to(App::make('backend_url').'/plugins/')->with('success', "Plugin $namespace has been installed.");

					break;
				case 'deactivate':
					$this->data['PluginManager']->deactivate_plugin($vendor, $package);

					return Redirect::to(App::make('backend_url').'/plugins/')->with('success', "Plugin $namespace has been deactivated.");
					break;
				case 'delete':
					$this->data['PluginManager']->uninstall($vendor, $package);

					return Redirect::to(App::make('backend_url').'/plugins/')->with('success', "Plugin $namespace has been uninstalled.");
					break;
				case 'refresh':
					$this->data['PluginManager']->refresh($vendor, $package);

					return Redirect::to(App::make('backend_url').'/plugins/')->with('success', "Plugin $namespace has been refreshed.");
					break;
			}
		}
		catch (Exception $e)
		{
			return Redirect::to(App::make('backend_url').'/plugins/')->withErrors([$e->getMessage()]);
		}
	}
}
