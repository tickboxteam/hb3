<?php namespace Hummingbird\Controllers;

use App;
use Redirect;
use View;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Hummingbird\Controllers\CmsbaseController;
use Hummingbird\Libraries\Themes;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class ThemeController extends CmsbaseController {
    public $code_location = 'themes';

    public function __construct(Request $Request) {
        parent::__construct($Request);

        if( !Breadcrumbs::exists('hummingbird/themes') ):
            Breadcrumbs::for('hummingbird/themes', function ($breadcrumbs) { 
                $breadcrumbs->parent('HummingbirdBreadcrumbs');
                $breadcrumbs->push('Themes', url('hummingbird.themes.index'));
            });
        endif;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $this->data['tag'] = 'Manage Themes';
        $this->data['themes'] = Themes::all();
        $this->data['activeTheme'] = Themes::activeTheme();

        return View::make('HummingbirdBase::cms/themes', $this->data);
    }
    
    /**
     * Activate said theme
     * @param  String $theme
     */
    public function activate($theme) {
        Themes::activate($theme);
        Return Redirect::route('hummingbird.themes.index');
    }
}
