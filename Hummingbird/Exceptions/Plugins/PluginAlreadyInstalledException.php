<?php 
namespace Hummingbird\Exceptions\Plugins;

/**
 * Custom PluginAlreadyInstalledException Exception
 *
 * @package Hummingbird\Exceptions
 * @author Daryl Phillips
 * @copyright  2017 Tickbox Marketing Ltd
 * @since      Class available since Release 0.0.0
 */ 
class PluginAlreadyInstalledException extends \Exception
{
    /**
     * Constructor PluginAlreadyInstalledException
     *
     */
    public function __construct($message = "Plugin already installed.", $code = 0, Exception $previous = null)
    {
        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);

        $this->status_code = 400;
    }
    
	public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}