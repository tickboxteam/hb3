<?php namespace Hummingbird\Facades;

use Illuminate\Support\Facades\Facade;

class BackendMenu extends Facade
{
    /**
     * Get the registered name of the component.
     * 
     * Resolves to:
     * 
     * @return string
     */
    protected static function getFacadeAccessor() { return 'cms.navigation'; }
}
