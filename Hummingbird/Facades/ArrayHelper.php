<?php namespace Hummingbird\Facades;

use Illuminate\Support\Facades\Facade;

class ArrayHelper extends Facade
{
    /**
     * Get the registered name of the component.
     * 
     * Resolves to:
     * 
     * @return string
     */
    protected static function getFacadeAccessor() { return 'array.helper'; }
}
