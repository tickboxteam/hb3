<?php namespace Hummingbird\Handlers;

use Request;
use Hummingbird\Models\Searcher;
use Hummingbird\Interfaces\Searchable;

class SearchableHandler
{
    /**
    * Search and return items associated with this search
    *
    * @return Collection 
    */
    public function search($words = '')
    {
        if(Request::filled('s'))
            $words = trim(Request::get('s'));
        
        if(Request::filled('search'))
            $words = trim(Request::get('search'));

        return Searcher::search($words);
    }


    /**
    * Add or update the given searchable subject
    *
    * @param Searchable $subject
    * @throws InvalidArgumentException
    */
    public function insertOrUpdateSearch(Searchable $subject)
    {
        if ($subject instanceof Searchable) {
            if($subject->isSearchable()) {
                Searcher::updateOrCreate([
                    'item_id'   => $subject->getSearchableId(),
                    'type'      => $subject->getSearchableType()
                ], [
                    'item_id'   => $subject->getSearchableId(),
                    'title'     => $subject->getSearchableTitle(),
                    'url'       => $subject->getSearchableSlug(),
                    'summary'   => $subject->getSearchableSummary(),
                    'content'   => $subject->getSearchableBody(),
                    'type'      => $subject->getSearchableType(),
                    'weight'    => $subject->getSearchableWeight()
                ]);
            }
            else {
                $this->removeFromSearch($subject);
            }

            return;
        }

        throw new InvalidArgumentException('Subject must be a searchable or array of searchables');
    }


    /**
    * Remove the given subject from the search index.
    *
    * @param Searchable $subject
    */
    public function removeFromSearch(Searchable $subject)
    {
        Searcher::where('item_id', $subject->getSearchableId())->where('type', $subject->getSearchableType())->delete();
    }


    /**
    * Remove everything from the index.
    *
    * @return Void
    */
    public function clearSearch()
    {
        Searcher::truncate();
    }
}