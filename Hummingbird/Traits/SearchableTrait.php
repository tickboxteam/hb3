<?php namespace Hummingbird\Traits;

use General;
use Request;

trait SearchableTrait {
    /**
     * Return whether this element is searchable
     *
     * @return Boolean
     */
    public function isSearchable() {
        return true;
    }

    /**
     * Return the id of the searchable subject.
     *
     * @return string
     */
    public function getSearchableId() {
        return $this->id;
    }


    /**
     * Return the title of the searchable subject
     *
     * @return string
     */
    public function getSearchableTitle() {
        return $this->title;
    }


    /**
     * Return the slug of the searchable subject
     *
     * @return string
     */
    public function getSearchableSlug() {
        return $this->permalink;
    }


    /**
     * Returns the article summary that must be indexed
     *
     * @return array
     */
    public function getSearchableSummary() {
        return $this->summary;
    }


    /**
     * Returns the article body that must be indexed
     *
     * @return array
     */
    public function getSearchableBody() {
        return $this->content;
    }


    /**
     * Return the type of the searchable subject.
     *
     * @return string
     */
    public function getSearchableType() {
        return get_class($this);
    }


    /**
     * Return the type of the searchable subject.
     *
     * @return string
     */
    public function getSearchableWeight() {
        if(Request::filled('weight'))
            return (int) Request::get('weight');

        return 1;
    }


    /**
     * Return the searchable_image if there is one set
     *
     * @return String
     */
    public function getSearchableImage() {
        if(!empty($this->searchable_image))
            return $this->searchable_image;

        return NULL;
    }


    /**
     * Check if the resource has a searchable_image
     *
     * @return Boolean
     */
    public function hasSearchableImage($fields_to_search = array('search_image', 'featured_image', 'content')) {
        if(null === $this->searchable_image) {
            $this->searchable_image = General::findImage($this, $fields_to_search);
        }

        return !empty($this->searchable_image);
    }
}