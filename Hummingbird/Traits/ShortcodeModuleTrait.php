<?php namespace Hummingbird\Traits;

use Hummingbird\Libraries\ModuleParser;
use Hummingbird\Models\Module;
use Hummingbird\Models\Moduletemplate;

/**
 * This is added to the FrontendControllers which allow us to output content from shortcodes
 * 
 * @package Hummingbird\Traits
 */
trait ShortcodeModuleTrait {   
    /**
     * Render modules/shortcodes into blog content
     * @param  string $html
     * @return string
     */
    protected function render_shortcodes_modules($html = '') {
        // FIXME: Could be better/automatic on page
        if(strpos($html, "[HB::PAGE-NAVIGATION]") !== FALSE)
        {
            $html = str_replace("[HB::PAGE-NAVIGATION]", $this->side_navGenerator($this->data['page']), $html);
        }

        if(strpos($html, '[HB::MODULE') !== FALSE)
        {
            $modules = array();

            $find_modules_pattern = '#\[HB::MODULE\((.*?)\)\]#';
            preg_match_all($find_modules_pattern, $html, $modules);

            if(isset($this->microsite) AND null !== $this->microsite AND $this->microsite->type == 'standard') $this->Microsites->purgeDBConnection();
            foreach($modules[1] as $key => $selected_module)
            {
                $module = Module::find($selected_module);

                if(null !== $module AND $module->template())
                {
                    $template = Moduletemplate::find($module->moduletemplate_id); //get template
                    $ModuleBuilder = new ModuleParser($template, $module, $this->data['page']); //build the ModuleParser

                    $html = str_replace($modules[0][$key], $ModuleBuilder->render(), $html);
                }
                else
                {
                    /* Standard HTML Modules - no template */
                    $mod_html = (null !== $module) ? $module->module_data['structure']:'';
                    $html = str_replace($modules[0][$key], $mod_html, $html);
                }
            }

            $find_modules_pattern = '#\[HB::MODULETEMPLATE\((.*?)\)\]#';
            preg_match_all($find_modules_pattern, $html, $modules);
            
            foreach($modules[1] as $key => $selected_module)
            {   
                $selected_module    = (int) $selected_module;
                $modules            = Module::where('moduletemplate_id', $selected_module)->whereNull('deleted_at')->orderBy('name', 'ASC')->get();
                $html_to_store      = '';

                foreach($modules as $module)
                {
                    if(null !== $module AND $module->template())
                    {
                        $ModuleBuilder = new ModuleParser($module->moduletemplate, $module); //build the ModuleParser
                        $html_to_store .= $ModuleBuilder->render();
                    }
                }

                $html = str_replace("[HB::MODULETEMPLATE($selected_module)]", $html_to_store, $html); 
            }

            if(isset($this->microsite) AND null !== $this->microsite AND $this->microsite->type == 'standard') $this->Microsites->purgeDBConnection($this->Microsites->new_prefix);
        }

        return $html;
    }
}
