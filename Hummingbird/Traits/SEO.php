<?php namespace Hummingbird\Traits;

use ArrayHelper;
use Config;
use General;
use OpenGraph;
use HB3Meta;

// TODO: Allow fields to be overriden - handy if certain things can't be done in main class
//      setSeoTitle()
//      setSeoDescription()
//      setSeoImage()
//          setOpenGraphImage()
//          setTwitterImage()
//      setArticleModifiedTime()
//      setArticlePublishedTime()

/**
 * Seo Trait to allow us to set the correct meta data content for all content on site
 *
 * @since   1.1.16
 * @version 1.1
 */
trait SEO {
    public function registerSEO( $article ) {
        $ArticleImage = General::findImage($article, ['featured_image', 'search_image', 'content']);

        HB3Meta::setTitle( $article->getSeoTitle() );

        $Description = $article->getSeoDescription();
        
        if( trim($Description) != '' ):
            HB3Meta::setDescription( trim($Description) );
        endif;

        if( $article->hasSeoRobots() ):
            HB3Meta::addMeta('robots', $article->getSeoRobots());
        endif;

        // Facebook SEO
        OpenGraph::setTitle( $article->getFacebookSeoTitle() );
        OpenGraph::setUrl( $article->getCanonicalURL() );

        $FacebookDesc = $article->getFacebookSeoDescription();
        OpenGraph::setDescription( (!empty($FacebookDesc)) ? $FacebookDesc : HB3Meta::getDescription() );
        
        $FacebookImage = $article->getFacebookSeoImage();
        $FacebookImageToUse = (!empty($FacebookImage)) ? $FacebookImage : $ArticleImage;
        
        if( $FacebookImageToUse ) {
            $ImageUrlArray = parse_url($FacebookImageToUse);
            $ImageUrl = (isset($ImageUrl['host'])) ? $FacebookImageToUse : url($FacebookImageToUse);
            OpenGraph::addImage( $ImageUrl );
        }

        // Twitter Card SEO
        HB3Meta::addMeta('twitter:title', $article->getTwitterSeoTitle() );

        $TwitterDesc = $article->getTwitterSeoDescription();
        HB3Meta::addMeta('twitter:description', (!empty($TwitterDesc)) ? $TwitterDesc : HB3Meta::getDescription() );
        HB3Meta::addMeta('twitter:card', $article->getTwitterCardSEOType());

        $TwitterImage = $article->getTwitterSeoImage();
        $TwitterImageToUse = (!empty($TwitterImage)) ? $TwitterImage : $ArticleImage;

        if( $TwitterImageToUse ) {
            $ImageUrlArray = parse_url($TwitterImageToUse);
            $ImageUrl = (isset($ImageUrl['host'])) ? $TwitterImageToUse : url($TwitterImageToUse);
            HB3Meta::addMeta('twitter:image', $ImageUrl );
        }
        
        // Other SEO
        HB3Meta::addMeta('article:modified_time', $article->updated_at->format('c'));
        HB3Meta::addMeta('article:published_time', $article->updated_at->format('c'));
        HB3Meta::addCanonicalLink( $article->getCanonicalURL() );

        // Forcing the site to ignore being indexable via Search engines
        if( Config::get('app.debug') || env('SEARCH_ENGINE_DISCOURAGE') == true ) {
            HB3Meta::addMeta('robots', 'noindex, nofollow');
        }
    }

    public function registerCanonicalLink($article, $link) {
        if( !$article->hasCanonicalURL() ):
            HB3Meta::addCanonicalLink( $link );
        endif;
    }
}
