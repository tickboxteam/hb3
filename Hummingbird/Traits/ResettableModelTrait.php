<?php namespace Hummingbird\Traits;

/**
 * A Resettable trait that allows to clean and prepare models for updates, 
 * rather than having to set them to NULL manually
 *
 * @since   1.1.30
 */
trait ResettableModelTrait {
    /**
     * Abilty to reset all fields as specified (or fillable fields) to a default value.
     * Initial version fields must be nullable
     *
     * @since   1.1.30
     * @version 1.0
     */
    public function reset() {
        if( $this->hasResettableFields() ) {
            $this->fillable = $this->getResettableFields();
        }

        // TODO: Allow fields to be merged from Fillable and Resettable
        // TODO: Allow fields to be excluded from the above merge

        $this->fill( array_fill_keys($this->getResettableFields(), NULL) );

        return $this;
    }

    /**
     * Do we have any resettable fields, that aren't defaults
     *
     * @since   1.1.30
     * @version 1.0
     */
    public function hasResettableFields() {
        return ( isset($this->resettable) && is_array($this->resettable) && count($this->resettable) > 0 );
    }

    /**
     * Return the fields that we want to reset.
     * Custom fields can be defined inside "$resettable".
     * No custom fields (or if left empty), will use the $fillable fields.
     *
     * @since   1.1.30
     * @version 1.0
     */
    public function getResettableFields() {
        return ( $this->hasResettableFields() ) ? $this->resettable : $this->getFillable();
    }
}
