<?php

namespace Hummingbird\Traits;

use DB;

/**
 * Trait helper for Categories and Tags
 *
 * @package Hummingbird
 * @author Daryl Phillips
 *
 */
trait TaxonomyHelper
{

    /**
     * Returns the type associated to a taxonomy type
     *
     * @return String $type
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Returns the number of items associated to this particular taxonomy type
     *
     * @return Integer
     */
    public function used()
    {
        return DB::table('taxonomy_relationships')->where('term_id', '=', $this->id)->count();
    }


    /**
     * Query scope for returning item by type
     *
     * @param 
     * @return Query
     */
    public function scopeType($query)
    {
        return $query->where('type', $this->type);
    }

    /**
     * Check/Set the description attribute
     */
    public function setDescriptionAttribute($value) {
        $value = trim($value);

        $this->attributes['description'] = ( $value != '' ) ? $value : NULL;
    }

    /**
     * Check/Set the image attribute
     */
    public function setImageAttribute($value) {
        $value = trim($value);

        $this->attributes['image'] = ( $value != '' ) ? $value : NULL;
    }

    /**
     * Check/Set the name attribute
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = trim($value);
    }

    /**
     * Allow taxonomy items to have a custom display name

     * @return String
     */
    public function displayName() {
        if( isset($this->display_name) && !empty($this->display_name) ) {
            return $this->display_name;
        }

        return $this->name;
    }

    /**
     *
     *
     */
    public function relations($type = null, $all = true)
    {
        $this_q = DB::table('taxonomy_relationships')->where('term_id', '=', $this->id);

        $return = ($all) ? $this_q->get():false;

        return (null !== $type) ? $this_q->where('tax_type', $type)->get():$return;
    }


    /**
     * Truncate all the relationships associated to categories
     *
    */
    public function truncate_relationships()
    {
        DB::table('taxonomy_relationships')->where('term_id', $this->id)->delete();
    }

    /**
     * Get all categories with the same Permalink (inc. trashed)
     */
    public function scopeTaxonomyItemsWithDuplicatePermalinks($query) {
        return $query->selectRaw('slug, COUNT(*) as Duplicates')->withTrashed()->type()->groupBy('slug')->having('Duplicates', '>', 1);
    }
}
