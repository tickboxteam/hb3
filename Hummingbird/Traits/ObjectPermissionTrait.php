<?php namespace Hummingbird\Traits;

use Auth;

trait ObjectPermissionTrait
{
    public function scopeObjectPermissions($query) {
        if(Auth::user() && !Auth::user()->isSuperUser() && Auth::user()->objectPermissions()->where('item_type', static::class)->count() > 0) {
            $query->whereIn('id', Auth::user()->objectPermissions()->where('item_type', static::class)->pluck('item_id')->all());
        }

        return $query;
    }
}
