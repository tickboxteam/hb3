<?php namespace Hummingbird\Traits;

trait HB3MigratorTrait {
    /**
     * Import all elements
     */
    public function process_import() {
        $this->importDatabase();

        $this->process_elements();

        $this->rollback();
    }

    /**
     * Process request to all the elements we want to import
     */
    public function process_elements() {
        // silence is golden - as this handled by services importing
    }
}