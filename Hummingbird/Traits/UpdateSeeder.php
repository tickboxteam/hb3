<?php 

namespace Hummingbird\Traits;

trait UpdateSeeder
{
    public function __construct($current_version, $latest_version, $versions, $behind)
    {
        $this->current_version  = (null === $current_version) ? array_keys($versions)[0]:$current_version;
        $this->latest_version   = $latest_version;
        $this->versions_behind  = $behind;
        $this->all_versions     = $versions;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($callback = 'run')
    {
        foreach($this->all_versions as $version_key => $version)
        {
            $version_key = str_replace(".", "", "v".$version_key);

            if(class_exists($version_key))
            {
                $obj = new $version_key;

                if(method_exists($obj, $callback)) $obj->$callback();
            }          
        }
    }
}
