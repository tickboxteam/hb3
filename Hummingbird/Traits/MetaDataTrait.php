<?php namespace Hummingbird\Traits;

use File;
use Request;

use Hummingbird\Models\MetaData;

trait MetaDataTrait {
    /**
     * Get all related meta-data items associated to a particular item_type
     */
    public function meta() {
        return $this->hasMany(MetaData::class, 'item_id', 'id')->where('item_type', self::class);
    }

    /**
     * Get the meta_title for the item
     */
    public function metaTitle() {
        return $this->meta()->where('key', 'meta_title')->first();
    }

    /**
     * Get the meta_description for the item
     */
    public function metaDescription() {
        return $this->meta()->where('key', 'meta_description')->first();
    }

    /**
     * Get the meta_robots for this item
     */
    public function metaRobots() {
        return $this->meta()->where('key', 'meta_robots')->first();
    }

    /**
     * Get the meta_canonical for this item
     */
    public function metaCanonicalURL() {
        return $this->meta()->where('key', 'meta_canonical')->first();
    }

    /**
     * Get the meta_facebook_title for this item
     */
    public function metaFacebookTitle() {
        return $this->meta()->where('key', 'meta_facebook_title')->first();
    }

    /**
     * Get the meta_facebook_description for this item
     */
    public function metaFacebookDescription() {
        return $this->meta()->where('key', 'meta_facebook_description')->first();
    }

    /**
     * Get the meta_facebook_image for this item
     */
    public function metaFacebookImage() {
        return $this->meta()->where('key', 'meta_facebook_image')->first();
    }

    /**
     * Get the meta_facebook_title for this item
     */
    public function metaTwitterTitle() {
        return $this->meta()->where('key', 'meta_twitter_title')->first();
    }

    /**
     * Get the meta_facebook_description for this item
     */
    public function metaTwitterDescription() {
        return $this->meta()->where('key', 'meta_twitter_description')->first();
    }

    /**
     * Get the meta_facebook_image for this item
     */
    public function metaTwitterImage() {
        return $this->meta()->where('key', 'meta_twitter_image')->first();
    }

    /**
     * Get the SEO Title for this item
     */
    public function getSeoTitle() {
        return ( $this->metaTitle() && !empty( $this->metaTitle()->value ) ) ? $this->metaTitle()->value : $this->title;
    }

    /**
     * Get the SEO Description for this item
     * If no meta description, use summary or article content
     */
    public function getSeoDescription() {
        if( $this->metaDescription() !== NULL ) {
            if( !empty( $this->metaDescription()->value ) ) {
                return $this->metaDescription()->value;
            }
        }   

        if( !empty($this->summary) ) {
            return $this->summary;
        }

        $meta_length = 0;
        $metaDescription = '';

        if( !empty($this->content) ):
            $doc = new \DOMDocument();
            @$doc->loadHTML($this->content);

            foreach($doc->getElementsByTagName('p') as $para) {
                if( !empty($para) && strlen($para->textContent) > 0 && $meta_length <= 160 ):
                    $metaDescription .= trim($para->textContent);
                    $meta_length = strlen($metaDescription);
                endif;

                if( $meta_length >= 160 ) {
                    break;
                }
            }
        endif;
        
        if( !empty($metaDescription) ):
            return $metaDescription;
        endif;

        return substr(clean_html_and_shortcodes_from_string($this->content), 0, 160);
    }

    /**
     * Have we set any Robot based items for this article?
     * @return Boolean
     */
    public function hasSeoRobots() {
        return ( $this->metaRobots() && !empty($this->metaRobots()->value) );
    }

    /**
     * Get the robots, if set, for the article
     * @return Mixed
     */
    public function getSeoRobots() {
        if( $this->hasSeoRobots() ) {
            return $this->metaRobots()->value;
        }
    }

    /**
     * Do we have a legitimate URL to represent duplicate content?
     * @return boolean
     */
    public function hasCanonicalURL() {
        return ( $this->metaCanonicalURL() && !empty($this->metaCanonicalURL()->value) );
    }

    /**
     * Get the canonical URL
     * @return Override or default
     */
    public function getCanonicalURL() {
        $url = Request::url();

        if( $this->hasCanonicalURL() ) {
            $url = $this->metaCanonicalURL()->value;
        }

        return $url;
    }

    public function getFacebookSeoTitle() {
        if( $this->metaFacebookTitle() !== NULL && !empty($this->metaFacebookTitle()->value) ) {
            return $this->metaFacebookTitle()->value;
        }

        return $this->getSeoTitle();
    }

    public function getFacebookSeoDescription() {
        if( $this->metaFacebookDescription() !== NULL && !empty($this->metaFacebookDescription()->value) ) {
            return $this->metaFacebookDescription()->value;
        }
    }

    public function getFacebookSeoImage() {
        if( $this->metaFacebookImage() !== NULL && !empty($this->metaFacebookImage()->value) ) {
            $Image      = $this->metaFacebookImage()->value;
            $ImagePath  = parse_url($Image);

            return ( !isset($ImagePath['host']) && File::exists(storage_path('app/public') . $Image ) ) ? url($Image) : $Image;
        }

        if( isset($this->featured_image) && !empty($this->featured_image) ) {
            $ImagePath  = parse_url($this->featured_image);

            return ( !isset($ImagePath['host']) && File::exists(storage_path('app/public') . $this->featured_image ) ) ? url($this->featured_image) : $this->featured_image;
        }

        return NULL;
    }
    
    public function getTwitterSeoTitle() {
        if( $this->metaTwitterTitle() !== NULL && !empty($this->metaTwitterTitle()->value) ) {
            return $this->metaTwitterTitle()->value;
        }

        return $this->getSeoTitle();
    }

    public function getTwitterSeoDescription() {
        if( $this->metaTwitterDescription() !== NULL && !empty($this->metaTwitterDescription()->value) ) {
            return $this->metaTwitterDescription()->value;
        }
    }

    public function getTwitterSeoImage() {
        if( $this->metaTwitterImage() !== NULL && !empty($this->metaTwitterImage()->value) ) {
            $Image      = $this->metaTwitterImage()->value;
            $ImagePath  = parse_url($Image);

            return ( !isset($ImagePath['host']) && File::exists(storage_path('app/public') . $Image ) ) ? url($Image) : $Image;
        }

        if( isset($this->featured_image) && !empty($this->featured_image) ) {
            $ImagePath  = parse_url($this->featured_image);

            return ( !isset($ImagePath['host']) && File::exists(storage_path('app/public') . $this->featured_image ) ) ? url($this->featured_image) : $this->featured_image;
        }

        return NULL;
    }

    /**
     * Get the type of card for this article
     */
    public function getTwitterCardSEOType() {
        $TwitterCard = $this->meta()->where('key', 'meta_twitter_card')->first();
        $DefaultTwitterCard = (env('SEO_TWITTER_CARD') && in_array(env('SEO_TWITTER_CARD'), ['summary', 'summary_large_image']) ) ? env('SEO_TWITTER_CARD') : 'summary';

        return ( $TwitterCard && in_array($TwitterCard->value, ['summary', 'summary_large_image']) ) ? $TwitterCard->value : $DefaultTwitterCard;
    }
}
