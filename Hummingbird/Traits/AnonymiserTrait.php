<?php namespace Hummingbird\Traits;

/**
 * Trait class for allowing any model(s) to anonymise content
 */
trait AnonymiserTrait {
    /**
     * Get the referene we're going to replace the content with
     * @return String
     */
    public function getFieldReference() {
        if( isset($this->anonymiser) && isset($this->anonymiser['field_reference']) && !empty($this->anonymiser['field_reference']) ):
            return $this->anonymiser['field_reference'];
        endif;

        return '*DELETED*';
    }

    /**
     * Anonymise field content
     */
    public function anonymise() {

        $config = [
            'fields' => [],
            'field_reference' => '*DELETED*'
        ];

        $this->anonymiser = array_merge( $config, ( isset($this->anonymiser) ) ? $this->anonymiser : [] );

        if( isset($this->anonymiser) && isset($this->anonymiser['fields']) && is_array($this->anonymiser['fields']) && count($this->anonymiser['fields']) > 0 ):

            foreach( $this->anonymiser['fields'] as $Field ):
                $this->attributes[ $Field ] = $this->getFieldReference();
            endforeach;

            $this->save();
        endif;

        return $this;
    }
}