<?php namespace Hummingbird\Validators;

use Auth;

class HummingbirdValidators extends \Illuminate\Validation\Validator {
    public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array() )
    {
        parent::__construct( $translator, $data, $rules, $messages, $customAttributes );
        $this->implicitRules[] = studly_case('unique_custom');
    }

    /**
     * Unique validator added to allow us to check, when users are logged in,
     * that we can ignore their current ID. This does not replace 
     * "unique" but extends it.
     */
    public function validateUniqueLoggedIn($attribute, $value, $parameters, $validator) {
        if( !Auth::user() ) {
            return false;
        }

        $this->requireParameterCount(1, $parameters, 'unique_custom');

        $table = $parameters[0];

        // The second parameter position holds the name of the column that needs to
        // be verified as unique. If this parameter isn't specified we will just
        // assume that this column to be verified shares the attribute's name.
        $column = isset($parameters[1]) ? $parameters[1] : $attribute;

        list($idColumn, $id) = array(null, null);

        if (isset($parameters[2]))
        {
            list($idColumn, $id) = $this->getUniqueIds($idColumn, $parameters);

            if (strtolower($id) == 'null') $id = null;
        }

        // The presence verifier is responsible for counting rows within this store
        // mechanism which might be a relational database or any other permanent
        // data store like Redis, etc. We will use it to determine uniqueness.
        $verifier = $this->getPresenceVerifier();

        $extra = $this->getUniqueExtra($parameters);

        return $verifier->getCount(

            $table, $column, $value, Auth::user()->id, $idColumn, $extra

        ) == 0;
    }


    /**
     * Validate whether a telephone number is the correct format
     *
     * @return Boolean
     */
    public function validateValidPhoneNumber($attribute, $value, $parameters, $validator) {
        $this->requireParameterCount(1, $parameters, 'valid_phone_number');

        if( empty($value) || (!empty($value) && isset($parameters[1]) && (bool) !$parameters[1] ) )
            return true;
        
        return preg_match("/^([0-9\s\-\+\(\)]*)$/", $value);
    }
}