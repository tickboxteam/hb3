<?php namespace Hummingbird\Validators;

use Config;
use GuzzleHttp\Client;

/**
 * Validate whether a Google ReCaptcha input is valid
 *
 * reCAPTCHA score is checked to check for bots.
 * Where 0.9 - probably human, 0.1 - bot
 * Values 0.3 and below are considered as very suspicious activity, these users are interpreted as bots.
 */
class GoogleReCaptcha
{
    /**
     * Score for the Google reCATPCHA request we should validate against
     * @var Float
     */
    public $score;

    /**
     * Initialise Google reCAPTCHA validation
     */
    public function __construct() {
        $this->score = ( !empty(env('GOOGLE_RECAPTCHA_SCORE')) ) ? (float) env('GOOGLE_RECAPTCHA_SCORE') : 0.5;
    }

    /**
     * Validate this request
     */
    public function validate($attribute, $value, $parameters, $validator) {
        if( site_is_in_debug_mode() ):
            return true;
        endif;

        $client = new Client();
    
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            ['form_params'=>
                [
                    'secret'    => env('GOOGLE_RECAPTCHA_SECRET'),
                    'response'  => $value
                 ]
            ]
        );

        $body = json_decode((string)$response->getBody());

        // Version 2 - does not return a score, only a success state
        if( !isset($body->score) ) {
            return $body->success;
        }

        // Version 3 - returns both success and a score 
        return ( $body->success && ( $body->score > $this->score || empty($this->score) ) );
    }
}