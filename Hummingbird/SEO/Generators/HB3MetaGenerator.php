<?php namespace Hummingbird\SEO\Generators;

use Request;
use Artesaos\SEOTools\SEOMeta;

class HB3MetaGenerator extends SEOMeta {
    /**
     * The canonical URL (if set)
     *
     * @var string
     */
    protected $canonical_url;

    /**
     * Render the meta tags.
     *
     * @return string
     */
    public function generate($minify = false) {
        // FIXME: Not sure if the CSRF TOKEN IS THE BEST PLACE HERE
        $this->addMeta('csrf-token', csrf_token());

        $html = explode(PHP_EOL, parent::generate());

        $canonicalLink  = $this->getCanonicalLink();

        if( !empty($canonicalLink) ) {
            $canonicalLink = url($canonicalLink);

            if( substr($canonicalLink , -1) !='/') {
                $canonicalLink .= '/';
            }

            $html[] = sprintf('<link rel="canonical" href="%s" />', $canonicalLink, PHP_EOL);
        }

        return implode(PHP_EOL, $html);
    }

    /**
     * Set the Meta description. Without the limitation (of 160 characters for pages that don't have meta data)
     *
     * @param string $description
     */
    public function setDescription($description) {
        return parent::setDescription( $this->cleanShortcodesFromMeta( strip_tags($description) ) );
    }

    /**
     * Add a canonical link to the SEO
     * 
     * @param String $url
     * @param null $value
     * @param string $name
     */
    public function addCanonicalLink($url = NULL) {
        $this->canonical_url = (!empty($url)) ? $url : NULL;
    }

    /**
     * Get canonical link
     * @param string $name
     */
    public function getCanonicalLink() {
        return (!empty($this->canonical_url)) ? $this->canonical_url : NULL;
    }

    /**
     * Do we have Link? If so, is it the same as the current URL?
     * 
     * @param  String  $Link
     * @return boolean
     */
    protected function isCanonicalLinkInvalid( $Link ) {
        return trim($Link, "/") != Request::path();
    }


    /**
     * Adding the meta tags (cloned from Vinicius73\SEO\Generators\MetaGenerator)
     * We'll clean all shortcodes, via a new function, from the page SEO just in case.
     * 
     * @param Mixed     $meta
     * @param null      $value
     * @param string    $name
     * @version         1.0
     * @since           1.1.37
     */
    public function addMeta($meta, $value = null, $name = 'name') {
        parent::addMeta($meta, $this->cleanShortcodesFromMeta($value), $name);
    }


    /**
     * Add the meta refresh tag, for on page redirects
     * @param String $destination
     * @param Integer $duration
     */
    public function addMetaRefresh($destination, $duration = NULL) {
        if( !empty($destination) ):
            $duration = (!empty($duration)) ? $duration : (!empty(env('HTTP_EQUIV_REDIRECT')) ? env('HTTP_EQUIV_REDIRECT') : 3 );
            $this->addMeta('refresh', "{$duration};url='".url($destination)."'", 'http-equiv');
        endif;
    }

    /**
     * We're going to clean any shortcodes from our SEO setup. As these are not needed.
     *
     * @param string $value
     */
    protected function cleanShortcodesFromMeta($value) {
        preg_match_all('#\[(.*?)\]#', $value, $shortcodes);

        if( count($shortcodes) > 0 ) {
            foreach($shortcodes[1] as $key => $selected_module) {
                $value = str_replace($shortcodes[0][$key], "", $value);
            }
        }

        return $value;
    }
}