<?php 
/*
|--------------------------------------------------------------------------
| Application Routes 
|--------------------------------------------------------------------------
|
| This file is where we will define all routes for the system. This file 
| contains the base system routes. Both frontend and backend.
|
| TODO: Split out admin routes to frontend routes.
|
*/

Route::group(['middleware' => ['hb3.AfterInstallation']], function() {
    Route::group(array('prefix' => General::backend_url()), function() {
        // LOGIN PAGES
        Route::group(['middleware' => ['hb3.IsAuthenticated']], function() {
            Route::post('account-reset/new-password/store', array('as' => 'hummingbird.reset-password.new-password-store', 'uses' => 'Admin\Auth\ResetPasswordController@store_new_password') );
            Route::get('account-reset/new-password/{token}', array('as' => 'hummingbird.reset-password.new-password', 'uses' => 'Admin\Auth\ResetPasswordController@new_password') );
            
            Route::post('account-reset/request', array('as' => 'hummingbird.reset-password.request', 'uses' => 'Admin\Auth\AuthController@password_reminder_process') );
            Route::get('account-reset', array('as' => 'hummingbird.reset-password', 'uses' => 'Admin\Auth\AuthController@password_reminder') );

            Route::post('new-account/{remember_token}', array('as' => 'hummingbird.new-account.update', 'uses' => 'Admin\Auth\AuthController@postNew_account') );
            Route::get('new-account/{remember_token}', array('as' => 'hummingbird.new-account.setup', 'uses' => 'Admin\Auth\AuthController@getNew_account') );
        });
    });

    include __DIR__ . '/fortify/admin.php';

    // BASE HUMMINGBID PAGES
    Route::get(General::backend_url(), array('middleware' => ['hb3.AdminAuth', 'hb3.ForcePasswordChange'], 'as' => 'hummingbird.dashboard', 'uses' => 'DashboardController@index'));

    // HUMMINGBIRD MODULES
    Route::group(array('as' => 'hummingbird.', 'prefix' => General::backend_url(), 'middleware' => ['hb3.AdminAuth', 'hb3.ForcePasswordChange']),  function() {
        Route::any('forbidden', array('uses' => 'CmsbaseController@forbidden', 'as' => 'forbidden'));
        
        // INDIVIDUAL ROUTES
        Route::post('failed-jobs/flush', array('as' => 'failed-jobs.flush', 'uses' => 'JobsController@flush') );
        Route::get('failed-jobs/retry-all', array('as' => 'failed-jobs.retry-all', 'uses' => 'JobsController@retry_all') );
        Route::get('failed-jobs/retry/{id}', array('as' => 'failed-jobs.retry', 'uses' => 'JobsController@retry') );
        Route::resource('failed-jobs', 'JobsController', [ 'only' => ['index', 'destroy'] ]);

        // GENERAL CMS ROUTES
            Route::get('users/export', array('as' => 'users.export', 'uses' => 'UserController@getExport'));
            Route::post('users/export', array('as' => 'users.export.process', 'uses' => 'UserController@postExport'));
            Route::post('users/import', array('as' => 'users.import', 'uses' => 'UserController@import'));
            Route::get('users/re-instate/{id}', array('as' => 'users.re-instate', 'uses' => 'UserController@reinstate') );
            
            Route::any('profile', array('as' => 'profile', 'uses' => 'UserController@profile'))->middleware('password.confirm');

            Route::get('users/preview/destroy', array('as' => 'users.preview.destroy', 'uses' => 'UserController@preview_remove'));
            Route::get('users/preview/{user_id?}', array('as' => 'users.preview', 'uses' => 'UserController@preview'));

            Route::any('users/password-force-change/{id}', array('as' => 'users.password-force-update', 'uses' => 'UserController@userPasswordForceReset'));
            Route::any('users/password-force-change', array('as' => 'users.multi.password-force-update', 'uses' => 'UserController@usersPasswordForceReset'));
            Route::any('users/change-status/{id}', ['as' => 'users.status.update', 'uses' => 'UserController@changeStatus']);

        Route::resource('users', 'UserController', [ 'only' => ['index', 'store', 'show', 'update', 'destroy'] ]);


            Route::get('pages/export', array('middleware' => ['hb3.isSuperUser'], 'as' => 'pages.export', 'uses' => 'PageController@export_pages'));
            Route::get('pages/restore/{id}', array('as' => 'pages.restore', 'uses' => 'PageController@restorePage'));
            Route::get('pages/empty-trash', array('as' => 'pages.empty-trash', 'uses' => 'PageController@emptyTrash'));
            Route::post('pages/search', array('as' => 'pages.search', 'uses' => 'PageController@search'));
            Route::get('pages/replicate/{page_id?}', ['as' => 'pages.replicate', 'uses' => 'Hummingbird\Controllers\PageController@replicate']);
            Route::get('pages/re-order/{parent?}', ['as' => 'pages.re-order', 'uses' => 'PageController@reorder']);
            Route::put('pages/re-order', ['as' => 'pages.re-order.update', 'uses' => 'PageController@updateOrder']);
            Route::any('pages/publish/{page_id}/{value}', ['as' => 'pages.publish', 'uses' => 'PageController@publish']);
            Route::any('pages/import/{action?}', ['as' => 'pages.import', 'uses' => 'PageController@import']);
            Route::any('pages/trash', ['as' => 'pages.trash.index', 'uses' => 'PageController@getTrash']);
        Route::resource('pages', 'PageController', [ 'only' => ['index', 'store', 'show', 'update', 'destroy'] ]);

            Route::any('menus/deleteItem/{id}', ['as' => 'menus.deleteItem', 'uses' => 'MenuController@deleteItem']);
            Route::post('menus/re-order/{id}', ['as' => 'menus.re-order.update', 'uses' => 'MenuController@update_order']);
            Route::get('menus/re-order/{id}', ['as' => 'menus.re-order', 'uses' => 'MenuController@get_update_order']);
        Route::resource('menus', 'MenuController', [ 'only' => ['index', 'store', 'edit', 'update', 'destroy'] ]);
        
            Route::post('errors/truncate', ['as' => 'errors.truncate', 'uses' => 'ErrorController@truncate']);
            Route::delete('errors/remove', ['as' => 'errors.remove', 'uses' => 'ErrorController@remove']);
        Route::resource('errors', 'ErrorController', array('except' => 'destroy'));

            Route::any('modules/replicate/{id}', array('as' => 'modules.replicate', 'uses' => 'ModuleController@replicate'));
        Route::resource('modules', 'ModuleController');
        
            Route::get('module-templates/lock/{id}', array('as' => 'module-templates.lock', 'uses' => 'ModuleTemplateController@postLock'));
        Route::resource('module-templates', 'ModuleTemplateController', [ 'only' => ['index', 'store', 'edit', 'update', 'destroy'] ]);
            
        Route::group(array('prefix' => 'redirections'), function() {
            Route::any('export', array('as' => 'redirections.export', 'uses' => 'RedirectionsController@export'));
            Route::any('import', array('as' => 'redirections.import', 'uses' => 'RedirectionsController@import'));

            Route::group(array('prefix' => 'deleted'), function() {
                Route::post('reinstate-all', array('as' => 'redirections.reinstate-all', 'uses' => 'RedirectionsController@reinstate_all'));
                Route::post('reinstate/{id}', array('as' => 'redirections.reinstate', 'uses' => 'RedirectionsController@reinstate'));
                Route::delete('purge-all', array('as' => 'redirections.purge-all', 'uses' => 'RedirectionsController@purge_all'));
                Route::delete('purge/{id}', array('as' => 'redirections.purge', 'uses' => 'RedirectionsController@purge'));
            });
            Route::get('deleted', array('as' => 'redirections.deleted', 'uses' => 'RedirectionsController@show_deleted'));
        });
        Route::resource('redirections', 'RedirectionsController', [ 'except' => ['create'] ]);

        Route::group(['middleware' => ['hb3.isSuperUser']], function () {
                Route::post('plugins/process', array('as' => 'plugins.process', 'uses' => 'Admin\PluginController@plugin_update'));
            Route::resource('plugins', 'Admin\PluginController', [ 'except' => ['create', 'store', 'show', 'update'] ]);
                
                Route::put('settings/update/{param?}', array('as' => 'settings.update', 'uses' => 'Admin\SystemSettingsController@update'));
            Route::resource('settings', 'Admin\SystemSettingsController', ['only' => ['index']]);
            
                Route::get('themes/activate/{theme}', array('as' => 'themes.activate', 'uses' => 'ThemeController@activate'));
            Route::resource('themes', 'ThemeController');
        });

            Route::get('roles/preview/{role_id?}', 'RoleController@preview');
        Route::resource('roles', 'RoleController', [ 'only' => ['index', 'store', 'show', 'update', 'destroy'] ]);

            Route::get('templates/clone/{id}', array('as' => 'templates.clone', 'uses' => 'Admin\TemplatesController@cloneTemplate'));
            Route::get('templates/get-html/{id}', 'Admin\TemplatesController@returnTemplateHTML');
        Route::resource('templates', 'Admin\TemplatesController', [ 'only' => ['index', 'store', 'edit', 'update', 'destroy'] ]);

        Route::resource('activity-logs', 'ActivitylogController', array('only' => array('index')));

            Route::any('updates/run', array('as' => 'updates.run', 'uses' => 'UpdatesController@run_these_updates'));
            Route::any('updates/progress', array('as' => 'updates.progress', 'uses' => 'UpdatesController@return_progress'));
        Route::resource('updates', 'UpdatesController', array('only' => array('index')));

            Route::post('tags/deleted/reinstate-all', array('as' => 'tags.reinstate-all', 'uses' => 'TagsController@reinstate_all'));
            Route::post('tags/deleted/reinstate/{id}', array('as' => 'tags.reinstate', 'uses' => 'TagsController@reinstate'));
            Route::delete('tags/deleted/purge-all', array('as' => 'tags.purge-all', 'uses' => 'TagsController@purge_all'));
            Route::delete('tags/deleted/purge/{id}', array('as' => 'tags.purge', 'uses' => 'TagsController@purge'));
            Route::get('tags/deleted', array('as' => 'tags.show-deleted', 'uses' => 'TagsController@showDeleted'));
        Route::resource('tags', 'TagsController', array('except' => array('create')));

        // Taxonomy - Categories
        Route::group(array('prefix' => 'categories'), function() {
            Route::post('re-order', array('as' => 'categories.save-reorder', 'uses' => 'CategoriesController@update_order'));
            Route::get('re-order', array('as' => 'categories.reorder', 'uses' => 'CategoriesController@get_update_order'));

            Route::any('children', array('as' => 'categories.children', 'uses' => 'CategoriesController@getChildren'));
            Route::post('deleted/reinstate-all', array('as' => 'categories.reinstate-all', 'uses' => 'CategoriesController@reinstate_all'));
            Route::any('deleted/reinstate/{id}', array('as' => 'categories.reinstate', 'uses' => 'CategoriesController@reinstate'));
            Route::delete('deleted/purge-all', array('as' => 'categories.purge-all', 'uses' => 'CategoriesController@purge_all'));
            Route::delete('deleted/purge/{id}', array('as' => 'categories.purge', 'uses' => 'CategoriesController@purge'));
            Route::any('deleted', array('as' => 'categories.deleted', 'uses' => 'CategoriesController@showDeleted'));
        });
        Route::resource('categories', 'CategoriesController', array('except' => array('create', 'edit')));
        
        Route::any('media/settings/update/{id}', array('as' => 'media.settings.update', 'uses' => 'MediaController@updateSetting'));
        Route::any('media/settings/delete', array('as' => 'media.settings.delete', 'uses' => 'MediaController@deleteSetting'));
        Route::any('media/settings/store', array('as' => 'media.settings.store', 'uses' => 'MediaController@createSetting'));
        Route::any('media/settings/{id}', array('as' => 'media.settings.show', 'uses' => 'MediaController@showSettings'));
        Route::any('media/settings', array('as' => 'media.settings', 'uses' => 'MediaController@settings'));
        Route::any('media/postEditImage/{media}', array('as' => 'media.postEditImage', 'uses' => 'MediaController@postEditImage'));
        Route::any('media/editMediaItem/{media}', array('as' => 'media.editMediaItem', 'uses' => 'MediaController@postEditMediaItem'));
        Route::any('media/edit-image/{image}', array('as' => 'media.edit-image', 'uses' => 'MediaController@editImage'));
        Route::any('media/globalMediaLibrary', array('as' => 'media.globalMediaLibrary', 'uses' => 'MediaController@globalMediaLibrary')); //
        Route::any('media/mediaLibraryRequest', array('as' => 'media.mediaLibraryRequest', 'uses' => 'MediaController@mediaLibraryRequest'));
        Route::any('media/getMediaModalTemplate', array('as' => 'media.getMediaModalTemplate', 'uses' => 'MediaController@getMediaModalTemplate'));
        Route::any('media/updateUploadedFiles', array('as' => 'media.updateUploadedFiles', 'uses' => 'MediaController@updateUploadedFiles'));
        Route::any('media/uploadFile', array('as' => 'media.uploadFile', 'uses' => 'MediaController@uploadFile'));
        Route::any('media/importFiles', array('as' => 'media.importFiles', 'uses' => 'MediaController@importFiles'));
        Route::any('media/delete-item/{media}', array('as' => 'media.delete-item', 'uses' => 'MediaController@destroyMedia'));
        Route::any('media/delete/{collection}', array('as' => 'media.delete', 'uses' => 'MediaController@destroy'));
        Route::any('media/view-media/{item}', array('as' => 'media.view-media', 'uses' => 'MediaController@viewItem'));
        Route::any('media/update/{collection}', array('as' => 'media.update', 'uses' => 'MediaController@update'));
        Route::any('media/edit/{collection}', array('as' => 'media.edit', 'uses' => 'MediaController@edit'));
        Route::any('media/view/{collection}', array('as' => 'media.view', 'uses' => 'MediaController@index'));
        Route::resource('media', 'MediaController', ['except' => ['update']]);
    });

    // FRONTEND PAGES
    Route::group(array('prefix' => 'archives'), function() {
        Route::any('tags/{slug}', array('as' => 'HB3.Archives.Tag', 'uses' => 'Frontend\ArchivesFrontendController@viewByTag'))->where('slug', '(.*)?');
        Route::any('categories/{slug}', array('as' => 'HB3.Archives.Category', 'uses' => 'Frontend\ArchivesFrontendController@viewByCategory'))->where('slug', '(.*)?');
        Route::any('', ['as' => 'HB3.Archives', 'uses' => 'Frontend\ArchivesFrontendController@index']);
    });
    
    Route::any('search', ['as' => 'HB3.SiteSearch', 'uses' => 'SearchController@search']);
});
