<?php 
use Hummingbird\Libraries\Installer;

/*
|--------------------------------------------------------------------------
| Application Installation 
|--------------------------------------------------------------------------
|
| The routes that define the installation process of Hummingbird 3.
| These are all wrapped (except 'done') inside of Middleware to stop double
| installation.
|
*/
Route::group(['prefix' => Installer::uri()], function() {
    Route::group(['middleware' => ['hb3.BeforeInstallation']], function() {
        Route::post( 'plugins', array('as' => 'hummingbird.install.plugins.validate', 'uses' => 'InstallController@postPlugins') );
        Route::get( 'plugins', array('as' => 'hummingbird.install.plugins', 'uses' => 'InstallController@getPlugins') );

        Route::post( 'permissions', array('as' => 'hummingbird.install.permissions.validate', 'uses' => 'InstallController@postPermissions') );
        Route::get( 'permissions', array('as' => 'hummingbird.install.permissions', 'uses' => 'InstallController@getPermissions') );

        Route::post( 'site', array('as' => 'hummingbird.install.site.validate', 'uses' => 'InstallController@postSite') );
        Route::get( 'site', array('as' => 'hummingbird.install.site', 'uses' => 'InstallController@getSite') ); 

        Route::get( 'run-migrations', array('as' => 'hummingbird.install.run-migrations', 'uses' => 'InstallController@getRunMigrations') );

        Route::post( 'database', array('as' => 'hummingbird.install.database.validate', 'uses' => 'InstallController@postDatabase') );
        Route::get( 'database', array('as' => 'hummingbird.install.database', 'uses' => 'InstallController@getDatabase') );
    });

    Route::get('done', array('as' => 'hummingbird.install.complete', 'uses' => 'InstallController@complete') );
});