<?php namespace Hummingbird\Providers\Shortcodes;

use Illuminate\Support\Facades\Facade;

class SystemShortcodesFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'hummingbird.shortcodes.wysiwyg'; }
}