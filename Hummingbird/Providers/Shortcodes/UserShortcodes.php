<?php namespace Hummingbird\Providers\Shortcodes;

use Auth;
use User;

class UserShortcodes extends ShortcodeHandler {

    /**
     * Get the firstname for this care giver
     */
    public function get_user_firstname($shortcode, $content, $compiler, $name, $data = []) {
        if( Auth::user() ) {
            return Auth::user()->firstname;
        }

        if( isset($data['User']) && $data['User'] instanceof User ) {
            return $data['User']->firstname;
        }
    }
}