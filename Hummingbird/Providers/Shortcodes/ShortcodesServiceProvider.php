<?php namespace Hummingbird\Providers\Shortcodes;

use SystemShortcodes as RegisterShortcodes;
use Illuminate\Support\ServiceProvider;
use Hummingbird\Shortcodes\SystemShortcodes;

class ShortcodesServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    

    /**
     * Bootstrap the application events.
     */
    public function boot() {}


    /**
     * Register the service provider.
     */
    public function register() {
        $this->app->singleton(
            'hummingbird.shortcodes.wysiwyg',
            function ($app) {
                return new SystemShortcodes;
            }
        );

        RegisterShortcodes::registerShortcodes([
            ['name' => 'auth.firstname', 'action' => 'Hummingbird\Providers\Shortcodes\UserShortcodes@get_user_firstname']
        ]);
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array(
            'hummingbird.shortcodes.wysiwyg'
        );
    }
}