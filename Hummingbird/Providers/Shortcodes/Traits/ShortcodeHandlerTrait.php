<?php namespace Hummingbird\Providers\Shortcodes\Traits;

trait ShortcodeHandlerTrait {

    /**
     * Get the PHP date format needed for this date
     * @param  Object $shortcode
     * @return return
     */
    protected function getDateFormatFromShortcode($shortcode) {
        return ( $shortcode->dateformat ) ? $shortcode->dateformat : $this->config['dateFormat'];
    }
}