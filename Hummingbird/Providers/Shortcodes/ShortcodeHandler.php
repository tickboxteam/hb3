<?php namespace Hummingbird\Providers\Shortcodes;

/**
 * Base ShortcodeHandler class allowing us to complete certain actions
 */
class ShortcodeHandler {
    /**
     * Configuration items for Shortcode Handler
     *
     * @var Array
     */
    protected $config = [
        'dateFormat' => 'd M Y',
    ];
}