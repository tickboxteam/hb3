<?php namespace Hummingbird\Providers\Permissions;

use Illuminate\Support\Facades\Facade;

class PermissionsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'permissions';
    }
}