<?php namespace Hummingbird\Providers\Search;

use Illuminate\Support\ServiceProvider;

class SearchIndexServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    

    /**
     * Bootstrap the application events.
     */
    public function boot() {}


    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->singleton('SearchIndex', function ($app) {
            return new \Hummingbird\Handlers\SearchableHandler;
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['searchindex'];
    }
}