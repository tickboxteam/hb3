<?php namespace Hummingbird\Providers\Core;

use Hummingbird\Libraries\Installer;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class CoreRouteServiceProvider extends ServiceProvider {

	/**
	 * This namespace is applied to the controller routes in your routes file.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'Hummingbird\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {   
        if( config()->get('hb3-hummingbird.install') ):
            Route::group([
                'middleware' => 'web',
                'namespace' => $this->namespace,
            ], function ($router) {
                require base_path('Hummingbird/Routes/system.php');

                Route::any('{slug}', ['as' => 'HB3.PageLoad', 'uses' => 'FrontendController@show', 'middleware' => ['hb3.AfterInstallation']])->where('slug', '(.*)?');
            });
        endif;

        if( !config()->get('hb3-hummingbird.install') ):
            Route::group([
                'middleware' => 'web',
                'namespace' => $this->namespace,
            ], function ($router) {
                require base_path('Hummingbird/Routes/install.php');

                Route::any('', ['as' => 'HB3.PageLoad', 'uses' => 'InstallController@getIndex']);
            });
        endif;
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {

    }
}
