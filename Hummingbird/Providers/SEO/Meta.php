<?php namespace Hummingbird\Providers\SEO;

use Illuminate\Support\Facades\Facade;

class Meta extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'hummingbird.seo.generators.meta'; }
}
