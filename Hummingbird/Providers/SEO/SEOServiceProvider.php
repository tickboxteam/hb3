<?php namespace Hummingbird\Providers\SEO;

use HB3Meta;
use Illuminate\Config\Repository as Config;
use Illuminate\Support\ServiceProvider;
use Hummingbird\SEO\Generators\GenerateSitemap;
use Hummingbird\SEO\Generators\HB3MetaGenerator;

class SEOServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    

    /**
     * Bootstrap the application events.
     */
    public function boot() {
        // Write a unique title tag for each page;
            // Be brief, but descriptive;
            // Avoid generic and vague titles;
            // Use sentence case or title case;
            // Create something click-worthy—not clickbait;
            // Match search intent;
            // Include your target keyword where it makes sense;
            // Keep it under 60 characters.

        // There are four common issues with title tags:
            // Too long/short. Google says to “avoid unnecessarily long or verbose titles” while keeping them “descriptive and concise.”
            // Doesn’t exist. Google says that every page should have a title tag.
            // Multiple title tags on one page. Search engines may display an undesirable title tag if there is more than one on a page.
            // Duplicate titles across multiple pages. Google says that “it’s important to have distinct, descriptive titles for each page on your site.”
        HB3Meta::setTitleDefault(env('SEO_TITLE', config('hb3-hummingbird.site_name')) );

        // Write a unique description for each page;
            // Try to summarize content accurately;
            // Avoid generic descriptions;
            // Use sentence case;
            // Create something click-worthy, not clickbait;
            // Match search intent;
            // Include your target keyword where it makes sense;
            // Keep it under 160 characters

        // The four common issues with meta descriptions are the same as those with title tags:
            // Too long/short. Google says “there’s no limit on how long a meta description can be, but the search result snippets are truncated as needed, typically to fit the device width.”
            // Doesn’t exist. Google says to “make sure that every page on your site has a meta description.”
            // Multiple meta descriptions on one page. More than one tag may confuse search engines.
            // Duplicate meta descriptions across multiple pages. Google says that you should “differentiate the descriptions for different pages.”
        HB3Meta::setDescription( env('SEO_DESC') );

        // With that in mind, here are the values you can use in this tag:
            //     index: tells bots to index the page;
            //     noindex: tells bots not to index the page;
            //     follow: tells bots to crawl links on the page, and that you also vouch for them;
            //     nofollow: tells bots not to crawl links on the page, and that no endorsement is implied.

        // Best practices
            // Use meta robots tags only when you want to restrict the way Google crawls a page;
            // Don’t block pages with meta robots tags in robots.txt;
            HB3Meta::addMeta('robots', 'index, follow');

        // Open Graph SEO
            // og:title: The title of your object as it should appear within the graph, e.g., "The Rock".
            // og:type - The type of your object, e.g., "video.movie". Depending on the type you specify, other properties may also be required.
            HB3Meta::addMeta('og:type', 'website');
            // og:url - The canonical URL of your object that will be used as its permanent ID in the graph, e.g., "http://www.imdb.com/title/tt0117500/".
            // og:audio - A URL to an audio file to accompany this object.
            // og:description - A one to two sentence description of your object.
            // og:determiner - The word that appears before this object's title in a sentence. An enum of (a, an, the, "", auto). If auto is chosen, the consumer of your data should chose between "a" or "an". Default is "" (blank).
            // og:locale - The locale these tags are marked up in. Of the format language_TERRITORY. Default is en_US.
            HB3Meta::addMeta('og:locale', env('SEO_OPENGRAPH_LOCALE') ?: 'en_GB');
            // og:locale:alternate - An array of other locales this page is available in.
            // og:site_name - If your object is part of a larger web site, the name which should be displayed for the overall site. e.g., "IMDb".
            HB3Meta::addMeta('og:site_name', env('SEO_TITLE', config('hb3-hummingbird.site_name')));
            // og:video - A URL to a video file that complements this object.

        // Twitter Card SEO
            HB3Meta::addMeta('twitter:card', env('SEO_TWITTER_CARD') ?: 'summary');

            if( env('SEO_TWITTER') ) {
                HB3Meta::addMeta('twitter:site', env('SEO_TWITTER'));
            }

        // Google Specific SEO
        if( env('GOOGLE_SITE_VERIFICATION') ) {
            HB3Meta::addMeta('google-site-verification', env('GOOGLE_SITE_VERIFICATION'));
        }
    }


    /**
     * Register the service provider.
     */
    public function register() {
        // Register the meta tags generator
        $this->app->singleton(
            'hummingbird.seo.generators.meta',
            function ($app) {
                return new HB3MetaGenerator(new Config($app['config']->get('seotools.meta', [])));
            }
        );
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array( 'hummingbird.seo.generators.meta' );
    }
}