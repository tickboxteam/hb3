<?php namespace Hummingbird\Providers\EmailTracker;

use App;
use Config;
use Event;
use File;
use Session;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Support\ServiceProvider;

class EmailTrackerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    

    /**
     * Bootstrap the application events.
     */
    public function boot() {
        $this->publishes([__DIR__.'/config/tracking.php' => config_path("emailTracker")]);
        $this->mergeConfigFrom(__DIR__.'/config/tracking.php', "emailTracker");

        // Hook into the mailer
        Event::listen(MessageSending::class, function(MessageSending $event) {
            $tracker = new Tracker;
            $tracker->messageSending($event);
        });
    }


    /**
     * Register the service provider.
     */
    public function register() {
        if( File::exists( __DIR__ . '/routes.php' ) ) {
            include __DIR__ . '/routes.php';
        }
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array();
    }
}