<?php namespace Hummingbird\Providers\EmailTracker\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class EmailActivity extends Eloquent {

    public $table       = 'email_tracking_activities';
    public $fillable    = ['tracker_id', 'type', 'link'];


    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    public static function boot() {
        parent::boot();

        static::creating(function($Taxonomy) {
            if( !isset($Taxonomy->link) ):
                $Taxonomy->link = '';
            endif;
        });
    }

    /**
     * The tracker attached to it
     */
    public function tracker() {
        return $this->belongsTo(EmailTrack::class, 'id', 'tracker_id');
    }
}