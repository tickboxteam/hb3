<?php namespace Hummingbird\Providers\EmailTracker\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class EmailTrack extends Eloquent {

    public $table       = 'email_tracking';
    public $fillable    = ['hash', 'message_id', 'headers', 'sender_name', 'sender_email', 'subject', 'recipient_name', 'recipient_email', 'content', 'content_plain', 'type', 'is_admin'];
    public $guarded     = [];

    public function activities() {
        return $this->hasMany(EmailActivity::class, 'tracker_id', 'id');
    }

    public function opens() {
        return $this->activities()->where('type', 'open');
    }

    public function clicks() {
        return $this->activities()->where('type', 'click');
    }

    public function setHashAttribute($value) {
        $this->attributes['hash'] = strtolower($value);
    }

    public function setTypeAttribute($value) {
        $this->attributes['type'] = ( !empty($value) ) ? $value : null;
    }

    public function setIsAdminAttribute($value) {
        $this->attributes['is_admin'] = ( !empty($value) ) ? true : null;
    }

    /**
     * Find EmailTrack by its hash
     */
    public function scopeHash($query, $value) {
        return $query->where('hash', $value);
    }
}