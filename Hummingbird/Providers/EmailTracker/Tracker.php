<?php namespace Hummingbird\Providers\EmailTracker;

use Config;
use Event;
use Hummingbird\Providers\EmailTracker\Models\EmailTrack;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Support\Str;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Part\Multipart\AlternativePart;
use Symfony\Component\Mime\Part\Multipart\MixedPart;
use Symfony\Component\Mime\Part\Multipart\RelatedPart;
use Symfony\Component\Mime\Part\TextPart;

/**
 * @package https://github.com/jdavidbakr/mail-tracker
 */
class Tracker
{
    /**
     * The hash we're going to use for tracking
     *
     * @var string
     */
    protected $hash;

    /**
     * Inject the tracking code into the message
     */
    public function messageSending(MessageSending $event)
    {
        if( $this->emailTrackingActivated() ) {
            $message = $event->message;

            // Create the trackers
            $this->createTrackers($message);
        }
    }

    public function messageSent(MessageSent $event): void
    {
        $sentMessage = $event->sent;
        $headers = $sentMessage->getOriginalMessage()->getHeaders();
        $hash = optional($headers->get('X-Mailer-Hash'))->getBody();
        $sentEmail = EmailTrack::where('hash', $hash)->first();

        if ($sentEmail) {
            $sentEmail->message_id = $event->getMessageId();
            $sentEmail->save();
        }
    }

    /**
     * Adding tracking codes
     * @param String $html
     * @param String $hash
     * @return String
     */
    protected function addTrackers($html, $hash)
    {
        if( config('emailTracker.track_email_opens') == true ) {
            $html = $this->injectTrackingPixel($html, $hash);
        }

        if( config('emailTracker.track_email_clicks') == true ) {
            $html = $this->injectLinkTracker($html, $hash);
        }

        return $html;
    }

    /**
     * Insert tracking pixel into the email
     * @param  String $html Message content
     * @param  String $hash Hash of message
     * @return String
     */
    protected function injectTrackingPixel($html, $hash)
    {
        if( empty($html) ):
            return $html;
        endif;

        // Append the tracking url
        $tracking_pixel = '<img border="0" width="1" alt="" height="1" src="'.route('EmailTrack.Open', [$hash]).'" />';

        $dom = new \DOMDocument;
        @$dom->loadHTML($html);
        $body = $dom->getElementsByTagName('body')->item(0);
        $fragment = $dom->createDocumentFragment();
        $fragment->appendXML($tracking_pixel);

        if( $body ) {
            $body->appendChild($fragment);
        }
        else {
            $dom->appendChild($fragment);
        }

        return $dom->saveHTML();
    }

    /**
     * Replace all links, if they exist, with a tracker link
     * @param  String $html Message content
     * @param  String $hash Hash of message
     * @return String
     */
    protected function injectLinkTracker($html, $hash)
    {
        $this->hash = $hash;

        $html = preg_replace_callback(
            "/(<a[^>]*href=[\"])([^\"]*)/",
            [$this, 'inject_link_callback'],
            $html
        );

        return $html;
    }

    /**
     * Set link, if not empty, to the one we want to return to
     * @param  Array $matches
     * @return String
     */
    protected function inject_link_callback($matches)
    {
        $url = ( !empty($matches[2]) ) ? str_replace('&amp;', '&', $matches[2]) : url('/');

        return $matches[1].route(
            'EmailLinkTrack.Open',
            [ 'l' => trim($url), 'h' => $this->hash ]
        );
    }

    /**
     * Create the trackers
     *
     * @param  Swift_Mime_Message $message
     * @return void
     */
    protected function createTrackers(Email $message)
    {
        foreach ($message->getTo() as $toAddress) {
            $to_email = $toAddress->getAddress();
            $to_name = $toAddress->getName();
            foreach ($message->getFrom() as $fromAddress) {
                $from_email = $fromAddress->getAddress();
                $from_name = $fromAddress->getName();
                $headers = $message->getHeaders();

                $EmailType  = ( $headers->has('EMAIL-TYPE') ) ? trim( str_replace("EMAIL-TYPE:", "", $headers->get('EMAIL-TYPE')->getValue()) ) : NULL;
                $IsAdmin    = ( $headers->has('EMAIL-ADMIN') ) ? $headers->get('EMAIL-ADMIN')->getValue() : NULL;

                $headers->remove('EMAIL-TYPE');
                $headers->remove('EMAIL-ADMIN');

                if( 
                    ( 
                        ( empty($IsAdmin) && config('emailTracker.allow_email_tracking') == true ) || 
                        ( !empty($IsAdmin) && config('emailTracker.allow_admin_email_tracking') == true ) 
                    ) && ( !in_array($EmailType, config('emailTracker.block_email_tracking_by_type')) )
                ) {
                    if ($headers->get('X-No-Track')) {
                        continue;
                        // Don't send with this header
                        // $headers->remove('X-No-Track');
                        // Don't track this email
                        // continue;
                    }
                    do {
                        $hash = str_random(32);
                        $used = EmailTrack::where('hash', $hash)->count();
                    } while ($used > 0);

                    while ($used > 0);
                    $headers->addTextHeader('X-Mailer-Hash', $hash);
                    $subject = $message->getSubject();

                    $original_content = $message->getBody();
                    $original_html = '';
                    if(
                        ($original_content instanceof(AlternativePart::class)) ||
                        ($original_content instanceof(MixedPart::class)) ||
                        ($original_content instanceof(RelatedPart::class))
                    ) {
                        $messageBody = $message->getBody() ?: [];
                        $newParts = [];
                        foreach($messageBody->getParts() as $part) {
                            if($part->getMediaSubtype() == 'html') {
                                $original_html = $part->getBody();
                                $newParts[] = new TextPart(
                                    $this->addTrackers($original_html, $hash),
                                    $message->getHtmlCharset(),
                                    $part->getMediaSubtype(),
                                    null
                                );
                            } else if ($part->getMediaSubtype() == 'alternative') {
                                if (method_exists($part, 'getParts')) {
                                    foreach ($part->getParts() as $p) {
                                        if($p->getMediaSubtype() == 'html') {
                                            $original_html = $p->getBody();
                                            $newParts[] = new TextPart(
                                                $this->addTrackers($original_html, $hash),
                                                $message->getHtmlCharset(),
                                                $p->getMediaSubtype(),
                                                null
                                            );

                                            break;
                                        }
                                    }
                                }
                            } else {
                                $newParts[] = $part;
                            }
                        }
                        $message->setBody(new (get_class($original_content))(...$newParts));
                    } else {
                        $original_html = $original_content->getBody();
                        if($original_content->getMediaSubtype() == 'html') {
                            $message->setBody(new TextPart(
                                    $this->addTrackers($original_html, $hash),
                                    $message->getHtmlCharset(),
                                    $original_content->getMediaSubtype(),
                                    null
                                )
                            );
                        }
                    }

                    $tracker = EmailTrack::updateOrCreate(['hash' => $hash, 'message_id' => Str::uuid()], [
                        'headers' => $headers->toString(),
                        'sender_name' => $from_name,
                        'sender_email' => $from_email,
                        'subject' => $subject,
                        'recipient_name' => $to_name,
                        'recipient_email' => $to_email,
                        'content' => $original_content->getBody(),
                        'content_plain' => strip_tags($original_content->getBody()),
                        'type' => ($EmailType) ? $EmailType : 'Unknown',
                        'is_admin' => ($IsAdmin) ? $IsAdmin : NULL
                    ]);
                }
            }
        }
    }

    /**
     * Are we able to send emails?
     * @return Boolean
     */
    protected function emailTrackingActivated() {
        return ( config('emailTracker.allow_email_tracking') || config('emailTracker.allow_admin_email_tracking') );
    }
}