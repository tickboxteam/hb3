<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Allow email tracking
    |--------------------------------------------------------------------------
    |
    | This option controls whether we should allow email tracking for customer 
    | related emails. Not administrator.
    |
    */
    'allow_email_tracking' => env('allow_email_tracking', TRUE),


    /*
    |--------------------------------------------------------------------------
    | Allow email tracking (for admins)
    |--------------------------------------------------------------------------
    |
    | This option controls whether we should allow email tracking for 
    | administrator emails.
    |
    */
    'allow_admin_email_tracking' => env('allow_admin_email_tracking', FALSE),


    /*
    |--------------------------------------------------------------------------
    | Allow email opens to be tracked
    |--------------------------------------------------------------------------
    |
    | Option controls whether we should allow for opens to be tracked in
    | emails.
    |
    */
    'track_email_opens' => env('track_email_opens', TRUE),


    /*
    |--------------------------------------------------------------------------
    | Allow email links to be tracked
    |--------------------------------------------------------------------------
    |
    | Option controls to allow whether links, when clicked, can be tracked.
    |
    */
    'track_email_clicks' => env('track_email_clicks', TRUE),


    /*
    |--------------------------------------------------------------------------
    | Block email tracking by type
    |--------------------------------------------------------------------------
    |
    | Option to allow emails to be blocked by type
    |
    */
    'block_email_tracking_by_type' => []
);