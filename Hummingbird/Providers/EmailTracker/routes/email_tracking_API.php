<?php

Route::any('e/open/{tracker_id?}', ['as' => 'EmailTrack.Open', 'uses' => 'Hummingbird\Controllers\EmailTracker\Api\EmailTracking@track_open']);

Route::any('e/click', ['as' => 'EmailLinkTrack.Open', 'uses' => 'Hummingbird\Controllers\EmailTracker\Api\EmailTracking@track_click']);