<?php namespace Hummingbird\Providers\Mailer\Exceptions;

class Hb3MailerException extends \Exception {
    public static function noRecipientsToSendTo() {
        return new static("Mailer can not send to an empty recipients list");
    }

    public static function emailDataMustBeInAnArrayFormat() {
        return new static("Email data must be passed as a an array");
    }
}