<?php namespace Hummingbird\Providers\Mailer;

use ArrayHelper;
use Config;
use Log;
use Mail;
use View;
use Hummingbird\Providers\Mailer\Exceptions\Hb3MailerException;

class Hb3Mailer {
    /**
     * Are we sending an administrator email?
     *
     * @var bool
     */
    public $is_admin = false;

    /**
     * Are we sending the email with Shortcodes?
     *
     * @var bool
     */
    public $with_shortcodes = true;

    /**
     * Any data that we should be providing for
     * email content
     *
     * @var array
     */
    public $data = [];

    /**
     * The view for our email
     *
     * @var string
     */
    public $view;

    /**
     * The recipients
     *
     * @var mixed
     */
    public $recipients = [];

    /**
     * The CC recipients
     *
     * @var mixed
     */
    public $cc_recipients = [];

    /**
     * The BCC recipients
     *
     * @var mixed
     */
    public $bcc_recipients = [];

    /**
     * The subject for our email
     *
     * @var string
     */
    public $subject;

    /**
     * The type of email we are sending
     *
     * @var string
     */
    public $email_type;

    /**
     * The name of the sender
     * @var String
     */
    public $sender_name;

    /**
     * The email of the sender
     * @var String
     */
    public $sender_email;
    /**
     * Send email
     *
     * @throws Hummingbird\Providers\Mailer\Exceptions\Hb3MailerException
     */
    public function send() {
        if( empty($this->getRecipients()) ):
            throw Hb3MailerException::noRecipientsToSendTo();
        endif;

        Mail::send([], [
        ], function($m) {
            $m->from( $this->getSenderEmail(), $this->getSenderName() )
                ->to( $this->getRecipients() )
                ->subject( $this->getSubject() )
                ->html( $this->generateEmailContent(), 'text/html');

            if( $this->hasCCRecipients() ):
                $m->cc( $this->getCCRecipients() );
            endif;


            if( $this->hasBCCRecipients() ):
                $m->bcc( $this->getBCCRecipients() );
            endif;

            $headers = $m->getHeaders();

            if( !empty($this->getEmailType()) ) {
                $headers->addTextHeader('EMAIL-TYPE', $this->getEmailType());
            }

            if( $this->isAdminEmail() ) {
                $headers->addTextHeader('EMAIL-ADMIN', TRUE);
            }
        });
    }

    /**
     * Generate the email content
     */
    protected function generateEmailContent() {
        $View = View::make( $this->getView() );
        
        if( $this->hasMailData() ) {
            $View->with( $this->getMailData() );
        }

        if( !$this->canRenderShortcodeContent() ) {
            return $View->render();
        }

        return $View->withShortcodes()->render();
    }

    /**
     * Setting the email view
     * 
     * @param String
     */
    public function setView($view) {
        if( View::exists( $view ) ):
            $this->view = $view;
        endif;
    }

    /**
     * Get the view
     * 
     * @return String
     */
    public function getView() {
        return $this->view;
    }

    /**
     * Add new recipient
     * 
     * @param String
     */
    public function addRecipient( $recipient ) {
        if( filter_var( $recipient ,FILTER_VALIDATE_EMAIL ) && !in_array( $recipient, $this->getRecipients() ) ):
            $this->recipients[] = $recipient;
        endif;
    }

    /**
     * Add recipients list
     * 
     * @param Array
     */
    public function addRecipients( $recipients ) {
        if( is_string( $recipients) ):
            $recipients = ArrayHelper::cleanExplodedArray( explode(",", $recipients) );
        endif;

        if( is_array($recipients) ) {
            foreach($recipients as $recipient) {
                $this->addRecipient( $recipient );
            }
        }
    }

    /**
     * Get the recipients for the mailer
     * 
     * @return Array
     */
    public function getRecipients() {
        return $this->recipients;
    }

    /**
     * Get the CC recipients for the mailer
     * 
     * @return Array
     */
    public function hasCCRecipients() {
        return count($this->cc_recipients);
    }

    /**
     * Add recipient to the CC list
     * 
     * @param String
     */
    public function addCCRecipient( $recipient ) {
        if( filter_var( $recipient ,FILTER_VALIDATE_EMAIL ) && !in_array( $recipient, $this->getRecipients() ) ):
            $this->cc_recipients[] = $recipient;
        endif;
    }

    /**
     * Add recipients to the CC list
     * 
     * @param Array
     */
    public function addCCRecipients( $recipients ) {
        if( is_string( $recipients) ):
            $recipients = ArrayHelper::cleanExplodedArray( explode(",", $recipients) );
        endif;

        if( is_array($recipients) ) {
            foreach($recipients as $recipient) {
                $this->addCCRecipient( $recipient );
            }
        }
    }

    /**
     * Get the CC recipients for the mailer
     * 
     * @return Array
     */
    public function getCCRecipients() {
        return $this->cc_recipients;
    }

    /**
     * Get the BCC recipients for the mailer
     * 
     * @return Array
     */
    public function hasBCCRecipients() {
        return count($this->bcc_recipients);
    }

    /**
     * Add recipient to the BCC list
     * 
     * @param String
     */
    public function addBCCRecipient( $recipient ) {
        if( filter_var( $recipient ,FILTER_VALIDATE_EMAIL ) && !in_array( $recipient, $this->getRecipients() ) ):
            $this->bcc_recipients[] = $recipient;
        endif;
    }

    /**
     * Add recipients to the BCC list
     * 
     * @param Array
     */
    public function addBCCRecipients( $recipients ) {
        if( is_string( $recipients) ):
            $recipients = ArrayHelper::cleanExplodedArray( explode(",", $recipients) );
        endif;

        if( is_array($recipients) ) {
            foreach($recipients as $recipient) {
                $this->addBCCRecipient( $recipient );
            }
        }
    }

    /**
     * Get the BCC recipients for the mailer
     * 
     * @return Array
     */
    public function getBCCRecipients() {
        return $this->bcc_recipients;
    }


    /**
     * Set the subject
     * @param String
     */
    public function setSubject( $subject ) {
        $this->subject = $subject;
    }

    /**
     * Get the mailer subject
     * 
     * @return String
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * Set the email type (for tracking)
     * @param String
     */
    public function setEmailType( $email_type ) {
        $this->email_type = $email_type;
    }

    /**
     * Get the mailer subject
     * 
     * @return String
     */
    public function getEmailType() {
        return $this->email_type;
    }

    /**
     * Add the Data we may need for emails
     * @param array $data
     */
    public function addMailData( $data = [] ) {
        if( !is_array($data) ) {
            throw Hb3MailerException::emailDataMustBeInAnArrayFormat();
        }

        if( !empty($data) ) {
            $this->data = array_merge( $this->data, $data );
        }
    }

    /**
     * Do we have data for our emails?
     * @return Boolean
     */
    public function hasMailData() {
        return (!empty($this->data) && count($this->data) > 0);
    }

    /**
     * Get data for our emails
     * @return Array
     */
    public function getMailData() {
        return ( $this->hasMailData() ) ? $this->data : [];
    }

    /**
     * Set whether we are sending an admin email
     * @param boolean $admin
     */
    public function setAdminEmail( $admin = false ) {
        if( $admin === FALSE || $admin === TRUE ) {
            $this->is_admin = $admin;
        }
    }

    /**
     * Getting the sender email
     * @return String
     */
    public function getSenderEmail() {
        return ( !empty($this->sender_email) ) ? $this->sender_email : Config::get('mail.from.address');
    }

    /**
     * Override where this email is being sent from
     * @return String
     */
    public function setSenderEmail($email) {
        $emails = ArrayHelper::cleanExplodedArray( explode(",", $email) );

        // Only return the first email
        $this->sender_email = $emails[0];
    }

    /**
     * Get the sender name
     * @return String
     */
    public function getSenderName() {
        return ( !empty($this->sender_name) ) ? $this->sender_name : Config::get('mail.from.name');
    }

    /**
     * Override the name of the person this is coming from
     * @return String
     */
    public function setSenderName($name) {
        $this->sender_name = $name;
    }

    /**
     * Are we sending an admin email?
     * @return boolean
     */
    public function isAdminEmail() {
        return $this->is_admin;
    }

    /**
     * Set whether we want to use or not use shortcode content
     * @param boolean $use_shortcodes
     */
    public function setShortcodeContent( $use_shortcodes = true ) {
        if( $use_shortcodes === FALSE || $use_shortcodes === TRUE ) {
            $this->with_shortcodes = $use_shortcodes;
        }
    }

    /**
     * Can we render content with shortcodes?
     * @return Boolean
     */
    public function canRenderShortcodeContent() {
        return $this->with_shortcodes;
    }
}