<?php namespace Hummingbird\Tests;

use Hummingbird\Models\Permission;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class Admin_CorePermissionsTest extends HummingbirdTestCase {
    protected $BaseAdminPermissions = [
            'activitylog.read', 'activitylog.export', 'activitylog.delete',
            'categories.create', 'categories.delete ', 'categories.export ', 'categories.filterCategories', 'categories.lock', 'categories.read', 'categories.update',
            'error.delete', 'error.export', 'error.read', 
            'media.create', 'media.delete', 'media.read ', 'media.update', 'media.upload',
            'modules.create', 'modules.read', 'modules.update', 'modules.delete',
            'modules.templates.create', 'modules.templates.read', 'modules.templates.update', 'modules.templates.delete', 'modules.templates.lock', 
            'menu.create', 'menu.delete', 'menu.lock', 'menu.read', 'menu.update',
            'page.create', 'page.delete', 'page.export', 'page.filterPages', 'page.lock', 'page.publish', 'page.read', 'page.update', 
            'redirections.create', 'redirections.delete', 'redirections.export', 'redirections.lock', 'redirections.read', 'redirections.update',
            'role.create', 'role.delete', 'role.lock', 'role.read', 'role.update',
            'setting.read', 'setting.update',
            'tags.create', 'tags.delete', 'tags.export', 'tags.lock', 'tags.read', 'tags.update',
            'template.create', 'template.delete', 'template.lock', 'template.read', 'template.update',
            'user.create', 'user.delete', 'user.lock', 'user.read', 'user.update'
        ];

    /**
     * Check that all core permissions exist in the database
     */
    public function test_admin_permissions_exist() {
        $this->assertEquals(count($this->BaseAdminPermissions), Permission::whereIn('name', $this->BaseAdminPermissions)->count());
    }
}
