<?php namespace Hummingbird\Tests\Unit\Validators;

use InvalidArgumentException;
use Validator;
use \Hummingbird\Tests\TestCase as HummingbirdTestCase;

class validPhoneNumberTest extends HummingbirdTestCase {
    /** @test */
    public function test_string_not_valid_telephone_number() {
        $Validator = Validator::make(['phoneno' => '123dsfdjd'], [
            'phoneno' => 'valid_phone_number:check_empty,false'
        ], []);

        $this->assertFalse( $Validator->passes() );
    }

    /** @test */
    public function test_correct_phone_number_for_uk() {
        $Validator = Validator::make(['phoneno' => '01173250091'], [
            'phoneno' => 'valid_phone_number:check_empty,false'
        ]);

        $this->assertTrue( $Validator->passes() );
    }

    /** @test */
    public function test_correct_phone_number_for_other_countries() {
        $Validator = Validator::make(['phoneno' => '2139181105'], [
            'phoneno' => 'valid_phone_number:check_empty,false'
        ]);

        $this->assertTrue( $Validator->passes() );
    }

    /** @test */
    public function test_correct_phone_number_validation_with_country_code() {
        $Validator = Validator::make(['phoneno' => '+441173250091'], [
            'phoneno' => 'valid_phone_number:check_empty,false'
        ]);

        $this->assertTrue( $Validator->passes() );
    }
}