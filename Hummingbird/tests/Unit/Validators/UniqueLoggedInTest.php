<?php namespace Hummingbird\Tests\Unit\Validators;

use Session;
use Validator;
use Hummingbird\Models\User;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Hummingbird\Database\DummySeeds\DummyRolesSeeder;
use Hummingbird\Database\DummySeeds\DummyUsersSeeder;

class UniqueLoggedInTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();
        
        $this->seed(DummyRolesSeeder::class);
        $this->seed(DummyUsersSeeder::class);
        
        $this->SuperUser = User::where('email', 'super@admin.com')->first();
    }

    /** @test */
    public function test_validator_fails_if_not_logged_in() {
        $Validator = Validator::make(['email' => 'new-admin@admin.com'], [
            'email' => 'unique_logged_in:users,email,[auth_id]'
        ], []);

        $this->assertFalse( $Validator->passes() );
    }

    /** @test */
    public function test_validator_fails_if_logged_in_but_email_already_exists() {
        Auth::loginUsingId($this->SuperUser->id, true);

        $Validator = Validator::make(['email' => 'admin@admin.com'], [
            'email' => 'unique_logged_in:users,email,[auth_id]'
        ], []);

        $this->assertFalse( $Validator->passes() );
    }

    /** @test */
    public function test_validator_passes_if_logged_in_and_email_is_unique() {
        Auth::loginUsingId($this->SuperUser->id, true);

        $Validator = Validator::make(['email' => 'new-admin@admin.com'], [
            'email' => 'unique_logged_in:users,email,[auth_id]'
        ], []);

        $this->assertTrue( $Validator->passes() );
    }
}