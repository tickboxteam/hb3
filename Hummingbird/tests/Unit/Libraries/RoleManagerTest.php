<?php namespace Hummingbird\Tests\Unit\Libraries;

use DB;
use Hummingbird\Libraries\RoleManager;
use Hummingbird\Models\Role;
use Hummingbird\Models\User;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;
use Hummingbird\Database\DummySeeds\DummyRolesSeeder;
use Hummingbird\Database\DummySeeds\DummyUsersSeeder;
use Illuminate\Support\Facades\Auth;

class RoleManagerTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        $this->seed(DummyRolesSeeder::class);
        $this->seed(DummyUsersSeeder::class);

        $this->SuperUser = User::where('email', 'super@admin.com')->first();
        $this->AdminUser = User::where('email', 'admin@admin.com')->first();

        // create a new role
        $NewRole = Role::updateOrCreate(['name' => 'Irure enim adipisicing amet'], [
            'parentrole_id' => $this->AdminUser->role->id
        ]);
    }

    protected function tearDown(): void {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Role::truncate();

        parent::tearDown();
    }

    /** @test */
    public function test_to_check_super_users_can_see_all_roles() {
        Auth::loginUsingId($this->SuperUser->id, true);

        $this->RoleManager = new RoleManager;
        $this->assertEquals(3, count($this->RoleManager->getRoles()));
    }

    /** @test */
    public function test_to_check_roles_only_see_there_own_and_children() {
        Auth::loginUsingId($this->AdminUser->id, true);

        $this->RoleManager = new RoleManager;
        $this->assertEquals(2, count($this->RoleManager->getRoles()));
    }
}