<?php namespace Hummingbird\Tests\Unit\Traits;

use Artisan;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;
use Hummingbird\Tests\Models\CustomUserEntry;

class AnonymiserTraitTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        Artisan::call('migrate', [
            '--path' => __DIR__ . '/../../Database', 
            '--force' => true
        ]);
    }

    /** @test */
    public function test_to_check_fields_are_anonymised() {
        $faker = Faker::create('en_GB');

        $CustomUserEntry = CustomUserEntry::updateOrCreate(['email' => $faker->email], [
            'salutation' => $faker->title,
            'name' => $faker->name
        ]);

        $CustomUserEntry->anonymise();

        $this->assertEquals('*DELETED*', $CustomUserEntry->salutation);
        $this->assertEquals('*DELETED*', $CustomUserEntry->name);
        $this->assertEquals('*DELETED*', $CustomUserEntry->email);
        $this->assertNull($CustomUserEntry->dob);
    }
}