<?php namespace Hummingbird\Tests\Unit\Helpers;

use Config;
use Hummingbird\Jobs\Frontend\ExternalLinkTargets;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class ExternalLinkTargetTest extends HummingbirdTestCase {
    /**
     * Setting up base test class
     */
    protected function setUp(): void {
        parent::setUp();

        Config::set('app.url', 'https://www.php-unit.co.uk');
        $this->ExternalLinkTargets = new ExternalLinkTargets;
    }

    /** @test */
    public function test_that_external_links_target_is_not_updated_if_env_not_set() {
        putenv("EXTERNAL_LINKS_TARGET");

        $PageContent = trim( $this->ExternalLinkTargets->handle('<html><a href="https://www.google.co.uk"></a></html>') );
        $this->assertEquals('<html><a href="https://www.google.co.uk"></a></html>', $PageContent);
    }

    /** @test */
    public function test_that_external_links_target_matches_what_has_been_set() {
        putenv("EXTERNAL_LINKS_TARGET=_blank");

        $PageContent = trim( $this->ExternalLinkTargets->handle('<html><a href="https://www.google.co.uk"></a></html>') );
        $this->assertEquals('<html><a href="https://www.google.co.uk" target="_blank"></a></html>', $PageContent);
    }

    /** @test */
    public function test_that_external_links_target_remains_rather_than_being_changed() {
        putenv("EXTERNAL_LINKS_TARGET=_top");

        $PageContent = trim( $this->ExternalLinkTargets->handle('<html><a href="https://www.google.co.uk" target="_blank"></a></html>'));
        $this->assertEquals('<html><a href="https://www.google.co.uk" target="_blank"></a></html>', $PageContent);
    }

    /** @test */
    public function test_that_standard_internal_links_remain_unchanged() {
        putenv("EXTERNAL_LINKS_TARGET=_top");

        $PageContent = trim( $this->ExternalLinkTargets->handle('<html><a href="/abc"></a></html>'));
        $this->assertEquals('<html><a href="/abc"></a></html>', $PageContent);
    }
}
