<?php namespace Hummingbird\Tests\Unit\Helpers;

use ArrayHelper;
use Config;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class HelpersTest extends HummingbirdTestCase {

    /** @test */
    public function test_emails_passed_as_array_are_cleaned_and_returned() {
        $Emails = ['my@email.co.uk', 'your@email.co.uk'];
        $CleanedEmails = ArrayHelper::cleanExplodedArray( explode(",", check_and_clean_emails( $Emails )) );

        $this->assertCount(2, $CleanedEmails);
    }
    
    /** @test */
    public function test_emails_passed_as_array_with_invalid_emails_removed() {
        $Emails = ['my@email.co.uk', 'not an email address', 'your@email.co.uk'];
        $CleanedEmails = ArrayHelper::cleanExplodedArray( explode(",", check_and_clean_emails( $Emails )) );

        $this->assertNotContains('not an email address', $CleanedEmails);
    }
    
    /** @test */
    public function test_emails_passed_as_string_are_cleaned_and_returned() {
        $Emails = 'my@email.co.uk, your@email.co.uk';
        $CleanedEmails = ArrayHelper::cleanExplodedArray( explode(",", check_and_clean_emails( $Emails )) );

        $this->assertCount(2, $CleanedEmails);
    }
    
    /** @test */
    public function test_emails_passed_as_string_with_invalid_emails_removed() {
        $Emails = 'my@email.co.uk, not an email address, your@email.co.uk';
        $CleanedEmails = ArrayHelper::cleanExplodedArray( explode(",", check_and_clean_emails( $Emails )) );

        $this->assertNotContains('not an email address', $CleanedEmails);
    }
}
