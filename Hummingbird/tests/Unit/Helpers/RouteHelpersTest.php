<?php namespace Hummingbird\Tests\Unit\Helpers;

use \Hummingbird\Tests\TestCase as HummingbirdTestCase;

class RouteHelpersTest extends HummingbirdTestCase {
    /** @test */
    public function test_to_check_empty_array_is_returned_with_no_url_passed_to_getQueryStringParameters() {
        $this->assertEmpty( getQueryStringParameters() );
    }

    /** @test */
    public function test_to_check_empty_array_is_returned_with_no_query_strings_with_url_passed_to_getQueryStringParameters() {
        $this->assertEmpty( getQueryStringParameters('https://www.google.co.uk') );
    }

    /** @test */
    public function test_to_check_array_is_returned_with_url_passed_to_getQueryStringParameters() {
        $this->assertEquals(1, count( getQueryStringParameters('https://www.google.co.uk?version=123456') ) );
    }
}