<?php namespace Hummingbird\Tests\Unit\Helpers;

use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class SEOHelpersTest extends HummingbirdTestCase {
    /** @test */
    public function test_to_check_string_is_checked_when_cleaning_robot_tags() {
        $this->assertEquals( 'index, noindex', clean_meta_robot_tags('index,noindex') );
    }

    /** @test */
    public function test_to_check_array_is_checked_when_cleaning_robot_tags() {
        $this->assertEquals( 'index, noindex', clean_meta_robot_tags(['index', 'noindex']) );
    }

    /** @test */
    public function test_to_check_nullable_item_returns_empty_array_when_cleaning_robot_tags() {
        $this->assertEquals('', clean_meta_robot_tags(null) );
    }

    /** @test */
    public function test_to_check_empty_string_returns_empty_array_when_cleaning_robot_tags() {
        $this->assertEquals('', clean_meta_robot_tags('') );
    }
}
