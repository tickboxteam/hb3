<?php namespace Hummingbird\Tests\Unit\Helpers;

use General;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class LinkHelpersTest extends HummingbirdTestCase {
    public function test_link_is_external() {
        $this->assertTrue( isLinkExternal('https://www.google.com') );
    }

    public function test_link_is_external_when_url_has_no_http_prefix() {
        $this->assertTrue( isLinkExternal('www.google.com') );
    }

    public function test_link_is_external_when_url_has_no_schema_or_subdomain() {
        $this->assertTrue( isLinkExternal('google.com') );
    }
    
    public function test_link_is_external_when_url_has_domain_in_it() {
        $this->assertTrue( isLinkExternal('google.com') );
    }

    public function test_link_is_internal() {
        $this->assertFalse( isLinkExternal('/test-page/') );
    }

    /**
     * HB3-26086648: Bug for link_helpers.php, when trying to check if images exist
     */
    public function test_image_source_is_internal() {
        $this->assertFalse( isLinkExternal( '/app/hummingbird/Tests/Resources/images/no-image.jpg' ) );
    }

    public function test_link_with_domain_is_internal() {
        $this->assertFalse( isLinkExternal( config('app.url') . '/test-page/') );
    }

    /** 
     * 
     * @test
     * @since v1.8.8
     */
    public function test_link_with_domain_is_internal_without_host() {
        config(['app.url' => 'mydomain.co.uk']);
        validate_app_url();

        $this->assertFalse( isLinkExternal( config('app.url') . '/test-page/') );
    }

    public function test_plain_text_email_is_obfuscated() {
        $ObfuscatedEmail = General::email_obfuscation('test@example.com');

        $this->assertNotEquals('test@example.com', $ObfuscatedEmail);
        $this->assertStringNotContainsString('@example.com', $ObfuscatedEmail);
    }

    public function test_mailto_email_is_obsfucated() {
        $ObfuscatedMailto = General::email_obfuscation('<a href="mailto:test@example.com">mailto:test@example.com</a>');

        $this->assertStringNotContainsString('mailto:', $ObfuscatedMailto);
        $this->assertNotSame('<a href="mailto:test@example.com">mailto:test@example.com</a>', $ObfuscatedMailto);
    }

    /** @test */
    public function test_to_check_that_we_can_clean_and_sluggify_a_url() {
        $this->assertEquals('/foo', clean_and_sluggify_url( 'foo/' ) );
        $this->assertEquals('/foo/bar', clean_and_sluggify_url( 'foo/bar/' ) );
        $this->assertEquals('/foo/bar/baz', clean_and_sluggify_url( 'foo/bar/baz/' ) );
        $this->assertEquals('/aliqua-velit-sed/foo', clean_and_sluggify_url( '/Aliqua velit sed/foo/' ) );
    }

    /** @test */
    public function test_to_check_that_we_can_clean_and_sluggify_a_url_and_maintain_query_parameters() {
        $this->assertEquals('/foo/bar?utm_source=baz&utm_medium=Foo+Bar+Baz&utm_campaign=Baz+Bar&utm_id=Foo', clean_and_sluggify_url( 'foo/bar/?utm_source=baz&utm_medium=Foo+Bar+Baz&utm_campaign=Baz+Bar&utm_id=Foo' ) );
    }

    /** @test */
    public function test_to_check_that_url_generating_persists_document_extensions() {
        $this->assertEquals('/foo/bar/document.pdf', clean_and_sluggify_url( 'foo/bar/document.pdf' ) );
    }

    /** @test */
    public function test_to_validate_a_route_exists_and_true_is_returned() {
        $this->assertTrue( route_exists('hummingbird.login') );
    }

    /** @test */
    public function test_to_check_if_a_route_is_invalid_then_false_is_returned() {
        $this->assertFalse( route_exists('invalid.route.name') );
    }

    /** @test */
    public function test_external_link_that_is_fully_formed() {
        $this->assertEquals('https://www.google.com/tickbox-marketing/', clean_and_sluggify_url( 'https://www.google.com/tickbox-marketing/') );
    }

    /** @test */
    public function test_external_link_can_be_slugged_properly() {
        $this->assertEquals('https://www.google.com/tickbox-marketing', clean_and_sluggify_url( ' https://www.google.com/Tickbox Marketing ') );
    }

    /** @test */
    public function test_external_link_can_be_slugged_properly_with_trailing_slash() {
        $this->assertEquals('https://www.google.com/tickbox-marketing/', clean_and_sluggify_url( ' https://www.google.com/Tickbox Marketing/ ') );
    }

    /** @test */
    public function test_external_link_can_be_slugged_properly_with_double_trailing_slash() {
        $this->assertEquals('https://www.google.com/tickbox-marketing/', clean_and_sluggify_url( ' https://www.google.com/Tickbox Marketing// ') );
    }

    /** @test */
    public function test_external_link_can_be_slugged_properly_with_no_schema() {
        $this->assertEquals('https://www.google.com/tickbox-marketing', clean_and_sluggify_url( ' www.google.com/Tickbox Marketing ') );
    }

    /** @test */
    public function test_external_link_can_be_slugged_properly_without_www() {
        $this->assertEquals('https://google.com/tickbox-marketing', clean_and_sluggify_url( ' google.com/Tickbox Marketing ') );
    }

    /** @test */
    public function test_external_link_can_be_slugged_properly_with_querystrings() {
        $this->assertEquals('https://www.google.com/tickbox-marketing?my-first-query-string=my-value', clean_and_sluggify_url( ' https://www.google.com/Tickbox Marketing?my-first-query-string=my-value ') );
    }

    /** @test */
    public function test_external_link_can_be_slugged_properly_with_querystrings_and_trailing_slash() {
        $this->assertEquals('https://www.google.com/tickbox-marketing/?my-first-query-string=my-value', clean_and_sluggify_url( ' https://www.google.com/Tickbox Marketing/?my-first-query-string=my-value ') );
    }

    /**
     * HB3-26253508: Redirects: anchor tag with id is being erased
     */
    public function test_redirects_where_anchor_id_is_present_and_is_not_removed() {
        $this->assertEquals('https://www.google.com/tickbox-marketing/#test', clean_and_sluggify_url('https://www.google.com/tickbox-marketing/#test') );
    }

    /**
     * HB3-26230658: Not cleaning if the link is a JS based action (for Menu builder)
     * 
     * @test
     */
    public function test_links_are_not_cleaned_if_they_are_javascript_links() {
        $this->assertEquals('javascript:window.location.reload();', clean_and_sluggify_url('javascript:window.location.reload();') );
    }

    /**
     * HB3-26501825: Not cleaning if the link is a JS based action (for Menu builder)
     * URL example: /work-with-us/work-for-us/vacancies-by-location/?id=1/
     * 
     * @test
     */
    public function test_regression_cleaning_urls_with_querystrings_and_a_trailing_slash() {
        $this->assertEquals('/work-with-us/work-for-us/vacancies-by-location?id=1', clean_and_sluggify_url( '/work-with-us/work-for-us/vacancies-by-location/?id=1/' ) );
    }

    /**
     * HB3-26501825: Not cleaning if the link is a JS based action (for Menu builder)
     * URL example: //wetland-centres/caerlaverock/experience/
     * 
     * @test
     */
    public function test_regression_cleaning_urls_from_double_slashes_at_the_front() {
        $this->assertEquals('/wetland-centres/caerlaverock/experience/', clean_and_sluggify_url( '//wetland-centres/caerlaverock/experience/' ) );
    }
}