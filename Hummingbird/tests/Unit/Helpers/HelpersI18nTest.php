<?php namespace Hummingbird\Tests\Unit\Helpers;

use \Hummingbird\Tests\TestCase as HummingbirdTestCase;

class HelpersI18nTest extends HummingbirdTestCase {
    /** @test */
    public function test_to_see_if_language_file_can_be_returned_if_found() {
        $this->assertEquals( trans_fb('passwords.token'), 'This password reset token is invalid.' );
    }

    /** @test */
    public function test_to_see_if_original_key_is_returned_if_value_not_found_and_fallback_not_provided() {
        $this->assertEquals( trans_fb('passwords.tokens'), 'passwords.tokens' );
    }

    /** @test */
    public function test_to_see_if_fallback_language_value_is_returned_if_not_found() {
        $this->assertEquals( trans_fb('passwords.tokens', 'This password reset token is invalid so please try again.'), 'This password reset token is invalid so please try again.' );
    }
}
