<?php namespace Hummingbird\Tests\Unit\Helpers;

use \Hummingbird\Tests\TestCase as HummingbirdTestCase;

class DateHelpersTest extends HummingbirdTestCase {
    /** @test */
    public function test_to_check_we_can_superscript_to_dates() {
        $this->assertEquals('1<sup>st</sup>', add_superscript_to_dates( 1 ) );
    }

    /** @test */
    public function test_to_check_we_can_superscript_to_dates_without_sup_tag() {
        $this->assertEquals('1st', add_superscript_to_dates( 1, false ) );
    }
}