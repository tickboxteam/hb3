<?php namespace Hummingbird\Tests\Unit\Helpers;

use Config;
use Hummingbird\Models\Setting;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SystemHelpersTest extends HummingbirdTestCase {
    protected function tearDown(): void {
        Setting::updateOrCreate(['key' => 'system'], [
            'value' => serialize([])
        ]);

        parent::tearDown();
    }

    public function test_admin_helper_can_return_correct_system_version() {
        $this->assertNotNull( system_version() );

        $this->assertEquals('3.6.2', system_version() );
    }

    public function test_admin_helper_can_return_correct_debug_value() {
        Config::set('app.debug', true);
        $this->assertTrue( site_is_in_debug_mode() );

        Config::set('app.debug', false);
        $this->assertFalse( site_is_in_debug_mode() );
    }

    /** @test */
    public function test_to_check_the_standard_php_path_is_returned() {
        putenv('PHP_LOCATION');
        $this->assertEquals('php', get_system_php_location() );
    }

    /** @test */
    public function test_to_check_the_env_php_path_is_returned() {
        putenv('PHP_LOCATION=/path/to/custom/php');
        $this->assertEquals('/path/to/custom/php', get_system_php_location() );
    }

    /** @test */
    public function test_to_check_the_standard_composer_path_is_returned() {
        putenv('COMPOSER_LOCATION');
        $this->assertEquals('composer', get_system_composer_location() );
    }

    /** @test */
    public function get_system_composer_location() {
        putenv('COMPOSER_LOCATION=/path/to/custom/composer');
        $this->assertEquals('/path/to/custom/composer', get_system_composer_location() );
    }

    /** @test */
    public function test_to_check_the_standard_artisan_path_is_returned() {
        putenv('ARTISAN_LOCATION');
        $this->assertEquals('artisan', get_system_artisan_location() );
    }

    /** @test */
    public function get_system_artisan_location() {
        putenv('ARTISAN_LOCATION=/path/to/custom/artisan');
        $this->assertEquals('/path/to/custom/artisan', get_system_artisan_location() );
    }








    /** @test */
    public function test_site_is_not_restricted() {
        putenv('APP_RESTRICTED');
        
        $this->assertFalse( is_site_restricted() );
    }

    /** @test */
    public function test_site_is_restricted_by_env() {
        putenv('APP_RESTRICTED=true');
        
        $this->assertTrue( is_site_restricted() );
    }

    /** @test */
    public function test_site_is_restricted_by_settings() {
        putenv('APP_RESTRICTED');
        putenv('APP_RESTRICTED_REDIRECT_TO');
        
        Setting::updateOrCreate(['key' => 'system'], [
            'value' => serialize([
                'APP_RESTRICTED' => 'true'
            ])
        ]);
        
        $this->assertTrue( is_site_restricted() );
    }

    /** @test */
    public function test_site_throws_401_authorized_error_if_no_redirect_set() {
        putenv('APP_RESTRICTED=true');
        putenv('APP_RESTRICTED_REDIRECT_TO');
        putenv('APP_RESTRICTED_IPS');

        $this->expectException(HttpException::class);
        can_access_site();
    }

    /** @test */
    public function test_site_redirects_if_restriction_set_in_env() {
        putenv('APP_RESTRICTED=true');
        putenv('APP_RESTRICTED_REDIRECT_TO=https://www.google.co.uk');

        $Response = can_access_site();

        $this->assertEquals('https://www.google.co.uk', $Response->getTargetUrl());
    }

    /** @test */
    public function test_site_redirects_if_restriction_set_in_settings() {
        putenv('APP_RESTRICTED');
        putenv('APP_RESTRICTED_REDIRECT_TO=https://www.google.co.uk');

        Setting::updateOrCreate(['key' => 'system'], [
            'value' => serialize([
                'APP_RESTRICTED' => 'true',
                'APP_RESTRICTED_REDIRECT_TO' => 'https://www.tickboxmarketing.co.uk'
            ])
        ]);

        $Response = can_access_site();

        $this->assertEquals('https://www.tickboxmarketing.co.uk', $Response->getTargetUrl());
    }

    /** @test */
    public function test_env_returns_correct_array_of_restricted_ip_addresses() {
        putenv('APP_RESTRICTED_IPS=192.168.0.1');

        $RestrictedIPs = get_system_access_ip_addresses();
        $this->assertEquals( 1, count( $RestrictedIPs ) );
        $this->assertTrue( in_array('192.168.0.1', $RestrictedIPs) );
    }

    /** @test */
    public function test_env_returns_correct_array_of_restricted_ip_addresses_by_removing_invalid_entries() {
        putenv('APP_RESTRICTED_IPS=192.168.0.1,xyx,,192.168.1.1');

        $RestrictedIPs = get_system_access_ip_addresses();
        $this->assertEquals( 2, count( $RestrictedIPs ) );
        $this->assertTrue( in_array('192.168.0.1', $RestrictedIPs) );
    }

    /** @test */
    public function test_env_returns_correct_array_of_restricted_ip_addresses_by_duplicates() {
        putenv('APP_RESTRICTED_IPS=192.168.0.1,192.168.0.1');

        $RestrictedIPs = get_system_access_ip_addresses();
        $this->assertEquals( 1, count( $RestrictedIPs ) );
    }

    /** @test */
    public function test_cms_settings_returns_correct_array_of_restricted_ip_addresses() {
        putenv('APP_RESTRICTED_IPS=');
        
        Setting::updateOrCreate(['key' => 'system'], [
            'value' => serialize([
                'APP_RESTRICTED_IPS' => '192.168.0.1'
            ])
        ]);

        $RestrictedIPs = get_system_access_ip_addresses();
        $this->assertEquals( 1, count( $RestrictedIPs ) );
        $this->assertTrue( in_array('192.168.0.1', $RestrictedIPs) );
    }

    /** @test */
    public function test_restricted_ip_addresses_are_merged_between_env_and_system_settings() {
        putenv('APP_RESTRICTED_IPS=192.168.0.1');
        
        Setting::updateOrCreate(['key' => 'system'], [
            'value' => serialize([
                'APP_RESTRICTED_IPS' => '192.168.0.2'
            ])
        ]);

        $RestrictedIPs = get_system_access_ip_addresses();
        $this->assertEquals( 2, count( $RestrictedIPs ) );
        $this->assertTrue( in_array('192.168.0.1', $RestrictedIPs) );
        $this->assertTrue( in_array('192.168.0.2', $RestrictedIPs) );
    }

    /** @test */
    public function test_site_access_allowed_if_ip_is_in_safe_range() {
        putenv('APP_RESTRICTED=true');
        putenv('APP_RESTRICTED_REDIRECT_TO=https://www.google.co.uk');
        putenv('APP_RESTRICTED_IPS=1.1.1.1');

        // Set this to something we can verify
        $_SERVER["REMOTE_ADDR"] = '1.1.1.1';

        $this->assertTrue( can_access_site() );
    }

    /** @test */
    public function test_site_access_not_allowed_and_is_redirected_if_ip_not_in_safe_range() {
        putenv('APP_RESTRICTED=true');
        putenv('APP_RESTRICTED_REDIRECT_TO=https://www.google.co.uk');
        putenv('APP_RESTRICTED_IPS=1.1.1.1');

        // Set this to something we can verify
        $_SERVER["REMOTE_ADDR"] = '1.1.1.2';

        $Response = can_access_site();

        $this->assertEquals('https://www.google.co.uk', $Response->getTargetUrl());
    }

    /** @test */
    public function test_backend_url_is_restricted_to_all() {
        Setting::updateOrCreate(['key' => 'system'], [
            'value' => serialize([
                'APP_RESTRICTED' => 'false',
                'APP_RESTRICTED_REDIRECT_TO' => '',
                'APP_RESTRICTED_IPS' => '',
                'APP_BACKEND_RESTRICTED' => 'true',
                'APP_BACKEND_RESTRICTED_IPS' => ''
            ])
        ]);

        $response = $this->call('GET', route('hummingbird.login'));
        $response->assertStatus(302);
    }

    /** @test */
    public function test_backend_url_is_restricted_to_set_ip_addresses() {
        $_SERVER["REMOTE_ADDR"] = '1.1.1.2';
        $this->serverVariables = ['REMOTE_ADDR' => '1.1.1.2'];

        Setting::updateOrCreate(['key' => 'system'], [
            'value' => serialize([
                'APP_RESTRICTED' => 'false',
                'APP_RESTRICTED_REDIRECT_TO' => '',
                'APP_RESTRICTED_IPS' => '',
                'APP_BACKEND_RESTRICTED' => 'true',
                'APP_BACKEND_RESTRICTED_IPS' => '1.1.1.1'
            ])
        ]);

        $response = $this->call('GET', route('hummingbird.login'), [], [], [], ['REMOTE_ADDR' => '1.1.1.2']);
        $response->assertStatus(302);
    }

    /** @test */
    public function test_backend_url_is_available_to_set_ip_addresses() {
        $_SERVER["REMOTE_ADDR"] = '1.1.1.1';
        $this->serverVariables = ['REMOTE_ADDR' => '1.1.1.1'];

        Setting::updateOrCreate(['key' => 'system'], [
            'value' => serialize([
                'APP_RESTRICTED' => 'false',
                'APP_RESTRICTED_REDIRECT_TO' => '',
                'APP_RESTRICTED_IPS' => '',
                'APP_BACKEND_RESTRICTED' => 'true',
                'APP_BACKEND_RESTRICTED_IPS' => '1.1.1.1'
            ])
        ]);

        // Set this to something we can verify
        $response = $this->call('GET', route('hummingbird.login'), [], [], [], ['REMOTE_ADDR' => '1.1.1.1']);
        $response->assertStatus(200);
    }

    /** @test */
    public function test_backend_url_is_open_to_all() {
        Setting::updateOrCreate(['key' => 'system'], [
            'value' => serialize([
                'APP_RESTRICTED' => 'false',
                'APP_RESTRICTED_REDIRECT_TO' => '',
                'APP_RESTRICTED_IPS' => '',
                'APP_BACKEND_RESTRICTED' => 'false',
                'APP_BACKEND_RESTRICTED_IPS' => ''
            ])
        ]);

        // Set this to something we can verify
        $response = $this->call('GET', route('hummingbird.login'));
        $response->assertStatus(200);
    }
}
