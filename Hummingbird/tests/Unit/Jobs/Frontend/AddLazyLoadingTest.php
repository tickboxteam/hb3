<?php namespace Hummingbird\Tests\Unit\Jobs\Frontend;

use Hummingbird\Jobs\Frontend\AddLazyLoading;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class AddLazyLoadingTest extends HummingbirdTestCase {
    /** @test */
    protected function setUp(): void {
        parent::setUp();

        $this->LazyLoader = new AddLazyLoading;
        $this->content = '<main class="main--content" style="margin-top: 140px;">
            <div class="container px-6 2xl:px-0 max-w-screen-lg pb-15 lg:pb-20">
                <div id="page_header" class="pt-8 pb-5 md:pb-8 md:pt-13">
                    <h1>Tickbox Marketing</h1>
                </div>

                <div id="hb-template-wrapper">
                    <img src="/Resources/images/seo-fb.png" />
                </div>
            </div>
        </main>';
    }

    /** @test */
    public function test_to_check_lazy_loading_is_ignored_when_disabled() {
        putenv('media_lazy_loading=false');
        putenv('media_lazy_loading_exclude_paths=');

        $UpdatedContent = $this->LazyLoader->handle( $this->content );

        $this->assertFalse( strpos($UpdatedContent, 'loading="lazy"') !== FALSE );
    }

    /** @test */
    public function test_to_check_lazy_loading_is_added_when_enabled() {
        putenv('media_lazy_loading=true');
        putenv('media_lazy_loading_exclude_paths=');

        $UpdatedContent = $this->LazyLoader->handle( $this->content );

        $this->assertTrue( strpos($UpdatedContent, 'loading="lazy"') !== FALSE );
    }

    /** @test */
    public function test_to_check_lazy_loading_can_exclude_certain_paths() {
        putenv('media_lazy_loading=true');
        putenv('media_lazy_loading_exclude_paths=Resources/images');

        $UpdatedContent = $this->LazyLoader->handle( $this->content );

        $this->assertFalse( strpos($UpdatedContent, '<img src="/Resources/images/seo-fb.png" />') !== FALSE );
    }
}