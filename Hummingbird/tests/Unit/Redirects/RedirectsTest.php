<?php namespace Hummingbird\Tests\Unit\Redirects;

use Hummingbird\Models\Redirections;
use Hummingbird\Models\RedirectStat;
use Hummingbird\Jobs\Redirects\RedirectTriggered;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class RedirectsTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        $this->Redirect = Redirections::updateOrCreate([
            'from' => "/" . str_slug('This is my test URL') . "/"
        ], [
            'to' => '/foo/',
            'type' => '301'
        ]);
    }

    protected function tearDown(): void {
        RedirectStat::truncate();
        Redirections::truncate();
        
        parent::tearDown();
    }

    /** @test */
    public function test_to_check_redirect_works_on_url() {
        $this->expectsEvents(RedirectTriggered::class);
        
        $response = $this->call('GET', $this->Redirect->from);
    }

    /** @test */
    public function test_redirect_can_be_triggered_and_statistic_recorded() {
        $this->call('GET', $this->Redirect->from);

        $this->assertEquals( 1,  $this->Redirect->statistics()->count());
    }

    /** @test */
    public function test_to_check_redirect_groups_return_only_custom_category_option() {
        $this->assertNotContains('Categories', redirects_get_category_list());
    }

    /** @test */
    public function test_to_check_redirect_groups_returns_distinct_category_options() {
        $this->Redirect->category = 'My Category';
        $this->Redirect->save();

        $this->assertEquals(1, count(redirects_get_category_list()['Categories']));
    }

    /** @test */
    public function test_to_check_redirect_works_on_url_with_query_string() {
        $this->Redirect = Redirections::updateOrCreate([
            'from' => "/" . str_slug('This is my test URL') . "/"
        ], [
            'from' => "/" . str_slug('This is my test URL') . "/?tickbox=geonaa",
            'to' => '/foo/',
            'type' => '301'
        ]);

        $this->expectsEvents(RedirectTriggered::class);
     
        $this->call('GET', $this->Redirect->from);
    }

    /** @test */
    public function test_to_check_redirects_and_statistics_remove_on_force_delete() {
        $this->call('GET', $this->Redirect->from);

        $this->assertEquals(1, $this->Redirect->statistics()->count());

        $this->Redirect->forceDelete();

        $this->assertEquals(0, RedirectStat::all()->count() );
    }
}