<?php namespace Hummingbird\Tests\Unit\Auth;

use Validator;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;
use Illuminate\Validation\Rules\Password;

class PasswordStrengthTest extends HummingbirdTestCase {
    /**
     * [setPasswordRuleBroker description]
     *
     * TODO: As this is cloned from CoreServiceProvider we probably need a better way of using this
     */
    protected function setPasswordRuleBroker( $Options = [] ) {
        Password::defaults(function() use($Options) {
            $PasswordLength = (isset($Options['pass_length']) && is_numeric($Options['pass_length']) && $Options['pass_length'] >= 8) ? $Options['pass_length'] : 8;
            $Letters  = ( isset($Options['letters']) && filter_var( $Options['letters'], FILTER_VALIDATE_BOOLEAN) );
            $MixedCase  = ( isset($Options['mixed_case']) && filter_var( $Options['mixed_case'], FILTER_VALIDATE_BOOLEAN) );
            $Numbers    = ( isset($Options['pass_numbers']) && filter_var( $Options['pass_numbers'], FILTER_VALIDATE_BOOLEAN) );
            $Symbols    = ( isset($Options['pass_symbols']) && filter_var( $Options['pass_symbols'], FILTER_VALIDATE_BOOLEAN) );
            $Uncompromised = ( isset($Options['pass_compromised']) && filter_var( $Options['pass_compromised'], FILTER_VALIDATE_BOOLEAN) );

            $PasswordRule = Password::min( $PasswordLength );

            if( $Letters ) {
                $PasswordRule->letters();
            }

            if( $MixedCase ) {
                $PasswordRule->mixedCase();
            }

            if( $Numbers ) {
                $PasswordRule->numbers();
            }

            if( $Symbols ) {
                $PasswordRule->symbols();
            }

            if( $Uncompromised ) {
                $PasswordRule->uncompromised();
            }

            return $PasswordRule;
        });
    }

    /**
     * Standard function for validating
     * @param  String $password
     * @return Validator
     */
    protected function validatePassword( $password ) {
        return Validator::make([ 'password' => $password ], [
            'password' => [ Password::defaults() ]
        ]);
    }

    /** @test */
    public function test_to_check_default_system_password_validates() {
        $this->setPasswordRuleBroker([]);

        // Validate a password 7 characters in length - will fail
        $this->assertFalse( $this->validatePassword('1234567')->passes() );

        // Now validate a password 8 characters in length - will pass
        $this->assertTrue( $this->validatePassword('12345678')->passes() );
    }

    // min(8)
    /** @test */
    public function test_set_password_length_to_less_than_8_invalidates() {
        $this->setPasswordRuleBroker([
            'pass_length' => 7
        ]);

        // Validate a password 7 characters in length - will fail
        $this->assertFalse( $this->validatePassword('1234567')->passes() );
    }

    /** @test */
    public function test_set_password_length_to_a_non_numeric_value_will_default_to_8_characters_in_length() {
        $this->setPasswordRuleBroker([
            'pass_length' => 'xxx'
        ]);

        // Validate a password 7 characters in length - will fail
        $this->assertFalse( $this->validatePassword('1234567')->passes() );

        // Now validate a password 8 characters in length - will pass
        $this->assertTrue( $this->validatePassword('12345678')->passes() );
    }


    // letters
    /** @test */
    public function test_to_check_that_passwords_with_or_without_letters_validates() {
        $this->setPasswordRuleBroker([
            'letters' => 'false'
        ]);

        // Validate a password without mixed case - will pass
        $this->assertTrue( $this->validatePassword('12345678')->passes() );

        // Validate a password with mixed case - will pass
        $this->assertTrue( $this->validatePassword('password123')->passes() );
    }

    /** @test */
    public function test_to_check_letters_validates() {
        $this->setPasswordRuleBroker([
            'letters' => 'true'
        ]);

        // Validate a password without letters - will fail
        $this->assertFalse( $this->validatePassword('12345678')->passes() );

        // Validate a password with letters - will pass
        $this->assertTrue( $this->validatePassword('a1234567')->passes() );
    }


    // mixedCase
    /** @test */
    public function test_to_check_that_passwords_with_or_without_mixed_case_validates() {
        $this->setPasswordRuleBroker([
            'mixed_case' => 'false'
        ]);

        // Validate a password without mixed case - will pass
        $this->assertTrue( $this->validatePassword('password')->passes() );

        // Validate a password with mixed case - will pass
        $this->assertTrue( $this->validatePassword('paSSword')->passes() );
    }

    /** @test */
    public function test_to_check_mixed_case_validates() {
        $this->setPasswordRuleBroker([
            'mixed_case' => 'true'
        ]);

        // Validate a password without mixed case - will fail
        $this->assertFalse( $this->validatePassword('password')->passes() );

        // Validate a password with mixed case - will pass
        $this->assertTrue( $this->validatePassword('paSSword')->passes() );
    }


    // numbers
    /** @test */
    public function test_to_check_that_passwords_with_or_without_numbers_validates() {
        $this->setPasswordRuleBroker([
            'pass_numbers' => 'false'
        ]);

        // Validate a password without numbers - will pass
        $this->assertTrue( $this->validatePassword('password')->passes() );

        // Validate a password with numbers - will pass
        $this->assertTrue( $this->validatePassword('password123')->passes() );
    }

    /** @test */
    public function test_to_check_numbers_validates() {
        $this->setPasswordRuleBroker([
            'pass_numbers' => 'true'
        ]);

        // Validate a password without mixed case - will fail
        $this->assertFalse( $this->validatePassword('password')->passes() );

        // Validate a password with mixed case - will pass
        $this->assertTrue( $this->validatePassword('password123')->passes() );
    }


    // symbols
    /** @test */
    public function test_to_check_that_passwords_with_or_without_symbols_validates() {
        $this->setPasswordRuleBroker([
            'pass_symbols' => 'false'
        ]);

        // Validate a password without a symbol - will pass
        $this->assertTrue( $this->validatePassword('password')->passes() );

        // Validate a password with a symbol - will pass
        $this->assertTrue( $this->validatePassword('password!')->passes() );
    }

    /** @test */
    public function test_to_check_symbols_validates() {
        $this->setPasswordRuleBroker([
            'pass_symbols' => 'true'
        ]);

        // Validate a password without mixed case - will fail
        $this->assertFalse( $this->validatePassword('password')->passes() );

        // Validate a password with mixed case - will pass
        $this->assertTrue( $this->validatePassword('password!')->passes() );
    }


    // compromised
    /** @test */
    public function test_to_check_that_compromised_passwords_can_be_used_when_disabled() {
        $this->setPasswordRuleBroker([
            'pass_compromised' => 'false'
        ]);

        // Validate a password without mixed case - will fail
        $this->assertTrue( $this->validatePassword('password')->passes() );
    }

    /** @test */
    public function test_to_check_that_compromised_passwords_invalidates() {
        $this->setPasswordRuleBroker([
            'pass_compromised' => 'true'
        ]);

        // Validate a password without mixed case - will fail
        $this->assertFalse( $this->validatePassword('password')->passes() );
    }

    /** @test */
    public function test_to_check_non_compromised_passwords_validates() {
        $this->setPasswordRuleBroker([
            'pass_compromised' => 'true'
        ]);

        // Validate a password with mixed case - will pass
        $this->assertTrue( $this->validatePassword('abCDeFgH123!')->passes() );
    }
}