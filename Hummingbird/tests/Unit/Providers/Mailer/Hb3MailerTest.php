<?php namespace Hummingbird\Tests\Unit\Providers\Mailer;

use Mail;
use Hummingbird\Providers\Mailer\Hb3Mailer;
use Hummingbird\Providers\Mailer\Exceptions\Hb3MailerException;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class Hb3MailerTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();
        
        $this->Mailer = new Hb3Mailer;
    }

    /* @test */
    public function test_to_check_we_can_set_a_new_view() {
        $this->Mailer->setView('HummingbirdBase::shortcodes.email_preview_partial');
        $this->assertEquals('HummingbirdBase::shortcodes.email_preview_partial', $this->Mailer->getView());
    }

    /* @test */
    public function test_to_check_that_a_view_must_exist_before_it_can_be_overriden() {
        $this->Mailer->setView('this.is.my.new.view');
        $this->assertNull($this->Mailer->getView());
    }

    /* @test */
    public function test_to_check_we_can_change_the_subject() {
        $this->Mailer->setSubject('New subject');
        $this->assertEquals('New subject', $this->Mailer->getSubject());
    }

    /* @test */
    public function test_to_check_we_can_set_the_email_type() {
        $this->Mailer->setEmailType('New email type');
        $this->assertEquals('New email type', $this->Mailer->getEmailType());
    }

    /* @test */
    public function test_to_check_we_can_add_email_data() {
        $this->Mailer->addMailData(['Page title' => 'Adipisicing ut incididunt adipisicing.']);
        $this->assertEquals('Adipisicing ut incididunt adipisicing.', $this->Mailer->getMailData()['Page title']);
    }

    /* @test */
    public function test_to_check_email_data_must_be_an_array() {
        $this->expectException(Hb3MailerException::class);
        $this->Mailer->addMailData('foo');
    }

    /* @test */
    public function test_add_recipient_to_list() {
        $this->Mailer->addRecipient('test@test.co.uk');
        $this->assertEquals('1', count($this->Mailer->getRecipients()));
    }

    public function test_check_to_make_sure_recipients_cannot_be_duplicated() {
        $this->Mailer->addRecipient('test@test.co.uk');
        $this->Mailer->addRecipient('test@test.co.uk');
        $this->assertEquals('1', count($this->Mailer->getRecipients()));
    }

    public function test_to_check_invalid_emails_are_not_added_to_the_recipients_list() {
        $this->Mailer->addRecipient('test@test.co.uk');
        $this->Mailer->addRecipient('test');
        $this->assertEquals('1', count($this->Mailer->getRecipients()));
    }

    public function test_to_check_we_can_pass_a_comma_delimited_list_of_recipients() {
        $this->Mailer->addRecipients('test@test.co.uk, test1@test.co.uk, test2@test.co.uk ,');
        $this->assertEquals('3', count($this->Mailer->getRecipients()));
    }

    public function test_to_check_we_can_pass_an_array_of_recipients() {
        $this->Mailer->addRecipients(['test@test.co.uk', 'test1@test.co.uk', 'test2@test.co.uk']);
        $this->assertEquals('3', count($this->Mailer->getRecipients()));
    }

    /** @test */
    public function test_to_check_emails_can_not_be_sent_if_recipients_are_missing() {
        $this->expectException(Hb3MailerException::class);
        $this->Mailer->setView('HummingbirdBase::shortcodes.email_preview_partial');
        $this->Mailer->send();
    }
}