<?php namespace Hummingbird\Tests\Unit;

use Hummingbird\Models\Page;
use Hummingbird\Models\PageVersion;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class SearchablePageTestCase extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        Page::flushEventListeners();
        Page::boot();

        PageVersion::flushEventListeners();
        PageVersion::boot();
    }

    protected function tearDown(): void {
        parent::tearDown();

        Page::truncate();
        PageVersion::truncate();
    }

    /** @test */
    public function test_to_check_page_on_creation_is_set_to_searchable() {
        $Page = Page::create([
            'title' => 'test_to_check_page_on_creation_is_set_to_searchable',
            'permalink' => str_slug('test_to_check_page_on_creation_is_set_to_searchable'),
            'content' => 'Anim reprehenderit qui ea consequat sunt do labore aute dolor eu in eu in excepteur in sed ea dolore sunt dolore in cupidatat ut adipisicing consectetur proident duis est sit officia commodo pariatur ea culpa nulla eu id dolor culpa et velit anim reprehenderit et.',
            'parentpage_id' => '1',
        ]);

        $this->assertEquals( 1, $Page->searchable );
    }

    /** @test */
    public function test_to_check_page_is_NOT_searchable_if_env_is_set_to_false() {
        putenv("HB3_PAGES_SEARCHABLE='false'");
        
        $Page = Page::create([
            'title' => 'test_to_check_page_is_NOT_searchable_if_env_is_set_to_false',
            'sub_title' => 'Test',
            'summary' => 'Do labore irure sint laborum dolore in adipisicing esse.',
            'content' => 'Anim reprehenderit qui ea consequat sunt do labore aute dolor eu in eu in excepteur in sed ea dolore sunt dolore in cupidatat ut adipisicing consectetur proident duis est sit officia commodo pariatur ea culpa nulla eu id dolor culpa et velit anim reprehenderit et.',
        ]);

        $this->assertNull( $Page->searchable );
    }

    /** @test */
    public function test_to_check_page_is_searchable_if_env_is_set_to_true() {
        putenv("HB3_PAGES_SEARCHABLE=true");

        $Page = Page::create([
            'title' => 'test_to_check_page_is_searchable_if_env_is_set_to_true',
            'sub_title' => 'Test',
            'summary' => 'Do labore irure sint laborum dolore in adipisicing esse.',
            'content' => 'Anim reprehenderit qui ea consequat sunt do labore aute dolor eu in eu in excepteur in sed ea dolore sunt dolore in cupidatat ut adipisicing consectetur proident duis est sit officia commodo pariatur ea culpa nulla eu id dolor culpa et velit anim reprehenderit et.',
        ]);

        $this->assertEquals( 1, $Page->searchable );
    }
}
