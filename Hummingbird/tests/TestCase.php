<?php namespace Hummingbird\Tests;

use Session;
use Hummingbird\Tests\Traits\CreatesApplication;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase {
    use CreatesApplication;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
	protected $baseUrl;
	
	protected function setUp(): void {
	    parent::setUp();

	    $this->app['Illuminate\Contracts\Console\Kernel']->call('migrate', [
            '--force' => true,
            '--path'=> "/Hummingbird/Tests/Database/Migrations"
        ]);

        app()->make('config')->set('mail.driver', 'log');

        $this->baseUrl = config()->get('app.url');

		$this->startSession();
	}

	protected function tearDown(): void {
		$this->flushSession();
		
		parent::tearDown();
	}
}
