<?php namespace Hummingbird\Tests\Models;

use Hummingbird\Traits\AnonymiserTrait;
use Illuminate\Database\Eloquent\Model as Eloquent;

class CustomUserEntry extends Eloquent {
    use AnonymiserTrait;

    public $timestamps = false;
    public $table    = 'custom_user_entries';
    public $fillable = ['salutation', 'name', 'email'];

    /**
     * What we want to anonymise
     * @var Array
     */
    public $anonymiser = [
        'fields' => ['salutation', 'name', 'email'],
        'field_reference' => '*DELETED*'
    ];
}
