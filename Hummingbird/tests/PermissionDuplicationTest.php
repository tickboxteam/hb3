<?php namespace Hummingbird\Tests;

use Hummingbird\Models\Permission;
use Hummingbird\Models\PermissionGroup;

use \Hummingbird\Tests\TestCase as HummingbirdTestCase;

/**
 * HB3-25782951: Bug fix to correct the fact that Permissions
 * throw errors when registering permissions with the same
 * name or key name
 */
class PermissionDuplicationTest extends HummingbirdTestCase {
    public function test_permission_keys_can_not_be_duplicated()
    {
        $PermissionHandler = new \Hummingbird\Libraries\PermissionHandler;
        $PermissionGroup = PermissionGroup::name('Pages')->first();

        // Register new permission for Page tests
        $PermissionHandler->registerPermission('page.testDupPages', [ 'label' => 'Test Pages?', 'description' => 'Leave empty', 'action' => 'Tickbox.PageList.GET' ], $PermissionGroup);

        $PermissionHandler->registerPermission('page.testDupPages', [ 'label' => 'Duplicate Pages test', 'description' => 'Leave empty duplicate', 'action' => 'Tickbox.PageList.GET' ], $PermissionGroup);
        $this->assertEquals(1, Permission::name('page.testDupPages')->get()->count() );
    }
}
