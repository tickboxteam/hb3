<?php namespace Hummingbird\Tests;

use Hummingbird\Models\Permission;
use Hummingbird\Models\Role;
use Hummingbird\Models\User;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;
use Hummingbird\Database\DummySeeds\DummyRolesSeeder;
use Hummingbird\Database\DummySeeds\DummyUsersSeeder;
use Illuminate\Support\Facades\Auth;

class PermissionTest extends HummingbirdTestCase {
    protected $user;

    protected function setUp(): void {
        parent::setUp();

        $this->seed(DummyRolesSeeder::class);
        $this->seed(DummyUsersSeeder::class);

        Auth::loginUsingId( User::where('email', 'admin@admin.com')->first()->id );

        $this->user = Auth::user();
    }

    public function testNotHavingPermission() {
        Permission::firstOrCreate(['name' => 'test', 'display_name' => 'Test permission']);

        $this->assertFalse($this->user->userAbility('test'));
    }

    public function testHavingPermission()
    {
        $Permission = Permission::firstOrCreate(['name' => 'test', 'display_name' => 'Test permission']);

        $role = Role::where('name', 'admin')->first();
        $role->perms()->sync( [$Permission->id] );

        $this->assertFalse($this->user->hasPermissionAction('test'));

        $this->user->permissions()->sync( [$Permission->id] );
        $this->user->load('permissions');

        $this->assertTrue( $this->user->hasPermissionAction('test') ); // string version of permission
        $this->assertTrue( $this->user->hasPermissionAction( $Permission ) ); // object permission
        $this->assertTrue( $role->hasPermissionAction('test') ); // string version of permission
        $this->assertTrue( $role->hasPermissionAction( $Permission ) ); // object permission
    }

    public function test_permission_can_still_be_checked_even_with_blank_spaces() {
        $Permission = Permission::orderByRaw("RAND()")->take(1)->firstOrFail();
        
        $this->user->permissions()->sync( [$Permission->id] );
        
        $this->assertTrue( $this->user->userAbility( $Permission->name ) );
        $this->assertTrue( $this->user->userAbility( " {$Permission->name}" ) );
        $this->assertTrue( $this->user->userAbility( "{$Permission->name} " ) );
        $this->assertTrue( $this->user->userAbility( " {$Permission->name} " ) );
                
        $this->assertTrue( $this->user->hasPermissionAction( $Permission->name ) );
        $this->assertTrue( $this->user->hasPermissionAction( " {$Permission->name}" ) );
        $this->assertTrue( $this->user->hasPermissionAction( "{$Permission->name} " ) );
        $this->assertTrue( $this->user->hasPermissionAction( " {$Permission->name} " ) );
    }
}
