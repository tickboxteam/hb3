<?php namespace Hummingbird\Tests\Database\Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CustomUserEntryTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( !Schema::hasTable('custom_user_entries') ):
			Schema::create('custom_user_entries', function($table) {
				$table->increments('id');
				$table->string('salutation');
				$table->string('name');
				$table->string('email');
			});
		endif;
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('custom_user_entries');
	}

}
