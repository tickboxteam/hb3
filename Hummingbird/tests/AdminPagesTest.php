<?php namespace Hummingbird\Tests;

use SearchIndex;
use Hummingbird\Models\Page;
use Hummingbird\Models\Searcher;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;
use Hummingbird\Database\DummySeeds\DummyPagesSeeder;

class AdminPagesTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        $this->seed(DummyPagesSeeder::class);
    }

    public function test_can_set_summary() {
        $Page = Page::first();

        $Page->summary = 'Lorem ipsum';
        $Page->save();

        $this->assertEquals($Page->summary, 'Lorem ipsum');

        $Page->summary = '';
        $Page->save();

        $this->assertEmpty($Page->summary);
    }

    public function test_can_be_searchable() {
        $Page       = Page::where('title', 'Dummy Seed: Page')->firstOrFail();
        $Page->fill([
            'title' => 'Search title',
            'summary' => 'This is a summary',
            'searchable' => 1,
            'status' => 'public',
            'parentpage_id' => 1
        ])->save();

        SearchIndex::insertOrUpdateSearch($Page);

        $Searcher   = Searcher::where('item_id', $Page->id )->first();

        $this->assertNotNull($Searcher); // Item should be searchable
        $this->assertEquals($Page->title, $Searcher->title);
        $this->assertEquals($Page->summary, $Searcher->summary);
    }    
}
