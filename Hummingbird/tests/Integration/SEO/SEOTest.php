<?php namespace Hummingbird\Tests\Integration\SEO;

use Config;
use File;
use HB3Meta;
use Hummingbird\Models\MetaData;
use Hummingbird\Models\Page;

use \Hummingbird\Tests\TestCase as HummingbirdTestCase;

use Symfony\Component\DomCrawler\Crawler;


class SEOTest extends HummingbirdTestCase {
    protected $site_title   = 'Nulla et veniam nostrud incididunt';
    protected $site_desc    = 'Lorem ipsum quis commodo aliqua ea nisi incididunt qui tempor duis officia in ea culpa enim adipisicing culpa in aliquip et ut labore velit magna duis sint.';

    protected function setUp(): void {
        parent::setUp();

        app()->make('config')->set('app.false', false);
        putenv('SEARCH_ENGINE_DISCOURAGE=false');
        
        HB3Meta::setTitleDefault($this->site_title);
        HB3Meta::setDescription($this->site_desc);  
        HB3Meta::addMeta('og:site_name', 'This is my site_name');
    }

    protected function tearDown(): void {
        MetaData::truncate();

        parent::tearDown();
    }

    public function test_to_check_base_settings_can_be_set() {
        $this->assertEquals( $this->site_title, HB3Meta::getTitle() );
        $this->assertEquals( $this->site_desc, HB3Meta::getDescription() );
    }

    public function test_to_check_when_page_loads_so_does_correct_meta_data() {
        $Page = Page::findOrFail(1);
        $Page->fill([
            'content' => '',
            'summary' => ''
        ])->save();

        // Here we are going to check that the SEO renders how we would expect
        $response = $this->call('GET', '/');
        $crawler = new Crawler($response->getContent());

        $this->assertStringContainsString( $this->site_title, $crawler->filterXPath('//title')->text());
        $this->assertEquals( $this->site_desc, $crawler->filter('meta[name="description"]')->eq(0)->attr('content'));
    }

    public function test_to_check_we_can_override_page_specific_meta_data() {
        putenv('APP_DEBUG=false');
        config()->set('app.debug', false);

        $Page = Page::findOrFail(1);
        $Page->featured_image = url('/app/hummingbird/tests/Resources/images/seo.png');
        $Page->meta()->saveMany([
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_title', 'value' => 'New Title')),
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_description', 'value' => 'Description override')),
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_robots', 'value' => 'none')),
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_twitter_card', 'value' => 'summary_large_image'))
        ]);
        $Page->save();

        // Here we are going to check that the SEO renders how we would expect
        $response = $this->call('GET', '/');
        $crawler = new Crawler($response->getContent());

        $this->assertEquals( "New Title - {$this->site_title}", $crawler->filterXPath('//title')->text());
        $this->assertEquals( 'Description override', $crawler->filter('meta[name="description"]')->eq(0)->attr('content'));
        $this->assertEquals( 'none', $crawler->filter('meta[name="robots"]')->eq(0)->attr('content'));

        // Checking OpenGraph
        $this->assertEquals( 'website', $crawler->filter('meta[name="og:type"]')->eq(0)->attr('content'));
        $this->assertEquals( 'en_GB', $crawler->filter('meta[name="og:locale"]')->eq(0)->attr('content'));
        $this->assertEquals( 'This is my site_name', $crawler->filter('meta[name="og:site_name"]')->eq(0)->attr('content'));
        $this->assertEquals( 'New Title', $crawler->filter('meta[property="og:title"]')->eq(0)->attr('content'));
        $this->assertEquals( 'Description override', $crawler->filter('meta[property="og:description"]')->eq(0)->attr('content'));
        $this->assertEquals( url($Page->permalink), $crawler->filter('meta[property="og:url"]')->eq(0)->attr('content'));
        $this->assertEquals( $Page->featured_image, $crawler->filter('meta[property="og:image"]')->eq(0)->attr('content'));

        // Checking Twitter Cards
        $this->assertEquals( 'summary_large_image', $crawler->filter('meta[name="twitter:card"]')->eq(0)->attr('content'));
        $this->assertEquals( 'New Title', $crawler->filter('meta[name="twitter:title"]')->eq(0)->attr('content'));
        $this->assertEquals( 'Description override', $crawler->filter('meta[name="twitter:description"]')->eq(0)->attr('content'));
        $this->assertEquals( $Page->featured_image, $crawler->filter('meta[name="twitter:image"]')->eq(0)->attr('content'));

        // Checking Article details
        $this->assertEquals( $Page->updated_at->format('c'), $crawler->filter('meta[name="article:modified_time"]')->eq(0)->attr('content'));
        $this->assertEquals( $Page->updated_at->format('c'), $crawler->filter('meta[name="article:published_time"]')->eq(0)->attr('content'));
    }

    public function test_check_to_make_sure_facebook_meta_data_can_be_set() {
        $FB_image = url('/app/hummingbird/tests/Resources/images/seo-fb.png');

        $Page = Page::findOrFail(1);
        $Page->featured_image = url('/app/hummingbird/tests/Resources/images/seo.png');
        $Page->meta()->saveMany([
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_facebook_title', 'value' => 'Facebook Title')),
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_facebook_description', 'value' => 'Facebook description')),
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_facebook_image', 'value' => $FB_image))
        ]);
        $Page->save();

        // Here we are going to check that the SEO renders how we would expect
        $response = $this->call('GET', '/');
        $crawler = new Crawler($response->getContent());

        // Checking OpenGraph
        $this->assertEquals( 'Facebook Title', $crawler->filter('meta[property="og:title"]')->eq(0)->attr('content'));
        $this->assertEquals( 'Facebook description', $crawler->filter('meta[property="og:description"]')->eq(0)->attr('content'));
        $this->assertEquals( $FB_image, $crawler->filter('meta[property="og:image"]')->eq(0)->attr('content'));
    }

    public function test_check_to_make_sure_twitter_meta_data_can_be_set() {
        $TW_image = url('/app/hummingbird/tests/Resources/images/seo-tw.png');

        $Page = Page::findOrFail(1);
        $Page->featured_image = url('/app/hummingbird/tests/Resources/images/seo.png');
        $Page->meta()->saveMany([
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_twitter_title', 'value' => 'Twitter Title')),
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_twitter_description', 'value' => 'Twitter description')),
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_twitter_image', 'value' => $TW_image))
        ]);
        $Page->save();

        // Here we are going to check that the SEO renders how we would expect
        $response = $this->call('GET', '/');
        $crawler = new Crawler($response->getContent());

        // Checking OpenGraph
        $this->assertEquals( 'Twitter Title', $crawler->filter('meta[name="twitter:title"]')->eq(0)->attr('content'));
        $this->assertEquals( 'Twitter description', $crawler->filter('meta[name="twitter:description"]')->eq(0)->attr('content'));
        $this->assertEquals( $TW_image, $crawler->filter('meta[name="twitter:image"]')->eq(0)->attr('content'));
    }

    public function test_to_check_we_can_disable_search_engines_from_indexing_and_following_links() {
        putenv('APP_DEBUG=true');
        config()->set('app.debug', true);

        $Page = Page::findOrFail(1);
        $Page->featured_image = url('/app/hummingbird/tests/Resources/images/seo.png');
        $Page->meta()->saveMany([
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_title', 'value' => 'New Title')),
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_description', 'value' => 'Description override')),
            new MetaData(array('item_type' => get_class($Page), 'key' => 'meta_robots', 'value' => 'none'))
        ]);
        $Page->save();

        // Here we are going to check that the SEO renders how we would expect
        $response = $this->call('GET', '/');
        $crawler = new Crawler($response->getContent());

        $this->assertEquals( 'noindex, nofollow', $crawler->filter('meta[name="robots"]')->eq(0)->attr('content'));
    }

    /** @test */
    public function test_to_check_we_can_add_google_site_verification() {
        HB3Meta::addMeta('google-site-verification', "jPTYiQhrO1rRwkXL2zufygYchltpWniJpHS-w8Ny3LX");

        // Here we are going to check that the SEO renders how we would expect
        $response = $this->call('GET', '/');
        $crawler = new Crawler($response->getContent());

        $this->assertEquals( 'jPTYiQhrO1rRwkXL2zufygYchltpWniJpHS-w8Ny3LX', $crawler->filter('meta[name="google-site-verification"]')->eq(0)->attr('content'));
    }
}
