<?php namespace Hummingbird\Tests\Integration\EmailTracking;

use Config;
use DB;
use Mail;
use Hummingbird\Providers\EmailTracker\Tracker;
use Hummingbird\Providers\EmailTracker\Models\EmailActivity;
use Hummingbird\Providers\EmailTracker\Models\EmailTrack;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class EmailTrackingTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        EmailActivity::flushEventListeners();
        EmailActivity::boot();
        EmailTrack::flushEventListeners();
        EmailTrack::boot();

        if( is_null(config()->get('mail.from.address')) ):
            config()->set('mail.from.address', 'noreply@test.co.uk');
        endif;

        if( is_null(config()->get('mail.from.namespace')) ):
            config()->set('mail.from.name', 'PHP Unit Test');
        endif;
    }

    protected function tearDown(): void {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        EmailActivity::truncate();
        EmailTrack::truncate();

        parent::tearDown();
    }

    public function test_to_check_we_can_track_emails() {
        Mail::send([], [], function ($message) {
            $message->to('webmaster@tickboxmarketing.co.uk')
                ->subject('Test email')
                ->html('Hi, welcome user!');
        });

        $this->assertEquals(1, EmailTrack::all()->count());
    }

    public function test_to_check_we_can_track_email_opens() {
        Mail::send([], [], function ($message) {
            $message->to('webmaster@tickboxmarketing.co.uk')
                ->subject('Test email')
                ->html('Hi, welcome user!');
        });

        $EmailTrack = EmailTrack::first();

        $this->call('GET', route('EmailTrack.Open', [$EmailTrack->hash]));
        $this->assertEquals(1, $EmailTrack->opens()->count());
    }

    public function test_to_check_we_can_track_email_clicks() {
        Mail::send([], [], function ($message) {
            $message->to('webmaster@tickboxmarketing.co.uk')
                ->subject('Test email')
                ->html('Hi, welcome user! <a href="https://www.google.co.uk">Click here to go to Google</a>');
        });

        $EmailTrack = EmailTrack::first();
        $response = $this->call('GET', route('EmailLinkTrack.Open', [ 'l' => "https://www.google.co.uk", 'h' => $EmailTrack->hash ]));

        $this->assertEquals(1, $EmailTrack->clicks()->count());
    }

    public function test_to_check_that_email_tracking_can_be_disabled() {
        config()->set('emailTracker.allow_email_tracking', false);

        Mail::send([], [], function ($message) {
            $message->to('webmaster@tickboxmarketing.co.uk')
                ->subject('Test email')
                ->html('Hi, welcome user!');
        });

        $this->assertEquals(0, EmailTrack::all()->count());
    }

    public function test_to_enable_emails_to_administrators() {
        config()->set('emailTracker.allow_admin_email_tracking', false);

        Mail::send([], [], function ($message) {
            $message->to('webmaster@tickboxmarketing.co.uk')
                ->subject('Test email')
                ->html('Hi, welcome user! <a href="https://www.google.co.uk">Click here to go to Google</a>');

            $message->getHeaders()->addTextHeader('EMAIL-TYPE', 'HB3-TESTING');
            $message->getHeaders()->addTextHeader('EMAIL-ADMIN', TRUE);
        });

        $this->assertEquals(0, EmailTrack::all()->count());
    }

    public function test_to_disable_email_tracking_by_type() {
        config()->set('emailTracker.block_email_tracking_by_type', ['HB3-ADMIN']);

        Mail::send([], [], function ($message) {
            $message->to('webmaster@tickboxmarketing.co.uk')
                ->subject('Test email')
                ->html('Hi, welcome user! <a href="https://www.google.co.uk">Click here to go to Google</a>');

            $message->getHeaders()->addTextHeader('EMAIL-TYPE', 'HB3-ADMIN');
        });

        $this->assertEquals(0, EmailTrack::all()->count());
    }
}
