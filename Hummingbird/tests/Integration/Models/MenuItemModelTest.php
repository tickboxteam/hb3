<?php namespace Hummingbird\Tests\Integration\Models;

use Hummingbird\Models\Menuitem;
use \Hummingbird\Tests\TestCase as HummingbirdTestCase;

/**
 * Model tests for Menus and Menu items
 *
 * @since   1.5.0
 * @version 1.0.0
 */
class MenuItemModelTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        Menuitem::boot();
    }

    protected function tearDown(): void {
        parent::tearDown();
    }

    /** @test */
    public function test_to_check_external_links_for_menu_items_remain_external() {
        $menuitem = new Menuitem;
        $menuitem = $menuitem->fill(['menu_item_name' => 'My external link', 'url' => 'https://www.google.com']);
        $menuitem->save();

        $this->assertEquals('https://www.google.com/', $menuitem->url);
    }

    /** @test */
    public function test_to_check_standard_link_is_cleaned() {
        $menuitem = new Menuitem;
        $menuitem = $menuitem->fill(['menu_item_name' => 'My internal link', 'url' => 'hello world']);
        $menuitem->save();

        $this->assertEquals('/hello-world/', $menuitem->url);
    }

    /** @test */
    public function test_to_check_standard_link_is_cleaned_and_sluggified() {
        $menuitem = new Menuitem;
        $menuitem = $menuitem->fill(['menu_item_name' => 'My internal link', 'url' => 'hello world/this-is MY test / link']);
        $menuitem->save();

        $this->assertEquals('/hello-world/this-is-my-test/link/', $menuitem->url);
    }

    /** @test */
    public function test_to_check_standard_link_cleans_beginning_or_trailing_slashes() {
        $menuitem = new Menuitem;
        $menuitem = $menuitem->fill(['menu_item_name' => 'My internal link', 'url' => '/hello world']);
        $menuitem->save();

        $this->assertEquals('/hello-world/', $menuitem->url);

        $menuitem->url = 'hello world/';
        $menuitem->save();

        $this->assertEquals('/hello-world/', $menuitem->url);
    }
}