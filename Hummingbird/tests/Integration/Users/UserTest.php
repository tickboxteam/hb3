<?php namespace Hummingbird\Tests\Integration\Users;

use Hummingbird\Models\MetaData;
use Hummingbird\Models\User;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;
use Hummingbird\Database\DummySeeds\DummyRolesSeeder;
use Hummingbird\Database\DummySeeds\DummyUsersSeeder;
use Illuminate\Support\Facades\Auth;

/**
 * CategoryManagementTest class to help test and check
 * category related specific actions
 *
 * @since   1.1.30
 * @version 1.0.0
 */
class UserTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        $this->seed(DummyRolesSeeder::class);
        $this->seed(DummyUsersSeeder::class);

        User::flushEventListeners();
        User::boot();

        $this->SuperUser = User::where('email', 'super@admin.com')->first();
        $this->SuperUser->force_password_reset = true;
        $this->SuperUser->save();

        $this->AdminUser = User::where('email', 'admin@admin.com')->first();
    }

    protected function tearDown(): void {
        MetaData::where('item_id', $this->SuperUser->id)->where('item_type', get_class($this->SuperUser))->delete();
        // User::truncate(); //FIXME: We need to resolve the issue with truncating user info (foreign key constraints)
        
        parent::tearDown();
    }

    /*
    |--------------------------------------------------------------------------
    | Password checks
    |--------------------------------------------------------------------------
    */
    /** @test */
    public function test_password_reset_means_pages_redirect_to_profile() {
        Auth::loginUsingId($this->SuperUser->id, true);

        $response = $this->call('GET', route('hummingbird.dashboard'));
        $response->assertRedirect( route('hummingbird.profile') );
    }

    /** @test */
    public function test_profile_change_password_allows_access_to_all_pages() {
        Auth::loginUsingId($this->SuperUser->id, true);

        $this->call('POST', route('hummingbird.profile'), [
            '_token' => csrf_token(),
            'name'      => 'Tempor officia',
            'username'  => $this->SuperUser->email,
            'email'     => $this->SuperUser->email,
            'password'  => 'abcDEF123!',
            'password_confirmation' => 'abcDEF123!'
        ]);

        $response = $this->call('GET', route('hummingbird.dashboard'));
        $response->assertStatus(200);
    }

    /** @test */
    public function test_administrator_updates_user_profile_and_sets_password_reset_flag() {
        $this->SuperUser->force_password_reset = NULL;
        $this->SuperUser->save();

        Auth::loginUsingId($this->SuperUser->id, true);

        $this->call('PUT', route('hummingbird.users.update', [$this->AdminUser->id]), [
            '_token' => csrf_token(),
            'name'      => $this->AdminUser->name,
            'username'  => $this->AdminUser->email,
            'email'     => $this->AdminUser->email,
            'password'  => 'abcDEF123!',
            'password_confirmation' => 'abcDEF123!',
            'role_id'   => $this->AdminUser->role_id,
            'status'    => $this->AdminUser->status
        ]);

        $this->AdminUser = User::where('email', 'admin@admin.com')->first();

        $this->assertEquals( 1, $this->AdminUser->force_password_reset );
    }

    /*
    |--------------------------------------------------------------------------
    | User meta data checks
    |--------------------------------------------------------------------------
    */
    /** @test */
    public function test_to_check_user_does_not_have_meta_option_set() {
        $this->assertFalse( $this->SuperUser->hasMetaOption('my-test-meta-option') );
    }

    /** @test */
    public function test_to_check_that_we_cant_add_a_meta_option_without_a_valid_key() {
        $this->expectException( \Exception::class );

        $this->SuperUser->addMetaOption( NULL, NULL);
    }

    /** @test */
    public function test_to_check_that_we_can_add_a_new_meta_option() {
        $this->SuperUser->addMetaOption( 'my-test-meta-option', 'Test value' );
        $this->assertTrue( $this->SuperUser->hasMetaOption('my-test-meta-option') );
    }

    /** @test */
    public function test_to_check_that_we_can_update_meta_option() {
        $this->SuperUser->addMetaOption( 'my-test-meta-option', 'Test value' );
        $this->SuperUser->updateMetaOption( 'my-test-meta-option', 'My new value' );
        $this->assertEquals( 'My new value', $this->SuperUser->getMetaOption('my-test-meta-option') );
    }
}