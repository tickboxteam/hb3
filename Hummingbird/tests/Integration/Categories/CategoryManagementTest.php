<?php namespace Hummingbird\Tests\Integration\Categories;

use Artisan;
use Hummingbird\Models\Categories;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;
use Hummingbird\Database\DummySeeds\DummyCategoriesSeeder;
use Hummingbird\Database\DummySeeds\DummyRolesSeeder;
use Hummingbird\Database\DummySeeds\DummyUsersSeeder;

/**
 * CategoryManagementTest class to help test and check
 * category related specific actions
 *
 * @since   1.1.30
 * @version 1.0.0
 */
class CategoryManagementTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        Categories::boot();

        $this->seed(DummyRolesSeeder::class);
        $this->seed(DummyCategoriesSeeder::class);
    }

    protected function tearDown(): void {
        Categories::truncate();

        parent::tearDown();
    }

    public function test_child_categories_update_parents_when_a_child_is_deleted() {
        for( $i = 1; $i <= 4; $i++) {
            $this->categories['category_' . $i] = Categories::where('name', 'Test category #' . $i)->first();
        }

        $this->assertNull( $this->categories['category_1']->parent );
        $this->assertEquals( 1, $this->categories['category_2']->parent );
        $this->assertEquals( 2, $this->categories['category_3']->parent );
        $this->assertEquals( 3, $this->categories['category_4']->parent );

        $this->categories['category_2']->delete();
        $this->categories['category_3'] = Categories::where('name', 'Test category #3')->first();
        $this->categories['category_4'] = Categories::where('name', 'Test category #4')->first();
        $this->assertEquals( 1, $this->categories['category_3']->parent );
        $this->assertEquals( 3, $this->categories['category_4']->parent );

        $this->categories['category_1']->delete();
        $this->categories['category_3'] = Categories::where('name', 'Test category #3')->first();
        $this->categories['category_4'] = Categories::where('name', 'Test category #4')->first();
        $this->assertNull( $this->categories['category_3']->parent );
        $this->assertEquals( 3, $this->categories['category_4']->parent );
    }

    public function test_category_reinstates_with_parent() {
        $this->categories['category_2'] = Categories::where('name', 'Test category #2')->first();

        $this->categories['category_2']->delete();
        $this->categories['category_2']->restore();
        $this->assertEquals( 1, $this->categories['category_2']->parent );
    }

    public function test_category_reinstates_and_sets_parent_to_null_if_og_parent_is_unavailable() {
        $this->categories['category_1'] = Categories::where('name', 'Test category #1')->first();
        $this->categories['category_2'] = Categories::where('name', 'Test category #2')->first();

        $this->categories['category_1']->delete();
        $this->categories['category_2']->delete();
        $this->categories['category_2']->restore();
        $this->assertNull( $this->categories['category_2']->parent );
    }
}
