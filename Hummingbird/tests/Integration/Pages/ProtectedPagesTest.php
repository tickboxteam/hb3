<?php namespace Hummingbird\Tests\Integration\Pages;

use Hummingbird\Models\Page;
use Hummingbird\Tests\TestCase as HummingbirdTestCase;

class ProtectedPagesTest extends HummingbirdTestCase {
    protected function setUp(): void {
        parent::setUp();

        $this->RootPage = Page::updateOrCreate(['title' => 'RootPage'], [
            'summary' => 'Lorem ipsum dolor exercitation aute enim ea amet velit in mollit est enim in veniam non irure labore.',
            'content' => 'Root: Hiding all content',
            'url' => str_slug( 'RootPage' ),
            'parentpage_id' => 1,
            'status' => 'public',
            'protected' => 0,
            'username' => '', //FIXME: This doesn't feel right because of the issues with default values
            'password' => '', //FIXME: This doesn't feel right because of the issues with default values
            'search_image' => '', //FIXME: This doesn't feel right because of the issues with default values
            'featured_image' => '', //FIXME: This doesn't feel right because of the issues with default values
            'custom_menu_name' => '', //FIXME: This doesn't feel right because of the issues with default values
            'position' => 0 //FIXME: This doesn't feel right because of the issues with default values
        ]);

        $this->ParentPage = Page::updateOrCreate(['title' => 'ParentPage'], [
            'summary' => 'Lorem ipsum dolor exercitation aute enim ea amet velit in mollit est enim in veniam non irure labore.',
            'content' => 'ParentPage: Hidden content',
            'url' => str_slug( 'ParentPage' ),
            'parentpage_id' => $this->RootPage->id,
            'status' => 'public',
            'protected' => 0,
            'username' => '', //FIXME: This doesn't feel right because of the issues with default values
            'password' => '', //FIXME: This doesn't feel right because of the issues with default values
            'search_image' => '', //FIXME: This doesn't feel right because of the issues with default values
            'featured_image' => '', //FIXME: This doesn't feel right because of the issues with default values
            'custom_menu_name' => '', //FIXME: This doesn't feel right because of the issues with default values
            'position' => 0 //FIXME: This doesn't feel right because of the issues with default values
        ]);

        $this->Page = Page::updateOrCreate(['title' => 'Page'], [
            'summary' => 'Lorem ipsum dolor exercitation aute enim ea amet velit in mollit est enim in veniam non irure labore.',
            'content' => 'Page: My hidden content',
            'url' => str_slug( 'Page' ),
            'parentpage_id' => $this->ParentPage->id,
            'status' => 'public',
            'protected' => 0,
            'username' => '', //FIXME: This doesn't feel right because of the issues with default values
            'password' => '', //FIXME: This doesn't feel right because of the issues with default values
            'search_image' => '', //FIXME: This doesn't feel right because of the issues with default values
            'featured_image' => '', //FIXME: This doesn't feel right because of the issues with default values
            'custom_menu_name' => '', //FIXME: This doesn't feel right because of the issues with default values
            'position' => 0 //FIXME: This doesn't feel right because of the issues with default values
        ]);

        $this->flushSession();
    }

    /** @test */
    public function test_to_check_form_is_shown_when_page_is_protected(): void {
        $this->Page->protected = 1;
        $this->Page->username = 'my-username';
        $this->Page->password = 'password';
        $this->Page->save();

        $response = $this->get( $this->Page->permalink );
        $response->assertSee( 'Please enter the login details below.' );
    }

    /** @test */
    public function test_to_check_access_is_given_when_credentials_are_correct(): void {
        $this->Page->protected = 1;
        $this->Page->username = 'my-username';
        $this->Page->password = 'password';
        $this->Page->save();

        $this->post( url( $this->Page->permalink ), [
            'login' => 1,
            'username' => 'my-username',
            'password' => 'password'
        ]);

        $response = $this->get( $this->Page->permalink );
        $response->assertSessionHas('login-' . $this->Page->id);
        $response->assertSee('Page: My hidden content');
    }


    /** @test */
    public function test_to_check_redirect_if_page_is_not_protected_but_parent_is(): void {
        $this->ParentPage->protected = 1;
        $this->ParentPage->username = 'my-username';
        $this->ParentPage->password = 'password';
        $this->ParentPage->save();

        $response = $this->get( $this->Page->permalink );
        $response->assertRedirect( url( $this->ParentPage->permalink ) );
    }


    /** @test */
    public function test_to_check_redirect_triggered_from_protected_parent_page_if_credentials_passed(): void {
        $this->ParentPage->protected = 1;
        $this->ParentPage->username = 'my-username';
        $this->ParentPage->password = 'password';
        $this->ParentPage->save();

        $response = $this->post( url( $this->ParentPage->permalink ), [
            'login' => 1,
            'username' => 'my-username',
            'password' => 'password'
        ]);

        $response = $this->get( url( $this->Page->permalink ) );
        $response->assertSessionHas('login-' . $this->ParentPage->id);

        // dd($response);
        // $response->assertSee('Page: My hidden content');
    }
}