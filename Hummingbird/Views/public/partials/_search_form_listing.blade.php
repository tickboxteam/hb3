<article>
	<div class="article">
		@if($SearchResult->item->hasSearchableImage())
			<div class="col-md-4 news-article-image">
				<a href="{!! $SearchResult->url !!}"><img src="{!! $SearchResult->item->getSearchableImage() !!}" alt="{!! $SearchResult->title !!}" /></a>
			</div>
		@endif

		<div class="article--body">
			<div class="meta">
				<h2 class="articleTitle"><a class="black" href="{!! $SearchResult->url !!}">{!! highlighter(trim(Request::get('s')), $SearchResult->title) !!}</a></h2>
			</div>

			<p>{!! highlighter(trim(Request::get('s')), $SearchResult->content) !!}</p>
		</div>
	</div>
</article>