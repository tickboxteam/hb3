<form class="page-protected" action="{!! Request::url() !!}" method="post">
    {!! Form::hidden('_token', csrf_token()) !!}
    {!! Form::hidden('login', 1) !!}

    <h4>Protected</h4>
    <p>Please enter the login details below.</p>
    
    @if(Session::has('protected-login-error'))
        <p style="color:red;">Login failed, please try again.</p>
    @endif

    @if(!empty($page->username))
        <label for="username">Username:</label>
        <input type="text" id="username" name="username" placeholder="Username" autofocus=""/>
    @endif

    <label for="password">Password</label>
    <input type="password" id="password" name="password">
    <button type="submit">Sign in</button>
</form>