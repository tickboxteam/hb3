<?php
    foreach( $Pages as $Page ):
        $Page_children = ( isset($Page['children']) ) ? $Page['children']:false;
        $Page = ( isset($Page['page']) ) ? $Page['page']: $Page;

        if( is_object($Page) ):
            $IsCurrentPage = ( Request::is( trim($Page->permalink, '/') ) );
            $ListDefaultClasses = config()->get('hb3-templates.side-navigation.list.classes');
            $ListItemDefaultClasses = config()->get('hb3-templates.side-navigation.list-items.classes');

            if( $IsCurrentPage && $Counter > 0 ) {
                $ListDefaultClasses .= " " . config()->get('hb3-templates.side-navigation.list.classes_active');
                $ListItemDefaultClasses .= " " . config()->get('hb3-templates.side-navigation.list-items.classes_active');
            }

            if( $Page->show_in_nav OR (!$Page->show_in_nav AND strpos(Request::url() .'/', $Page->permalink) !== false AND $Page->permalink != '/') ):?>
                <li class="{!! $ListDefaultClasses !!}">
                    <a class="{!! $ListItemDefaultClasses !!}" href="{!! $Page->permalink !!}">{!! $Page->getMenuName() !!}</a>

                    <?php if(is_array($Page_children) AND count($Page_children) > 0): ?>
                        <ul>
                            @include('HummingbirdBase::public.partials.side-navigation.children', ['Pages' => $Page_children, 'CurrentPage' => $CurrentPage, 'PageArguments' => $PageArguments, 'Counter' => $Counter + 1])
                        </ul>
                    <?php endif;?>
                </li>
            <?php endif;
        endif;
    endforeach;
?>