@if(!empty($page))
	<h1>{!! $page->title !!}</h1>
@else
	<h1>Search</h1>
@endif

@if(!empty($SearchTerm))
	<h3>Search results for: &quot;{!! $SearchTerm !!}&quot;</h3>
@endif

@include('hummingbird.public.partials._search_form')

<section>
	<div class="articles">
		@if($SearchResults->count() > 0)
			@foreach($SearchResults as $SearchResult)
				@include('hummingbird.public.partials._search_form_listing')
			@endforeach
		@else
			<p>No search results available.</p>
		@endif
	</div>
</section>