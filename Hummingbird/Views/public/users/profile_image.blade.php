<?php
/**
 * User profile image partial
 *
 * @package     Hummingbird 3
 * @author      Daryl Phillips
 * @copyright   2023 Tickbox Marketing Ltd
 * @since       1.7.3
*/
?>

<div class="profile--meta">
	@if( $User->hasAvatar() )
    	<img class="profile--avatar" src="{!! $User->avatar !!}" alt="{!! $User->name !!}" />
    @endif

    <span class="profile--intials">{!! $User->initials !!}</span>
</div>