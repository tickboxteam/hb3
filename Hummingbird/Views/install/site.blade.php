@extends('HummingbirdBase::install.layout')

@section('content')
<?php
    $site_url = (null !== Request::old('site_name')) ? Request::old('site_url'):$_SERVER['SERVER_NAME'];
    $site_timezone = (null !== Request::old('site_timezone')) ? Request::old('site_timezone'):Config::get('app.timezone');
?>

<div class='step'>
    <h2>Step {!! $step !!}</h2>
    <form method='post' action=''>
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <fieldset>
            <label>Site Name:</label>
            <input type="text" placeholder="Your website name here" name="site_name" value="{!! Request::old('site_name') !!}">
            {!! $errors->first('site_name') !!}
        </fieldset>
        <fieldset>
            <label>Site URL:</label>
            <input type="text" placeholder="www.yourdomain.com" name="site_url" value="{!! $site_url !!}">
            {!! $errors->first('site_url') !!}
        </fieldset>

        <fieldset>
            <label>Encryption Key:</label>
            <input type="text" placeholder="{!! str_limit(Crypt::encrypt(time().time()), 32, '') !!}" name="site_key" value="{!! str_limit(Crypt::encrypt(time()), 32, '') !!}">
            {!! $errors->first('site_key') !!}
        </fieldset>
        <fieldset style="display:none;">
            <label>Password Strength (%):</label>
            <input type="text" placeholder="80" name="site_password_strength" value="0">
            {!! $errors->first('site_password_strength') !!}
        </fieldset>
        <fieldset>
            {!! Form::label('site_installtype', 'Installation Type ') !!}
            {!! Form::select('site_installtype', Hummingbird\Libraries\Installer::get_types(), Request::old('site_installtype')) !!}
            {!! $errors->first('site_installtype') !!}
        </fieldset>
        <fieldset>
            {!! Form::label('site_timezone', 'Timezone ') !!}
            {!! Form::select('site_timezone', Hummingbird\Libraries\DateTimeHelper::timezone_select_list(), $site_timezone) !!}
            {!! $errors->first('site_timezone') !!}
        </fieldset>
        
        <input type='submit' value='Continue' />
    </form>
</div>
@stop