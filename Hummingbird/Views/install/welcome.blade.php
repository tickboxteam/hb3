@extends('HummingbirdBase::install.layout')

@section('content')
	<div class="row">
		<div class="col-md-4">
			<?php $failed = false;?>
			@if (version_compare(PHP_VERSION, '5.6') >= 0)
				<div class="alert alert-box alert-success">
			    	<i class="fa fa-thumbs-up"></i> You are running {!! PHP_VERSION !!}, which is above our system requirements 
			    </div>
			@else
				<?php $failed = true;?>
				<div class="alert alert-box alert-danger">
			    	<i class="fa fa-thumbs-down"></i> You need to install at least PHP version 5.6
			    </div>
			@endif

			@if (function_exists('curl_version'))
				<div class="alert alert-box alert-success">
			    	<i class="fa fa-thumbs-up"></i> CURL is enabled
			    </div>
			@else
				<?php $failed = true;?>
				<div class="alert alert-box alert-danger">
			    	<i class="fa fa-thumbs-down"></i> Make sure that CURL is enabled
			    </div>
			@endif



			@if (function_exists('file_get_contents'))
				<div class="alert alert-box alert-success">
			    	<i class="fa fa-thumbs-up"></i> file_get_contents() is enabled
			    </div>
			@else
				<?php $failed = true;?>
				<div class="alert alert-box alert-danger">
			    	<i class="fa fa-thumbs-down"></i> Please enabled file_get_contents
			    </div>
			@endif

			@if (class_exists('ZipArchive'))
				<div class="alert alert-box alert-success">
			    	<i class="fa fa-thumbs-up"></i> ZIP Archive is available
			    </div>
			@else
				<?php $failed = true;?>
				<div class="alert alert-box alert-danger">
			    	<i class="fa fa-thumbs-down"></i> Please install the ZIP Archive library
			    </div>
			@endif



			@if(extension_loaded('imagick') OR extension_loaded('gd'))
				<div class="alert alert-box alert-success">
				@if(extension_loaded('imagick') AND extension_loaded('gd'))
					<i class="fa fa-thumbs-up"></i> AMAZING - you have both libraries installed
				@elseif(extension_loaded('imagick') AND !extension_loaded('gd'))
					<i class="fa fa-thumbs-up"></i> You currently have Imagick installed
				@else
					<i class="fa fa-thumbs-up"></i> You currently have GD installed
			    @endif
			    </div>
			@else
				<?php $failed = true;?>
				<div class="alert alert-box alert-danger">
			    	<i class="fa fa-thumbs-down"></i> Please make sure either GD or Imagick libraries have been installed
			    </div>
			@endif

			@if( !is_writeable( storage_path() )) )
				<?php $failed = true;?>
				<div class="alert alert-box alert-danger">
			    	<i class="fa fa-thumbs-down"></i> Please make sure the storage folder is writeable
			    </div>
			@else
				@if( !is_writeable(storage_path() . '/framework/cache') )
					<?php $failed = true;?>
					<div class="alert alert-box alert-danger">
				    	<i class="fa fa-thumbs-down"></i> Please make sure the cache folder is writeable in &quot;{!! storage_path() . '/framework/cache' !!}&quot;
				    </div>
				@endif

				@if( !is_writeable(storage_path() . '/logs') )
					<?php $failed = true;?>
					<div class="alert alert-box alert-danger">
				    	<i class="fa fa-thumbs-down"></i> Please make sure the logs folder is writeable in &quot;{!! storage_path() . '/logs' !!}&quot;
				    </div>
				@endif

				@if( !is_writeable(storage_path() . '/framework/sessions') )
					<?php $failed = true;?>
					<div class="alert alert-box alert-danger">
				    	<i class="fa fa-thumbs-down"></i> Please make sure the sessions folder is writeable in &quot;{!! storage_path() . '/framework/sessions' !!}&quot;
				    </div>
				@endif

				@if( !is_writeable(storage_path() . '/framework/views') )
					<?php $failed = true;?>
					<div class="alert alert-box alert-danger">
				    	<i class="fa fa-thumbs-down"></i> Please make sure the views folder is writeable in &quot;{!! storage_path() . '/framework/views' !!}&quot;
				    </div>
				@endif
			@endif
		</div>

		<div class="col-md-8">
		    <h1>We are about to install Hummingbird 3.0</h1>

		    @if(isset($failed) AND !$failed)
		    	<a class="btn btn-default" href="/{!! Hummingbird\Libraries\Installer::uri() !!}/database">Continue</a>
		    @else
		    	<small>Please make sure the above errors have been recitified before you proceed</small>
		    @endif
		</div>
	</div>
@stop
