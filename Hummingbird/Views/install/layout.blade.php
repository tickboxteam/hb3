<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Installing Hummingbird3</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/css/reset.css"> <!-- CSS reset -->
    <link href="/themes/admin/hummingbird/default/css/bootstrap.css" rel="stylesheet" />
    <link href="/themes/admin/hummingbird/default/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="/themes/admin/hummingbird/default/js/html5shiv.min.js"></script>
        <script src="/themes/admin/hummingbird/default/js/respond.min.js"></script>
    <![endif]-->

	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);
		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.step {
			width: 300px;
            margin: 0 auto;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 26px;
			margin: 16px 0;
		}
	</style>
    
    @yield('head')
</head>
<body>
	<div class="container">
    	@yield('content')
    </div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="/themes/admin/hummingbird/default/js/jquery.min.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="/themes/admin/hummingbird/default/js/bootstrap.min.js"></script>
</body>
</html>