<?php
/*
 * Blank partial for rendering HTML for previewing all emails
 *
 * @since   v1.1.28
 * @version v1.0.0 
*/?>

@if( isset($htmlContent) ) 
    {!! $htmlContent !!}
@endif