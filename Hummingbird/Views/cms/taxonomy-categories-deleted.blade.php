@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {{ Session::get('message') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <h1 class="normal pull-left">Deleted categories</h1>

                @if(count($categories) > 0)
                    @if(Auth::user()->userAbility('categories.delete'))
                        {!! Form::open(array('route' => array('hummingbird.categories.purge-all'), 'method' => 'delete')) !!}
                            <button type="submit" class="pull-right btn btn-danger" data-target="#catpurgeall"><i class="fa fa-trash"></i> Purge all</button>
                        {!! Form::close() !!}
                    @endif
        
                    {!! Form::open(array('route' => array('hummingbird.categories.reinstate-all'), 'method' => 'POST')) !!}
                        <button type="submit" class="pull-right btn btn-info" style="margin-right:5px;"><i class="fa fa-arrow-circle-left"></i> Reinstate all</button>
                    {!! Form::close() !!}
                @endif

                <div class="clearfix">&nbsp;</div>

                @if(count($categories) > 0)
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Category name</th>
                                    <th>Description</th>
                                    <th>Deleted</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                            @foreach($categories as $cat)
                                <tr>
                                    <td>{!! $cat->name !!}</td>
                                    <td>{{$cat->description}}</td>
                                    <td>{{ $cat->deleted_at->diffForHumans() }}</td>
                                    <td>

                                        {!! Form::open(array('route' => array('hummingbird.categories.reinstate', $cat->id), 'method' => 'put', 'class' => 'inline-block')) !!}

                                            <button type="submit" class="btn btn-xs btn-info" title="Reinstate {!! $cat->name !!}"><i class="fa fa-arrow-circle-left"></i></button> 
                                        {!! Form::close() !!}

                                        @if(Auth::user()->userAbility('categories.delete'))
                                            {!! Form::open(array('route' => array('hummingbird.categories.purge', $cat->id), 'id' => "catepurge_{$cat->id}", 'method' => 'delete', 'class' => 'inline-block')) !!}
                                                <button type="submit" class="btn btn-xs btn-danger btn-confirm-delete" title="Permanently deleted {{$cat->name}}" data-target="#catepurge_{{ $cat->id }}"><i class="fa fa-trash"></i></button>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>

                    <div class="clearfix"></div>
                @else
                    <div class="clearfix">&nbsp;</div>
                    <div class="alert alert-box alert-warning text-center">No categories have been deleted.</div>
                @endif
            </div>
        </section>
    </div>

@stop
