<?php
    $MediaItem_IMG  = Image::make( storage_path('app/public') . $image->location );
    $width          = $MediaItem_IMG->width();
    $height         = $MediaItem_IMG->height();
    $ratio          = $width / $height;
    $type           = $MediaItem_IMG->mime();
    $base64         = $MediaItem_IMG->encode('data-url');
    $MediaItem_IMG_FS = $MediaItem_IMG->filesize();
?>


<div class="row">
    <div class="col-md-12">
        @if (count($breadcrumbs) > 0)
            <div class="row">
                <div class="col-md-12">
                    <!--breadcrumbs start -->
                    <ul class="breadcrumb">
                        <?php $i = 0;?>
                        @foreach ($breadcrumbs as $breadcrumb)
                            @if($i > 0)
                                <li>
                                    @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                                        <a class="view-collection" @if(isset($breadcrumb['id'])) data-id="{{ $breadcrumb['id'] }}" @endif href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                                    @else
                                        @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                                    @endif
                                </li>
                            @endif
                            <?php $i++?>
                        @endforeach
                    </ul>
                    <!--breadcrumbs end -->
                </div>
            </div>
        @endif
    </div>

    <div class="col-md-6 image-calc" data-col-size="8" data-ratio="{{$ratio}}">
        <div class="image-holder">
            <img id="target" src="{{$image->location}}" class="img-responsive" data-width="{{$width}}" data-height="{{$height}}" />
        </div>
    </div>

    <div class="col-md-6 pull-right" data-id="{{$image->id}}" data-downloads="{{$is_dl}}" data-additional="{{$is_additional}}" >
        <div class="row">
            <div class="col-md-12 hide">
                <section class="panel">
                    <h4>Options</h4>

                    <div class="form-group clearfix">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default constrain" data-toggle="tooltip" data-placement="right" title="Constrain proportions">
                                <input type="checkbox" autocomplete="off" checked> <i class="fa fa-lock"></i>
                            </label>
                            <label class="btn btn-default crop" data-toggle="tooltip" data-placement="right" title="Crop image">
                                <input type="checkbox" autocomplete="off" checked> <i class="fa fa-crop"></i>
                            </label>
                        </div>
                    </div>

                    <h4>Scale</h4>
                    {!! Form::open(array('route' => array('hummingbird.media.postEditImage', $image->id), 'method' => 'post', 'id' => 'scale-form')) !!}
                        <input type="hidden" name="update" value="scale" />

                        <div class="form-group">
                            <label>Width</label>
                            <input name="width" type="text" data-type="width" class="form-control image-scale" placeholder="Scale width" value="{{$width}}">
                        </div>
                        <div class="form-group">
                            <label>Height</label>
                            <input name="height" type="text" data-type="height" class="form-control image-scale" placeholder="Scale height" value="{{$height}}">
                        </div>

                        <button type="submit" class="btn btn-info">Scale</button>
                    {!! Form::close() !!}
                </section>
            </div>
            <div class="col-md-12 hide">
                {!! Form::open(array('route' => array('hummingbird.media.postEditImage', $image->id), 'method' => 'post')) !!}
                    <input type="hidden" id="type" name="type" value="scale">
                    <input type="hidden" name="update" value="save">

                    <input type="hidden" id="w" name="w">
                    <input type="hidden" id="h" name="h">
                    <input type="hidden" id="x" name="x">
                    <input type="hidden" id="y" name="y">

                    <input type="hidden" id="width" name="width" value="{{$width}}" />
                    <input type="hidden" id="height" name="height" value="{{$height}}" />

                    <button type="submit" class="create-image btn btn-primary pull-right"><i class="fa fa-image"></i> Create new image</button>
                {{Form::close()}}
            </div>

            <div class="col-md-12">
                <h4>Details</h4>

                {!! Form::open(array('route' => array('hummingbird.media.editMediaItem', $image->id), 'method' => 'post', 'class' => 'form-horizontal')) !!}
                    <input id="collection" type="hidden" name="collection" value="{{ $image->collection }}" />
                    <input id="action" type="hidden" name="action" value="update" />

                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Version:</label>
                        <div class="col-sm-10">
                            <select name="src" id="src" class="form-control">
                                @if(count($versions) <= 0 || $MediaItem_IMG_FS <= 1000000 )
                                    <option value="{{$image->location}}">Original - {{$MediaItem_IMG->width()}} x {{$MediaItem_IMG->height() }}</option>
                                @endif

                                @if(count($versions) > 0)
                                    @foreach($versions as $version)
                                        <?php $details = Image::make(storage_path('app/public') . $version->location);?>
                                        <option value="{{$version->location}}">{{$details->width()}} x {{$details->height()}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                            <input name="title" id="title" type="text" class="form-control" placeholder="Media Title" value="{{$image->title}}">
                        </div>
                    </div>

                    @if($is_image)
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">Alt Text:</label>
                            <div class="col-sm-10">
                                <input name="alt" type="text" class="form-control" id="alt" placeholder="Caption" value="{{$image->alt}}">
                            </div>
                        </div>
                        <div class="form-group @if($featured) hide @endif">
                            <label for="caption" class="col-sm-2 control-label">Caption</label>
                            <div class="col-sm-10">
                                <input name="caption" type="text" class="form-control" id="caption" placeholder="Caption" value="{{$image->caption}}">
                            </div>
                        </div>
                    @endif

                    <div class="form-group @if($featured) hide @endif">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea style="height:auto;" class="form-control textareas" rows="5" name="description" id="description" placeholder="Enter description...">{{$image->description}}</textarea>
                        </div>
                    </div>

                    @if($is_image)
                        <div class="form-group @if($featured) hide @endif">
                            <label for="url" class="col-sm-2 control-label">Link:</label>
                            <div class="col-sm-10">
                                <select name="url" id="url" class="form-control">
                                    <option value="none">None</option>
                                    <option value="url">URL</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group url-show hide">
                            <label for="target_window" class="col-sm-2 control-label">Open:</label>
                            <div class="col-sm-10">
                                <select name="target_window" id="target_window" class="form-control">
                                    <option value="_self">Same Window</option>
                                    <option value="_blank">New Window</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group url-show hide">
                            <label for="link" class="col-sm-2 control-label">URL:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="link" name="link" />
                            </div>
                        </div>
                    @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<style>
    .image-holder-crop {}
    .image-holder-crop .holding-crop {border:1px dashed #999; padding:20px;margin:auto;text-align: center;height:500px;}
    .btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open > .dropdown-toggle.btn-default {background-color:#00AEEF;color:white;border-color:#00AEEF;}
</style>