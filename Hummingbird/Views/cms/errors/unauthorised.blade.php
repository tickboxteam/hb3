@extends('HummingbirdBase::cms.layout')

@section('content')

<h1>Permission denied</h1>

<p>You do not have permission to open this page. If you are new user or were recently assigned credentials, please try logging in and out.</p>

<p>If the problem persists, please contact your system administrator.</p>

@stop
