<?php 
$messages = array(
            'There are 320 different species of hummingbird found throughout the Americas. The smallest of all is the tiny bee hummingbird, Mellsuga helenae. They are among the smallest of birds.',
            'Their English name derives from the characteristic hum made by their rapid wing beats.',
            'Each species of hummingbird makes a different humming sound, determined by the number of wing beats per second.',
            'They can hover in mid-air by rapidly flapping their wings 12–90 times per second (depending on the species).',
            'During courtship, the wingtips of the ruby-throated hummingbird beats up to 200 times per second, as opposed to the usual wing beat of 90 times per second.  When the female appears, her partner displays by flying to and fro in a perfect arc.  The pair then dives up and down vertically, facing each other.',
            'They can also fly backwards, and are the only group of birds able to do so.',
            'They can fly at speeds exceeding 15 m/s or 54 km/h.',
            'Male hummingbirds are reported to have achieved speeds of almost 400 body lengths per second when swooping in an effort to impress females.',
            'They have feet so tiny that they cannot walk on the ground, and find it awkward to shuffle along a perch.',
            'They have a short, high-pitched squeaky call.',
            'Some are so small, they have been known to be caught by dragonflies and praying mantis, trapped in spider’s webs, snatched by frogs and stuck on thistles.',
            'The hummingbird needs to eat twice its bodyweight in food every day, and to do so they must visit hundreds of flowers daily.',
            'Hummingbirds eat nectar for the most part, although they may catch an insect now and then for a protein boost.',
            'Their super fast wing beats use up a lot of energy, so they spend most of the day sitting around resting. To save energy at night, many species go into torpor (a short-term decrease in body temperature and metabolic rate).',
            'Despite their tiny size, the ruby-throated hummingbird makes a remarkable annual migration, flying over 3000km from the eastern USA, crossing over 1000km of the Gulf of Mexico in a single journey to winter in Central America.',
            'The ruby-throated hummingbird has only approximately 940 feathers on its entire body.',
            'Before migrating, the hummingbird stores a layer of fat equal to half its body weight.',
            'The female hummingbird builds a tiny nest high up in a tree, often up to six metres from the ground. She coats the outside with lichen and small pieces of bark and lines the inside with plant material.',
            'The male takes no responsibility in rearing the young and may find another mate after the first brood hatches.',
            'Some species of hummingbird are so rare that they have been identified only from the skins exported to Europe.'
        );
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hummingbird CMS | Error</title>

        <script src="https://cdn.tailwindcss.com"></script>

        <style>
        	.text-hummingbird-600 {
        		color: #0090d4;
        	}
        	.bg-hummingbird-600 {
        		background-color: #0090d4;
        	}
        	.hover\:bg-hummingbird-500 {
        		background-color: #00aeef;
        	}
        	.focus-visible\:outline-hummingbird-600 {
        		outline-color: #0090d4;
        	}
        </style>
    </head>

    <body id="dashboard" class="cms-body">	
		<main class="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
			<div class="text-center">
				<h1 class="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">Page not found</h1>
				<p class="mt-6 text-base leading-7 text-gray-600">Sorry, we couldn’t find the page you’re looking for.</p>

				<div class="mt-10 flex items-center justify-center gap-x-6">
					<a href="{!! route('hummingbird.dashboard') !!}" class="rounded-md bg-hummingbird-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-hummingbird-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-hummingbird-600">Go back</a>
					<a href="mailto:support@tickboxmarketing.co.uk" class="text-sm font-semibold text-gray-900">Contact support <span aria-hidden="true">&rarr;</span></a>
				</div>
			</div>
		</main>
    </body>
</html>