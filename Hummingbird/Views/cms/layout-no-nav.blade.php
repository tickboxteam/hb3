<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex">

        <title>Hummingbird CMS | </title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="/themes/admin/hummingbird/default/css/reset.css"> <!-- CSS reset -->
        <link href="/themes/admin/hummingbird/default/css/bootstrap.css" rel="stylesheet" />
        <link href="/themes/admin/hummingbird/default/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="/themes/admin/hummingbird/default/css/main.css" rel="stylesheet" />
        <link href="/themes/admin/hummingbird/default/css/responsive.css" rel="stylesheet" />
        <link type="text/css" rel="stylesheet" href="/themes/admin/hummingbird/default/lib/redactor/redactor.css"  />
        <link type="text/css" rel="stylesheet" href="/themes/admin/hummingbird/default/lib/redactor/plugins/clips/clips.css" />
        <link type="text/css" rel="stylesheet" href="/themes/admin/hummingbird/default/lib/offline/offline-theme-dark.css" />
        <link type="text/css" rel="stylesheet" href="/themes/admin/hummingbird/default/lib/offline/offline-language-english.css" />
    </head>

    <body id="dashboard" class="cms-body @if( Session::has('view_user_perms') || Session::has('view_role_perms') ) preview @endif">
        <!-- START WRAPPER -->
        <div class="wrapper relative">
            <div class="main-content relative" style="width:100%;margin-left:0;">
                <div class="content-section clearfix" style="height:100%;background:#DDDCE2 url(http://magnt.com/images/site_v2/content_bg.jpg) repeat ">
                    @yield('breadcrumbs')
                    @yield('content')
                </div>
            </div>
            <!-- END OF CONTENT -->
        </div>
    </body>
</html>