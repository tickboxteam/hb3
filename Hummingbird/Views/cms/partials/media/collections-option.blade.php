<option value="{!! $top_level_collection->id !!}" @if($collection->parent_collection == $top_level_collection->id) selected @endif @if($collection->id == $top_level_collection->id) disabled @endif>{!! str_repeat("&nbsp;", $counter) !!}{!! $top_level_collection->name !!}</option>

@if($top_level_collection->children->count() > 0 && $collection->id != $top_level_collection->id)
    @foreach($top_level_collection->children as $collection_Child)
        @include('HummingbirdBase::cms.partials.media.collections-option', array('counter' => $counter + 1, 'top_level_collection' => $collection_Child))
    @endforeach
@endif
