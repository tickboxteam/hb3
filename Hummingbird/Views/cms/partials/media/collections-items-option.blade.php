<option value="{!! $top_level_collection->id !!}" @if($media->collection == $top_level_collection->id) selected @endif>{!! str_repeat("&nbsp;", $counter) !!}{!! $top_level_collection->name !!}</option>

@if($top_level_collection->children->count() > 0)
    @foreach($top_level_collection->children as $collection_Child)
        @include('HummingbirdBase::cms.partials.media.collections-items-option', array('counter' => $counter + 1, 'top_level_collection' => $collection_Child))
    @endforeach
@endif
