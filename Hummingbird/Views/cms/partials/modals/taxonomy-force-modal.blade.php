<div id="taxonomy-confirm" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                <h3>Are you sure you want to do that?</h3>
            </div>
            <div class="modal-body">
                <p>You are about to update this article without a category being added.</p>
                <p>This means your content may not appear on the website.</p>
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" id="modal-taxonomy-close" data-dismiss="modal">Go back and add a category</button>
                <a href="#" id="modal-taxonomy-confirmed" class="btn btn-danger">Go ahead and update</a>
            </div>
        </div>
    </div>
</div>