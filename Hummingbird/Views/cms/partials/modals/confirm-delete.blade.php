<div id="delete-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                <h3>Are you sure you want to do that?</h3>
            </div>
            <div class="modal-body">
                <p>You are about to delete &quot;<span class="delete-title"></span>&quot;.</p>
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-body modal-body-standard">
                <p>You are about to delete this item.</p>
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-body modal-body-custom">
                <p class="modal-body-custom-data"></p>
                <p>Are you sure you want to do that?</p>
            </div>
            <div class="modal-footer">
                <a href="#" id="btn-delete-confirmed" class="btn btn-danger">Yes, please delete.</a>
            </div>
        </div>
    </div>
</div>