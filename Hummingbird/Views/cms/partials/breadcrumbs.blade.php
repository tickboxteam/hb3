@if ($breadcrumbs)
    <div class="row">
        <div class="col-md-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                @foreach ($breadcrumbs as $breadcrumb)
                    @if ($breadcrumb->url && !$loop->last)
                        <li><a href="{!! $breadcrumb->url !!}"> @if(!empty($breadcrumb->icon)) <i class="fa {!! $breadcrumb->icon !!}"></i> @endif {!! $breadcrumb->title !!}</a></li>
                    @else
                        <li class="active">@if(!empty($breadcrumb->icon)) <i class="fa {!! $breadcrumb->icon !!}"></i> @endif {!! $breadcrumb->title !!}</li>
                    @endif
                @endforeach
            </ul>
            <!--breadcrumbs end -->
        </div>
    </div>
@endif
