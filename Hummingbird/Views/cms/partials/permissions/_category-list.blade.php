<li>
    <div class="perms checkbox">
        <label>{!! Form::checkbox('category_perms[]', $Category->id, (in_array($Category->id, $ObjectPermissions))) !!} {!! $Category->name !!}</label>
    </div>

    @if($Category->children->count() > 0)
        <ul>
            @foreach($Category->children()->orderBy('name')->get() as $ChildCategory)
                @include('HummingbirdBase::cms.partials.permissions._category-list', array('counter' => 0, 'Category' => $ChildCategory))
            @endforeach    
        </ul>
    @endif
</li>
