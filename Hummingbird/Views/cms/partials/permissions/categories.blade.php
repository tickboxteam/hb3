@if($Categories->count() > 0)     
    <h6>{!! Form::checkbox("perms[]", $Permission->id, $Object->hasPermissionAction($Permission), array('class' => 'permission', 'data-perm' => $Permission->name)) !!} Filter Categories?</h6>
    <p><small>Leave empty for to allow access to all categories through content pages.</small></p>
    <hr />

    <div class="perms" data-perm-required="{!! $Permission->name !!}" @if(!$Object->hasPermissionAction($Permission)) style="display:none;" @endif>
        <ul>
            @foreach($Categories as $Category)
                @include('HummingbirdBase::cms.partials.permissions._category-list', array('counter' => 0))
            @endforeach
        </ul>
    </div>
@endif
