<div class="row">
    <div class="col-md-3">
        <!-- Start Permissions Pills -->
        <ul class="nav nav-pills nav-stacked" id="PermissionTabs">
            @foreach($PermissionGroups as $PermissionGroup)
                <li><a href="#perm-{!! str_slug($PermissionGroup->name) !!}" data-toggle="pill">{!! $PermissionGroup->name !!}</a></li>
            @endforeach
        </ul>
        <!-- End Permissions Pills -->
    </div>

    <!-- Start Permissions Tabs -->
    <div class="col-md-9">
        <div class="tab-content">
            <div class="pull-right">
                <a href="#" class="btn btn-xs btn-default checkbox-filter" data-type="all">Check all</a>
                <a href="#" class="btn btn-xs btn-default checkbox-filter" data-type="deselect">Deselect all</a>
                <a href="#" class="btn btn-xs btn-default checkbox-filter" data-type="invert">Invert</a>
            </div>
            @foreach($PermissionGroups as $PermissionGroup)
                <div class="tab-pane no-margin-top" id="perm-{!! str_slug($PermissionGroup->name) !!}">
                    <div class="panel-body" style="padding:0;">
                        <h4 class="normal">{!! $PermissionGroup->name !!}</h4>

                        @if($PermissionGroup->description)
                            <p>{!! $PermissionGroup->description !!}</p>
                        @endif

                        <hr />

                        @foreach($PermissionGroup->permissions as $Permission)
                            @if(in_array($Permission->name, $AllowedPermissions))
                                <div class="perms checkbox">
                                    @if(empty($Permission->action))
                                        <label>
                                            {!! Form::checkbox("selected_perms[]", $Permission->id, ($Object->hasPermissionAction($Permission)), array('class' => 'permission', 'data-perm' => $Permission->name)) !!} &nbsp;{!! $Permission->display_name !!}
                                            @if(!empty($Permission->description))
                                                <span class="help-block">{!! $Permission->description !!}</span>
                                            @endif
                                        </label>
                                    @else
                                        @if($response = Event::dispatch("{$Permission->action}", array('Object' => $Object, 'Permission' => $Permission)))
                                            {!! $response[0] !!}
                                        @endif
                                    @endif
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <!-- End Permissions Tabs -->
</div>