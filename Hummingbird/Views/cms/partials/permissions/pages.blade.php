<input type="hidden" name="perms[]" value="{!! $Permission->id !!}" />

@if($Pages->count() > 0) 
    <div class="perms" data-perm-required="page.read" @if(!$Object->hasPermissionAction('page.read')) style="display:none;" @endif>
        <h6>Filter Pages?</h6>
        <p><small>Leave empty for to allow access to all pages through content pages.</small></p>

        <ul>
            @foreach($Pages as $Page)
                @include('HummingbirdBase::cms.partials.permissions._page-list', array('counter' => 0))
            @endforeach
        </ul>
    </div>
@endif