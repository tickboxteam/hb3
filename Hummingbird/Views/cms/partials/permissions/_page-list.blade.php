<li>
    <div class="perms checkbox">
        <label>{!! Form::checkbox('page_perms[]', $Page->id, (in_array($Page->id, $ObjectPermissions))) !!} {!! str_repeat("&nbsp;", $counter) !!} {!! $Page->title !!}</label>
    </div>
</li>

<?php $counter++;?>

@if($Page->children->count() > 0)
    @foreach($Page->children as $ChildPage)
        @include('HummingbirdBase::cms.partials.permissions._page-list', array('counter' => $counter, 'Page' => $ChildPage))
    @endforeach
@endif