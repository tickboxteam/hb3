@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                {!! Session::get('success') !!}
            </div>
        </div>
    </div>  
@endif

@if(Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-info fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                {!! Session::get('message') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('errors'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                {!! Session::get('errors') !!}
            </div>
        </div>
    </div>
@endif


<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <section class="panel">
                    {!! Form::open(array('route' => array('hummingbird.media.update', $collection->id), 'method' => 'post', 'class' => 'form-horizontal')) !!}
                        <div class="form-group">
                            <label for="filename" class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-10">
                                <input name="name" id="name" type="text" class="form-control" placeholder="Collection Title" value="{!! $collection->name !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_collection" class="col-sm-2 control-label">Parent</label>
                            <div class="col-sm-6"> 
                                <select name="parent_collection" class="form-control" id="parent_collection">
                                    <option value="">-- Root --</option>
                                    <option disabled></option>
                                    @foreach($Collections as $top_level_collection)
                                        @include('HummingbirdBase::cms.partials.media.collections-option', array('counter' => 0))
                                    @endforeach
                                </select>
                            </div>
                            <p class="help-block col-sm-6 col-sm-offset-2">Where in the hierarchy this collection lives.</p>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea style="height:auto;" class="form-control textareas" rows="5" name="description" id="description" placeholder="Collection description...">{!! $collection->description !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default btn-primary">Save</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </section>
            </div>
        </div>
    </div>
</div>


@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }
        });
    </script>   
@stop