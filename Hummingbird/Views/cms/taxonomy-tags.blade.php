@extends('HummingbirdBase::cms.layout')

@section('styles')
    <style>
        .btn-group-wrap {
            text-align: center;
        }

        div.btn-toolbar {
            margin: 0 auto; 
            text-align: center;
            width: inherit;
            display: inline-block;
        }

        .btn-hummingbird:hover, .btn-hummingbird:focus, .btn-hummingbird:active, .btn-hummingbird.active, .open > .dropdown-toggle.btn-hummingbird
        {
            background-color: #00AEEF;
            color: white;
            border-color: #00AEEF;
        }

        #taxonomies .tax 
        {
            margin-bottom:15px;
        }

        #taxonomies .tax h6 
        {
            padding:10px;
            text-align:center;
            background-color: #00AEEF;
            color:white;
            -webkit-box-shadow: 0px 0px 2px -1px #000000;
            -moz-box-shadow: 0px 0px 2px -1px #000000;
            box-shadow: 0px 0px 2px -1px #000000;
        }

        .taxonomy {line-height: 200%;}

        .taxonomy .remove-taxonomy {
            font-size: 1.2rem;
            display: inline-block;
            line-height: 1;
            margin-left: 0;
            color: white;
            width: 24px;
            height: 24px;
            line-height: 20px;
            -webkit-border-radius: 12px;
            -webkit-background-clip: padding-box;
            -moz-border-radius: 12px;
            -moz-background-clip: padding;
            border-radius: 12px;
            background-clip: padding-box;
            text-align: center;
            -moz-box-shadow: 0 2px 5px rgba(0,0,0,.2);
            -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.2);
            box-shadow: 0 2px 5px rgba(0,0,0,.2);
            right:0px;
            top:0px;
        }
    </style>
@stop

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                @if( is_array( Session::get('success') ) )
                    @foreach (Session::get('success') as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                @else
                    {{ Session::get('success') }}
                @endif
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-info alert-danger fade in">
                {{ Session::get('message') }}
            </div>
        </div>
    </div>  
@endif

@if( Session::has('error') && ( !isset($errors) || (isset($errors) && $errors->count() <= 0) ) )
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {{ Session::get('error') }}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel">

            <!-- Button trigger modal -->
            @if(isset($deleted_tags) AND $deleted_tags > 0 && Auth::user()->userAbility('tags.delete'))
                <a href="{{ route('hummingbird.tags.show-deleted') }}" class="pull-right btn btn-danger"><i class="fa fa-archive"></i> Show deleted ({{$deleted_tags}})</a>
            @endif
            
            @if(Auth::user()->userAbility('tags.create'))
                <button id="add-tax-btn" class="pull-right btn btn-default" data-toggle="modal" data-target="#add-tags" style="margin-right: 5px;"><i class="fa fa-plus"></i> Create tags</button>
            @endif

            <div class="clearfix">&nbsp;</div>

            @if(count($alphabetical) > 0)
                <div class="btn-group-wrap">
                    <div class="btn-toolbar">
                        <div class="btn-group btn-group-sm">
                            @foreach($alphabetical as $item_key => $items)
                                <?php $target_key = ($item_key == '0-9') ? 'numeric':$item_key;?>
                                <button class="btn btn-default btn-hummingbird target" data-target="{{$target_key}}">{{$item_key}}</button>
                            @endforeach 
                        </div>
                    </div>
                </div>

                <div id="taxonomies">
                    @foreach($alphabetical as $item_key => $items)
                        <?php $target_key = ($item_key == '0-9') ? 'numeric':$item_key;?>

                        <div class="col-sm-2 tax" id="{{$target_key}}">
                            <h6>{{$item_key}}</h6>

                            @foreach($items as $item)
                                <div class="taxonomy relative">
                                    @if(Auth::user()->userAbility('tags.delete'))
                                        {!! Form::open(array('route' => array('hummingbird.tags.destroy', $item->id), 'id' => 'tagdestroy_' . $item->id, 'method' => 'delete')) !!}
                                            <button type="submit" data-id="{{$item->id}}" class="hide absolute remove-taxonomy btn btn-xs btn-danger btn-confirm-delete" data-target="#tagdestroy_{{ $item->id }}"><i class="fa fa-trash"></i></button>
                                        {!! Form::close() !!}
                                    @endif
                                    
                                    @if(Auth::user()->userAbility('tags.update'))
                                        <a href="/hummingbird/tags/{{$item->id}}">{{$item->name}} ({{$item->used()}})</a>
                                    @else
                                        {{$item->name}} ({{$item->used()}})
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    @endforeach 
                    <div class="clearfix">&nbsp;</div>
                </div>
            @else
                <h3>There are no tags. Please try adding some.</h3>
            @endif
        </div>
    </section>
</div>

@if(Auth::user()->userAbility('tags.create'))
    <div class="modal fade" id="add-tags">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new Tag</h4>
                </div>

                <div class="modal-body">
                    <section class="panel">
                        <header class="panel-heading tab-bg-dark-navy-blue ">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#single">Add new tags</a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="single" class="tab-pane active">
                                    {!! Form::open(array('route' => array('hummingbird.tags.index'), 'method' => 'post', 'class' => 'form-horizontal')) !!}
                                        <fieldset>
                                            <div class="form-group @if( $errors->has('tags') ) has-error @endif">
                                                {!! Form::label('tags', 'Tags:', array('class' => 'col-sm-3 required')) !!}
                                                <div class="col-sm-9">
                                                    {!! Form::textarea('tags', '', array('class' => 'form-control textareas', 'size' => '2x2', 'id' => 'tags')) !!}
                                                    <p class="help-block">To add more than one tag, simply add a comma delimited list.<br />
                                                    <strong>For example: </strong> <i class="italic">Tag 1, Tag 2, Tag 3</i></p>

                                                    @if( $errors->has('tags') )
                                                        <p class="text-danger">{!! $errors->first('tags') !!}</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <div class="form-group pull-right">
                                                <button type="submit" class="btn btn-primary">Insert</button>
                                            </div>
                                        </fieldset>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endif

@stop

@section('scripts')
    <script>
        function tagDisplay() {
            $("#taxonomies .tax").addClass("hide");

            $('.target').each(function(key,val) {
                if($(this).hasClass("active"))  {
                    $("#taxonomies #" + $(this).data('target')).removeClass('hide');
                }
            });
        }

        $(document).ready(function() {
            if($('.target').length > 0) {
                $(".target").click(function(e) {
                    e.preventDefault();

                    $(this).toggleClass('active');
                    $(this).blur();
                    
                    tagDisplay();

                    if(!$('.target').hasClass("active"))  {
                        $("#taxonomies .tax").removeClass("hide");
                    }
                });

                $(".target-new").click(function(e) {
                    e.preventDefault();

                    $('html, body').animate({
                        scrollTop: $("#" + $(this).data('target')).offset().top
                    }, 1000);
                });
            }

            @if( isset($errors) && $errors->count() > 0 )
                $("#add-tax-btn").trigger('click');
            @endif
        });
    </script>
@stop
