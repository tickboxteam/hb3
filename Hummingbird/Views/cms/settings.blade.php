@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('styles')
    <style>
        .no-margin-top {margin-top:0;}
        * .tab-pane.no-margin-top {margin-top:0;}

        #footer {min-height:60px;height:auto;}
        .margin-bottom-20 { margin-bottom:20px; }

        .nav-tabs {border:0; background-color:#333;}
        .nav-tabs > li { margin-bottom:0; }
        .nav-tabs > li > a { color:#AEAEAE;margin-right:0; }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li > a:focus, .nav-tabs > li.active > a:focus, .nav-tabs > li > a:hover {
            background-color:transparent;
            border-color:transparent;
            color:white;
        }
        .nav-tabs > li.active, .nav-tabs > li:not(.active) > a:hover { 
            background-color: #00AEEF;
            border-color:transparent;
            -webkit-border-radius: 0 !important;
                -moz-border-radius: 0 !important;
                    border-radius: 0 !important;
            -webkit-box-shadow: 0 4px 2px -2px rgba(0,0,0,0.25);
                -moz-box-shadow: 0 4px 2px -2px rgba(0,0,0,0.25);
                    box-shadow: 0 4px 2px -2px rgba(0,0,0,0.25);
        }
        .nav-tabs > li.active > a { color:white; }

        .notification { position:absolute; top:-5px; right:10px; }
        .notification.badge { background-color:#06B53C; }

        .tab-pane { margin:2em 0; }
        
        .form-horizontal .radio, .form-horizontal .checkbox { padding-top:0; }
        .radio .help-block, .checkbox .help-block { margin-top:0; }

        .lighter-form-control { background-color:#fAFAFA; border:0; }

        .redactor-box {overflow: hidden;}

        /* toolbar background and other styles*/
        .redactor-toolbar {
            background: #2A313A;
            color:white;
            -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
        }

        .redactor-editor {
            border-top:0;
            border-left:1px solid #E9EDF0;
            border-right:1px solid #E9EDF0;
            border-bottom:1px solid #E9EDF0;
        }
         
        /* toolbar buttons*/
        .redactor-toolbar li a {
            color: white;
        }
         
         /*buttons hover state*/
        .redactor-toolbar li a:hover {
            background: #00AEEF;
            color: white;
        }
         
        /*buttons active state*/
        .redactor-toolbar li a:active,
        .redactor-toolbar li a.redactor-act {
            background: #00AEEF;
            color: white;
        }

        body .redactor-box-fullscreen {
            -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
        }

        body .wrapper.addOpacity {opacity:0.1;}

        .imagemanager-insert.active {
            border:1px solid blue;
        }


.onoffswitch4 {
    position: relative; width: 90px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}

.onoffswitch4-checkbox {
    display: none;
}

.onoffswitch4-label {
    display: block; overflow: hidden; cursor: pointer;
    border: 2px solid #ededed; border-radius: 0px;
}

.onoffswitch4-checkbox:checked ~ .onoffswitch4-label {
    border-color: #00AEEF;
}

.onoffswitch4-inner {
    display: block; width: 200%; margin-left: -100%;
    -moz-transition: margin 0.3s ease-in 0s; -webkit-transition: margin 0.3s ease-in 0s;
    -o-transition: margin 0.3s ease-in 0s; transition: margin 0.3s ease-in 0s;
}

.onoffswitch4-inner:before, .onoffswitch4-inner:after {
    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 26px;
    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
    -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;
    border: 2px solid transparent;
    background-clip: padding-box;
}

.onoffswitch4-inner:before {
    content: "Yes";
    padding-left: 10px;
    background-color: #FFFFFF; color: #2A313A;
}

.onoffswitch4-inner:after {
    content: "No";
    padding-right: 10px;
    background-color: #FFFFFF; color: #2A313A;
    text-align: right;
}

.onoffswitch4-switch {
    display: block; width: 25px; margin: 0px;
    background: #ededed;
    position: absolute; top: 0; bottom: 0; right: 65px;
    -moz-transition: all 0.3s ease-in 0s; -webkit-transition: all 0.3s ease-in 0s;
    -o-transition: all 0.3s ease-in 0s; transition: all 0.3s ease-in 0s; 
}

.onoffswitch4-checkbox:checked + .onoffswitch4-label .onoffswitch4-inner {
    margin-left: 0;
}

.onoffswitch4-checkbox:checked + .onoffswitch4-label .onoffswitch4-switch {
    right: 0px; 
    background-color: #00AEEF;
}
    </style>
@stop

@section('content')
    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-info fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(array('route' => array('hummingbird.settings.update'), 'method' => 'put'), array('class' => 'form-inline')) !!}
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#debug" aria-controls="debug" role="tab" data-toggle="tab">Debugging / Access</a></li>
                                <li role="presentation"><a href="#password_settings" aria-controls="password_settings" role="tab" data-toggle="tab">Password &amp; Security</a></li>
                            </ul>

                            <div class="tab-content">
                                @include('HummingbirdBase::cms.settings._debug')
                                @include('HummingbirdBase::cms.settings._password')
                            </div>

                            <div class="form-group">
                                <div class="col-sm-8">
                                    <button type="submit" class="btn btn-default btn-primary">Update</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </section>
        </div>
    </div>
@stop

@section('scripts')

@stop
