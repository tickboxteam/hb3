@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')
    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {{ Session::get('message') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                {!! Form::open(array('route' => array("hummingbird.redirections.update", $redirect->id), 'method' => 'PUT', 'class' => 'form-horizontal')) !!}
                    <div class="form-group @if( $errors->has('from') || $errors->has('to') ) has-error @endif ">
                        <label for="from" class="col-sm-2 control-label">Redirect: <strong>*</strong></label>   
                        <div class="col-sm-10">
                            <div class="input-group">
                                {!! Form::text('from', $redirect->from, array('class' => 'from form-control', 'placeholder' => 'From URL')) !!}
                                <span class="input-group-addon" style="border-left: 0; border-right: 0;">-</span>
                                {!! Form::text('to', $redirect->to, array('class' => 'to form-control', 'placeholder' => 'Destination URL')) !!}
                            </div>

                            @if( $errors->has('from') )
                                <p class="text-danger">{{ $errors->first('from') }}</p>
                            @endif

                            @if( $errors->has('to') )
                                <p class="text-danger">{{ $errors->first('to') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group @if( $errors->has('type') ) has-error @endif">
                        <label for="type" class="col-sm-2 control-label">Type: <strong>*</strong></label>
                        <div class="col-sm-10">
                            {!! Form::select('type', [NULL => '--- Please select ---'] + Hummingbird\Models\Redirections::get_types_selection(), $redirect->type, array('class' => 'form-control')) !!}

                            @if( $errors->has('type') )
                                <p class="text-danger">{{ $errors->first('type') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group @if( $errors->has('category') ) has-error @endif">
                        <label for="type" class="col-sm-2 control-label">Category: *</label>
                        <div class="col-sm-10">
                            {!! Form::select('category', redirects_get_category_list(), $redirect->category, array('class' => 'form-control', 'id' => 'category')) !!}

                            @if( $errors->has('category') )
                                <p class="text-danger">{{ $errors->first('category') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group @if( $errors->has('custom_category') ) has-error @endif @if( Request::old('category') != 'NEW' ) hide @endif">
                        <div class="col-sm-offset-2 col-sm-6">
                            {!! Form::text('custom_category', NULL, array('class' => 'custom_field form-control', 'id' => 'custom_category')) !!}
                            <span class="help-block">Enter a new category name for this redirect</span>

                            @if( $errors->has('custom_category') )
                                <p class="text-danger">{{ $errors->first('custom_category') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group @if( $errors->has('function') ) has-error @endif">
                        <label for="type" class="col-sm-2 control-label">Function: *</label>
                        <div class="col-sm-10">
                            {!! Form::select('function', [NULL => '--- Please select ---', 'Short URL' => 'Short URL', 'Site structure' => 'Site structure', 'Tracking' => 'Tracking'], $redirect->function, array('class' => 'form-control', 'id' => 'function')) !!}

                            @if( $errors->has('function') )
                                <p class="text-danger">{{ $errors->first('function') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="comments" class="col-sm-2 control-label">Comments: </label>
                        <div class="col-sm-10">
                            {!! Form::textarea('comments', $redirect->comments, array('class' => 'form-control textareas', 'data-summary-limit' => 255)) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="pull-right btn btn-default btn-primary">Update</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </section>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            if($("#category").length > 0)
            {
                $("#category").change(function()
                {
                    if($("#category").val() == 'NEW')
                    {
                        $("#custom_category").closest('.form-group').removeClass("hide");   
                    }
                    else
                    {
                        $("#custom_category").closest('.form-group').addClass("hide");
                    }
                });
            }
        });
    </script>
@stop