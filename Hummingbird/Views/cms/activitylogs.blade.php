@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <div class="row">
                    {!! Form::open(array('route' => array('hummingbird.activity-logs.index'), 'method' => 'get'), array('class' => 'form-inline')) !!}
                        <div class="col-md-3">
                            <h6>By user:</h6>
                            {!! Form::select('user', [NULL => '--- All ---'] + Hummingbird\Models\Activitylog::getDistinctUsers(), Request::get('user'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-3">
                            <h6>By action:</h6>
                            {!! Form::select('action', [NULL => '--- All ---'] + Hummingbird\Models\Activitylog::getDistinctActions(), Request::get('action'), array('class' => 'form-control')) !!}
                        </div>
                        
                        <div class="col-md-3">
                            <h6>By type:</h6>
                            {!! Form::select('type', [NULL => '--- All ---'] + Hummingbird\Models\Activitylog::getDistinctTypes(), Request::get('type'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-3">
                            <h6>&nbsp;</h6>
                            <button type="submit" class="btn btn-default filter-results">Filter results</button>
                            <a href="{!! route('hummingbird.activity-logs.index') !!}" class="btn">Reset search</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                @if( $activitylogs->count() )
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>User</th>
                                    <th>IP address</th>
                                    <th>Action</th>
                                    <th>Type</th>
                                    <th>Comments</th>
                                    <th>When</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($activitylogs as $activitylog)
                                    <tr>
                                        <td>{!! $activitylog->user ? $activitylog->user->name : 'System' !!}</td>
                                        <td>{!! $activitylog->ip_address ?: '-' !!}</td>
                                        <td>{!! $activitylog->action !!}</td>
                                        <td>{!! $activitylog->type ?: '-' !!}</td>
                                        <td>{!! $activitylog->notes ?: '-' !!}</td>
                                        <td data-toggle="tooltip" data-placement="left" title="Date of action: {!! $activitylog->created_at->format('d/m/Y H:i:s') !!}">{!! $activitylog->created_at->diffForHumans() !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="text-center"> 
                        {!! $activitylogs->appends(Request::except('page'))->render() !!}
                    </div>
                @else
                    <div class="alert alert-info">No activity logs registered</div>
                @endif
            </section>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop