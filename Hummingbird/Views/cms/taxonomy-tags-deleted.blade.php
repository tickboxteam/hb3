@extends('HummingbirdBase::cms.layout')

@section('styles')
    <style>
        .btn-group-wrap {
            text-align: center;
        }

        div.btn-toolbar {
            margin: 0 auto; 
            text-align: center;
            width: inherit;
            display: inline-block;
        }

        .btn-hummingbird:hover, .btn-hummingbird:focus, .btn-hummingbird:active, .btn-hummingbird.active, .open > .dropdown-toggle.btn-hummingbird
        {
            background-color: #00AEEF;
            color: white;
            border-color: #00AEEF;
        }

        #taxonomies .tax 
        {
            margin-bottom:15px;
        }

        #taxonomies .tax h6 
        {
            padding:10px;
            text-align:center;
            background-color: #00AEEF;
            color:white;
            -webkit-box-shadow: 0px 0px 2px -1px #000000;
            -moz-box-shadow: 0px 0px 2px -1px #000000;
            box-shadow: 0px 0px 2px -1px #000000;
        }

        .taxonomy {line-height: 200%;}

        .taxonomy .form-taxonomy {
            font-size: 1.2rem;
            display: inline-block;
            line-height: 1;
            margin-left: 0;
            color: white;
            width: 24px;
            height: 24px;
            line-height: 20px;
            -webkit-border-radius: 12px;
            -webkit-background-clip: padding-box;
            -moz-border-radius: 12px;
            -moz-background-clip: padding;
            border-radius: 12px;
            background-clip: padding-box;
            text-align: center;
            -moz-box-shadow: 0 2px 5px rgba(0,0,0,.2);
            -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.2);
            box-shadow: 0 2px 5px rgba(0,0,0,.2);
        }

        .taxonomy .remove-taxonomy {
            right:0px;
            top:0px;
        }


        .taxonomy .restore-taxonomy {
            right:25px;
            top:0px;
        }
    </style>
@stop

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {{ Session::get('success') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-info alert-danger fade in">
                {{ Session::get('message') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {{ Session::get('error') }}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel">

            @if($tags->count() > 0)
                {!! Form::open(array('route' => array('hummingbird.tags.purge-all'), 'id' => 'tagpurgeall', 'method' => 'delete')) !!}
                    <button type="submit" class="pull-right btn btn-danger btn-confirm-delete" data-target="#tagpurgeall"><i class="fa fa-trash"></i> Purge all</button>
                {!! Form::close() !!}

                {!! Form::open(array('route' => array('hummingbird.tags.reinstate-all'), 'method' => 'POST')) !!}
                    <button type="submit" class="pull-right btn btn-info" style="margin-right:5px;"><i class="fa fa-arrow-circle-left"></i> Reinstate all</button>
                {!! Form::close() !!}
            @endif

            <div class="clearfix">&nbsp;</div>

            @if(count($alphabetical) > 0)
                <div class="btn-group-wrap @if(count($alphabetical) < 5) hidden-sm hidden-md hidden-lg @endif">
                    <div class="btn-toolbar">
                        <div class="btn-group btn-group-sm">
                            @foreach($alphabetical as $item_key => $items)
                                <?php $target_key = ($item_key == '0-9') ? 'numeric':$item_key;?>
                                <button class="btn btn-default btn-hummingbird target" data-target="{{$target_key}}">{{$item_key}}</button>
                            @endforeach 
                        </div>
                    </div>
                </div>

                <div id="taxonomies">
                    @foreach($alphabetical as $item_key => $items)
                        <?php $target_key = ($item_key == '0-9') ? 'numeric':$item_key;?>

                        <div class="col-sm-2 tax" id="{{$target_key}}">
                            <h6>{{$item_key}}</h6>

                            @foreach($items as $item)
                                <div class="taxonomy relative">
                                    {!! Form::open(array('route' => array('hummingbird.tags.reinstate', $item->id))) !!}
                                        <button type="submit" data-id="{{ $item->id }}" class="hide absolute form-taxonomy restore-taxonomy btn btn-xs btn-info"><i class="fa fa-arrow-circle-left"></i></button> 
                                    {!! Form::close() !!}

                                    {!! Form::open(array('route' => array('hummingbird.tags.purge', $item->id), 'id' => "tagpurge_{$item->id}", 'method' => 'delete')) !!}
                                        <button type="submit" data-id="{{$item->id}}" class="hide absolute form-taxonomy remove-taxonomy btn btn-xs btn-danger btn-confirm-delete" data-target="#tagpurge_{{ $item->id }}"><i class="fa fa-trash"></i></button>
                                    {!! Form::close() !!}

                                    {{$item->name}}
                                </div>
                            @endforeach
                        </div>
                    @endforeach

                    <div class="clearfix">&nbsp;</div> 
                </div>
            @else
                <div class="clearfix">&nbsp;</div>
                <div class="alert alert-box alert-warning text-center">No tags have been deleted.</div>
            @endif
        </div>
    </section>
</div>

@stop

@section('scripts')
    <script type='text/javascript' src='/themes/admin/hummingbird/default/lib/masonry/masonry.js'></script>

    <script>
        var masonry_cont;

        function initMasonry()
        {
            var $masonry_cont = $('#taxonomies');
            // initialize
            $masonry_cont.masonry(
            {
                columnWidth: '#taxonomies .tax',
                itemSelector: '#taxonomies .tax'
            });
        }

        $(document).ready(function()
        {
            if($(".tax .taxonomy").length > 0)
            {
                $(document).on({
                    mouseenter: function ()
                    {
                        $(this).find('.form-taxonomy').removeClass('hide');
                    },
                    mouseleave: function () 
                    {
                        $(this).find('.form-taxonomy').addClass('hide');
                    }
                }, ".tax .taxonomy");
            }

            if($('.target').length > 0)
            {
                $(".target").click(function(e)
                {
                    e.preventDefault();

                    /* reset to normal */
                    $(".target").removeClass('active');
                    $("#taxonomies .tax.active").removeClass('col-sm-12, active').addClass('col-sm-2');
                    $("#taxonomies .tax").removeClass('hide');

                    /* Now add new styles */
                    $(this).addClass("active");
                    $("#taxonomies .tax:not(#" + $(this).data('target')).addClass("hide");
                    $("#taxonomies #" + $(this).data('target')).removeClass('col-sm-2').addClass("col-sm-12 active").find('.taxonomy').addClass("col-sm-2");

                    /* Re-init masonry */
                    initMasonry();
                });

                $(".target-new").click(function(e)
                {
                    e.preventDefault();

                    $('html, body').animate({
                        scrollTop: $("#" + $(this).data('target')).offset().top
                    }, 1000);
                });
            }
        });
    </script>
@stop
