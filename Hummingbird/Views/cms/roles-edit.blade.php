@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')
    {!! Form::open(array('route' => array('hummingbird.roles.update', $role->id), 'class' => 'form-horizontal', 'method' => 'PUT')) !!}
        {!! Form::setModel($role) !!}
        <div class="row">
            <div class="col-md-12">  
                <div class="row">      
                    <div class="col-md-8">
                        <section class="panel">
                            <h3 class="normal">Role details</h3>

                            <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                <label for="name" class="col-sm-2 control-label">Name: </label>
                                <div class="col-sm-10">
                                    {!! Form::text('name', NULL, array('placeholder' => 'Role name', 'class' => 'form-control')) !!}

                                    @if( $errors->has('name') )
                                        <p class="text-danger">{!! $errors->first('name') !!}</p>
                                    @endif
                                </div>
                            </div>

                            @if(count($system_roles) > 0)
                                <div class="form-group">
                                    <label for="role" class="col-sm-2 control-label">Parent role:</label>
                                    <div class="col-sm-10">
                                        <select name="parentrole_id" id="parentrole_id" class="form-control">
                                            @foreach($system_roles as $system_role)
                                                <option value="{!! $system_role->id !!}" @if($system_role->id == $role->parentrole_id) selected @endif @if( $system_role->id == $role->id ) disabled @endif>{!! $system_role->listing_name ?: $system_role->name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @else
                                <input type="hidden" name="parentrole_id" value="{!! Auth::user()->role->id !!}" />        
                            @endif
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button name="submit" value="update" type="submit" class="pull-right btn btn-default btn-primary">Update</button>
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="col-md-4">
                        <section class="panel">
                            <h3 class="normal">Users</h3>
                            <p class="small">Users associated with {!! $role->name !!}</p>

                            @if($users->count() > 0)
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Last logged in</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{!! $user->name !!}</td>
                                                <td>{!! $user->lastLoginDate() !!}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <p>No users found with this role</p>
                            @endif
                        </section>
                    </div>
                </div>
            </div>
        
            @if(!$role->isSuperUser())
                <div class="col-sm-12">
                    <section class="panel">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="normal">Role permissions</h1>
                                <p class="intro">Here you can define what individual roles can or can not do.</p>
                            </div>
                        </div>

                        @if($PermissionGroups->count() > 0)
                            <hr />

                            <!-- Start Permissions -->
                            @include('HummingbirdBase::cms.partials.permissions._display_permissions', array('Object' => $role))
                            <!-- End Permissions -->

                            <button name="submit" value="update" type="submit" class="pull-right btn btn-default btn-primary">Update</button>
                        @else
                            <div class="alert alert-warning text-centre">There are no permissions available.</div>
                        @endif

                        <div class="row">&nbsp;</div>
                    </section>
                </div>
            @endif
        </div>
    {!! Form::close() !!}
@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/js/hummingbird/permissions.js"></script>
@stop