<div class="row">
    {!! Form::open(array('method' => 'get', 'class' => 'filter-form ')) !!}

        <div class="col-md-6">
            <h6>Search:</h6>
            {!! Form::text('s', Request::get('s'), array('class' => 'form-control')) !!}
        </div>
        
        <div class="col-md-2">
            <h6>Order by fields:</h6>

            <div class="input-group">
                <select class="filter form-control " name="order_by_fields" id="order_by_fields" >
                    <option value="name" @if(Request::get('order_by_fields') == 'name') selected="selected" @endif>Name</option>
                    <option value="updated_at" @if(Request::get('order_by_fields') == 'updated_at') selected="selected" @endif>Last updated</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <h6>Order by direction:</h6>

            <div class="input-group">
                <select class="filter form-control " name="order_by_direction" id="order_by_direction" >
                    <option value="ASC" @if(Request::get('order_by_direction') == 'ASC') selected="selected" @endif>ASC</option>
                    <option value="DESC" @if(Request::get('order_by_direction') == 'DESC') selected="selected" @endif>DESC</option>
                </select>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-6">
            <div class="input-group" style="margin-top: 10px;">
                <button type="submit" class="btn btn-sm btn-default filter-results">Filter results</button>
                <a href="{{ route('hummingbird.module-templates.index') }}" class="btn" style="padding-left: 5px;">Reset search</a>
            </div>
        </div>
    {!! Form::close() !!}           
</div>
