<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{!! config()->get('HummingbirdBase::hummingbird.site_name') !!} - Forgotten Password | Hummingbird CMS</title>
        <meta name="robots" content="noindex">
        <link rel="shortcut icon" href="/themes/admin/hummingbird/default/favicon.ico">

        <script src="https://cdn.tailwindcss.com"></script>
    </head>
    <body>
        <section class="w-screen h-screen bg-gray-100 dark:bg-gray-900 bg-cover m-auto">
            <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto h-full md:h-screen lg:py-0">
                <div class="flex items-center mb-6">
                    <img src="{!! asset( "themes/admin/hummingbird/default/layout/hb-icon.png" ) !!}" class="h-24" />
                </div>

                <div class="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h2>Reset your password</h2>

                        <form class="space-y-4 md:space-y-6" action="{!! route('hummingbird.reset-password.request') !!}" method="POST">
                            @csrf

                            @if( session()->has('error') )
                            <div class="p-4 mb-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400" role="alert">{!! session()->get('error') !!}</div>
                            @endif

                            @if( session()->has('success') )
                                <div class="p-4 mb-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400" role="alert">{!! session()->get('success') !!}</div>
                            @endif

                            <div>
                                <label for="email" class="block mb-2 text-sm font-medium @if(!$errors->has('email')) text-gray-900 @else text-red-700 @endif">Email</label>
                                <input type="text" name="email" id="email" class="bg-gray-50 border @if(!$errors->has('email')) border-gray-300 text-gray-900 @else border-red-500 text-red-900 @endif text-sm rounded-lg focus:ring-sky-600 focus:border-sky-600 block w-full p-2.5" placeholder="Email address" value="{!! request()->old('email') !!}" required>

                                @if($errors->has('email'))
                                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{!! $errors->first('email') !!}</p>
                                @endif
                            </div>

                            <button type="submit" class="w-full text-white bg-sky-600 hover:bg-sky-700 focus:ring-4 focus:outline-none focus:ring-sky-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-sky-600 dark:hover:bg-sky-700 dark:focus:ring-sky-800">Request new password</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>