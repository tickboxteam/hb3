<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{!! config()->get('HummingbirdBase::hummingbird.site_name') !!} - Reset your password | Hummingbird CMS</title>
        <meta name="robots" content="noindex">
        <link rel="shortcut icon" href="/themes/admin/hummingbird/default/favicon.ico">

        <script src="https://cdn.tailwindcss.com"></script>
        <link rel="stylesheet" href="/themes/admin/hummingbird/default/fonts/font-awesome/css/font-awesome.min.css"/>

        <style>
            .password-strength--progress h6 {
                font-size: 0.875rem;
                line-height: 1.25rem;
                margin-bottom: 8px;
                margin-top: 8px;
            }

            .list-item {
                font-size: 0.875rem;
                line-height: 1.25rem;
            }

            .list-item .fa.fa-check:before {
                color: rgb(34 197 94 / var(--tw-text-opacity, 1));
            }

            .fa.fa-eye-slash:before {
                color: rgb(107 114 128 / var(--tw-bg-opacity, 1));
            }

            .fa.fa-eye:before {
                color: rgb(2 132 199 / var(--tw-text-opacity, 1));
            }

            .text-danger {
                margin-top: 8px;
                font-size: 0.875rem;
                line-height: 1.25rem;
                color: rgb(220 38 38 / var(--tw-text-opacity, 1));
            }
        </style>
    </head>
    <body>
        <section class="w-screen h-screen bg-gray-100 dark:bg-gray-900 bg-cover m-auto">
            <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto h-full md:h-screen lg:py-0">
                <div class="flex items-center mb-6">
                    <img src="{!! asset( "themes/admin/hummingbird/default/layout/hb-icon.png" ) !!}" class="h-24" />
                </div>

                <div class="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h2>Set your new password</h2>

                        <form class="space-y-4 md:space-y-6" action="{!! route('hummingbird.reset-password.new-password-store') !!}" method="POST">
                            @csrf

                            {!! Form::hidden('token', $token) !!}

                            <div>
                                <label for="email" class="block mb-2 text-sm font-medium @if(!$errors->has('email')) text-gray-900 @else text-red-700 @endif">Email</label>
                                <input type="email" name="email" id="email" class="bg-gray-50 border @if(!$errors->has('email')) border-gray-300 text-gray-900 @else border-red-500 text-red-900 @endif text-sm rounded-lg focus:ring-sky-600 focus:border-sky-600 block w-full p-2.5" placeholder="Your email address" value="{!! request()->old('email') !!}" required>

                                @if($errors->has('email'))
                                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{!! $errors->first('email') !!}</p>
                                @endif
                            </div>
                            <div>
                                <label for="password" class="block mb-2 text-sm font-medium @if(!$errors->has('password')) text-gray-900 @else text-red-700 @endif">Password</label>
                                <div class="relative">
                                    <input type="password" name="password" id="password" placeholder="••••••••" class="bg-gray-50 border @if(!$errors->has('password')) border-gray-300 text-gray-900 @else border-red-500 text-red-900 @endif text-sm rounded-lg focus:ring-sky-600 focus:border-sky-600 block w-full p-2.5" required>
                                    <span class="absolute top-3.5 right-4 fa fa-eye-slash" onclick="toggle(this)"></span>
                                </div>

                                @if($errors->has('password'))
                                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{!! $errors->first('password') !!}</p>
                                @endif

                                @include('HummingbirdBase::cms.users.auth.password-checker')
                            </div>

                            <div>
                                <label for="password_confirmation" class="block mb-2 text-sm font-medium text-gray-900">Confirm Password</label>
                                <div class="relative">
                                    <input type="password" name="password_confirmation" id="password_confirmation" placeholder="••••••••" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-sky-600 focus:border-sky-600 block w-full p-2.5" required>
                                    <span class="absolute top-3.5 right-4 fa fa-eye-slash" onclick="toggle(this)"></span>
                                </div>
                            </div>

                            <button type="submit" class="w-full text-white bg-sky-600 hover:bg-sky-700 focus:ring-4 focus:outline-none focus:ring-sky-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-sky-600 dark:hover:bg-sky-700 dark:focus:ring-sky-800">Set new password</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <script src="/themes/admin/hummingbird/default/js/hummingbird/password-strength.js"></script>
        <script>
            function toggle(el){
                let input = el.parentNode.querySelector('input');

                if( input ) {
                    input.setAttribute('type', ( input.getAttribute('type') == 'password' ) ? 'text' : 'password' );
                    el.classList.toggle('fa-eye-slash');
                    el.classList.toggle('fa-eye');
                }
            }
        </script>
    </body>
</html>