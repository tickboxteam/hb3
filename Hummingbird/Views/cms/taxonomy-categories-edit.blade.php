@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />

    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>
@stop

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')
    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {{ Session::get('message') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            </div>
        </div>  
    @endif

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="clearfix">
                @if(Auth::user()->userAbility('categories.delete'))
                    {!! Form::open(array('route' => array('hummingbird.categories.destroy', $category->id), 'id' => 'catedestroy', 'method' => 'delete')) !!}
                        <button type="submit" class="pull-right btn btn-xs btn-danger btn-confirm-delete" data-target="#catedestroy"><i class="fa fa-trash"></i> Remove</button>
                    {!! Form::close() !!}
                @endif
            </div>

            <div class="row">
                <div class="col-md-8">
                    {!! Form::open(array('route' => array('hummingbird.categories.update', $category->id), 'method' => 'put', 'class' => 'form-horizontal')) !!}
                        {!! Form::setModel($category) !!}

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name:</label>
                            <div class="col-sm-10">
                                {!! Form::text('name', NULL, array('id' => 'name', 'class' => 'tax-name form-control', 'placeholder' => 'Category name')) !!}

                                @if( $errors->has('name') )
                                    <span class="help-block">{!! $errors->first('name') !!}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="display_name" class="col-sm-2 control-label">Display Name:</label>
                            <div class="col-sm-10">
                                {!! Form::text('display_name', NULL, array('id' => 'display_name', 'class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="slug" class="col-sm-2 control-label">Slug:</label>
                            <div class="col-sm-10">
                                {!! Form::text('slug', NULL, array('id' => 'slug', 'class' => 'form-control', 'placeholder' => 'Category slug')) !!}
                                <div class="regenerate @if(str_slug($category->name) == $category->slug) hide @endif hide">
                                    <a href="#" class="btn btn-xs"><i class="fa fa-cog"></i> Generate new slug</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Description:</label>
                            <div class="col-sm-10">
                                {!! Form::textarea('description', NULL, array('class' => 'form-control textareas', 'size' => '1x1')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="parent" class="col-sm-2 control-label">Parent:</label>
                            <div class="col-sm-4">
                                <select name="parent" id="parent" class="selectpicker form-control"  data-live-search="true">
                                    <option value="">--- Top Level ---</option>
                                    @foreach($list_cats as $cat)
                                        <option value="{!! $cat->id !!}" @if($category->parent == $cat->id) selected="selected" @endif>{!! $cat->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sortOrder" class="col-sm-2 control-label">Order:</label>
                            <div class="col-sm-4">
                                {!! Form::number('SortOrder', $category->SortOrder, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group media-library-holder">
                            {!! Form::label('featured_image', 'Image ', array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-8 image-link">
                                <div class="input-group">
                                    <input type="text" name="featured_image" class="form-control media-image" readonly="" @if($category->image != '') value="{!! $category->image !!}" @endif>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file pull-right">
                                            Browse… <input type="file" multiple="" class="browse-media-library" @if($category->image != '') data-collection-id="" @endif>
                                        </span>
                                    </span>
                                </div>
                                <span class="help-block @if($category->image == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                            </div>

                            <div class="col-sm-2 @if($category->image == '') hide @endif image-holder">
                                <img src="{!! $category->image !!}" class="img-responsive">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default btn-primary">Update</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
    </div>

    @if($category)    
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <h4>Children</h4>

                    @if(count($children) > 0)
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Slug</th>
                                        <th># used</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($children as $cat)
                                        <tr>
                                            <td>{!! $cat->name !!}</td>
                                            <td>{!! $cat->slug !!}</td>
                                            <td>{!! $cat->used() !!}</td>
                                            <td>
                                                <a href="/hummingbird/categories/{!! $cat->id !!}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="clearfix">&nbsp;</div>
                        <div class="alert alert-info text-center">No child categories have been assigned/created.</div>
                    @endif
                </section>
            </div>
        </div>

        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                    </div>
                
                    <div class="modal-body">
                        <p>You are about to delete the category &quot;<strong class="deleting"></strong>&quot;.</p>
                        <p>Do you want to proceed?</p>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger btn-ok">Delete</a>
                    </div>
                </div>
            </div>
        </div>
    @endif

@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/lib/media-library/media-library.js"></script>
    <script src="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>

    <script>
        $(document).ready(function()
        {
            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }

            $('#confirm-delete').on('show.bs.modal', function(e)
            {
                $('.deleting').html($('.delete-category ').data('name'));
            });
        });
    </script> 
@stop
