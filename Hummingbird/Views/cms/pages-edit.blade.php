@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/templates/builder.css" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {!! Session::get('success') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-info fade in">
                {!! Session::get('message') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {!! Session::get('error') !!}
            </div>
        </div>
    </div>  
@endif

@if(isset($versions) AND count($versions) > 0)
    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <h6>Recent versions ({!! count($versions) !!}) <a class="view-versions">View versions</a></h6>
                <ul class="recent-versions">
                    @if(isset($Microsites) AND $Microsites->microsite_active)
                        <?php $Microsites->purgeDBConnection();?>
                    @endif
                    
                    @foreach($versions as $version)
                        <li @if(Request::get('version') == $version->id || (!Request::filled('version') && $version->page_id == $page->id && $version->updated_at == $page->updated_at)) class="bold" @endif>{!! $version->title !!} - created by {!! ($version->user) ? $version->user->name : 'Unknown' !!} on {!! $version->updated_at->format('j F Y H:i:s') !!} [<a href="{!! route('hummingbird.pages.show', [$page_id, 'version' => $version->id]) !!}">view</a>]</li>
                    @endforeach

                    @if(isset($Microsites) AND $Microsites->microsite_active)
                        <?php $Microsites->purgeDBConnection($Microsites->new_prefix);?>
                    @endif
                </ul>
            </section>
        </div>
    </div>
@endif  

@if (!empty($out_of_date))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-warning fade in">
                You are editing draft version of the page. Click <a href="?live=1">here</a> to edit live version.
            </div>
        </div>
    </div>  
@endif

@if(Session::get('preview'))
    <div id="preview">
        <a href="{!! $page->page->permalink !!}?version={!! $page->id !!}" target="_blank">&nbsp;</a>
    </div>

    <?php Session::forget('preview');?>
@endif

{!! Form::open(array('url' => route('hummingbird.pages.update', [$page_id, 'version' => ( isset($page_version) ? $page->id : NULL )]), 'class' => 'form-horizontal', 'autocomplete' => 'off', 'method' => 'PUT')) !!}
    <input id="action" type="hidden" name="action" value="" />

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <div class="form-group hidden-xs">
                    <div class="col-sm-12">
                        @if(Auth::user()->userAbility('page.publish'))
                            @if($page->status == 'public')
                                <a href="#" class="update btn btn-default btn-primary pull-right publish" style="margin-left:5px;" data-action="publish">Publish</a>
                            @else
                                <a href="#" class="update btn btn-default btn-primary pull-right publish" style="margin-left:5px;" data-action="unpublish">Unpublish</a>
                            @endif
                        @endif
                        <a href="#" class="update btn btn-default btn-default pull-right" style="margin-left:5px;" data-action="save"><i class="fa fa-save"></i> Save draft</a>
                        <a href="#" class="update btn btn-default btn-default pull-right" style="margin-left:5px;" data-action="preview"><i class="fa fa-globe"></i> Preview</a>
                    </div>
                </div>
                
                <div class="form-group">
                    {!! Form::label('title', 'Title: ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        {!! Form::text('title', $page->title, array('class' => 'form-control')) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('URL', 'URL: ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        <input type="hidden" name="old_page_url" value="{!! $page->url !!}" />
                        <input class="form-control" name="url" type="text" value="{!! $page->url !!}" @if( $page->id == 1 ) disabled="disabled" @endif>

                        @if( $page->status == 'public' )
                            <span class="help-block"><a href="{!! $page->permalink !!}" title="View: {!! $page->title !!}" target="_blank"><i class="glyphicon glyphicon-new-window btn-xs"></i> {!! $page->permalink !!}</a> - The page will open in a new window</span>
                        @endif

                        @if( $page->status == 'draft' )
                            <span class="help-block"><a href="{{rtrim($page->permalink,'/')}}?version={{$page->id}}" title="View: {{$page->title}}" target="_blank"><i class="glyphicon glyphicon-new-window btn-xs"></i> {{rtrim($page->permalink,'/')}}?version={{$page->id}}</a> - The page will open in a new window</span>
                        @endif

                        @if( get_class($page) == 'PageVersion' && $page->status == 'draft' && $page->page->status == 'public' ) 
                            <span class="help-block"><a href="{!! $page->page->permalink !!}" title="View: {!! $page->page->title !!}" target="_blank"><i class="glyphicon glyphicon-new-window btn-xs"></i> {!! $page->page->permalink !!}</a> - The page will open in a new window</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('custom_menu_name', 'Custom menu name ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        {!! Form::text('custom_menu_name', $page->custom_menu_name, array('class' => 'form-control', 'id' => 'custom_menu_name', 'autocomplete' => 'off')) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('status', 'Status: ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        {!! Form::select('status', array('draft' => 'Draft', 'public' => 'Public'), $page->status, array('class' => 'form-control status-dropdown')) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('parentpage_id', 'Parent page: ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        <select name="parentpage_id" id="parentpage_id" class="form-control">
                            <option value=""> --- Root ---</option>
                            @foreach($page_list as $key => $title)
                                <option value="{!! $key !!}" @if($key == $page->parentpage_id) selected="selected" @endif>{!! $title !!}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('template_name', 'Template:', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        {!! Form::select('template_name', [NULL => '--- Please select ---'] + $themetemplates + ['other' => 'Other'], ( in_array($page->template, $themetemplates) ) ? $page->template : 'other', array('class' => 'form-control')) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('template', 'Layout:', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        {!! Form::select('template', [NULL => '--- Select layout ---'] + $templates, NULL, array('class' => 'form-control')) !!}
                    </div>
                    <div class="col-sm-4">
                        <button id="add-template" class="btn btn-success btn-sm disabled" disabled="disabled"><i class="fa fa-plus"></i> Apply template</button>
                        <a id="save-template" class=" hide btn btn-info btn-sm" href="#"><i class="fa fa-save"></i> Save </a>
                        <a class="clean btn btn-danger btn-sm" href="#"><i class="fa fa-trash"> Reset</i></a>
                    </div>
                </div>

                <div class="form-group template-content">
                    <textarea class="hide" id="generated_html" name="generated_html">{!! $page->content !!}</textarea>

                    <div class="flexi_outer">
                        <div class="relative @if($page->content == '') empty @endif" id="flexicontainer">
                            @if($page->content != '')
                                {!! $page->content !!}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group hide">
                    {!! Form::label('content', 'Content: ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-12">
                        {!! Form::textarea('content', $page->content, array('class' => 'redactor-content')) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('protected', 'Password protected? ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-10">
                        {!! Form::checkbox('protected', 1, $page->protected, array('class' => 'password-protected')) !!}
                    </div>
                </div>

                <div class="form-group password-protection @if(!$page->protected) hide @endif">
                    {!! Form::label('username', 'Username ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        {!! Form::text('username', $page->username, array('class' => 'form-control', 'id' => 'username')) !!}
                    </div>
                </div>

                <div class="form-group password-protection @if(!$page->protected) hide @endif">
                    {!! Form::label('password', 'Password ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        {!! Form::password('password', array('class' => 'form-control', 'id' => 'password', 'autocomplete' => "new-password")) !!}
                        <span class="help-block">Leave empty to keep the same password</span>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-default toggle-password">Show/Hide password</button>
                        <button type="button" class="btn btn-primary generate-password">Create password</button>
                    </div>
                </div>      

                <div class="form-group">
                    <div class="col-sm-10">
                        <h4>Details</h4>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('summary', 'Summary: ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-6">
                        {!! Form::textarea('summary', $page->summary, array('class' => 'form-control textareas summary-truncate')) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('show_in_nav', 'Show in nav? ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-10">
                        {!! Form::hidden('show_in_nav', '0') !!}

                        {!! Form::checkbox('show_in_nav', 1, $page->show_in_nav, array('id' => 'show-in-nav')) !!}
                    </div>
                </div>

                <div class="form-group media-library-holder">
                    {!! Form::label('featured_image', 'Featured image ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-8 image-link">
                        <div class="input-group">
                            <input type="text" name="featured_image" class="form-control media-image" readonly="" @if($page->featured_image != '') value="{!! $page->featured_image !!}" @endif>
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file pull-right">
                                    Browse… <input type="file" multiple="" class="browse-media-library" @if($page->featured_image != '') data-collection-id="" @endif>
                                </span>
                            </span>
                        </div>
                        <span class="help-block @if($page->featured_image == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                    </div>

                    <div class="col-sm-2 @if($page->featured_image == '') hide @endif image-holder">
                        <img src="{!! $page->featured_image !!}" class="img-responsive">
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('searchable', 'Show in search? ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-10">
                        {!! Form::hidden('searchable', '0', array('id' => 'not_searchable')) !!}

                        {!! Form::checkbox('searchable', 1, $page->searchable, array('class' => 'searchable')) !!}
                    </div>
                </div>

                @if(empty($page_version))
                    <div class="form-group show-search-image @if(!$page->searchable) hide @endif">
                        {!! Form::label('weight', 'Search weighting', array('class' => 'col-sm-2')) !!}
                        <div class="col-sm-10">
                            {!! Form::number('weight', $page->weight, array('class' => 'form-control', 'min' => 1)) !!}
                        </div>
                    </div>
                @endif

                <div class="form-group show-search-image @if(!$page->searchable) hide @endif media-library-holder">
                    {!! Form::label('search_image', 'Search image ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-8 image-link">
                        <div class="input-group">
                            <input type="text" name="search_image" class="form-control media-image" readonly="" @if($page->search_image != '') value="{!! $page->search_image !!}" @endif>
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file pull-right">
                                    Browse… <input type="file" multiple="" class="browse-media-library" @if($page->search_image != '') data-collection-id="" @endif>
                                </span>
                            </span>
                        </div>
                        <span class="help-block @if($page->search_image == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                    </div>

                    <div class="col-sm-2 @if($page->search_image == '') hide @endif image-holder">
                        <img src="{!! $page->search_image !!}" class="img-responsive">
                    </div>
                </div>

<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}</style>

                <div class="form-group">
                    {!! Form::label('breadcrumb', 'Show breadcrumb? ', array('class' => 'col-sm-2')) !!}
                    <div class="col-sm-10">
                        {!! Form::hidden('breadcrumb', 0) !!}

                        {!! Form::checkbox('breadcrumb', 1, $page->breadcrumb) !!}
                    </div>
                </div>

                <hr />
                <div class="form-group">
                    <div class="col-sm-10">
                        <h4>SEO</h4>
                    </div>
                </div>

                <?php 
                    $meta_title = (isset($meta['meta_title'])) ? $meta['meta_title']:'';
                    $meta_description = (isset($meta['meta_description'])) ? $meta['meta_description']:'';
                    $meta_robots = (isset($meta['meta_robots'])) ? $meta['meta_robots']:'';
                    $meta_canonical = (isset($meta['meta_canonical'])) ? $meta['meta_canonical']:'';
                    $meta_facebook_title = (isset($meta['meta_facebook_title'])) ? $meta['meta_facebook_title']:'';
                    $meta_facebook_description = (isset($meta['meta_facebook_description'])) ? $meta['meta_facebook_description']:'';
                    $meta_facebook_image = (isset($meta['meta_facebook_image'])) ? $meta['meta_facebook_image']:'';
                    $meta_twitter_title = (isset($meta['meta_twitter_title'])) ? $meta['meta_twitter_title']:'';
                    $meta_twitter_description = (isset($meta['meta_twitter_description'])) ? $meta['meta_twitter_description']:'';
                    $meta_twitter_image = (isset($meta['meta_twitter_image'])) ? $meta['meta_twitter_image']:'';
                    $meta_twitter_card = (isset($meta['meta_twitter_card'])) ? $meta['meta_twitter_card'] : ( env('SEO_TWITTER_CARD') ?: 'summary' );
                ?>
                
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-3">
                            <ul class="nav nav-pills nav-stacked" id="metaTabs">
                                <li class="active"><a href="#meta-standard" data-toggle="pill">Standard</a></li>
                                <li><a href="#meta-facebook" data-toggle="pill">Facebook</a></li>
                                <li><a href="#meta-twitter" data-toggle="pill">X</a></li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div class="tab-pane no-margin-top active" id="meta-standard">
                                    <h5>Standard article details</h5>

                                    <div class="form-group">
                                        <label for="meta_title" class="col-sm-2 control-label">Title:</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('meta_title', $meta_title, array('class' => 'form-control', 'placeholder' => 'Meta title')) !!}
                                            <span class="help-block">By default, the post title will be used if blank</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="meta_description" class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10">
                                            {!! Form::textarea('meta_description', $meta_description, array('class' => 'form-control', 'rows' => '3')) !!}
                                            <span class="help-block">By default, the post summary will be used if blank</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="meta_robots" class="col-sm-2 control-label">Robots:</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('meta_robots', $meta_robots, array('class' => 'form-control')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="meta_canonical" class="col-sm-2 control-label">Canonical URL:</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('meta_canonical', $meta_canonical, array('class' => 'form-control')) !!}
                                            <span class="help-block">A canonical URL is the URL of the page that Google thinks is most representative from a set of duplicate pages on your site. <u>Leave empty if not needed.</u></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane no-margin-top" id="meta-facebook">
                                    <h5>Article SEO for Facebook</h5>

                                    <div class="form-group">
                                        <label for="meta_facebook_title" class="col-sm-2 control-label">Title:</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('meta_facebook_title', $meta_facebook_title, array('class' => 'form-control', 'placeholder' => 'Facebook title')) !!}
                                            <span class="help-block">By default, the standard meta title will be used. Failing that the article title will be used</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="meta_facebook_description" class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10">
                                            {!! Form::textarea('meta_facebook_description', $meta_facebook_description, array('class' => 'form-control', 'rows' => '3')) !!}
                                            <span class="help-block">By default, the standard meta description will be used. Failing that the article summary/content will be used</span>
                                        </div>
                                    </div>

                                    <div class="form-group media-library-holder">
                                        {!! Form::label('meta_facebook_image', 'Image ', array('class' => 'col-sm-2 control-label')) !!}
                                        <div class="col-sm-8 image-link">
                                            <div class="input-group">
                                                <input type="text" name="meta_facebook_image" class="form-control media-image" readonly="" value="{!! $meta_facebook_image !!}">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-default btn-file pull-right">
                                                        Browse… <input type="file" multiple="" class="browse-media-library">
                                                    </span>
                                                </span>
                                            </div>
                                            <span class="help-block">Although 200 x 200 is the absolute minimum, Facebook advises using an image that's at least 600 x 314 pixels. For the best display on high-resolution devices, the company suggests choosing an image that's at least 1200 x 630 pixels.</span>
                                            <span class="help-block @if(empty($meta_facebook_image)) hide @endif"><a href="#" class="remove-media-library">Remove image</a></span>
                                        </div>

                                        <div class="col-sm-2 @if(empty($meta_facebook_image)) hide @endif image-holder">
                                            <img src="{!! $meta_facebook_image !!}" class="img-responsive">
                                        </div>
                                    </div>                                    
                                </div>
                                
                                <div class="tab-pane no-margin-top" id="meta-twitter">
                                    <h5>Article SEO for X</h5>
                                    <div class="form-group">
                                        <label for="meta_twitter_title" class="col-sm-2 control-label">Title:</label>
                                        <div class="col-sm-10">
                                            {!! Form::text('meta_twitter_title', $meta_twitter_title, array('class' => 'form-control', 'placeholder' => 'X title')) !!}
                                            <span class="help-block">By default, the standard meta title will be used. Failing that the article title will be used</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="meta_twitter_description" class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10">
                                            {!! Form::textarea('meta_twitter_description', $meta_twitter_description, array('class' => 'form-control', 'rows' => '3')) !!}
                                            <span class="help-block">By default, the standard meta description will be used. Failing that the article summary/content will be used</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="meta_twitter_card" class="col-sm-2 control-label">Card type:</label>
                                        <div class="col-sm-10">
                                            {!! Form::select('meta_twitter_card', array('summary' => 'Summary', 'summary_large_image' => 'Summary: Large image'), $meta_twitter_card, array('class' => 'form-control status-dropdown')) !!}
                                            <span class="help-block">Default will be the "{!! env('SEO_TWITTER_CARD') ?: 'summary' !!}" card</span>
                                        </div>
                                    </div>

                                    <div class="form-group media-library-holder">
                                        {!! Form::label('meta_twitter_image', 'Image ', array('class' => 'col-sm-2 control-label')) !!}
                                        <div class="col-sm-8 image-link">
                                            <div class="input-group">
                                                <input type="text" name="meta_twitter_image" class="form-control media-image" readonly="" value="{!! $meta_twitter_image !!}">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-default btn-file pull-right">
                                                        Browse… <input type="file" multiple="" class="browse-media-library">
                                                    </span>
                                                </span>
                                            </div>
                                            <span class="help-block">X requires your image to be no smaller than 144 x 144 pixels and less than 1 MB in file size. The social network also says it'll automatically resize images larger than 4096 x 4096 pixels.</span>
                                            <span class="help-block @if(empty($meta_twitter_image)) hide @endif"><a href="#" class="remove-media-library">Remove image</a></span>
                                        </div>

                                        <div class="col-sm-2 @if(empty($meta_twitter_image)) hide @endif image-holder">
                                            <img src="{!! $meta_twitter_image !!}" class="img-responsive">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <hr />
                </div>

                @if(isset($Microsites) AND $Microsites->microsite_active)
                    <?php $Microsites->purgeDBConnection();?>
                @endif

                @include('HummingbirdBase::cms.taxonomy-layout', array('taxonomy' => $taxonomy))

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        @if(Auth::user()->userAbility('page.publish'))
                            @if($page->status == 'public')
                                <a href="#" class="update btn btn-default btn-primary pull-right publish" style="margin-left:5px;" data-action="publish">Publish</a>
                            @else
                                <a href="#" class="update btn btn-default btn-primary pull-right publish" style="margin-left:5px;" data-action="unpublish">Unpublish</a>
                            @endif
                        @endif
                        
                        <a href="#" class="update btn btn-default btn-default pull-right" style="margin-left:5px;" data-action="save"><i class="fa fa-save"></i> Save draft</a>
                        <a href="#" class="update btn btn-default btn-default pull-right" style="margin-left:5px;" data-action="preview"><i class="fa fa-globe"></i> Preview</a>
                    </div>
                </div>    

                @if(isset($Microsites) AND $Microsites->microsite_active)
                    <?php $Microsites->purgeDBConnection($Microsites->new_prefix);?>
                @endif
            </section>
        </div>
    </div>
{!! Form::close() !!}


<div id="media-lib-modal" class="modal fade" style="z-index:100000;">
    <div class="modal-dialog clearfix">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close redactor-modal-btn redactor-modal-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Media library</h4>
            </div>
        
            <div class="modal-body media-images">
                <div class="row"></div>
            </div>
            <div class="modal-footer">
                <footer>
                    <button type="button" class="btn btn-primary redactor-modal-btn redactor-modal-action-btn">Insert</button>
                </footer>
            </div>
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@include('HummingbirdBase::cms.templates-new-modal')


<style type="text/css">
#flexicontainer.empty {
    border:1px dashed #EDEDED;
    min-height:150px;
}

#flexicontainer.empty .empty-text
{
    text-align: center;
    margin:0 auto;
    top:50%;
    left:45%;
    margin-top:50px;   
}

#flexicontainer.empty .empty-text h6 {font-size:30px;font-size:3rem;text-shadow: 2px 2px 3px rgba(0,0,0,0.2);}
#flexicontainer.empty .empty-text p {font-size:15px;font-size:1.5rem;}
.redactor-box {overflow: hidden;}

/* toolbar background and other styles*/
.redactor-toolbar {
    background: #2A313A;
    color:white;
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
}

.redactor-editor {
    border-top:0;
    border-left:1px solid #E9EDF0;
    border-right:1px solid #E9EDF0;
    border-bottom:1px solid #E9EDF0;
}
 
/* toolbar buttons*/
.redactor-toolbar li a {
    color: white;
}
 
 /*buttons hover state*/
.redactor-toolbar li a:hover {
    background: #00AEEF;
    color: white;
}
 
/*buttons active state*/
.redactor-toolbar li a:active,
.redactor-toolbar li a.redactor-act {
    background: #00AEEF;
    color: white;
}

body .redactor-box-fullscreen {
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
}

body .wrapper.addOpacity {opacity:0.1;}

.imagemanager-insert.active {
    border:1px solid blue;
}

.redactor-toolbar, .redactor-dropdown {z-index:1 !important;}

button.disabled {background-color: #C1C6C7 !important;
border-color: #8C8C8C !important;}

</style>

@stop

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="/themes/admin/hummingbird/default/lib/templates/builder.templator.js"></script>
    <script src="/themes/admin/hummingbird/default/lib/templates/builder.js"></script>
    <script src="/themes/admin/hummingbird/default/lib/media-library/media-library.js"></script>
    <script src="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>

    <script>
    $(document).ready(function()
    {
        $('.recent-versions').hide();
        
        $(".view-versions").click(function(e) {
            $('.recent-versions').toggle();
            if($('.recent-versions').is(':visible')) {
                $('.view-versions').text('Hide Versions');
            } else {
                $('.view-versions').text('View Versions');
            }
        });
        
        if($(".empty").length > 0)
        {
            $(".clean").trigger('click');
        }

        if($("#template").length > 0)
        {
            $("#template").change(function()
            {
                if($(this).val() > 0)
                {
                    $("#add-template").prop('disabled', false);
                    $("#add-template").removeClass('disabled');
                }
                else
                {
                    $("#add-template").prop('disabled', true);
                    $("#add-template").addClass('disabled');
                }
            });

            $("#add-template").click(function(e)
            {
                e.preventDefault();

                $('.form-group.error').remove();

                if($("#template").val() > 0)
                {
                    $.ajax({
                        dataType: "json",
                        cache: false,
                        url: '/hummingbird/templates/get-html/'+$("#template").val(),
                        success: $.proxy(function(data)
                        {
                            if(data.state)
                            {
                                $("#flexicontainer").html( clean_template() + data.message );
                                activate_init();
                                
                                $("#template").val(0).trigger('change');
                            }
                            else
                            {
                                $(this).closest('.form-group').after('<div class="form-group error"><div class="col-md-12 alert alert-danger text-center">'+data.message+'</div></div>').delay(5000).queue(function(next)
                                {
                                    $('.form-group.error').remove();
                                });
                            }
                        }, this)
                    });
                }
                else
                {
                    $(this).closest('.form-group').after('<div class="form-group error"><div class="clearfix col-md-12 alert alert-danger text-center">Please select a template</div></div>').delay(5000).queue(function(next)
                    {
                        $('.form-group.error').remove(); 
                    });
                }
            });
        }

        if($("#preview").length > 0)
        {
            $("#preview a")[0].click();
        }

        if($(".select").length > 0)
        {
            $(".select").click(function(e)
            {
                e.preventDefault();

                var type = $(this).data('select');

                switch(type)
                {
                    case 'all':
                        $($(this).data('select-el')).each(function()
                        {
                            $(this).prop('checked', true);
                        });
                        break;
                    case 'none':
                        $($(this).data('select-el')).each(function()
                        {
                            $(this).prop('checked', false);
                        });
                        break;
                    case 'invert':
                        $($(this).data('select-el')).each(function()
                        {
                            var new_val = ($(this).prop('checked')) ? false:true;
                            $(this).prop('checked', new_val);
                        });
                        break;
                }
            });
        }
        
        if($(".password-protected").length > 0)
        {
            $(".password-protected").click(function()
            {
                $(".password-protection").toggleClass("hide");

                return;
            });
        }

        if($(".generate-password").length > 0)
        {
            $(".generate-password").click(function(e)
            {
                e.preventDefault();

                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                for( var i=0; i < 10; i++ )
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                $("#password").val(text);
            });
        }

        if($(".toggle-password").length > 0)
        {
            $(".toggle-password").click(function(e)
            {
                var password_field = $("#password");

                if(password_field.attr('type') == 'password')
                {
                    password_field.attr('type', 'text');
                }
                else
                {
                    password_field.attr('type', 'password');
                }
            });
        }


        if($(".searchable").length > 0)
        {
            $(".searchable").click(function()
            {
                $(".show-search-image").toggleClass('hide');
            });
        }

        $('.status-dropdown').change(function() {
            if ($(this).val() == 'public') {
                $('.publish.btn').html('Publish');
                $('.publish.btn').attr('data-action', 'publish');
            } else {
                $('.publish.btn').html('Unpublish');
                $('.publish.btn').attr('data-action', 'unpublish');
            }
        });
    });
    </script>
@stop