{{-- 
	Template partial for categories to load children categories based on a single parent.

	Recursively calls itself and displays the next list if it has data
--}}

<?php 
	$child_categories = Hummingbird\Models\Categories::objectPermissions()->type()->parent($category->id)->ordered()->get();
?>

@if(count($child_categories) > 0)
	<ol class="dd-list">
	    @foreach($child_categories as $child_category)
	        <li class="dd-item" data-id="{{$child_category->id}}">
	            <div class="dd-handle">{!! $child_category->name !!}</div>

	            @include('HummingbirdBase::cms.taxonomy-categories-reorder-template', array('category' => $child_category))
	        </li>
	    @endforeach
	</ol>
@endif