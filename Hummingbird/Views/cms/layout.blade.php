<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex">
        <meta name="csrf-token" content="{!! csrf_token() !!}" />

        <title>Hummingbird CMS | @if(isset($tag)) {!! $tag !!} @endif</title>
        <link rel="icon" type="image/x-icon" href="/themes/admin/hummingbird/default/favicon.ico">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="/themes/admin/hummingbird/default/css/reset.css"> <!-- CSS reset -->
        <link href="/themes/admin/hummingbird/default/css/bootstrap.css" rel="stylesheet" />
        <link href="/themes/admin/hummingbird/default/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="/themes/admin/hummingbird/default/css/main.css" rel="stylesheet" />
        <link href="/themes/admin/hummingbird/default/css/responsive.css" rel="stylesheet" />
        <link type="text/css" rel="stylesheet" href="/themes/admin/hummingbird/default/lib/redactor/redactor.css"  />
        <link type="text/css" rel="stylesheet" href="/themes/admin/hummingbird/default/lib/redactor/plugins/clips/clips.css" />
        <link type="text/css" rel="stylesheet" href="/themes/admin/hummingbird/default/lib/offline/offline-theme-dark.css" />
        <link type="text/css" rel="stylesheet" href="/themes/admin/hummingbird/default/lib/offline/offline-language-english.css" />
        @yield('styles')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="/themes/admin/hummingbird/default/js/html5shiv.min.js"></script>
            <script src="/themes/admin/hummingbird/default/js/respond.min.js"></script>
        <![endif]-->

        <style>
            .header-flash 
            {
                text-align:center;
                color:white;
                font-size:1em;
                background-color:#2A313A;
                padding-top:20px;
                padding-bottom:20px;
                -webkit-box-shadow: 0px 0px 4px 2px #000000;
                -moz-box-shadow: 0px 0px 4px 2px #000000;
                box-shadow: 0px 0px 4px 2px #000000;
            }
            .header-flash a, .header-flash a:hover
            {
                color:#00AEEF;
            }

            .search-area { background-color:#CDCDCD; color:black; top:0;left:0;z-index:1001;
opacity: 1; background-color: rgb(255, 255, 255); background-image: linear-gradient(to left, rgb(236, 233, 230), rgb(255, 255, 255));
             }
            .maxwidth { width:100%; }
            .maxheight { height:100%; }

            .search-area input, .search-box input {
                display: inline-block;
                font-family: Arial, FontAwesome;
                font-size: inherit;
                text-rendering: auto;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }


            .search-area > form > input { position:absolute;border:0; max-width:500px;border-bottom:1px solid #DEDEDE; padding:0 0 25px 0;color:#DEDEDE;width:50%;left:50%;top:50%;background-color:transparent; font-size:40px;margin-left:-250px;margin-top:-43px; }

            .search-area > form > input:focus { outline-color: transparent; outline-style: none; color:#666;}
            .search-area .close {
height: 30px;
    width: 30px;
    border-radius: 100%;
    background-color: #fff;
    display: block;
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -moz-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    -moz-align-items: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    -moz-justify-content: center;
    justify-content: center;
    -webkit-box-direction: normal;
    -webkit-box-orient: horizontal;
    -webkit-flex-direction: row;
    -moz-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    opacity: 1;
            }

input[name="s"]::-webkit-input-placeholder { /* Chrome and Safari */
   color: #80858e !important;
}
 
input[name="s"]:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
   color: #80858e !important;
   opacity: 1;
}
 
input[name="s"]::-moz-placeholder { /* Mozilla Firefox 19+ */
   color: #80858e !important;
   opacity: 1;
}
 
input[name="s"]:-ms-input-placeholder { /* Internet Explorer 10-11 */
   color: #80858e !important;
}
 
input[name="s"]::-ms-input-placeholder { /* Microsoft Edge */
   color: #80858e !important;
}
 
input[name="s"]::placeholder {
   color: #80858e !important;
}
        </style>
    </head>

    <body id="dashboard" class="cms-body @if( Session::has('view_user_perms') || Session::has('view_role_perms') ) preview @endif">
        @if( Session::has('view_user_perms') )
            <div class="header-flash">
            You are currently in preview mode, viewing &quot;{!! Auth::user()->name !!}&quot;. To reset <a href="{!! route('hummingbird.users.preview.destroy', [ 'referrer' => Request::url() ]) !!}">click here</a>.
            </div>
        @endif

        @if( Session::has('view_role_perms') )
            <div class="header-flash">
                You are currently viewing how the role &quot;{!! App::make('RoleController')->preview_role !!}&quot; will look. To cancel <a href="{!! route('hummingbird.roles.preview', [NULL, 'action' => 'remove']) !!}" style="color:red;">click here</a>.
            </div>
        @endif

        @if(Session::has('microsite-builder'))
            <div class="header-flash">
                You are currently in microsite mode. To return to the main site <a href="{!! route('hummingbird.dashboard') !!}">click here</a>.
            </div>
        @endif

        <!-- START WRAPPER -->
        <div class="wrapper relative">
            <!-- START NAV -->
            @include('HummingbirdBase::cms.include.nav')
            <!-- END NAV -->

            <div class="main-content relative">
                <div class="header-section">
                    <div class="pull-left" style="width:50%;">
                        <a href="#" id="nav-toggle"><i class="fa fa-bars"></i></a>

                        @if(isset($has_search) AND $has_search)
                            <div class="search-box">
                                <form action="{!! Request::url() !!}" method="get">
                                    <input name="s" type="text" placeholder="&#xF002; Search..." />
                                </form>
                            </div>

                            <div class="search-area fixed maxwidth maxheight" style="display:none;">
                                <form action="{!! Request::url() !!}" method="get">
                                    <input name="s" type="text" placeholder="&#xF002; Search..." />

                                    <a href="#" class="close absolute" style="top: 25%; right: 25%;">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"><g id="svgCross" fill="none" fill-rule="evenodd" transform="translate(-480 -392)"><path fill="currentColor" fill-rule="nonzero" d="M486.893 398l4.922 4.922a.632.632 0 01-.893.893L486 398.893l-4.922 4.922a.632.632 0 11-.893-.893l4.922-4.922-4.922-4.922a.632.632 0 11.893-.893l4.922 4.922 4.922-4.922a.632.632 0 01.893.893L486.893 398z"></path></g>
                                        </svg>
                                    </a>
                                </form>
                            </div>
                        @endif
                    </div>

                    <a style="float: right; font-weight:bold; padding-right: 20px; padding-top: 23px;" class="hide logout" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                    <div class="pull-right">
                        <div class="profile">
                            <div class="dropdown navbar-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    {!! Auth::user()->get_profile_image() !!}
                                    {{-- 
                                    <img src="{{ Auth::user()->profile_image() }}" class="img-circle">
                                    <a class="hide" href="#">{{Auth::user()->name}} <i class="fa fa-chevron-right"></i></a> --}}
                                </a>
                                <ul class="dropdown-menu" style="left:auto;right:0;">
                                    <li class="arrow top"></li>
                                    <li><a href="{!! route('hummingbird.profile') !!}"><i class="fa fa-user"></i> Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <!-- </div> -->

                    <div class="pull-right profile hide">
                        <div class="menu-right">
                            <ul class="notification-menu relative">
                                <li>
                                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                        {!! Auth::user()->get_profile_image() !!}
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                        <li><a href="#"><i class="fa fa-user"></i>  Profile</a></li>
                                        <li><a href="#"><i class="fa fa-cog"></i>  Settings</a></li>
                                        <li><a href="#"><i class="fa fa-sign-out"></i> Log Out</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>


                <style>
                    .profile .notification-menu {padding-left:0;padding-right:0;}
                </style>

                <div class="content-section clearfix">
                    <?php /*
                    <div class="row ">
                        <div class="col-md-12">
                            <!--breadcrumbs start -->
                            <ul class="breadcrumb panel">
                                <li><a href="/hummingbird/"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li class="active">{PAGE ACCESSED}</li>
                            </ul>
                            <!--breadcrumbs end -->
                        </div>
                    </div>
                    */?>

<!--                     <?php if(!Session::has('show-welcome'))
                    {
                        Session::put('show-welcome', true);?>
                        <h1 class="normal pull-left">Welcome back, {{ Auth::user()->name }}!</h1>
                    <?php }?>

                    <h6 class="normal pull-right" id="clock-date"><?php echo date("l, j F Y"); ?> <span id="clock"></span></h1>
                        
                    <div class="clearfix">&nbsp;</div> -->

                    @yield('breadcrumbs')
                    @yield('content')
                    
                    @include('HummingbirdBase::cms.partials.modals.confirm-delete')

                    <div class="clearfix">&nbsp;</div>
                </div>
                
                <!-- TEMP: REMOVE -->
                <section id="footer">
                    <a href="http://www.tickboxmarketing.co.uk/" target="_blank">&copy; Tickbox Marketing {!! date("Y") !!}</a> | <small>Hummingbird v{!! system_version() !!}</small>
                </section>
            </div>
            <!-- END OF CONTENT -->
        </div>
        <a id="return-to-top" href="#" class="opacity"><i class="fa fa-angle-up"></i></a>


        <form id="logout-form" action="{{ route('AuthLogout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        <!-- END OF WRAPPER -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/themes/admin/hummingbird/default/js/jquery.min.js"></script>
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/themes/admin/hummingbird/default/js/bootstrap.min.js"></script>

        <!-- Include Hummingbird - Individual JS Files -->        
        <script src="/themes/admin/hummingbird/default/lib/redactor/redactor.js?x={{ time() }}"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/hummingbird.default.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/clips/clips.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/counter.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/definedlinks.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/hummingbird.filemanager.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/fontcolor.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/fontfamily.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/fontsize.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/hummingbird.formatter.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/fullscreen.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/hummingbird.imagemanager.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/limiter.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/table.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/textdirection.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/textexpander.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/video.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/redactor/plugins/hummingbird.shortcodes.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/offline/offline.min.js"></script>
        <script src="/themes/admin/hummingbird/default/lib/autogrow/autogrow.js"></script>

        <!-- Include Hummingbird Core JS -->
        <script src="/themes/admin/hummingbird/default/js/hummingbird.js?x={{ time() }}"></script>

        <script>
            if( !RedactorShortcodes ) var RedactorShortcodes = {};

            @if( class_exists(SystemShortcodes::class) )
                @foreach( SystemShortcodes::get_active_shortcodes() as $Shortcode )
                    RedactorShortcodes["<?php echo $Shortcode;?>"] = "<?php echo $Shortcode;?>";
                @endforeach
            @endif
        </script>

        <script>
            // var idleSeconds = 1000 * (60 * 15); // 1/4 hour no activity
            // var idleTimer;

            // function resetIdleTimer()
            // {
            //     clearTimeout(idleTimer);
            //     idleTimer = setTimeout(userIdle, idleSeconds);
            // }

            // function userIdle()
            // {
            //     window.location = '/hummingbird/locked/';
            // }
            // http://stackoverflow.com/questions/7276677/jquery-redirect-to-url-after-specified-time

            $(document).ready(function()
            {
                if($('.permission-holder').length > 0)
                {
                    $('.permission-holder h3 input:checkbox').click(function()
                    {
                        var el = $(this).closest('.permission-holder');

                        el.find(".perms").slideToggle();
                    });
                }

                // if($('.cms-body').length > 0)
                // {
                //     $(document.body).bind('mousemove keydown DOMMouseScroll mousewheel mousedown touchstart touchmove', resetIdleTimer);
                //     resetIdleTimer(); // Start the timer when the page loads
                // }

                $(document).on('mouseenter', ".tooltip-table", function () {
                    var $this = $(this);
                    if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
                        $this.tooltip({
                            container : 'body',
                            title: $this.text(),
                            placement: "bottom"
                        });
                        $this.tooltip('show');
                    }
                });

                if($(".tooltip_show").length > 0)
                {
                    $(".tooltip_show").tooltip();
                }

                if($(".media-upload").length > 0)
                {
                    $(".media-upload").click(function(e)
                    {
                        e.preventDefault();

                        if($("#dropzone").length > 0)
                        {
                            $("#dropzone").slideToggle();
                        }
                    });
                }

                if($(".thumbnail img").length > 0)
                {
                    $(".thumbnail img").click(function()
                    {
                        // $("#editImage").modal();
                    });
                }

                if($(".Activity").length > 0)
                {
                    $(".Activity").append('<div class="updates updates-alert">10</div>');
                }

                if($("#sidebar .menu").length > 0)
                {
                    $(".menu").each(function()
                    {
                        if($(this).find('ul').children().length <= 0)
                        {
                            // $(this).remove();
                        }
                        else
                        {
                            if($(this).find('ul li a.active').length > 0)
                            {
                                var el = $(this).find('ul li a.active');

                                $(this).find('ul:not(.submenu)').css('display', 'block');
                                $(this).find('h3 a i.fa').toggleClass("fa-plus fa-minus");

                                if(el.closest('ul').hasClass('submenu'))
                                {
                                    el.closest('li.option').find('a.hasSubMenu').trigger('click');
                                }
                            }
                        }
                    });

                    $(".menu h3").click(function(e)
                    {
                        $(this).find('a.menu-collapse').trigger('click');
                        return false;
                    });

                    $(".menu h3 > a.menu-collapse").click(function(e)
                    {
                        e.preventDefault();

                        $(this).toggleClass('active');
                        $(this).find('.fa').toggleClass("fa-plus fa-minus");
                        $(this).closest('.menu').find('ul:first').slideToggle();

                        return false;
                    });

                    if($(".search-box").length > 0)
                    {
                        $(".search-box input").focus(function()
                        {
                            $(".search-area").fadeIn().find('input').focus();
                            $(this).blur();
                        });
                    }

                    $(".search-area .close").click(function() {
                        $(this).closest('.search-area').fadeOut();
                        $(this).closest('form').find('input').blur();
                    });
                }
            });
        </script>

        @yield('scripts')
    </body>
</html>