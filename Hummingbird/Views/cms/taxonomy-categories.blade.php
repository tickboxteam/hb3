@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {!! Session::get('success') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-info alert-danger fade in">
                {!! Session::get('message') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {!! Session::get('error') !!}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <h1 class="pull-left normal">Categories</h1>

            <!-- Button trigger modal -->
            @if(isset($deleted_cats) AND $deleted_cats > 0 && Auth::user()->userAbility('categories.delete'))
                <a href="{!! route('hummingbird.categories.deleted') !!}" class="pull-right btn btn-danger"><i class="fa fa-archive"></i> Show deleted ({!! $deleted_cats !!})</a>
            @endif
    
            @if(Auth::user()->userAbility('categories.create'))
                <button id="add-tax-cat-btn" type="button" class="pull-right btn btn-default" data-toggle="modal" data-target="#add-cats" style="margin-right: 5px; margin-left: 5px;"><i class="fa fa-plus"></i> Create category</button>
            @endif

            <a href="{!! route('hummingbird.categories.save-reorder') !!}" class="pull-right btn btn-primary" ><i class="fa fa-list"></i> Re-order categories</a>   

            <div class="clearfix">&nbsp;</div>

            @if(count($cats) > 0)
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Category name</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        @foreach($list_cats as $cat)
                            <?php $catParents = [];
                            $CatParent = $cat->parentCategory;

                            while( null !== $CatParent ) {
                                $catParents[] = $CatParent->id;
                                $CatParent = $CatParent->parentCategory;
                            }?>

                            <tr @if(null !== $cat->parent) class="child hide" data-primary-parent="{!! $cat->parent !!}" data-parent="{!! implode(",", $catParents) !!}" @endif>
                                <td class="table__item">{!! $cat->name !!} ({!! $cat->used() !!}) @if($cat->hasChildren()) <a href="#" class="expand" data-id="{!! $cat->id !!}"><i class="fa fa-plus font-size-10"></i></a> @endif</td>
                                <td>{!! $cat->description !!}</td>
                                <td>
                                    @if(Auth::user()->userAbility('categories.update'))<a href="{!! route('hummingbird.categories.show', $cat->id) !!}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>@endif

                                    @if(Auth::user()->userAbility('categories.delete'))
                                        {!! Form::open(array('route' => array('hummingbird.categories.destroy', $cat->id), 'id' => 'catdestroy_'.$cat->id, 'method' => 'delete', 'class' => 'inline-block')) !!}
                                            <button type="submit" data-id="{!! $cat->id !!}" value="{!! $cat->id !!}" class="btn btn-xs btn-danger btn-confirm-delete" data-target="#catdestroy_{!! $cat->id !!}"><i class="fa fa-trash"></i></button>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>

                <div class="clearfix"></div>
            @else
                <h3>There are no categories. Please try adding some.</h3>
            @endif
        </div>
    </section>
</div>

@if(Auth::user()->userAbility('categories.create'))
    <div class="modal fade" id="add-cats">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new category</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12"> 
                            {!! Form::open(array('route' => array('hummingbird.categories.index'), 'method' => 'post', 'class' => 'form-horizontal')) !!}
                                
                                <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                    {!! Form::label('name', 'Name:', array('class' => 'col-sm-3 required')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('name', Request::old('name'), array('class' => 'tax-name form-control', 'placeholder' => 'Category name')) !!}

                                        @if( $errors->has('name') )
                                            <p class="text-danger">{!! $errors->first('name') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                @if(count($list_cats) > 0)
                                    <div class="form-group @if( $errors->has('parent') ) has-error @endif">
                                        <label for="tags" class="col-sm-3">Parent category:</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="parent" name="parent">
                                                <option value=""> -- </option>
                                                @foreach($list_cats as $cat)
                                                    <option value="{!! $cat->id !!}" @if(Request::old('parent') == $cat->id) selected="selected" @endif>{!! $cat->name !!}</option>
                                                @endforeach
                                            </select>

                                            @if( $errors->has('parent') )
                                                <p class="text-danger">{!! $errors->first('parent') !!}</p>
                                            @endif
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" name="parent" />
                                @endif

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" class="btn btn-primary">Create</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endif

@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            @if(count($errors) > 0)
                $("#add-tax-cat-btn").trigger('click');
            @endif
        });

        var catTree = document.querySelectorAll('table .table__item a.expand');
        for(var i = 0; i < catTree.length; i++){
            catTree[i].addEventListener('click', function(e) {
                var classList = e.target.classList;
                var parent = e.target.parentElement;
                var targetID = parent.dataset.id;

                if(!parent.classList.contains("active")) {
                    parent.classList.add('active');
                    classList.remove('fa-plus');
                    classList.add('fa-minus');

                    var childCategories = document.querySelectorAll('table tr[data-primary-parent="' + targetID + '"]');

                    [].forEach.call(childCategories, function(childCategory) {
                        childCategory.classList.remove('hide');
                    });
                } else {
                    parent.classList.remove('active');
                    classList.remove('fa-minus');
                    classList.add('fa-plus');
                    
                    var childCategories = document.querySelectorAll('table tr[data-parent*="' + targetID + '"]');

                    [].forEach.call(childCategories, function(childCategory) {
                        childCategory.classList.add('hide');

                        [].forEach.call(childCategory.querySelectorAll('.table__item a.expand.active'), function(catShowing) {
                            catShowing.classList.remove('active');
                            catShowing.querySelector('i').classList.remove('fa-minus');
                            catShowing.querySelector('i').classList.add('fa-plus');
                        });
                    });
                }
                e.preventDefault();
            });
        }

    </script>
@stop
