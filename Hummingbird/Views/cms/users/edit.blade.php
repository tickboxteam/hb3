@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('styles')
    <style type="text/css">
        .user-profile {}
        .user-profile .content-header 
        {
            height:100px;
            /*background:url('https://farm9.staticflickr.com/8653/16645863609_b38343d46b_b.jpg') no-repeat 50% 50%;*/
            background-size:cover;
            position:relative;
            margin:-20px -20px 20px;
            overflow: hidden;
        }

        .user-profile .content-header .header-section {height:auto;color:white;position:absolute;top:0;left:0;right:0;padding:20px;border:0;background:rgb(0,0,0);background:rgba(0,0,0,0.4);}
        .user-profile .content-header .header-section h1 {font-size:2.8rem;font-size:28px;font-weight:300;}
        .user-profile .content-header .header-section h1 > small {color:#EDEDED;}


        .hide {transition: opacity 500 ease-in-out;}

        .permission-holder .row 
        {
            margin-bottom:10px;
            padding-bottom:10px;
            border-bottom:1px solid #CCC;
            margin-left:0;
            margin-right:0;
        }

        .profile img
        {

            border-radius: 50%;
            -webkit-border-radius: 50%;
            border: 10px solid #f1f2f7;
            margin-top: 20px;
        }

        .profile img:hover {opacity:0.5}


    </style>
@stop


@section('content')

    <div class="user-profile">
        <div class="content-header content-header-media" data-background="{!! $user->getPreference('cover-image') !!}">
            <div class="header-section">
                <h1>{!! $user->name !!} <br /><small>{!! $user->username !!}</small></h1>
            </div>
        </div>    

        @if (Session::has('success'))
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        {!! Session::get('success') !!}
                    </div>
                </div>
            </div>  
        @endif

        @if(Session::has('message'))
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-info fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        {!! Session::get('message') !!}
                    </div>
                </div>
            </div>  
        @endif

        @if(Session::has('error'))
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        {!! Session::get('error') !!}
                    </div>
                </div>
            </div>  
        @endif

        {!! Form::open(array('route' => array('hummingbird.users.update', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal')) !!}
            {{ Form::setModel( $user ) }}
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <div class="form-group">
                            <label for="firstname" class="col-sm-2 control-label">Firstname: </label>
                            <div class="col-sm-10">
                                {!! Form::text('firstname', NULL, array('class' => 'form-control', 'placeholder' => 'User\'s firstname')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="surname" class="col-sm-2 control-label">Surname: </label>
                            <div class="col-sm-10">
                                {!! Form::text('surname', NULL, array('class' => 'form-control', 'placeholder' => 'User\'s surname')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username" class="col-sm-2 control-label">Username:</label>
                            <div class="col-sm-10">
                                {!! Form::text('username', NULL, array('class' => 'form-control', 'placeholder' => 'Username')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-10">
                                {!! Form::email('email', NULL, array('class' => 'form-control', 'placeholder' => 'User email address')) !!}
                            </div>
                        </div>
                        
                        <div class="form-group @if( $errors->has('password') ) has-error @endif">
                            <label for="password" class="col-sm-2 control-label">Password:</label>
                            <div class="col-sm-10">
                                <div class="input-group marg-bott-5">
                                    {!! Form::password('password', array('class' => 'form-control', 'id' => 'password')) !!}
                                    <span class="input-group-addon" onclick="toggle(this)"><span class="fa fa-eye"></span></span>
                                </div>
                                
                                @if( $errors->has('password') )
                                    <p class="text-danger">{!! $errors->first('password') !!}</p>
                                @endif

                                <span>Leave blank to keep current password.</span>

                                @include('HummingbirdBase::cms.users.auth.password-checker')
                            </div>
                        </div>
                        
                        <div class="form-group hide">
                            <label for="confirm-password" class="col-sm-2 control-label">Confirm Password:</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    {!! Form::password('password_confirmation', array('class' => 'form-control', 'id' => 'password_confirmation')) !!}
                                    <span class="input-group-addon" onclick="toggle(this)"><span class="fa fa-eye"></span></span>
                                </div>
                            </div>
                        </div>

                        @if(count($roles) > 0)
                            <div class="form-group">
                                <label for="role" class="col-sm-2 control-label">Role:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="role_id" id="role">
                                        @foreach($roles as $role)
                                            <option value="{!! $role->id !!}" <?php if($user->role && $user->role->id == $role->id) echo 'selected';?>>{!! $role->listing_name ?: $role->name !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="role_id" value="{!! $user->role->id !!}" />        
                        @endif

                        <div class="form-group">
                            <label for="status" class="col-sm-2 control-label">Status:</label>
                            <div class="col-sm-10">
                                {!! Form::select('status', $statuses, $user->status, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default btn-primary">Update</button>

                                @if( Auth::user()->isSuperUser() && empty($user->force_password_reset) )
                                    <a class="btn btn-default btn-warning pull-right" href="{!! route('hummingbird.users.password-force-update', $user->id) !!}"><i class="fa fa-key"></i> Force password change</a>
                                @endif
                            </div>
                        </div>
                    </section>
                </div>

                @if(!$user->isSuperUser())
                    <div class="col-sm-12">
                        <section class="panel">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="normal">User permissions</h1>
                                    <p class="intro">The permissions to perform certain operations are assigned to specific roles.</p>
                                    <p>Members or staff (or other system users) are assigned to particular roles, and through those role assignments acquire the permissions needed to perform particular system functions.</p>
                                    <div class="alert alert-warning" role="alert"><strong>Note: </strong>User permissions can be refined, based on the role they've been assigned to, by editing the below.</div>
                                </div>
                            </div>

                            <hr />
                            
                            <!-- Start Permissions -->
                            @include('HummingbirdBase::cms.partials.permissions._display_permissions', array('Object' => $user))
                            <!-- End Permissions -->

                            <button name="submit" value="update" type="submit" class="pull-right btn btn-default btn-primary">Update</button>

                            <div class="row">&nbsp;</div>
                        </section>
                    </div>
                @endif
            </div>
        {!! Form::close() !!}
    </div>
@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/js/hummingbird/permissions.js"></script>
    <script src="/themes/admin/hummingbird/default/js/hummingbird/password-strength.js"></script>

    <script>
        function simpleParallax(el, min_scroll)
        {
            min_scroll = (min_scroll <= 0) ? 0:min_scroll;

            // This variable is storing the distance scrolled
            var scrolled = jQuery('body').scrollTop() + 1;
            scrolled = (scrolled * 0.1) + min_scroll; // calculate new position
            scrolled = (scrolled > 100) ? 100:scrolled; // 100% max position
            scrolled = (scrolled < 0) ? 0:scrolled; // 0% min position
            
            // Every element with the class "scroll" will have parallax background 
            // Change the "0.3" for adjusting scroll speed.
            el.css('background-position', 'center ' + scrolled + '%');
        }

        $(document).ready(function()
        {
            if($("#PermissionTabs").length > 0) {
                $("#PermissionTabs li:first a").trigger('click');
            }

            if($('.user-profile .content-header').length > 0)
            {
                if($(".user-profile .content-header").data('background') != '')
                {
                    $(".user-profile .content-header").css('background-image', 'url(' + $(".user-profile .content-header").data('background') + ')');
                    $(".user-profile .content-header, .user-profile .content-header .header-section").css('height', '250px');
                }

                $(window).scroll(function(e) 
                {
                    simpleParallax($('.user-profile .content-header'), 0);
                });
            }
        });
    </script>
@stop
