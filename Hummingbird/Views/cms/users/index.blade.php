@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop

@section('content')
    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <div class="row">
                    @include('HummingbirdBase::cms.users.user-filter-template')
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                @if(Auth::user()->userAbility('user.create'))
                    <button id="add-user-btn" type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-user"><i class="fa fa-plus-circle"></i> Add user</button>
                @endif

                @if(Request::get('s') AND Request::get('s') != '')
                    <div class="clearfix">
                        <h3 class="normal">Searching for: {!! Request::get('s') !!}</h3>
                    </div>
                @endif

                @if(count($users) > 0)
                    {!! Form::open(array('route' => array('hummingbird.users.multi.password-force-update'), 'id' => 'userpasswordreset', 'method' => 'post')) !!}
                        @if( Auth::user()->userAbility('user.update') )
                            <button type="submit" class="btn btn-info remove-selected hide" disabled data-update-body="You are about to reset the password for the users." data-target="#userpasswordreset"><i class="fa fa-key"></i>&nbsp; Force password change for selected</button>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        @if( Auth::user()->userAbility('user.update') )
                                            <th scope="row">{!! Form::checkbox('select_all', 0, NULL, array('class' => 'checkbox-toggle', 'data-target' => '.checkbox-toggle-target')) !!}</th>
                                        @endif
                                        <th>Name</th>
                                        <th>E-mail</th>
                                        <th>Role</th>
                                        <th>Last logged in</th>
                                        <th>Latest activity</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
            
                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            @if( Auth::user()->userAbility('user.update') )
                                                <td>{!! Form::checkbox('user-selected[]', $user->id, NULL, array('class' => 'checkbox-toggle-target')) !!}</td>
                                            @endif
                                            <td>{!! Hummingbird\Models\User::returnActiveLabel($user) !!} {!! $user->name !!}</td>
                                            <td>{!! $user->email !!}</td>
                                            <td>{!! ($user->role) ? $user->role->name : '-' !!}</td>
                                            <td>{!! ($user->last_login) ? $user->last_login->diffForHumans() : '-' !!}</td>
                                            <td>{!! $user->formatActivity($user->activity()->first()) !!}</td>
                                            <td>
                                                @if(Auth::user()->userAbility('user.update'))
                                                    <a href="{!! route('hummingbird.users.status.update', ["{$user->id}"]) . "?" . http_build_query(Request::only(['s', 'user'])) !!}" class="toggle-status"><i class="fa @if( $user->isActive() ) fa-toggle-on @else fa-toggle-off @endif"></i></a>
                                                @else
                                                    {!! $page->status !!}
                                                @endif
                                            </td>
                                            <td>
                                                @if( !$user->isSuperUser() )
                                                    <a style="background-color:black;color:white;" class="btn btn-xs" href="{!! route('hummingbird.users.preview', $user->id) !!}" data-original-title="Preview {!! $user->name !!} permissions"><i class="fa fa-globe "></i></a>
                                                @endif

                                                @if( Auth::user()->userAbility('user.update') && ( !$user->isSuperUser() || ( Auth::user()->isSuperUser() && $user->isSuperUser() ) ) )
                                                    <a class="btn btn-xs btn-info" href="{!! route('hummingbird.users.show', $user->id) !!}" data-original-title="Edit {!! $user->name !!} "><i class="fa fa-edit "></i></a>
                                                @endif

                                                @if( Auth::user()->isSuperUser() && empty($user->force_password_reset) )
                                                    <a class="btn btn-xs btn-warning" href="{!! route('hummingbird.users.password-force-update', $user->id) !!}" data-toggle="tooltip" data-placement="top" title="Force password to be changed on next login"><i class="fa fa-key"></i></a>
                                                @endif

                                                @if( Auth::user()->userAbility('user.delete') && ( !$user->isSuperUser() || ( Auth::user()->isSuperUser() && $user->isSuperUser() ) ) )
                                                    {!! Form::open(array('route' => array('hummingbird.users.destroy', $user->id), 'method' => 'delete', 'class' => 'inline-block', 'id' => 'delete_user_' . $user->id)) !!}
                                                        <button id="delete_user_{!! $user->id !!}" type="submit" class="btn btn-xs btn-danger btn-confirm-delete" data-target="#delete_user_{!! $user->id !!}" data-title="{!! $user->name !!}" data-id="{!! $user->id !!}" title="Delete &quot;{!! $user->name !!}&quot;" data-delete-body="You are about to delete this page (&quot;{!! $user->name !!}&quot;)."><i class="fa fa-trash"></i></button>
                                                    {!! Form::close() !!}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    {!! Form::close() !!}
                @else
                    <h3>There are no users. Please try adding some.</h3>
                @endif

                <div class="text-center"> 
                    {!! $users->appends(['s' => Request::get('s')])->render() !!}
                </div>
            </section>
        </div>
    </div>

    <div class="row hide">
        <div class="col-md-12">
            <section class="panel">
                <h3>Run import </h3>

                <div class="table-responsive">
                    {!! Form::open(array('route' => 'hummingbird.users.import', 'method' => 'post')) !!}
                        <table class="table table-striped table-hover">
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center">
                                        <input type="submit" value="Import" name="import">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>

    @if(Auth::user()->userAbility('user.create'))
        <div class="modal fade" id="add-user">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add new user</h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                @if( session()->has('UserDuplicationError') )
                                    <div class="alert alert-info">Looks like this user has already been created, but is deleted, <a href="{!! route('hummingbird.users.re-instate', session()->get('UserDuplicationError')) !!}">would you like to re-instate them?</a></div>
                                @endif

                                {!! Form::open(array('route' => array('hummingbird.users.store'), 'class' => 'form-horizontal')) !!}
                                    <div class="form-group @if( $errors->has('firstname') ) has-error @endif">
                                        {!! Form::label('firstname', 'Firstname: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('firstname', Request::old('firstname'), array('class' => 'form-control')) !!}

                                            @if( $errors->has('firstname') )
                                                <p class="text-danger">{!! $errors->first('firstname') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group @if( $errors->has('surname') ) has-error @endif">
                                        {!! Form::label('surname', 'Surname: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('surname', Request::old('surname'), array('class' => 'form-control')) !!}

                                            @if( $errors->has('surname') )
                                                <p class="text-danger">{!! $errors->first('surname') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group @if( $errors->has('username') ) has-error @endif">
                                        {!! Form::label('username', 'Username: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('username', Request::old('username'), array('class' => 'form-control')) !!}

                                            @if( $errors->has('username') )
                                                <p class="text-danger">{!! $errors->first('username') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group @if( $errors->has('email') ) has-error @endif">
                                        {!! Form::label('email', 'Email: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('email', Request::old('email'), array('class' => 'form-control')) !!}

                                            @if( $errors->has('email') )
                                                <p class="text-danger">{!! $errors->first('email') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    @if(count($roles) > 0)
                                        <div class="form-group @if( $errors->has('role_id') ) has-error @endif">
                                            {!! Form::label('role_id', 'Role: ', array('class' => 'col-sm-3')) !!}
                                            <div class="col-sm-9">
                                                <select name="role_id" id="role_id" class="form-control">
                                                    @foreach($roles as $role)
                                                        <option value="{!! $role->id !!}">{!! $role->listing_name ?: $role->name !!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @else
                                        <input type="hidden" name="role_id" value="{!! Auth::user()->role->id !!}" />
                                    @endif

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-10">
                                            <button type="submit" class="btn btn-default btn-primary">Add user</button>
                                        </div>
                                    </div> 
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>

    <script>

        $(document).ready(function() {

            $('.filter-results').click(function(e) {
                $('.filter-form').submit();
            });
            
            @if(count($errors) > 0)
                $("#add-user-btn").trigger('click');
            @endif

            $(".checkbox-toggle").click(function(e) {
                if( typeof $(this).data('target') !== 'undefined' ) {
                    $( $(this).data('target') ).prop('checked', $(this).prop('checked'));
                }

                if( $(".checkbox-toggle-target:checked").length > 0 ) {
                    $(".remove-selected").removeAttr('disabled').removeClass('hide');
                }
                else {
                    $(".remove-selected").attr('disabled', true).addClass('hide');   
                }
            });

            if( $(".checkbox-toggle-target").length > 0 ) {
                $(".checkbox-toggle-target").click(function() {
                    if( $(".checkbox-toggle-target:checked").length > 0 ) {
                        $(".remove-selected").removeAttr('disabled').removeClass('hide');
                    }
                    else {
                        $(".remove-selected").attr('disabled', true).addClass('hide');  
                    }
                });
            }

            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop