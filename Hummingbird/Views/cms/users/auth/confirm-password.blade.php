<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{!! config()->get('HummingbirdBase::hummingbird.site_name') !!} - Confirm your password | Hummingbird CMS</title>
        <meta name="robots" content="noindex">
        <link rel="shortcut icon" href="/themes/admin/hummingbird/default/favicon.ico">

        <script src="https://cdn.tailwindcss.com"></script>
    </head>
    <body>
        <section class="w-screen h-screen bg-gray-100 dark:bg-gray-900 bg-cover m-auto">
            <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto h-full md:h-screen lg:py-0">
                <div class="flex items-center mb-6">
                    <img src="{!! asset( "themes/admin/hummingbird/default/layout/hb-icon.png" ) !!}" class="h-24" />
                </div>

                <div class="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h2>{{ __('Confirm your password') }}</h2>
                        <p>{{ __('Please confirm your password before continuing. We won\'t ask for your password again for a while (unless you log out and back in again.') }}</p>

                        <form class="space-y-4 md:space-y-6" method="POST" action="{{ route('password.confirm') }}">
                            @csrf
                            <div>
                                <label for="password" class="block mb-2 text-sm font-medium @if(!$errors->has('password')) text-gray-900 @else text-red-700 @endif">Password</label>
                                <input type="password" name="password" id="password" placeholder="••••••••" class="bg-gray-50 border @if(!$errors->has('password')) border-gray-300 text-gray-900 @else border-red-500 text-red-900 @endif text-sm rounded-lg focus:ring-sky-600 focus:border-sky-600 block w-full p-2.5">

                                @if($errors->has('password'))
                                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{!! $errors->first('password') !!}</p>
                                @endif
                            </div>

                            <div class="flex space-x-2 space-y-2 flex-wrap justify-between items-baseline">
                                <a href="{!! route('hummingbird.dashboard') !!}" class="rounded-lg py-2 underline text-blue-500 hover:text-blue-600 duration-300 text-sm">Go back home</a>

                                <button type="submit" class="text-white bg-sky-600 hover:bg-sky-700 focus:ring-4 focus:outline-none focus:ring-sky-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-sky-600 dark:hover:bg-sky-700 dark:focus:ring-sky-800">{{ __('Confirm') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>