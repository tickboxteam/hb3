<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{!! config()->get('HummingbirdBase::hummingbird.site_name') !!} - 2FA Challenge | Hummingbird CMS</title>
        <meta name="robots" content="noindex">
        <link rel="shortcut icon" href="/themes/admin/hummingbird/default/favicon.ico">

        <script src="https://cdn.tailwindcss.com"></script>
        <script>
            document.addEventListener("DOMContentLoaded", function() {
                var toggleForms = document.querySelectorAll('.two-factor-form');
                var toggleBtns = document.querySelectorAll('.toggle-2fa-btns');

                if( toggleBtns.length > 0 ) {
                    toggleBtns.forEach(function(toggleBtn) {
                        toggleBtn.addEventListener('click', function(event) {
                            toggleForms.forEach(function(toggleForm) {
                                toggleForm.style.display = (toggleForm.style.display === "none") ? "block" : "none";
                            });
                        })
                    });
                }
            });
        </script>
    </head>
    <body>
        <section class="w-screen h-screen bg-gray-100 dark:bg-gray-900 bg-cover m-auto">
            <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto h-full md:h-screen lg:py-0">
                <div class="flex items-center mb-6">
                    <img src="{!! asset( "themes/admin/hummingbird/default/layout/hb-icon.png" ) !!}" class="h-24" />
                </div>

                <div class="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h2>{{ __('Two factor challenge') }}</h2>
                        <p>{{ __('Please enter your authentication code to login.') }}</p>

                        <form class="space-y-4 md:space-y-6 two-factor-form" method="POST" action="{{ route('two-factor.login.store') }}" @if( $errors->has('recovery_code') ) style="display: none;" @endif>
                            @csrf

                            <div>
                                <label for="code" class="sr-only block mb-2 text-sm font-medium @if(!$errors->has('code')) text-gray-900 @else text-red-700 @endif">{{ __('2FA Code:') }}</label>
                                <input type="text" name="code" id="code" class="bg-gray-50 border @if(!$errors->has('code')) border-gray-300 text-gray-900 @else border-red-500 text-red-900 @endif text-sm rounded-lg focus:ring-sky-600 focus:border-sky-600 block w-full p-2.5" placeholder="2FA code" required>

                                @if($errors->has('code'))
                                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{!! $errors->first('code') !!}</p>
                                @endif

                                <div class="flex space-x-2 space-y-2 flex-wrap justify-between items-baseline">
                                    <a href="#" class="toggle-2fa-btns rounded-lg py-2 underline text-blue-500 hover:text-blue-600 duration-300 text-sm">Use recovery code</a>
                                </div>
                            </div>

                            <button type="submit" class="w-full text-white bg-sky-600 hover:bg-sky-700 focus:ring-4 focus:outline-none focus:ring-sky-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-sky-600 dark:hover:bg-sky-700 dark:focus:ring-sky-800">{{ __('Confirm Code') }}</button>
                        </form>

                        <form class="space-y-4 md:space-y-6 two-factor-form" method="POST" action="{{ route('two-factor.login.store') }}" @if( !$errors->has('recovery_code') ) style="display: none;" @endif>
                            @csrf

                            <div>
                                <label for="recovery_code" class="sr-only block mb-2 text-sm font-medium @if(!$errors->has('recovery_code')) text-gray-900 @else text-red-700 @endif">{{ __('Recovery code:') }}</label>
                                <input type="text" name="recovery_code" id="recovery_code" class="bg-gray-50 border @if(!$errors->has('recovery_code')) border-gray-300 text-gray-900 @else border-red-500 text-red-900 @endif text-sm rounded-lg focus:ring-sky-600 focus:border-sky-600 block w-full p-2.5" placeholder="Recovery Code" required>

                                @if($errors->has('recovery_code'))
                                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{!! $errors->first('recovery_code') !!}</p>
                                @endif

                                <div class="flex space-x-2 space-y-2 flex-wrap justify-between items-baseline">
                                    <a href="#" class="toggle-2fa-btns rounded-lg py-2 underline text-blue-500 hover:text-blue-600 duration-300 text-sm">Use 2FA code</a>
                                </div>
                            </div>

                            <button type="submit" class="w-full text-white bg-sky-600 hover:bg-sky-700 focus:ring-4 focus:outline-none focus:ring-sky-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-sky-600 dark:hover:bg-sky-700 dark:focus:ring-sky-800">{{ __('Confirm Code') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
