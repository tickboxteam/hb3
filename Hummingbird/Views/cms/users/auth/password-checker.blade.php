<?php 
    $SystemSettings = \Hummingbird\Models\Setting::where('key', 'system')->firstOrFail();
    $Settings = $SystemSettings->value;

    $PasswordLength = (isset($Settings['pass_length']) && is_numeric($Settings['pass_length']) && $Settings['pass_length'] >= 8) ? $Settings['pass_length'] : 8;
    $Letters  = ( isset($Settings['letters']) && filter_var( $Settings['letters'], FILTER_VALIDATE_BOOLEAN) );
    $MixedCase  = ( isset($Settings['mixed_case']) && filter_var( $Settings['mixed_case'], FILTER_VALIDATE_BOOLEAN) );
    $Numbers    = ( isset($Settings['pass_numbers']) && filter_var( $Settings['pass_numbers'], FILTER_VALIDATE_BOOLEAN) );
    $Symbols    = ( isset($Settings['pass_symbols']) && filter_var( $Settings['pass_symbols'], FILTER_VALIDATE_BOOLEAN) );
    $Uncompromised = ( isset($Settings['pass_compromised']) && filter_var( $Settings['pass_compromised'], FILTER_VALIDATE_BOOLEAN) );
?>

<div id="password-strength--progress" class="password-strength--progress hide">
    <h6>Minimum password requirements</h6>
    <ul class="list-unstyled">
        <li class="list-item" data-regex=".{<?=$PasswordLength?>,}">
            <span class="eight-character">
                <i class="fa fa-circle" aria-hidden="true"></i>
                &nbsp;At least {!! $PasswordLength !!} {!! General::singular_or_plural($PasswordLength, 'character') !!}
            </span>
        </li>

        @if( $Letters )
            <li class="list-item" data-regex="[a-zA-Z]">
                <span class="letters">
                    <i class="fa fa-circle" aria-hidden="true"></i>
                    &nbsp;Letters
                </span>
            </li>
        @endif

        @if( $MixedCase )
            <li class="list-item" data-regex="[A-Z]">
                <span class="mixed-case">
                    <i class="fa fa-circle" aria-hidden="true"></i>
                    &nbsp;MixedCase
                </span>
            </li>
        @endif

        @if( $Numbers )
            <li class="list-item" data-regex="[0-9]">
                <span class="one-number">
                    <i class="fa fa-circle" aria-hidden="true"></i>
                    &nbsp;Number (0-9)
                </span> 
            </li>
        @endif

        @if( $Symbols )
            <li class="list-item" data-regex="[^A-Za-z0-9]">
                <span class="one-special-char">
                    <i class="fa fa-circle" aria-hidden="true"></i>
                    &nbsp;Special Character (!@#$%^&*)
                </span>
            </li>
        @endif
    </ul>
</div>