@extends('HummingbirdBase::cms.layout')

@section('content')
    <div class="user-profile">
        <div class="content-header content-header-media" data-background="{!! $user->getPreference('cover-image') !!}">
            <div class="header-section">
                <h1>{!! $user->name !!} ({!! $user->role->name !!})<br /><small>{!! $user->username !!}</small></h1>
            </div>
        </div>

        @if (Session::has('success'))
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        {!! Session::get('success') !!}
                    </div>
                </div>
            </div>  
        @endif

        @if(Session::has('message'))
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-info fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        {!! Session::get('message') !!}
                    </div>
                </div>
            </div>  
        @endif

        @if (Session::has('errors'))
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        Unable to update your profile. Please see errors below.
                    </div>
                </div>
            </div>  
        @endif

        @if( !empty(Auth::user()->force_password_reset) )
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-info fade in">
                        Your password has expired. So please change your password to something memorable. It'll need to match the system requirements which are shown below (when you start entering a password).
                    </div>
                </div>
            </div>
        @endif

        @if( !Auth::user()->two_factor_secret && !Auth::user()->two_factor_confirmed_at && is_2fa_forced() )
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-info fade in">
                        Two factor authentication is required before you can complete any actions.
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-8">
                <section class="panel"> 
                    {!! Form::open(array('route' => 'hummingbird.profile', 'class' => 'form-horizontal')) !!}
                        <div class="form-group @if( $errors->has('firstname') ) has-error @endif">
                            <label for="name" class="col-sm-2 control-label">Firstname: </label>
                            <div class="col-sm-10">
                                {!! Form::text('firstname', $user->firstname, array('class' => 'form-control', 'placeholder' => 'Your firstname')) !!}
                                
                                @if( $errors->has('firstname') )
                                    <p class="text-danger">{!! $errors->first('firstname') !!}</p>
                                @endif
                            </div>
                        </div>

                        <div class="form-group @if( $errors->has('surname') ) has-error @endif">
                            <label for="surname" class="col-sm-2 control-label">Surname: </label>
                            <div class="col-sm-10">
                                {!! Form::text('surname', $user->surname, array('class' => 'form-control', 'placeholder' => 'Your surname')) !!}
                                
                                @if( $errors->has('surname') )
                                    <p class="text-danger">{!! $errors->first('surname') !!}</p>
                                @endif
                            </div>
                        </div>

                        <div class="form-group @if( $errors->has('username') ) has-error @endif">
                            <label for="username" class="col-sm-2 control-label">Username:</label>
                            <div class="col-sm-10">
                                <input id="username" name="username" type="text" class="form-control" placeholder="Username" value="{!! $user->username !!}">
                                
                                @if( $errors->has('username') )
                                    <p class="text-danger">{!! $errors->first('username') !!}</p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group @if( $errors->has('email') ) has-error @endif">
                            <label for="email" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-10">
                                <input id="email" name="email" type="text" class="form-control" placeholder="Email address" value="{!! $user->email !!}">
                                
                                @if( $errors->has('email') )
                                    <p class="text-danger">{!! $errors->first('email') !!}</p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group @if( $errors->has('password') ) has-error @endif">
                            <label for="password" class="col-sm-2 control-label">Password:</label>
                            <div class="col-sm-10">
                                <div class="input-group marg-bott-5">
                                    {!! Form::password('password', array('class' => 'form-control', 'id' => 'password')) !!}
                                    <span class="input-group-addon" onclick="toggle(this)"><span class="fa fa-eye"></span></span>
                                </div>

                                @if( $errors->has('password') )
                                    @foreach ($errors->get('password') as $error)
                                        <p class="text-danger">{!! $error !!}</p>
                                    @endforeach
                                @endif

                                <span>Leave blank to keep current password.</span>

                                @include('HummingbirdBase::cms.users.auth.password-checker')
                            </div>
                        </div>
                        
                        <div class="form-group hide">
                            <label for="confirm-password" class="col-sm-2 control-label">Confirm Password:</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    {!! Form::password('password_confirmation', array('class' => 'form-control', 'id' => 'password_confirmation')) !!}
                                    <span class="input-group-addon" onclick="toggle(this)"><span class="fa fa-eye"></span></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default btn-primary">Update</button>
                                <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default pull-right"><i class="fa fa-sign-out"></i> Logout</a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </section>
            </div>
            
            <div class="col-md-4">
                <section class="panel clearfix pt-5 pb-5">
                    <h3 style="margin-top:0;">Two Factor Authentication</h3>
                    <p>Add additional security to your account using two factor authentication.</p>
                    
                    @if( auth()->user()->two_factor_secret && !auth()->user()->two_factor_confirmed_at )
                        <div class="alert alert-block alert-info">
                            Please finish configuring two factor authentication below.
                        </div>
                    @endif

                    @if (session('status') == 'two-factor-authentication-confirmed' )
                        <div class="alert alert-block alert-success">
                            Two factor authentication confirmed and enabled successfully.
                        </div>
                    @endif

                    @if( auth()->user()->two_factor_secret && auth()->user()->two_factor_confirmed_at )
                        <p>Two factor authentication is now enabled. Scan the QR code using your phone's authenticator application.</p>
                    @endif

                    @if( auth()->user()->two_factor_secret )
                        <div class="text-center" style="padding-top: 20px; padding-bottom:20px; margin-bottom: 10px;">
                            {!! auth()->user()->twoFactorQrCodeSvg() !!}
                        </div>
                    @endif

                    @if( auth()->user()->two_factor_secret && !auth()->user()->two_factor_confirmed_at )
                        {!! Form::open(array('route' => 'two-factor.confirm', 'class' => 'form-horizontal')) !!}
                            @csrf

                            <div class="form-group @if( $errors->confirmTwoFactorAuthentication->has('code') ) has-error @endif">
                                <label for="code" class="col-sm-3 control-label">{{ __('2FA code:') }}</label>
                                <div class="col-sm-9">
                                    <input id="code" name="code" type="text" class="form-control">

                                    @if( $errors->confirmTwoFactorAuthentication->has('code') )
                                        <p class="text-danger">{!! $errors->confirmTwoFactorAuthentication->first('code') !!}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group @if( $errors->confirmTwoFactorAuthentication->has('code') ) has-error @endif">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <button type="submit" class="btn btn-default btn-primary pull-right">{{ __('Confirm 2FA setup') }}</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    @endif

                    @if(auth()->user()->two_factor_secret)
                        @if( auth()->user()->two_factor_confirmed_at ) 
                            <h4>Recovery codes</h4>
                            <p>Store these recovery codes in a secure password manager. They can be used to recover access to your account if your two factor authentication device is lost or unable to generate the appropriate code.</p>

                            @if (session('status') == 'recovery-codes-generated' )
                                <div class="alert alert-block alert-success">
                                    Your recovery codes have been regenerated. Please store them in a secure place.
                                </div>
                            @endif
                            
                            <figure class="highlight" style="padding: 9px 14px; margin-bottom: 14px; background-color: #f7f7f9; border: 1px solid #e1e1e8; border-radius: 4px;">
                                @foreach(auth()->user()->recoveryCodes() as $code)
                                    <p>{!! $code !!}
                                @endforeach                                
                            </figure>
                        @endif
                            
                        @if( auth()->user()->two_factor_confirmed_at )
                            {!! Form::open(array('route' => 'two-factor.recovery-codes', 'class' => 'form-horizontal')) !!}
                                @csrf
                                <button class="btn btn-primary float-left pull-left">Regenerate recovery codes</button>
                            {!! Form::close() !!}
                        @endif

                        @if( auth()->user()->two_factor_confirmed_at )
                            {!! Form::open(array('route' => 'two-factor.enable', 'class' => 'form-horizontal confirm-password')) !!}
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger float-right pull-right">Disable</button>
                            {!! Form::close() !!}
                        @endif
                    @else
                        <div class="alert alert-block alert-info">Two factor authentication is not enabled.</div>

                        {!! Form::open(array('route' => 'two-factor.disable', 'class' => 'form-horizontal confirm-password')) !!}
                            @csrf
                            <button class="btn btn-primary">Enable</button>
                        {!! Form::close() !!}
                    @endif
                </section>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/js/hummingbird/password-strength.js"></script>
    
    <script>
        function simpleParallax(el, min_scroll)
        {
            min_scroll = (min_scroll <= 0) ? 0:min_scroll;

            //This variable is storing the distance scrolled
            var scrolled = jQuery('body').scrollTop() + 1;
            scrolled = (scrolled * 0.3) + min_scroll; //calculate new position
            scrolled = (scrolled > 100) ? 100:scrolled; //100% max position
            scrolled = (scrolled < 0) ? 0:scrolled; //0% min position
            
            //Every element with the class "scroll" will have parallax background 
            //Change the "0.3" for adjusting scroll speed.
            el.css('background-position', 'center ' + scrolled + '%');
        }

        $(document).ready(function()
        {
            if($('.user-profile .content-header').length > 0)
            {
                if($(".user-profile .content-header").data('background') != '')
                {
                    $(".user-profile .content-header").css('background-image', 'url(' + $(".user-profile .content-header").data('background') + ')');
                    $(".user-profile .content-header, .user-profile .content-header .header-section").css('height', '250px');
                }

                $(window).scroll(function(e) 
                {
                    simpleParallax($('.user-profile .content-header'), 50);
                });
            }
        });

        function toggle(el){
            let password_input = el.parentNode.querySelector('input');
            let icon = el.querySelector('.fa')

            if( password_input ) {
                password_input.setAttribute('type', ( password_input.getAttribute('type') == 'password' ) ? 'text' : 'password' );
                icon.classList.toggle('fa-eye-slash');
            }
        }
    </script>
@stop