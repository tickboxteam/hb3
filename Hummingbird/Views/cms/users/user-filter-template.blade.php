<?php
/**
 * Hummingbird: Page filter template, for re-ordering pages
 *
 * @package     HB3
 * @author      Geona Antony, Daryl Phillips
 * @copyright   2021
 * @since       1.1.23
 * @version     2
*/?>

{!! Form::open(array('route' => array('hummingbird.users.index'), 'method' => 'get', 'class' => 'form-inline export-form')) !!}
    <div class="col-md-3">
        <h6>Search:</h6>
        {!! Form::text('s', Request::get('s'), array('class' => 'form-control')) !!}
    </div>

    <div class="col-md-3">
        <h6>Role:</h6>
        {!! Form::select('filter_by_role', [NULL => '--- All ---'] + Hummingbird\Models\Role::whereNull('deleted_at')->orderBy('name')->pluck('name', 'id')->all(), Request::get('filter_by_role'), array('class' => 'form-control')) !!}
    </div>

    <div class="col-md-2">
        <h6>By status:</h6>
        {!! Form::select('status', [NULL => '--- All ---', 'active' => 'Active', 'inactive' => 'Inactive', 'banned' => 'Banned'], Request::get('status'), array('class' => 'form-control')) !!}
    </div>

    <div class="col-md-2">
        <h6>Order by fields:</h6>
        {!! Form::select('order_by_fields', ['name' => 'Name', 'role_id' => 'Role', 'last_login' => 'Last logged in'], Request::get('order_by_fields'), array('class' => 'form-control')) !!}
    </div>


    <div class="col-md-2">
        <h6>Order by direction:</h6>
        {!! Form::select('order_by_direction', ['ASC' => 'ASC', 'DESC' => 'DESC'], Request::get('order_by_direction'), array('class' => 'form-control')) !!}
    </div>

    <div class="col-md-3">
        <h6>&nbsp;</h6>
        <button type="submit" class="btn btn-default filter-results" data-url="{!! route('hummingbird.pages.index') !!}">Filter results</button>
        
        <a href="{!! url('hummingbird/pages') !!}" class="btn">Reset search</a>

    </div>
{!! Form::close() !!}

