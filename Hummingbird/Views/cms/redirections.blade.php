@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-info fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">

                <div class="row">
                    {!! Form::open(array('route' => array('hummingbird.redirections.index'), 'method' => 'get', 'class' => 'export-form')) !!}
                        <div class="col-md-2">
                            <h6>Search:</h6>
                            {!! Form::text('s', Request::get('s'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-2">
                            <h6>By category:</h6>
                            {!! Form::select('category', [NULL => '--- All ---'] + Hummingbird\Models\Redirections::categoriesOnly()->pluck('category', 'category')->all(), Request::get('category'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-2">
                            <h6>Type:</h6>
                            {!! Form::select('type', [NULL => '--- All ---'] + Hummingbird\Models\Redirections::get_types_selection(), Request::get('type'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-2">
                            <h6>Function:</h6>
                            {!! Form::select('function', [NULL => '--- All ---', 'Short URL' => 'Short URL', 'Site structure' => 'Site structure', 'Tracking' => 'Tracking'], Request::get('function'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-2">
                            <h6>Order by:</h6>
                            {!! Form::select('orderBy', ['id' => 'ID', 'from' => 'From', 'to' => 'To', 'no_used' => '# times used', 'last_used' => 'Last used', 'created_at' => 'Date added'], Request::get('orderBy'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-2">
                            <h6>Order:</h6>
                            {!! Form::select('order', ['asc' => 'ASC', 'desc' => 'DESC'], Request::get('order'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-2 text-center">
                            <a href="#" class="btn btn-default export-action" data-url="{!! route('hummingbird.redirections.index') !!}" >Filter results</a>

                            @if(Auth::user()->userAbility('redirections.read'))<a href="#" class="btn btn-default export-action" data-url="{!! route('hummingbird.redirections.export') !!}">Export CSV</a>@endif

                            <div class="clearfix"></div>

                            <a href="{!! route('hummingbird.redirections.index') !!}" class="btn">Reset search</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </section>

            <section class="panel">
                <div class="clearfix">
                    @if( Hummingbird\Models\Redirections::onlyTrashed()->count() > 0 && Auth::user()->userAbility('redirections.delete') )
                        <a href="{!! route('hummingbird.redirections.deleted') !!}" class="pull-left btn btn-default"><i class="fa fa-archive"></i> Show deleted redirects ({!! Hummingbird\Models\Redirections::onlyTrashed()->count() !!})</a>         
                    @endif

                    @if(count($redirects) > 0 && Auth::user()->userAbility('redirections.delete'))
                        {!! Form::open(array('route' => array('hummingbird.redirections.destroy', 'remove-all'), 'method' => 'delete', 'id' => 'remove-all-redirects')) !!}
                            <button type="submit" class="btn btn-danger pull-right btn-confirm-delete" style="margin-left:10px;" data-target="#remove-all-redirects" data-delete-body="You are about to delete all available redirects."><i class="fa fa-trash"></i> Remove all</button>
                        {!! Form::close() !!}
                    @endif

                    @if(Auth::user()->userAbility('redirections.create'))
                        <button id="add-redirect-btn" type="button" class="pull-right btn btn-info" data-toggle="modal" style="margin-left:10px;" data-target="#add-redirect"><i class="fa fa-plus-circle"></i> Add redirect</button>
                    @endif

                    <button type="button" class="pull-right btn btn-default" data-toggle="modal" data-target="#help"style="margin-left:10px;"><i class="fa fa-info"></i> Help</button>
                </div>

                @if ($searchTerm)
                    <div class="clearfix">
                        <h3 class="normal">Searching for: {!! $searchTerm !!}</h3>
                    </div>
                @endif

                <div class="row clearfix" style="margin-top:10px;">
                    <div class="col-md-12"> 
                        @if(count($redirects) > 0)
                            {!! Form::open(array('route' => array('hummingbird.redirections.destroy', 'delete'), 'id' => 'redirectsdestroy', 'method' => 'delete')) !!}
                                @if( Auth::user()->userAbility('redirections.delete') )
                                    <button type="submit" class="btn btn-danger btn-confirm-delete remove-selected hide" disabled data-delete-body="You are about to delete these redirects." data-target="#redirectsdestroy"><i class="fa fa-trash"></i>&nbsp; Remove selected</button>
                                @endif

                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                @if( Auth::user()->userAbility('redirections.delete') )
                                                    <th scope="row">{!! Form::checkbox('select_all', 0, NULL, array('class' => 'checkbox-toggle', 'data-target' => '.checkbox-toggle-target')) !!}</th>
                                                @endif

                                                <th>ID</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Type</th>
                                                <th>Category</th>
                                                <th>Function</th>
                                                <th>Last used</th>
                                                <th># Used</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($redirects as $redirect)
                                                <tr>
                                                    @if( Auth::user()->userAbility('redirections.delete') )
                                                        <td>{!! Form::checkbox('delete-selected[]', $redirect->id, NULL, array('class' => 'checkbox-toggle-target')) !!}</td>
                                                    @endif

                                                    <td>{!! $redirect->id !!}</td>
                                                    <td>{!! $redirect->from !!}</td>
                                                    <td style="word-break:break-word;">{!! $redirect->to !!}</td>
                                                    <td>{!! $redirect->type !!}</td>
                                                    <td>
                                                        @if(!empty($redirect->category))
                                                            <span class="badge badge--hummingbird">{!! $redirect->category !!}</span>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>

                                                    <td>@if( $redirect->function ) <span class="badge badge--hummingbird">{!! $redirect->function !!}</span> @else - @endif</td>

                                                    @if(null === $redirect->last_used)
                                                        <td>n/a</td>
                                                    @else
                                                        <td class="text-nowrap">{!! $redirect->last_used->format('d-m-Y H:i:s') !!}</td>
                                                    @endif

                                                    <td>{!! number_format($redirect->no_used) !!}</td>

                                                    <td class="text-nowrap">
                                                        @if(Session::has('microsite-builder'))
                                                            <a href="{!! url($redirect->from) !!}" class="btn btn-default btn-xs" target="_blank"><i class="fa fa-globe"></i></a>
                                                        @else
                                                            <a href="{!! url($redirect->from) !!}" class="btn btn-default btn-xs" target="_blank"><i class="fa fa-globe"></i></a>
                                                        @endif

                                                        @if(Auth::user()->userAbility('redirections.update'))<a href="{!! route('hummingbird.redirections.show', $redirect->id) !!}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>@endif

                                                        @if(Auth::user()->userAbility('redirections.delete'))
                                                            <button id="redirect_destroy_{!! $redirect->id !!}" name="delete-individual" value="{!! $redirect->id !!}" type="submit" class="btn btn-xs btn-danger btn-confirm-delete" data-delete-body="You are about to delete this redirect." data-target="#redirect_destroy_{!! $redirect->id !!}"><i class="fa fa-trash"></i></button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            {!! Form::close() !!}

                            <div class="text-center"> 
                                {!! $redirects->appends(Request::except('page'))->render() !!}
                            </div>
                        @else
                            <div class="clearfix">&nbsp;</div>
                            <div class="alert alert-info text-center">No redirects have been added</div>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>

    @if(Auth::user()->userAbility('redirections.create'))
        <div class="modal fade" id="add-redirect">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add new redirect</h4>
                    </div>

                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12"> 
                                {!! Form::open(array('route' => array('hummingbird.redirections.store'), 'class' => 'form-horizontal')) !!}
                                    <div class="form-group @if( $errors->has('from') ) has-error @endif">
                                        {!! Form::label('from', 'From: *', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('from', Request::old('from'), array('class' => 'form-control')) !!}

                                            @if( $errors->has('from') )
                                                <p class="text-danger">{!! $errors->first('from') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group @if( $errors->has('to') ) has-error @endif">
                                        {!! Form::label('to', 'To: *', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('to', Request::old('to'), array('class' => 'form-control')) !!}

                                            @if( $errors->has('to') )
                                                <p class="text-danger">{!! $errors->first('to') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group @if( $errors->has('type') ) has-error @endif">
                                        {!! Form::label('type', 'Type: *', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::select('type', [NULL => '--- Please select ---'] + Hummingbird\Models\Redirections::get_types_selection(), '', array('class' => 'form-control')) !!}

                                            @if( $errors->has('type') )
                                                <p class="text-danger">{!! $errors->first('type') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group @if( $errors->has('category') ) has-error @endif">
                                        {!! Form::label('category', 'Category: *', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::select('category', redirects_get_category_list(), NULL, array('class' => 'form-control', 'id' => 'category')) !!}

                                            @if( $errors->has('category') )
                                                <p class="text-danger">{!! $errors->first('category') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group @if(Request::old('category') != 'NEW') hide @endif @if( $errors->has('custom_category') ) has-error @endif">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            {!! Form::text('custom_category', NULL, array('class' => 'form-control', 'id' => 'custom_category')) !!}
                                            @if( $errors->has('custom_category') )
                                                <p class="text-danger">{!! $errors->first('custom_category') !!}</p>
                                            @endif

                                            <span class="help-block">Enter a new category name for this redirect</span>
                                        </div>
                                    </div>

                                    <div class="form-group @if( $errors->has('function') ) has-error @endif">
                                        {!! Form::label('function', 'Function: *', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::select('function', [NULL => '--- Please select ---', 'Short URL' => 'Short URL', 'Site structure' => 'Site structure', 'Tracking' => 'Tracking'], NULL, array('class' => 'form-control', 'id' => 'function')) !!}

                                            @if( $errors->has('function') )
                                                <p class="text-danger">{!! $errors->first('function') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('comments', 'Comments: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('comments', NULL, array('class' => 'form-control textareas', 'data-summary-limit' => 255)) !!}
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-10">
                                            <button type="submit" class="btn btn-default btn-primary">Add redirect</button>
                                        </div>
                                    </div> 
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif


    <div class="modal fade" id="help">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Help: Website Redirects</h4>
                </div>

                <div class="modal-body">
                    <h4>You can:</h4>
                    <p>Keep your content fresh and clean by removing dead links. Google will applaud you for it! Go one step further and also create marketing URLs. <strong><i>Easy.</i></strong></p>
                    <p>You can create:</p>
                    <ul>
                        <li>301 Redirects or permanent redirects</li>
                        <li>302 Redirects or temporary redirects</li>
                        <li>Visible redirects - to give the user a sense of movement</li>
                    </ul>

                    <h4>Other Helpful hints:</h4>
                    <ul>
                        <li>Export:
                            <ul>
                                <li>Export redirects not used in the last six months</li>
                                <li>Export all (or customised) reports</li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            $(".checkbox-toggle").click(function(e) {
                if( typeof $(this).data('target') !== 'undefined' ) {
                    $( $(this).data('target') ).prop('checked', $(this).prop('checked'));
                }

                if( $(".checkbox-toggle-target:checked").length > 0 ) {
                    $(".remove-selected").removeAttr('disabled').removeClass('hide');
                }
                else {
                    $(".remove-selected").attr('disabled', true).addClass('hide');   
                }
            });

            if( $(".checkbox-toggle-target").length > 0 ) {
                $(".checkbox-toggle-target").click(function() {
                    if( $(".checkbox-toggle-target:checked").length > 0 ) {
                        $(".remove-selected").removeAttr('disabled').removeClass('hide');
                    }
                    else {
                        $(".remove-selected").attr('disabled', true).addClass('hide');  
                    }
                });      
            }

            if($("#category").length > 0) {
                $("#category").change(function() {
                    if($("#category").val() == 'NEW') {
                        $("#custom_category").closest('.form-group').removeClass("hide");   
                    }
                    else {
                        $("#custom_category").closest('.form-group').addClass("hide");
                    }
                });
            }

            @if(count($errors) > 0)
                $("#add-redirect-btn").trigger('click');
            @endif
    
            $('.export-form a.export-action').click(function(e) {
                e.preventDefault();

                $('.export-form').attr('action', $(this).data('url')).submit();
            });
        });
    </script>
@stop