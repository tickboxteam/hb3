<div class="modal fade" id="content-settings">
    <div class="modal-dialog modal-x-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Template Options</h4>
            </div>
            <div class="modal-body" id="options">
                <div class="items clearfix">
                    <ul>
                        <li class="modal-option" data-type="row" data-for="container,column"> 
                            <div class="title">Row</div>
                            <div class="description"><p>Insert a new row</p></div>
                        </li>
                        <li class="modal-option" data-type="column" data-for="row"> 
                            <div class="title">Column</div>
                            <div class="description"><p>Insert a new column</p></div>
                        </li>
                        <li class="modal-option hide" data-type="side-nav" data-for="container,row,column"> 
                            <div class="title">Side Navigation</div>
                            <div class="description"><p>Insert a side navigation</p></div>
                        </li>
                        <li class="modal-option" data-type="wysiwyg" data-for="container,row,column"> 
                            <div class="title">HTML</div>
                            <div class="description"><p>Insert a text block</p></div>
                        </li>
                        <li class="modal-option" data-type="image" data-for="container,row,column">    
                            <div class="title">Image</div>
                            <div class="description"><p>Insert a media item</p></div>
                        </li>
                        
                    </ul>
                </div>

                <!-- <h3>WYSIWYG Editor</h3> -->
                <!-- <a href="#" class="modal-option" data-type="wysiwyg"><img width="100" src="http://www.departments.bucknell.edu/isr/TeachingAndTechnology/Blackboard/WhatsNew61/images/WYSIWYG1.jpg" /></a> -->
            </div>

            <div class="modal-body hide" id="image-library">
                <div class="row"></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="settings-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Settings</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">ID:</label>
                        <div class="col-sm-10">
                            <input id="id" name="id" type="text" class="form-control" placeholder="ID" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Class:</label>
                        <div class="col-sm-10">
                            <input id="class" name="class" type="text" class="form-control" placeholder="Class" value="">
                            <input id="hidden-classes" name="hidden-classes" type="hidden"> 
                        </div>
                    </div>

                    <div class="form-group column-size hide">
                        <label for="username" class="col-sm-2 control-label">Class:</label>
                        <div class="col-sm-10">
                            <p class="m-bot-none">
                                <?php for($i = 1; $i <= 12; $i++)
                                {?>
                                    <a href="#" data-size="<?=$i?>" class="btn btn-sm btn-default column-width"><?=$i?></a>
                                <?php }?>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="close-modal button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->