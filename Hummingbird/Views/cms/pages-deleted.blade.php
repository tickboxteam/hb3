@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {{ Session::get('success') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-info alert-danger fade in">
                {{ Session::get('message') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {{ Session::get('error') }}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="table">
                <table class="table table-striped">
                    <thead>
                        <th scope="row">Title</th>
                        <th>Parent page</th>
                        <th>Date Deleted</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach($pages as $page)
                        
                            <tr>
                                <?php 
                                    $link = ($page->permalink[0] != '/') ? '/'.$page->permalink:$page->permalink;
                                    $link = str_replace("//", "/", $link);
                                ?>
                                
                                <td>
                                    @if(Auth::user()->userAbility('page.update'))<a href='/{{App::make('backend_url')}}/pages/edit/{{$page->id}}'>@endif
                                        {{$page->title}}
                                    @if(Auth::user()->userAbility('page.update'))</a>@endif
                                </td>
                                <td>{{($page->parentpage_id > 0) ? $page->parentpage->title : '-'}}</td>
                                <td>{{$page->deleted_at}}</td>
                                <td>
                                    @if($page->status == 'public')
                                        <a target="_blank" href="{{$link}}" class="btn btn-xs" style="background-color:black;color:white;"><i class="fa fa-globe"></i> Preview</a>
                                    @endif

                                    @if(Auth::user()->userAbility('page.update'))<a href='/{{App::make('backend_url')}}/pages/edit/{{$page->id}}' class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>@endif
                                    @if(Auth::user()->userAbility('page.delete') AND !$page->locked)<a href='/{{App::make('backend_url')}}/pages/delete/{{$page->id}}' class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>@endif
                                </td>
                            </tr>
                        
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>


@stop