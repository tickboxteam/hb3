<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{!! config()->get('HummingbirdBase::hummingbird.site_name') !!} - Login | Hummingbird CMS</title>
        <meta name="robots" content="noindex">
        <link rel="shortcut icon" href="/themes/admin/hummingbird/default/favicon.ico">

        <script src="https://cdn.tailwindcss.com"></script>
    </head>
    <body>
        <section class="w-screen h-screen bg-gray-100 dark:bg-gray-900 bg-cover m-auto">
            <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto h-full md:h-screen lg:py-0">
                <div class="flex items-center mb-6">
                    <img src="{!! asset( "themes/admin/hummingbird/default/layout/hb-icon.png" ) !!}" class="h-24" />
                </div>

                <div class="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <form class="space-y-4 md:space-y-6" action="{!! route('hummingbird.login-validate') !!}" method="POST">
                            @csrf
                            <div>
                                <label for="username" class="block mb-2 text-sm font-medium @if(!$errors->has('username')) text-gray-900 @else text-red-700 @endif">Username</label>
                                <input type="text" name="username" id="username" class="bg-gray-50 border @if(!$errors->has('username')) border-gray-300 text-gray-900 @else border-red-500 text-red-900 @endif text-sm rounded-lg focus:ring-sky-600 focus:border-sky-600 block w-full p-2.5" placeholder="Username or email" value="{!! request()->old('username') !!}" required>

                                @if($errors->has('username'))
                                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{!! $errors->first('username') !!}</p>
                                @endif
                            </div>
                            <div>
                                <label for="password" class="block mb-2 text-sm font-medium @if(!$errors->has('password')) text-gray-900 @else text-red-700 @endif">Password</label>
                                <input type="password" name="password" id="password" placeholder="••••••••" class="bg-gray-50 border @if(!$errors->has('password')) border-gray-300 text-gray-900 @else border-red-500 text-red-900 @endif text-sm rounded-lg focus:ring-sky-600 focus:border-sky-600 block w-full p-2.5" required>

                                @if($errors->has('password'))
                                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{!! $errors->first('password') !!}</p>
                                @endif
                            </div>

                            <div>
                                <label class="inline-flex items-center mb-5 cursor-pointer">
                                    <input type="hidden" name="remember" value="0" />

                                    <input name="remember" type="checkbox" value="1" class="sr-only peer" checked>
                                    
                                    <div class="relative w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:w-5 after:h-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600 dark:peer-checked:bg-blue-600"></div>
                                    <span class="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300">Remember me</span>
                                </label>
                            </div>

                            <button type="submit" class="w-full text-white bg-sky-600 hover:bg-sky-700 focus:ring-4 focus:outline-none focus:ring-sky-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-sky-600 dark:hover:bg-sky-700 dark:focus:ring-sky-800">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>