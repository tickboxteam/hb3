@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/nestable/nestable.css" />
@stop

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="normal pull-left">Re-ordering menu</h1>

                    @if($menu->menuitems->count() > 0)
                        {!! Form::open() !!}
                            <button type="submit" class="pull-right btn btn-primary"><i class="fa fa-save"></i> Update menu</button>
                            
                            <div class="clearfix">&nbsp;</div>
                            <p>Do you want to change the order of your menu items and how they display? Simply drag-and-drop your menu items into their new order.</p>

                            <div class="clearfix">&nbsp;</div>
                            <div id="SortOrder" class="dd">
                                <ol class="dd-list">
                                    @foreach($menu->topLevelItems()->get() as $menuitem)
                                        @include('HummingbirdBase::cms.menus._menu_item_reorder_list', ['menuitem' => $menuitem, 'Counter' => 0])
                                    @endforeach
                                </ol>
                            </div>

                            {!! Form::hidden('SortOrder', '', array('id' => 'SortOrder_output')) !!}
                        {!! Form::close() !!}
                    @else
                        <div class="clearfix">&nbsp;</div>
                        <div class="alert alert-box alert-warning text-center">There are no menus to re-order</div>
                    @endif
                </div>
            </div>
        </section>
    </div>
</div>

@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/lib/nestable/nestable.js"></script>
    <script>
        var Nestable = function () {
            var updateOutput = function (e) {
                var list    = e.length ? e : $(e.target),
                    output  = list.data('output');

                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                } else {
                    output.val('JSON browser support required for this demo.');
                }
            };

            // activate Nestable for list 1
            $('#SortOrder').nestable().on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#SortOrder').data('output', $('#SortOrder_output')));
        }();
    </script>
@stop