@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/jcrop/jquery.Jcrop.min.css" />
@stop


@section('content')
    <?php
        //http://burfieldcreative.co.uk/image-sizes-wordpress-website/
        $FileHelper = new Hummingbird\Libraries\FileHelper;
        $file_extension = $FileHelper->getFileExtension($image->location);
        $isImage = $FileHelper->isImage($file_extension);
        $exists = file_exists( storage_path('app/public') . $image->location);

        if($isImage && $exists) {
            list($width, $height) = getimagesize(storage_path('app/public') . $image->location);
            $ratio = $width/$height;

            $image_loc = storage_path('app/public') . $image->location;

            $type = pathinfo($image_loc, PATHINFO_EXTENSION);
            $img = file_get_contents($image_loc);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);
        }
    ?>
    @if($isImage && !$exists)
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    Image no longer found on the server. <a href="{!! route('hummingbird.media.delete-item', $image->id) !!}" class="media-delete" data-id="{!! $image->id !!}">Delete permanently?</a>.
                </div>
            </div>
        </div>  
    @endif

    @if($isImage && $exists)
        <div class="row">
            <div class="col-md-8 image-calc" data-col-size="8" data-ratio="{!! $ratio !!}">
                <div class="image-holder">
                    <img id="target" src="{!! $image->location !!}" class="img-responsive" data-width="{!! $width !!}" data-height="{!! $height !!}" />
                </div>
            </div>

            <div class="col-md-4 pull-right">
                <div class="row">
                    <div class="col-md-12">
                        <section class="panel">
                            <h4>Options</h4>

                            <div class="form-group clearfix">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default constrain" data-toggle="tooltip" data-placement="right" title="Constrain proportions">
                                        <input type="checkbox" autocomplete="off" checked> <i class="fa fa-lock"></i>
                                    </label>
                                    <label class="btn btn-default rotate rotate-left hide" data-toggle="tooltip" data-placement="right" title="Rotate image -45&deg;">
                                        <input type="checkbox" autocomplete="off" checked> <i class="fa fa-rotate-left"></i>
                                    </label>
                                    <label class="btn btn-default rotate rotate-right hide" data-toggle="tooltip" data-placement="right" title="Rotate image 45&deg;">
                                        <input type="checkbox" autocomplete="off" checked> <i class="fa fa-rotate-right"></i>
                                    </label>
                                    <label class="btn btn-default crop" data-toggle="tooltip" data-placement="right" title="Crop image">
                                        <input type="checkbox" autocomplete="off" checked> <i class="fa fa-crop"></i>
                                    </label>
                                </div>
                            </div>

                            <h4>Scale</h4>
                            {!! Form::open(array('route' => array('hummingbird.media.postEditImage', $image->id), 'method' => 'post', 'id' => 'scale-form')) !!}
                                <input type="hidden" name="update" value="scale" />

                                <div class="form-group ratio hide">
                                    <label>Ratio</label>
                                    <select id="ratio" class="form-control">
                                        <option value="{!! 16/9 !!}">16:9</option>
                                        <option value="{!! 4/3 !!}">4:3</option>
                                    </select>
                                </div>                        

                                <div class="form-group">
                                    <label>Width</label>
                                    <input name="width" type="text" data-type="width" class="form-control image-scale" placeholder="Scale width" value="{!! $width !!}">
                                </div>
                                <div class="form-group">
                                    <label>Height</label>
                                    <input name="height" type="text" data-type="height" class="form-control image-scale" placeholder="Scale height" value="{!! $height !!}">
                                </div>

                                <button type="submit" class="btn btn-info">Scale</button>
                            {!! Form::close() !!}
                        </section>
                    </div>
                    <div class="col-md-12">
                        {!! Form::open(array('route' => array('hummingbird.media.postEditImage', $image->id), 'method' => 'post')) !!}
                            <input type="hidden" id="type" name="type" value="scale">
                            <input type="hidden" name="update" value="save">

                            <input type="hidden" id="w" name="w">
                            <input type="hidden" id="h" name="h">
                            <input type="hidden" id="x" name="x">
                            <input type="hidden" id="y" name="y">

                            <input type="hidden" id="width" name="width" value="{!! $width !!}" />
                            <input type="hidden" id="height" name="height" value="{!! $height !!}" />

                            <button type="submit" class="create-image btn btn-primary pull-right"><i class="fa fa-image"></i> Create new image</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <style>
            .image-holder-crop {}
            .image-holder-crop .holding-crop {border:1px dashed #999; padding:20px;margin:auto;text-align: center;height:500px;}
            .btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open > .dropdown-toggle.btn-default {background-color:#00AEEF;color:white;border-color:#00AEEF;}
        </style>
    @endif
@stop

@if($isImage && $exists)
    @section('scripts')
        <script src="/themes/admin/hummingbird/default/lib/jcrop/jquery.Jcrop.min.js"></script>

        <script>
            var locked;
            var width = {!! $width !!};
            var height = {!! $height !!};
            var rotated = 0;
            var ratio = 16/9;

            function getOuterHeight(el)
            {
                return el.outerHeight();
            }

            function initJCrop2()
            {
                $('#target').Jcrop(
                {
                    // start off with jcrop-light class
                    aspectRatio: ratio,
                    bgFade:     true,
                    bgOpacity: .5,
                    bgColor: 'black',
                    onSelect: updateCoords,
                    trueSize: [width, height]
                });
            }

            function updateCoords(c)
            {
                $('#x').val(Math.round( parseFloat(c.x)));
                $('#y').val(Math.round( parseFloat(c.y)));
                $('#w').val(Math.round( parseFloat(c.w)));
                $('#h').val(Math.round( parseFloat(c.h)));
            };

            function checkCoords()
            {
                if (parseInt($('#w').val())) return true;
                alert('Please select a crop region then press submit.');
                return false;
            };

            $(document).ready(function()
            {
                // The variable jcrop_api will hold a reference to the
                // Jcrop API once Jcrop is instantiated.
                var jcrop_api;

                // In this example, since Jcrop may be attached or detached
                // at the whim of the user, I've wrapped the call into a function
                // initJcrop();

            // This function is bound to the onRelease handler...
            // In certain circumstances (such as if you set minSize
            // and aspectRatio together), you can inadvertently lose
            // the selection. This callback re-enables creating selections
            // in such a case. Although the need to do this is based on a
            // buggy behavior, it's recommended that you in some way trap
            // the onRelease callback if you use allowSelect: false
            function releaseCheck()
            {
                updateCoords();
                jcrop_api.setOptions({ allowSelect: true });
                jcrop_api.setOptions({ allowMove: true });
                $('#can_click').attr('checked',false);
            };

            // The function is pretty simple
            function initJcrop()
            {
                // Invoke Jcrop in typical fashion
                $('#target').Jcrop(
                {
                    onRelease: updateCoords,
                    bgFade:     true,
                    bgOpacity: .4,
                    bgColor: 'black',
                    onSelect: updateCoords,
                    trueSize: [width, height]
                },function()
                {
                    jcrop_api = this;
                    jcrop_api.setOptions({ allowSelect: true });
                    jcrop_api.setOptions({ allowMove: true });
                    jcrop_api.setOptions({ aspectRatio: ratio });
                    jcrop_api.setOptions({ allowResize: true });
                });
            }


                //http://andrew.hedges.name/experiments/aspect_ratio/
                var api;
                var max_width = {!! $width !!};
                var max_height = {!! $height !!};
                var search_width;

                if($(".image-scale").length > 0)
                {
                    $(".image-scale").on('keyup', function(e)
                    {
                        e.preventDefault();

                        $(this).closest('form').find('.alert').remove();

                        var type = $(this).data('type');

                        width = $(".image-scale[data-type='width']").val();
                        height = $(".image-scale[data-type='height']").val();

                        if((isNaN(width) || isNaN(height)) || (width > max_width) || height > max_height)
                        {
                            $(this).closest('form').prepend('<div class="col-md-12 text-center"><div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>The scale ratio is not valid. Please try again.</div></div>');
                        }
                        else
                        {
                            switch(type)
                            {
                                case 'width':
                                    var new_height = Math.ceil(max_height / max_width * width); // 2560 / 256 = 10

                                    $(".image-scale[data-type='height']").val(new_height);

                                    // $("#target").css('width', width + 'px').data('width', width);
                                    // $("#target").css('height', new_height + 'px').data('height', new_height);

                                    width = width;
                                    height = new_height;
                                    break;
                                case 'height':
                                    var new_width = Math.ceil(max_width / max_height * height); // 2560 / 256 = 10

                                    $(".image-scale[data-type='width']").val(new_width);

                                    // $("#target").css('width', new_width + 'px').data('width', new_width);
                                    // $("#target").css('height', height + 'px').data('height', height);

                                    width = new_width;
                                    height = height;
                                    break;
                            }

                            // jcrop_api.destroy();

                            // $("#target").attr('src', data);
                            // $("#target").attr('src', 'data:image/jpeg;base64,' + data);
                            // $(".image-holder img").attr('style','');
                            // $(".image-holder img").css('opacity', '1');
                            // $(".image-holder .fa-refresh").remove();


                            if($(".crop").hasClass('active'))
                            {
                                initJcrop();
                            }
                        }
                    });
                }

                if($(".constrain").length > 0)
                {
                    $(".constrain").click(function(e)
                    {
                        e.preventDefault();

                        $(".form-group.ratio").toggleClass('hide');

                        if(typeof jcrop_api !== 'undefined')
                        {
                            locked = (!$(this).hasClass('active')) ? 1:'';

                            if(locked)
                            {
                                jcrop_api.setOptions({ aspectRatio: ratio });
                            }
                            else
                            {
                                jcrop_api.setOptions({ aspectRatio: 0 });
                            }
                            jcrop_api.focus();
                        }
                    });
                }

                
                if($("#scale-form").length > 0)
                {
                    $("#scale-form").submit(function(e)
                    {
                        e.preventDefault();
                        
                        $(".image-holder .fa-refresh").remove();

                        $(".image-holder img").css('opacity', '0.5');

                        var margin_left = $(".image-holder img").width();
                        $(".image-holder").prepend('<i class="fa fa-refresh fa-3x fa-spin absolute" style="left:-50%;top:50%;z-index:100;color:white;margin-left:'+margin_left+'px;"></i>');

                        var data = {}
                        data.update = 'scale';
                        data.return_image = true;
                        data.width = $(".image-scale[data-type='width']").val();
                        data.height = $(".image-scale[data-type='height']").val();
                        data._token = "{!! csrf_token() !!}";

                        if(data.width != search_width)
                        {
                            search_width = data.width;

                            $.post('/hummingbird/media/postEditImage/{!! $image->id !!}', data, function(response)
                            {
                                var had_jcrop = false;

                                if(typeof jcrop_api !== 'undefined') 
                                {
                                    had_jcrop = true;
                                    jcrop_api.destroy();
                                }

                                // $("#target").attr('src', data);
                                $("#target").attr('src', 'data:image/jpeg;base64,' + response);
                                $(".image-holder img").attr('style','');
                                // $(".image-holder img").css('opacity', '1');
                                $(".image-holder .fa-refresh").remove();

                                width = data.width;
                                height = data.height;

                                if($(".crop").hasClass('active'))
                                {
                                    initJcrop();
                                }
                            });
                        }
                        else
                        {
                            $(".image-holder img").css('opacity', '1');
                            $(".image-holder .fa-refresh").remove();   
                        }
                    });
                }

                if($(".rotate").length > 0)
                {
                    $(".rotate").click(function(e)
                    {
                        e.preventDefault();
                        
                        $(".image-holder .fa-refresh").remove();

                        $(".image-holder img").css('opacity', '0.5');

                        var margin_left = $(".image-holder img").width();
                        $(".image-holder").prepend('<i class="fa fa-refresh fa-3x fa-spin absolute" style="left:-50%;top:50%;z-index:100;color:white;margin-left:'+margin_left+'px;"></i>');

                        if($(this).hasClass('rotate-left'))
                        {
                            rotated--;
                        }
                        else
                        {
                            rotated++;
                        }

                        var data = {}
                        data.update = 'rotate';
                        data.rotate = rotated;
                        data.return_image = true;
                        data._token = "{!! csrf_token() !!}";

                        $.post('/hummingbird/media/postEditImage/{!! $image->id !!}', data, function(response)
                        {
                            var had_jcrop = false;

                            if(typeof jcrop_api !== 'undefined') 
                            {
                                had_jcrop = true;
                                jcrop_api.destroy();
                            }

                            // $("#target").attr('src', data);
                            $("#target").attr('src', 'data:image/jpeg;base64,' + response);
                            $(".image-holder img").attr('style','');
                            // $(".image-holder img").css('opacity', '1');
                            $(".image-holder .fa-refresh").remove();

                            width = data.width;
                            height = data.height;

                            if($(".crop").hasClass('active'))
                            {
                                initJcrop();
                            }
                        });
                    });
                }

                if($(".crop").length > 0)
                {
                    $(".crop").click(function(e)
                    {
                        e.preventDefault();

                        if(!$(this).hasClass('active'))
                        {
                            initJcrop();
                            return true;
                        }
                        else
                        {
                            if(typeof jcrop_api !== 'undefined')
                            {
                                jcrop_api.destroy();
                                jcrop_api = undefined;
                            }
                        }
                    });
                }

                if($(".create-image").length > 0)
                {
                    $(".create-image").click(function(e)
                    {
                        e.preventDefault();

                        $("#width").val($(".image-scale[data-type='width']").val());
                        $("#height").val($(".image-scale[data-type='height']").val());
                            
                        var type = (typeof jcrop_api !== 'undefined') ? 'crop':'scale';
                        $("#type").val(type);
                        
                        $(this).closest('form').trigger('submit');
                    });
                }

                if($("#ratio").length > 0)
                {
                    $("#ratio").change(function()
                    {
                        ratio = $(this).val();
                        ratio = (ratio == '') ? 16/9:ratio;

                        if($(".constrain").hasClass("active"))
                        {
                            jcrop_api.setOptions({ aspectRatio: ratio });
                            jcrop_api.focus();
                        }
                    });
                }
            });

        </script>

    @stop
@endif