@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop


@section('content')

    @if (Session::has('errors'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    @foreach ($errors->all('<li>:message</li>') as $error)
                        {!! $error !!}    
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        @if($PluginManager->hasPlugins())
            @foreach($PluginManager->get_plugins() as $vendor => $vendor_plugins)
                <div class="col-md-12">
                    <section class="panel">
                        <h4>{!! $vendor !!}</h4>
                        
                        <div class="table">
                            <table class="table table-striped">
                                <thead>
                                    <th scope="row" class="col-md-2">Name</th>
                                    <th scope="row" class="col-md-2">Version</th>
                                    <th scope="row" class="col-md-4">Description</th>
                                    <th scope="row" class="col-md-4">Actions</th>
                                </thead>

                                <tbody>
                                    @foreach($vendor_plugins as $plugin)
                                        <tr>
                                            <td>@if(isset($plugin['name'])) {!! $plugin['name'] !!} @else {!! $plugin['namespace'] !!} @endif @if($PluginManager->plugin_is_active($plugin['namespace'])) <span class="badge badge--hummingbird">Active</span> @endif</td>
                                            <td>{!! $PluginManager->getPluginVersion($plugin) !!}</td>
                                            <td>@if(isset($plugin['description'])) {!! $plugin['description'] !!} @else - @endif</td>
                                            <td>
                                                {!! Form::open(array('route' => 'hummingbird.plugins.process', 'method' => 'post')) !!}
                                                    {!! Form::hidden('package', $plugin['namespace']) !!}
                                                    @if(!$PluginManager->plugin_is_active($plugin['namespace']))
                                                        <button type="submit" name="action" value="activate" class="btn">Activate</button>
                                                    @else
                                                        <button type="submit" name="action" value="deactivate" class="btn">Deactivate</button>
                                                        <button type="submit" name="action" value="delete" class="btn">Delete</button>
                                                        <button type="submit" name="action" value="refresh" class="btn">Plugin refresh</button>
                                                    @endif
                                                    
                                                    <button type="submit" name="action" value="refresh" class="btn">Plugin refresh</button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>                       
                    </section>
                </div>
            @endforeach
        @else
            <div class="col-md-12">
                <section class="panel">
                    <div class="alert alert-box alert-info text-center">No plugins available</div>
                </section>
            </div>
        @endif
    </div>
@stop
