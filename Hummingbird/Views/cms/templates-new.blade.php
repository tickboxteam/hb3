@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {!! Session::get('success') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-info alert-danger fade in">
                {!! Session::get('message') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {!! Session::get('error') !!}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel">            
            @if(Auth::user()->userAbility('template.create'))
                <button id="add-modal-btn" type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-template"><i class="fa fa-plus-circle"></i> New template</button>
            @endif

            <div class="clearfix">&nbsp;</div> 
            @if(count($templates) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($templates as $template)
                                <tr>
                                    <td>{!! $template->name !!} @if($template->locked) <span class="text-danger bold">(locked)</span> @endif</td>
                                    <td>
                                        @if(Auth::user()->userAbility('template.update'))<a href="{!! route('hummingbird.templates.edit', $template->id) !!}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>@endif
                                        
                                        @if(Auth::user()->userAbility('template.create'))
                                            <a href="{!! route('hummingbird.templates.clone', array($template->id)) !!}" class="btn btn-xs btn-preview" data-toggle="tooltip" data-placement="top" title="Clone this template"><i class="fa fa-exchange"></i></a>
                                        @endif

                                        @if( Auth::user()->userAbility('template.lock') )
                                            @if( $template->locked == 0 )
                                                <a href="{!! route('hummingbird.templates.edit', $template->id) . "?lock=1" !!}" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Lock this template"><i class="fa fa-unlock"></i></a>
                                            @endif

                                            @if( $template->locked == 1 && Auth::user()->isSuperUser() )                      
                                                <a href="{!! route('hummingbird.templates.edit', $template->id) . "?lock=0" !!}" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Unlock this template"><i class="fa fa-lock"></i></a>
                                             @endif
                                        @endif

                                        @if( Auth::user()->isSuperUser() || (!$template->locked && Auth::user()->userAbility('template.delete')) )
                                            {!! Form::open(array('route' => array('hummingbird.templates.destroy', $template->id), 'method' => 'delete', 'class' => 'inline-block', 'id' => 'delete_template_' . $template->id)) !!}
                                                <button id="delete_template_{!! $template->id !!}" type="submit" class="btn btn-xs btn-danger btn-confirm-delete" data-target="#delete_template_{!! $template->id !!}" data-title="{!! $template->name !!}" data-id="{!! $template->id !!}" title="Delete &quot;{!! $template->name !!}&quot;" data-delete-body="You are about to delete this website template (&quot;{!! $template->name !!}&quot;)."><i class="fa fa-trash"></i></button>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            @else
                <div class="alert alert-info alert-info fade in text-center">No templates created.</div>
            @endif
        </div>
    </section>
</div>


@if(Auth::user()->userAbility('template.create'))
    <div class="modal fade" id="add-template">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new template</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12"> 
                            {!! Form::open(array('route' => array('hummingbird.templates.store'), 'class' => 'form-horizontal')) !!}
                                <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                    {!! Form::label('name', 'Name: ', array('class' => 'col-sm-3 required')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('name', '', array('class' => 'form-control')) !!}

                                        @if( $errors->has('name') )
                                            <p class="text-danger">{!! $errors->first('name') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" class="btn btn-default btn-primary">Add Template</button>
                                    </div>
                                </div> 
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endif


@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            $('[data-toggle="tooltip"]').tooltip();
            
            @if(count($errors) > 0)
                $("#add-modal-btn").trigger('click');
            @endif
        });
    </script>
@stop