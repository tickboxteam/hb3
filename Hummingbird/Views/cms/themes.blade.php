@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')
    <?php $even = false; ?>

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <h1 class="normal">All themes</h1>

                <div class="table">
                    <table class='results' cellpadding='5' cellspacing='0'>
                        <thead>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Version</th>
                            <th>Screenshot</th>
                            <th>Actions</th>
                        </thead>
                        <tbody>
                            @foreach($themes as $theme)
                                <?php $theme_desc = Hummingbird\Libraries\Themes::themeDescription($theme); ?>
                            
                            <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
                                <td>{!! $theme !!}</td>
                                <td>@if($theme_desc !== FALSE){!! $theme_desc->description !!}@endif</td>
                                <td>@if($theme_desc !== FALSE){!! $theme_desc->version !!}@endif</td>
                                <td><img src="{{General::placeHoldItImage(50, 50)}}" alt="{!! $theme !!} screenshot" /></td>
                                <td><a href="{!!  route('hummingbird.themes.activate', ['theme' => $theme]) !!}">{!! $theme!=$activeTheme? 'Activate': '' !!}</a></td>
                            </tr>
                            
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

@stop