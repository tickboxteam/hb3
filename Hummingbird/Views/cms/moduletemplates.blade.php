@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                @include('HummingbirdBase::cms.moduletemplates-filter-template')
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                @if(Auth::user()->userAbility('modules.templates.create'))
                    <button id="add-template-btn" type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-template"><i class="fa fa-plus-circle"></i> Add template</button>
                @endif

                @if(count($moduletemplates) > 0)
                    <div class="table">
                        <table class="table table-striped">
                            <thead>
                                <th scope="row">Name</th>
                                <th scope="row">Description</th>
                                <th>Actions</th>
                            </thead>

                            <tbody>
                                @foreach($moduletemplates as $moduletemplate)
                                    <tr>
                                        <td>{!! $moduletemplate->name !!} @if(!$moduletemplate->editable)<span class="text-danger bold">(locked)</span>@endif</td>
                                        <td>{!! $moduletemplate->getNotes() !!}</td>
                                        <td>
                                            @if($moduletemplate->editable == 1 && Auth::user()->userAbility('modules.templates.update'))<a href="{!! route('hummingbird.module-templates.edit', $moduletemplate->id) !!}" class="btn btn-xs btn-info" title="Edit {!! $moduletemplate->name !!}"><i class="fa fa-edit"></i></a>
                                            @endif

                                            @if( Auth::user()->userAbility('modules.templates.lock') )
                                                @if( $moduletemplate->editable == 0 )                                            
                                                    <a href="{!! route('hummingbird.module-templates.lock', $moduletemplate->id) . "?lock=1" !!}" class="btn btn-xs btn-warning" title="Unlock {!! $moduletemplate->name !!}"><i class="fa fa-unlock"></i></a> 
                                                @endif

                                                @if( $moduletemplate->editable == 1 && Auth::user()->isSuperUser() )
                                                    <a href="{!! route('hummingbird.module-templates.lock', $moduletemplate->id) . "?lock=0" !!}" class="btn btn-xs btn-warning" title="Lock {!! $moduletemplate->name !!}"><i class="fa fa-lock"></i></a>    
                                                 @endif

                                            @endif

                                            @if( ( !$moduletemplate->editable && Auth::user()->isSuperUser() ) || ( $moduletemplate->editable && Auth::user()->userAbility('modules.templates.delete') ) )
                                                {!! Form::open(array('route' => array('hummingbird.module-templates.destroy', $moduletemplate->id), 'id' => 'modtemplatesdelete', 'method' => 'delete', 'class' => 'inline-block')) !!}
                                                    <button id="mod_temp_{!! $moduletemplate->id !!}" value="{!! $moduletemplate->id !!}" type="submit" class="btn btn-xs btn-danger btn-confirm-delete" data-delete-body="You are about to delete this template (and all associated modules)." data-target="#mod_temp_{!! $moduletemplate->id !!}"><i class="fa fa-trash"></i></a>
                                                {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="text-center"> 
                        {!! $moduletemplates->appends(Request::except('page'))->render() !!}
                    </div>
                @else
                    <div class="alert alert-info text-center">
                        No templates have been added.
                    </div>
                @endif
            </section>
        </div>
    </div>

    @if(Auth::user()->userAbility('modules-templates.create'))
        <div class="modal fade" id="add-template">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title">Add new template</h3>
                    </div>

                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12"> 
                                {!! Form::open(array('route' => 'hummingbird.module-templates.store', 'class' => 'form-horizontal')) !!}
                                    <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                        {!! Form::label('name', 'Name:', array('class' => 'col-sm-3 required')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('name', Request::old('name'), array('class' => 'form-control')) !!}

                                            @if( $errors->has('name') )
                                                <p class="text-danger">{!! $errors->first('name') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-10">
                                            <button type="submit" class="btn btn-default btn-primary">Add template</button>
                                        </div>
                                    </div> 
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.filter-results').click(function(e) {
                $('.filter-form').submit();
            });
        
            @if(count($errors) > 0)
                $("#add-template-btn").trigger('click');     
            @endif
        });
    </script>
@stop