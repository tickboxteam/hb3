@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {!! Session::get('success') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-info fade in">
                {!! Session::get('message') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {!! Session::get('error') !!}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="row">
                {!! Form::open(array('route' => array('hummingbird.pages.index'), 'method' => 'get', 'class' => 'form-inline export-form')) !!}
                    <div class="col-md-3">
                        <h6>Search:</h6>
                        {!! Form::text('s', Request::get('s'), array('class' => 'form-control')) !!}
                    </div>

                    <div class="col-md-3">
                        <h6>Filter by:</h6>
                        @include('HummingbirdBase::cms.page-filter-template')
                    </div>

                    <div class="col-md-3">
                        <h6>By status:</h6>
                        {!! Form::select('status', [NULL => '--- All ---', 'draft' => 'Draft', 'public' => 'Public'], Request::get('status'), array('class' => 'form-control')) !!}
                    </div>

                    <div class="col-md-3">
                        <h6>&nbsp;</h6>
                        <button type="submit" class="btn btn-default filter-results" data-url="{!! route('hummingbird.pages.index') !!}">Filter results</button>
                        
                        <a href="{!! url('hummingbird/pages') !!}" class="btn">Reset search</a>

                        @if( Auth::user()->isSuperUser() )
                            <a href="#" class="btn btn-default filter-results" data-url="{!! route('hummingbird.pages.export') !!}">Export CSV</a>
                        @endif
                    </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="clearfix">
                @if(Request::segment(3) == 'trash')
                    @if($pages->count() > 0)
                        <a href="{!! route('hummingbird.pages.empty-trash') !!}" class="pull-right btn btn-primary" data-confirm="Are you sure you want to delete all entries? This action is irreversible." style="margin-left:5px;"><i class="fa fa-trash-o"></i> Empty trash</a>
                    @endif
                @else
                    @if(Auth::user()->userAbility('page.create'))
                        <button id="add-page-btn" type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-page"><i class="fa fa-plus-circle"></i> Add page</button>
                    @endif
                    
                    <a href="/hummingbird/pages/trash/" class="pull-right btn btn-default" style="margin-left:5px;margin-right:5px;"><i class="fa fa-trash"></i> View trash</a>
                    <a href="{!! route('hummingbird.pages.re-order') !!}" class="pull-right btn btn-default" style="margin-left:5px;"><i class="fa fa-sort"></i> Reorder pages</a>
                @endif
            </div>

            @if(Request::get('s') AND Request::get('s') != '')
                <div class="clearfix">
                    <h3 class="normal">Searching for: {!! Request::get('s') !!}</h3>
                </div>
            @endif

            @if($pages->count() > 0)
                <div class="table">
                    <table class="table table-striped">
                        <thead>
                            <th scope="row">Title</th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </thead>
                        <tbody>
                            @foreach($pages as $page)
                            
                                <tr>
                                    <?php 
                                        $link = ($page->permalink[0] != '/') ? '/'.$page->permalink:$page->permalink;
                                        $link = str_replace("//", "/", $link);

                                        $version_link = ($page->latest_version() !== NULL) ? "?version=".$page->latest_version()->id : NULL;
                                    ?>
                                    
                                    <td>
                                        @if(Auth::user()->userAbility('page.update') && Request::segment(3) != 'trash')
                                            <a href="{!! route('hummingbird.pages.show', $page->id) !!}{!! $version_link !!}">{!! $page->title !!}@if( $page->locked ) <span class="text-danger bold">(locked)</span> @endif</a>
                                        @else
                                            {!! $page->title !!}@if( $page->locked ) <span class="text-danger bold">(locked)</span> @endif
                                        @endif
                                    </td>
                                    
                                    <td>
                                        @if(null !== $page->parentpage)
                                            {!! $page->permalink !!}
                                        @else
                                            -
                                        @endif
                                    </td>

                                    <td>
                                        @if(Auth::user()->userAbility('page.update'))
                                            @if($page->isLive())
                                                @if(Auth::user()->userAbility('page.update'))
                                                    <a href="{!! route('hummingbird.pages.publish', ["page:{$page->id}", 0]) . "?" . http_build_query(Request::only(['s', 'page'])) !!}" class="toggle-publish"><i class="fa fa-toggle-on"></i></a>
                                                @endif
                                            @else
                                                @if(Auth::user()->userAbility('page.update'))
                                                    <a href="{!! route('hummingbird.pages.publish', ["page:{$page->id}", 1]) . "?" . http_build_query(Request::only(['s', 'page'])) !!}" class="toggle-publish"><i class="fa fa-toggle-off"></i></a>
                                                @endif
                                            @endif
                                        @else
                                            {!! $page->status !!}
                                        @endif
                                    </td>
                                    <td>
                                        
                                        @if(Request::segment(3) == 'trash')
                                            <a href="{!! route('hummingbird.pages.restore', $page->id) !!}" class="btn btn-xs btn-info">Restore</a>
                                        @else

                                            @if(Auth::user()->userAbility('page.update'))
                                                <a href="{!! route('hummingbird.pages.show', $page->id) !!}{!! $version_link !!}" class="btn btn-xs btn-info" title="Replicate &quot;{!! $page->title !!}&quot;"><i class="fa fa-edit"></i></a>
                                            @endif

                                            <?php $PreviewLink = $link;
                                            $PreviewLink .= ( $page->status != 'public' ) ? '?version=latest' : '';?>

                                            <a target="_blank" href="{!! $PreviewLink !!}" class="btn btn-xs" style="background-color:black;color:white;">
                                                <i class="fa fa-globe"></i>
                                            </a>

                                            @if( Auth::user()->userAbility('page.create') )
                                                <a href="{!! route('hummingbird.pages.replicate', $page->id) !!}" class="btn btn-xs btn-default" title="Replicate &quot;{!! $page->title !!}&quot;"><i class="fa fa-copy"></i></a>
                                            @endif

                                            @if( ( !$page->locked && Auth::user()->userAbility('page.delete') ) || ( $page->locked && Auth::user()->isSuperUser() ) )
                                                {!! Form::open(array('url' => route('hummingbird.pages.destroy', $page->id) . "?" . http_build_query(Request::only(['s', 'page'])), 'method' => 'delete', 'class' => 'inline-block', 'id' => 'delete_page_' . $page->id)) !!}
                                                    <button id="delete_page_{!! $page->id !!}" type="submit" class="btn btn-xs btn-danger btn-confirm-delete" data-target="#delete_page_{!! $page->id !!}" data-title="{!! $page->title !!}" data-id="{!! $page->id !!}" title="Delete &quot;{!! $page->title !!}&quot;" data-delete-body="You are about to delete this page (&quot;{!! $page->title !!}&quot;)."><i class="fa fa-trash"></i></button>
                                                {!! Form::close() !!}
                                            @endif
                                        @endif                                    
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="text-center"> 
                    {!! $pages->appends(Request::except('page'))->render() !!}
                </div>
            @else
                <div class="alert alert-info">No pages found</div>
            @endif
        </section>
    </div>
</div>

@if(Auth::user()->userAbility('page.create'))
    <div class="modal fade" id="add-page">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new page</h4>
                </div>

                <div class="modal-body">
                    
                    <div class="row">
                        <div class="col-md-12"> 
                            {!! Form::open(array('route' => array('hummingbird.pages.store'), 'class' => 'form-horizontal')) !!}
                                <div class="form-group @if( $errors->has('title') ) has-error @endif">
                                    {!! Form::label('title', 'Title: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('title', Request::old('title'), array('class' => 'form-control')) !!}
                                        @if ($errors->has('title'))
                                            <p class="text-danger">{!! $errors->first('title') !!}</p>
                                        @endif
                                    </div>

                                </div>

                                @if(count($page_list) > 0)
                                    <div class="form-group">
                                        {!! Form::label('parentpage_id', 'Parent page: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::select('parentpage_id', $page_list, array('id' => 'parentpage_id', 'class' => 'selectpicker form-control')) !!}
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" name="parentpage_id" value="1" />
                                @endif

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" class="btn btn-default btn-primary">Add Page</button>
                                    </div>
                                </div> 
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endif

@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>

    <script>
        $(document).ready(function()
        {
            $("#parentpage_id").data('live-search', 'true');
            $('#parentpage_id').selectpicker('render');
            
            $(".filter").data('live-search', 'true');
            $('.filter').selectpicker('render');

            $('#filter').change(function(e) {
                e.preventDefault();
                $('.filter-form').submit();
            });

            @if(count($errors) > 0)
                $("#add-page-btn").trigger('click');
            @endif
    
            $('.filter-results').click(function(e) {
                e.preventDefault();

                var url = $(this).data('url');
                $(this).closest('form').attr('action', url).submit();
            });
        });
    </script>
@stop
