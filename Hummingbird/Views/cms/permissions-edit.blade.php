@extends('HummingbirdBase::cms.layout')

@section('content')

<h1><a href="{{url(App::make('backend_url').'/permissions')}}">All Permissions</a> > Edit permission: {{$permission->name}}</h1>

<?php echo Form::open(array('url' => '/cms/permissions/edit/' . $permission->id)) ?>

<table cellpadding="5" cellspacing="0" id="edit">
    <tbody>
        <tr>
            <td>{{ Form::label('display_name', 'Name: ') }}</td>            
            <td>{{ Form::text('display_name', $permission->display_name) }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('description', 'Description: ') }}</td>            
            <td>{{ Form::text('description', $permission->description) }}</td>
        </tr>
    </tbody>
</table>


<p><input type="submit" value="Update Permission"></p>
<?php echo Form::close() ?>

@stop
