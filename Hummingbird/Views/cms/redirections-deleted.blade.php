@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')
    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                @if(count($redirects) > 0 && Auth::user()->userAbility('redirections.delete'))
                    <div class="clearfix">
                        {!! Form::open(array('route' => array(App::make('backend_url').'.redirections.purge-all'), 'method' => 'delete', 'id' => 'purge-all-redirects')) !!}
                            <button type="submit" class="btn btn-danger pull-right btn-confirm-delete" style="margin-left:10px;" data-target="#purge-all-redirects" data-delete-body="You are about to purge all deleted redirects."><i class="fa fa-trash"></i> Purge all</button>
                        {!! Form::close() !!}

                        {!! Form::open(array('route' => array(App::make('backend_url').'.redirections.reinstate-all'), 'method' => 'POST', 'id' => 'purge-all-redirects')) !!}
                            <button type="submit" class="pull-right btn btn-info" style="margin-left:10px;"><i class="fa fa-arrow-circle-left"></i> Reinstate all</button>
                        {!! Form::close() !!}
                    </div>
                @endif

                <div class="row clearfix" style="margin-top:10px;">
                    <div class="col-md-12"> 
                        @if(count($redirects) > 0)
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Type</th>
                                            <th>Category</th>
                                            <th>Last used</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($redirects as $redirect)
                                            <tr>
                                                <td>{!! $redirect->id !!}</td>
                                                <td>{!! $redirect->from !!}</td>
                                                <td style="word-break:break-word;">{!! $redirect->to !!}</td>
                                                <td>{!! $redirect->type !!}</td>
                                                <td>
                                                    @if(!empty($redirect->category))
                                                        <span class="badge badge--hummingbird">{!! $redirect->category !!}</span>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                @if(null === $redirect->last_used)
                                                    <td>n/a</td>
                                                @else
                                                    <td class="text-nowrap">{!! $redirect->last_used->format('d-m-Y H:i:s') !!}</td>
                                                @endif

                                                <td class="text-nowrap">
                                                    {!! Form::open(array('route' => array('hummingbird.redirections.reinstate', $redirect->id), 'method' => 'post', 'class' => 'inline-block')) !!}

                                                        <button type="submit" class="btn btn-xs btn-info" title="Reinstate redirect: {!! $redirect->from !!}"><i class="fa fa-arrow-circle-left"></i></button> 
                                                    {!! Form::close() !!}

                                                    {!! Form::open(array('route' => array('hummingbird.redirections.purge', $redirect->id), 'id' => 'redirectpurge-' . $redirect->id, 'method' => 'delete', 'class' => 'inline-block')) !!}
                                                        <button type="submit" class="btn btn-xs btn-danger btn-confirm-delete" title="Permanently delete this redirect?" data-delete-body="Permanently delete this redirect?" data-target="#redirectpurge-{!! $redirect->id !!}"><i class="fa fa-trash"></i></button>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="text-center"> 
                                {!! $redirects->appends(Request::except('page'))->render() !!}
                            </div>
                        @else
                            <div class="alert alert-info text-center">No redirects have been deleted</div>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>
@stop