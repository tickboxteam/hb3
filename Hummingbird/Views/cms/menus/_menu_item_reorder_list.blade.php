<li class="dd-item" data-id="{!! $menuitem->id !!}">
    <div class="dd-handle">{!! str_repeat("&nbsp;&nbsp;&nbsp;", $Counter) !!}{!! $menuitem->menu_item_name !!}</div>
</li>

@if( $menuitem->children->count() > 0 )
    <?php $Counter++?>
    @foreach( $menuitem->children()->orderBy('order')->get() as $ChildMenuItem )
        @include('HummingbirdBase::cms.menus._menu_item_reorder_list', ['menuitem' => $ChildMenuItem, 'Counter' => $Counter])
    @endforeach
@endif