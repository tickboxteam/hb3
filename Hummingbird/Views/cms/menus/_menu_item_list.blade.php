<tr>
    <td data-menuitem-id="{!! $menuitem->id !!}" data-menu-type="menu_item_name">{!! str_repeat("&nbsp;&nbsp;&nbsp;", $Counter) !!}{!! $menuitem->menu_item_name !!}</td>
    <td>
        @if( strpos($menuitem->url, 'javascript:') === FALSE )
            <a data-menuitem-id="{!! $menuitem->id !!}" data-menu-type="menu_item_url" href="{!! $menuitem->url !!}" target="_blank">{!! $menuitem->url !!}</a>
        @else
            {!! $menuitem->url !!}
        @endif
    </td>
    <td>
        <a href="/hummingbird/menus/edit/{!! $menuitem->id !!}" class="btn btn-xs btn-info edit-menu editing" data-menuitem-id="{!! $menuitem->id !!}"><i class="fa fa-edit"></i></a>
        <a href="/hummingbird/menus/edit/{!! $menuitem->id !!}" class="btn btn-xs btn-warning editing save-menu hide" data-action="edit"><i class="fa fa-save"></i></a>
        <a href="{!! route('hummingbird.menus.deleteItem', $menuitem->id) !!}" class="btn btn-xs btn-danger btn-confirm-delete" data-title="{!! $menuitem->menu_item_name  !!}"><i class="fa fa-trash"></i></a>
    </td>

    <input type="hidden" name="menu_items[{!! $menuitem->id !!}][parent]" value="{!! $menuitem->parent !!}" />
    <input type="hidden" name="menu_items[{!! $menuitem->id !!}][menu_item_name]" value="{!! $menuitem->menu_item_name !!}" />
    <input type="hidden" name="menu_items[{!! $menuitem->id !!}][url]" value="{!! $menuitem->url !!}" />
    <input type="hidden" name="menu_items[{!! $menuitem->id !!}][title]" value="{!! $menuitem->title !!}" />
    <input type="hidden" name="menu_items[{!! $menuitem->id !!}][classes]" value="{!! $menuitem->classes !!}" />
    <input type="hidden" name="menu_items[{!! $menuitem->id !!}][menu_item_id]" value="{!! $menuitem->menu_item_id !!}" />
    <input type="hidden" name="menu_items[{!! $menuitem->id !!}][new_window]" value="{!! $menuitem->new_window == 1 ? 1:0 !!}" />
</tr>

@if( $menuitem->children->count() > 0 )
    <?php $Counter++?>
    @foreach( $menuitem->children()->orderBy('order')->get() as $ChildMenuItem )
        @include('HummingbirdBase::cms.menus._menu_item_list', ['menuitem' => $ChildMenuItem, 'Counter' => $Counter])
    @endforeach
@endif
