<?php
    $Prepend = ( $Counter > 0 ) ? '-':'';
?>

<option value="{!! $Page->id !!}">{!! str_repeat("&nbsp;", $Counter * 2) !!}{!! $Prepend . " " . $Page->title !!}</option>

@if( $Page->active_children()->count() )
    <?php $Counter++?>
    @foreach($Page->active_children()->get() as $ChildPage)
        @include('HummingbirdBase::cms.menus._menu_item_page_option_dd', ['Page' => $ChildPage, 'Counter' => $Counter])
    @endforeach
@endif