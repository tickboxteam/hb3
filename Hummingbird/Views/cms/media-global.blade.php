<?php 
    $FileHelper = new Hummingbird\Libraries\FileHelper;
    $valid_files = $FileHelper->getValidKeys();
?>

<div class="modal fade" id="media-lib">
    <div class="modal-dialog clearfix modal-x-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close redactor-modal-btn redactor-modal-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Media Library</h4>
            </div>

            <div class="modal-body clearfix">
            	<div class="col-sm-12" id="library">

                    @if (count($breadcrumbs) > 0)
                        <div class="row">
                            <div class="col-md-12">
                                <!--breadcrumbs start -->
                                <ul class="breadcrumb">
                                    <li><a class="view-collection" href="{{url('hummingbird/media')}}">Media centre</a></li>
                                    <?php $i = 0;?>
                                    @foreach ($breadcrumbs as $breadcrumb)
                                        @if($i > 0)
                                            <li>
                                                @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                                                    <a class="view-collection" @if(isset($breadcrumb['id'])) data-id="{{ $breadcrumb['id'] }}" @endif href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                                                @else
                                                    @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                                                @endif
                                            </li>
                                        @endif
                                        <?php $i++?>
                                    @endforeach
                                </ul>
                                <!--breadcrumbs end -->
                            </div>
                        </div>
                    @endif

                    @if(isset($collection))
                        <div class="search-box">    
                            <input name="s" @if( Request::filled('s') ) value="{{ Request::get('s') }}" @endif type="text" placeholder="&#xF002; Search..." class="form-control" style="margin-bottom: 10px;" />   
                            <button type="submit"><i class="fa fa-search"></i><span class="sr-only">Search</span></button>  
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="normal">{{$collection->name}}</h3>
                            </div>
                        </div>
                    @endif

                    @if(count($media_collections) > 0)
                        <div class="row media-collection-list">
                    		@foreach($media_collections as $collection)
        					    <div class="col-xs-3 col-sm-4 col-md-3 col-lg-2">
        					    	<div class="collection">
        					            <div class="caption">
                                            <h3>{{ $collection->name }}<a href="{{ route('hummingbird.media.edit', $collection->id) }}" class="btn-xs" role="button" target="_blank"><i class="fa fa-edit"></i></a></h3>

        					                @if($collection->description != '')
        					                	<p>{{$collection->description}}</p>
        					               	@else
        					               		<p>{{$collection->name}}</p>
        					               	@endif

        					                <p>
                                                <a href="{{ route('hummingbird.media.view', $collection->id) }}" data-id="{{$collection->id}}" data-additional="{{$is_additional}}" class="view-collection btn-sm btn-info" role="button">Show</a>
        					                </p>
        					            </div>
        						    </div>
        						</div>
                    		@endforeach
                        </div>
                    @endif
                    
                    <style>
                        .row.media-collection-list {
                            display: flex;
                            flex-wrap: wrap;
                        }
                    </style>

                    @if (count($media_items) > 0)
                        <!-- <div class="row media"> -->
                            <div class="col-sm-12">
                                        <!-- <div class="col-sm-12"> -->
                                <h5>Media</h5>
                                
                                @if( Request::filled('s') )
                                    <h6>Searching for: {{ Request::get('s') }}</h6>
                                @endif

                            <!-- </div> -->
                                <div id="media-lib" class="gallery">
                                    @foreach($media_items as $media)
                                        <?php 
                                            $file_extension = $FileHelper->getFileExtension($media->location);
                                            $isImage = $FileHelper->isImage($file_extension);
                                        ?>

                                        <div class="col-xs-4 col-sm-4 col-md-2 thumb" data-target="images" data-item-id="{{$media->id}}" data-additional="{{$is_additional}}">
                                            <div class="images link-click" data-href="{{ route('hummingbird.media.view', $collection->id) }}">
                                                @if($isImage)
                                                    <img src="{{$media->mediaCentreThumb()}}" alt="{{$media->filename}}" />
                                                @else
                                                    <div class="filename text-center"><i class="fa fa-file-{{str_slug($file_extension)}} fa-2x text-center"></i><br />{{$media->filename}}</div>
                                                @endif
                                            </div>

                                            <div class="options">
                                                <span class="option view btn-default"><a href="{{ route('hummingbird.media.view-media', $media->id) }}"><i class="fa fa-pencil"></i></a></span>
                                                
                                                @if($isImage)
                                                    <span class="option edit btn-info">
                                                        <a href="{{ route('hummingbird.media.edit-image', $media->id) }}"><i class="fa fa-crop"></i></a>
                                                    </span>
                                                @endif
                                                
                                                <span class="option delete btn-danger"><a href="{{ route('hummingbird.media.delete-item', $media->id) }}" class="media-delete" data-id="{{$media->id}}"><i class="fa fa-trash"></i></a></span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            <!-- </div> -->
                        </div>

                        <style>
                            .gallery .thumb {
                                margin-top:15px;
                                margin-bottom:15px;
                            }

                            .gallery .thumb .images .filename {word-wrap:break-word;font-size:1.3rem;font-size:13px;}

                            .gallery .thumb .images img 
                            {
                                position: relative;
                                display:block;
                                max-width:100%;
                                height:auto;
                                width:100%;
                                transition: all 0.5s ease;
                                opacity:1.0;
                                -webkit-border-radius: 3px;
                                -moz-border-radius: 3px;
                                border-radius: 3px;
                            }

                            .gallery .thumb .options
                            {
                                position:absolute;
                                top:-6px;
                                right:6px;
                                display:none;
                            }

                            .gallery .thumb .options .option
                            {
                                font-size:1.2rem;
                                display: inline-block;
                                line-height: 1;
                                margin-left: 2px;
                                color: white;
                                width: 24px;
                                height: 24px;
                                line-height: 24px;
                                -webkit-border-radius: 12px;
                                -webkit-background-clip: padding-box;
                                -moz-border-radius: 12px;
                                -moz-background-clip: padding;
                                border-radius: 12px;
                                background-clip: padding-box;
                                text-align: center;
                                -moz-box-shadow: 0 2px 5px rgba(0,0,0,.2);
                                -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.2);
                                box-shadow: 0 2px 5px rgba(0,0,0,.2);
                            }

                            .gallery .thumb .options .option a {
                                color:white;
                                display:block;
                            }

                            .gallery .thumb .options .option.btn-default a {color:white;}
                            .gallery .thumb .options .option.btn-default {background-color: #2A313A}
                            .gallery .thumb .options .option.btn-default:hover {background-color:black;} 

                            .link-click {display:block;}
                            .link-click:hover {text-decoration: underline;cursor:pointer;}
                        </style>
                    @else
                        <div class="col-sm-12">
                            <div class="alert alert-info text-center">No items found</div>
                        </div>
                    @endif
            	</div>
            </div>
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<style>
    #media-lib .modal-dialog,
    #media-lib .modal-content {
        /* 80% of window height */
        height: 95%;
    }

    #media-lib .modal-body {
        /* 100% = dialog height, 120px = header + footer */
        max-height: calc(100% - 131px);
        overflow-y: auto;
        padding: 0;
    }

    #media-lib.modal {z-index:10001;}
    #media-lib #library {background-color:#F9F9FB;padding-top:15px;padding-bottom:15px;}

    #media-lib .search-box {
        position: relative;
    }

    #media-lib .search-box button {
        position: absolute;
        top: 0;
        right: 0;
        background-color: #2A313A;
        border: 0;
        color: white;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        height: 34px;
        padding: 5px 10px;
    }

    #media-lib .search-box button:hover {
        background-color: #15181c;
    }
    
    .collection {border-radius:5px;background-color:white;margin:10px 0;}
    .collection .caption {padding:10px 15px;}
    .collection h3 {font-weight:normal;font-size:18px;font-size:1.8rem; word-break: break-all;}
    .collection p {margin-top: 10px;color: #80858e; word-break: break-all;}

</style>