<?php
/**
 * Hummingbird: Page filter template, for re-ordering pages
 *
 * @package     HB3
 * @author      Geona Antony, Daryl Phillips
 * @copyright   2021
 * @since       1.1.23
 * @version     2
*/?>

@if( count($page_list) > 0)
    <select name="filter" id="filter" class="filter selectpicker form-control">
        <option value=""> --- Filter by parent page ---</option>
        @foreach($page_list as $key => $title)
            <option value="{!! $key !!}" @if($key == Request::get('filter')) selected="selected" @endif>{!! $title !!}</option>
        @endforeach
    </select>
@endif