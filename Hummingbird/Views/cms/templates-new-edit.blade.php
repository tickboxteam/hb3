@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/templates/builder.css" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
@stop

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    {!! Form::open(array('route' => array('hummingbird.templates.update', $template->id), 'method' => 'PUT', 'id' => 'scale-form', 'class' => 'form-horizontal')) !!}
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="col-md-12 container_top">
                        <div class="page_header">
                            <h1 class="pull-left">Template builder</h1>   
                            @if(!$template->locked)<a href="#" class="pull-right clean btn btn-danger btn-xs"><i class="fa fa-trash"> Clear HTML</i></a>@endif       
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div> 

                    @if( Auth::user()->isSuperUser() || !$template->locked)
                        <div class="structure_input">
                            <div class="html">Add: 
                                <a href="#" data-size="container">Container</a> |
                                <a href="#" data-size="row">Row</a> |
                                <a href="#" data-size="single-column">Single column</a> | 
                                <a href="#" data-size="12">Full width</a> | 
                                <a href="#" data-size="6,6">1/2</a> | 
                                <a href="#" data-size="4,4,4">1/4</a> |  
                                <a href="#" data-size="3,3,3,3">1/3</a> | 
                                <a href="#" data-size="2,2,2,2,2,2">1/6</a>
                            </div>

                            <br />

                            <input type="text" name="structure" value="" placeholder="3, 6, 3"/>
                            <input type="button" class="btn btn-info btn-sm"  id="structure_button" value="Insert row"/>
                            <input type="button" class="btn btn-success pull-right" id="save_button" value="Save template" style=""/>
                        </div>
                    @endif
                </section>
            </div>

            <div class="col-md-12" style="margin-bottom:20px;">
                <textarea class="hide" id="generated_html" name="html">{!! $template->html !!}</textarea>

                <div class="flexi_outer">
                    <div class="relative" id="flexicontainer">
                        {!! $template->html !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <section class="panel">
                <h4>Template details</h4>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Template name:</label>
                    <div class="col-sm-10">
                        {!! Form::text('name', $template->name, array('class' => 'form-control', 'placeholder' => 'Template Name' )) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        {!! Form::textarea('description', $template->description, array('class' => 'form-control textareas', 'placeholder' => 'Enter description...', 'rows' => 5 )) !!}
                    </div>
                </div>

                @if( Auth::user()->isSuperUser() || !$template->locked )
                    <div class="form-group">
                        <div class="col-sm-offset-10 col-sm-2">
                        <input type="submit" class="btn btn-success pull-right" id="save_button" value="Update template" style=""/>
                        </div>
                    </div>
                @endif
            </section>
        </div>
    {!! Form::close() !!}

    @include('HummingbirdBase::cms.templates-new-modal')

@stop


@section('scripts')
    @if( !Auth::user()->isSuperUser() && $template->locked )<script>var locked = true;</script>@endif
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="/themes/admin/hummingbird/default/lib/templates/builder.init.js"></script>
    <script src="/themes/admin/hummingbird/default/lib/templates/builder.js"></script>
@stop