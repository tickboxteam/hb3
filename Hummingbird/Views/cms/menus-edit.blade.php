@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')
    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('errors'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <p>There was a problem editing the menu. Please go back try again.</p>
                </div>
            </div>
        </div>
    @endif

    {!! Form::open(array('route' => array('hummingbird.menus.update', $menu->id), 'method' => 'PUT', 'id' => 'scale-form', 'class' => 'form-horizontal')) !!}
        {!! Form::hidden('action', 'update') !!}
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="col-md-8">
                        <div class="form-group @if( $errors->has('name') ) has-error @endif">
                            <label for="name" class="col-sm-2">Name:</label>
                            <div class="col-sm-10">
                                <input class="input_box form-control" id="name" type="text" name="name" value="{!! $menu->name !!}">

                                @if( $errors->has('name') )
                                    <p class="text-danger">{!! $errors->first('name') !!}</p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group @if( $errors->has('description') ) has-error @endif">
                            <label for="description" class="col-sm-2">Notes:</label>
                            <div class="col-sm-10">
                                {!! Form::textarea('description', $menu->description, array('class' => 'form-control textareas', 'rows' => '3')) !!}

                                @if( $errors->has('description') )
                                    <p class="text-danger">{!! $errors->first('description') !!}</p>
                                @endif
                            </div>
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <div class="col-sm-4">
                            
                            <input type="submit" class="btn btn-success pull-right" id="save" value="Save menu"/>
                            @if( $menu->menuitems->count() > 0 )
                                <a href="{!! route('hummingbird.menus.re-order.update', $menu->id) !!}" class="pull-right btn btn-default" style="margin-right:5px;"><i class="fa fa-sort"></i> Reorder menu items</a>
                            @endif

                            <button id="add-menu-item-btn" type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#menu-management"><i class="fa fa-plus-circle"></i> Add menu item</button>

                        </div>
                    </div>

                    <hr />

                    <h4 class="normal">Menu items:</h4>

                    @if( $menu->menuitems->count() > 0 )

                        <div class="table">
                            <table class="table table-striped">
                                <thead>
                                    <th>Navigation label</th>
                                    <th>Permalink</th>
                                    <th>Actions</th>
                                </thead>
                                <tbody>
                                    @foreach($menu->topLevelItems()->get() as $menuitem)
                                        @include('HummingbirdBase::cms.menus._menu_item_list', ['menuitem' => $menuitem, 'Counter' => 0])
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-info">No menu items have been added.</div>
                    @endif
                </section>
            </div>
        </div>
    {!! Form::close() !!}

    <div class="modal fade" id="menu-management-edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit menu item</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">                             
                            {!! Form::open(array('route' => array('hummingbird.menus.update', $menu->id), 'class' => 'form-horizontal', 'id' => 'menu-edit')) !!}
                                {!! Form::hidden('action', 'update-item') !!}

                                <div class="form-group">
                                    {!! Form::label('parent_menu_item', 'Parent menu item: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::select('parent_menu_item', [NULL => '--- No parent ---'] + $menu->topLevelItems()->pluck('menu_item_name', 'id')->all(), NULL, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('menu_item_name', 'Navigation label: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menu_item_name', NULL, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('menuitem_permalink', 'Permalink: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menuitem_permalink', NULL, array('class' => 'form-control')) !!}

                                        <span class="help-block"><strong>Internal</strong>: /test-url/<br />
                                            <strong>External:</strong> http://www.google.com</strong>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group" data-menu-type="custom">
                                    {!! Form::label('menu_item_title', 'Title attribute: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menu_item_title', NULL, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <div class="form-group" data-menu-type="custom">
                                    {!! Form::label('menu_item_new_window', 'New window: ', array('class' => 'col-sm-3')) !!}

                                    <div class="col-sm-9">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('menu_item_new_window', 1, NULL) !!}
                                                <span class="help-block">Opens links in new windows/tabs</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('menuitem_classes', 'Menu item classes: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menuitem_classes', NULL, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('menu_item_id', 'Menu item ID: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menu_item_id', NULL, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" class="btn btn-default btn-primary">Save menu item</button>
                                    </div>
                                </div> 
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="menu-management">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new menu item</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">                             
                            {!! Form::open(array('route' => array('hummingbird.menus.update', $menu->id), 'class' => 'form-horizontal', 'method' => 'PUT')) !!}
                                {!! Form::hidden('action', 'new-menu-items') !!}
                                <div class="form-group @if( $errors->has('menu_item_type') ) has-error @endif">
                                    {!! Form::label('menu_item_type', 'Type of menu item: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9"> 
                                        {!! Form::select('menu_item_type', [
                                            NULL => '--- Please select ---',
                                            'custom' => 'Custom menu item',
                                            'page' => 'Page'
                                        ], NULL, array('class' => 'form-control', 'id' => 'menuitem_type-select')) !!}
                                        
                                        @if( $errors->has('menu_item_type') )
                                            <p class="text-danger">{!! $errors->first('menu_item_type') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if( $errors->has('parent_menu_item') ) has-error @endif">
                                    {!! Form::label('parent_menu_item', 'Parent menu item: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::select('parent_menu_item', [NULL => '--- No parent ---'] + $menu->topLevelItems()->pluck('menu_item_name', 'id')->all(), NULL, array('class' => 'form-control')) !!}

                                        @if( $errors->has('parent_menu_item') )
                                            <p class="text-danger">{!! $errors->first('parent_menu_item') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if( $errors->has('menu_item_name') ) has-error @endif" data-menu-type="custom" @if( Request::old('menu_item_type') != 'custom' ) style="display:none;" @endif>
                                    {!! Form::label('menu_item_name', 'Navigation label: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menu_item_name', NULL, array('class' => 'form-control')) !!}
                                    
                                        @if( $errors->has('menu_item_name') )
                                            <p class="text-danger">{!! $errors->first('menu_item_name') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if( $errors->has('menu_item_permalink') ) has-error @endif" data-menu-type="custom" @if( Request::old('menu_item_type') != 'custom' ) style="display:none;" @endif>
                                    {!! Form::label('menu_item_permalink', 'Permalink: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menu_item_permalink', NULL, array('class' => 'form-control')) !!}

                                        <span class="help-block"><strong>Internal</strong>: /test-url/<br />
                                            <strong>External:</strong> http://www.google.com</strong>
                                        </span>
                                    
                                        @if( $errors->has('menu_item_permalink') )
                                            <p class="text-danger">{!! $errors->first('menu_item_permalink') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if( $errors->has('menu_item_title') ) has-error @endif" data-menu-type="custom" @if( Request::old('menu_item_type') != 'custom' ) style="display:none;" @endif>
                                    {!! Form::label('menu_item_title', 'Title attribute: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menu_item_title', NULL, array('class' => 'form-control')) !!}
                                    
                                        @if( $errors->has('menu_item_title') )
                                            <p class="text-danger">{!! $errors->first('menu_item_title') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if( $errors->has('menu_item_new_window') ) has-error @endif" data-menu-type="custom" @if( Request::old('menu_item_type') != 'custom' ) style="display:none;" @endif>
                                    {!! Form::hidden('menu_item_new_window', '0') !!}

                                    {!! Form::label('menu_item_new_window', 'New window: ', array('class' => 'col-sm-3')) !!}

                                    <div class="col-sm-9">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox('menu_item_new_window', 1, NULL) !!}
                                                <span class="help-block">Opens links in new windows/tabs</span>
                                            </label>
                                        </div>
                                    
                                        @if( $errors->has('menu_item_new_window') )
                                            <p class="text-danger">{!! $errors->first('menu_item_new_window') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if( $errors->has('menu_item_classes') ) has-error @endif" data-menu-type="custom" @if( Request::old('menu_item_type') != 'custom' ) style="display:none;" @endif>
                                    {!! Form::label('menu_item_classes', 'Menu item classes: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menu_item_classes', NULL, array('class' => 'form-control')) !!}
                                    
                                        @if( $errors->has('menu_item_classes') )
                                            <p class="text-danger">{!! $errors->first('menu_item_classes') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if( $errors->has('menu_item_menu_id') ) has-error @endif" data-menu-type="custom" @if( Request::old('menu_item_type') != 'custom' ) style="display:none;" @endif>
                                    {!! Form::label('menu_item_menu_id', 'Menu item ID: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('menu_item_menu_id', NULL, array('class' => 'form-control')) !!}
                                    
                                        @if( $errors->has('menu_item_menu_id') )
                                            <p class="text-danger">{!! $errors->first('menu_item_menu_id') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group @if( $errors->has('menu_item_pages') ) has-error @endif" data-menu-type="page" @if( Request::old('menu_item_type') != 'page' ) style="display:none;" @endif>
                                    {!! Form::label('menu_item_pages', 'Pages: ', array('class' => 'col-sm-3')) !!}
                                    <div class="col-sm-9">
                                        <select id="menu_item_pages" name="menu_item_pages[]" class="selectpicker form-control" data-live-search="true" multiple>
                                            @foreach( Hummingbird\Models\Page::where('parentpage_id', NULL)->status('public')->get() as $ParentPage)
                                                @include('HummingbirdBase::cms.menus._menu_item_page_option_dd', ['Page' => $ParentPage, 'Counter' => 0])
                                            @endforeach
                                        </select>
                                    
                                        @if( $errors->has('menu_item_pages') )
                                            <p class="text-danger">{!! $errors->first('menu_item_pages') !!}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" class="btn btn-default btn-primary">Add menu</button>
                                    </div>
                                </div> 
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop




@section('scripts')
    <script src="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>
    <script>
        $(document).ready(function()
        {
            @if( Session::has('CreateMenuItemError') )
                $(".btn[data-target='#menu-management']").trigger('click');
            @endif

            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }

            $("#menuitem_type-select").change(function() {
                if( $(this).val() == '' ) {
                    $("div[data-menu-type]").hide();
                }

                if( $(this).val() == 'custom' ) {
                    $("div[data-menu-type='custom']").show();
                    $("div[data-menu-type='page']").hide();
                }

                if( $(this).val() == 'page' ) {
                    $("div[data-menu-type='page']").show();
                    $("div[data-menu-type='custom']").hide();
                }
            });

            if($(".edit-menu").length > 0)
            {
                $(".edit-menu").click(function(e)
                {
                    e.preventDefault();

                    $("#menu-management-edit").attr('menuitem-id', $(this).data('menuitem-id'));
                    $("#menu-management-edit select[name='parent_menu_item']").val(
                        ( $("input[name='menu_items[" + $(this).data('menuitem-id') + "][parent]'").val() > 0 ) ? $("input[name='menu_items[" + $(this).data('menuitem-id') + "][parent]'").val() : ''
                    );

                    // TODO: DISABLE SELF IN DROPDOWN

                    $("#menu-management-edit input[name='menu_item_name']").val( $("input[name='menu_items[" + $(this).data('menuitem-id') + "][menu_item_name]'").val() );                    
                    $("#menu-management-edit input[name='menu_item_title']").val( $("input[name='menu_items[" + $(this).data('menuitem-id') + "][title]'").val() );
                    $("#menu-management-edit input[name='menuitem_permalink']").val( $("input[name='menu_items[" + $(this).data('menuitem-id') + "][url]'").val() );
                    $("#menu-management-edit input[name='menuitem_classes']").val( $("input[name='menu_items[" + $(this).data('menuitem-id') + "][classes]'").val() );
                    $("#menu-management-edit input[name='menu_item_id']").val( $("input[name='menu_items[" + $(this).data('menuitem-id') + "][menu_item_id]'").val() );
                    $("#menu-management-edit input[name='menu_item_new_window']").prop('checked', ( $("input[name='menu_items[" + $(this).data('menuitem-id') + "][new_window]'").val() == 1 ) ? true : false );

                    $("#menu-management-edit").modal('show');
                });
            }

            $("#menu-edit").submit(function(e) {
                e.preventDefault();

                var menuitem_parent = ( $("#menu-management-edit select[name='parent_menu_item']").val() > 0 ) ? $("#menu-management-edit select[name='parent_menu_item']").val() : '';

                var menuitem_name = $("#menu-management-edit input[name='menu_item_name']").val();
                var menuitem_title = $("#menu-management-edit input[name='menu_item_title']").val();
                var menuitem_permalink = $("#menu-management-edit input[name='menuitem_permalink']").val();
                var menuitem_classes = $("#menu-management-edit input[name='menuitem_classes']").val();
                var menu_item_id = $("#menu-management-edit input[name='menu_item_id']").val();
                var menu_new_window = $("#menu-management-edit input[name='menu_item_new_window']").is(':checked') ? 1 : 0;

                $("td[data-menuitem-id='" + $("#menu-management-edit").attr('menuitem-id') + "'][data-menu-type='menu_item_name']").text( menuitem_title );
                $("a[data-menuitem-id='" + $("#menu-management-edit").attr('menuitem-id') + "'][data-menu-type='menu_item_url']").attr('href', menuitem_permalink );
                $("a[data-menuitem-id='" + $("#menu-management-edit").attr('menuitem-id') + "'][data-menu-type='menu_item_url']").text( menuitem_permalink );

                $("input[name='menu_items[" + $("#menu-management-edit").attr('menuitem-id') + "][parent]'").val( menuitem_parent );
                $("input[name='menu_items[" + $("#menu-management-edit").attr('menuitem-id') + "][menu_item_name]'").val( menuitem_name );
                $("input[name='menu_items[" + $("#menu-management-edit").attr('menuitem-id') + "][title]'").val( menuitem_title );
                $("input[name='menu_items[" + $("#menu-management-edit").attr('menuitem-id') + "][url]'").val( menuitem_permalink );
                $("input[name='menu_items[" + $("#menu-management-edit").attr('menuitem-id') + "][classes]'").val( menuitem_classes );
                $("input[name='menu_items[" + $("#menu-management-edit").attr('menuitem-id') + "][menu_item_id]'").val( menu_item_id );
                $("input[name='menu_items[" + $("#menu-management-edit").attr('menuitem-id') + "][new_window]'").val( menu_new_window );

                $("#menu-management-edit").modal('hide');
            });

            if($(".save-menu").length > 0)
            {
                $(".save-menu").click(function(e)
                {
                    e.preventDefault();

                    var parent = $(this).closest('tr');

                    var new_name = parent.find('.new-name').val();
                    var old_name = parent.find('.old-name').val();

                    var new_url = parent.find('.new-url').val();
                    var old_url = parent.find('.old-url').val();

                    if(new_name != old_name && new_name != '')
                    {
                        parent.find('span.item-name').text(new_name);
                    }

                    if(new_url != old_url && new_url != '')
                    {
                        parent.find('span.item-url a').attr('href', new_url);
                        parent.find('span.item-url a').text(new_url);
                    }

                    parent.find('span, .editing').toggleClass('hide');
                });
            }
        });
    </script>
@stop