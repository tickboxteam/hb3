<aside id="sidebar">
    <div class="hide sidebar-fixed fixed" style="background-color:grey;width:240px;bottom:0;left:0;height:60px;z-index:100;overflow:hidden;">
        <div class="nav-button col-md-12" style="font-size:26px;color:white !important;"><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="color:white !important;"><i class="fa fa-sign-out"></i></a></div>
    </div>

    <div class="brand">
        <img src="/themes/admin/hummingbird/default/layout/hb-icon-white.png" /> 
        <span>Hummingbird</span><span class="bold">CMS</span>
    </div>

    @if( App::bound('cms.navigation') )
        <div class="nav">
            @if( Auth::user() && empty(Auth::user()->force_password_reset) && ( ( Auth::user()->has2FAEnabled() && is_2fa_forced() ) || !is_2fa_forced() ) )
                @if( BackendMenu::hasBackendNavigationTopLevelItems() )
                    @foreach(BackendMenu::getBackendNavigationTopLevelItems() as $menu__item)
                        @if( count($menu__item['children']) > 0 )
                            <div class="menu">
                                @foreach($menu__item['children'] as $menu__item_child)
                                    <ul>
                                        <li>
                                            <a href="{!! $menu__item_child['url'] !!}" @if( !empty($menu__item_child['active']) ) class="active" @endif>@if(!empty($menu__item_child['icon']))<i class="fa {!! $menu__item_child['icon'] !!}"></i> @endif @if( isset($menu__item_child['icon_image']) ) <img src="{!! $menu__item_child['icon_image'] !!}" /> @endif @if( isset($menu__item_child['icon_svg']) ) {!! $menu__item_child['icon_svg'] !!} @endif<span class="title">{!! $menu__item_child['label'] !!}</span></a>
                                        </li>
                                    </ul>
                                @endforeach
                            </div>
                        @endif
                    @endforeach
                @endif

                @if( BackendMenu::hasBackendNavigationItems() )
                    @foreach(BackendMenu::getBackendNavigationItems() as $menu__item)
                        @if( ( isset($menu__item['url']) && !empty($menu__item['url']) ) || (isset($menu__item['children']) && count($menu__item['children']) ) > 0 )
                            <div class="menu @if( isset($menu__item['classes']) && $menu__item['classes'] != '' ) {!! $menu__item['classes'] !!} @endif @if( count($menu__item['children']) > 0 ) menu-children @endif">
                                @if( count($menu__item['children']) > 0 )
                                    <h3>{!! $menu__item['label'] !!} <a class="menu-collapse white"><i class="fa fa-plus font-size-10"></i></a></h3>
                                @endif
                                
                                @if( count($menu__item['children']) > 0 )
                                    <ul style="display:none;"> 
                                        @foreach($menu__item['children'] as $menu__item_child)

                                            @if( count($menu__item_child['children']) > 0 )
                                                <li class="option">
                                                    <a href="#" class="hasSubMenu">@if(!empty($menu__item_child['icon']))<i class="fa {{$menu__item_child['icon']}}"></i> @endif @if( isset($menu__item_child['icon_image']) ) <img src="{!! $menu__item_child['icon_image'] !!}" /> @endif @if( isset($menu__item_child['icon_svg']) ) {!! $menu__item_child['icon_svg'] !!} @endif<span class="title">{!! $menu__item_child['label'] !!}</span><i class="fa fa-chevron-right"></i></a>
                                                    
                                                    <ul class="submenu" style="display:none;">                                                
                                                        @foreach($menu__item_child['children'] as $child)
                                                            <li>
                                                                <a href="{!! $child['url'] !!}" @if( !empty($child['active']) ) class="active" @endif><i class="fa {!! (isset($child['icon'])) ? $child['icon']: 'fa-angle-right' !!}"></i>{!! $child['label'] !!}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="{!! $menu__item_child['url'] !!}" @if( !empty($menu__item_child['active']) ) class="active" @endif>@if(!empty($menu__item_child['icon']))<i class="fa {!! $menu__item_child['icon'] !!}"></i> @endif @if( isset($menu__item_child['icon_image']) ) <img src="{!! $menu__item_child['icon_image'] !!}" /> @endif @if( isset($menu__item_child['icon_svg']) ) {!! $menu__item_child['icon_svg'] !!} @endif<span class="title">{!! $menu__item_child['label'] !!}</span></a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @else
                                    <ul>
                                        <li>
                                            <a href="{!! $menu__item['url'] !!}" @if( !empty($menu__item['active']) ) class="active" @endif>@if(!empty($menu__item['icon']))<i class="fa {!! $menu__item['icon'] !!}"></i> @endif @if( isset($menu__item['icon_image']) ) <img src="{!! $menu__item['icon_image'] !!}" /> @endif @if( isset($menu__item['icon_svg']) ) {!! $menu__item['icon_svg'] !!} @endif<span class="title">{!! $menu__item['label'] !!}</span></a>
                                        </li>
                                    </ul>
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif
            @endif
        </div>
    @endif
</aside>