<?php
/**
 * Hummingbird: Settings > Debug
 *
 * @package     HB3
 * @author      Daryl Phillips <darylp@tickboxmarketing.co.uk>
 * @copyright   2023, Tickbox Marketing Ltd
*/
?>

<div role="tabpanel" class="tab-pane" id="password_settings">    
    <div class="col-sm-12">
        <h4>Password Strength & Security</h4>
    </div>

    <div class="form-group clearfix @if( $errors->has('pass_length') ) has-error @endif ">
        {!! Form::label('pass_length', 'Length: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            {!! Form::number('pass_length', ( isset($SystemSettings->value['pass_length']) ) ? $SystemSettings->value['pass_length'] : 8, array('class' => 'form-control lighter-form-control', 'min' => 8, 'max' => 16)) !!}
            @if( $errors->has('pass_length') )
                <span class="help-block">{!! $errors->first("pass_length") !!}</span>
            @endif

            <span class="help-block">Minimum password length is 8</span>
        </div>
    </div>

    <div class="form-group clearfix d-flex">
        {!! Form::label('letters', 'Letters: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            <div class="onoffswitch4">
                {!! Form::hidden('letters', 'false') !!}

                {!! Form::checkbox('letters', 'true', ( isset($SystemSettings->value['letters']) && $SystemSettings->value['letters'] == 'true' ), array('class' => 'onoffswitch4-checkbox', 'id' => 'letters_chk')) !!}

                <label class="onoffswitch4-label" for="letters_chk">
                    <span class="onoffswitch4-inner"></span>
                    <span class="onoffswitch4-switch"></span>
                </label>
            </div>

            <span class="help-block">Passwords must contain a letter character</span>
        </div>
    </div>
    
    <div class="form-group clearfix d-flex">
        {!! Form::label('mixed_case', 'Mixed Case: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            <div class="onoffswitch4">
                {!! Form::hidden('mixed_case', 'false') !!}
                {!! Form::checkbox('mixed_case', 'true', ( isset($SystemSettings->value['mixed_case']) && $SystemSettings->value['mixed_case'] == 'true' ), array('class' => 'onoffswitch4-checkbox', 'id' => 'mixedcase_chk')) !!}
                <label class="onoffswitch4-label" for="mixedcase_chk">
                    <span class="onoffswitch4-inner"></span>
                    <span class="onoffswitch4-switch"></span>
                </label>
            </div>

            <span class="help-block">Passwords must contain both uppercase / lowercase characters</span>
        </div>
    </div>

    <div class="form-group clearfix d-flex">
        {!! Form::label('pass_numbers', 'Numbers: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            <div class="onoffswitch4">
                {!! Form::hidden('pass_numbers', 'false') !!}
                {!! Form::checkbox('pass_numbers', 'true', ( isset($SystemSettings->value['pass_numbers']) && $SystemSettings->value['pass_numbers'] == 'true' ), array('class' => 'onoffswitch4-checkbox', 'id' => 'pass_numbers_chk')) !!}
                <label class="onoffswitch4-label" for="pass_numbers_chk">
                    <span class="onoffswitch4-inner"></span>
                    <span class="onoffswitch4-switch"></span>
                </label>
            </div>

            <span class="help-block">Passwords must contain a number</span>
        </div>
    </div>

    <div class="form-group clearfix d-flex">
        {!! Form::label('pass_symbols', 'Symbols: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            <div class="onoffswitch4">
                {!! Form::hidden('pass_symbols', 'false') !!}
                {!! Form::checkbox('pass_symbols', 'true', ( isset($SystemSettings->value['pass_symbols']) && $SystemSettings->value['pass_symbols'] == 'true' ), array('class' => 'onoffswitch4-checkbox', 'id' => 'pass_symbols_chk')) !!}
                <label class="onoffswitch4-label" for="pass_symbols_chk">
                    <span class="onoffswitch4-inner"></span>
                    <span class="onoffswitch4-switch"></span>
                </label>
            </div>

            <span class="help-block">Passwords must contain a symbol</span>
        </div>
    </div>

    <div class="form-group clearfix d-flex">
        {!! Form::label('pass_compromised', 'Compromised: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            <div class="onoffswitch4">
                {!! Form::hidden('pass_compromised', 'false') !!}
                {!! Form::checkbox('pass_compromised', 'true', ( isset($SystemSettings->value['pass_compromised']) && $SystemSettings->value['pass_compromised'] == 'true' ), array('class' => 'onoffswitch4-checkbox', 'id' => 'pass_compromised_chk')) !!}
                <label class="onoffswitch4-label" for="pass_compromised_chk">
                    <span class="onoffswitch4-inner"></span>
                    <span class="onoffswitch4-switch"></span>
                </label>
            </div>
            
            <span class="help-block">If a password appears at least once in a data leak, it will be considered compromised. We use the <a href="https://en.wikipedia.org/wiki/K-anonymity" target="_blank">k-Anonymity</a> model to determine if a password has been leaked via the <a href="https://haveibeenpwned.com/" target="_blank">haveibeenpwned.com</a> service without sacrificing the user's privacy or security</span>
        </div>
    </div>

    <div class="form-group clearfix d-flex @if( $errors->has('APP_2FA_FORCE') ) has-error @endif">
        {!! Form::label('APP_2FA_FORCE', 'Force 2FA: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            <div class="onoffswitch4">
                {!! Form::hidden('APP_2FA_FORCE', 'false') !!}

                {!! Form::checkbox('APP_2FA_FORCE', 'true', ( isset($SystemSettings->value['APP_2FA_FORCE']) && $SystemSettings->value['APP_2FA_FORCE'] === 'true' ), array('class' => 'onoffswitch4-checkbox', 'id' => 'APP_2FA_FORCE_chk')) !!}

                <label class="onoffswitch4-label" for="APP_2FA_FORCE_chk">
                    <span class="onoffswitch4-inner"></span>
                    <span class="onoffswitch4-switch"></span>
                </label>
            </div>
            <span class="help-block">Add additional security to all accounts using Two Factor Authentication. If enabled, it will force all users to switch and use 2FA on their next login</span>
        </div>
    </div>
</div>