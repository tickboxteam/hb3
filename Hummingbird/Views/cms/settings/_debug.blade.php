<?php
/**
 * Hummingbird: Settings > Debug
 *
 * @package     HB3
 * @author      Daryl Phillips <darylp@tickboxmarketing.co.uk>
 * @copyright   2023, Tickbox Marketing Ltd
*/
?>

<div role="tabpanel" class="tab-pane active" id="debug">
    <div class="form-group clearfix d-flex @if( $errors->has('APP_DEBUG') ) has-error @endif">
        <h4>Debugging</h4>

        {!! Form::label('APP_DEBUG', 'Debug mode: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            <div class="onoffswitch4">
                {!! Form::hidden('APP_DEBUG', 'false') !!}

                {!! Form::checkbox('APP_DEBUG', 'true', config()->get('app.debug'), array('class' => 'onoffswitch4-checkbox', 'id' => 'APP_DEBUG_chk')) !!}

                <label class="onoffswitch4-label" for="APP_DEBUG_chk">
                    <span class="onoffswitch4-inner"></span>
                    <span class="onoffswitch4-switch"></span>
                </label>
            </div>
        </div>
    </div>
    
    <hr />

    <div class="form-group clearfix @if( $errors->has('APP_BACKEND_RESTRICTED') ) has-error @endif d-flex">
        <h4>Backend restriction</h4>
        {!! Form::label('APP_BACKEND_RESTRICTED', 'Restrict mode: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            <div class="onoffswitch4">
                {!! Form::hidden('APP_BACKEND_RESTRICTED', 'false') !!}
                {!! Form::checkbox('APP_BACKEND_RESTRICTED', 'true', ( isset($SystemSettings->value['APP_BACKEND_RESTRICTED']) && $SystemSettings->value['APP_BACKEND_RESTRICTED'] == 'true' ), array('class' => 'onoffswitch4-checkbox', 'id' => 'APP_BACKEND_RESTRICTED_chk')) !!}
                <label class="onoffswitch4-label" for="APP_BACKEND_RESTRICTED_chk">
                    <span class="onoffswitch4-inner"></span>
                    <span class="onoffswitch4-switch"></span>
                </label>
            </div>
        </div>
    </div>

    <div class="form-group clearfix @if( $errors->has('APP_BACKEND_RESTRICTED_IPS') ) has-error @endif ">
        {!! Form::label('APP_BACKEND_RESTRICTED_IPS', 'Backend restricted IP addresses: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            {!! Form::text('APP_BACKEND_RESTRICTED_IPS', ( isset($SystemSettings->value['APP_BACKEND_RESTRICTED_IPS']) ) ? $SystemSettings->value['APP_BACKEND_RESTRICTED_IPS'] : NULL, array('class' => 'form-control lighter-form-control')) !!}
            @if( $errors->has('APP_BACKEND_RESTRICTED_IPS') )
                <span class="help-block">{!! $errors->first("APP_BACKEND_RESTRICTED_IPS") !!}</span>
                <hr />
            @endif

            <span class="help-block">If you wish to restrict CMS access to specific IP address(es), add them here.</span>
        </div>
    </div>

    <hr />

    <div class="form-group clearfix @if( $errors->has('APP_RESTRICTED') ) has-error @endif d-flex">
        <h4>Site restriction (whole site)</h4>
        {!! Form::label('APP_RESTRICTED', 'Restrict mode: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            <div class="onoffswitch4">
                {!! Form::hidden('APP_RESTRICTED', 'false') !!}
                {!! Form::checkbox('APP_RESTRICTED', 'true', ( isset($SystemSettings->value['APP_RESTRICTED']) && $SystemSettings->value['APP_RESTRICTED'] == 'true' ), array('class' => 'onoffswitch4-checkbox', 'id' => 'APP_RESTRICTED_chk')) !!}
                <label class="onoffswitch4-label" for="APP_RESTRICTED_chk">
                    <span class="onoffswitch4-inner"></span>
                    <span class="onoffswitch4-switch"></span>
                </label>
            </div>
        </div>
    </div>

    <div class="form-group clearfix @if( $errors->has('APP_RESTRICTED_IPS') ) has-error @endif ">
        {!! Form::label('APP_RESTRICTED_IPS', 'Site restricted IP addresses: ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            {!! Form::text('APP_RESTRICTED_IPS', ( isset($SystemSettings->value['APP_RESTRICTED_IPS']) ) ? $SystemSettings->value['APP_RESTRICTED_IPS'] : NULL, array('class' => 'form-control lighter-form-control')) !!}
            @if( $errors->has('APP_RESTRICTED_IPS') )
                <span class="help-block">{!! $errors->first("APP_RESTRICTED_IPS") !!}</span>
                <hr />
            @endif

            <span class="help-block">If you wish to restrict website access to specific IP address(es), add them here.</span>
        </div>
    </div>

    <div class="form-group clearfix @if( $errors->has('APP_RESTRICTED_REDIRECT_TO') ) has-error @endif ">
        {!! Form::label('APP_RESTRICTED_REDIRECT_TO', 'Redirect URL (on restricted access): ', array('class' => 'col-sm-2')) !!}
        <div class="col-sm-10">
            {!! Form::text('APP_RESTRICTED_REDIRECT_TO', ( isset($SystemSettings->value['APP_RESTRICTED_REDIRECT_TO']) ) ? $SystemSettings->value['APP_RESTRICTED_REDIRECT_TO'] : NULL, array('class' => 'form-control lighter-form-control')) !!}
            @if( $errors->has('APP_RESTRICTED_REDIRECT_TO') )
                <span class="help-block">{!! $errors->first("APP_RESTRICTED_REDIRECT_TO") !!}</span>
                <hr />
            @endif

            <span class="help-block">If access is prohibited, then redirect them to this URL. Perfect for staging/development environments.</span>
        </div>
    </div>
</div>