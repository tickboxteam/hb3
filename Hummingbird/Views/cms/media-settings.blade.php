@extends('HummingbirdBase::cms.layout')

@section('styles')
@stop

@section('breadcrumbs')
	@if (count($breadcrumbs) > 0)
		<div class="row">
		    <div class="col-md-12">
		        <!--breadcrumbs start -->
		        <ul class="breadcrumb">
		        	<?php $i = 0;?>
			        @foreach ($breadcrumbs as $breadcrumb)
			        	<?php $i++;?>
			            <li>
			            @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
			            	<a href="{!! $breadcrumb['url'] !!}">@if($breadcrumb['icon']) <i class="{!! $breadcrumb['icon'] !!}"></i> @endif{!! $breadcrumb['title'] !!}</a>
			            @else
			            	@if($breadcrumb['icon']) <i class="{!! $breadcrumb['icon'] !!}"></i> @endif{!! $breadcrumb['title'] !!}
			            @endif
			            </li>
			        @endforeach
		        </ul>
		        <!--breadcrumbs end -->
		    </div>
		</div>
	@endif
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="panel">
            	<h3 class="normal pull-left">Media Centre Settings</h3>
            	<button type="button" class="btn btn-round btn-primary add-collection pull-right" data-toggle="modal" data-target="#add-collection"><i class="fa fa-new"></i> Add Image Size</button>

            	<div class="clearfix"></div>

				<p>The sizes listed below determine the maximum dimensions (in pixels) to use when adding an image to the Media Library.</p>

				<p>By default we automatically create a 250px wide version of all images to use in the media centre. This <strong><span class="italic">can not</span></strong> be deleted.</p>
			</section>
		</div>

        <div class="col-md-12">
            <section class="panel">
            	<div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name/Type</th>
                                <th>Description</th>
                                <th>Width</th>
                                <th>Height</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>

                        	@foreach($image_sizes as $size)
                        		<tr>
                        			<td>{!! $size['name'] !!}</td>
                        			<td>{!! $size['description'] !!}</td>
                        			<td>@if(isset($size['width']) AND $size['width'] > 0) {!! $size['width'] !!} @else Auto @endif</td>
                        			<td>@if(isset($size['height']) AND $size['height'] > 0) {!! $size['height'] !!} @else Auto @endif</td>
                        			<td>
                        				Actions here
                        			</td>
                        		</tr>
                        	@endforeach
                        </tbody>
                    </table>
                </div>
			</section>
		</div>
	</div>

	<div id="add-collection" class="modal fade">
	    <div class="modal-dialog clearfix">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close redactor-modal-btn redactor-modal-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">New image size</h4>
	            </div>

				{!! Form::open(array('route' => 'hummingbird.media.settings.store', 'method' => 'post')); !!}
		            <div class="modal-body">
						<div class="form-group">
							<label for="title">Title: <span style="color:red;">*</span></label>
							<input type="text" class="form-control" name="title" id="title" placeholder="Enter handle for new image size">
						</div>

						<div class="form-group">
							<label for="type">Type:</label>
							<input type="text" class="form-control" name="type" id="type" placeholder="Enter type for images (for image grouping)">
						</div>

						<div class="form-group">
							<div class="row clearfix">
								<div class="col-sm-6">
									<label for="title">Width: </label>
									<input type="text" class="form-control" name="width" id="width" placeholder="Enter width...">
								</div>
								<div class="col-sm-6">
									<label for="title">Height: </label>
									<input type="text" class="form-control" name="height" id="height" placeholder="Enter height...">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> Specify a fixed width/height or allow for automatic height or width by providing individual values.</div>
						</div>

                        <div class="form-group">
                        	<div class="row clearfix">
	                            <label for="description" class="col-sm-2 control-label">Description: </label>
	                            <div class="col-sm-10">
	                                <textarea class="form-control textareas" rows="5" name="description" id="description" placeholder="Description..."></textarea>
	                            </div>
                            </div>
                        </div>
		            </div>

		            <div class="modal-footer">
		                <footer>
		                    <button type="submit" class="btn btn-primary">Add Image size</button>
		                </footer>
		            </div>
	            {!! Form::close() !!}
	        </div>
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
@stop

@section('scripts')
	<script>
		$(document).ready(function() {
			@if( Session::has('MediaError') )
				$("button[data-target='#add-collection']").trigger('click');
			@endif
		});
	</script>
@stop