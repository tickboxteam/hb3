@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {!! Session::get('success') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-info alert-danger fade in">
                {!! Session::get('message') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {!! Session::get('error') !!}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('errors'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <p>There was a problem adding menu. Please go back try again.</p>
            </div>
        </div>
    </div>
@endif



<?php $even = false; ?>

<div class="row">
    <div class="col-md-8">
        <section class="panel">
            @if(count($menus) > 0)
                <div class="table">
                    <table class="results table table-striped">
                        <thead>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </thead>
                        <tbody>
                            @foreach($menus as $menu)               
                                <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
                                    <td>{!! $menu->name !!}</td>
                                    <td>{!! $menu->description !!}</td>
                                    <td>
                                        @if(Auth::user()->userAbility('menu.update'))<a href="{!! route('hummingbird.menus.edit', $menu->id) !!}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>@endif

                                        @if(Auth::user()->userAbility('menu.delete'))
                                            {!! Form::open(array('route' => array('hummingbird.menus.destroy', $menu->id), 'method' => 'delete', 'class' => 'inline-block', 'id' => 'delete_menu_' . $menu->id)) !!}
                                                <button id="delete_menu_{!! $menu->id !!}" type="submit" class="btn btn-xs btn-danger btn-confirm-delete" data-target="#delete_menu_{!! $menu->id !!}" data-id="{!! $menu->id !!}" data-delete-body="You are about to delete this menu (&quot;{!! $menu->name !!}&quot;). If this is correct, all menu items will also be deleted."><i class="fa fa-trash"></i></button>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-info alert-box text-center">No menus available</div>
            @endif
        </section>
    </div>
    
    @if(Auth::user()->userAbility('menu.create'))
        <div class="col-md-4">
            <section class="panel">
                <h1 class="normal">Add new menu</h1>

                {!! Form::open(array('route' => 'hummingbird.menus.store', 'method' => 'post', 'class' => 'form-horizontal')) !!}
                    <div class="form-group @if( $errors->has('name') ) has-error @endif">
                        <label for="name" class="col-sm-2">Name:</label>
                        <div class="col-sm-10">
                            <input class="input_box form-control" id="name" type="text" name="name">
                            @if( $errors->has('name') )
                                <p class="text-danger">{!! $errors->first('name') !!}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6 pull-right">
                            <button type="submit" class="btn btn-sm btn-success pull-right">Add menu</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </section>
        </div>
    @endif
</div>

@stop
