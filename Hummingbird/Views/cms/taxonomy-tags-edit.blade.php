@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <div class="clearfix">
                    {!! Form::open(array('route' => array('hummingbird.tags.destroy', $tag->id), 'method' => 'delete')) !!}
                        <button type="submit" class="pull-right btn btn-xs btn-danger"><i class="fa fa-trash"></i> Remove</button>
                    {!! Form::close() !!}
                </div>

                <div class="row">
                    <div class="col-md-8">
                        {!! Form::open(array('route' => array('hummingbird.tags.update', $tag->id), 'method' => 'put', 'class' => 'form-horizontal')) !!}
                            {!! Form::setModel($tag) !!}
                            <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                <label for="name" class="col-sm-2 control-label">Tag name:</label>
                                <div class="col-sm-10">
                                    {!! Form::text('name', NULL, array('id' => 'name', 'class' => 'tax-name form-control', 'placeholder' => 'Tag name')) !!}
                                    @if( $errors->has('name') )
                                        <span class="help-block">{!! $errors->first('name') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="display_name" class="col-sm-2 control-label">Display Name:</label>
                                <div class="col-sm-10">
                                    {!! Form::text('display_name', NULL, array('id' => 'display_name', 'class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                <div class="col-sm-10">
                                    {!! Form::text('slug', NULL, array('id' => 'slug', 'class' => 'form-control', 'placeholder' => 'Tag slug')) !!}
                                    <div class="regenerate @if(str_slug($tag->name) == $tag->slug) hide @endif hide">
                                        <a href="#" class="btn btn-xs"><i class="fa fa-cog"></i> Generate new slug</a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Description:</label>
                                <div class="col-sm-10">
                                    {!! Form::textarea('description', NULL, array('class' => 'form-control textareas', 'size' => '1x1')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default btn-primary">Update</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </section>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            if( $('.form-control.textareas').length > 0 ) {
                $(".form-control.textareas").autogrow();
            }
        });
    </script> 
@stop
