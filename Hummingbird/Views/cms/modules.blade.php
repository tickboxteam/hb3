@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />

    <style>
        .light {color:#CCC;}
        section.modules ul {padding-left:0;}
        section.modules ul li {list-style-type: none;padding-top:10px;padding-bottom:5px;}


        .panel {padding:20px;}
        .panel.btn-danger {color:#d43f3a;background-color:#d43f3a;color:white;}
        .panel.btn-danger a, .panel.btn-danger a:visited, .panel.btn-danger a:active {color:white;}
    </style>
@stop

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-danger fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    <!-- Modal for copy to clipboard success -->
    <div id="copyToClipbrdModal" class="modal fade" role="dialog" >
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content alert-success">
                <div class="modal-body">
                    <p></p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                @include('HummingbirdBase::cms.modules-filter-template')
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                @if(Auth::user()->userAbility('modules.create'))
                    <button id="add-module-btn" type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-module"><i class="fa fa-plus-circle"></i> Add module</button>
                @endif

                @if(count($modules) > 0)
                    <div class="table">
                        <table class="table table-striped">
                            <thead>
                                <th scope="row">Name</th>
                                <th scope="row">Template</th>
                                <th scope="row">Last updated</th>
                                <th scope="row">Shortcode</th>
                                <th>Actions</th>
                            </thead>

                            <tbody>
                                @foreach($modules as $module)
                                    <tr>
                                        <td>{!! $module->name !!}</td>
                                        <td><?php $template = ($module->moduletemplate_id != '') ? Hummingbird\Models\Moduletemplate::where('id', '=', $module->moduletemplate_id)->first():NULL;
                                            $name = (null !== $template) ? $template->name:'No template'; ?>
                                            {!! $name !!}
                                        </td>
                                        <td>{!! $module->updated_at->format( "d/m/Y H:i:s" ) !!}</td>
                                        <td>
                                            <a href="#" class="clipboard_copy"data-clipboard-value="{!! $module->shortcode() !!}" title="Copy to clipboard &quot;{!! $module->shortcode() !!}&quot;">{!! $module->shortcode() !!} </a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-xs btn-default clipboard_copy" data-clipboard-value="{!! $module->shortcode() !!}" title="Copy to clipboard &quot;{!! $module->name !!}&quot;"><i class="fa fa-clipboard"></i></a>

                                            @if(Auth::user()->userAbility('modules.create'))<a href="{!! route('hummingbird.modules.replicate', $module->id) !!}" class="btn btn-xs btn-default" title="Replicate &quot;{!! $module->name !!}&quot;"><i class="fa fa-copy"></i></a>@endif

                                            @if(Auth::user()->userAbility('modules.update'))<a href={!! route('hummingbird.modules.show', $module->id) !!} class="btn btn-xs btn-info" title="Edit {!! $module->name !!}"><i class="fa fa-edit"></i></a>@endif

                                            @if(Auth::user()->userAbility('modules.delete'))
                                                {!! Form::open(array('route' => array('hummingbird.modules.destroy', $module->id), 'method' => 'delete', 'class' => 'inline-block', 'id' => 'delete_module_' . $module->id)) !!}
                                                    <button id="delete_module_{!! $module->id !!}" type="submit" class="btn btn-xs btn-danger btn-confirm-delete" data-target="#delete_module_{!! $module->id !!}" data-title="{!! $module->name !!}" data-id="{!! $module->id !!}" title="Delete &quot;{!! $module->name !!}&quot;" data-delete-body="You are about to delete this module (&quot;{!! $module->name !!}&quot;)."><i class="fa fa-trash"></i></button>
                                                {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>

                    <div class="text-center"> 
                        {!! $modules->appends(Request::except('page'))->render() !!}
                    </div>
                @else
                    <div class="alert alert-info text-center">
                        There are no modules.
                    </div>
                @endif
            </section>
        </div>   
    </div>
    
    @if(Auth::user()->userAbility('modules.create'))
        <div class="modal fade" id="add-module">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add new module</h4>
                    </div>

                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12"> 
                                {!! Form::open(array('route' => 'hummingbird.modules.store', 'class' => 'form-horizontal')) !!}
                                    <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                        {!! Form::label('name', 'Name: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('name', Request::old('name'), array('class' => 'form-control')) !!}

                                            @if( $errors->has('name') )
                                                <p class="text-danger">{!! $errors->first('name') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('moduletemplate_id', 'Template: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            <select class="form-control" name="moduletemplate_id" id="moduletemplate_id">
                                                <optgroup label="General">
                                                    <option value="">No template (plain HTML)</option>
                                                    <option disabled></option>
                                                </optgroup>

                                                <?php $templates = Hummingbird\Models\Moduletemplate::orderBy('name', 'ASC')->get();?>
                                                
                                                @if( $templates->count() > 0 )
                                                    <optgroup label="Templates">
                                                        @foreach( $templates as $template )
                                                            <option value="{!! $template->id !!}" @if( Request::old('moduletemplate_id') == $template->id ) selected="selected" @endif>{!! $template->name !!}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-10">
                                            <button type="submit" class="btn btn-default btn-primary">Add Module</button>
                                        </div>
                                    </div> 
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.filter-results').click(function(e) {
                $('.filter-form').submit();
            });

            @if(count($errors) > 0)
                $("#add-module-btn").trigger('click');
            @endif
        });
    </script>
@stop
