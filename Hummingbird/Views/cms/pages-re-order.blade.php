@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop


@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('styles')
    <style>
        .sortable {
            padding: 0;
        }

        .sortable li {
            cursor: move;
        }

        .sortable-chosen {
            opacity: .5;
            background: #C8EBFB;
        }
    </style>
@stop

@section('content')
    @include('HummingbirdBase::cms.partials.notifications')

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <div class="clearfix" style="margin-bottom:20px">
                    <a href="/hummingbird/pages" class="pull-right btn btn-primary"style="margin-left:5px;"><i class="fa fa-arrow-circle-o-left"></i> Go back</a>
                </div>

                <hr />

                <div class="form-group row">
                    {!! Form::label('parent', 'Select parent page to re-order it\'s children', array('class' => 'col-sm-4')) !!}
                    <div class="col-sm-8">
                        {!! Form::select('parent', $pageList, $current, array('id' => 'parent', 'class' => 'selectpicker form-control')) !!}
                    </div>
                </div>

                @if($sortable->count() > 0)
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Drag and drop to re-order</label>
                        </div>
                        <div class="col-sm-8">
                            <ul id="items" class="list-group sortable">
                                @foreach($sortable as $page)
                                    <li class="list-group-item" data-id="{{$page->id}}"> <strong class="{{$page->isLive() ? 'text-success' : 'text-danger' }}">[{{$page->isLive() ? 'Public' : 'Draft' }}]</strong> {{$page->title}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @else
                    <div class="alert alert-info">This page doesn't have any children to be sorted</div>
                @endif
                
            </section>
        </div>
    </div>

    
@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sortablejs@1.7.0/Sortable.min.js" integrity="sha256-+BvLlLgWJALRwV4lbCh0i4zqHhDqxR8FKUJmIl/u/vQ=" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function()
        {
            $("#parent").data('live-search', 'true');
            $('#parent').selectpicker('render');

            $('#parent').change(function (e) {
                var parent = $(e.target).val();
                window.location = `{{ url(App::make('backend_url')) }}/pages/re-order/${parent}`;
            });
        });

        const el = document.getElementById('items');
        if (el) {
            Sortable.create(el, {
                animation: 150,
                onUpdate: (e) => {
                    const newOrder = [...e.to.children].map((item, i) => ({
                        'id': item.dataset.id,
                        'position': i + 1,
                    }));
                    
                    fetch('{{ url(App::make('backend_url')) }}/pages/re-order', {
                        method: 'PUT',
                        headers: {
                            "Content-Type": "application/json; charset=utf-8",
                        },
                        body: JSON.stringify({
                            'pages': newOrder,
                            '_token': "{{ csrf_token() }}"
                        })
                    })
                    .then((response) => {
                        if (!response.ok) {
                            throw Error(response.statusText);
                        }
                        return response;
                    })
                    .then((data) => message('Page order updated', 'success'))
                    .catch((error) => message(`Server returned error: ${error.message}`, 'danger'));
                }
            });
        }

        function message(message, type) {
            const notifications = document.querySelector('.notifications');

            if (!type) {
                type = 'danger';
            }

            if (notifications) {
                notifications.innerHTML = `<div class="alert alert-${type} text-center">${message}</div>`;
            }

            setTimeout(() => notifications.innerHTML = '', 3000);
        }
    </script>
@stop