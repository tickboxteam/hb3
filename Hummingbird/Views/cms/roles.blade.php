@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-info alert-info fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                @if(Auth::user()->userAbility('roles.create'))
                    <button id="add-role-btn" type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-role"><i class="fa fa-plus-circle"></i> Add role</button>
                @endif
                
                @if( $total_system_roles > 0 )
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($system_roles as $system_role)
                                    @if(!$system_role->hidden)

                                        <tr>
                                            <td>{!! $system_role->listing_name ?: $system_role->name !!}</td>
                                            <td>
                                                @if(!$system_role->locked)
                                                    <a href="{!! route('hummingbird.roles.show', $system_role->id) !!}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                                                    
                                                    {!! Form::open(array('route' => array('hummingbird.roles.destroy', $system_role->id), 'method' => 'delete', 'class' => 'inline-block', 'id' => 'delete_role_' . $system_role->id)) !!}
                                                        <button id="delete_role_{!! $system_role->id !!}" type="submit" class="btn btn-xs btn-danger btn-confirm-delete" data-target="#delete_role_{!! $system_role->id !!}" data-title="{!! $system_role->name !!}" data-id="{!! $system_role->id !!}" title="Delete role &quot;{!! $system_role->name !!}&quot;" data-delete-body="You are about to delete the role (&quot;{!! $system_role->name !!}&quot;)."><i class="fa fa-trash"></i></button>
                                                    {!! Form::close() !!}
                                                @else
                                                    <img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQYmxCNd0RIQJytdZl4TVPYFOKw5h5E147WSv3zge8ENkp_DLggZnh7pJI" width="15" />
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <h3>No roles have been created.</h3>
                @endif
            </section>
        </div>
    </div>

    @if(Auth::user()->userAbility('roles.create'))
        <div class="modal fade" id="add-role">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add new role</h4>
                    </div>

                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12"> 
                                {!! Form::open(array('route' => ['hummingbird.roles.store'], 'class' => 'form-horizontal')) !!}
                                    <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                        {!! Form::label('name', 'Name: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('name', Request::old('name'), array('class' => 'form-control')) !!}

                                            @if( $errors->has('name') )
                                                <p class="text-danger">{!! $errors->first('name') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group @if( $errors->has('parentrole_id') ) has-error @endif">
                                        {!! Form::label('parentrole_id', 'Parent role: ', array('class' => 'col-sm-3')) !!}
                                        <div class="col-sm-9">
                                            <select name="parentrole_id" id="parentrole_id" class="form-control">
                                                @foreach($system_roles as $system_role)
                                                    <option value="{!! $system_role->id !!}">{!! $system_role->listing_name ?: $system_role->name !!}</option>
                                                @endforeach
                                            </select>

                                            @if( $errors->has('parentrole_id') )
                                                <p class="text-danger">{!! $errors->first('parentrole_id') !!}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-10">
                                            <button type="submit" class="btn btn-default btn-primary">Add role</button>
                                        </div>
                                    </div> 
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            @if(count($errors) > 0)
                $("#add-role-btn").trigger('click');
            @endif
        });
    </script>
@stop
