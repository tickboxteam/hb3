<div class="row">
    <div class="col-md-12">
        @if (count($breadcrumbs) > 0)
            <div class="row">
                <div class="col-md-12">
                    <!--breadcrumbs start -->
                    <ul class="breadcrumb">
                        <?php $i = 0;?>
                        @foreach ($breadcrumbs as $breadcrumb)
                            @if($i > 0)
                                <li>
                                    @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                                        <a class="view-collection" @if(isset($breadcrumb['id'])) data-id="{{ $breadcrumb['id'] }}" @endif href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                                    @else
                                        @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                                    @endif
                                </li>
                            @endif
                            <?php $i++?>
                        @endforeach
                    </ul>
                    <!--breadcrumbs end -->
                </div>
            </div>
        @endif
    </div>
    
    <div class="col-md-6 image-calc" data-col-size="8" >
        <div class="image-holder">
            <div class="filename text-center"><i class="fa fa-file-{{str_slug($extension)}} fa-2x text-center"></i><br />{{$file->title}}</div>
        </div>
    </div>
    
    <div class="col-md-6 pull-right" data-id="{{$file->id}}"  data-downloads="{{$is_dl}}">
        <div class="row">
        	<div class="col-md-12">
                <h4>Details</h4>

                {!! Form::open(array('route' => array('hummingbird.media.postEditMediaItem', $file->id), 'method' => 'post', 'class' => 'form-horizontal')) !!}
                    <input id="collection" type="hidden" name="collection" value="{{ $file->collection }}" />
                    <input id="action" type="hidden" name="action" value="update" />

                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                            <input name="title" id="title" type="text" class="form-control" placeholder="Media Title" value="{{$file->title}}">
                        </div>
                    </div>

                    <div class="form-group @if($featured) hide @endif">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea style="height:auto;" class="form-control textareas" rows="5" name="description" id="description" placeholder="Enter description...">{{$file->description}}</textarea>
                        </div>
                    </div>

               {!! Form::close() !!}
            </div>
		</div>
	</div>
