@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    {!! Breadcrumbs::render(Request::path()) !!}
@stop

@section('styles')
    <link rel="stylesheet" href="/themes/admin/hummingbird/default/lib/datetimepicker/datetimepicker.css" />
@stop

@section('content')

    @if (Session::has('message'))
        <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <div class="row">
                    {!! Form::open(array('route' => array('hummingbird.errors.index'), 'method' => 'get'), array('class' => 'form-inline')) !!}
                        <div class="col-md-3">
                            <h6>Search:</h6>
                            {!! Form::text('s', Request::get('s'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-2">
                            <h6>By type:</h6>
                            {!! Form::select('type', [NULL => '--- All ---', 'Files' => 'Files', 'URL' => 'URL', 'Other' => 'Other'], Request::get('type'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-3">
                            <h6>Date:</h6>

                            <div class="input-group">
                                {!! Form::text('from', Request::get('from'), array('class' => 'datepicker form-control datepicker-from', 'placeholder' => 'From...')) !!}
                                <span class="input-group-addon" style="border-left:0;border-right:0;">to</span>
                                {!! Form::text('to', Request::get('to'), array('class' => 'datepicker form-control datepicker-to', 'placeholder' => 'To...')) !!}
                            </div>
                        </div>

                        <div class="col-md-2">
                            <h6>Order:</h6>
                            {!! Form::select('orderBy', ['asc' => 'ASC', 'desc' => 'DESC'], Request::get('orderBy'), array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-2">
                            <h6>&nbsp;</h6>
                            <button type="submit" class="btn btn-default filter-results" data-url="{{ route('hummingbird.errors.index') }}" >Filter</button>

                            <a href="{{ route('hummingbird.errors.index') }}" class="btn">Reset</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>

    @if( $errors->count() )
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    @if( Auth::user()->isSuperUser() )
                        {!! Form::open(array('route' => array('hummingbird.errors.truncate'), 'id' => 'errorsdestroyall', 'method' => 'POST')) !!}
                            <button type="submit" class="btn btn-danger pull-right btn-confirm-delete" data-delete-body="You are about to delete all errors from the log." data-target="#errorsdestroyall"><i class="fa fa-trash"></i>&nbsp; Remove all</button>
                        {!! Form::close() !!}
                    @endif

                    {!! Form::open(array('route' => array('hummingbird.errors.remove'), 'id' => 'errorsdestroy', 'method' => 'delete')) !!}
                        @if( Auth::user()->userAbility('error.delete') )
                            <button type="submit" class="btn btn-danger btn-confirm-delete remove-selected hide" disabled data-delete-body="You are about to delete errors from the log." data-target="#errorsdestroy"><i class="fa fa-trash"></i>&nbsp; Remove selected</button>
                        @endif

                        <div class="table">
                            <table class="table table-striped table-hover break-words">
                                <thead>
                                    <tr>
                                        @if( Auth::user()->userAbility('error.delete') )
                                            <th scope="row">{!! Form::checkbox('select_all', 0, NULL, array('class' => 'checkbox-toggle', 'data-target' => '.checkbox-toggle-target')) !!}</th>
                                        @endif

                                        <th># Visited</th>
                                        <th>URL</th>
                                        <th>Type</th>
                                        <th>Last activated</th>
                                        @if(Auth::user()->userAbility('error.delete'))
                                            <th>Actions</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($errors as $error)
                                    
                                        <tr>
                                            @if( Auth::user()->userAbility('error.delete') )
                                                <td>{!! Form::checkbox('delete-selected[]', $error->id, NULL, array('class' => 'checkbox-toggle-target')) !!}</td>
                                            @endif
                                            <td>{!! $error->accessed !!}</td>
                                            <td><div class="tooltip-table">{!! $error->url !!}</div></td>
                                            <td>{!! $error->type !!}</td>    
                                            <td>{!! $error->updated_at !!}</td>
                                            @if(Auth::user()->userAbility('error.delete'))
                                                <td>
                                                    <button id="error_destroy_{!! $error->id !!}" name="delete-individual" value="{!! $error->id !!}" type="submit" class="btn btn-xs btn-danger btn-confirm-delete" data-delete-body="You are about to delete this error from the log." data-target="#error_destroy_{!! $error->id !!}"><i class="fa fa-trash"></i></a>
                                                </td>
                                            @endif
                                        </tr>
                                    
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    {!! Form::close() !!}

                    <div class="text-center"> 
                        {!! $errors->appends(Request::only(['s', 'type', 'from', 'to', 'orderBy']))->render() !!}
                    </div>
                </section>
            </div>
        </div>
    @else
        <div class="alert alert-success">No errors found.</div>
    @endif
@stop

@section('scripts')
    <script src="/themes/admin/hummingbird/default/lib/moment/moment.js"></script>
    <script src="/themes/admin/hummingbird/default/lib/datetimepicker/datetimepicker.js"></script>
    <script>
        $(document).ready(function() {
            $(".checkbox-toggle").click(function(e) {
                if( typeof $(this).data('target') !== 'undefined' ) {
                    $( $(this).data('target') ).prop('checked', $(this).prop('checked'));
                }

                if( $(".checkbox-toggle-target:checked").length > 0 ) {
                    $(".remove-selected").removeAttr('disabled').removeClass('hide');
                }
                else {
                    $(".remove-selected").attr('disabled', true).addClass('hide');   
                }
            });

            if( $(".checkbox-toggle-target").length > 0 ) {
                $(".checkbox-toggle-target").click(function() {
                    if( $(".checkbox-toggle-target:checked").length > 0 ) {
                        $(".remove-selected").removeAttr('disabled').removeClass('hide');
                    }
                    else {
                        $(".remove-selected").attr('disabled', true).addClass('hide');  
                    }
                });      
            }

            $('.datepicker-from').datetimepicker(
            {
                icons: {
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: "DD-MM-YYYY"
            });

            $('.datepicker-to').datetimepicker(
            {
                icons: {
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: "DD-MM-YYYY"
            });


            $(".datepicker-to").on("dp.change", function (e) 
            {
                $('.datepicker-from').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>
@stop
