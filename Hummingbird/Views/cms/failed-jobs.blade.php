@extends('HummingbirdBase::cms.layout')

@section('styles')
    <style>
        pre {
            height: 150px;
        }
        pre code {
            font-size: 12px;
            font-size: 1.2rem;
        }
    </style>
@stop

@section('content')
    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-info fade in">
                    {!! Session::get('message') !!}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                @if( $FailedJobs->count() > 0 )
                    @if( Auth::user()->isSuperUser() )
                        {!! Form::open(array('route' => array('hummingbird.failed-jobs.flush'), 'id' => 'faileddestroyall', 'method' => 'POST')) !!}
                            <button type="submit" class="btn btn-danger pull-right btn-confirm-delete" data-delete-body="You are about to clear all failed jobs. These can not be recovered." data-target="#faileddestroyall"><i class="fa fa-trash"></i>&nbsp; Remove all</button>
                        {!! Form::close() !!}

                        <a class="btn btn-warning pull-right" href="{!! route('hummingbird.failed-jobs.retry-all') !!}" style="margin-right: 5px;"><i class="fa fa-refresh"></i>&nbsp; Restart all jobs</a>
                    @endif

                    <div class="table-responsive">
                        <table class="table table-striped table-horizontal table-hover">
                            <thead>
                                <th scope="row">Type</th>
                                <th>Job</th>
                                <th>Data</th>
                                <th>Failed</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach( $FailedJobs as $FailedJob )
                                    <tr>
                                        <td>{!! $FailedJob->queue !!}</td>
                                        <td>{!! $FailedJob->payloadJob !!}</td>
                                        <td><pre><code>{!! $FailedJob->payloadData !!}</code></pre></td>
                                        <td>{!! $FailedJob->failed_at->diffForHumans() !!}</td>
                                        <td class="text-center">
                                            <a href="{!! route('hummingbird.failed-jobs.retry', $FailedJob->id) !!}" class="btn btn-xs btn-warning"><i class="fa fa-refresh"></i></a>

                                            {!! Form::open(array('route' => array('hummingbird.failed-jobs.destroy', $FailedJob->id), 'id' => 'destroyFailedJob_' . $FailedJob->id, 'method' => 'DELETE', 'class' => 'inline-block')) !!}
                                                <button type="submit" class="btn btn-xs btn-danger pull-right btn-confirm-delete" data-delete-body="You are about to delete this job. This can not be recovered." data-target="#destroyFailedJob_{!! $FailedJob->id !!}"><i class="fa fa-trash"></i></button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="text-center"> 
                        {!! $FailedJobs->appends( Request::except('page') )->render() !!}
                    </div>
                @else
                    <div class="alert alert-info">No failed jobs found</div>
                @endif
            </section>
        </div>
    </div>
@stop