<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset</h2>

		<div>
			To reset your password, complete this <a href="{!! route('hummingbird.reset-password.new-password', array($token)) !!}">form</a>.<br/>
			This link will expire in {!! Config::get('auth.reminder.expire', 60) !!} minutes.
		</div>
	</body>
</html>
