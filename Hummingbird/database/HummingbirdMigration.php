<?php namespace Hummingbird\Database;

use Config;
use DB;
use Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HummingbirdMigration extends Migration {
    /**
     * The indexes stored on the table
     * @var Mixed
     */
    protected $tableIndexes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $ServerVars;

    public function __construct() {
        $this->getServerValues();

        foreach(Schema::getConnection()->getDoctrineSchemaManager()->listTableNames() as $table ) {
            $this->loadTableIndexes( $table );
        }
    }

    /**
     * Load all server variables
     */
    protected function getServerValues() {
        foreach( DB::select( 'SHOW VARIABLES LIKE "%version%"' ) as $ServerVar ) {
            $this->ServerVars[ $ServerVar->Variable_name ] = $ServerVar->Value;
        }
    }

    /**
     * Load the table indexes for a specific table
     * @param   String $table
     * @since   1.1.30
     * @version 1.0.0
     */
    public function loadTableIndexes($table) {
        if( Schema::hasTable($table) && !isset($this->tableIndexes[$table]) ) {
            $table_name = Config::get('database.connections.mysql.prefix') . $table;

            $this->tableIndexes[$table] = Schema::getConnection()->getDoctrineSchemaManager()->listTableIndexes($table_name);
        }
    }

    /**
     * Does the table have an index?
     * @param   String $table
     * @param   String $field
     * @param   String $index_type
     * @return  Boolean
     * @since   1.1.30
     * @version 1.0.0
     */
    public function tableHasIndex($table, $field, $index_type) {
        if( !isset($this->tableIndexes[$table]) ) {
            $this->loadTableIndexes($table);
        }

        return $this->checkIndexExists($table, "{$table}_{$field}_{$index_type}");
    }

    /**
     * Checking if the specific index exists on this table
     * @param  String $table
     * @param  String $index
     * @return Boolean
     */
    public function checkIndexExists($table, $index) {
        return ( isset($this->tableIndexes[$table]) && (array_key_exists("{$index}", $this->tableIndexes[$table])) );
    }
}
