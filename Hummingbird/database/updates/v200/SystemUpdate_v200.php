<?php namespace Hummingbird\Database\Updates;

use Event;
use Hummingbird\Libraries\Themes;
use Hummingbird\Models\Plugin;
use Illuminate\Database\Seeder as Seeder;
use Illuminate\Filesystem\Filesystem;

/**
 * An upgrade to allow us to run things when the system moves from
 * Laravel 4.2 to Laravel 5.0
 * 
 * @since 2.0.0
 */
class SystemUpdate_v200 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        // Legacy directories we no longer need, which can be removed
        foreach([base_path('backups'), base_path('updates')] as $deleteDirectory ):
            if( app(Filesystem::class)->isDirectory( $deleteDirectory ) ) {
                app(Filesystem::class)->deleteDirectory( $deleteDirectory );
            }
        endforeach;

        // Check we have the public directory for storage elements
        if( !app(Filesystem::class)->isDirectory( storage_path('app/public') ) ) {
            app(Filesystem::class)->makeDirectory( storage_path('app/public'), 0755, true);
        }

        // Move uploads, if the base directory exists to storage directory
        if( !app(Filesystem::class)->isDirectory( storage_path('app/public/uploads') ) ) {
            // Do we have the original uploads directory? Yes- let's move it
            if( app(Filesystem::class)->isDirectory( base_path('uploads') ) ) {
                app(Filesystem::class)->move( base_path('uploads'), storage_path('app/public/uploads') );
            }
            else {
                // No original directory - create a new one
                app(Filesystem::class)->makeDirectory( storage_path('app/public/uploads'), 0755, true);
            }
        }

        // Symlinking uploads
        if( is_link( public_path("uploads") ) === false ) {
            symlink( storage_path( "app/public/uploads" ), public_path( "uploads" ) );
        }

        // - Symlinking admin theme
        $PublicThemePath = public_path( 'themes/public' );
        $adminThemeAssetsPath = public_path( 'themes/admin' );

        if( !file_exists($adminThemeAssetsPath) ) {
            app(Filesystem::class)->makeDirectory($adminThemeAssetsPath, 0755, true);
        }
        
        // Create target symlink public theme assets directory if required
        if( !file_exists( public_path( "/themes/admin/hummingbird" ) ) ) {
            symlink( base_path( "themes/admin/hummingbird" ), public_path( "themes/admin/hummingbird" ) );
        }

        // Symlinking themes (all)
        $ActiveTheme = Themes::activeTheme();

        if( !file_exists($PublicThemePath) ) {
            app(Filesystem::class)->makeDirectory($PublicThemePath, 0755, true);
        }
        
        // Create target symlink public theme assets directory if required
        if( !file_exists( $PublicThemePath . "/{$ActiveTheme}" ) ) {
            symlink( base_path( "themes/public/{$ActiveTheme}" ), $PublicThemePath . "/{$ActiveTheme}" );
        }

        // Create plugin directories
        $PluginDirectory = base_path( 'plugins' );
        if( !app(Filesystem::class)->isDirectory( $PluginDirectory ) ) {
            app(Filesystem::class)->makeDirectory($PluginDirectory, 0755, true);
        }

        // Do we have the old workbech directory? If so, let's move everything
        if( app(Filesystem::class)->isDirectory( base_path('workbench') ) ):
            foreach( app(Filesystem::class)->directories( base_path('workbench') ) as $PluginVendor ):
                $PluginVendorPathInfo = pathinfo($PluginVendor);
                $NewPluginVendor = ucfirst( $PluginVendorPathInfo['basename'] );

                // Check if we have the vendor directory? If not, create it.
                if( !app(Filesystem::class)->isDirectory( $PluginDirectory . "/{$NewPluginVendor}" ) ) {
                    app(Filesystem::class)->makeDirectory( $PluginDirectory . "/{$NewPluginVendor}", 0755, true);
                }

                // Move all packages to new directory
                foreach( app(Filesystem::class)->directories( $PluginVendor ) as $Package ):
                    $PackagePathInfo = pathinfo($Package);

                    app(Filesystem::class)->move( $Package, $PluginDirectory . "/{$NewPluginVendor}/{$PackagePathInfo['basename']}" );
                endforeach;
            endforeach;

            // Now remove this old directory as we no longer need it.
            app(Filesystem::class)->deleteDirectory( base_path('workbench') );
        endif;

        // Symlinking plugins (all), that are active
        foreach( Plugin::where('active', 1)->get() as $ActivePlugin ):
            $PluginParts = explode("\\", $ActivePlugin['namespace']);

            Event::dispatch('hummingbird.plugin.Install.After', ['package' => [
                'vendor' => $PluginParts[0],
                'package_name' => $PluginParts[1]
            ]]);
        endforeach;
    }
}