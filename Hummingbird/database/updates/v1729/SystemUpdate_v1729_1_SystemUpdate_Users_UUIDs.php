<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\User;

/**
 * Seeder to allow us to assign UUID's to existing system users
 * 
 * @since 1.7.29
 */
class SystemUpdate_v1729_1_SystemUpdate_Users_UUIDs extends Seeder {
    public function run() {
        foreach( User::withTrashed()->whereNull('uuid')->get() as $SystemUser ):
            $SystemUser->uuid = (string) Illuminate\Support\Str::uuid();
            $SystemUser->save();
        endforeach;
    }
}
