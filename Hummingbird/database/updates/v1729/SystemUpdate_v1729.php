<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * Base seeder class for running all CMS upgrades
 * as part of the 1.7.29 version.
 * 
 * @since 1.7.29
 */
class SystemUpdate_v1729 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( SystemUpdate_v1729_1_SystemUpdate_Users_UUIDs::class );
    }
}