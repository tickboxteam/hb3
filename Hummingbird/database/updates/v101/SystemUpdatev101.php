<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Page;
use Hummingbird\Models\Searcher;

class SystemUpdatev101 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run()
    {
        $this->addSearchPage();
        $this->cleanSearchIndexes();
    }

    /**
     * Create the necessary Search page, if there isn't on already
     *
     * @return void
     */
    public function addSearchPage()
    {
        $Page = Page::where('url', 'search')->first();

        if(null === $Page)
        {
            $installed_page                 = new Page;
            $installed_page->title          = 'Search';
            $installed_page->url            = str_slug('Search');
            $installed_page->parentpage_id  = 1;
            $installed_page->permalink      = str_slug('Search');
            $installed_page->status         = 'public';
            $installed_page->locked         = TRUE;

            $installed_page->content = '';
            $installed_page->username = ''; //FIXME: This doesn't feel right because of the issues with default values
            $installed_page->password = ''; //FIXME: This doesn't feel right because of the issues with default values
            $installed_page->search_image = ''; //FIXME: This doesn't feel right because of the issues with default values
            $installed_page->featured_image = ''; //FIXME: This doesn't feel right because of the issues with default values
            $installed_page->custom_menu_name = ''; //FIXME: This doesn't feel right because of the issues with default values
            $installed_page->position = 0; //FIXME: This doesn't feel right because of the issues with default values
            $installed_page->enable_comments = 0; //FIXME: This doesn't feel right because of the issues with default values

            $installed_page->save();
        }
    }

    /**
     * Clean the search table (removes any HTML)
     * @return Void
     */
    public function cleanSearchIndexes()
    {
        // Clean the searcher - if they have anything
        foreach(Searcher::all() as $Searcher):
            $Searcher->forceClean()->save(['touch' => false]);
        endforeach;
    }
}