<?php namespace Hummingbird\Database\Updates;

use Hummingbird\Models\Redirections;
use Illuminate\Database\Seeder as Seeder;

/**
 * Seeder class will clean the existing redirects to remove trailing slashes. 
 * This does not apply to links where they are external redirects
 * 
 * @since 3.6.0
 */
class SystemUpdate_v360_1_RedirectSlashSeeder extends Seeder
{
    /**
     * Run the requested seeds
     * 
     * @return void
     */
    public function run():void
    {
        foreach(Redirections::withTrashed()->orderBy('id', 'ASC')->get() as $Redirect)
        {
            try {
                $Redirect->timestamps = false;
                $Redirect->from = clean_and_sluggify_url( $Redirect->from );
                $Redirect->to = clean_and_sluggify_url( $Redirect->to );
                $Redirect->save();                
            }
            catch(\Exception $e) {
                $this->command->info( 'Editing Redirect ID: #' . $Redirect->id);
                $this->command->info( $e->getFile() );
                $this->command->info( $e->getLine() );
                $this->command->info( $e->getMessage() );
                $this->command->info( $e->getTraceAsString() );
            }
        }
    }
}