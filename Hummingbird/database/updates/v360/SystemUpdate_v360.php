<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;

/**
 * Base seeder class for running all CMS upgrades
 * as part of the 3.6.0 version.
 * 
 * @since 3.6.0
 */
class SystemUpdate_v360 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( SystemUpdate_v360_1_RedirectSlashSeeder::class );
        $this->call( SystemUpdate_v360_2_MenuItemSlashSeeder::class );
    }
}