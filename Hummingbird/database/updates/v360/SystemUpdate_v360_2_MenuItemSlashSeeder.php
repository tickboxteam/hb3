<?php namespace Hummingbird\Database\Updates;

use Hummingbird\Models\Menuitem;
use Illuminate\Database\Seeder as Seeder;

/**
 * Seeder class will clean the existing menuitems and remove the 
 * trailing slashes for all internal redirects. This aims to 
 * reduce the number of redirects that the system has to do 
 * to deliver the page that was requested. For example: 
 * "/about-us/", is redirected to "/about-us". So 
 * when we have this in the navigation or menu, 
 * then the trailing slash is not needed
 * 
 * @since 3.6.0
 */
class SystemUpdate_v360_2_MenuItemSlashSeeder extends Seeder
{
    /**
     * Run the requested seeds
     * 
     * @return void
     */
    public function run():void
    {
        foreach(Menuitem::orderBy('id', 'ASC')->get() as $Menuitem)
        {
            try {
                $Menuitem->timestamps = false;
                $Menuitem->url = $Menuitem->url;
                $Menuitem->save();                
            }
            catch(\Exception $e) {
                Log::info( 'Editing MenuItem ID: #' . $Menuitem->id);
                Log::error( $e->getTraceAsString() );
            }
        }
    }
}