<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;


/**
 * System update seeder for version v1.1.41
 * 
 * @since 1.1.41
 */
class SystemUpdate_v1141 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run()
    {
        $this->call( SystemUpdate_v1141_ActivityLogUpdateSeeder::class );
    }
}