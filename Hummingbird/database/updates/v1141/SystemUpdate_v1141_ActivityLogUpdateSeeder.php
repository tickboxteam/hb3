<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Activitylog;

/**
 * Seeder class to allow us to translate all activity log entries
 * which are incorrect
 * 
 * @since 1.1.41
 */
class SystemUpdate_v1141_ActivityLogUpdateSeeder extends Seeder {
    public function run() {
        // UPDATE ALL ACTIVITY LOG ENTRIES FOR "CREATE" to "CREATED"
        Activitylog::where('action', 'create')->update(['action' => 'CREATED']);
    }
}
