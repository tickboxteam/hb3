<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * Base seeder class for running all CMS upgrades
 * as part of the 1.7.21 version.
 * 
 * @since 1.7.21
 */
class SystemUpdate_v1721 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( SystemUpdate_v1721_HomepageThemeTemplateCheckSeeder::class );
    }
}