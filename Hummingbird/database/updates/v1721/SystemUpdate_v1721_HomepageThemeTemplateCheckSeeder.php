<?php namespace Hummingbird\Database\Updates;

use View;
use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Libraries\Themes;
use Hummingbird\Models\Page;

/**
 * Seeder will check to see what page template is available in the theme,
 * before changing it. If homepage exists, then update the page
 * 
 */
class SystemUpdate_v1721_HomepageThemeTemplateCheckSeeder extends Seeder {
    public function __construct() {
        $this->theme = Themes::activeTheme();
        
        View::addLocation(base_path()."/themes/public/$this->theme/");
        View::addNamespace('theme', base_path() . '/themes/public/' . $this->theme . '/');
    }

    public function run() {
        if( View::exists('theme::homepage') ):
            $HomePage = Page::findOrFail(1);
            $HomePage->template = 'homepage';
            $HomePage->save();
        endif;
    }
}
