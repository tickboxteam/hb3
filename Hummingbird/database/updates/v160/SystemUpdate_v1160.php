<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;


/**
 * System seeder for the latest version of the CMS
 * 
 * @since 1.6.0
 */
class SystemUpdate_v1160 extends Seeder {
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( SystemUpdate_v1160_1_UserSeederUpdates::class );
    }
}