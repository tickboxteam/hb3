<?php namespace Hummingbird\Database\Updates;

use DB;
use Illuminate\Database\Seeder as Seeder;

/**
 * User Seeder updates to force passwords to be reset
 * 
 * @since 1.6.0
 */
class SystemUpdate_v1160_1_UserSeederUpdates extends Seeder {
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        DB::table('users')->update(['force_password_reset' => 1]);
    }
}