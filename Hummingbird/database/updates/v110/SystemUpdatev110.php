<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;

class SystemUpdatev110 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run()
    {
        $this->call( SystemUpdatev110_PermissionsSeeder::class );
    }
}