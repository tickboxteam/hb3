<?php namespace Hummingbird\Database\Updates;

use Hummingbird\Models\PermissionGroup;

/**
 * Create new permissions for this version.
 * This will allow for new Action based permissions.
 *
 * @package v1.1.0
 */
class SystemUpdatev110_PermissionsSeeder extends \Hummingbird\Database\Seeds\HbSeeder 
{
    public function add_seed_data() {
        $PermissionHandler = new \Hummingbird\Libraries\PermissionHandler;

        // Register new permission for Page Filters
        $PermissionGroup = PermissionGroup::name('Pages')->first();
        $PermissionHandler->registerPermission('page.filterPages', [
            'display_name'  => 'Filter Pages?',
            'description'   => 'Leave empty for to allow access to all pages through content pages.',
            'action'        => 'Tickbox.PageList.GET'
        ], ($PermissionGroup ?: NULL));

        // Register new permission for Category Filters
        $PermissionGroup = PermissionGroup::name('Categories')->first();
        $PermissionHandler->registerPermission('categories.filterCategories', [
            'display_name'  => 'Filter Categories?',
            'description'   => 'Leave empty for to allow access to all categories.',
            'action'        => 'Tickbox.CategoryList.GET'
        ], ($PermissionGroup ?: NULL));
    }
}
