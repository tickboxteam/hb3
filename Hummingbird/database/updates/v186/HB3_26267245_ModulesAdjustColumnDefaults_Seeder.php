<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Module;

/**
 * Adjusting Modules columns to be cleaned
 * 
 * @since 
 */
class HB3_26267245_ModulesAdjustColumnDefaults_Seeder extends Seeder {
    public function run() {
        Module::where('notes', '')->update([
            'notes' => NULL
        ]);

        Module::where('module_data', '')->update([
            'module_data' => NULL
        ]);
    }
}