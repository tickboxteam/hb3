<?php namespace Hummingbird\Database\Updates;

use DB;
use Illuminate\Database\Seeder as Seeder;

/**
 * Cleaning menuitems, whose parent's are set to 0 but should be NULL
 * 
 * @since 
 */
class HB3_26227301_PagesAdjustColumnDefaults_Seeder extends Seeder {
    public function run($arguments = array()) {
        $MenuItemTable = config()->get('database.connections.mysql.prefix') . "menuitems";
        DB::statement("UPDATE `{$MenuItemTable}` SET `parent` = NULL WHERE `parent` = 0");
    }
}