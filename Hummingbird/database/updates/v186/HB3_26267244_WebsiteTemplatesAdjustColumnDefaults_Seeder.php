<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Template;

/**
 * Adjusting Website templates columns to be cleaned
 * 
 * @since 
 */
class HB3_26267244_WebsiteTemplatesAdjustColumnDefaults_Seeder extends Seeder {
    public function run() {
        Template::where('html', '')->update([
            'html' => NULL
        ]);

        Template::where('description', '')->update([
            'description' => NULL
        ]);
    }
}