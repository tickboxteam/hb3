<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * Base seeder class for running all CMS upgrades
 * as part of the 1.8.6 version.
 * 
 * @since 1.8.6
 */
class SystemUpdate_v186 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( HB3_26267246_ModuleTemplatesAdjustColumnDefaults_Seeder::class );
        $this->call( HB3_26267245_ModulesAdjustColumnDefaults_Seeder::class );
        $this->call( HB3_26267244_WebsiteTemplatesAdjustColumnDefaults_Seeder::class );
        $this->call( HB3_26227301_PagesAdjustColumnDefaults_Seeder::class );
    }
}