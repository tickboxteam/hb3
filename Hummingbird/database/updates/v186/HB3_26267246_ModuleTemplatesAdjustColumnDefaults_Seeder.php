<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Moduletemplate;

/**
 * Adjusting Module templates columns to be cleaned
 * 
 * @since 
 */
class HB3_26267246_ModuleTemplatesAdjustColumnDefaults_Seeder extends Seeder {
    public function run() {
        Moduletemplate::where('type', '')->update([
            'type' => NULL
        ]);
    }
}