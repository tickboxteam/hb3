<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;


/**
 * System seeder for the latest version of the CMS
 * 
 * @since x.x.x
 */
class SystemUpdate_v166_Core_route_naming_setups extends Seeder {
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( SystemUpdate_v166_1_remove_page_modules_references::class );
    }
}