<?php namespace Hummingbird\Database\Updates;

use DB;
use Illuminate\Database\Seeder as Seeder;

/**
 * Removing references to "pagemodules" from the CMS
 * 
 * @since 
 */
class SystemUpdate_v166_1_remove_page_modules_references extends Seeder {
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        DB::table('migrations')->where('migration', '2014_09_10_135829_create_pagemodules_table')->delete();
    }
}