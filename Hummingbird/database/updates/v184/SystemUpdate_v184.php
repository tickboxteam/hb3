<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * Base seeder class for running all CMS upgrades
 * as part of the 1.8.4 version.
 * 
 * @since 1.8.4
 */
class SystemUpdate_v184 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( SystemUpdate_v184_1_ActivityLogsClearEmptyUserIDs::class );
    }
}