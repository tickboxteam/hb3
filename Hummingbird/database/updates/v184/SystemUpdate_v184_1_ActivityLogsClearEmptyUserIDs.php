<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Activitylog;

/**
 * Nullable user ID's should be set, rather than set to zero (0)
 * 
 * @since 1.8.4
 */
class SystemUpdate_v184_1_ActivityLogsClearEmptyUserIDs extends Seeder {
    public function run() {
        Activitylog::where('user_id', 0)->update(['user_id' => NULL]);
    }
}