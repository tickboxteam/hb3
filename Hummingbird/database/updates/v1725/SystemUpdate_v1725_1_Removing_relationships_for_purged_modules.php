<?php namespace Hummingbird\Database\Updates;

use DB;
use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Module;


/**
 * A seeder that will help clean up any modules that have taxonomy 
 * relationships still connected. On from this version future
 * links will be removed through a model event
 */
class SystemUpdate_v1725_1_Removing_relationships_for_purged_modules extends Seeder {
    public function run() {
        $ModuleIds = Module::pluck('id')->all();

        // Only run if we have modules
        if( count($ModuleIds) > 0 ):
            DB::table('taxonomy_relationships')->whereNotIn('item_id', $ModuleIds)->where('tax_type', get_class(new Module))->delete();
        endif;
    }
}
