<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * Base seeder class for running all CMS upgrades
 * as part of the 1.7.25 version.
 * 
 * @since 1.7.25
 */
class SystemUpdate_v1725 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( SystemUpdate_v1725_1_Removing_relationships_for_purged_modules::class );
    }
}