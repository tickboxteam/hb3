<?php namespace Hummingbird\Database\Updates;

use DB;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Categories;
use Hummingbird\Models\Media;
use Hummingbird\Models\Menu;
use Hummingbird\Models\Module;
use Hummingbird\Models\Moduletemplate;
use Hummingbird\Models\ObjectPermission;
use Hummingbird\Models\Page;
use Hummingbird\Models\PageVersion;
use Hummingbird\Models\Redirections;
use Hummingbird\Models\RequestNotFound;
use Hummingbird\Models\Role;
use Hummingbird\Models\Tags;
use Hummingbird\Models\Template;
use Hummingbird\Models\User;
use Illuminate\Database\Seeder as Seeder;

/**
 * As we're replacing and updating all namespaces then all references
 * to the old namespaces need to be modified too.
 * 
 */
class SystemUpdate_v170_1_FrameworkUpgradesSeeder extends Seeder {
    public function run() {
        Activitylog::where('type', 'Categories')->update(['type' => get_class(new Categories)]); 
        Activitylog::where('type', 'Media')->update(['type' => get_class(new Media)]);
        Activitylog::where('type', 'Menu')->update(['type' => get_class(new Menu)]);
        Activitylog::where('type', 'Module')->update(['type' => get_class(new Module)]);
        Activitylog::where('type', 'Moduletemplate')->update(['type' => get_class(new Moduletemplate)]);
        Activitylog::where('type', 'Page')->update(['type' => get_class(new Page)]);
        Activitylog::where('type', 'Redirections')->update(['type' => get_class(new Redirections)]);
        Activitylog::where('type', 'RequestNotFound')->update(['type' => get_class(new RequestNotFound)]);
        Activitylog::where('type', 'Role')->update(['type' => get_class(new Role)]);
        Activitylog::where('type', 'Tags')->update(['type' => get_class(new Tags)]);
        Activitylog::where('type', 'Template')->update(['type' => get_class(new Template)]);
        Activitylog::where('type', 'User')->update(['type' => get_class(new User)]);

        ObjectPermission::where('item_type', 'Categories')->update(['item_type' => get_class(new Categories)]);
        ObjectPermission::where('reference_type', 'User')->update(['reference_type' => get_class(new User)]);
        ObjectPermission::where('reference_type', 'Role')->update(['reference_type' => get_class(new Role)]);

        DB::table('taxonomy_relationships')->where('tax_type', 'Media')->update(['tax_type' => get_class(new Media)]);
        DB::table('taxonomy_relationships')->where('tax_type', 'Module')->update(['tax_type' => get_class(new Module)]);
        DB::table('taxonomy_relationships')->where('tax_type', 'Page')->update(['tax_type' => get_class(new Page)]);
        DB::table('taxonomy_relationships')->where('tax_type', 'PageVersion')->update(['tax_type' => get_class(new PageVersion)]);

        DB::table('search')->where('type', 'Page')->update(['type' => get_class(new Page)]);
    }
}
