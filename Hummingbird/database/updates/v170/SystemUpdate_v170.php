<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * Hummingbird3 seeder package for the latest core upgrade - v1.7.0. 
 * Package includes changes to allow us to convert from L42 to L5.
 *
 * First phase is just cleaning up the namespaces to make sure 
 * the transition from L4.2 to L5 is smoother.
 * 
 * @since 1.7.0
 */
class SystemUpdate_v170 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run()
    {
        $this->call( SystemUpdate_v170_1_FrameworkUpgradesSeeder::class );
    }
}