<?php namespace Hummingbird\Database\Updates;

use Artisan;
use Illuminate\Database\Seeder as Seeder;

/**
 * A seeder that will check all taxonomy items and clean their URLs. To avoid
 * incorrect or duplicated permalinks (slug)
 */
class SystemUpdate_v1726_TaxonomyItems_Improving_sluggable_properties extends Seeder {
    public function run() {
        Artisan::call( 'hb3:category-permalinks-clean' );
        Artisan::call( 'hb3:tag-permalinks-clean' );
    }
}
