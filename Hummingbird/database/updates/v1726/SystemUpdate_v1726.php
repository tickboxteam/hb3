<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * Base seeder class for running all CMS upgrades
 * as part of the 1.7.26 version.
 * 
 * @since 1.7.26
 */
class SystemUpdate_v1726 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( SystemUpdate_v1726_TaxonomyItems_Improving_sluggable_properties::class );
    }
}