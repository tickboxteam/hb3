<?php namespace Hummingbird\Database\Updates;

use DB;
use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\User;

/**
 * Seeder fixes relationship issues where the taxonomy relationships for
 * Users is not updated to the full namespace
 * 
 */
class SystemUpdate_v175_1_TaxonomyRelationshipsSeeder extends Seeder {
    public function run() {
        DB::table('taxonomy_relationships')->where('tax_type', 'User')->update(['tax_type' => get_class(new User)]);
    }
}
