<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\MediaCollection;
use Hummingbird\Models\Page;
use Hummingbird\Models\Setting;

/**
 * Seeder fixes relationship issues where the incorrect namespace has been found
 * when viewing activity logs
 * 
 */
class SystemUpdate_v175_2_ActivityLogsNamespaceUpdateSeeder extends Seeder {
    public function run() {
        Activitylog::where('type', 'MediaCollection')->update(['type' => get_class(new MediaCollection)]);
        Activitylog::where('type', 'PageController')->update(['type' => get_class(new Page)]);
        Activitylog::where('type', 'Setting')->update(['type' => get_class(new Setting)]);
    }
}
