<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * 
 * 
 * @since 1.7.5
 */
class SystemUpdate_v175 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run()
    {
        $this->call( SystemUpdate_v175_1_TaxonomyRelationshipsSeeder::class );
        $this->call( SystemUpdate_v175_2_ActivityLogsNamespaceUpdateSeeder::class );
    }
}