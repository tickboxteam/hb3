<?php namespace Hummingbird\Database\Updates;

use Event;
use Hummingbird\Libraries\Themes;
use Hummingbird\Models\Plugin;
use Hummingbird\Models\Setting;
use Illuminate\Database\Seeder as Seeder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Filesystem\Filesystem;

/**
 * System seeder to make sure that Password Strength, which was overhauled, respects 
 * the original password strength but with individual options for customising 
 * the strength to the application
 * 
 * @since v3.5.0
 */
class SystemUpdate_v350_1_PasswordStrengthSeeder extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $Settings = [];

        try {
            $SystemSettings = Setting::where('key', 'system')->firstOrFail();
            $Settings = $SystemSettings->value;
        }
        catch(ModelNotFoundException $e) {
        }

        $Settings['pass_length'] = 8;
        $Settings['letters'] = 'true';
        $Settings['mixed_case'] = 'true';
        $Settings['pass_numbers'] = 'true';
        $Settings['pass_symbols'] = 'true';
        $Settings['pass_compromised'] = 'false';

        // Store these new settings
        Setting::updateOrCreate([
            'key' => 'system'
        ], [
            'value' => serialize( $Settings )
        ]);
    }
}