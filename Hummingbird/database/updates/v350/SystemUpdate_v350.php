<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;

/**
 * Base seeder class for running all CMS upgrades
 * as part of the 3.5.0 version.
 * 
 * @since 3.5.0
 */
class SystemUpdate_v350 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( SystemUpdate_v350_1_PasswordStrengthSeeder::class );
    }
}