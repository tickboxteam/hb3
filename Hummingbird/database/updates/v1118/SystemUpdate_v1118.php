<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;

class SystemUpdate_v1118 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run()
    {
        $this->call( SystemUpdatev1118_PermissionsSeeder::class );
    }
}