<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * Seeding missing permission keys for HB3
 * 
 * @package 1.1.18
 */
class SystemUpdatev1118_PermissionsSeeder extends Seeder
{
    public function __construct() {
        $this->PermissionHandler = new \Hummingbird\Libraries\PermissionHandler;
    }

    public function run() {
        $this->seed_modules_permissions();
    }

    /**
     * Seeding missing module permissions (and grouping)
     */
    public function seed_modules_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'modules',
            'group_name'    => 'Modules & Templates',
            'group_desc'    => 'Manage modules and Templates',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [],
                'templates.create'    => [], 
                'templates.read'      => [], 
                'templates.update'    => [], 
                'templates.delete'    => [],
                'templates.lock'      => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
