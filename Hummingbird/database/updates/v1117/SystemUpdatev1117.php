<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;

/**
 * Base class for calling all upgrades for latest CMS version
 *
 * @package v1.1.17
 */
class SystemUpdatev1117 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run()
    {
        $this->call( SystemUpdatev1117_SEO_Overhaul_Updates::class );
    }
}