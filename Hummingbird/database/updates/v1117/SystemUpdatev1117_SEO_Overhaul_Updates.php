<?php namespace Hummingbird\Database\Updates;
use Hummingbird\Models\MetaData;

/**
 * Remove all meta keywords from the databse
 *
 * @package v1.1.17
 */
class SystemUpdatev1117_SEO_Overhaul_Updates extends \Hummingbird\Database\Seeds\HbSeeder {
    public function add_seed_data() {
        $this->removeMetaKeywords();
    }

    protected function removeMetaKeywords() {
        MetaData::where('key', 'meta_keywords')->delete();
    }
}
