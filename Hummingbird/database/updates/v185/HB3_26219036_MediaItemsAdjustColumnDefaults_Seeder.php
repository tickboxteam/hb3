<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Media;

/**
 * Adjusting Media item columns to be cleaned
 * 
 * @since 
 */
class HB3_26219036_MediaItemsAdjustColumnDefaults_Seeder extends Seeder {
    public function run() {
        Media::where('caption', '')->update([
            'caption' => NULL
        ]);

        Media::where('alt', '')->update([
            'alt' => NULL
        ]);

        Media::where('description', '')->update([
            'description' => NULL
        ]);
    }
}