<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Redirections;
/**
 * When we run this seeder, we want to reset all of the fields to be
 * NULL. These fields are: Comments, Category
 * 
 * @since 
 */
class HB3_26212119_RedirectsAdjustColumnDefaults_Seeder extends Seeder {
    public function run() {
        Redirections::withTrashed()->whereNotNull('comments')->where('comments', '')->update([
            'comments' => NULL
        ]);

        Redirections::withTrashed()->whereNotNull('category')->where('category', '')->update([
            'category' => NULL
        ]);
    }
}