<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Categories;
use Hummingbird\Models\Tags;

/**
 * Adjusting the taxonomy items to have default NULL values.
 * 
 * @since 
 */
class HB3_26212121_TaxonomyAdjustColumnDefaults_Seeder extends Seeder {
    public function run() {
        Categories::withTrashed()->where('image', '')->update([
            'image' => NULL
        ]);

        Categories::withTrashed()->where('description', '')->update([
            'description' => NULL
        ]);

        Tags::withTrashed()->where('image', '')->update([
            'image' => NULL
        ]);

        Tags::withTrashed()->where('description', '')->update([
            'description' => NULL
        ]);
    }
}