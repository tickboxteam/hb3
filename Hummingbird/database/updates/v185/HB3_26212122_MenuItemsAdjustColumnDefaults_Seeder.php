<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Menuitem;

/**
 * 
 * 
 * @since 
 */
class HB3_26212122_MenuItemsAdjustColumnDefaults_Seeder extends Seeder {
    public function run() {
        Menuitem::where('menu_id', '<=', 0)->orWhere('menu_id', '')->update([
            'menu_id' => NULL
        ]);

        Menuitem::where('data', '')->update([
            'data' => NULL
        ]);

        Menuitem::where('parent', 0)->update([
            'parent' => NULL
        ]);
    }
}