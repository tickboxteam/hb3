<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Menu;

/**
 * Adjusting Menu columns to be cleaned
 * 
 * @since 
 */
class HB3_26219080_MenusAdjustColumnDefaults_Seeder extends Seeder {
    public function run() {
        Menu::where('description', '')->update([
            'description' => NULL
        ]);
    }
}