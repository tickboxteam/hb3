<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Page;

/**
 * 
 * 
 * @since 
 */
class HB3_26211481_PageCommentsRequiresDefaultValue_Seeder extends Seeder {
    public function run() {
        Page::withTrashed()->where('enable_comments', '!=', 1)->update([
            'enable_comments' => NULL
        ]);
    }
}