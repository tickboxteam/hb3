<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\MediaCollection;

/**
 * 
 * 
 * @since 
 */
class HB3_26219034_MediaCollectionsAdjustColumnDefaults_Seeder extends Seeder {
    public function run() {
        MediaCollection::where('description', '')->update([
            'description' => NULL
        ]);
    }
}