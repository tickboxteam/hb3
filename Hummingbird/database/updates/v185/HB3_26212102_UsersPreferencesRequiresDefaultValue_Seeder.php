<?php namespace Hummingbird\Database\Updates;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\User;

/**
 * 
 * 
 * @since 
 */
class HB3_26212102_UsersPreferencesRequiresDefaultValue_Seeder extends Seeder {
    public function run() {
        User::withTrashed()->where('preferences', '')->update([
            'preferences' => NULL
        ]);
    }
}