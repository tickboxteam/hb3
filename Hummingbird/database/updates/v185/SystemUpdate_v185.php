<?php namespace Hummingbird\Database\Updates;
use Illuminate\Database\Seeder as Seeder;

/**
 * Base seeder class for running all CMS upgrades
 * as part of the 1.8.5 version.
 * 
 * @since 1.8.5
 */
class SystemUpdate_v185 extends Seeder
{
    /**
     * Run the requested seeds
     * @return Void
     */
    public function run() {
        $this->call( HB3_26211481_PageCommentsRequiresDefaultValue_Seeder::class );
        $this->call( HB3_26212102_UsersPreferencesRequiresDefaultValue_Seeder::class );
        $this->call( HB3_26212119_RedirectsAdjustColumnDefaults_Seeder::class );
        $this->call( HB3_26212121_TaxonomyAdjustColumnDefaults_Seeder::class );
        $this->call( HB3_26212122_MenuItemsAdjustColumnDefaults_Seeder::class );
        $this->call( HB3_26219034_MediaCollectionsAdjustColumnDefaults_Seeder::class );
        $this->call( HB3_26219036_MediaItemsAdjustColumnDefaults_Seeder::class );
        $this->call( HB3_26219080_MenusAdjustColumnDefaults_Seeder::class );
    }
}