<?php namespace Hummingbird\Database\DummySeeds;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Categories;

/**
 * Dummy seeder class for inserting categories
 *
 * @since   1.1.30
 * @version 1.0.0
 */
class DummyCategoriesSeeder extends Seeder {
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $CategoryModel = new Categories;

        DB::table( $CategoryModel->getTable() )->update(['deleted_at' => NULL]);
        
        for( $i = 1; $i <= 4; $i++) {
            Categories::updateOrCreate(['name' => 'Test category #' . $i, 'type' => 'category'], [
                'display_name'  => 'Test category #' . $i,
                'slug'          => str_slug('Test category #' . $i),
                'parent'        => ( $i > 1 ) ? ($i-1) : NULL,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ]);
        }
    }
}