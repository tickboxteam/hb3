<?php namespace Hummingbird\Database\DummySeeds;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Page;

class DummyPagesSeeder extends Seeder {
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        Page::updateOrCreate(['title' => 'Dummy Seed: Page'], [
            'summary' => 'Lorem ipsum dolor exercitation aute enim ea amet velit in mollit est enim in veniam non irure labore.',
            'content' => 'Do in est quis occaecat ea ullamco irure ut labore in fugiat non ut tempor do est non enim et in occaecat elit voluptate consectetur exercitation in magna laboris aliquip voluptate et culpa mollit ex deserunt aliquip commodo laborum occaecat et in sint do dolore ut consequat elit et esse pariatur ad dolore pariatur nulla duis nostrud officia voluptate voluptate officia officia occaecat in cupidatat in irure aute dolore nostrud sunt magna est ex exercitation consectetur in ad ut sunt consectetur eu ea ut dolor sit irure cillum elit adipisicing cillum do nostrud minim ad duis dolore aliquip in minim occaecat fugiat laboris minim qui pariatur duis voluptate ea ad est ea amet voluptate sint occaecat irure in in reprehenderit dolore ut ad cupidatat ut adipisicing amet dolor veniam proident consectetur mollit qui amet in ut laborum quis esse sint sunt dolore ut ex tempor magna anim labore voluptate elit pariatur ullamco fugiat officia in proident ut est aute ut reprehenderit incididunt ut labore non do dolore nostrud veniam commodo ad consequat do ut quis magna anim enim nulla voluptate voluptate cillum dolore exercitation sunt enim do do magna amet qui nostrud cupidatat ut voluptate ut pariatur laboris enim esse eu ut eu sint occaecat cillum enim commodo nisi sed exercitation sunt dolor occaecat in. Do dolor est proident incididunt culpa irure ullamco minim sunt fugiat est ut dolor irure aliqua qui est ut.',
            'url' => str_slug( 'Dummy Seed: Page' ),
            'parentpage_id' => 1,
            'status' => 'public',
            'username' => '', //FIXME: This doesn't feel right because of the issues with default values
            'password' => '', //FIXME: This doesn't feel right because of the issues with default values
            'search_image' => '', //FIXME: This doesn't feel right because of the issues with default values
            'featured_image' => '', //FIXME: This doesn't feel right because of the issues with default values
            'custom_menu_name' => '', //FIXME: This doesn't feel right because of the issues with default values
            'position' => 0 //FIXME: This doesn't feel right because of the issues with default values
        ]);
    }
}