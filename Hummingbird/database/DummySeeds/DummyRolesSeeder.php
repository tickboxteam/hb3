<?php namespace Hummingbird\Database\DummySeeds;

use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Role;

class DummyRolesSeeder extends Seeder {
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $SuperAdmin = Role::updateOrCreate(['name' => 'superadmin'], [
            'hidden'    => 1,
            'locked'    => 1
        ]);

        Role::updateOrCreate(['name' => 'admin'], [
            'parentrole_id' => $SuperAdmin->id
        ]);
    }
}