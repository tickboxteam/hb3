<?php namespace Hummingbird\Database\DummySeeds;

use Crypt;
use Illuminate\Database\Seeder as Seeder;
use Hummingbird\Models\Role;
use Hummingbird\Models\User;

class DummyUsersSeeder extends Seeder {
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name', 'superadmin')->firstOrFail();
        $role2 = Role::where('name', 'admin')->firstOrFail();

        User::updateOrCreate(['email' => 'super@admin.com'], [
            'username'          => 'super-admin',
            'email'             => 'super@admin.com',
            'password'          => Crypt::encrypt('password'),
            'name'              => 'Super Admin',
            'firstname'         => 'Super',
            'surname'           => 'Admin',
            'status'            => 'active',
            'remember_token'    => str_random(60),
            'role_id'           => $role->id,
            'is_admin'          => 1
        ]);

        User::updateOrCreate(['email' => 'admin@admin.com'], [
            'username'          => 'website-admin',
            'email'             => 'admin@admin.com',
            'password'          => Crypt::encrypt('password'),
            'name'              => 'Website Admin',
            'firstname'         => 'Website',
            'surname'           => 'Admin',
            'status'            => 'active',
            'remember_token'    => str_random(60),
            'role_id'           => $role2->id,
            'is_admin'          => 1
        ]);
    }
}