<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedirectionStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('redirections_stats'))
		{
	        Schema::create('redirections_stats', function($table)
	        {
	            $table->increments('id');
	            $table->integer('redirect_id');
	            $table->datetime('visited_at')->nullable();
	            $table->string('ip_address');
	            $table->string('referrer');
	        });
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('redirections_stats'))
		{
			Schema::drop('redirections_stats');
		}
	}

}
