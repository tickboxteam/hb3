<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Hummingbird\Database\HummingbirdMigration;

class RedirectsUniqueSoftDelete extends HummingbirdMigration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('redirections')) {
			Schema::table('redirections', function($table) {
				if( $this->tableHasIndex('redirections', 'from', 'unique') ) {
					$table->dropIndex('redirections_from_unique');
				}

				if( !$this->tableHasIndex('redirections', 'from_deleted_at', 'unique') ) {
					$table->unique(array('from', 'deleted_at'));
				}
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('redirections', function($table) {
			if( $this->tableHasIndex('redirections', 'from_deleted_at', 'unique') ) {
				$table->dropIndex('redirections_from_deleted_at_unique');
			}

			if( !$this->tableHasIndex('redirections', 'from', 'unique') ) {
				$table->unique('from');
			}
		});
	}

}
