<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableToAddForcePasswordResetColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('users'))
		{
			if(!Schema::hasColumn('users', 'force_password_reset'))
			{
				Schema::table('users', function($table)
				{
				    $table->tinyInteger('force_password_reset')->nullable()->after('last_login');
				});
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('users'))
		{
			if(Schema::hasColumn('users', 'force_password_reset'))
			{
				Schema::table('users', function($table)
				{
				    $table->dropColumn('force_password_reset');
				});
			}
		}
	}

}
