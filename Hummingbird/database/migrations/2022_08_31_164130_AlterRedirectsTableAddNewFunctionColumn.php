<?php

use Hummingbird\Database\HummingbirdMigration;
use Illuminate\Database\Schema\Blueprint;

class AlterRedirectsTableAddNewFunctionColumn extends HummingbirdMigration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if( Schema::hasTable('redirections') && !Schema::hasColumn('redirections', 'function') ) {
			Schema::table('redirections', function($table) {
				$table->string('function')->after('category')->nullable();

				// Add indexes to redirections - function
				if( !$this->tableHasIndex('redirections', 'function', 'index') ) {
					$table->index('function');
				}
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		if( Schema::hasTable('redirections') && Schema::hasColumn('redirections', 'function') ) {
			Schema::table('redirections', function($table) {
				if( $this->tableHasIndex('redirections', 'function', 'index') ) {
					$table->dropIndex( "redirections_function_index" );
				}

				$table->dropColumn('function');
			});
		}
	}
}
