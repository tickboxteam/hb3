<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersToHaveUUIDs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('users', function($table) {
            if( !Schema::hasColumn( "users", "uuid" ) ) {
                $table->text('uuid')->after('id')->nullable();
            }
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('users', function($table) {
            if( Schema::hasColumn( "users", "uuid" ) ) {
                $table->dropColumn('uuid');
            }
        });
	}

}
