<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFailedJobsTableToIncludeUUID extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Schema::hasTable('failed_jobs') && !Schema::hasColumn('failed_jobs', 'uuid') ):
            Schema::table('failed_jobs', function (Blueprint $table) {
                $table->string('uuid')->after('id')->nullable()->unique();
            });

            // Any jobs in the system, lets update them
            DB::table('failed_jobs')->whereNull('uuid')->cursor()->each(function ($job) {
                DB::table('failed_jobs')
                    ->where('id', $job->id)
                    ->update(['uuid' => (string) Illuminate\Support\Str::uuid()]);
            });
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( Schema::hasTable('failed_jobs') && Schema::hasColumn('failed_jobs', 'uuid') ):
            Schema::table('failed_jobs', function($table) {
                $table->dropColumn('uuid');
            });
        endif;
    }
}
