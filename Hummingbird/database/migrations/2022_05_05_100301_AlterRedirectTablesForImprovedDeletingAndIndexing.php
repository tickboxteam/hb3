<?php

use Hummingbird\Database\HummingbirdMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRedirectTablesForImprovedDeletingAndIndexing extends HummingbirdMigration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('redirections') ) {
			Schema::table('redirections', function($table) {
				// Remove current indexes from primary redirects table - redirections_from_deleted_at_unique
				if( $this->tableHasIndex('redirections', 'from_deleted_at', 'unique') ) {
					$table->dropIndex('redirections_from_deleted_at_unique');
				}

				// Add new indexes to redirects table - from, to, type, category
				foreach( ['from', 'to', 'type', 'category'] as $NewIndex ) {
					if( !$this->tableHasIndex('redirections', $NewIndex, 'index') ) {
						$table->index( $NewIndex );
					}
				}

				// Add new unique index to redirects table - id, from, deleted_at
				if( !$this->tableHasIndex('redirections', 'id_from_deleted_at', 'unique') ) {
					$table->unique( ['id', 'from', 'deleted_at'] );
				}
			});
		}

		if( Schema::hasTable('redirections_stats') ) {
			$NewTableName = DB::getTablePrefix() . 'redirections_stats';
			DB::statement("ALTER TABLE `{$NewTableName}` CHANGE `redirect_id` `redirect_id` INT(11) UNSIGNED NOT NULL");

			Schema::table('redirections_stats', function($table) {
				// Remove column from redirections_stats - ip_address
				if( Schema::hasColumn('redirections_stats', 'ip_address') ) {
					$table->dropColumn('ip_address');	
				}

				// Add indexes to redirections_stats - redirect_id
				if( !$this->tableHasIndex('redirections_stats', 'redirect_id', 'index') ) {
					$table->index('redirect_id');
				}
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		if( Schema::hasTable('redirections_stats') ) {
			Schema::table('redirections_stats', function($table) {
				$NewTableName = DB::getTablePrefix() . 'redirections_stats';
				DB::statement("ALTER TABLE `{$NewTableName}` CHANGE `redirect_id` `redirect_id` INT(11) NOT NULL");

				// Remove indexes from redirection_stats = redirect_id
				if( $this->tableHasIndex('redirections_stats', 'redirect_id', 'index') ) {
					$table->dropIndex('redirections_stats_redirect_id_index');
				}

				// Add column to redirection_stats = ip_address
				if( !Schema::hasColumn('redirections_stats', 'ip_address') ) {
					$table->string('ip_address')->after('visited_at');
				}
			});
		}

		if( Schema::hasTable('redirections') ) {
			Schema::table('redirections', function($table) {
				// Drop new unique index to redirects table - id, from, deleted_at
				if( $this->tableHasIndex('redirections', 'id_from_deleted_at', 'unique') ) {
					$table->dropIndex( 'redirections_id_from_deleted_at_unique' );
				}

				// Drop new indexes to redirects table - from, to, type, category
				foreach( ['from', 'to', 'type', 'category'] as $NewIndex ) {
					if( $this->tableHasIndex('redirections', $NewIndex, 'index') ) {
						$table->dropIndex( "redirections_{$NewIndex}_index" );
					}
				}

				// Add old indexes back to primary redirects table - redirections_from_deleted_at_unique
				if( !$this->tableHasIndex('redirections', 'from_deleted_at', 'unique') ) {
					$table->unique( ['id', 'from', 'deleted_at'] );
				}
			});
		}
	}
}
