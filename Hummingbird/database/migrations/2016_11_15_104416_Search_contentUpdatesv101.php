<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Hummingbird\Models\Searcher;

class SearchContentUpdatesv101 extends Migration
{
	public $table = 'search';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable($this->table))
		{
			$search_items = Searcher::all();

			if(count($search_items) > 0)
			{
				foreach($search_items as $item)
				{
					$item->content = $item->content;
					$item->summary = $item->summary;
					$item->save();
				}
			}
		}
	}

	public function down(){}
}
