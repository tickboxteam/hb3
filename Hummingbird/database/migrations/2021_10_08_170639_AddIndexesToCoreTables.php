<?php

use Hummingbird\Database\HummingbirdMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToCoreTables extends HummingbirdMigration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('activitylogs') && !$this->tableHasIndex('activitylogs', 'user_id', 'index') ) {
			Schema::table('activitylogs', function($table) {
				$table->index('user_id');
			});
		}

		if( Schema::hasTable('modules') && !$this->tableHasIndex('modules', 'moduletemplate_id', 'index') ) {
			Schema::table('modules', function($table) {
	        	$table->index('moduletemplate_id');
			});
		}

		if( Schema::hasTable('pages') && !$this->tableHasIndex('pages', 'user_id', 'index') ) {
			Schema::table('pages', function($table) {
	        	$table->index('user_id');
			});
		}

		if( Schema::hasTable('pages_versions') && !$this->tableHasIndex('pages_versions', 'user_id', 'index') ) {
			Schema::table('pages_versions', function($table) {
	        	$table->index('user_id');
			});
		}

		if( Schema::hasTable('users') && !$this->tableHasIndex('users', 'role_id', 'index') ) {
			Schema::table('users', function($table) {
	        	$table->index('role_id');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{	
		if( Schema::hasTable('activitylogs') && $this->tableHasIndex('activitylogs', 'user_id', 'index') ) {
			Schema::table('activitylogs', function($table) {
	        	$table->dropIndex('activitylogs_user_id_index');
			});
		}

		if( Schema::hasTable('modules') && $this->tableHasIndex('modules', 'moduletemplate_id', 'index') ) {
			Schema::table('modules', function($table) {
	        	$table->dropIndex('modules_moduletemplate_id_index');
			});
		}

		if( Schema::hasTable('pages') && $this->tableHasIndex('pages', 'user_id', 'index') ) {
			Schema::table('pages', function($table) {
	        	$table->dropIndex('pages_user_id_index');
			});
		}

		if( Schema::hasTable('pages_versions') && $this->tableHasIndex('pages_versions', 'user_id', 'index') ) {
			Schema::table('pages_versions', function($table) {
	        	$table->dropIndex('pages_versions_user_id_index');
			});
		}

		if( Schema::hasTable('users') && $this->tableHasIndex('users', 'role_id', 'index') ) {
			Schema::table('users', function($table) {
	        	$table->dropIndex('users_role_id_index');
			});
		}
	}

}
