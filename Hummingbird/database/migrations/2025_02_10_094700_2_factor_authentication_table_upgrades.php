<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if( Schema::hasTable('users') ):
            Schema::table('users', function (Blueprint $table) {
                if( !Schema::hasColumn('users', 'two_factor_secret') ):
                    $table->text('two_factor_secret')->after('password')->nullable();
                endif;

                if( !Schema::hasColumn('users', 'two_factor_recovery_codes') ):
                    $table->text('two_factor_recovery_codes')->after('two_factor_secret')->nullable();
                endif;

                if( !Schema::hasColumn('users', 'two_factor_confirmed_at') ):
                    $table->timestamp('two_factor_confirmed_at')->after('two_factor_recovery_codes')->nullable();
                endif;
            });
        endif;
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if( Schema::hasTable('users') ):
            Schema::table('users', function (Blueprint $table) {
                if( Schema::hasColumn('users', 'two_factor_secret') ):
                    $table->dropColumn('two_factor_secret');
                endif;

                if( Schema::hasColumn('users', 'two_factor_recovery_codes') ):
                    $table->dropColumn('two_factor_recovery_codes');
                endif;

                if( Schema::hasColumn('users', 'two_factor_confirmed_at') ):
                    $table->dropColumn('two_factor_confirmed_at');
                endif;
            });
        endif;
    }
};
