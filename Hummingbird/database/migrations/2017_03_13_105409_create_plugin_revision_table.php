<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePluginRevisionTable extends Migration
{
	public $table = 'system_plugin_history';
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable($this->table))
		{
	        Schema::create($this->table, function($table)
	        {
	            $table->increments('id');
	            $table->string('namespace'); 	// Vendor/PackageName
	            $table->string('type'); 		// Migration/Seed/Comment
	            $table->string('version');		// 1.0.0
	            $table->string('note');
	            $table->dateTime('created_at')->nullable();
	        });
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->table);
	}
}
