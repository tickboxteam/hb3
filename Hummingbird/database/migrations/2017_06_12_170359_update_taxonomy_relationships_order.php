<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTaxonomyRelationshipsOrder extends Migration
{
	public $table = 'taxonomy_relationships';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable($this->table) && !Schema::hasColumn($this->table, 'SortOrder'))
		{
			Schema::table($this->table, function($table)
			{
				$table->integer('SortOrder')->after('item_id')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable($this->table) && Schema::hasColumn($this->table, 'SortOrder'))
		{
			Schema::table($this->table, function($table)
			{
				$table->dropColumn('SortOrder');
			});
		}
	}
}
