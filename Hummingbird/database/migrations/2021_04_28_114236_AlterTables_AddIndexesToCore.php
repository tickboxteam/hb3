<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesAddIndexesToCore extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('menuitems')) {
			Schema::table('menuitems', function ($table) {
				$table->index('menu_id');
				$table->index('parent');
				$table->index('order');
			});
		}

		if (Schema::hasTable('meta_content')) {
			if( Schema::hasColumn('meta_content', 'meta_id') ) {
				Schema::table('meta_content', function(Blueprint $table) {
					$table->dropColumn('meta_id');
				});
			}

			if( Schema::hasColumn('meta_content', 'key') ) {
				$NewTableName = DB::getTablePrefix() . "meta_content";
				DB::statement("ALTER TABLE `{$NewTableName}` MODIFY `key` VARCHAR(255)");
			}

			Schema::table('meta_content', function ($table) {
				$table->index('item_id');
				$table->index('item_type');
				$table->index('key');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('menuitems')) {
			Schema::table('menuitems', function ($table) {
				$table->dropIndex('menuitems_menu_id_index');
				$table->dropIndex('menuitems_parent_index');
				$table->dropIndex('menuitems_order_index');
			});
		}

		if (Schema::hasTable('meta_content')) {
			Schema::table('meta_content', function ($table) {
				$table->dropIndex('meta_content_item_id_index');
				$table->dropIndex('meta_content_item_type_index');
				$table->dropIndex('meta_content_key_index');
			});

			if( Schema::hasColumn('meta_content', 'key') ) {
				$NewTableName = DB::getTablePrefix() . "meta_content";
				DB::statement("ALTER TABLE `{$NewTableName}` MODIFY `key` TEXT");
			}

			if( !Schema::hasColumn('meta_content', 'meta_id') ) {
				Schema::table('meta_content', function(Blueprint $table) {
					$table->integer('meta_id');
				});
			}
		}
	}

}
