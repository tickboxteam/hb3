<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTaxonomyTableAddDisplayNameFieldV115 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('taxonomy') && !Schema::hasColumn('taxonomy', 'display_name') ) {
			Schema::table('taxonomy', function(Blueprint $table) {
				$table->string('display_name')->after('name')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('taxonomy') && Schema::hasColumn('taxonomy', 'display_name')) {
			Schema::table('taxonomy', function($table) {
				$table->dropColumn('display_name');
			});
		}
	}
}
