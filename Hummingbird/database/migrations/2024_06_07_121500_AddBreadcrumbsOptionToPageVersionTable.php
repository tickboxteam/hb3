<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBreadcrumbsOptionToPageVersionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('pages_versions') && !Schema::hasColumn('pages_versions', 'breadcrumb') ) {
			Schema::table('pages_versions', function($table) {
			    $table->tinyInteger('breadcrumb')->nullable()->after('show_in_nav');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('pages_versions') && Schema::hasColumn('pages_versions', 'breadcrumb') ) {
			Schema::table('pages_versions', function($table) {
			    $table->dropColumn('breadcrumb');
			});
		}
	}

}
