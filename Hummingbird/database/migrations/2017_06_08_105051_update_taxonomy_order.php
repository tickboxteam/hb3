<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTaxonomyOrder extends Migration
{
	public $table = 'taxonomy';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable($this->table) && !Schema::hasColumn($this->table, 'SortOrder'))
		{
			Schema::table($this->table, function($table)
			{
				$table->integer('SortOrder')->after('parent')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable($this->table) && Schema::hasColumn($this->table, 'SortOrder'))
		{
			Schema::table($this->table, function($table)
			{
				$table->dropColumn('SortOrder');
			});
		}
	}
}
