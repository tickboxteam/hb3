<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SearchTableFulltext21112017 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$table_name = Config::get('database.connections.mysql.prefix') . "search";

		if(Schema::hasTable('search'))
		{
			Schema::table('search', function($table) use($table_name) {
				// now to check the indexes - safely
				$sm = Schema::getConnection()->getDoctrineSchemaManager();
				$indexesFound = $sm->listTableIndexes($table_name);

				if(array_key_exists("title", $indexesFound))
					$table->dropIndex("title");

				if(array_key_exists("url", $indexesFound))
					$table->dropIndex("url");

				if(array_key_exists("content", $indexesFound))
					$table->dropIndex("content");

				if(array_key_exists("summary", $indexesFound))
					$table->dropIndex("summary");

				if(array_key_exists("title_2", $indexesFound))
					$table->dropIndex("title_2");
			});

			DB::statement("ALTER TABLE $table_name ENGINE = MyISAM");
			DB::statement("ALTER TABLE $table_name ADD FULLTEXT title(title)");
			DB::statement("ALTER TABLE $table_name ADD FULLTEXT url(url)");
			DB::statement("ALTER TABLE $table_name ADD FULLTEXT content(content)");
			DB::statement("ALTER TABLE $table_name ADD FULLTEXT summary(summary)");
			DB::statement("ALTER TABLE $table_name ADD FULLTEXT title_2(title,url,content,summary)");
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$table_name = Config::get('database.connections.mysql.prefix') . "search";

		if(Schema::hasTable('search'))
		{
			Schema::table('search', function($table) use($table_name) {
				// now to check the indexes - safely
				$sm = Schema::getConnection()->getDoctrineSchemaManager();
				$indexesFound = $sm->listTableIndexes($table_name);

				if(array_key_exists("title", $indexesFound))
					$table->dropIndex("title");

				if(array_key_exists("url", $indexesFound))
					$table->dropIndex("url");

				if(array_key_exists("content", $indexesFound))
					$table->dropIndex("content");

				if(array_key_exists("summary", $indexesFound))
					$table->dropIndex("summary");

				if(array_key_exists("title_2", $indexesFound))
					$table->dropIndex("title_2");
			});
		}

		DB::statement("ALTER TABLE $table_name ENGINE = InnoDB");
	}

}
