<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToErrorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('errors')) {
			Schema::table('errors', function ($table) {
				$table->index('url');
				$table->index('accessed');
				$table->index('type');
				$table->index('created_at');
				$table->index('updated_at');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('errors')) {
			Schema::table('errors', function ($table) {
				$table->dropIndex('errors_url_index');
				$table->dropIndex('errors_accessed_index');
				$table->dropIndex('errors_type_index');
				$table->dropIndex('errors_created_at_index');
				$table->dropIndex('errors_updated_at_index');
			});
		}
	}

}
