<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableToStoreAvatar extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('users') && !Schema::hasColumn('users', 'avatar') ) {
			Schema::table('users', function($table) {
				$table->text('avatar')->after('email')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('users') && Schema::hasColumn('users', 'avatar') ) {
			Schema::table('users', function($table) {
				$table->dropColumn('avatar');
			});
		}
	}

}
