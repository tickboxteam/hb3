<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HB3PermissionsUpgrade extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Rename "permissions" to "permissions_users"
		Schema::rename('permissions', 'permissions_users');

		// Creates the permissions table
		Schema::create('permissions', function ($table) {
		    $table->increments('id')->unsigned();
		    $table->string('name')->unique();
		    $table->string('display_name');
		    $table->integer('group_id')->unsigned()->nullable();
		    $table->text('description')->nullable();
		    $table->string('action')->nullable();
		    $table->timestamps();
		});

		// Creates the permission groups table
		Schema::create('groups_permissions', function ($table) {
		    $table->increments('id')->unsigned();
		    $table->string('name');
		    $table->text('description')->nullable();
		    $table->timestamps();
		});

		// Migrate all existing permissions
		$permissions_query = DB::table('perm_types_actions_xref')->leftJoin('permission_types', function($type_join) {
			$type_join->on('perm_types_actions_xref.perm_type', '=', 'permission_types.id');
		})->leftJoin('permission_actions', function($action_join) {
			$action_join->on('perm_types_actions_xref.perm_action', '=', 'permission_actions.id');
		})->get();

		$new_permissions = [];

		foreach( $permissions_query as $permission ):
			$PermissionGroup = PermissionGroup::firstOrCreate(array('name' => $permission->label));

			// Save the permission group
			if( null !== $PermissionGroup->id ) {
				$PermissionGroup->description = $permission->description;
				$PermissionGroup->save();
			}

			// Create permissions
			$new_permissions[] = array(
				'id' 			=> $permission->perm_id,
				'name' 			=> str_slug($permission->type) . "." . str_slug($permission->action),
				'display_name' 	=> $permission->action,
				'group_id'		=> $PermissionGroup->id
			);
		endforeach;

		if( count($new_permissions) > 0 ) {
			DB::table('permissions')->insert($new_permissions);
		}

		// Change column types for foreign keys
		$permissions_users_table = Config::get('database.connections.mysql.prefix') . "permissions_users";
		DB::statement("ALTER TABLE $permissions_users_table MODIFY COLUMN permission_id INT(10) unsigned NULL");
		DB::statement("ALTER TABLE $permissions_users_table MODIFY COLUMN user_id INT(10) unsigned NULL");

		// Alter "permissions_users" to include the following:
		Schema::table('permissions_users', function($table) {
			$table->increments('id')->unsigned();

			$table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
			$table->foreign('user_id')->references('id')->on('users');
		});

		// Re-order columns for "permission_users"
		DB::statement("ALTER TABLE $permissions_users_table MODIFY COLUMN id INT(10) NOT NULL AUTO_INCREMENT FIRST");

		// Creates the assigned_roles (Many-to-Many relation) table
		Schema::create('assigned_roles', function ($table) {
		    $table->increments('id')->unsigned();
		    $table->integer('user_id')->unsigned();
		    $table->integer('role_id')->unsigned();

		    $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
		    $table->foreign('role_id')->references('id')->on('roles');
		});

		// Change column types for foreign keys
		$permission_role_table = Config::get('database.connections.mysql.prefix') . "permission_role";
		DB::statement("ALTER TABLE $permission_role_table MODIFY COLUMN permission_id INT(10) unsigned NULL");
		DB::statement("ALTER TABLE $permission_role_table MODIFY COLUMN role_id INT(10) unsigned NULL");

		// Creates the permission_role (Many-to-Many relation) table
		Schema::table('permission_role', function ($table) {
		    $table->increments('id')->unsigned();

		    $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade'); // assumes a users table
		    $table->foreign('role_id')->references('id')->on('roles');
		});

		// Re-order columns for "permission_users"
		DB::statement("ALTER TABLE $permission_role_table MODIFY COLUMN id INT(10) NOT NULL AUTO_INCREMENT FIRST");

		// Drop "permission_actions"
        if (Schema::hasTable('permission_actions')) {
            Schema::drop('permission_actions');
        }

		// Drop "permission_types"
        if (Schema::hasTable('permission_types')) {
            Schema::drop('permission_types');
        }

		// Drop "perm_types_actions_xref"
        if (Schema::hasTable('perm_types_actions_xref')) {
            Schema::drop('perm_types_actions_xref');
        }

		// Drop "permission_overrides"
        if (Schema::hasTable('permission_overrides')) {
            Schema::drop('permission_overrides');
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {}
}
