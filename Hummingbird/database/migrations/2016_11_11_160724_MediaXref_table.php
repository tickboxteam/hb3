<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MediaXrefTable extends Migration
{
	public $table = 'media_xref';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable($this->table))
		{
	        Schema::create($this->table, function($table)
	        {
	            $table->integer('media_id');
	            $table->integer('item_id');
	            $table->string('item_type');
	            $table->integer('position');
	            $table->string('media_type');
	        });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
