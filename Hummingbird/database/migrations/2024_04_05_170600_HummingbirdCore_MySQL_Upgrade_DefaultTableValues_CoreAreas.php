<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HummingbirdCoreMySQLUpgradeDefaultTableValuesCoreAreas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('moduletemplates') ) {
			$NewTableName = DB::getTablePrefix() . "moduletemplates";
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `type` `type` VARCHAR(255) NULL DEFAULT NULL;");
		}

		if( Schema::hasTable('modules') ) {
			$NewTableName = DB::getTablePrefix() . "modules";

			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `notes` `notes` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `module_data` `module_data` MEDIUMTEXT NULL DEFAULT NULL");
		}

		if( Schema::hasTable('templates') ) {
			$NewTableName = DB::getTablePrefix() . "templates";

			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `html` `html` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `description` `description` MEDIUMTEXT NULL DEFAULT NULL");
		}

		if( Schema::hasTable('pages') ) {
			$NewTableName = DB::getTablePrefix() . "pages";
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `url` `url` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `permalink` `permalink` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `content` `content` LONGTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `username` `username` VARCHAR(255) NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `password` `password` VARCHAR(255) NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `featured_image` `featured_image` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `search_image` `search_image` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `custom_menu_name` `custom_menu_name` VARCHAR(255) NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `position` `position` INT(11) NULL DEFAULT NULL");
		}

		if( Schema::hasTable('pages_versions') ) {
			$NewTableName = DB::getTablePrefix() . "pages_versions";
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `url` `url` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `permalink` `permalink` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `content` `content` LONGTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `username` `username` VARCHAR(255) NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `password` `password` VARCHAR(255) NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `featured_image` `featured_image` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `search_image` `search_image` MEDIUMTEXT NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `custom_menu_name` `custom_menu_name` VARCHAR(255) NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `position` `position` INT(11) NULL DEFAULT NULL");
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// silence is golden
	}

}
