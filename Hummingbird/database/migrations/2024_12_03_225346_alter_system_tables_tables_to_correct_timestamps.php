<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSystemTablesTablesToCorrectTimestamps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach(['activitylogs', 'email_tracking', 'email_tracking_activities', 'errors', 'groups_permissions', 'modules', 'moduletemplates', 'pages', 'pages_versions', 'permissions', 'plugins', 'roles', 'search', 'templates', 'widgets'] as $TableName):
            if( Schema::hasTable($TableName) ) {

                $Table = Config::get('database.connections.mysql.prefix') . $TableName;
                
                DB::statement("ALTER TABLE `{$Table}` MODIFY created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, MODIFY updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL");
                
            }
        endforeach;

        if( Schema::hasTable('failed_jobs') ) {

            $Table = Config::get('database.connections.mysql.prefix') . 'failed_jobs';
            
            DB::statement("ALTER TABLE `{$Table}` MODIFY failed_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL");
            
        }

        if( Schema::hasTable('password_reminders') ) {

            $Table = Config::get('database.connections.mysql.prefix') . 'password_reminders';
            
            DB::statement("ALTER TABLE `{$Table}` MODIFY created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL");
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
