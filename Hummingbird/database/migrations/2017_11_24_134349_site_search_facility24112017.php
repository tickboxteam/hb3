<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SiteSearchFacility24112017 extends Migration {
	public $table = 'search';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable($this->table))
		{
			if(!Schema::hasColumn($this->table, 'weight'))
			{
				Schema::table($this->table, function($table) {
					$table->integer('weight')->default(1);
				});
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable($this->table) && Schema::hasColumn($this->table, 'weight'))
		{
			Schema::table($this->table, function($table)
			{
				$table->dropColumn('weight');
			});
		}
	}
}
