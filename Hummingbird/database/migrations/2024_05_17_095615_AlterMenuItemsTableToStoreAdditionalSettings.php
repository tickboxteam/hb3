<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMenuItemsTableToStoreAdditionalSettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('menuitems') ) {
			Schema::table('menuitems', function($table) {
				// Menu link - classes
				if( !Schema::hasColumn('menuitems', 'title') ):
					$table->text('title')->after('url')->nullable();
				endif;

				// Menu link - classes
				if( !Schema::hasColumn('menuitems', 'classes') ):
					$table->text('classes')->after('title')->nullable();
				endif;

				// Menu link - ID
				if( !Schema::hasColumn('menuitems', 'menu_item_id') ):
					$table->text('menu_item_id')->after('classes')->nullable();
				endif;

				// Menu link - description
				if( !Schema::hasColumn('menuitems', 'description') ):
					$table->text('description')->after('menu_item_id')->nullable();
				endif;

				// Menu link - New window target
				if( !Schema::hasColumn('menuitems', 'new_window') ):
					$table->tinyInteger('new_window')->after('description')->nullable();
				endif;
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('menuitems') ) {
			Schema::table('menuitems', function($table) {
				// Menu link - classes
				if( Schema::hasColumn('menuitems', 'title') ):
					$table->dropColumn('title');
				endif;

				// Menu link - classes
				if( Schema::hasColumn('menuitems', 'classes') ):
					$table->dropColumn('classes');
				endif;

				// Menu link - ID
				if( Schema::hasColumn('menuitems', 'menu_item_id') ):
					$table->dropColumn('menu_item_id');
				endif;

				// Menu link - description
				if( Schema::hasColumn('menuitems', 'description') ):
					$table->dropColumn('description');
				endif;

				// Menu link - New window target
				if( Schema::hasColumn('menuitems', 'new_window') ):
					$table->dropColumn('new_window');
				endif;
			});
		}
	}

}
