<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MediaCollectionUpdate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$table_name = DB::getTablePrefix() . "media-items";
		
		if(Schema::hasTable('media-items') && Schema::hasColumn('media-items', 'parent'))
		{
			DB::statement("ALTER TABLE `" . $table_name . "` CHANGE `parent` `parent` INT(100) NULL DEFAULT NULL");
		}

		if(Schema::hasTable('media-items') && Schema::hasColumn('media-items', 'collection'))
		{
			DB::statement("ALTER TABLE `" . $table_name . "` CHANGE `collection` `collection` INT(100) NULL DEFAULT NULL");
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// nothing to do - we need this field to stay as it is!
	}
}
