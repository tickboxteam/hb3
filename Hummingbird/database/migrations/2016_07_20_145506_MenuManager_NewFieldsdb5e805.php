<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuManagerNewFieldsdb5e805 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('menus'))
		{
			// remove location
			if(Schema::hasColumn('menus', 'location'))
			{
				Schema::table('menus', function($table)
				{
				    $table->dropColumn('location');
				});
			}

			// remove live feature
			if(Schema::hasColumn('menus', 'live'))
			{
				Schema::table('menus', function($table)
				{
				    $table->dropColumn('live');
				});
			}

			// add a description location
			if(!Schema::hasColumn('menus', 'description'))
			{
				Schema::table('menus', function($table)
				{
				    $table->text('description')->after('name');
				});
			}
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('menus'))
		{
			if(Schema::hasColumn('menus', 'description'))
			{
				Schema::table('menus', function($table)
				{
				    $table->dropColumn('description');
				});
			}
	    }

			// remove location
			if(!Schema::hasColumn('menus', 'location'))
			{
				Schema::table('menus', function($table)
				{
	                $table->string('location')->after('name');
				});
			}

			// remove live feature
			if(!Schema::hasColumn('menus', 'live'))
			{
				Schema::table('menus', function($table)
				{
	                $table->tinyInteger('live')->after('location');
				});
			}
	}
}
