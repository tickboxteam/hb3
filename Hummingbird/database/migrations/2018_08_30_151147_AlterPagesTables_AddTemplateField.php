<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPagesTablesAddTemplateField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('pages')) {
			Schema::table('pages', function($table) {
	        	$table->string('template')->after('user_id')->nullable();
			});
	    }

		if(Schema::hasTable('pages_versions')) {
			Schema::table('pages_versions', function($table) {
	        	$table->string('template')->after('user_id')->nullable();
			});
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('pages_versions') && Schema::hasColumn('pages_versions', 'SortOrder')) {
			Schema::table('pages_versions', function($table) {
				$table->dropColumn('template');
			});
		}

		if(Schema::hasTable('pages') && Schema::hasColumn('pages', 'SortOrder')) {
			Schema::table('pages', function($table) {
				$table->dropColumn('template');
			});
		}
	}

}
