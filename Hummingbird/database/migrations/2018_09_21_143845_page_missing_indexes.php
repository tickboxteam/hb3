<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageMissingIndexes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('pages')) {
			Schema::table('pages', function(Blueprint $table) {
				$table->index('user_id');
			});
		}

		if(Schema::hasTable('pages_versions')) {
			Schema::table('pages_versions', function(Blueprint $table) {
				$table->index('user_id');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
