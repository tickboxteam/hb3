<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ObjectPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('object_permissions')) {
			Schema::create('object_permissions', function ($table) {
			    $table->increments('id')->unsigned();
			    $table->integer('item_id')->unsigned()->nullable();
			    $table->string('item_type')->nullable();
			    $table->integer('reference_id')->unsigned()->nullable();
			    $table->string('reference_type')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('object_permissions');
	}
}
