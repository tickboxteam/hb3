<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesToAllowEmojicons extends Migration {
	private $tables = ['modules', 'moduletemplates', 'pages', 'pages_versions', 'templates'];

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		foreach( $this->tables as $Table ) {
			$NewTableName = DB::getTablePrefix() . $Table;
			DB::statement("ALTER TABLE `" . $NewTableName . "` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
