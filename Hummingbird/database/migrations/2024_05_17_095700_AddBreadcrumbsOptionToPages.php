<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBreadcrumbsOptionToPages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if( Schema::hasTable('pages') && !Schema::hasColumn('pages', 'breadcrumb') ) {
			Schema::table('pages', function($table) {
			    $table->tinyInteger('breadcrumb')->default(1)->nullable()->after('show_in_nav');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		if( Schema::hasTable('pages') && Schema::hasColumn('pages', 'breadcrumb') ) {
			Schema::table('pages', function($table) {
			    $table->dropColumn('breadcrumb');
			});
		}
	}

}
