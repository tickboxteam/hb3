<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePluginsTable extends Migration 
{
	public $table = 'plugins';
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable($this->table))
		{
			Schema::table($this->table, function($table)
			{
	        	$table->renameColumn('name', 'namespace');
	        	$table->text('description')->after('name');
	        	$table->integer('active')->nullable()->after('version');
	        	$table->integer('is_disabled')->nullable()->after('active');
	        	$table->dropColumn('foldername');

	        	$table->unique('namespace');
			});
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {}
}
