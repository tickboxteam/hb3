<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HB3AddIndexesPagesTables20190905 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('pages')) {
			Schema::table('pages', function($table) {
	        	$table->index('parentpage_id');
			});
		}

		if(Schema::hasTable('pages_versions')) {
			Schema::table('pages_versions', function($table) {
	        	$table->index('page_id');
	        	$table->index('parentpage_id');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('pages')) {
			Schema::table('pages', function($table) {
	        	$table->dropIndex('pages_parentpage_id_index');
			});
		}

		if(Schema::hasTable('pages_versions')) {
			Schema::table('pages_versions', function($table) {
	        	$table->dropIndex('pages_versions_page_id_index');
	        	$table->dropIndex('pages_versions_parentpage_id_index');
			});
		}
	}

}
