<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SearchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('search'))
		{
	        Schema::create('search', function($table)
	        {
	            $table->increments('id');
	            $table->integer('item_id');
	            $table->string('title');
	            $table->text('url');
	            $table->string('summary');
	            $table->text('content');
	            $table->string('type');
	            $table->timestamps();
	        });
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('search'))
		{
			Schema::drop('search');
		}
	}

}
