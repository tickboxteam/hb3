<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HummingbirdSystemUpgradeFixingDefaultTableValues extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('pages') ) {
			$NewTableName = DB::getTablePrefix() . "pages";
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `enable_comments` `enable_comments` TINYINT(1) NULL DEFAULT NULL;");
		}

		if( Schema::hasTable('users') ) {
			$NewTableName = DB::getTablePrefix() . "users";
			
			DB::statement("
				ALTER TABLE `" . $NewTableName . "` 
				CHANGE `preferences` `preferences` LONGTEXT NULL,
				CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			;");
		}

		if( Schema::hasTable('redirections') ) {
			$NewTableName = DB::getTablePrefix() . "redirections";
			
			DB::statement("
				ALTER TABLE `" . $NewTableName . "` 
				CHANGE `comments` `comments` VARCHAR(255) NULL DEFAULT NULL,
				CHANGE `category` `category` VARCHAR(255) NULL DEFAULT NULL,
				CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			;");
		}

		if( Schema::hasTable('taxonomy') ) {
			$NewTableName = DB::getTablePrefix() . "taxonomy";
			
			DB::statement("
				ALTER TABLE `" . $NewTableName . "` 
				CHANGE `image` `image` TEXT NULL DEFAULT NULL,
				CHANGE `description` `description` TEXT NULL DEFAULT NULL,
				CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			;");
		}

		if( Schema::hasTable('menus') ) {
			$NewTableName = DB::getTablePrefix() . "menus";
			
			DB::statement("
				ALTER TABLE `" . $NewTableName . "` 
				CHANGE `description` `description` TEXT NULL DEFAULT NULL,
				CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			;");
		}

		if( Schema::hasTable('menuitems') ) {
			$NewTableName = DB::getTablePrefix() . "menuitems";
			
			DB::statement("
				ALTER TABLE `" . $NewTableName . "` 
				CHANGE `menu_id` `menu_id` INT(11) NULL DEFAULT NULL,
				CHANGE `data` `data` VARCHAR(255) NULL DEFAULT NULL,
				CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			;");
		}
		
		if( Schema::hasTable('media-collections') ) {
			$NewTableName = DB::getTablePrefix() . "media-collections";
			
			DB::statement("
				ALTER TABLE `" . $NewTableName . "` 
				CHANGE `description` `description` TEXT NULL DEFAULT NULL,
				CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			;");
		}
		
		if( Schema::hasTable('media-items') ) {
			$NewTableName = DB::getTablePrefix() . "media-items";
			
			DB::statement("
				ALTER TABLE `" . $NewTableName . "` 
				CHANGE `caption` `caption` VARCHAR(255) NULL DEFAULT NULL,
				CHANGE `alt` `alt` TEXT NULL DEFAULT NULL,
				CHANGE `description` `description` TEXT NULL DEFAULT NULL,
				CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			;");
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// silence is golden
	}

}
