<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePluginTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('plugins'))
        {
	        Schema::create('plugins', function($table)
	        {
	            $table->increments('id');
	            $table->string('name')->unique();
	            $table->string('foldername')->unique();
	            $table->text('version');
	            $table->timestamps();
	        });
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if (Schema::hasTable('plugins'))
        {
			Schema::drop('plugins');
		}
	}

}
