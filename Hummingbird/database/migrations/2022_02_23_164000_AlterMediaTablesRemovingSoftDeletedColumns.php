<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Hummingbird\Models\Media;
use Hummingbird\Models\MediaCollection;

class AlterMediaTablesRemovingSoftDeletedColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// write migration to get all trashed items and permanently delete them to drop column from media-items
		if( Schema::hasTable('media-items') ) {
			foreach( Media::whereNotNull('deleted_at')->get() as $DeletedItem ) {
				$DeletedItem->delete();
			}

			if( Schema::hasColumn('media-items', 'deleted_at') ) {
				Schema::table('media-items', function(Blueprint $table) {
					$table->dropColumn('deleted_at');	
				});
			}
		}

		// write migration to get all trashed collections and permanently delete them and to drop column from media-collections
		if( Schema::hasTable('media-collections') ) {
			foreach( MediaCollection::whereNotNull('deleted_at')->get() as $DeletedCollection ) {
				$DeletedCollection->delete();
			}

			if( Schema::hasColumn('media-collections', 'deleted_at') ) {
				Schema::table('media-collections', function(Blueprint $table) {
					$table->dropColumn('deleted_at');	
				});
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// write migration to put the soft delete column back for media-collections
		if( Schema::hasTable('media-collections') && !Schema::hasColumn('media-collections', 'deleted_at') ) {
			Schema::table('media-collections', function(Blueprint $table) {
				$table->softDeletes();
			});
		}

		// write migration to put the soft delete column back for media-items
		if( Schema::hasTable('media-items') && !Schema::hasColumn('media-items', 'deleted_at') ) {
			Schema::table('media-items', function(Blueprint $table) {
				$table->softDeletes();
			});
		}
	}
}
