<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaxonomyTableRelationships extends Migration 
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('taxonomy_relationships'))
		{
	        Schema::create('taxonomy_relationships', function($table) 
	        {
	            $table->integer('term_id');
	            $table->string('tax_type', 255);
	            $table->integer('item_id');
	        });
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('taxonomy_relationships'))
		{
        	Schema::drop('taxonomy_relationships');
		}
	}

}