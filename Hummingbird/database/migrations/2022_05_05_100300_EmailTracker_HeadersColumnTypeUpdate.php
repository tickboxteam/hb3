<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmailTrackerHeadersColumnTypeUpdate extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('email_tracking') ) {
			$NewTableName = DB::getTablePrefix() . "email_tracking";
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `headers` `headers` TEXT;");
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('email_tracking') ) {
			$NewTableName = DB::getTablePrefix() . "email_tracking";
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `headers` `headers` VARCHAR(255);");
		}
	}
}
