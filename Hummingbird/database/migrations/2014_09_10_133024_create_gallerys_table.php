<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGallerysTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('gallerys'))
        {
            Schema::create('gallerys', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('gallerys'))
        {
            Schema::table('gallerys', function(Blueprint $table) {
                Schema::drop('gallerys');
            });
        }
    }

}
