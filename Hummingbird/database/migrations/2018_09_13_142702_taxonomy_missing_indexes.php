<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaxonomyMissingIndexes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('taxonomy_relationships')) {
			Schema::table('taxonomy_relationships', function(Blueprint $table) {
				$table->unique(array('term_id', 'tax_type', 'item_id'));
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('taxonomy_relationships')) {
			Schema::table('taxonomy_relationships', function(Blueprint $table) {
				$table->dropIndex('taxonomy_relationships_term_id_tax_type_item_id_unique');
			});
		}
	}

}
