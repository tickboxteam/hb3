<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTableNames extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('users'))
		{
			// make sure this field doesn't exist
			if (!Schema::hasColumn('users', 'firstname'))
			{
				Schema::table('users', function($table)
				{
				    $table->string('firstname')->after('name');
				});
			}

			// make sure this field doesn't exist
			if (!Schema::hasColumn('users', 'surname'))
			{
				Schema::table('users', function($table)
				{
				    $table->string('surname')->after('firstname');
				});	
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('users'))
		{
			Schema::table('users', function($table)
			{
			    $table->dropColumn('firstname');
			    $table->dropColumn('surname');
			});
		}
	}
}
