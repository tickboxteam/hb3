<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTrackingTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if(!Schema::hasTable('email_tracking')) {
	        Schema::create('email_tracking', function($table) {
                $table->increments('id');
                $table->string('hash');
                $table->string('message_id');
                $table->string('headers');
                $table->string('sender_name');
                $table->string('sender_email');
                $table->string('subject');
                $table->string('recipient_name')->nullable();
                $table->string('recipient_email');
                $table->text('content');
                $table->text('content_plain');
                $table->string('type')->nullable();
                $table->tinyInteger('is_admin')->nullable();

                // Timestamps
                $table->timestamps();

                // Indexes
                $table->index('type');
                $table->index('is_admin');
	        });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('email_tracking');
	}

}
