<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Hummingbird\Database\HummingbirdMigration;

class HB3AddIndexesMediaTables20190905 extends HummingbirdMigration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('media-collections') && !$this->tableHasIndex('media-collections', 'parent_collection', 'index')) {
			Schema::table('media-collections', function($table) {
	        	$table->index('parent_collection');
			});
		}

		if(Schema::hasTable('media-items')) {
			Schema::table('media-items', function($table) {
				if( !$this->tableHasIndex('media-items', 'parent', 'index') ) {
	        		$table->index('parent');
	        	}
	        	if( !$this->tableHasIndex('media-items', 'collection', 'index') ) {
	        		$table->index('collection');
	        	}

	        	if( !$this->tableHasIndex('media-items', 'locked', 'index') ) {
	        		$table->index('locked');
	        	}
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('media-collections') && $this->tableHasIndex('media-collections', 'parent_collection', 'index')) {
			Schema::table('media-collections', function($table) {
	        	$table->dropIndex('media-collections_parent_collection_index');
			});
		}

		if(Schema::hasTable('media-items')) {
			Schema::table('media-items', function($table) {
				if( $this->tableHasIndex('media-items', 'parent', 'index') ) {
	        		$table->dropIndex('media-items_parent_index');
	        	}

	        	if( $this->tableHasIndex('media-items', 'collection', 'index') ) {
	        		$table->dropIndex('media-items_collection_index');
	        	}

	        	if( $this->tableHasIndex('media-items', 'locked', 'index') ) {
	        		$table->dropIndex('media-items_locked_index');
	        	}
			});
		}
	}
}
