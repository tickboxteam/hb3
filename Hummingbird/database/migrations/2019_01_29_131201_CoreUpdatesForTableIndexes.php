<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoreUpdatesForTableIndexes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('object_permissions')) {
			Schema::table('object_permissions', function ($table) {
				$table->index('item_id');
				$table->index('item_type');
				$table->index('reference_id');
				$table->index('reference_type');
			});
		}

		if(Schema::hasTable('taxonomy_relationships')) {
			Schema::table('taxonomy_relationships', function(Blueprint $table) {
				$table->index('term_id');
				$table->index('tax_type');
				$table->index('item_id');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('object_permissions')) {
			Schema::table('object_permissions', function ($table) {
				$table->dropIndex('object_permissions_item_id_index');
				$table->dropIndex('object_permissions_item_type_index');
				$table->dropIndex('object_permissions_reference_id_index');
				$table->dropIndex('object_permissions_reference_type_index');
			});
		}

		if(Schema::hasTable('taxonomy_relationships')) {
			Schema::table('taxonomy_relationships', function(Blueprint $table) {
				$table->dropIndex('taxonomy_relationships_term_id_index');
				$table->dropIndex('taxonomy_relationships_tax_type_index');
				$table->dropIndex('taxonomy_relationships_item_id_index');
			});
		}
	}

}
