<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaxonomyTableUpdate1 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('taxonomy'))
		{
			if(!Schema::hasColumn('taxonomy', 'image'))
			{
				Schema::table('taxonomy', function($table)
				{
				    $table->text('image')->after('slug');
				});
			}
		}

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('taxonomy'))
		{
			if(Schema::hasColumn('taxonomy', 'image'))
			{
				Schema::table('taxonomy', function($table)
				{
				    $table->dropColumn('image');
				});
			}
		}
	}

}
