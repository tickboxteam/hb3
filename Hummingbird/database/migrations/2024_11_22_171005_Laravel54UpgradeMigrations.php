<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @since 2.4.1
 */
class Laravel54UpgradeMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Schema::hasTable('failed_jobs') && !Schema::hasColumn('failed_jobs', 'exception') ) {
            Schema::table('failed_jobs', function($table) {
                $table->longText('exception')->after('payload');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( Schema::hasTable('failed_jobs') && Schema::hasColumn('failed_jobs', 'exception') ) {
            Schema::table('failed_jobs', function($table) {
                $table->dropColumn('exception');
            });
        }
    }
}
