<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Hummingbird\Models\Module;
use Hummingbird\Models\Moduletemplate;

class AlterModulesAndModuleTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('modules') ) {
			// Clean old modules which have been deleted
			Module::whereNotNull('deleted_at')->forceDelete();

			Schema::table('modules', function(Blueprint $table) {
				if( Schema::hasColumn('modules', 'live') ) {
					$table->dropColumn('live');	
				}

				if( Schema::hasColumn('modules', 'deleted_at') ) {
					$table->dropColumn('deleted_at');	
				}
			});
		}

		if( Schema::hasTable('moduletemplates') ) {
			// Clean old module templates which have been deleted
			Moduletemplate::whereNotNull('deleted_at')->forceDelete();

			Schema::table('moduletemplates', function(Blueprint $table) {

				if( Schema::hasColumn('moduletemplates', 'areas') ) {
					$table->dropColumn('areas');	
				}

				if( Schema::hasColumn('moduletemplates', 'plugin') ) {
					$table->dropColumn('plugin');	
				}

				if( Schema::hasColumn('moduletemplates', 'plugin_data') ) {
					$table->dropColumn('plugin_data');	
				}

				if( Schema::hasColumn('moduletemplates', 'live') ) {
					$table->dropColumn('live');	
				}

				if( Schema::hasColumn('moduletemplates', 'deleted_at') ) {
					$table->dropColumn('deleted_at');	
				}
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('modules') ) {
			Schema::table('modules', function(Blueprint $table) {

				if( !Schema::hasColumn('modules', 'live') ) {
					$table->tinyInteger('live')->after('module_data');
				}

				if( !Schema::hasColumn('modules', 'deleted_at') ) {
					$table->softDeletes()->after('live');
				}
			});
		}

		if( Schema::hasTable('moduletemplates') ) {
			Schema::table('moduletemplates', function(Blueprint $table) {

				if( !Schema::hasColumn('moduletemplates', 'areas') ) {
					$table->string('areas')->after('js');
				}

				if( !Schema::hasColumn('moduletemplates', 'plugin') ) {
					$table->string('plugin')->after('notes');
				}

				if( !Schema::hasColumn('moduletemplates', 'plugin_data') ) {
					$table->string('plugin_data')->after('plugin');
				}

				if( !Schema::hasColumn('moduletemplates', 'live') ) {
					$table->tinyInteger('live')->default(0)->after('editable');
				}

				if( !Schema::hasColumn('moduletemplates', 'deleted_at') ) {
					$table->softDeletes()->after('live');
				}
			});
		}
	}

}
