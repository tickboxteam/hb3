<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTrackingActivityTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if(!Schema::hasTable('email_tracking_activities')) {
	        Schema::create('email_tracking_activities', function($table) {
                $table->increments('id');
                $table->integer('tracker_id')->unsigned();
                $table->string('type');
                $table->text('link');
                $table->timestamps();

                $table->index('tracker_id');
                $table->index('type');
                $table->foreign('tracker_id')->references('id')->on('email_tracking')->onDelete('cascade');
	        });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('email_tracking_activities') ) {
			Schema::table('email_tracking_activities', function($table) {
				$table->dropForeign('email_tracking_activities_tracker_id_foreign');
			});
		}
		
		Schema::dropIfExists('email_tracking_activities');
	}

}
