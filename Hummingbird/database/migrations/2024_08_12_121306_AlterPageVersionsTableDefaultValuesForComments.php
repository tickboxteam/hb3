<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPageVersionsTableDefaultValuesForComments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('pages_versions') ) {
			$NewTableName = DB::getTablePrefix() . "pages_versions";
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `enable_comments` `enable_comments` TINYINT(1) NULL DEFAULT NULL");
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('pages_versions') ) {
			$NewTableName = DB::getTablePrefix() . "pages_versions";
			DB::statement("ALTER TABLE `" . $NewTableName . "` CHANGE `enable_comments` `enable_comments` TINYINT(1) NOT NULL");
		}
	}

}
