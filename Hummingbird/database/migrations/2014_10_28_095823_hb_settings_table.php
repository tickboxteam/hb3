<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HbSettingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('settings'))
        {
	        Schema::create('settings', function($table) 
	        {
	            $table->increments('id');
	            $table->string('key', 255);
	            $table->longText('value');
	        });
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if (Schema::hasTable('settings'))
        {
        	Schema::drop('settings');
		}
	}
}
