<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Hummingbird\Models\Menu;
use Hummingbird\Models\Menuitem;

class AlterMenusAndMenuItemTablesRemoveDeletedAt extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('menus') ) {
			// Clean old menus which have been deleted
			Menu::whereNotNull('deleted_at')->get()->each(function($Menu) {
			    $Menu->delete();
			});

			Schema::table('menus', function(Blueprint $table) {
				if( Schema::hasColumn('menus', 'deleted_at') ) {
					$table->dropColumn('deleted_at');	
				}
			});
		}

		if( Schema::hasTable('menuitems') ) {
			// Clean old menu_items which have been deleted
			Menuitem::whereNotNull('deleted_at')->forceDelete();

			Schema::table('menuitems', function(Blueprint $table) {
				if( Schema::hasColumn('menuitems', 'deleted_at') ) {
					$table->dropColumn('deleted_at');	
				}
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('menus') ) {
			Schema::table('menus', function(Blueprint $table) {
				if( !Schema::hasColumn('menus', 'deleted_at') ) {
					$table->softDeletes();
				}
			});
		}

		if( Schema::hasTable('menuitems') ) {
			Schema::table('menuitems', function(Blueprint $table) {
				if( !Schema::hasColumn('menuitems', 'deleted_at') ) {
					$table->softDeletes();
				}
			});
		}
	}

}
