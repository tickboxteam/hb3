<?php

use Hummingbird\Database\HummingbirdMigration;
use Illuminate\Database\Schema\Blueprint;

class AlterSearchTableAddIndexToType extends HummingbirdMigration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('search') ) {
			Schema::table('search', function($table) {
				// Add new indexes to search
				foreach( ['type'] as $NewIndex ) {
					if( !$this->tableHasIndex('search', $NewIndex, 'index') ) {
						$table->index( $NewIndex );
					}
				}
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{		
		if( Schema::hasTable('search') ) {
			Schema::table('search', function($table) {
				// Remove indexes from search
				foreach( ['type'] as $NewIndex ) {
					if( $this->tableHasIndex('search', $NewIndex, 'index') ) {
						$table->dropIndex( "search_{$NewIndex}_index" );
					}
				}
			});
		}
	}

}
