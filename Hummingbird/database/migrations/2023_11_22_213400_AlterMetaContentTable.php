<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMetaContentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if( Schema::hasTable('meta_content') && !Schema::hasColumn('meta_content', 'id') ) {
			Schema::table('meta_content', function($table) {
			    $table->increments('id')->first();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if( Schema::hasTable('meta_content') && Schema::hasColumn('meta_content', 'id') ) {
			Schema::table('meta_content', function($table) {
			    $table->dropColumn('id');
			});
		}
	}

}
