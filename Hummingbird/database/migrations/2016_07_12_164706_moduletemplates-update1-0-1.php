<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuletemplatesUpdate101 extends Migration
{
	public $table_name = 'moduletemplates';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$fields = array('html', 'css', 'js');

		$this->table_name = DB::getTablePrefix().$this->table_name;

        if (Schema::hasTable('moduletemplates'))
        {
        	foreach($fields as $field)
        	{
				if(Schema::hasColumn('moduletemplates', $field))
				{
					DB::statement("ALTER TABLE `" . $this->table_name . "` MODIFY COLUMN " . $field . " TEXT");
				}
        	}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {}
}
