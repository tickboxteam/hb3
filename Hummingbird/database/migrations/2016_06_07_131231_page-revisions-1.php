<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageRevisions1 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('pages'))
		{
			if(!Schema::hasColumn('pages', 'position'))
			{
				Schema::table('pages', function($table)
				{
				    $table->integer('position')->after('custom_menu_name');
				});
			}
		}

		if(Schema::hasTable('pages_versions'))
		{
			if(!Schema::hasColumn('pages_versions', 'position'))
			{
				Schema::table('pages_versions', function($table)
				{
				    $table->integer('position')->after('custom_menu_name');
				});
			}
		}

		if(class_exists('Tickbox\Microsites\Microsites'))
		{
			if(Schema::hasTable('microsites'))
			{
				$microsites = \Tickbox\Microsites\Models\Microsite::all();

				if(count($microsites) > 0)
				{
					$prefix = (Config::get('database.connections.mysql.prefix') != '') ? '_':'microsite';

					foreach($microsites as $microsite)
					{
						$new_prefix = $prefix . $microsite->id . '_';

						if(Schema::hasTable($new_prefix . 'pages'))
						{							
							if(!Schema::hasColumn($new_prefix . 'pages', 'position'))
							{
								Schema::table($new_prefix . 'pages', function($table)
								{
								    $table->integer('position')->after('custom_menu_name');
								});
							}
						}

						if(Schema::hasTable($new_prefix . 'pages_versions' . 'pages'))
						{		
							if(!Schema::hasColumn($new_prefix . 'pages_versions', 'position'))
							{
								Schema::table($new_prefix . 'pages_versions', function($table)
								{
								    $table->integer('position')->after('custom_menu_name');
								});
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('pages'))
		{
			if(Schema::hasColumn('pages', 'position'))
			{
				Schema::table('pages', function($table)
				{
				    $table->dropColumn('position');
				});
			}
		}

		if(Schema::hasTable('pages_versions'))
		{
			if(!Schema::hasColumn('pages_versions', 'position'))
			{
				Schema::table('pages_versions', function($table)
				{
				    $table->dropColumn('position');
				});
			}
		}

		if(class_exists('Tickbox\Microsites\Microsites'))
		{
			if(Schema::hasTable('microsites'))
			{
				$microsites = \Tickbox\Microsites\Models\Microsite::where('type', 'standard')->get();

				if(count($microsites) > 0)
				{
					$prefix = (Config::get('database.connections.mysql.prefix') != '') ? '_':'microsite';

					foreach($microsites as $microsite)
					{
						$new_prefix = $prefix  . $microsite->id . '_';

						if(Schema::hasTable($new_prefix . 'pages'))
						{							
							Schema::table($new_prefix . 'pages', function($table)
							{
							    $table->dropColumn('position');
							});
						}

						if(Schema::hasTable($new_prefix . 'pages_versions' . 'pages'))
						{		
							Schema::table($new_prefix . 'pages_versions', function($table)
							{
							    $table->dropColumn('position');
							});
						}
					}
				}
			}
		}
	}

}
