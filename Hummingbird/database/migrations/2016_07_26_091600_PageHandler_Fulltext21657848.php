<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageHandlerFulltext21657848 extends Migration
{
	public $table = 'pages';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable($this->table))
		{
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "` ENGINE = MyISAM");
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "_versions` ENGINE = MyISAM");

			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "` ADD FULLTEXT KEY `title` (`title`), ADD FULLTEXT KEY `url` (`url`), ADD FULLTEXT KEY `content` (`content`),  ADD FULLTEXT KEY `title_2` (`title`,`url`,`content`)");
			
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "_versions` ADD FULLTEXT KEY `title` (`title`), ADD FULLTEXT KEY `url` (`url`), ADD FULLTEXT KEY `content` (`content`),  ADD FULLTEXT KEY `title_2` (`title`,`url`,`content`)");
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable($this->table))
		{
			/* remove indexes from Page Versions table */
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "_versions` DROP INDEX `title_2`");
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "_versions` DROP INDEX `content`");
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "_versions` DROP INDEX `url`");
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "_versions` DROP INDEX `title`");

			/* remove indexes from Page table */
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "` DROP INDEX `title_2`");
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "` DROP INDEX `content`");
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "` DROP INDEX `url`");
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "` DROP INDEX `title`");

			/* Change engine back to normal */
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "_versions` ENGINE = InnoDB");
			DB::statement("ALTER TABLE `" . Config::get('database.connections.mysql.prefix') . $this->table . "` ENGINE = InnoDB");
		}
	}

}
