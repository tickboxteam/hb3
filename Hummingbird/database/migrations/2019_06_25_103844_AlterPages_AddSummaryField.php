<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPagesAddSummaryField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('pages') && !Schema::hasColumn('pages', 'summary') ) {
			Schema::table('pages', function(Blueprint $table) {
				$table->string('summary')->after('title')->nullable();
			});
		}

		if( Schema::hasTable('pages_versions') && !Schema::hasColumn('pages_versions', 'summary') ) {
			Schema::table('pages_versions', function(Blueprint $table) {
				$table->string('summary')->after('title')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('pages') && Schema::hasColumn('pages', 'summary')) {
			Schema::table('pages', function($table) {
				$table->dropColumn('summary');
			});
		}

		if(Schema::hasTable('pages_versions') && Schema::hasColumn('pages_versions', 'summary')) {
			Schema::table('pages_versions', function($table) {
				$table->dropColumn('summary');
			});
		}
	}
}
