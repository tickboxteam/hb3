<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HummingbirdFrameworkUpgrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Schema::hasTable('users') && !Schema::hasColumn('users', 'email_verified_at') ) {
            Schema::table('users', function($table) {
                $table->timestamp('email_verified_at')->nullable()->after('force_password_reset');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( Schema::hasTable('users') && Schema::hasColumn('users', 'email_verified_at') ) {
            Schema::table('users', function($table) {
                $table->dropColumn('email_verified_at');
            });
        }
    }
}
