<?php

use Hummingbird\Database\HummingbirdMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIndexesForPageTables extends HummingbirdMigration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if( Schema::hasTable('pages') ) {
			Schema::table('pages', function($table) {
				// Add new indexes to pages
				foreach( ['status'] as $NewIndex ) {
					if( !$this->tableHasIndex('pages', $NewIndex, 'index') ) {
						$table->index( $NewIndex );
					}
				}
			});
		}

		if( Schema::hasTable('pages_versions') ) {
			Schema::table('pages_versions', function($table) {
				// Add new indexes to pages_versions
				foreach( ['status'] as $NewIndex ) {
					if( !$this->tableHasIndex('pages_versions', $NewIndex, 'index') ) {
						$table->index( $NewIndex );
					}
				}
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{		
		if( Schema::hasTable('pages_versions') ) {
			Schema::table('pages_versions', function($table) {
				// Remove indexes from pages_version
				foreach( ['status'] as $NewIndex ) {
					if( $this->tableHasIndex('pages_versions', $NewIndex, 'index') ) {
						$table->dropIndex( "pages_versions_{$NewIndex}_index" );
					}
				}
			});
		}

		if( Schema::hasTable('pages') ) {
			Schema::table('pages', function($table) {
				// Remove indexes from pages
				foreach( ['status'] as $NewIndex ) {
					if( $this->tableHasIndex('pages', $NewIndex, 'index') ) {
						$table->dropIndex( "pages_{$NewIndex}_index" );
					}
				}
			});
		}
	}

}
