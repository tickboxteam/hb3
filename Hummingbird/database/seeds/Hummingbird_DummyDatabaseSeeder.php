<?php 

use Illuminate\Database\Seeder as Seeder;

class Hummingbird_DummyDatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        // ordering is important here
        $this->call('DummyRolesSeeder');
        $this->call('DummyUsersSeeder');
    }
}
