<?php namespace Hummingbird\Database\Seeds;

class ActivityLogTableSeeder extends HbSeeder {

    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'activitylog',
            'group_name'    => 'Activity Logs',
            'group_desc'    => 'User reports and activity logs',
            'permissions'   => [
                'read'      => [], 
                'delete'    => [], 
                'export'    => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
