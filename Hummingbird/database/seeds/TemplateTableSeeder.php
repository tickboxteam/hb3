<?php namespace Hummingbird\Database\Seeds;

class TemplateTableSeeder extends HbSeeder {
    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'template',
            'group_name'    => 'Website Templates',
            'group_desc'    => 'Manage website templates',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [], 
                'lock'      => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
