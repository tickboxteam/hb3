<?php namespace Hummingbird\Database\Seeds;

class ErrorsTableSeeder extends HbSeeder {

    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'error',
            'group_name'    => 'Error Management',
            'group_desc'    => 'Manage system errors',
            'permissions'   => [
                'read'      => [], 
                'delete'    => [], 
                'export'    => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
