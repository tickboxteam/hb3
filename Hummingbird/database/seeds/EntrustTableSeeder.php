<?php namespace Hummingbird\Database\Seeds;

use Hummingbird\Libraries\PermissionHandler;
use Hummingbird\Models\Role;

class EntrustTableSeeder extends HbSeeder
{

    public function add_seed_data()
    {
        $superadmin = new Role;
        $superadmin->name = 'superadmin';
        $superadmin->hidden = 1;
        $superadmin->locked = 1;
        $superadmin->save();        
    }

    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'role',
            'group_name'    => 'Role Manager',
            'group_desc'    => 'Manage system roles',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [], 
                'lock'      => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
