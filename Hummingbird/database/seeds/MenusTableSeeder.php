<?php namespace Hummingbird\Database\Seeds;

class MenusTableSeeder extends HbSeeder {
    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'menu',
            'group_name'    => 'Navigation Manager',
            'group_desc'    => 'Manage navigation menus',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [], 
                'lock'      => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
