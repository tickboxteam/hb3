<?php namespace Hummingbird\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // ordering is important here
        $this->call('Hummingbird\Database\Seeds\EntrustTableSeeder');
        $this->call('Hummingbird\Database\Seeds\UsersTableSeeder');
        $this->call('Hummingbird\Database\Seeds\ErrorsTableSeeder');
        $this->call('Hummingbird\Database\Seeds\ActivityLogTableSeeder');
        $this->call('Hummingbird\Database\Seeds\SettingTableSeeder');
        $this->call('Hummingbird\Database\Seeds\RedirectionTableSeeder');
        $this->call('Hummingbird\Database\Seeds\TemplateTableSeeder');
        $this->call('Hummingbird\Database\Seeds\PagesTableSeeder');
        $this->call('Hummingbird\Database\Seeds\MenusTableSeeder');
        $this->call('Hummingbird\Database\Seeds\MediaLibrarySeeder');
        $this->call('Hummingbird\Database\Seeds\CategoriesSeeder');
        $this->call('Hummingbird\Database\Seeds\TagsSeeder');        
        $this->call('Hummingbird\Database\Seeds\ThemeInfoSeeder');
    }
}
