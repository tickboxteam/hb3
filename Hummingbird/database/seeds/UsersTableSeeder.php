<?php namespace Hummingbird\Database\Seeds;

class UsersTableSeeder extends HbSeeder {

    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'user',
            'group_name'    => 'User Management',
            'group_desc'    => 'Manage system users',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [], 
                'lock'      => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
