<?php namespace Hummingbird\Database\Seeds;

use Hummingbird\Models\Setting;

class SettingTableSeeder extends HbSeeder {
    public function add_seed_data() {
        $setting = new Setting;
        $setting->newSetting('responsive', array('framework'=>'bootstrap'));
    }

    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'setting',
            'group_name'    => 'Settings',
            'group_desc'    => 'Manage site settings',
            'permissions'   => [
                'read'      => [], 
                'update'    => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);  
    }
}
