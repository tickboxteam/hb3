<?php namespace Hummingbird\Database\Seeds;

class CategoriesSeeder extends HbSeeder {
    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'categories',
            'group_name'    => 'Categories',
            'group_desc'    => 'Manage category to website associations and link articles together',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [], 
                'lock'      => [],
                'export'    => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
