<?php namespace Hummingbird\Database\Seeds;

use Illuminate\Database\Seeder;

/*
 * Abstract class to organise all seed installs
 */

abstract class HB3Seeder extends Seeder
{
    public abstract function up($arguments = array());
    public abstract function down($arguments = array());
}
