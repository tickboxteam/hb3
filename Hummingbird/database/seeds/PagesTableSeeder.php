<?php namespace Hummingbird\Database\Seeds;

use Config;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\Page;
use Hummingbird\Models\PageVersion;

class PagesTableSeeder extends HbSeeder 
{
    /*
     * requires templates to have been created first
     */
    public function add_seed_data()
    {   
        $pages_to_install = config()->get('hb3-hummingbird.installPages');

        if(count($pages_to_install) > 0)
        {
            foreach($pages_to_install as $page)
            {
                /* Create version */
                $new_page = new Page;
                $new_page->parentpage_id = NULL;
                $new_page->title = $page['Name'];
                $new_page->url = ( isset($page['URL']) && $page['URL'] != '' ) ? $page['URL'] : str_slug( $page['Name'] );
                $new_page->content = '';
                $new_page->status = 'public';
                $new_page->locked = 1;

                $new_page->username = ''; //FIXME: This doesn't feel right because of the issues with default values
                $new_page->password = ''; //FIXME: This doesn't feel right because of the issues with default values
                $new_page->search_image = ''; //FIXME: This doesn't feel right because of the issues with default values
                $new_page->featured_image = ''; //FIXME: This doesn't feel right because of the issues with default values
                $new_page->custom_menu_name = ''; //FIXME: This doesn't feel right because of the issues with default values
                $new_page->position = 0; //FIXME: This doesn't feel right because of the issues with default values
                $new_page->enable_comments = 0; //FIXME: This doesn't feel right because of the issues with default values
                
                $new_page->save();
                $new_page->permalink = ( isset($page['URL']) && $page['URL'] != '' ) ? trim($page['URL'], '/') : str_slug( $page['Name'] );
                $new_page->save();

                /* Create a new version */
                $PageVersion = new PageVersion;
                $PageVersion->fill($new_page->toArray());
                $PageVersion->hash = time();
                $PageVersion->page_id = $new_page->id;
                $PageVersion->save();

                /* Store activity */
                Activitylog::log([
                    'action' => 'CREATED',
                    'type' => get_class($new_page),
                    'link_id' => $new_page->id,
                    'description' => 'Created page',
                    'notes' => "System created a new page - &quot;$new_page->title&quot;"
                ]);
            }
        }
    }

    public function register_permissions()
    {
        $PermissionGroupInfo = [
            'key_name'      => 'page',
            'group_name'    => 'Pages',
            'group_desc'    => 'Manage page content',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [], 
                'lock'      => [],
                'publish'   => [], 
                'export'    => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
