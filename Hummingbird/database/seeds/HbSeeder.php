<?php namespace Hummingbird\Database\Seeds;

use Hummingbird\Libraries\PermissionHandler;
use Illuminate\Database\Seeder;

/*
 * Parent class to organise all seed installs
 */

abstract class HbSeeder extends Seeder
{   
    public function __construct() {
        $this->PermissionHandler = new PermissionHandler;
    }

    public function add_seed_data() {}
    public function register_permissions() {}
    public function register_widgets() {}
    public function register_modules() {}
    public function register_blocks() {}

    public function run()
    {
        $this->add_seed_data();
        $this->register_permissions();
        $this->register_widgets();
        $this->register_modules();
        $this->register_blocks();
    }
}