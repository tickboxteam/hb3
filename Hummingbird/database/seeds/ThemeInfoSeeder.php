<?php namespace Hummingbird\Database\Seeds;
use Hummingbird\Models\Setting;

class ThemeInfoSeeder extends HbSeeder {
    public function add_seed_data()
    {
        $setting = new Setting;
        $setting->key = 'activeTheme';
        $setting->value = serialize(array('theme' => 'bootstrap'));
        $setting->save();        
    }
}
