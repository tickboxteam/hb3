<?php namespace Hummingbird\Database\Seeds;

class TagsSeeder extends HbSeeder {

    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'tags',
            'group_name'    => 'Tags',
            'group_desc'    => 'A non-hierarchical keyword or term assigned to website resources',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [], 
                'lock'      => [],
                'export'    => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
