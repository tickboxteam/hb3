<?php namespace Hummingbird\Database\Seeds;

class RedirectionTableSeeder extends HbSeeder {

    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'redirections',
            'group_name'    => 'Redirects',
            'group_desc'    => 'Manage website redirects',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [], 
                'lock'      => [],
                'export'    => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo); 
    }
}
