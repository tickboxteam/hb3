<?php namespace Hummingbird\Database\Seeds;

use Hummingbird\Models\Setting;

class MediaLibrarySeeder extends HbSeeder
{

    public function add_seed_data()
    {
        // superadmin user will be added on install
        $this->image_sizes = Setting::where('key', 'media')->first();

        $this->image_sizes = (!isset($this->image_sizes->id) OR null === $this->image_sizes->id) ? false:$this->image_sizes->value;

        if(!isset($this->image_sizes['sizes']) OR count($this->image_sizes['sizes']) == 0)
        {
            $this->image_sizes['sizes'] = array(
                0 => array (
                    'name' => 'File Manager Thumbnail',
                    'type' => 'Default',
                    'description' => 'File Manager Thumbnails',
                    'width' => 300
                )
            );

            (new Setting)->newSetting('media', $this->image_sizes);
        }
    }

    public function register_permissions() {
        $PermissionGroupInfo = [
            'key_name'      => 'media',
            'group_name'    => 'Media Centre',
            'group_desc'    => 'Manage files in the HB3 Media Centre',
            'permissions'   => [
                'create'    => [], 
                'read'      => [], 
                'update'    => [], 
                'delete'    => [], 
                'upload'    => []
            ]
        ];

        $this->PermissionHandler->registerPermissionGroupings($PermissionGroupInfo);
    }
}
