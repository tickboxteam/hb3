<?php

if( ! function_exists('redirects_get_category_list') ) {
    /**
     * Get the list of categories, with OptGroups, for grouping redirects
     * @return Array
     */
    function redirects_get_category_list() {
        $redirect_cats = [NULL => '--- Please select ---'] + ['Categories' => [], 'Custom' => ['NEW' => 'Create new category']];

        $redirect_cats['Categories'] = Hummingbird\Models\Redirections::categoriesOnly()->pluck('category', 'category')->all();

        if( count($redirect_cats['Categories']) <= 0 ) {
            unset($redirect_cats['Categories']);
        }

        return $redirect_cats;
    }
}