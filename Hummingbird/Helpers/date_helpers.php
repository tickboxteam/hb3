<?php
use Carbon\Carbon;

if( ! function_exists('add_superscript_to_dates') ) {
    /**
     * Allowing dates (the day itself) to be superscripted
     * @param Integer $Date
     * @param boolean $DateSuperscript
     *
     * @return String
     */
    function add_superscript_to_dates( $Date, $DateSuperscript = true ) {
        $Date = ( $Date <= 9 ) ? str_pad($Date, 2) : $Date;

        return Carbon::parse( Carbon::now()->format("Y-m-{$Date}") )->format( $DateSuperscript ? "j\<\s\u\p\>S\<\/\s\u\p\>" : "jS" );
    }
}