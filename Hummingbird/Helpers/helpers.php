<?php

if ( ! function_exists('highlighter'))
{
    /**
     * Highlight the chosen keyword (if found)
     *
     * @param  string  $searchterm
     * @param  string   $searchresult
     * @return string
     */
    function highlighter($searchterm, $searchresult)
    {
        $searchresult = str_replace('<strong>', '</strong>', $searchresult);
        $clean_word = preg_replace("/[^a-zA-Z0-9\s]/", "", $searchterm); // remove non-alphanumeric
        $pattern = "(\.|\s|\,|^|$|\-|')";

        return $searchresult = preg_replace('/'.$pattern.'('.$clean_word.')'.$pattern.'/si', '$1<strong class="search-highlight">$2</strong>$3', $searchresult);
    }
}

if(!function_exists('excerpt')) {
    /**
     * Generate an excerpt for an article
     *
     * @param  string  $name
     * @param  array   $parameters
     * @return string
     */
    function excerpt($string, $character_limit = 255, $ellipsis = '...') {
        $string = rtrim(substr($string, 0, $character_limit), ".");

        return ( strlen($string) > $character_limit ) ? $string . $ellipsis : $string;
    }
}


if(!function_exists('clean_html_and_shortcodes_from_string')) {
    /**
     * Clean the summary/content areas of any HTML and Shortcodes
     * @param  String $html
     * @return String
     */
    function clean_html_and_shortcodes_from_string($html) {
        $find_modules_pattern = '#\[HB::(.*?)]#';
        preg_match_all($find_modules_pattern, $html, $items);

        $html = (count($items[0]) > 0) ? str_replace($items[0], "", $html) : $html;
        $html = preg_replace("|<style\b[^>]*>(.*?)</style>|s", "", $html);
        return trim(strip_tags(trim($html)));
    }
}

if( !function_exists('check_and_clean_emails') ) {
    /**
     * Because we're storing emails, we need to at least check/clean
     * them so that we don't store emails which are incorrect or
     * invalid. Helps reduce issues later on.
     * 
     * @param  Mixed $emails
     * @return String
     */
    function check_and_clean_emails( $emails = '' ) {
        if( empty($emails) ) {
            return;
        }

        $emails = ( !is_array($emails) ) ? ArrayHelper::cleanExplodedArray( explode(",", $emails) ) : $emails;
        return implode(", ", array_filter($emails, function ($s) { return filter_var($s, FILTER_VALIDATE_EMAIL); }) );
    }
}

if( !function_exists('has_a_referrer_link') ) {
    /**
     * Does the site have a referrer link?
     * @return boolean
     */
    function has_a_referrer_link() {
        return ( isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] );
    }
}

if( !function_exists('grammarize') ) {
    /**
     * We want to grammarize the name correctly
     * Alice - would return Alice's
     * Chris - would return Chris'
     * 
     * @param  String $string
     * @return String
     */
    function grammarize($string) {
        return $string.'\''.($string[strlen($string) - 1] != 's' ? 's' : '');
    }
}