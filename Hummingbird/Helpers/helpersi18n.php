<?php
/*
|--------------------------------------------------------------------------
| Language Translation helpers
|--------------------------------------------------------------------------
|
*/
if (! function_exists('trans_fb')) {
    /**
     * Translate the given message with a fallback string if none exists.
     *
     * Sourced: https://stackoverflow.com/questions/37901955/laravel-specify-a-default-fallback-string-for-multi-language
     *
     * @param  string  $id
     * @param  string  $fallback
     * @param  array   $parameters
     * @param  string  $domain
     * @param  string  $locale
     *
     * @return \Symfony\Component\Translation\TranslatorInterface|string
     */
    function trans_fb($id, $fallback = '', array $parameters = array(), $domain = 'messages', $locale = null) {
        $translation = trans($id, $parameters, $domain, $locale);

        if( !empty($fallback) && $translation === $id ) {
            return $fallback;
        }

        return $translation;
    }
}