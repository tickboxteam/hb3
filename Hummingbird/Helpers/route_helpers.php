<?php

if( !function_exists('getQueryStringParameters') ):
    /**
     * Get all of the query strings from the URL
     * and return an array
     * 
     * @param  Mixed $url
     * @return Array
     */
    function getQueryStringParameters( $url = NULL ) {
        $parameters = [];

        $URLParameters = parse_url( $url );

        if( isset($URLParameters['query']) ) {
            parse_str( $URLParameters['query'], $parameters );
        }

        return $parameters;
    }
endif;