<?php
use Illuminate\Support\Str;

if( !function_exists('get_domain_tld_from_string') ) {
    /**
     * Helper function to allow us to get the domain/TLD
     * from any given string
     * @param  String $url
     * @return String
     */
    function get_domain_tld_from_string($url) {
        // https://gist.github.com/mlconnor/5266986
        preg_match('/(?:[-a-zA-Z0-9@:%_\+~.#=]{2,256}\.)?([-a-zA-Z0-9@:%_\+~#=]*)\.(aero|asia|biz|cat|com|coop|info|int|jobs|mobi|museum|name|net|org|post|pro|tel|travel|mlcee|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bl|bm|bn|bo|bq|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cw|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mf|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|za|zm|zw)/', $url, $DomainComponents);

        return $DomainComponents;
    }
}



if( ! function_exists('isLinkExternal') ) {
    /**
     * Check to see if the string passed is internal or external
     * @param  String  $url
     * @return boolean
     */
    function isLinkExternal($url) {
        if( !empty($url) ):
            $website    = parse_url( Config::get('app.url') );

            if( !isset($website['host']) ):
                $website = parse_url( 'https://' . Config::get('app.url') );
            endif;

            $components = parse_url($url);
            $Domain     = isset($components['host']) ? $components['host'] : NULL;

            if( is_null( $Domain ) ):
                $DomainComponents = get_domain_tld_from_string( $url );

                if( !empty($DomainComponents) ):
                    $components = parse_url( "https://{$url}");
                    $Domain = ( isset($components['host']) ) ? $components['host'] : NULL;
                endif;
            endif;

            return ( isset($Domain) && !empty($Domain) && $Domain != $website['host'] );
        endif;

        return false;
    }
}

if( !function_exists('obfuscate_mailto_links') ) {
    /**
     * Replace all instances of "mailto:", with an obfuscated version
     * @param  String $PageContent
     * @return String
     */
    function obfuscate_mailto_links($PageContent) {
        return str_replace("mailto:", Html::obfuscate('mailto:'), $PageContent);
    }
}

if( !function_exists('obfuscate_emails') ) {
    /**
     * Replace all instances of "xxx@xxx.xx.xx", with an obfuscated version.
     * Each email is replaced with a unique version
     * 
     * @param  String $PageContent
     * @return String
     */
    function obfuscate_emails($PageContent) {
        preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $PageContent, $emails);
        if( count($emails) > 0 ):
            foreach($emails[0] as $email):
                $PageContent = str_replace($email, HTML::email( $email ), $PageContent);
            endforeach;
        endif;

        return $PageContent;
    }
}

if( !function_exists('clean_and_sluggify_url') ) {
    /**
     * Pass in a URL and then clean it, in case we have variable parts
     * to the URL. If the URL ends with an extension then the
     * trailing slash is not added. All other links have a
     * trailing slash.
     * 
     * @param  String $url
     * @return Mixed
     */
    function clean_and_sluggify_url( $url ) {
        if( !empty($url) ) {

            if( strpos(strtolower($url), 'javascript:') !== FALSE ):
                return $url;
            endif;

            $original_url = trim($url);
            $QueryStringsParts = parse_url($original_url);
            $url = ( isset($QueryStringsParts['path']) ) ? trim( $QueryStringsParts['path'], "/") : '';

            $url_parts = explode("/", $url);
            $CleanedUrlParts = [];

            foreach($url_parts as $url_part) {
                $pathinfo = pathinfo($url_part);
                $CleanedUrlParts[] = ( isset($pathinfo['extension']) && !empty($pathinfo['extension']) ) ? $url_part : str_slug($url_part);
            }

            $ReturnUrl = implode("/", $CleanedUrlParts);
            $pathinfo = pathinfo( $ReturnUrl );
            $ReturnUrl = "/{$ReturnUrl}";
            $ReturnUrl = str_replace("//", "/", $ReturnUrl);

            if( isLinkExternal($original_url) ) {
                $ReturnUrl = "/" . implode("/", $CleanedUrlParts);
                $Scheme = ( isset($QueryStringsParts['scheme']) ) ? $QueryStringsParts['scheme'] : 'https';
                $Host = ( isset($QueryStringsParts['host']) ) ? $QueryStringsParts['host'] : '';

                if( $Host == '' ) {
                    $DomainComponents = get_domain_tld_from_string( $url );

                    if( !empty($DomainComponents) ):
                        $Host = $DomainComponents[0];
                    endif;
                }

                $ReturnUrl = str_replace("//", "/", str_replace($Host, "", $ReturnUrl));
                $ReturnUrl .= ( Str::endsWith($QueryStringsParts['path'], "/") ) ? "/" : "";
                $ReturnUrl = "{$Scheme}://{$Host}{$ReturnUrl}";
            }

            $ReturnUrlToBe = ( isset($QueryStringsParts['query']) && $QueryStringsParts['query'] != '' ) ? "{$ReturnUrl}?{$QueryStringsParts['query']}" : $ReturnUrl;

            $ReturnUrlToBe = ( isset($QueryStringsParts['fragment']) && $QueryStringsParts['fragment'] != '' ) ? "{$ReturnUrlToBe}#{$QueryStringsParts['fragment']}" : $ReturnUrlToBe;

            return $ReturnUrlToBe;
        }
    }
}

if( !function_exists('route_exists') ) {
    /**
     * Check if the route provided exists
     * @param  String $string
     * @return Boolean
     */
    function route_exists($string) {
        try {
            $route = route( $string );
            return true;
        }
        catch(\Exception $e) {
            //silence is golden
        }

        return false;
    }
}