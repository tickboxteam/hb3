<?php

if( ! function_exists('render_seo') ) {
    /**
     * Temporary function to allow us to render SEO for the website theme
     * TODO: Replace for a better way of register header/footer (CSS/JS/Meta) resources
     */
    function render_seo() {
        echo HB3Meta::generate();
        echo OpenGraph::generate();
    }
}

if( ! function_exists('render_breadcrumbs') ) {
    /**
     * [render_breadcrumbs description]
     * @param  [type] $page [description]
     * @return [type]       [description]
     */
    function render_breadcrumbs( $page = NULL ) {
        try {
            if( !empty($page) && !empty($page->breadcrumb) ):
                echo Breadcrumbs::render( (Request::path() == '/' ) ? 'website-breadcrumb' : str_replace("/", ".", Request::path() ) );
            endif;
        }
        catch(\Exception $e ) {
            if( site_is_in_debug_mode() ):
                Log::error( $e->getMessage() );
            endif;
        }
    }
}


if ( ! function_exists('clean_meta_robot_tags')):
	function clean_meta_robot_tags($Robots) {
        if( empty($Robots) ):
            return '';
        endif;

        $Robots = ( !empty($Robots) && is_string($Robots) ) ? ArrayHelper::cleanExplodedArray( explode(",", strtolower($Robots)) ) : ArrayHelper::cleanExplodedArray($Robots);

        // Available Meta Robot Items - https://yoast.com/robots-meta-tags/
        $AllowedRobotItems = ['index', 'noindex', 'follow', 'nofollow', 'none', 'all', 'noimageindex', 'noarchive', 'nocache', 'nosnippet', 'nositelinkssearchbox', 'nopagereadaloud', 'notranslate', 'max-snippet', 'max-video-preview', 'max-image-preview', 'rating', 'unavailable_after', 'noyaca'];

        $CleanedArray = array_filter($Robots,
            function($Robot) use($AllowedRobotItems) { return in_array($Robot, $AllowedRobotItems); 
        });

        return implode(", ", $CleanedArray);
	}
endif;

if( !function_exists('seo_clean_canonical_url') ):
    function seo_clean_canonical_url($URL = NULL) {
        $URL_Array = parse_url(trim($URL, "/"));

        if( empty($URL_Array['path']) ) {
            return NULL;
        }

        return $URL_Array['path'];
    }
endif;