<?php

if ( ! function_exists('validate_app_url') ) {
	/**
	 * Validate t
	 * @return String Version
	 */
	function validate_app_url() {
        // Check to make sure that the APP URL has either a http/https host defined - if not add it
        $website    = parse_url( config()->get('app.url') );

        if( !isset($website['host']) ):
            config()->set('app.url', 'https://' . config()->get('app.url') );
        endif;
	}
}


if ( ! function_exists('system_version') ) {
	/**
	 * Get the system version
	 * @return String Version
	 */
	function system_version() {
        return Hummingbird\Core\HummingbirdCMS::VERSION;
	}
}

if ( ! function_exists('site_is_in_debug_mode') ) {
	/**
	 * Is the site in debug mode?
	 * @return Boolean
	 */
	function site_is_in_debug_mode() {
		return Config::get('app.debug');
	}
}

if( !function_exists('is_site_restricted') ) {
	function is_site_restricted() {
		$RestrictedIPs = get_system_access_ip_addresses();
		$SettingsRestricted = false;
		$Restricted = ( strtoupper(env('APP_RESTRICTED')) === 'TRUE' );

		try {
			$SystemSettings = Hummingbird\Models\Setting::where('key', 'system')->firstOrFail();
			$Settings = $SystemSettings->value;

			if( isset($Settings['APP_RESTRICTED']) ):
				$SettingsRestricted = ( strtoupper($Settings['APP_RESTRICTED']) === 'TRUE' );
			endif;
		}
		catch(\Exception $e) {
			// silence - no key or no entries
		}

		$Restricted = ( $Restricted == true || $SettingsRestricted == true );

		// We're debugging on a IP access only level
		if( $Restricted && count($RestrictedIPs) > 0 ):
			$Restricted = ( isset($_SERVER["REMOTE_ADDR"]) ) ? !in_array($_SERVER["REMOTE_ADDR"], $RestrictedIPs) : FALSE;
		endif;

		return $Restricted;
	}
}


if( !function_exists('is_backend_restricted') ) {
	function is_backend_restricted() {
		$BackendRestrictedIPs = get_backend_access_ip_addresses();
		$BackendSettingsRestricted = false;
		$BackendRestricted = ( strtoupper(env('APP_BACKEND_RESTRICTED')) === 'TRUE' );

		try {
			$SystemSettings = Hummingbird\Models\Setting::where('key', 'system')->firstOrFail();
			$Settings = $SystemSettings->value;

			if( isset($Settings['APP_BACKEND_RESTRICTED']) ):
				$BackendSettingsRestricted = ( strtoupper($Settings['APP_BACKEND_RESTRICTED']) === 'TRUE' );
			endif;
		}
		catch(\Exception $e) {
			// silence - no key or no entries
		}

		$BackendRestricted = ( $BackendRestricted == true || $BackendSettingsRestricted == true );

		// We're debugging on a IP access only level
		if( $BackendRestricted && count($BackendRestrictedIPs) > 0 && isset($_SERVER["REMOTE_ADDR"]) ):
			$BackendRestricted = !in_array($_SERVER["REMOTE_ADDR"], $BackendRestrictedIPs);
		endif;

		return $BackendRestricted;
	}
}

if( !function_exists('get_system_access_ip_addresses') ) {
	function get_system_access_ip_addresses() {
		$DebugIPs = $ExtraDebugIPs = [];

		if( env('APP_RESTRICTED_IPS') && env('APP_RESTRICTED_IPS') != '' ):
			$DebugIPs = array_filter(ArrayHelper::cleanExplodedArray( explode(",", env('APP_RESTRICTED_IPS')) ), function( $IP_address ) {
				return filter_var($IP_address, FILTER_VALIDATE_IP);
			});
		endif;

		try {
			$SystemSettings = Hummingbird\Models\Setting::where('key', 'system')->firstOrFail();
			$Settings = $SystemSettings->value;

			if( isset($Settings['APP_RESTRICTED_IPS']) && $Settings['APP_RESTRICTED_IPS'] != '' ):
				$ExtraDebugIPs = array_filter(ArrayHelper::cleanExplodedArray( explode(",", $Settings['APP_RESTRICTED_IPS']) ), function( $IP_address ) {
					return filter_var($IP_address, FILTER_VALIDATE_IP);
				});
			endif;
		}
		catch(\Exception $e) {
			// silence - no key or no entries
		}

		return array_unique( array_merge($DebugIPs, $ExtraDebugIPs) );
	}
}


if( !function_exists('check_site_access') ) {
	function can_access_site() {
		if( is_site_restricted() ):
			$RestrictedURL = (env('APP_RESTRICTED_REDIRECT_TO') != '' && filter_var(env('APP_RESTRICTED_REDIRECT_TO'), FILTER_VALIDATE_URL) ) ? env('APP_RESTRICTED_REDIRECT_TO') : NULL;

			try {
				$SystemSettings = Hummingbird\Models\Setting::where('key', 'system')->firstOrFail();
				$Settings = $SystemSettings->value;

				if( isset($Settings['APP_RESTRICTED_REDIRECT_TO']) ):
					$RestrictedURL = ( $Settings['APP_RESTRICTED_REDIRECT_TO'] != '' && filter_var($Settings['APP_RESTRICTED_REDIRECT_TO'], FILTER_VALIDATE_URL) ) ? $Settings['APP_RESTRICTED_REDIRECT_TO'] : $RestrictedURL;
				endif;
			}
			catch(\Exception $e) {
				// silence - no key or no entries
			}

			if( !is_null($RestrictedURL) ):
				return Redirect::to( $RestrictedURL );
			endif;

			App::abort(403, 'Forbidden');
		endif;

		// check if backend is restricted
		if( Request::is('hummingbird', 'hummingbird/*') && is_backend_restricted() ):
			return Redirect::to( '/' );
		endif;

		return true;
	}
}

if( !function_exists('get_backend_access_ip_addresses') ) {
	function get_backend_access_ip_addresses() {
		$APP_BACKEND_RESTRICTED_IPS = $Extra_APP_BACKEND_RESTRICTED_IPS = [];

		if( env('APP_BACKEND_RESTRICTED_IPS') && env('APP_BACKEND_RESTRICTED_IPS') != '' ):
			$APP_BACKEND_RESTRICTED_IPS = array_filter(ArrayHelper::cleanExplodedArray( explode(",", env('APP_BACKEND_RESTRICTED_IPS')) ), function( $IP_address ) {
				return filter_var($IP_address, FILTER_VALIDATE_IP);
			});
		endif;

		try {
			$SystemSettings = Hummingbird\Models\Setting::where('key', 'system')->firstOrFail();
			$Settings = $SystemSettings->value;

			if( isset($Settings['APP_BACKEND_RESTRICTED_IPS']) && $Settings['APP_BACKEND_RESTRICTED_IPS'] != '' ):
				$Extra_APP_BACKEND_RESTRICTED_IPS = array_filter(ArrayHelper::cleanExplodedArray( explode(",", $Settings['APP_BACKEND_RESTRICTED_IPS']) ), function( $IP_address ) {
					return filter_var($IP_address, FILTER_VALIDATE_IP);
				});
			endif;
		}
		catch(\Exception $e) {
			// silence - no key or no entries
		}

		return array_unique( array_merge($APP_BACKEND_RESTRICTED_IPS, $Extra_APP_BACKEND_RESTRICTED_IPS) );
	}
}

if( !function_exists('get_system_php_location') ) {
	/**
	 * Get the path for the system PHP version
	 * @return String
	 */
	function get_system_php_location() {
		if( env('PHP_LOCATION') && trim(env('PHP_LOCATION')) != '' ) {
			return trim( env('PHP_LOCATION') );
		}

		return 'php';
	}
}

if( !function_exists('get_system_composer_location') ) {
	/**
	 * Get the path for the system PHP version
	 * @return String
	 */
	function get_system_composer_location() {
		if( env('COMPOSER_LOCATION') && trim(env('COMPOSER_LOCATION')) != '' ) {
			return trim( env('COMPOSER_LOCATION') );
		}

		return 'composer';
	}
}

if( !function_exists('get_system_artisan_location') ) {
	/**
	 * Get the path for the system PHP version
	 * @return String
	 */
	function get_system_artisan_location() {
		if( env('ARTISAN_LOCATION') && trim(env('ARTISAN_LOCATION')) != '' ) {
			return trim( env('ARTISAN_LOCATION') );
		}

		return 'artisan';
	}
}

if ( ! function_exists('uploads_path'))
{
	/**
	 * Get the path to the uploads folder.
	 *
	 * @param  string  $path
	 * @return string
	 */
	function uploads_path($path = '')
	{
		$path = ltrim( $path, "/");
		return storage_path( 'app/public' ) . "/{$path}";
	}
}


if( !function_exists('is_2fa_forced') ):
	function is_2fa_forced() {
		try {
			$SystemSettings = Hummingbird\Models\Setting::where('key', 'system')->firstOrFail();
			$Settings = $SystemSettings->value;

			if( isset($Settings['APP_2FA_FORCE']) ):
				return ( strtoupper($Settings['APP_2FA_FORCE']) === 'TRUE' );
			endif;
		}
		catch(\Exception $e) {
			// silence - no key or no entries
		}

		return false;
	}
endif;