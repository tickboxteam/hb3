<?php namespace Hummingbird\Classes\Permissions;

use View;
use Hummingbird\Models\Page;
use Hummingbird\Models\Role;
use Hummingbird\Models\User;

class PagePermissionsRetrieve {
    public function handle($Object, $Permission) {
        if (!$Object instanceof Role && !$Object instanceof User) {
            return;
        }

        return View::make('HummingbirdBase::cms.partials.permissions.pages', array(
            'Pages' => Page::whereNull('parentpage_id')->orderBy('position')->get(),
            'Permission' => $Permission,
            'Object' => $Object,
            'ObjectPermissions' => $Object->objectPermissions()->type(get_class(new Page))->pluck('item_id')->all()
        ));
    }
}
