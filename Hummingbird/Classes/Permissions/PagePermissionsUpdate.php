<?php namespace Hummingbird\Classes\Permissions;

use Request;
use Hummingbird\Models\ObjectPermission;
use Hummingbird\Models\Page;
use Hummingbird\Models\Permission;
use Hummingbird\Models\Role;
use Hummingbird\Models\User;

class PagePermissionsUpdate {
    public function handle($Object) {
        $Object->objectPermissions()->type(get_class(new Page))->delete();

        if ((!$Object instanceof Role && !$Object instanceof User) || !$Object->hasPermissionAction('page.read')) {
            $Object->detachPermission(Permission::where('name', 'page.filterPages')->first());
            return;
        }

        if(Request::filled('page_perms')) {
            $objectPermissions = array();

            foreach(Request::get('page_perms') as $Cat_Perm) {
                $objectPermissions[] = new ObjectPermission([
                    'item_id'       => $Cat_Perm,
                    'item_type'     => get_class(new Page),
                    'reference_type' => get_class($Object),
                    'reference_id'  => $Object->id
                ]);
            }

            if(count($objectPermissions) > 0) {
                $Object->objectPermissions()->saveMany($objectPermissions);
            }
        }
    }
}
