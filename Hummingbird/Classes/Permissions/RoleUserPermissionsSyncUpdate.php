<?php namespace Hummingbird\Classes\Permissions;

use Log;
use Hummingbird\Models\ObjectPermission;

class RoleUserPermissionsSyncUpdate {
    public function handle($Role) {
        foreach( $Role->users as $User ) {
            $permissions = array_unique(array_merge($User->permissions()->pluck('permission_id')->all(), $Role->perms()->pluck('permission_id')->all()));

            $User->permissions()->sync($permissions);

            // Object Permissions
            $ObjectPermissions      = array();
            $ObjectPermissionsTypes = array();
            $UserObjectPermissions  = array();

            // Retain original permissions for Objects on user
            foreach($User->objectPermissions()->get() as $UserObjectPermission) {
                if(!isset($ObjectPermissionsTypes[ $UserObjectPermission->item_type ]) || !in_array($UserObjectPermission->item_id, $ObjectPermissionsTypes[ $UserObjectPermission->item_type ])) {
                    
                    $ObjectPermissions[] = new ObjectPermission([
                        'item_id'       => $UserObjectPermission->item_id,
                        'item_type'     => $UserObjectPermission->item_type,
                        'reference_type' => get_class($User),
                        'reference_id'  => $User->id
                    ]);

                    $ObjectPermissionsTypes[ $UserObjectPermission->item_type ][] = $UserObjectPermission->item_id;
                }
            }

            // New permissions, if not set already.
            $RoleObjectPermissions  = $Role->objectPermissions()->get();

            foreach($RoleObjectPermissions as $RoleObjectPermission) {
                if(!isset($ObjectPermissionsTypes[ $RoleObjectPermission->item_type ]) || !in_array($RoleObjectPermission->item_id, $ObjectPermissionsTypes[ $RoleObjectPermission->item_type ])) {
                    
                    $ObjectPermissions[] = new ObjectPermission([
                        'item_id'       => $RoleObjectPermission->item_id,
                        'item_type'     => $RoleObjectPermission->item_type,
                        'reference_type' => get_class($User),
                        'reference_id'  => $User->id
                    ]);

                    $ObjectPermissionsTypes[ $RoleObjectPermission->item_type ][] = $RoleObjectPermission->item_id;
                }
            }

            $User->objectPermissions()->delete();
            $User->objectPermissions()->saveMany($ObjectPermissions);
        }
    }
}