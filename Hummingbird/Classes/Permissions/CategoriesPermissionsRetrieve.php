<?php namespace Hummingbird\Classes\Permissions;

use View;
use Hummingbird\Models\Categories;
use Hummingbird\Models\Role;
use Hummingbird\Models\User;

class CategoriesPermissionsRetrieve {
    public function handle($Object, $Permission) {
        if (!$Object instanceof Role && !$Object instanceof User) {
            return;
        }
        
        return View::make('HummingbirdBase::cms.partials.permissions.categories', array(
            'Categories' => Categories::type()->whereNull('parent')->orderBy('name')->get(),
            'Permission' => $Permission,
            'Object' => $Object,
            'ObjectPermissions' => $Object->objectPermissions()->type(get_class(new Categories))->pluck('item_id')->all()
        ));
    }
}
