<?php namespace Hummingbird\Classes\Permissions;

use Request;
use Hummingbird\Models\Categories;
use Hummingbird\Models\ObjectPermission;
use Hummingbird\Models\Permission;
use Hummingbird\Models\Role;
use Hummingbird\Models\User;

class CategoriesPermissionsUpdate {
    public function handle($Object) {
        $Object->objectPermissions()->type(get_class(new Categories))->delete();

        if(Request::filled('category_perms')) {
            $objectPermissions = array();

            foreach(Request::get('category_perms') as $Cat_Perm) {
                $objectPermissions[] = new ObjectPermission([
                    'item_id'       => $Cat_Perm,
                    'item_type'     => get_class(new Categories),
                    'reference_type' => get_class($Object),
                    'reference_id'  => $Object->id
                ]);
            }

            if(count($objectPermissions) > 0) {
                $Object->objectPermissions()->saveMany($objectPermissions);
            }
        }
    }
}
