<?php namespace Hummingbird\Classes;

use App;
use BackendMenu;
use Config;
use DB;
use Exception;
use File;
use FatalErrorException;
use General;
use Request;
use Hummingbird\Database\Seeds\HB3Seeder;
use Illuminate\Database\Migrations\Migration;

/**
 * Plugin base class looking after plugin builds
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */

//  https://octobercms.com/docs/database/structure
// https://octobercms.com/docs/plugin/updates#migration-seed-files

class PluginBase {
	private $MIGRATIONS 			= '/migrations';
	private $SEEDS 					= '/seeds';

	public $completed_upgrades 		= [];
    public $admin_menu_items        = [];
    public $admin_menu_groups       = [];


    public function boot() {
        if( Request::segment(1) == 'hummingbird' ) {
            BackendMenu::registerBackendGroups($this->getAdminNavigationGroups());
            BackendMenu::registerBackendNavigationItems($this->getAdminNavigation());
        }
    }


    public function getCurrentVersion() {
        return $this->package['current_version'];
    }

/**

Steps to installing a plugin are:

1) Migrate files
2) Seeds
3) Permissions (if needed)
4) Navigation items (if needed)


Steps to uninstalling (not deactivating) are:

1) Removing permissions (from roles and users)
2) Removing navigation items
3) Remove data (if necessary - settings, etc)
4) Remove migrations

*/
    
    /**
     * Return an appropriate list of functions for the installer.
     * 
     * @param String $INSTALLED_VERSION
     * @param Array $VERSIONS
     * @return Array $NEW_VERSIONS
     */
	protected function get_latest_install_versions($INSTALLED_VERSION, $VERSIONS)
	{
		// No versions installed - return all
		if(null === $INSTALLED_VERSION) return (array) $VERSIONS;

		$NEW_VERSIONS = (array) $VERSIONS;

		// Find the position of the key you're looking for.
		$position = array_search($INSTALLED_VERSION, array_keys($NEW_VERSIONS));

		// If a position is found, splice the array.
		if ($position !== false) {
		    return array_splice($NEW_VERSIONS, ($position + 1));
		}

		return $NEW_VERSIONS;
	}

	public function set_current_directory()
	{
		$this->current_directory = $this->package['path'] . '/src/versions/' . $this->current_version;
	}

	public function updater()
	{
		$this->update_started = time();
		$this->emptyLoader();

		$INSTALLED_VERSION 	= (isset($this->package['installed_version'])) ? $this->package['installed_version']:NULL;
		$CURRENT_VERSION	= $this->package['stable_version'];
		$VERSIONS 			= $this->get_latest_install_versions($INSTALLED_VERSION, $this->package['versions']);

		foreach($VERSIONS as $_VERSION_KEY => $_COMMENT)
		{
			$this->current_version = $_VERSION_KEY;
			$this->set_current_directory();

			if (File::exists($this->current_directory))
			{
				try
				{
                    // TODO: This will just allow us to do large DB changes
                    ini_set('memory_limit', '-1');
                    set_time_limit(0);


					$this->runMigrations();

					DB::transaction(function() {
						$this->runSeeds();
					});
				}
				catch (Exception $e)
				{
					\Log::error($e);
					throw $e;
				}
			}
			else
			{
				// add to database plugin_version_history table
				$this->recordNote(null, $_COMMENT);
			}
		}

		$this->emptyLoader();

		return TRUE;
	}

	public function runMigrations()
	{
		if(File::exists($this->current_directory . $this->MIGRATIONS))
		{
			$files = File::allFiles($this->current_directory . $this->MIGRATIONS);
			
			usort($files, function($a, $b) {
				$a = $a->getFilename();
				$b = $b->getFileName();

				if ($a == $b) {
					return 0;
				}
				return ($a < $b) ? -1 : 1;
			});

			foreach($files as $file)
			{
                $object = $this->resolve($file); // /Applications/MAMP/htdocs/HUMMINGBIRD3/core/workbench/tickbox/blog/src/versions/1.0.0/migrations/2017_03_08_123752_create_post_table.php

                if ($object instanceof Migration)
                {
                	$object->up();

		            // add to database plugin_version_history table
		            $this->recordNote($file, 'migrations');
                }
			}
		}
	}

	public function runSeeds()
	{
		if(File::exists($this->current_directory . $this->SEEDS))
		{
			$files = File::allFiles($this->current_directory . $this->SEEDS);
			
			usort($files, function($a, $b) {
				$a = $a->getFilename();
				$b = $b->getFileName();

				if ($a == $b) {
					return 0;
				}
				return ($a < $b) ? -1 : 1;
			});

			foreach($files as $file)
			{
                $object = $this->resolve($file);

                if ($object instanceof HB3Seeder)
                {
                	$object->up(array('package' => $this->package));

		            // add to database plugin_version_history table
		            $this->recordNote($file, 'seeds');
                }
			}
		}
	}

	public function reset($all = null)
	{
		$toreset = DB::table('system_plugin_history')->where('namespace', $this->package['namespace'])->orderBy('id', 'DESC');

        if(null === $all AND isset($this->update_started)) {
            $toreset->where('created_at', '>=', $this->update_started);
        }

		if( $toreset->count() > 0 )
		{
			foreach($toreset->get() as $item)
			{
				if(($item->type == 'migrations' || $item->type == 'seeds') && strpos($item->note, ".php") !== FALSE)
				{
					$this->current_version = $item->version;
					$this->set_current_directory();

					$file 	= $this->current_directory . '/' . $item->type . '/'. $item->note;
		            $object = $this->resolve($file);

		            if ($object instanceof Migration)
		            {
		            	if(method_exists($object, 'down')) $object->down();
		            }
		            else if($object instanceof HB3Seeder)
		            {
		            	if(method_exists($object, 'down')) $object->down(array('package' => $this->package));
		            }
	            }
			}
		}

		DB::table('system_plugin_history')->where('namespace', $this->package['namespace'])->delete();

		$this->package['installed_version'] = NULL;
	}


	public function emptyLoader()
	{
		$this->current_directory 	= NULL;
		$this->completed_upgrades 	= [];
	}


	public function recordNote($file = null, $comment = 'Plugin upgrade')
	{
		$note 		= (null !== $file) ? basename($file):$comment;
		$comment 	= (null !== $file) ? $comment:'Plugin upgrade';

		DB::table('system_plugin_history')->insert(
			array(
				'namespace' => $this->package['namespace'],
				'type' 		=> $comment,
				'version'	=> $this->current_version,
				'note' 		=> $note,
				'created_at' => date("Y-m-d H:i:s")
			)
		);
	}

    /**
     * Resolve a migration instance from a file.
     * @param  string  $file
     * @return object
     */
    public function resolve($file)
    {
        if (!File::isFile($file))
            return;

        require_once $file;
        if ($class = $this->getClassFromFile($file))
            return new $class;
    }

    /**
     * Extracts the namespace and class name from a file.
     * @param string $file
     * @return string
     */
    public function getClassFromFile($file)
    {
        $fileParser = fopen($file, 'r');
        $class = $namespace = $buffer = '';
        $i = 0;

        while (!$class) {
            if (feof($fileParser))
                break;

            $buffer .= fread($fileParser, 512);
            $tokens = token_get_all($buffer);

            if (strpos($buffer, '{') === false)
                continue;

            for (; $i < count($tokens); $i++) {

                /*
                 * Namespace opening
                 */
                if ($tokens[$i][0] === T_NAMESPACE) {

                    for ($j = $i + 1; $j < count($tokens); $j++) {
                        if ($tokens[$j] === ';')
                            break;

                        $namespace .= is_array($tokens[$j]) ? $tokens[$j][1] : $tokens[$j];
                    }

                }

                /*
                 * Class opening
                 */
                if ($tokens[$i][0] === T_CLASS) {

                    for ($j = $i + 1; $j < count($tokens); $j++) {
                        if ($tokens[$j] === '{' && isset($tokens[$i+2][1]) ) {
                            $class = $tokens[$i+2][1];
                            break;
                        }
                    }

                }

            }
        }

        if (!strlen(trim($namespace)) && !strlen(trim($class)))
            return false;

        return trim($namespace) . '\\' . trim($class);
    }
    
    /**
     * [hasSetting description]
     * @param  [type]  $setting_name [description]
     * @return boolean               [description]
     */
    public function getSettings() {
        return $this->settings;
    }

    /**
     * [hasSetting description]
     * @param  [type]  $setting_name [description]
     * @return boolean               [description]
     */
    public function hasSetting($setting_name)
    {
    	if(isset($this->settings) && isset($this->settings[$setting_name]) && !empty($this->settings[$setting_name]))
    		return TRUE;

    	return NULL;
	}


    /**
     * [hasSetting description]
     * @param  [type]  $setting_name [description]
     * @return boolean               [description]
     */
    public function getSetting($setting_name)
    {
    	if(isset($this->settings) && isset($this->settings[$setting_name]))
    		return $this->settings[$setting_name];

    	return NULL;
	}

    /**
     * Return the package namespace
     * 
     * @return String
     */
    public function getPackageNamespace() {
        if(isset($this->package) && isset($this->package['namespace'])) {
            return $this->package['namespace'];
        }
    }

    /**
     * Return the package name (if available)
     * 
     * @return String
     */
    public function getVendorName() {
        if(isset($this->package) && isset($this->package['vendor'])) {
            return $this->package['vendor'];
        }
    }

    /**
     * Return the package name (if available)
     * 
     * @return String
     */
    public function getPackageName() {
        if(isset($this->package) && isset($this->package['package_name'])) {
            return $this->package['package_name'];
        }
    }

    /**
     * Get the User Administrator Navigation (if there are any)
     * @return Array
     */
    public function getAdminNavigation() {
        return config( $this->getConfigAlias() . '-admin.navigation_items') ?: [];
    }

    /**
     * Get the User Administrator Navigation Groups (if there are any)
     * @return Array
     */
    public function getAdminNavigationGroups() {
        return config( $this->getConfigAlias() . '-admin.navigation_groups') ?: [];
    }

    /**
     * Configuration alias
     * @return [type] [description]
     */
    public function getConfigAlias() {
        if( isset($this->package) && isset($this->package['namespace']) ) {
            return strtolower( str_replace("\\", "-", $this->package['namespace']) );
        }
    }
}
