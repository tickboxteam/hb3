<?php namespace Hummingbird\Classes;

use App;
use Config;
use DB;
use File;
use General;
use Log;
use Schema;
use Hummingbird\Interfaces\HB3MigratorInterface;
use Hummingbird\Models\Activitylog;
use Hummingbird\Models\MediaCollection;
use Hummingbird\Models\MigratedMediaItem;
use Hummingbird\Traits\HB3MigratorTrait;

/**
 * Hummingbird3 Migrator class to help data imports
 * from alternative systems.
 */
class HB3Migrator implements HB3MigratorInterface {
    use HB3MigratorTrait;

    /**
     * Are we debugging outputs?
     * @var Boolean
     */
    public $debug = false;

    /**
     * Base import path for all resources
     * @var String
     */
    public $import_path;

    /**
     * Import path for SQL
     * @var String
     */
    public $import_database;

    /**
     * The tables that we're going to import
     * For renaming and rolling back
     * @var array
     */
    public $import_tables = [];

    /**
     * Old prefix key for tables imported
     * @var string
     */
    public $old_prefix_key = '';

    /**
     * The base media collection where sub collections can be created
     * @var MediaCollection
     */
    public $media_parent_collection;

    /**
     * The base media collection name
     * @var string
     */
    public $base_media_parent_collection_name = 'Migration';

    /**
     * Inititalise the Migration class
     */
    public function __construct() {
        $this->import_path      = base_path() . "/import";
        $this->import_database  = $this->import_path . '/import.sql';
        $this->setBaseCollection();
    }

    /**
     * The database we're importing
     *
     * @throws Exception
     */
    public function importDatabase() {
        if( $this->debug ):
            echo 'Importing Database...';
        endif;

        if( !File::exists($this->import_database) ):
            throw new \Exception("Database file could not be found.");
        endif;

        DB::unprepared(file_get_contents($this->import_database));
        
        $this->renameOldTables();

        if( $this->debug ):
            echo 'Database migrated' . PHP_EOL;
        endif;
    }

    /** 
     * Rename, the newly imported tables, so we can access them
     */
    public function renameOldTables()
    {
        $system_tables = DB::connection()->getDoctrineSchemaManager()->listTableNames();

        foreach($this->tables as $original_table_name):
            if( in_array($original_table_name, $system_tables) ):
                $newname = Config::get('database.connections.mysql.prefix') . $this->old_prefix_key . $original_table_name;

                if( $this->debug ):
                    echo 'Renaming table: ' . $original_table_name . PHP_EOL;
                endif;

                DB::statement("RENAME TABLE $original_table_name TO $newname");
            endif;
        endforeach;
    }

    /**
     * On rollback, lets remove all of the tables we've imported
     */
    public function rollback() {
        foreach( $this->tables as $table ):
            $TableName = Config::get('database.connections.mysql.prefix') . $this->old_prefix_key . $table;

            DB::statement("DROP TABLE IF EXISTS $TableName");
            Schema::dropIfExists($TableName);

        endforeach;

        if( $this->debug ):
            echo 'Import complete and import tables removed' . PHP_EOL;
        endif;
    }

    /**
     * Collections help us organise files and images. Create the 
     * initial migration collection (or retrieve it)
     */
    public function setBaseCollection() {
        $this->media_parent_collection = MediaCollection::updateOrCreate(['name' => $this->base_media_parent_collection_name], [
            'description' => 'Used for storing files/images from the import',
            'permalink' => str_slug($this->base_media_parent_collection_name),
        ]);
    }

    /**
     * Create a new/empty collection
     * 
     * @param  Array $data
     * @return MediaCollection
     */
    public function getOrCreateCollection($data) {
        $Collection = MediaCollection::updateOrCreate(['name' => $data['name']], $data);

        if( $Collection->wasRecentlyCreated ) {
            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($Collection),
                'link_id' => $Collection->id,
                'url' => General::backend_url() . "/media/view/" . $Collection->id,
                'description' => 'Created collection',
                'notes' => "System created a new collection &quot;" . $data['name'] . "&quot;"
            ]);
        }

        return $Collection;
    }


    /**
     * Processing files that exist in content blocks to be imported. 
     * This is for: images, documents, video, audio etc
     * 
     * @param  String $html
     * @param  MediaCollection $MediaCollection
     * @return String
     */
    public function processFiles($html, $MediaCollection) {
        if( empty($html) ):
            return $html;
        endif;

        $doc = new \DOMDocument();
        @$doc->loadHTML($html);

        // Images
        $images = $doc->getElementsByTagName('img');

        if ($images->length > 0) { 
            foreach ($images as $image) {
                $image_src  = $this->processFile($image->getAttribute('src'), $MediaCollection);
                $html       = str_replace($image->getAttribute('src'), $image_src, $html);
            }
        }

        // Everything else
        $files = $doc->getElementsByTagName('a');

        if ($files->length > 0) { 
            foreach ($files as $file) {
                if( !empty($file) && !empty($file->getAttribute('href')) && strpos('mailto:', $file->getAttribute('href'))) {
                    $file_src   = $this->processFile($file->getAttribute('href'), $MediaCollection);
                    $html       = str_replace($file->getAttribute('href'), $file_src, $html);
                }
            }
        }

        return $html;
    }   


    /**
     * Processing single files and moving them to the correct
     * server location as well as adding them to the CMS
     * for managing later on
     * 
     * @param  String $filename
     * @param  MediaCollection $MediaCollection
     * @return String
     */
    public function processFile($filename, $MediaCollection) {
        if(!empty($filename)) {
            $filename = trim(General::clean_unicode($filename));

            // Have we migrated this?
            $MigratedMediaItem = MigratedMediaItem::bySource($filename)->first();

            if( null !== $MigratedMediaItem && File::exists(base_path() . $MigratedMediaItem->destination) ) {
                return $MigratedMediaItem->destination;
            }

            $base_file  = $this->import_path . '/' . ltrim($filename, "/");
            if(File::exists( $base_file ) ) {
                try {
                    $resource   = App::make('Hummingbird\Controllers\MediaController')->uploadFile_byFunction($base_file, $MediaCollection->id);

                    if($resource !== false) {
                        MigratedMediaItem::create([
                            'source'        => $filename,
                            'destination'   => $resource
                        ]);

                        return $resource;
                    }
                }
                catch(\Exception $e) {
                    Log::error($e->getMessage());
                }
            }
        }

        return $filename;
    }
}