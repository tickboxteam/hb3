<?php namespace Hummingbird;

use Hummingbird\Libraries\PluginManager;
use Illuminate\Support\ServiceProvider;

class PluginManagerServiceProvider extends ServiceProvider {
 
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    public function boot() {
        if( config()->get('hb3-hummingbird.install') ):
            $PluginManager = $this->app->make('plugin.manager');

            // Register other plugin Service Providers in a loop here?
            foreach ($PluginManager->get_active_plugins() as $plugin) {
                $ServiceProvider = $PluginManager->getServiceProvider($plugin->namespace);

                if(null !== $ServiceProvider) {
                    $this->app->register( $ServiceProvider );
                }
            }
        endif;
    }


    public function register() {
        $this->app->singleton('plugin.manager', function($app) {
            return new PluginManager;
        });
	}
}