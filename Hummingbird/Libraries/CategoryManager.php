<?php namespace Hummingbird\Libraries;

use Hummingbird\Models\Categories;

/**
 * 
 * 
 *
 * @author
 * @copyright 
 * @license 
 * @package 
 */

class CategoryManager
{
    /**
     * 
     *
     *
     */
    public function __construct()
    {
        $this->data['list_cats'] = array();
        $this->data['cats'] = Categories::whereNull('deleted_at')->type()->orderBy('name', 'ASC')->orderby('id', 'ASC')->get();

        $this->data['deleted_cats'] = Categories::deletedCategories(true);

        $this->buildTree($this->data['cats']);
    }


    /**
     * 
     *
     *
     */
    public function run()
    {
        return $this->data['list_cats'];
    }


    /**
     * 
     *
     *
     */
    public function buildTree($elements, $parentId = NULL, $level = 0) 
    {
        foreach ($elements as $key => $element)
        {
            /* Top level parent */
            if ($element->parent !== $parentId) continue;

            // $element['name'] = ($level > 0) ? '&#8627;'.$element['name']:$element['name']; //append arrow to name
            $new_name = ($level > 0) ? str_repeat('&nbsp;', ($level * 3) * 2) . '<i class="fa fa-long-arrow-right"></i> ' . $element['name']: $element['name'];
            $element['name'] = $new_name;

            // $element['name'] = ($level > 0) ? '&nbsp;'.$element['name']:$element['name']; //append arrow to name
            $this->data['list_cats'][$element->id] = $element;
            $this->data['list_cats'][$element->id]['level'] = $level;
            $this->buildTree($elements, $element->id, $level + 1);
        }
    }
}
