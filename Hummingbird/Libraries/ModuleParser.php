<?php namespace Hummingbird\Libraries;

use ArrayHelper;
use Config;
use File;
use General as Hb3General;
use Log;
use View;
use Hummingbird\Models\Media;
use Hummingbird\Models\Module;
use Hummingbird\Models\Moduletemplate;

/**
 * Hummingbird3 ModuleParser and Handler
 *
 * @package hummingbird
 * @author Daryl Phillips
 * @version 1.0.1
 *
 */
class ModuleParser
{
    protected $module;

    protected $module_template;

    protected $module_bag;

    protected $loop_tags_pattern = '#\{{(.*?)\}}#';

    protected $loop_settings_pattern = '#\[(.*?)\]#';

    protected $loop_loops_pattern = '#\{(loop:[a-zA-Z0-9\_\:\|\=\,\.\ ]+)\}(.+)\{\/loop\}#';

    protected $loop_loops_tags_pattern = '#\{(.*?)\}#';

    protected $fields = array();

    protected $field_counts;

    protected $characters_to_remove = array("{", "}", "[", "]", ":");

    protected $tabs = false;

    public $tabs_data = array('min' => 1, 'fields_used' => array(), 'fields' => array());

    public function __construct(Moduletemplate $module_template, Module $module, $page = null, $attributes = [])
    {
        $this->theme    = Themes::activeTheme();
        $this->template_dir = base_path() . '/' . Hb3General::theme_path() . '/' . $this->theme . '/modules/';

        $this->module   = $module;
        $this->module_template = $module_template;

        if(isset($page))
        {
            $this->page = $page;
        }

        $this->attributes = $attributes;

        $this->parseTemplate();
    }


    /**
     * Clean the template, loop through and build the tags  
     */ 
    public function parseTemplate()
    {
        $this->cleanTemplate();
        $this->loop_tags();
        $this->loop_loops();
    }


    /**
     * Cleaning the templates
     */ 
    public function cleanTemplate()
    {
        $this->module_template->html = str_replace(array("\r\n", "\r", "\n"), "", $this->module_template->html);
    }


    /**
     * Removing standardised tags for the template builder from view
     */ 
    public function cleanTag($tag, $remove = array())
    {
        $remove = array_merge($remove, $this->characters_to_remove);

        return str_replace($remove, "", $tag);
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function get_var_by_key($key = false)
    {
        if(isset($key) AND isset($this->$key)) return $this->$key;

        return false;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function loop_tags()
    {
        $loop_tags = array();

        /* get all types */
        preg_match_all($this->loop_tags_pattern, $this->module_template->html, $loop_tags);

        if(count($loop_tags) > 0)
        {   
            $this->field_counts = count($loop_tags[1]);

            foreach($loop_tags[1] as $key => $tag)
            {
                $this->loop_settings($key, $tag);
            }
        }
    }

    public function loop_loops()
    {
        $loops = array();

        /* get all types */
        preg_match_all($this->loop_loops_pattern, $this->module_template->html, $loops);

        if(count($loops) > 0)
        {
            foreach($loops[1] as $key => $tag)
            {
                if(strpos($tag, 'loop:tabs') !== false)
                {
                    $this->tabs = true;
                    $loop_tags = array();

                    $loops_settings = explode("|", $tag);

                    if(count($loops_settings) > 0)
                    {
                        foreach($loops_settings as $loop_setting)
                        {
                            if($loop_setting != '')
                            {
                                if(strpos($loop_setting, ":") !== false)
                                {
                                    $loop_info = explode(':', $loop_setting);
                                }
                                else if(strpos($loop_setting, "=") !== false)
                                {
                                    $loop_info = explode('=', $loop_setting);
                                }
                                
                                $this->tabs_data[$loop_info[0]] = $loop_info[1];
                            }
                        }
                    }


                    /* get all types */
                    preg_match_all($this->loop_loops_tags_pattern, $loops[2][$key], $loop_tags);

                    if(count($loop_tags) > 0)
                    {   
                        foreach($loop_tags[1] as $key => $tag)
                        {
                            $original_tag = $tag;
                            $settings_arr = $this->loop_settings($key, $tag, false);
                            $settings = $settings_arr['settings'];

                            $tag = $settings_arr['tag'];

                            if(!isset($settings['duplicate']) OR $settings['duplicate'] != 'true')
                            {

                                if(isset($this->tabs_data['fields_used']) AND in_array($tag, $this->tabs_data['fields_used']))
                                {
                                    $counter = 1;

                                    while(in_array($tag . '_' . $counter, $this->tabs_data['fields_used']))
                                    {
                                        $counter++;
                                    }

                                    $tag .= '_' . $counter;
                                }

                                $settings['name'] = $settings['id'] = $tag;
                                $settings['default'] = (isset($settings_arr['settings']['default'])) ? $settings_arr['settings']['default']:'';
                                $settings['original_name'] = $original_tag;
                                $settings['wysiwyg'] = (isset($settings_arr['settings']['wysiwyg']) && $settings_arr['settings']['wysiwyg'] == 'true') ? TRUE : FALSE;

                                $this->tabs_data['fields_used'][] = $tag;

                                $this->tabs_data['fields'][][$tag] = array(
                                    'settings' => $settings
                                ); 
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function loop_settings($key, $original_tag, $callback = true)
    {
        $settings_matches = array();
        $settings_arr = array();
        $tag = $original_tag;

        /* check for any settings */
        preg_match_all($this->loop_settings_pattern, $tag, $settings_matches);
        
        if(count($settings_matches[0]) > 0)
        {
            $settings = str_replace(array("[", "]"), "", $settings_matches[0][0]);
            $settings = explode(":", $settings);

            foreach($settings as $setting)
            {
                $field = explode("=", $setting);
                $settings_arr[$field[0]] = isset($field[1]) ? $field[1] : '';

                if($field[0] == 'options')
                {
                    $settings_arr['options'] = [];
                    $settings_tmp = ArrayHelper::cleanExplodedArray(explode(",", $field[1]));

                    foreach($settings_tmp as $key => $ind_setting)
                    {
                        if(strpos($ind_setting, "*") !== FALSE)
                        {
                            $option_key = ArrayHelper::cleanExplodedArray(explode("*", $ind_setting));
                            $settings_arr['options'][$option_key[0]] = $option_key[1];
                        }
                        else
                        {
                            $settings_arr['options'] = $ind_setting;
                        }
                    }
                }

                $tag = str_replace($setting, "", $tag);
            } 
        }

        $settings_arr['original_name'] = $original_tag;

        $tag = $this->cleanTag($tag);

        if($callback)
        {
            return $this->addToBag($tag, $settings_arr);
        }
        
        return array('tag' => $tag, 'settings' => $settings_arr);
    }

    public function addToBag($tag, $settings)
    {
        if(!isset($settings['duplicate']) OR (isset($settings['duplicate']) AND $settings['duplicate'] != 'true'))
        {
            if(isset($this->fields) AND in_array($tag, $this->fields))
            {
                $counter = 1;

                while(in_array($tag . '_' . $counter, $this->fields))
                {
                    $counter++;
                }

                $tag .= '_' . $counter;
            }
        
            $settings['name'] = $settings['id'] = $tag;
            $settings['default'] = (isset($settings['default'])) ? $settings['default']:'';
            $settings['no_output'] = (isset($settings['no_output']) AND $settings['no_output'] == 'true') ? true:false;
            $settings['wysiwyg'] = (isset($settings['wysiwyg']) && $settings['wysiwyg'] == 'true') ? TRUE : FALSE;

            $this->fields[] = $tag;

            $this->module_bag['fields'][][$tag] = array(
                'settings' => $settings
            );
        }
    }

    public function hasTabs()
    {
        return ($this->tabs) ? true:false;
    }


    public function get_field_title($tag, $settings)
    {
        if(isset($settings['title']) AND $settings['title'] != '')
        {
            return $settings['title'];
        }

        return $tag;
    }

    public function get_field_default($tag, $settings, $current_val = '')
    {
        if($current_val == '' AND isset($settings['default']) AND $settings['default'] != '')
        {
            return $current_val;
        }
    }


    /**
     * 
     * 
     *
     * @param    
     * @return   
     *
     */ 
    public function render()
    {
        if(null !== $this->module)
        {
            $module_template = $this->module_template->html;
            $module_data = $this->module->module_data;

            /* Render Static Fields */
            if( isset($this->module_bag['fields']) && count($this->module_bag['fields']) > 0)
            {
                foreach($this->module_bag['fields'] as $items)
                {
                    foreach($items as $tag => $item)
                    {
                        $new_html = '';
                        $replace_tag = $item['settings']['original_name'];

                        if(isset($module_data['structure'][$item['settings']['name']]) AND $module_data['structure'][$item['settings']['name']] != '' AND isset($item['settings']['no_output']) && $item['settings']['no_output'] === FALSE)
                        {
                            switch($tag)
                            {
                                case 'image':
                                    if(isset($item['settings']['is_bg']) AND $item['settings']['is_bg'] == 'true')
                                    {
                                        $new_html = $module_data['structure'][$item['settings']['name']];
                                    }
                                    else
                                    {
                                        // TODO: Needs a little more work and perhaps could be used as a partial?
                                        $Image  = Media::where('location', $module_data['structure'][$item['settings']['name']])->first();
                                        $AltText = (isset($module_data['structure']['title'])) ? $module_data['structure']['title'] : NULL;
                                        $AltText = ($Image && !empty($Image->alt)) ? $Image->alt : $AltText; 

                                        ob_start();?>

                                        <img src="<?php echo $module_data['structure'][$item['settings']['name']]?>" <?php if(!empty($item['settings']['classes'])):?> class="<?php echo $item['settings']['classes']?>" <?php endif;?> <?php if(!empty($item['settings']['id'])):?> id="<?php echo $item['settings']['id']?>" <?php endif;?> <?php if(!empty($AltText)):?> alt="<?php echo $AltText?>" <?php endif;?> loading="lazy"/>

                                        <?php $new_html = ob_get_clean();
                                    }
                                break;
                                case 'dropdown':
                                    $new_html = $module_data['structure']['dropdown'];
                                    break;
                                default:
                                    $new_html = $module_data['structure'][$item['settings']['name']];
                                    break;
                            }
                        }
                        
                        //FIXME: Fix issues surrounding how we render modules and duplicate tags
                        $module_template = str_replace(array("{{" . $replace_tag . "}}", "{" . $replace_tag . "-replica}", "{{" . $replace_tag . "[duplicate=true]}}", "{{ " . trim($replace_tag) . "[duplicate=true] }}"), $new_html, $module_template);
                    }
                }
            }

            /* Render loops - standard */
            if($this->hasTabs())
            {
                $tabs_data_live = (isset($module_data['structure']['tab_settings'])) ? explode(",", $module_data['structure']['tab_settings']) : [];
                $tabs_order = array();
                $tabs_html = '';

                foreach($tabs_data_live as $item)
                {
                    if(trim($item) != '')
                    {
                        $tabs_order[] = trim($item);
                    }
                }

                $loops = array();

                /* get all types */
                preg_match_all($this->loop_loops_pattern, $module_template, $loops);

                $module_template = str_replace($loops[0][0], "{MODULEDATA}", $module_template);

                $loop_template = $loops[2][0];

                foreach($tabs_order as $i)
                {
                    $copied_template = $loop_template;
                    $copied_template = str_replace("[tab-number]", $i, $copied_template);

                    foreach($this->tabs_data['fields'] as $items)
                    {                        
                        foreach($items as $tag => $item)
                        {
                            $new_html = '';

                            if( isset($module_data['structure']['tab_' . $i . '_' . $item['settings']['name']]) && ( !isset($item['settings']['no_output']) || isset($item['settings']['no_output']) && $item['settings']['no_output'] === FALSE ) ) {
                                switch($tag)
                                {
                                    case 'image':
                                        if(isset($item['settings']['is_bg']) AND $item['settings']['is_bg'] == 'true')
                                        {
                                            $new_html = $module_data['structure']['tab_' . $i . '_' . $item['settings']['name']];
                                        }
                                        else
                                        {
                                            $new_html = '<img src="'.$module_data['structure']['tab_' . $i . '_' . $item['settings']['name']].'" loading="lazy"/>';
                                        }
                                        break;
                                    default:
                                        $new_html = $module_data['structure']['tab_' . $i . '_' . $item['settings']['name']];
                                        break;
                                }
                            }

                            //FIXME: Fix issues surrounding how we render modules and duplicate tags
                            $copied_template = str_replace(
                                array(
                                    "{".$item['settings']['original_name']."}", 
                                    "{" . $item['settings']['original_name'] . "-replica}", 
                                    "{" . $item['settings']['original_name'] . "[duplicate=true]}", 
                                    "{ " . trim($item['settings']['original_name']) . "[duplicate=true] }",
                                    "{" . $item['settings']['name'] . "[duplicate=true]}", 
                                    "{ " . trim($item['settings']['name']) . "[duplicate=true] }"
                                ), 
                            $new_html, $copied_template);
                        }
                    }

                    $tabs_html .= $copied_template;
                }

                $module_template = str_replace("{MODULEDATA}", $tabs_html, $module_template);
            }

            try {
                if( !empty($this->module_template->type) ) {
                    $ModuleFunctionalType = str_replace(array('.blade', '.php'), '', $this->module_template->type);

                    if( View::exists("theme::modules.{$ModuleFunctionalType}") ) {
                        $module_template = View::make('theme::modules.' . $ModuleFunctionalType)->with([
                            'module' => $this->module, 
                            'module_data' => $module_data,
                            'module_template' => $module_template,
                            'page' => $this->getPage(),
                            'attributes' => $this->attributes
                        ])->render();

                        if( empty($module_template) ) {
                            return '';
                        }
                    }
                    
                    if( !View::exists("theme::modules.{$ModuleFunctionalType}") && File::exists($this->template_dir . $ModuleFunctionalType . '.php') ) {
                        // this module template has a custom handler
                        // the custom handler exists, use it to prepare $html
                        include($this->template_dir . $ModuleFunctionalType . '.php');                    
                    }
                }
            }
            catch(\Exception $e) {
                // We're now going to reset the module template, if there are any issues above
                $module_template = '';

                if( Config::get('app.debug') ) {
                    $module_template = "<pre>There was a problem with this module. The error thrown is: {$e->getMessage()}<br />On this file: {$e->getFile()} <br />On this line: {$e->getLine()}</pre>";
                }

                Log::error("Error loading module (" . $this->module->id . "):" . $e->getMessage());
            }

            return trim($module_template) . $this->parseCSS() . $this->parseJS();
        }

        return;
    }

    public function parseCSS()
    {
        if($this->module_template && !empty($this->module_template->css)) {
            return '<style type="text/css">' . $this->module_template->css . '</style>';   
        }

        if(!empty($this->module->module_data['css'])) {
            return '<style type="text/css">' . $this->module->module_data['css'] . '</style>';
        }
    }

    public function parseJS()
    {
        if($this->module_template && !empty($this->module_template->js)) {
            return '<script>' . $this->module_template->js . '</script>';
        }

        if(!empty($this->module->module_data['js'])) {
            return '<script>' . $this->module->module_data['js'] . '</script>';
        }
    }


    public function getPage()
    {
        return (!empty($this->page)) ? $this->page:NULL;
    }
}
