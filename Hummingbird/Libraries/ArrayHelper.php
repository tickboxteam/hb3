<?php namespace Hummingbird\Libraries;

/**
 * Array Helper class
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class ArrayHelper
{
    /**
     * Cleans an recently exploded array. Removing empty, or custom items
     *
     * @param Array $exploded_items
     * @param String $remove_items
     * @return Array $new_data
     *
     */ 
    public function cleanExplodedArray(Array $exploded_items, $remove_items = false)
    {
    	$new_data = array();

    	if(count($exploded_items) > 0)
    	{
    		foreach($exploded_items as $item)
    		{
                $item = trim($item); //remove whitespace from array item
                
    			if($remove_items !== false)
    			{
    				$item = str_replace($remove_items, "", $item);
    			}

    			if($item != '') $new_data[] = trim($item);
    		}
    	}

    	return $new_data;
    }

    /**
     * Helper Class: Turn an object into an array by casting it
     *
     * @param  Object $object
     * @return  Array $object
     *
     */ 
    public function object_to_array($object) 
    {
        return (array) $object;
    }

    /**
     * Helper Class: Turn an array into an object by casting it
     *
     * @param  Array $array
     * @return  Object $array
     *
     */ 
    public function array_to_object($array)
    {
        return (object) $array;
    }

    /**
     * Sort function based on the argument input
     *
     * @param String $sort_func
     * @param Array $array (passed by reference)
     * @return Array $new_array
     *
     */ 
    public function sort($sort_func, &$array)
    {
        $new_array = $array;
        $sort_func($new_array);

        return $new_array;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function csv_to_array($filename = '', $delimiter = ',')
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;
     
        $header = NULL;
        $data = array();
        
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function get_array_key_by_value($items, $value, $field = '')
    {
        if(!is_array($items)) return false;

        foreach($items as $key => $item)
        {
            if($field == '')
            {
                if(in_array($value, $items[$key]))
                {
                    return $key;
                }
            }
            else if ( isset($item[$field]) AND $item[$field] === $value )
            {
                return $key;
            }
        }

        return false;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function extract($fields = [], $data = [])
    {
        if(empty($fields)) return $data;

        $extracted_data = [];
        foreach($fields as $field)
        {
            $extracted_data[$field] = (array_key_exists($field, $data)) ? $data[$field]:'';
        }

        return $extracted_data;
    }
}