<?php namespace Hummingbird\Libraries;

use App;
use Artisan;
use Exception;
use Event;
use File;
use Log;
use Hummingbird\Models\Plugin;
use Hummingbird\Exceptions\Plugins\PluginAlreadyInstalledException;
use Illuminate\Console\Command;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Filesystem\Filesystem;

/**
 * Plugin manager
 *
 * @package Hummingbird\Libraries
 * @author Tickbox Marketing: Daryl Phillips
 *
 */
class PluginManager
{
    protected $app;

    /**
     * @var String Plugin directory to workbench
     */
    protected $path = '/plugins';

    /**
     * @var String Plugin info file
     */
    protected $plugin_info = 'plugin.json';

    /**
     * @var String Plugin updates info
     */
    protected $updates_info = 'updates.json';

    /**
     * @var bool Check if all plugins have had the register() method called.
     */
    protected $registered = false;

    /**
     * @var array Disabled plugins that shouldn't be activated
     */
    protected $disabled = [];

    /**
     * @var array Array of currently active plugins - from file
     */
    protected $active_plugins = [];

    /**
     * @var Object package we are currently looking at
     */
    protected $package = NULL;


    /**
     * Initializes the plugin manager
     */
    public function __construct() 
    {
        $this->app = App::make('app');

        $this->plugin_path();
        $this->metaFile = $this->path . '/plugin.lock';

        $this->load_active_plugins();
        $this->load_vendors_plugins();
    }

    /**
     * Validates the plugin directory and creates one if necessary
     */
    public function plugin_path()
    {
        $path = base_path() . $this->path;

        //check that the workbench directory for plugins is available
        if (!File::isDirectory($path))
        {
            $result = File::makeDirectory($path, 0775);
        }
        else
        {
            if (!is_writable($path))
            {
                // Everything for owner, read and execute for others
                chmod($path, 0775);
            }
        }
    }

    /**
     * Loads all of the active files from a file.
     */
    public function load_active_plugins()
    {
        $plugins = Plugin::all();

        foreach($plugins as $plugin)
        {
            // active
            if($plugin->active)
                $this->active_plugins[$plugin->namespace] = $plugin;

            // not active
            if(!$plugin->active)
                $this->inactive[$plugin->namespace] = $plugin;

            // is disabled (no updates, install or registration facility)
            if($plugin->is_disabled)
                $this->disabled[] = $plugin->namespace;
        }
    }

    /**
     * Load all vendors and packages associated to those vendors
     */
    public function load_vendors_plugins()
    {
        /* already run - ignore request */
        if ($this->registered) {
            return;
        }

        $this->plugins = []; //reset plugins store

        /**
         * Locate all vendors/plugins
         */
        $providers = File::directories(base_path() . $this->path);

        /* iterate over locations */
        if(count($providers) > 0)
        {
            foreach($providers as $provider)
            {
                /* Clean and get provider name */
                $Vendor_Name = $this->cleanPackageNamespace($provider);

                /* Now get packages inside */
                $packages = File::directories($provider);

                if(count($packages) > 0)
                {
                    foreach($packages as $package)
                    {   
                        $Package_Name = $this->cleanPackageNamespace($package);
                        $Package_Namespace = $Vendor_Name . "\\" . $Package_Name;

                        $details            = array(
                            'vendor'        => $Vendor_Name, 
                            'path'          => $package,
                            'safe_path'     => substr(str_replace(base_path(), "", $package), 1),
                            'namespace'     => $Package_Namespace, 
                            'package_name'  => $Package_Name,
                            'vendor_autoloaded' => $this->vendor_autoloaded($package),
                            'ServiceProvider'   => $Package_Namespace .'\\' . $Package_Name . 'ServiceProvider',
                            'current_version' => (isset($this->active_plugins[$Package_Namespace])) ? $this->active_plugins[$Package_Namespace]->version : NULL
                        );
                        
                        $details            = array_merge($details, $this->read_plugin_info($package));
                        $this->plugins[$Vendor_Name][$Package_Name] = $details;
                    }
                }
            }
        }
    }


    /**
     * Get the Service Provider for booting inside the PluginManagerServiceProvider
     *
     * @param String $namespace
     * @return String | NULL
     */
    public function getServiceProvider($namespace)
    {
        $Vendor_Name = explode("\\", $namespace);

        $this->get_plugin($Vendor_Name[0], $Vendor_Name[1]);

        if(null !== $this->package)
        {
            if( File::exists( $this->package['path'] . '/vendor/autoload.php' ) ):
                // Artisan::call('hb3:dumpautoload', ['package' => $this->package['safe_path']]);
            endif;

            require $this->package['path'] . '/vendor/autoload.php';

            if(class_exists($this->package['ServiceProvider'])) {
                return $this->package['ServiceProvider'];
            }
        }

        return NULL;
    }


    /**
     * If plugin is not disabled, register the plugin via App::register()
     */
    public function register() {
        if( !$this->is_disabled() ) {
            require $this->package['path'] . '/vendor/autoload.php';

            if(class_exists($this->package['ServiceProvider'])) {

                $this->app->register($this->package['ServiceProvider']);
                return true;
            }
        }

        return false;
    }

    /**
     * Return all plugins
     */
    public function get_plugins()
    {
        return $this->plugins;
    }

    /**
     * Return all active plugins
     */
    public function get_active_plugins()
    {
        return $this->active_plugins;
    }


    /**
     * Set the plugin we are looking at
     *
     * @param String $Vendor_Name
     * @param String $Package_Name
     */
    public function get_plugin($Vendor_Name, $Package_Name)
    {
        if(isset($this->plugins[$Vendor_Name][$Package_Name])) $this->package = $this->plugins[$Vendor_Name][$Package_Name];

        $this->is_installed(); // is this plugin active/installed?
    }

    /**
     * Check whether the plugin is active
     *
     */
    public function is_active()
    {
        $this->package['installed_version'] = (isset($this->active_plugins[$this->package['namespace']])) ? $this->active_plugins[$this->package['namespace']]['version']:NULL;
    }


    /**
     * Check whether the plugin is active
     *
     */
    public function is_installed()
    {
        // Not installed or missing - don't do anything
        if( empty($this->package) ):
            return;
        endif;

        // Default is null
        $this->package['installed_version'] = NULL;

        // is active
        if(isset($this->active_plugins[$this->package['namespace']]))
            $this->package['installed_version'] = $this->active_plugins[$this->package['namespace']]->version;

        // not active - but has been installed before
        if(isset($this->inactive[$this->package['namespace']]))
            $this->package['installed_version'] = $this->inactive[$this->package['namespace']]->version;
    }

    /**
     * Check if this plugin has been disabled
     *
     * @param string Plugin
     * @return bool     if in disabled array
     */
    public function is_disabled()
    {
        if(empty($this->disabled)) return false;

        return in_array($this->package['namespace'], $this->disabled);
    }


    /**
     * From the package path return the package name
     *
     * @param string Package_Name   - the current path
     * @return string PackageName
     */
    public function cleanPackageNamespace($Package_Name)
    {
        $Package_Name = str_replace('\\', '/', $Package_Name);
        $Package_Name = explode("/", $Package_Name);
        $Package_Name_arr = explode("-", end($Package_Name));
        $Package_Name = '';

        foreach($Package_Name_arr as $Package)
        {
            $Package_Name .= ucfirst(trim($Package));
        }

        return $Package_Name;
    }


    /**
     * Install the designated package - this can already be installed
     *
     * @param string Vendor
     * @param string Package_Name
     * @throws Exception
     */
    public function install($Vendor, $Package_Name)
    {
        $this->get_plugin($Vendor, $Package_Name);

        if( null !== $this->package ) {
            try {
                Artisan::call('hb3:dumpautoload', ['package' => $this->package['safe_path']]);

                if( $this->register() ) {
                    Event::dispatch('hummingbird.plugin.Install.Before', ['package' => $this->package]);

                    $PluginFile = $this->package['namespace'].'\\Plugin';
                    $this->checkInstallFile($PluginFile);
                    $Plugin = new $PluginFile($this->package);
                    
                    if(!$Plugin->updater())
                        throw new \Exception($this->package['namespace'] . " could not be updated.");

                    $this->active_plugins[$this->package['namespace']] = $this->package;
                    $this->create_plugin_reference();

                    Event::dispatch('hummingbird.plugin.Install.After', ['package' => $this->package]);

                    return;
                }

                throw new \Exception($this->package['namespace'] . " could not be registered.");
            } catch(\Exception $e) {
                Log::error( "There was a problem registering this plugin. See error." );
                Log::info( $e->getMessage() );
            }
        }
    }


    /**
     * This will pull the designated package down and rebuild it
     *
     * @param string Vendor
     * @param string Package_Name
     * @throws Exception
     */
    public function refresh($Vendor, $Package_Name)
    {
        $this->get_plugin($Vendor, $Package_Name);

        if(null !== $this->package)
        {
            $PluginFile = $this->package['namespace'].'\\Plugin';
            $this->checkInstallFile($PluginFile);
            $Plugin = new $PluginFile($this->package);

            Event::dispatch('hummingbird.plugin.Uninstall.Before', ['package' => $this->package]);

            $Plugin->reset(true);
            
            if(!$Plugin->updater())
                throw new \Exception($this->package['namespace'] . " could not be updated.");

            Event::dispatch('hummingbird.plugin.Uninstall.After', ['package' => $this->package]);

            $this->create_plugin_reference();

            Event::dispatch('hummingbird.plugin.Install.After', ['package' => $this->package]);
        }
    }

    
    /**
     * This will just deactivate the current plugin
     *
     * @param string Vendor
     * @param string Package_Name
     * @throws Exception
     */
    public function deactivate_plugin($Vendor, $Package_Name)
    {
        $this->get_plugin($Vendor, $Package_Name);

        if(null !== $this->package) {
            if(isset($this->active_plugins[$this->package['namespace']])) {
                unset($this->active_plugins[$this->package['namespace']]);
            }

            try {
                $Plugin = Plugin::where('namespace', $this->package['namespace'])->firstOrFail();
                $Plugin->active = NULL;
                $Plugin->save();

                Event::dispatch('hummingbird.plugin.Uninstall.After', ['package' => $this->package]);
            }
            catch(\Exception $e) {
                // Package not found/installed, nothing to do
            }
        }
    }


    /**
     * This will uninstall the migrations and seedings
     *
     * @param string Vendor
     * @param string Package_Name
     * @throws Exception
     */
    public function uninstall($Vendor, $Package_Name)
    {
        $this->get_plugin($Vendor, $Package_Name);

        if(null !== $this->package)
        {
            $PluginFile = $this->package['namespace'].'\\Plugin';
            $this->checkInstallFile($PluginFile);
            $Plugin = new $PluginFile($this->package);

            Event::dispatch('hummingbird.plugin.Uninstall.Before', ['package' => $this->package]);

            $Plugin->reset(true);

            Event::dispatch('hummingbird.plugin.Uninstall.After', ['package' => $this->package]);

            unset($this->db_installed[$this->package['namespace']]);
            $this->destroy_plugin_reference(true);
        }
    }


    /**
     * Return whether we have plugins
     *
     * @return Boolean
     */
    public function hasPlugins()
    {
        return (count($this->plugins) > 0);
    }


    /**
     * Read the plugin.json file for the current package
     *
     * @param String $package_path
     * @return Array
     */
    public function read_plugin_info($package_path)
    {
        if(File::exists($package_path . '/' . $this->plugin_info))
        {
            $plugin_file = json_decode(File::get($package_path . '/' . $this->plugin_info));

            return (array) $plugin_file->plugin;
        }

        return [];
    }


    /**
     * Get the current version of the package
     *
     * @param String $plugin_details
     * @return String
     */
    public function getPluginVersion($plugin_details)
    {
        return (isset($plugin_details['stable_version']) AND $plugin_details['stable_version'] != '') ? $plugin_details['stable_version']:'Unknown';
    }

    
    /**
     * Checks if the package is a dependancy - not dependant
     *
     * @throws Exception
     */
    public function is_dependancy()
    {
        if(null !== $this->package)
        {
            foreach($this->active_plugins as $Package_Namespace => $Package_Details)
            {
                if($this->package['namespace'] !== $Package_Namespace)
                {
                    if(isset($Package_Details['dependancies']) AND 
                        count($Package_Details['dependancies']) > 0 AND 
                        in_array($this->package['namespace'], $Package_Details['dependancies']))
                    {
                        throw new \Exception($this->package['name'] . ' has dependancies and can not be deactivated.');
                    }
                }
            }
        }
    }


    /**
     * Checking the dependancies will only install the plugin if - 
     * and only if - its dependancies have been installed
     *
     */
    public function has_dependancies()
    {
        // General::pp("STARTING");
        
        if(null !== $this->package)
        {
            // General::pp($this->package);
            
            if(isset($this->package['dependancies']) AND count($this->package['dependancies']) > 0)
            {
                // General::pp(__LINE__);

                foreach($this->package['dependancies'] as $Package_dependancy)
                {
                    // General::pp($Package_dependancy);
                    if(!$this->plugin_is_active($Package_dependancy))
                    {
                        throw new \Exception($this->package['name'] . ' has dependancies and can not be installed.');
                    }
                }
            }
        }

        // General::pp("ENDED");
    }



    /**
     * Check whether the plugin is active
     *
     * @param String $Package_Namespace
     * @return Boolean
     */
    public function plugin_is_active($Package_Namespace)
    {
        return (array_key_exists($Package_Namespace, $this->active_plugins));
    }

    
    /**
     * Checks if the plugin has had its classmap autoloaded
     *
     * @param String $path
     * @return Boolean
     */
    public function vendor_autoloaded($path)
    {
        return (File::exists($path . '/vendor'));
    }

    
    /**
     * Checks if the package has an install file and loads it in
     *
     * @param String $PluginFile
     * @throws Exception
     */
    public function checkInstallFile($PluginFile)
    {
        if(!class_exists($PluginFile))
        {
            $PluginSource = $this->package['path'] . '/src/Plugin.php';
            if(!File::exists($PluginSource))
                throw new \Exception("Installation file for " . $this->package['namespace'] . " is missing");
            
            include_once $PluginSource;
        }

        if(!class_exists($PluginFile))
            throw new \Exception("Installation file for " , $this->package['namespace'] . " is missing");
    }

    
    /**
     * Checks if the package has an install file and loads it in
     *
     */
    public function create_plugin_reference()
    {
        $Plugin = Plugin::where('namespace', $this->package['namespace'])->first();

        if(null === $Plugin)
        {
            $Plugin = new Plugin();
            $Plugin->fill($this->package);
        }

        $Plugin->version = $this->package['stable_version'];
        $Plugin->active = (isset($this->active_plugins[$this->package['namespace']])) ? 1:0;
        $Plugin->is_disabled = $this->is_disabled();
        $Plugin->save();
    }

    /**
     * Removes package from database
     *
     */
    public function destroy_plugin_reference()
    {
        $Plugin = Plugin::where('namespace', $this->package['namespace'])->delete();
    }

    public function getPackageInformation($Vendor, $Package) {
        return (!empty($this->plugins[$Vendor][$Package])) ? $this->plugins[$Vendor][$Package] : [];
    }
}
