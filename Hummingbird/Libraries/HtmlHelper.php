<?php namespace Hummingbird\Libraries;

/**
 * HTML Helper functions for Hummingbird3
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */

class HtmlHelper
{
    /**
     *
     * 
     *
     *
     */  
    public static function message( $type = NULL , $data = array(), $html = '')
    {
    	if( null !== $type )
    	{
    		ob_start();

    		switch($type)
    		{
    			case 'temp-redirect':?>

		            <h1>This page has been moved, you will be re-directed in a moment</h1>
		            <p>If you are not re-directed please <a href="[url]" target="_blank">click here</a>.</p>

    				<?php break;
    			case '404':?>

    				<h1>404: Page not found</h1>';
    				<p>Sorry, but the page you are looking for has not been found.</p>
       				<p>Try checking the URL for errors, then hit the refresh button on your browser.</p>

    				<?php break;
    		}

    		$html = ob_get_clean();
    	}

    	if(empty($data)) return $html;

    	return HtmlHelper::field_merge($html, $data);
    }

    /**
     *
     * 
     *
     *
     */  
    public static function field_merge( $string, $data )
    {
    	foreach($data as $key => $value)
    	{
    		if(is_array($value)) $string = $this->field_merge($string, $value);

    		$string = str_replace("[$key]", "$value", $string);
    	}

    	return $string;
    }


    public static function maintenanceMode()
    {
        ob_start();?>

        <h3>Website under maintenance</h3>
        <p>I'm sorry, but the website is temporairly down. Please try again soon.</p>

        <?php return $content = ob_get_clean();
    }
}
?>