<?php namespace Hummingbird\Libraries;

/**
 * Navigation builder and manager
 *
 * @package hummingbird
 * @author Daryl Phillips
 */
class BackendMenu {
    /**
     * Admin user
     * @var Hummingbird\Models\User
     */
    protected $user;

    /**
     * Group defaults
     * @var Array
     */
    protected static $mainGroupDefaults = [
        'label'         => null,
        'icon'          => null,
        'url'           => null,
        'permissions'   => [],
        'order'         => 100,
        'children'      => [],
        'parent'        => NULL,
    ];

    /**
     * Item (in group) defaults
     * @var Array
     */
    protected static $mainItemDefaults = [
        'label'         => null,
        'icon'          => null,
        'url'           => null,
        'permissions'   => [],
        'children'      => [],
        'parent'        => NULL,
        'active'        => false
    ];

    /**
     * Top Level navigation items, without a group
     * @var Array
     */
    protected $topLevelItems = [];

    /**
     * Navigation items
     * @var Array
     */
    protected $items = [];

    /**
     * Registering backend groups.
     * 
     * @param  array  $groups
     */
    public function registerBackendGroups($groups = []) {
        if(count($groups) > 0) {
            foreach($groups as $group_index_name => $group_settings) {
                if(!isset($group_settings['parent']) || null === $group_settings['parent']) {
                    $itemIndexKey = $group_index_name;

                    if (isset($this->items[$itemIndexKey])) {
                        $group_settings = array_merge((array) $this->items[$itemIndexKey], $group_settings);
                    }

                    $item = array_merge(self::$mainGroupDefaults, $group_settings);

                    $this->items[$itemIndexKey] = $item;
                }
            }

            foreach($groups as $group_index_name => $group_settings) {

                if(isset($group_settings['parent']) AND null !== $group_settings['parent']) {
                    $parentItemIndexKey = $group_settings['parent'];
                    $itemIndexKey       = $group_index_name;
                    $BackendItem        = array_get($this->items, $parentItemIndexKey .'.children.'.$itemIndexKey);

                    if( $BackendItem ) {
                        $group_settings = array_merge((array) $BackendItem, $group_settings);
                    }
                
                    $item = array_merge(self::$mainItemDefaults, $group_settings);

                    array_set($this->items, $parentItemIndexKey .'.children.'.$itemIndexKey, $item);
                }
            }
        }
    }

    /**
     * Register items into the CMS navigation
     * 
     * @param  array  $items
     */
    public function registerBackendNavigationItems($items = []) {
        foreach ($items as $item_key => $item_detail) {
            $ItemsKey = 'items';

            if( !isset($item_detail['parent']) || null === $item_detail['parent']) {
                $item_detail['parent'] = 'topLevel';
                $ItemsKey = 'topLevelItems';
            }

            $parentItemIndexKey = $item_detail['parent'];
            $itemIndexKey       = $item_key;
            $BackendItem        = array_get($this->$ItemsKey, $parentItemIndexKey);
            $BackendItemIndex   = $parentItemIndexKey . '.children.' .$itemIndexKey;
            $item_detail['url'] = ( strpos($item_detail['url'], ".") !== FALSE ) ? route( $item_detail['url'] ) : url( $item_detail['url'] );
            $item_detail['active'] = $this->isLinkActive( $item_detail['url'] );

            if (isset($this->$ItemsKey[$parentItemIndexKey][$itemIndexKey])) {
                $item_detail = array_merge((array) $BackendItem, $item_detail);
            }
        
            $item = array_merge(self::$mainItemDefaults, $item_detail);

            array_set($this->$ItemsKey, $BackendItemIndex, $item);
        }
    }

    
    /**
     * Generate the CMS navigation based on elements installed
     *  
     * @return Array $items Containing results for navigation in groups
     */
    public function getBackendNavigationItems()
    {
        if( !$this->user ) {
            $this->setUser();
        }

        $this->items = $this->sort( $this->checkItemPermissions($this->items) );

        return $this->items;
    }

    /**
     * Generate the CMS navigation based on elements installed
     *  
     * @return Array $items Containing results for navigation in groups
     */
    public function getBackendNavigationTopLevelItems()
    {
        if( !$this->user ) {
            $this->setUser();
        }

        $this->topLevelItems = $this->sort( $this->checkItemPermissions($this->topLevelItems) );

        return $this->topLevelItems;
    }

    public function sort($items) {
        $items = $this->sortItemsByPosition( $this->sortItems($items) );

        foreach($items as $item_key => $item) {
            if( !empty($item['children']) ) {
                $items[$item_key]['children'] = $this->sort($item['children']);
            }
        }

        return $items;
    }

    /**
     * Sort by the order
     * @param  Array $items
     * @return Array       
     */
    public function sortItems($items) {
        usort($items, function($a, $b) {
            return strcmp($a['label'], $b['label']);
        });
    
        return $items;
    }

    public function sortItemsByPosition( $items )
    {
        $new_items = [];
        $nextIndexKey = 0;

        foreach( $items as $index_key => $item )
        {
            $nextIndexKey = ( isset($item['order']) && is_numeric($item['order']) ) ? (int) $item['order'] : $nextIndexKey + 10;

            while( isset($new_items[ $nextIndexKey ]) )
            {
                $nextIndexKey += 10;
                $HasKeyAlready = ( isset($new_items[ $nextIndexKey ]) );
            }

            $new_items[$nextIndexKey] = $item;
        }

        ksort($new_items);

        return $new_items;
    }

    /**
     * Removes menu items from an array if the supplied user lacks permission.
     * @param User $user A user object
     * @param Array $items A collection of menu items
     * 
     * @return Array The filtered menu items
     */
    protected function checkItemPermissions(array $items) {
        $items = $this->filterItemPermissions($items);

        foreach($items as $item_key => $item) {
            if( !empty($item['children']) ) {
                $items[$item_key]['children'] = $this->checkItemPermissions($items[$item_key]['children']);
                $items[$item_key]['children'] = $this->filterItemPermissions($items[$item_key]['children']);

                if( !count($items[$item_key]['children']) )
                    unset($items[$item_key]);
            }
        }

        return $items;
    }

    /**
     * Filter and remove those we do not have access too
     * @param  Array  $items
     * @return Array
     */
    protected function filterItemPermissions(array $items) {
        if (null === $this->user) {
            return $items;
        }

        $items = array_filter($items, function ($item) {
            // Role check first - priority to hide certain items
            if( !empty($item['roles']) ) {
                return in_array($this->user->role->name, $item['roles']);
            }

            // No permissions - global access
            if (!isset($item['permissions']) || !count($item['permissions']) ) {
                return true;
            }

            // Lastly check the user abilities if we have permissions
            return $this->user->userAbility($item['permissions'][0]);
        });

        return $items;
    }

    /**
     * Do we have any navigation items
     * @return boolean
     */
    public function hasBackendNavigationItems() {
        return count($this->items);
    }

    /**
     * Do we have any navigation items
     * @return boolean
     */
    public function hasBackendNavigationTopLevelItems() {
        return count($this->topLevelItems);
    }

    /**
     * Set the user currently logged in
     */
    public function setUser() {
        $this->user = auth()->user();
    }

    /**
     * A check to see if the link, we're displaying is active. 
     * We check against the dashboard so that the dashboard item is not active all the time
     * 
     * @param  String  $link The link we're checking
     * @return boolean Active link
     */
    public function isLinkActive($link) {
        if( request()->url() == url($link) && url($link) == route('hummingbird.dashboard') ) {
            return true;
        }

        return request()->url() == url($link);
    }
}