<?php namespace Hummingbird\Libraries;

use Log;
use Hummingbird\Libraries\PermissionHandler;

/**
 * Permission builder
 *
 * @package hummingbird
 * @author Daryl Phillips, Tickbox Marketing 2014-2015
 *
 */
class PermissionManager
{
    protected $permission;

    protected $perm_actions;

    /**
     * Standard permissions for CMS include CRUD - WITHOUT READ NO PERMISSION IS AVAILABLE
     * C = CREATE
     * R = READ
     * U = UPDATE
     * D = DELETE
     */
    protected $standard_actions = array('CREATE', 'READ', 'UPDATE', 'DELETE');

    /**
     * Build the handler and retrieve currently stored permission actions
     */
    public function __construct()
    {
        Log::warning("Class Hummingbird\Libraries\PermissionManager is deprecated. Use Hummingbird\Libraries\PermissionHandler");

        $this->permission   = new \Permission();
        $this->perm_actions = $this->permission->getActions();
    }

    /**
     * Check if our current permission action exists already
     * 
     * @param String Action
     * @return boolean 
     */
    public function checkPermissionExists($action)
    {
        Log::warning("Method Hummingbird\Libraries\PermissionManager::checkPermissionExists() is deprecated. Use Hummingbird\Libraries\PermissionHandler::permissionExists()");

        return PermissionHandler::permissionExists($action);
    }

    /**
     * Get the standard actions list
     * @return Array $standard_actions
     */
    public function get_standard_actions()
    {
        return $this->standard_actions;
    }

    /**
     * Standard permission actions, CRUD, are installed
     * @param 
     */
    public function install()
    {
        Log::error("Method Hummingbird\Libraries\PermissionManager::install() is deprecated. Use Hummingbird\Libraries\PermissionHandler to register new permissions.");
    }

    /**
     * Bespoke permissions required for CMS
     * @param 
     */
    public function install_actions($actions)
    {
        Log::error("Method Hummingbird\Libraries\PermissionManager::install_actions() is deprecated. Use Hummingbird\Libraries\PermissionHandler to register new permissions.");
    }

    /**
     * 
     * @param 
     * @return 
     */
    public function install_permissions(Array $data)
    {
        Log::warning("Method Hummingbird\Libraries\PermissionManager::install_permissions() is deprecated. Use Hummingbird\Libraries\PermissionHandler::registerPermissionGroupings()");

        $perms = $data['perms'];

        PermissionHandler::registerPermissionGroupings([
            'key_name'      => $data['type'],
            'group_name'    => $data['label'],
            'group_desc'    => $data['description'],
            'permissions'   => $data['perms']
        ]);
    }

    /**
     * 
     * @param 
     * @return 
     */
    public function removePermissions($type = null)
    {
        Log::warning("Method Hummingbird\Libraries\PermissionManager::removePermissions() is deprecated. Use Hummingbird\Libraries\PermissionHandler::removePermissions()");

        if(!is_null($type)) {
            PermissionHandler::removePermissionsByGroup($type);
        }
    }
}
