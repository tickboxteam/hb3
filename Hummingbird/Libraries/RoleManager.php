<?php namespace Hummingbird\Libraries;

use Auth;
use Hummingbird\Models\Role;

/**
 * Role manager class to help manage all CMS administrator roles
 *
 * @package hummingbird
 * @version 1.0.1
 * @date 16 Jan 2017, 16:32 GMT
 * @author Daryl Phillips
 *
 */
class RoleManager {
    private     $user;
    protected   $roles = [];
    protected   $new_roles = [];
    protected   $num_roles = 0;

    public function init() {
        /* Get all the roles - ordered by parentrole_id (Null first, n+1 last) */
        $this->all_roles = Role::orderby('parentrole_id', 'ASC')->orderby('name', 'ASC')->get();

        /* Store roles in that we need */
        $this->buildTree($this->all_roles);

        
    }


    /**
     * Initialise and return all roles
     *
     * @return Array
     */
    public function run()
    {
        if(isset($this->roles) AND count($this->roles) > 0) return array_flatten($this->roles);

        return [];
    }


    /**
     * Loop over all roles and attach them based on inheritance: Super > Admin > Editor > Viewer
     *
     */
    public function buildTree($elements, $parentId = 1, $level = 0, $stop_at_parent = false) 
    {
        foreach ($elements as $key => $element) {
            /* Top level parent */
            if ($element->parentrole_id !== $parentId OR $element->parentrole_id === NULL) continue;

            /* If hidden don't show */
            // if(!$element->hidden)
            // {
                /* Do not show in edit mode - stops moving child into child */
                if($element->id === $stop_at_parent OR $element->parentrole_id === $stop_at_parent) $this->data['role_parents'][] = $element->id;

                $this->roles[$element->id] = $element;
                $this->roles[$element->id]['level'] = $level;
                $this->buildTree($elements, $element->id, $level + 1, $stop_at_parent);
            // }
        }
    }



    public function setRoles() {
        $level = ($this->user->role->hidden) ? 0 : 1;
        $this->new_roles[] = $this->user->role;

        foreach( $this->user->role->children()->orderBy('name')->get() as $ChildRole ) {
            $Spacer = ($this->user->role->hidden) ? "" : str_repeat('&nbsp;&nbsp;', $level) .str_repeat('-', $level);
            $ChildRole->listing_name = "{$Spacer} {$ChildRole->name}";

            $this->new_roles[] = $ChildRole;

            if( !$ChildRole->hidden ):
                $this->num_roles++;
            endif;

            if( $ChildRole->children->count() > 0 ) {
                $this->setChildRoles($ChildRole, $level+1);
            }
        }
    }

    public function setChildRoles($Role, $level) {
        foreach( $Role->children()->orderBy('name')->get() as $ChildRole ) {
            $ChildRole->listing_name = str_repeat('&nbsp;&nbsp;', $level) .str_repeat('-', $level) . " {$ChildRole->name}";
            $this->new_roles[] = $ChildRole;

            if( !$ChildRole->hidden ):
                $this->num_roles++;
            endif;

            if( $ChildRole->children->count() > 0 ) {
                $this->setChildRoles($ChildRole, $level+1);
            }
        }
    }

    public function getRoles() {
        if( empty($this->user) ):
            $this->register();
        endif;

        return $this->new_roles;
    }

    public function getTotalAvailbleRoles() {
        if( empty($this->user) ):
            $this->register();
        endif;
        
        return $this->num_roles;
    }


    public function register() {
        $this->user = Auth::user();
        $this->setRoles();
    }
}
