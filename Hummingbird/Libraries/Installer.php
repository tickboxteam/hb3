<?php namespace Hummingbird\Libraries;

use App;
use Artisan;
use Config;
use File;
use Session;
use Validator;
use Hummingbird\Libraries\FileHelper;
use Hummingbird\Libraries\Plugins;
use Hummingbird\Models\Role;
use Hummingbird\Models\User;

/*
 * Class dedicated to installing a new site
 * 
 */
class Installer
{
    private $db_host;
    private $db_name;
    private $db_user;
    private $db_password;
    private $db_prefix;
    
    public $errors = array();
    
    public function setupCredentials($data)
    {        
        $rules = array(
            'db_host' => 'required',
            'db_name' => 'required',
            'db_user' => 'required',
            'db_password' => 'required',
            'db_prefix' => 'alpha_dash'
        );
        
        if($this->valid($data, $rules)) {
            if($this->connectToDb()) {
                return $this->setupEnvironmentFile($data);
            }
        }
        
        return false;
    }
    
    public function has_errors()
    {
        return count($this->errors) > 0;
    }
    
    public static function isInstalled()
    {
        return config()->get('hb3-hummingbird.install');
    }
    
    public static function uri()
    {
        return config()->get('hb3-hummingbird.installURI');
    }
    
    private function valid($data, $rules, $messages = array())
    {        
        $validator = Validator::make($data, $rules, $messages);        
        $this->errors = $validator->messages();
        
        return $validator->passes();
    }
    
    private function connectToDb()
    {
        // todo: quick test to see if that works 
        // http://fideloper.com/laravel-multiple-database-connections
        // http://stackoverflow.com/questions/14524181/laravel-4-multiple-tenant-application-each-tenant-its-own-database-and-one-gl
        return true;
    }
    
    public static function get_types()
    {
        return array(
            'standard' => 'Standard (Install with commonly used features pre-configured)',
            'advanced' => 'Advanced (Select your plugins/modules)'
        );
    }
    
    private function setupEnvironmentFile($data) {
        $CoreEnvArr = [];

        if( file_exists( base_path() . '/.env' ) ):
            foreach(file( base_path() . '/.env' ) as $line) {
                $LineSplit = explode("=", $line);
                $LineSplit[0] = trim($LineSplit[0]);

                if( !empty($LineSplit[0]) ):
                    $Section = explode("_", $LineSplit[0]);
                    $SectionGroup = trim( $Section[0] );
                    unset($Section[0]);

                    $SectionName = implode("_", $Section);

                    if( !isset($CoreEnvArr[$SectionGroup]) ):
                        $CoreEnvArr[ $SectionGroup ] = [];
                    endif;

                    $CoreEnvArr[ $SectionGroup ][ trim($LineSplit[0]) ] = isset($LineSplit[1]) ? trim($LineSplit[1]) : '';
                endif;
            }
        endif;

        if( !isset($CoreEnvArr['APP']) ) {
            $CoreEnvArr['APP'] = [];
        }

        if( !isset($CoreEnvArr['DB']) ) {
            $CoreEnvArr['DB'] = [];
        }

        if( !isset($CoreEnvArr['CACHE']) ) {
            $CoreEnvArr['CACHE'] = [];
        }

        if( !isset($CoreEnvArr['SESSION']) ) {
            $CoreEnvArr['SESSION'] = [];
        }

        $CoreEnvArr['APP']['APP_ENV'] = "production";
        $CoreEnvArr['APP']['APP_DEBUG'] = "true";
        $CoreEnvArr['APP']['APP_READY'] = "false";
        $CoreEnvArr['APP']['APP_KEY'] = ( config('app.key') ) ? config('app.key') : 'base64:'.base64_encode(random_bytes( config('app.cipher') == 'AES-128-CBC' ? 16 : 32 ));
        $CoreEnvArr['DB']['DB_HOST'] = "{$data['db_host']}";
        $CoreEnvArr['DB']['DB_DATABASE'] = "{$data['db_name']}";
        $CoreEnvArr['DB']['DB_USERNAME'] = "{$data['db_user']}";
        $CoreEnvArr['DB']['DB_PASSWORD'] = "{$data['db_password']}";
        $CoreEnvArr['DB']['DB_PREFIX'] = "{$data['db_prefix']}";
        $CoreEnvArr['CACHE']['CACHE_DRIVER'] = "array";
        $CoreEnvArr['SESSION']['SESSION_DRIVER'] = "file";

        return $this->writeToEnv( $CoreEnvArr );
    }
    
    public function runMigrations() {
        try {
            // FIXME: No command exists, needs to be a custom one
            // https://stackoverflow.com/questions/37238547/run-composer-dump-autoload-from-controller-in-laravel-5/42555169#42555169
            // Artisan::call('dump-autoload');

            // create core cms tables
            Artisan::call('migrate', [
                '--force' => true,
                '--path'=> "/Hummingbird/database/migrations"
            ]);

            Artisan::call('db:seed', array('--class' => 'Hummingbird\Database\Seeds\DatabaseSeeder', '--force' => true));

            // App::make("UpdatesController")->versionsBehind(TRUE);

            // $this->data['settings'] = Setting::where('key', '=', 'system_update')->first();

            // /* Seed new data */
            // if(class_exists('SystemUpdateSeeder')) (new SystemUpdateSeeder('1.0.0', $this->data['settings']->value['latest_version'], $this->data['settings']->value['versions'], $this->data['settings']->value['behind']))->run();

            return true;
        } catch (Exception $e) {
            // rollback
            Artisan::call('migrate:reset', array('--force' => true));
        }
        
        return false;
    }
    
    public function runPluginMigrations($inputs)
    {
        try {
            
            Plugins::install($inputs['plugins']);
            
        } catch (Exception $e) {
            // rollback
            
            return false;
        }
        
        return true;
    }
    
    public function complete()
    {
        $CoreEnvArr = [];

        foreach(file( base_path() . '/.env' ) as $line) {
            $LineSplit = explode("=", $line);
            $LineSplit[0] = trim($LineSplit[0]);

            if( !empty($LineSplit[0]) ):
                $Section = explode("_", $LineSplit[0]);
                $SectionGroup = trim( $Section[0] );
                unset($Section[0]);

                $SectionName = implode("_", $Section);

                if( !isset($CoreEnvArr[$SectionGroup]) ):
                    $CoreEnvArr[ $SectionGroup ] = [];
                endif;

                $CoreEnvArr[ $SectionGroup ][ trim($LineSplit[0]) ] = isset($LineSplit[1]) ? trim($LineSplit[1]) : '';
            endif;
        }

        $CoreEnvArr['APP']['APP_READY'] = 'true';
        $this->writeToEnv( $CoreEnvArr );

        return true;
    }
    
    public function updateSiteSettings($data)
    {
        $rules = array(
            'site_name' => 'required',
            'site_url' => 'required|regex:/^[a-zA-Z0-9_\\-\\.]*$/',
            // 'site_key' => 'required',
            'site_password_strength' => 'required|integer',
            'site_installtype' => 'required',
            'site_timezone' => 'required'
        );
        
        if($this->valid($data, $rules)) {
            $old_timezone = date_default_timezone_get();

            $CoreEnvArr = [];

            foreach(file( base_path() . '/.env' ) as $line) {
                $LineSplit = explode("=", $line);
                $LineSplit[0] = trim($LineSplit[0]);

                if( !empty($LineSplit[0]) ):
                    $Section = explode("_", $LineSplit[0]);
                    $SectionGroup = trim( $Section[0] );
                    unset($Section[0]);

                    $SectionName = implode("_", $Section);

                    if( !isset($CoreEnvArr[$SectionGroup]) ):
                        $CoreEnvArr[ $SectionGroup ] = [];
                    endif;

                    $CoreEnvArr[ $SectionGroup ][ trim($LineSplit[0]) ] = isset($LineSplit[1]) ? trim($LineSplit[1]) : '';
                endif;
            }

            $app_config = app_path().'/config/app.php';
            $hummingbird_config = app_path().'/Hummingbird/config/hummingbird.php';

            if( !isset($CoreEnvArr['APP']) ) {
                $CoreEnvArr['APP'] = [];
            }

            $CoreEnvArr['APP']['APP_NAME']  = "'".$data['site_name']."'";
            $CoreEnvArr['APP']['APP_URL']   = $data['site_url'];
            $CoreEnvArr['APP']['APP_BACKEND_URI'] = 'hummingbird';
            // $CoreEnvArr['APP']['APP_KEY']   = $data['site_key'];
            $CoreEnvArr['APP']['APP_TIMEZONE'] = $data['site_timezone'];
            
            // the below is refusing to persist (it has worked in the past)
            Session::put('installtype', $data['site_installtype']);
            $this->writeToEnv( $CoreEnvArr );

            return true;
        }
        
        return false;
    }
    
    public function addAdministrator($data)
    {        
        $superadmin_role = Role::where('name', 'superadmin')->first();
        
        $user = (new User)->fill($data);
        $user->firstname = $data['firstname'];
        $user->surname = $data['surname'];
        $user->role_id = $superadmin_role->id;
        $user->is_admin = 1;
        $user->status = User::APPROVED;        
        $user->is_admin = TRUE;
        $user->hidden = 1;
        
        if( !$user->save() ):
            $this->errors = $user->errors();
            return false;
        endif;
        
        return true;
    }
    
    public function checkFilePermissions()
    {
        // directories to check: storage and uploads
        $writable_directories = array(base_path().'/uploads', app_path().'/storage');
        
        foreach($writable_directories as $ind => $dir) {
            if (FileHelper::is_really_writable($dir)) {
                unset($writable_directories[$ind]);
            }
        }
        
        if(!empty($writable_directories)) {
            $this->errors['directories'] = $writable_directories;
            dd($writable_directories);
            return false;
        }
        
        return true;
    }



    public function writeToEnv( $Contents, $path = NULL ) {
        $path = ( empty($path) ) ? base_path() . '/.env' : $path;

        $VariableContents = "";

        foreach($Contents as $Var_Group => $Var_Items ):
            foreach($Var_Items as $ItemName => $ItemValue ):
                $VariableContents .= "{$ItemName}={$ItemValue}" . PHP_EOL;
            endforeach;

            $VariableContents .= "" . PHP_EOL;
        endforeach;

        return File::put($path, $VariableContents);
    }
}