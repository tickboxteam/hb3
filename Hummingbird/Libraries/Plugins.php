<?php namespace Hummingbird\Libraries;

class Plugins
{    
    public static function install($plugins = false)
    {
        $plugin_obj = new Plugins;
        $plugin_obj->plugins = $plugins ?: self::all();
        
        $plugin_obj->migrate();          
        $plugin_obj->seed();
    }
    
    public static function uninstall($plugins = false)
    {
        $plugin_obj = new Plugins;
        $plugin_obj->plugins = $plugins ?: self::all();
        
        $plugin_obj->reset();
        $plugin_obj->unseed();
    }
    
    private function migrate()
    {        
        foreach($this->plugins as $plugin) {
            
            Artisan::call('migrate', [
                '--force' => true,
                '--bench'=>"tickbox/$plugin"
            ]);
        }
    }
    
    private function seed()
    {        
        foreach($this->plugins as $plugin) {
            
            $classname = ucfirst($plugin).'Seeder';
            
            $seederClass = new $classname;
            $seederClass->run();        
        }
    }
    
    private function unseed()
    {        
        foreach($this->plugins as $plugin) {
            
            $classname = ucfirst($plugin).'Seeder';
            
            $seederClass = new $classname;
            $seederClass->unseed();        
        }
    }
    
    // migrate:reset doesn't support --bench
    public function reset()
    {     
        $migrate_obj = new \Tickbox\Events\CreateEventsTable;
        $migrate_obj->down();

        // foreach($this->plugins as $plugin) {
            
        //     $migrations = FileHelper::filenames(base_path() . '/workbench/tickbox/' . $plugin . '/src/migrations');

        //     if(count($migrations) > 0)
        //     {
        //         foreach($migrations as $migration)
        //         {
        //             // $classname = ucfirst($plugin).'Seeder';
        //             $migrate_obj = new $migration;
        //             $migrate_obj->down();   
        //         }
        //     }
        // }

        // foreach($this->plugins as $plugin) {
        //     $classname = ucfirst($plugin);

        //     $migrate_obj = new Tickbox\$classname;
        //     $migrate_obj->down();
 
            // $migrations = FileHelper::filenames(base_path() . '/workbench/tickbox/' . $plugin);
                
            // // dd($migrations);

            // foreach($migrations as $migration) {


            //     #todo: find class name trhough reflection
                
            //     // $classname = ucfirst($plugin).'Seeder';
            //     $migrate_obj = new CreateEventsTable;
            //     $migrate_obj->down();
            // }
            
            // Artisan::call('migrate:reset', [
            //     '--bench'=>"tickbox/$plugin"
            // ]);
        // }       
    }
    
    public static function get_blocks_for_area($area)
    {
        $plugins = self::all();
        
        foreach($plugins as $plugin) {
            #Illuminate\Workbench\Package::all();
        }
    }
}

?>



<?php 

/**
 * Plugin Installer to handle the migrations and seeding
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class PluginInstaller
{
    /**
     * Construct the PluginInstaller Object
     *
     * @param Plugin $plugin
     */
    public function __construct(Plugin $plugin)
    {
        $this->plugin = $plugin;
    }
    
    /**
     * Install the plugin - migrate and then seed
     *
     */
    public function install () {
        Artisan::call('migrate', array('--bench' => $package, '--force' => true));

        if(class_exists($this->package['namespace'] . '\\DatabaseSeeder'))
        {
            $this->seed_plugin();
            $this->database_info->value = serialize(array($this->package['namespace'] => $this->package['stable_version']));
        }
                    
        // all the logic for looping through versions and performing
        // required tasks for each version, plus anything else install related
        // $this->plugin->migrate();
        $this->plugin->seed();
    }
    
    /**
     * Remove the plugin and plugin data
     *
     */
    public function uninstall()
    {
        $this->plugin->reset();
        $this->plugin->clean();
    }
}