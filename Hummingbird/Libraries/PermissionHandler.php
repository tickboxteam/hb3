<?php namespace Hummingbird\Libraries;

use Hummingbird\Models\Permission;
use Hummingbird\Models\PermissionGroup;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Permission Handler for all things permission related.
 *
 * @package hummingbird
 * @author Daryl Phillips, Tickbox Marketing 2014-2018
 *
 * $PermissionGroupInfo = [
 *     'key_name'      => 'key_name',
 *     'group_name'    => 'key_group',
 *     'group_desc'    => 'key_description',
 *     'permissions'   => [ array_of_permissions ]
 * ];
 */
class PermissionHandler {

    /**
     * Register the base groupings that control permissions for HB3.
     * 
     * @param  Array $PermissionGroup
     */
    public function registerPermissionGroupings($PermissionGroup) {
        if(!empty($PermissionGroup['key_name']) && !empty($PermissionGroup['permissions'])) {
            $Perm_Group = PermissionGroup::firstOrCreate([
                'name' => $PermissionGroup['group_name'],
                'description' => $PermissionGroup['group_desc']
            ]);

            if(count($PermissionGroup['permissions']) > 0) {
                $this->registerPermissions($PermissionGroup['key_name'], $PermissionGroup['permissions'], $Perm_Group);
            }
        }
    }

    /**
     * Register permissions for individual elements.
     * @param  String $KeyName    
     * @param  Array  $Permissions
     * @param  Mixed $group            
     */
    public function registerPermissions($KeyName, $Permissions = array(), $group = NULL) {
        foreach($Permissions as $Permission => $PermissionDetails) {
            if(!$this->permissionExists("{$KeyName}.{$Permission}")) {
                $this->registerPermission("{$KeyName}.{$Permission}", $PermissionDetails, $group);
            }
        }
    }

    /**
     * Register an individual permission
     * @param  String $KeyName          
     * @param  Array $PermissionDetails
     * @param  Mixed $group            
     */
    public function registerPermission($KeyName, $PermissionDetails, $group = NULL) {
        $Permission = Permission::updateOrCreate([
            'name'          => $KeyName,
        ],[
            'display_name'  => !empty($PermissionDetails['label']) ? $PermissionDetails['label'] : $KeyName,
            'description'   => !empty($PermissionDetails['description']) ? $PermissionDetails['description'] : NULL,
            'action'        => !empty($PermissionDetails['action']) ? $PermissionDetails['action'] : NULL,
            'group_id'      => ($group) ? $group->id : 0
        ]);
    }

    /**
     * Does this permission already exist.
     * @param  String $Permission
     * @return Boolean
     */
    public function permissionExists($Permission) {
        return (Permission::name($Permission)->get()->count() > 0);
    }

    /**
     * Remove permissions by their Group
     * 
     * @param  String $GroupName
     */
    public function removePermissionGroup($GroupName) {
        try {
            $PermissionGroup = PermissionGroup::where('name', $GroupName)->firstOrFail();
            $PermissionGroup->forceDelete();
        }
        catch(\Exception $e) {
            if( get_class($e) != ModelNotFoundException::class ) {
                Log::error("Unable to delete the group due to a problem. Please review deleting '{$GroupName}'.");
            }
        }
    }

    /**
     * Pass in an array of permissions and remove them
     * 
     * @param  array  $Permissions
     */
    public function removePermissions($Permissions = array()) {
        foreach($Permissions as $Permission) {
            $this->removePermission($Permission);
        }
    }

    /**
     * Remove a single permission from the system
     * @param  String $Permission
     */
    public function removePermission($Permission) {
        $Permission = Permission::name($Permission)->forceDelete();
    }
}
