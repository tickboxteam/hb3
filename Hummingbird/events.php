<?php

Event::listen('500', function() {
    return View::make('500', array());
});

// Updating the Role Event
Event::listen('RoleWasUpdated', 'Hummingbird\Classes\Permissions\PagePermissionsUpdate', 5);
Event::listen('RoleWasUpdated', 'Hummingbird\Classes\Permissions\CategoriesPermissionsUpdate', 5);
Event::listen('RoleWasUpdated', 'Hummingbird\Classes\Permissions\RoleUserPermissionsSyncUpdate', 1);

// Creating a User
Event::listen('Backend.User.Added', 'Hummingbird\Jobs\Users\Admin\AdminUserCreatedEmail@handle');

// Updating the User
Event::listen(Illuminate\Auth\Events\Login::class, Hummingbird\Jobs\Users\LoggedIn::class);
Event::listen('UserWasUpdated', 'Hummingbird\Classes\Permissions\PagePermissionsUpdate');
Event::listen('UserWasUpdated', 'Hummingbird\Classes\Permissions\CategoriesPermissionsUpdate');

// Permissions: Retrieve certain items
Event::listen('Tickbox.PageList.GET', 'Hummingbird\Classes\Permissions\PagePermissionsRetrieve');
Event::listen('Tickbox.CategoryList.GET', 'Hummingbird\Classes\Permissions\CategoriesPermissionsRetrieve');

// Media Centre: Events
Event::listen('Tickbox.Media.AfterUpload', 'Hummingbird\Jobs\Media\MediaImageAfterUpload');
Event::listen('Tickbox.Media.AfterDelete', 'Hummingbird\Jobs\Media\MediaImageAfterDelete');

// Plugin: Events
Event::listen('hummingbird.plugin.Install.After', 'Hummingbird\Jobs\Plugins\PluginInstallAfter');
Event::listen('hummingbird.plugin.Uninstall.After', 'Hummingbird\Jobs\Plugins\PluginUninstallAfter');

// Frontend event hooks
Event::listen('frontend.page.beforeDisplay', 'Hummingbird\Jobs\Frontend\BeforeDisplay');
Eventy::addFilter('frontend.page.beforeDisplay', 'Hummingbird\Jobs\Frontend\AddLazyLoading@handle', 40, 2);
Eventy::addFilter('frontend.page.beforeDisplay', 'Hummingbird\Jobs\Frontend\ObfuscateEmails@handle', 30, 2);
Eventy::addFilter('frontend.page.beforeDisplay', 'Hummingbird\Jobs\Media\RenderWebPImages@handle', 20, 2);
Eventy::addFilter('frontend.page.beforeDisplay', 'Hummingbird\Jobs\Frontend\ExternalLinkTargets@handle', 10, 2);