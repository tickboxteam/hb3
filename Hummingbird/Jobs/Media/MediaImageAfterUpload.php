<?php namespace Hummingbird\Jobs\Media;

use Config;
use File;
use Image;

/**
 * 
 *
 */
class MediaImageAfterUpload {
    public function handle($MediaItemFilename) {
        $StoringMediaItemFilename = $MediaItemFilename;

        if( !in_array(File::extension($MediaItemFilename), ['jpg', 'jpeg', 'png', 'gif']) || !function_exists('imagewebp') ) {
            return;
        }

        if( function_exists('imagecreatefromwebp') && !empty(config()->get('hb3-media.webp_active')) ) {
            $WEBP_Compression = (int) config()->get('hb3-media.webp_compression');

            if( empty($WEBP_Compression) || $WEBP_Compression <= 0 ) {
                $WEBP_Compression = 90;
            }

            if( $WEBP_Compression > 100 ) {
                $WEBP_Compression = 100;
            }

            if( empty( config()->get('hb3-media.webp_replace_original') ) ) {
                $StoringMediaItemFilename .= ".webp";
            }

            $WebPVersion = Image::make( $MediaItemFilename );
            $WebPVersion->encode('webp', $WEBP_Compression)->save( $StoringMediaItemFilename, $WEBP_Compression);
        }
    }
}
