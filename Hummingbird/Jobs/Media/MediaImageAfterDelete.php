<?php namespace Hummingbird\Jobs\Media;

use File;

/**
 * 
 *
 */
class MediaImageAfterDelete {
    public function handle($MediaItemFilename) {
        if( !in_array(File::extension($MediaItemFilename), ['jpg', 'jpeg', 'png', 'gif']) ):
            return;
        endif;

        // Removing WEBP images
        if( File::exists( storage_path('app/public') . $MediaItemFilename . ".webp" ) ):
            File::delete( storage_path('app/public') . $MediaItemFilename . ".webp" );
        endif;
    }
}
