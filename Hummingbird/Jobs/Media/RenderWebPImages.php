<?php namespace Hummingbird\Jobs\Media;

use File;

/**
 * 
 */
class RenderWebPImages {
    public function handle($PageContent = NULL) {
        if( !empty($PageContent) ):
            $doc = new \DOMDocument();
            @$doc->loadHTML( mb_convert_encoding($PageContent, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
            $PictureDom = $doc->createElement('picture');

            if( env('media_picture_classes') != '' ):
                $PictureDom->setAttribute('class', env('media_picture_classes') );
            endif;

            $SourceSet = $doc->createElement('source');
            $SourceSet->setAttribute('type', 'image/webp');

            foreach($doc->getElementsByTagName('img') as $img):
                // Missing images or images that do not exist? Replace with fallback
                if( env('fallback_image') && File::exists( base_path() . env('fallback_image') ) && !isLinkExternal($img->getAttribute('src')) && strpos($img->getAttribute('src'), config()->get('app.url')) === FALSE && !File::exists( storage_path( 'app/public' ) . $img->getAttribute('src')) && strpos($img->getAttribute('src'), '.svg') === FALSE ):
                    $img->setAttribute( 'src', env('fallback_image') );
                endif;

                if( config()->get('hb3-media.webp_delivery_method') == 'picture' && File::exists( storage_path( 'app/public' ) . $img->getAttribute('src') . '.webp' ) && ( !$img->hasAttribute('webp') || $img->getAttribute('webp') === 'true' ) ):
                    $PictureDomCloned = $PictureDom->cloneNode();

                    $SourceSetCloned = $SourceSet->cloneNode();
                    $SourceSetCloned->setAttribute('srcset', $img->getAttribute('src') . '.webp');

                    $PictureDomCloned->appendChild($SourceSetCloned);

                    $img->parentNode->replaceChild($PictureDomCloned, $img);
                    $PictureDomCloned->appendChild($img);
                endif;

            endforeach;

            // As we've encoded it to UTF8, we now need to decode the content
            // https://stackoverflow.com/questions/5495920/php-url-decode-get-from-url
            $PageContent = rawurldecode($doc->saveHTML());
        endif;

        return $PageContent;
    }
}