<?php namespace Hummingbird\Jobs\Users\Admin;

use Log;
use Mail;
use Hummingbird\Models\User;

/**
 * On create a user am email needs to trigger to admin to create a profile.
 *
 * @since   1.2.0
 * @package Hummingbird
 */
class AdminUserCreatedEmail {
    public function handle( $Data ) {
        $User = $Data['User'];

        if ( !$User instanceof User ) {
            return;
        }

        try {
            Mail::send('HummingbirdBase::emails.backend.user.CreateUser', ['User' => $User], function ($message) use($User) {
                $message->to($User->email)
                    ->subject('Your account has been created. Update your details.');

                $headers = $message->getHeaders();
                $headers->addTextHeader('EMAIL-TYPE', 'HB3: New user created');
                $headers->addTextHeader('EMAIL-ADMIN', TRUE);
            });
        }
        catch( \Exception $e ) {
            Log::error( "There was a problem sending this email: " . $e->getMessage() );
        }
    }
}
