<?php namespace Hummingbird\Jobs\Users;

use Hummingbird\Models\User;

/**
 * LoggedIn job to help us track, neatly, when someone
 * logs in. All we want to do here is track the last
 * login time. But could be extended to do more.
 *
 * @since   1.2.0
 * @package Hummingbird
 */
class LoggedIn {
    public function handle($event) {
        if ( !$event->user instanceof User ) {
            return;
        }

        $event->user->last_login = now();
        $event->user->save();

        session()->forget('guest');
    }
}
