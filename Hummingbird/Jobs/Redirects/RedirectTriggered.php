<?php namespace Hummingbird\Jobs\Redirects;

use Hummingbird\Models\Redirections;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class to trigger the a log where a redirect is activated
 *
 * @since 1.1.31
 * @version 1
 */
class RedirectTriggered
{
    use SerializesModels;
 
    public $redirect;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Redirections $redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}