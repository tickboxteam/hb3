<?php namespace Hummingbird\Jobs\Frontend;

/**
 * Event handler class to 
 */
class ObfuscateEmails {
    public function handle($PageContent = NULL) {
        if( !empty($PageContent) ):
            $PageContent = obfuscate_mailto_links($PageContent);
            $PageContent = obfuscate_emails($PageContent);
        endif;

        return $PageContent;
    }
}