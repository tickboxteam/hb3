<?php namespace Hummingbird\Jobs\Frontend;

use Eventy;

class BeforeDisplay {
    public function handle($PageData = NULL) {
        if( isset($PageData['pageContent']) && !empty($PageData['pageContent']) ):

            $value = $og_value = $PageData['pageContent'];

            while( !is_null($value) ):
                $value = Eventy::filter('frontend.page.beforeDisplay', $PageData['pageContent']);

                if( $value != $og_value ):
                    $PageData['pageContent'] = $og_value = $value;
                endif;

                if( $og_value == $value ):
                    $value = NULL;
                endif;
            endwhile;
        endif;

        return $PageData['pageContent'];
    }
}