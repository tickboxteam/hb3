<?php namespace Hummingbird\Jobs\Frontend;

use ArrayHelper;
use Config;

/**
 * Event handler class to append the loading="lazy" to all iFrame 
 * and image elements. The loading="lazy" will only be added 
 * if the item does not appear in the exclusion array.
 * 
 */
class AddLazyLoading {
    public function handle($PageContent = NULL) {
        $this->LazyLoading = ( env('media_lazy_loading') === true || env('media_lazy_loading') === 'true' || env('media_lazy_loading') === '1' ) ? TRUE : FALSE;
        $this->ExcludePaths = ( env('media_lazy_loading_exclude_paths') && env('media_lazy_loading_exclude_paths') != '' ) ? ArrayHelper::cleanExplodedArray(explode(",", env('media_lazy_loading_exclude_paths'))) : [];

        if( !empty($PageContent) ) {
            $doc = new \DOMDocument();
            @$doc->loadHTML( mb_convert_encoding($PageContent, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );

            // Lazy load all images
            foreach($doc->getElementsByTagName('img') as $img) {
                // Only add lazy loading if not in the exclusion path
                if( !$img->hasAttribute('loading') && $this->LazyLoading && ( count($this->ExcludePaths) <= 0 || ( count($this->ExcludePaths) > 0 && !$this->srcInExcludePath($img->getAttribute('src')) ) ) ) {
                    $img->setAttribute('loading', 'lazy');
                }

                // Do the opposite if lazy loading was added and the path appears in the exlcusion
                if( $img->hasAttribute('loading') && !$this->LazyLoading && ( count($this->ExcludePaths) > 0 && $this->srcInExcludePath($img->getAttribute('src')) ) ) {
                    $img->removeAttribute('loading');
                }
            }

            // Lazy load (or remove lazy load) all iframes
            foreach($doc->getElementsByTagName('iframe') as $iframe) {
                if( !$iframe->hasAttribute('loading') && $this->LazyLoading ) {
                    $iframe->setAttribute('loading', 'lazy');
                }

                if( $iframe->hasAttribute('loading') && !$this->LazyLoading ) {
                    $iframe->removeAttribute('loading');
                }
            }

            // As we've encoded it to UTF8, we now need to decode the content
            // https://stackoverflow.com/questions/5495920/php-url-decode-get-from-url
            $PageContent = rawurldecode($doc->saveHTML());
        }

        return $PageContent;
    }

    /**
     * Check to see if the path does not appear in the exclusion array
     * @param  String $source
     * @return Boolean
     */
    protected function srcInExcludePath( $source ) {
        return array_filter($this->ExcludePaths, function($item) use ($source) {
            return strpos($item, $source) === false;
        });
    }
}