<?php namespace Hummingbird\Jobs\Frontend;

/**
 * Event handler to allow us to add targets to all external
 * website links that have been created/added.
 */
class ExternalLinkTargets {
    public function handle($PageContent = NULL) {
        if( !empty($PageContent) && env('EXTERNAL_LINKS_TARGET') && in_array(env('EXTERNAL_LINKS_TARGET'), ['_blank', '_self', '_parent', '_top']) ):
            $doc = new \DOMDocument();
            @$doc->loadHTML( mb_convert_encoding($PageContent, 'HTML-ENTITIES', 'UTF-8'), \LIBXML_HTML_NODEFDTD | \LIBXML_HTML_NOIMPLIED);

            foreach($doc->getElementsByTagName('a') as $link):
                if( isLinkExternal( $link->getAttribute('href') ) && ( !$link->hasAttribute('target') || $link->getAttribute('target') == '' ) ):
                    $link->setAttribute('target', env('EXTERNAL_LINKS_TARGET'));
                endif;
            endforeach;

            // As we've encoded it to UTF8, we now need to decode the content
            // https://stackoverflow.com/questions/5495920/php-url-decode-get-from-url
            $PageContent = rawurldecode( $doc->saveHTML() );

        endif;

        return $PageContent;
    }
}