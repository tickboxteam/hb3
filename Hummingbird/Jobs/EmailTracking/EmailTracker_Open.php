<?php namespace Hummingbird\Jobs\EmailTracking;

use Hummingbird\Providers\EmailTracker\Models\EmailActivity;

/**
 * 
 */
class EmailTracker_Open {
    public $tracker;

    public function __construct($tracker) {
        $this->tracker = $tracker;
    }

    public function handle() {
        $this->tracker->activities()->save( new EmailActivity(array('type' => 'open')) );
    }
}