<?php namespace Hummingbird\Jobs\EmailTracking;

use Hummingbird\Providers\EmailTracker\Models\EmailActivity;

/**
 * 
 */
class EmailTracker_Click {
    public $tracker;
    public $url;

    public function __construct($tracker, $url) {
        $this->tracker = $tracker;
        $this->url = $url;
    }

    public function handle() {
        $this->tracker->activities()->save( new EmailActivity(array('type' => 'click', 'link' => $this->url)) );
    }
}