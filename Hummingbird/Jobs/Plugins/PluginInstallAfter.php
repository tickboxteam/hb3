<?php namespace Hummingbird\Jobs\Plugins;

use Illuminate\Filesystem\Filesystem;

/**
 * Event handler to help us do things when plugins
 * are installed. These actions run after 
 * everything has been installed.
 */
class PluginInstallAfter {
    public function handle($package) {
        $Vendor = $package['vendor'];
        $Package_Name = $package['package_name'];

        if( app(Filesystem::class)->isDirectory( base_path( "plugins/{$Vendor}/{$Package_Name}/src/public" ) ) ) {
            $pluginsAssetsPath = public_path( "plugins/{$Vendor}/{$Package_Name}" );

            if( !file_exists($pluginsAssetsPath) ) {
                app(Filesystem::class)->makeDirectory($pluginsAssetsPath, 0755, true);
            }

            if( !file_exists( public_path( "plugins/{$Vendor}/{$Package_Name}/public" ) ) ) {
                // Create target symlink public theme assets directory if required
                symlink( base_path( "plugins/{$Vendor}/{$Package_Name}/src/public" ), public_path( "plugins/{$Vendor}/{$Package_Name}/public" ) );
            }
        }
    }
}
