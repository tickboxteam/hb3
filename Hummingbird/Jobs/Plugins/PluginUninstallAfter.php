<?php namespace Hummingbird\Jobs\Plugins;

use Illuminate\Filesystem\Filesystem;

/**
 * Event handler to help us do things when plugins
 * are uninstalled. These actions run after 
 * everything has been removed.
 */
class PluginUninstallAfter {
    public function handle($package) {
        $this->Vendor = $package['vendor'];
        $this->PackageName = $package['package_name'];

        $this->deletePublishedAssets();
        $this->cleanupPublicPluginFolders();
    }

    /**
     * Checking the config directory for published assets 
     * and then removing them.
     * 
     * @return void
     */
    protected function deletePublishedAssets():void {
        if( app(Filesystem::class)->isDirectory( base_path() . "/plugins/{$this->Vendor}/{$this->PackageName}/src/config" ) ):
            $ConfigVendorPath = strtolower($this->Vendor) . "-" . strtolower($this->PackageName);

            foreach( glob( base_path() . "/plugins/{$this->Vendor}/{$this->PackageName}/src/config/*.php" ) as $CoreConfig ):
                $ConfigInfo = pathinfo($CoreConfig);

                if( app(Filesystem::class)->exists( base_path() . "/config/{$ConfigVendorPath}-{$ConfigInfo['basename']}" ) ):
                    app(Filesystem::class)->delete( base_path() . "/config/{$ConfigVendorPath}-{$ConfigInfo['basename']}" );
                endif;
            endforeach;
        endif;
    }

    /**
     * Cleanup the public plugins directory whe uninstalling plugins
     * 
     * @return void
     */
    protected function cleanupPublicPluginFolders():void {
        // Do we have a symlinked public directory for this plugin?
        if( app(Filesystem::class)->isDirectory( public_path( "plugins/{$this->Vendor}/{$this->PackageName}/public" ) ) ) {
            unlink( public_path( "plugins/{$this->Vendor}/{$this->PackageName}/public" ) );
            app(Filesystem::class)->deleteDirectory( public_path( "plugins/{$this->Vendor}/{$this->PackageName}" ) );
        }

        // Check if the Vendor directory has anything inside, if not, remove it.
        if( app(Filesystem::class)->isDirectory(public_path( "plugins/{$this->Vendor}")) && count( app(Filesystem::class)->directories(public_path( "plugins/{$this->Vendor}")) ) == 0 ) {
            app(Filesystem::class)->deleteDirectory( public_path( "plugins/{$this->Vendor}" ) );
        }

        // Check if the public plugin directory has anything inside, if not, remove it.
        if( app(Filesystem::class)->isDirectory(public_path( "plugins")) && count( app(Filesystem::class)->directories(public_path( "plugins")) ) == 0 ) {
            app(Filesystem::class)->deleteDirectory( public_path( "plugins" ) );
        }
    }
}
