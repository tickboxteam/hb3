<?php namespace Hummingbird\Contracts;

interface MetaDataInterface {
    public function meta();
    public function metaTitle();
    public function metaDescription();
    public function metaRobots();
    public function metaCanonicalURL();
    public function metaFacebookTitle();
    public function metaFacebookDescription();
    public function metaFacebookImage();
    public function metaTwitterTitle();
    public function metaTwitterDescription();
    public function metaTwitterImage();

    public function getSeoTitle();
    public function getSeoDescription();

    public function getFacebookSeoTitle();
    public function getFacebookSeoDescription();
    public function getFacebookSeoImage();
    
    public function getTwitterSeoTitle();
    public function getTwitterSeoDescription();
    public function getTwitterSeoImage();

    public function hasSeoRobots();
    public function getSeoRobots();

    public function hasCanonicalURL();
    public function getCanonicalURL();
}
