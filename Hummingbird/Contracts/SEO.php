<?php namespace Hummingbird\Contracts;

interface SEO {
    public function registerSEO($article);
    public function registerCanonicalLink($article, $link);
}
