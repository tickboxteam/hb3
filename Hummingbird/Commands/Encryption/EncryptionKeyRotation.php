<?php namespace Hummingbird\Commands\Encryption;

use Event;
use Illuminate\Console\Command;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Str;

/**
 * Abilty to allow the sytem to rotate the Encryption keys.
 *
 * If data is encrypted you make hook into this command, 
 * which fires a "System.Encryption.Rotate" event. The 
 * parameters will pass in the Old and New Encrypter.
 *
 * Info: https://techsemicolon.github.io/blog/2019/06/14/laravel-app-key-rotation-policy-for-security/
 * 
 * @since 2.3.0
 */
class EncryptionKeyRotation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hb3:encrypter-upgrade {--oldappkey= : Old app key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Encryption key rotation for all records that are encrypted.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Legacy key
        $oldAppKey = $this->option('oldappkey');

        // Get cipher of encryption
        $cipher = config('app.cipher');

        // Get newly generated app_key
        $newAppKey = config('app.key');

        // Verify old app Key
        if (Str::startsWith($oldAppKey, 'base64:')) {
            $oldAppKey = base64_decode(substr($oldAppKey, 7));
        }

        // Verify new app Key
        if (Str::startsWith($newAppKey, 'base64:')) {
            $newAppKey = base64_decode(substr($newAppKey, 7));
        }

        // Initialize encryptor instance for old app key
        $oldEncryptor = new Encrypter((string)$oldAppKey, $cipher);

        // Initialize encryptor instance for new app key
        $newEncryptor = new Encrypter((string)$newAppKey, $cipher);

        Event::dispatch("System.Encryption.Rotate", [$oldEncryptor, $newEncryptor]);

        $this->info('Encryption completed with newly rotated key');
    }
}
