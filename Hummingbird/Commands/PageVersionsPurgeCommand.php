<?php namespace Hummingbird\Commands;

use General;
use Carbon\Carbon;
use Hummingbird\Models\PageVersion;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PageVersionsPurgeCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'hb3:page-versions-purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for purging older page versions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $log_info = 'Purge older page versions';

    /**
     * The maximum age of versions to keep
     *
     * @var integer
     */
    protected $MaxAge = 24;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->removalDate = Carbon::now()->startOfDay()->subMonths( $this->MaxAge );

        $this->purgePageVersions();
    }


    /**
     * Remove the page versions older than set max age
     */
    protected function purgePageVersions()
    {
        // Get all page versions older than 24 months old
        $PagesVersionPageIds = PageVersion::select('page_id')->groupBy('page_id')->get();
        
        foreach($PagesVersionPageIds as $PagesVersionPageId) {
            // Total number of page versions
            $TotalPageVersions = PageVersion::where('page_id', $PagesVersionPageId->page_id)->count();

            // Calculate the versions we need to delete
            $DeletePageVersionsQuery = PageVersion::where('created_at', '<', $this->removalDate->format('Y-m-d H:i:s'))->where('page_id', $PagesVersionPageId->page_id)->orderBy('created_at', 'DESC');
            $DeletePageVersionsIds = $DeletePageVersionsQuery->pluck('id')->toArray();

            // If we have any versions to be deleted
            if( $DeletePageVersionsQuery->count() > 0 ):

                // If we have the same number of versions overall, as we do versions to be deleted, then pop the first item off the array.
                // Keep one recent version (for backup)
                if( $TotalPageVersions == $DeletePageVersionsQuery->count() ):
                    array_shift($DeletePageVersionsIds); // remove the first item in the array (or the most recently created version)
                endif;

                // Now delete the versions where the ID's are in there
                PageVersion::whereIn('id', $DeletePageVersionsIds)->delete();
            endif;
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}