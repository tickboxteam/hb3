<?php namespace Hummingbird\Commands;

use Hummingbird\Models\MediaCollection;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RemoveEmptyCollectionCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'hb3-media:clean-collections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for cleaning/purging empty media collections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $log_info = 'Clean the Media Centre collections';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Checking for media collections.");

        $MediaCollections = MediaCollection::whereNull('parent_collection')->get();

        foreach($MediaCollections as $MediaCollection) {
            $this->checkAndCleanMediaCollection( $MediaCollection );
        }

        $this->info("Media collections checked.");
    }

    protected function checkAndCleanMediaCollection( $MediaCollection ) {
        if( $MediaCollection->children->count() > 0 ) {
            foreach( $MediaCollection->children as $ChildCollection ) {
                $this->checkAndCleanMediaCollection( $ChildCollection );
            }
        }

        if( $MediaCollection->children->count() + $MediaCollection->resources->count() <= 0 ) {
            $this->info("Removed collection: '{$MediaCollection->name}'. [Children: {$MediaCollection->children->count()}, Resources {$MediaCollection->resources->count()}]");

            $MediaCollection->delete();
        }
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
