<?php namespace Hummingbird\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Event;
use Hummingbird\Models\Media;

class ConvertExistingMediaItemsToWEBP extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'hb3-media:convert-to-webp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert existing images to WEBP';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $log_info = 'Converting all images to WEBP';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $MediaItems = Media::where(function($query) {
            $query->where('location', 'LIKE', '%.jpg%')
                ->orWhere('location', 'LIKE', '%.jpeg%')
                ->orWhere('location', 'LIKE', '%.png%')
                ->orWhere('location', 'LIKE', '%.gif%');
        })->get();

        foreach($MediaItems as $Media) {
            try {
                $this->info( base_path() . $Media->location );
                Event::dispatch('Tickbox.Media.AfterUpload', ['MediaItemFilename' => base_path() . $Media->location]);
            }catch(\Exception $e) {
                // silence is golden
            }
        }
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
