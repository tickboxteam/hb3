<?php namespace Hummingbird\Commands\Taxonomy;

use Hummingbird\Models\Categories;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CleanTaxonomyCategoryPermalinks extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'hb3:category-permalinks-clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean permalinks of all categories';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $DuplicateURLS = Categories::taxonomyItemsWithDuplicatePermalinks()->get();
        $EmptyLinks = Categories::type()->withTrashed()->where('slug', '')->orderBy('id', 'ASC')->get();

        if( $DuplicateURLS->count() <= 0 && $EmptyLinks->count() <= 0 ) {
            $this->info('No duplicate/empty categories. Well done!');
            return;
        }

        $CategoryPermalinks = $DuplicateURLS->pluck('slug')->all();

        foreach( $CategoryPermalinks as $CategoryPermalink ):
            $Counter = 0;

            // get all the posts (in reverse order)
            foreach( Categories::type()->withTrashed()->where('slug', $CategoryPermalink)->orderBy('id', 'ASC')->get() as $Category ):
                if( $Counter > 0 ):
                    $OldPermalink = $Category->slug;

                    $Category->timestamps = false;
                    $Category->force_clean_url = TRUE;
                    $Category->slug = str_slug("{$OldPermalink}-{$Counter}");
                    $Category->save();

                    $this->info("Category #{$Category->id} URL updated from {$OldPermalink} to {$Category->slug}");
                endif;

                $Counter++; 
            endforeach;
        endforeach;

        // Cleaning those categories where the URL is empty
        foreach( $EmptyLinks as $Category ):
            $Category->timestamps = false;
            $Category->force_clean_url = TRUE;
            $Category->save();

            $this->info("Category permalink has been set to {$Category->slug}");
        endforeach;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
