<?php namespace Hummingbird\Commands\Taxonomy;

use DB;
use Hummingbird\Models\Tags;
use Hummingbird\Models\Categories;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CleanTaxonomyRelationship extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'hb3:taxonomy-relationship-clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean relationship of all non existing taxonomy';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $TaxonomyIds = array_merge(
            Tags::type()->withTrashed()->pluck('id')->toArray(),
            Categories::type()->withTrashed()->pluck('id')->toArray()
        );
        
        DB::table('taxonomy_relationships')->whereNotIn('term_id', $TaxonomyIds)->delete();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
