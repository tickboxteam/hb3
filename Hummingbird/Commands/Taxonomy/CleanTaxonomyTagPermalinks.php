<?php namespace Hummingbird\Commands\Taxonomy;

use Hummingbird\Models\Tags;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CleanTaxonomyTagPermalinks extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'hb3:tag-permalinks-clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean permalinks of all tags';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $DuplicateURLS = Tags::taxonomyItemsWithDuplicatePermalinks()->get();
        $EmptyLinks = Tags::type()->withTrashed()->where('slug', '')->orderBy('id', 'ASC')->get();

        if( $DuplicateURLS->count() <= 0 && $EmptyLinks->count() <= 0 ) {
            $this->info('No duplicate/empty tags. Well done!');
            return;
        }

        $TagPermalinks = $DuplicateURLS->pluck('slug')->all();

        foreach( $TagPermalinks as $TagPermalink ):
            $Counter = 0;

            // get all the tags (in reverse order)
            foreach( Tags::type()->withTrashed()->where('slug', $TagPermalink)->orderBy('id', 'ASC')->get() as $Tag ):
                if( $Counter > 0 ):
                    $OldPermalink = $Tag->slug;

                    $Tag->timestamps = false;
                    $Tag->force_clean_url = true;
                    $Tag->slug = str_slug("{$OldPermalink}-{$Counter}");
                    $Tag->save();

                    $this->info("Tag #{$Tag->id} URL updated from {$OldPermalink} to {$Tag->slug}");
                endif;

                $Counter++; 
            endforeach;
        endforeach;

        // Cleaning those tags where the URL is empty
        foreach( $EmptyLinks as $Tag ):
            $Tag->timestamps = false;
            $Tag->force_clean_url = TRUE;
            $Tag->save();

            $this->info("Tag permalink has been set to {$Tag->slug}");
        endforeach;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
