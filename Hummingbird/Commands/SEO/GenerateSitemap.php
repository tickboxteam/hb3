<?php namespace Hummingbird\Commands\SEO;

use Log;
use Carbon\Carbon;
use Hummingbird\Models\Page;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GenerateSitemap extends Command {
    /**
     * Include trailing slashes?
     * @var boolean
     */
    public $trailingSlashes = true;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'hb3-seo:generate-sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generating a web sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $log_info = 'Generating the website sitemap';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // TODO: Index 'pages', 'news', 'events', 'products'
        $this->Sitemap = Sitemap::create();

        try {
            $Pages = Page::whereNull('parentpage_id')->where('status', 'public')->orderby('parentpage_id', 'ASC')->orderby('position', 'ASC')->get();

            foreach($Pages as $Page) {
                // No robots set OR no items containing "noindex", "nofollow" or "none" in the available list
                if( $this->canIndexPage( $Page ) ) {

                    $this->Sitemap->add( Url::create( $this->getSEOPermalink( url($Page->permalink) ) )
                        ->setLastModificationDate( $Page->updated_at ));

                    $this->addSubPages( $Page );
                }
            }
        }
        catch(\Exception $e) {
            Log::error("Unable to process sitemap page");
            Log::error($e->getTraceAsString());
        }

        $this->Sitemap->writeToFile( public_path() . '/sitemap.xml');
    }



    /**
     * Adding, recursively, all active child pages to the sitemap
     * @param Page $page
     */
    protected function addSubPages( $Page ) {
        if( $Page->hasActiveChildren() ) {
            foreach($Page->getActiveChildren() as $ChildPage) {
                if( $this->canIndexPage( $ChildPage ) ) {

                    $this->Sitemap->add( Url::create( $this->getSEOPermalink( url($ChildPage->permalink) ) )
                        ->setLastModificationDate( $ChildPage->updated_at ));
                }

                $this->addSubPages( $ChildPage );
            }
        }
    }

    /**
     * Is this page indexable?
     * 
     * @param   Page $page
     * @return  Boolean
     */
    protected function canIndexPage($Page) {
        return ( !$Page->hasSeoRobots() || !(count(array_intersect(['noindex', 'nofollow', 'none'], explode(",", $Page->getSeoRobots())))) );
    }


    /**
     * Return the permalink of the page with, or without, a
     * trailing slash.
     *
     * Google, and other Search Engines, prefer URLs with 
     * trailing slashes as well as site links being with
     * a trailing slash. We think this could cause
     * indexing issues.
     * 
     * @param  String $Permalink
     * @return String
     */
    public function getSEOPermalink( $Permalink ) {
        if( $this->trailingSlashes ) {
            if( substr($Permalink , -1) != '/' ) {
                return $Permalink .= '/';
            }   
        }

        return trim($Permalink, '/');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
