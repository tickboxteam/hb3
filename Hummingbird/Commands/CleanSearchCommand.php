<?php namespace Hummingbird\Commands;

use General;
use Hummingbird\Models\Searcher;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CleanSearchCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'hb3:search-cleaner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for cleaning the search of all broken links';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $log_info = 'Clean the Search Facility';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Checking the search table.");

        $ItemsRemovedFromSearch = 0;

        foreach( Searcher::all() as $Searcher_Item) {
            if( !$Searcher_Item->item || !class_exists($Searcher_Item->type) || ($Searcher_Item->item && !$Searcher_Item->item->isSearchable()) ) {
                $Searcher_Item->delete();

                $this->info( "'{$Searcher_Item->title}' has been removed from the search. {$Searcher_Item->id}" );                

                $ItemsRemovedFromSearch++;
            }
        }

        if( $ItemsRemovedFromSearch > 0 ) {
            $this->info( $ItemsRemovedFromSearch . ' ' . General::singular_or_plural($ItemsRemovedFromSearch, 'item') . ' have been removed from the search.');
        }

        $this->info("Search table has been checked.");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
