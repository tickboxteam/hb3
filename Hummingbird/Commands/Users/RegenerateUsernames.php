<?php namespace Hummingbird\Commands\Users;

use Hummingbird\Models\User;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RegenerateUsernames extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'hb3:users-username-regenerate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset all usernames for users (in order)';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Regenerate usernames where usernames contain email addresses
        foreach( User::withTrashed()->orderBy('id', 'ASC')->get() as $User ):
            if( strpos($User->username, '@') !== FALSE ):
                $User->username = '';
                $User->save();
            endif;
        endforeach;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
