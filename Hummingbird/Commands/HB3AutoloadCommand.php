<?php namespace Hummingbird\Commands;

use Log;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class HB3AutoloadCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'hb3:dumpautoload';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for autoloading the new classes in Hummingbird3';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $log_info = 'Reloading classes for Hummingbird3';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		set_time_limit(-1);
		$Command = 'php composer';

		if( env('COMPOSER_FULL_PATH') ) {
			$Command = env('COMPOSER_FULL_PATH');
		}

		if( empty($Command) && env('PHP_LOCATION') && env('COMPOSER_LOCATION') ) {
			$Command = env('PHP_LOCATION') . " " . env('COMPOSER_LOCATION');
		}

		if( !empty($Command) ) {
			$this->command 	= $Command . ' dumpautoload';
			$this->package = $this->argument('package');

			if( isset($this->package) ) {
				$this->command 	.= " -d {$this->package}";
				$this->log_info .= ": {$this->package}";
			}

			Log::info($this->log_info);
			exec($this->command);
			return true;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('package', InputArgument::OPTIONAL, 'This is the path of the package you want to autoload.', NULL)
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}
}
