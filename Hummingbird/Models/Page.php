<?php namespace Hummingbird\Models;

use ArrayHelper;
use Auth;
use DB;
use General;
use Hash;
use Request;
use View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hummingbird\Contracts\MetaDataInterface;
use Hummingbird\Interfaces\Searchable;
use Hummingbird\Libraries\Themes;
use Hummingbird\Models\Searcher;
use Hummingbird\Traits\MetaDataTrait;
use Hummingbird\Traits\ObjectPermissionTrait;

class Page extends Model implements MetaDataInterface, Searchable
{
    use MetaDataTrait, SoftDeletes, ObjectPermissionTrait;

    public $table = 'pages';
    public $cms_url = 'pages';
    public $dash_icon = 'fa-edit';
    public $cms_type = 'Page';

    protected $fillable = array('title', 'summary', 'url', 'parentpage_id', 'content', 'enable_comments', 'hide_from_search', 'breadcrumb', 'status', 'protected', 'username', 'password', 'show_in_nav', 'searchable', 'search_image', 'featured_image', 'custom_menu_name', 'position', 'template', 'locked');
    
    public $search_fields = array('title', 'url', 'content');
    public $export_fields = array('title', 'url', 'content', 'enable_comments', 'hide_from_search', 'status', 'protected');

    protected $permission_type = 'pages';

    public static $rules = array(
        'title' => 'required'
    );


    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function($page) {
            $page->searchable = ( !env('HB3_PAGES_SEARCHABLE') || filter_var( env('HB3_PAGES_SEARCHABLE'), FILTER_VALIDATE_BOOLEAN) ) ? 1 : 0;
            $page->template = config()->get('hb3-pages.default_template');
        });
        
        static::created(function($page) {
            $page->permalink = str_slug( $page->title );
            $page->save();
        });

        static::deleting(function($page) { 
            if( $page->forceDeleting ) {

                // Has versions - then delete them
                if($page->versions->count() > 0) {
                    $page->versions()->forceDelete();
                }
            }
        });
    }

    /**
     * Posts can belong to many categories and vice versa
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Categories::class, 'taxonomy_relationships', 'item_id', 'term_id')
            ->where('type', 'category')
            ->where('tax_type', get_class($this));
    }

    /**
     * Posts can belong can have many tags and vice versa
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tags::class, 'taxonomy_relationships', 'item_id', 'term_id')
            ->where('type', 'tag')
            ->where('tax_type', get_class($this));
    }


    /**
     * Get all page versions from this page
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function versions()
    {
        return $this->hasMany(PageVersion::class, 'page_id', 'id');
    }


    /**
     * Get the latest PageVersion
     */
    public function latest_version() {
        return $this->versions()->orderBy('created_at', 'DESC')->first();
    }



    public function searcher()
    {
        return $this->hasOne(Searcher::class, 'item_id', 'id')->where('type', self::class);
    }

    public function children() {
        return $this->hasMany(Page::class, 'parentpage_id', 'id')->orderBy('position');
    }


    public function active_children() {
        return $this->children()
            ->where('status', 'public');
    }


    /**
     * Restore a soft-deleted model instance.
     * Save this status as draft first
     *
     * @return bool|null
     */
    public function restore() {
        $this->attributes['status'] = 'draft';

        return parent::restore();
    }

    public function scopeObjectPermissions($query) {
        if(Auth::user() && !Auth::user()->isSuperUser() && Auth::user()->objectPermissions()->where('item_type', static::class)->count() > 0) {
            $query->whereIn('id', Auth::user()->objectPermissions()->where('item_type', static::class)->pluck('item_id')->all());
        }

        return $query;
    }




    public function getBackendURL()
    {
        return '/' . General::backend_url() . '/' . $this->cms_url . '/edit/' . $this->attributes['id'];
    }

    public function getSpecialPerms()
    {
        $perms = array();

        $perms[] = array(
            'label' => 'Limit accessible pages',
            'data' => array()
        );
    }
    
    public function isLive()
    {
        return ($this->status == 'public') ? true:false;
    }

    public function parentpage()
    {
        return $this->belongsTo(Page::class);
    }

    public function active_parent() {
        return $this->parentpage()
            ->where('status', 'public');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function storeTaxonomy($tags, $page, $class, $Microsites = NULL)
    {
        if(null !== $Microsites AND null !== $Microsites->microsite and $Microsites->microsite->type == 'standard')
        {
            $Microsites->purgeDBConnection();

            DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $page->id)->where('microsite_id', $Microsites->microsite->id)->delete();

            $input = array();
            foreach($tags as $tag)
            {
                $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $page->id, 'microsite_id' => $Microsites->microsite->id);
            }

            DB::table('taxonomy_relationships')->insert($input);
            
            $Microsites->purgeDBConnection($Microsites->new_prefix);

            return;
        }

        /* No microsites */
        DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $page->id)->delete();

        $input = array();
        foreach($tags as $tag)
        {
            $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $page->id);
        }

        if(count($input) > 0)
        {
            DB::table('taxonomy_relationships')->insert($input);
        }
    }
    
    public function taxonomy($Microsites = null)
    {
        $data = array('category' => array(), 'tag' => array());

        if(null !== $Microsites AND null !== $Microsites->microsite and $Microsites->microsite->type == 'standard')
        {
            $Microsites->purgeDBConnection();

            $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->where('taxonomy_relationships.microsite_id', $Microsites->microsite->id)->get();

            $Microsites->purgeDBConnection($Microsites->new_prefix);
        }
        else
        {
            $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type');

            if(null !== $Microsites AND null === $Microsites->microsite) $terms = $terms->whereNull("taxonomy_relationships.microsite_id");

            $terms = $terms->get();
        }
        
        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }

    /**
     * Setting the breadcrumb attribute to clean up data
     */
    public function setBreadcrumbAttribute( $value ) {
        $this->attributes['breadcrumb'] = ( !empty($value) ) ? TRUE : NULL;
    }

    /**
     * Check and set the custom menu name Attibute
     * FIXME: Temporary fix in place to stop email addresses being inserted here

     * @param String $value
     */
    public function setCustomMenuNameAttribute($value) {
        $value = trim(preg_replace("/[^@\s]*@[^@\s]*\.[^@\s]*/", "", $value));

        $this->attributes['custom_menu_name'] = (!empty($value)) ? $value : ''; //FIXME: This doesn't feel right because of the issues with default values
    }

    public function setPasswordAttribute($value)
    {
        if(strlen($value) == 0) return $this->attributes['password'] = '';

        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Set the summary attribute
     * @param String $value
     */
    public function setSummaryAttribute($value) {
        $this->attributes['summary'] = (!empty($value)) ? $value : NULL;
    }

    /**
     * Set the template attribute to NULL if empty or not set.
     * @param Mixed $value
     */
    public function setTemplateAttribute($value) {
        $this->attributes['template'] = ( trim($value) != '' ) ? trim($value) : NULL;
    }

    public function setUrlAttribute($value)
    {
        $this->attributes['url'] = ( empty($value) ) ? PagesHelper::get_url_from_title($this->attributes['title']) : trim($value, '/');

        if( (isset($this->attributes['id']) AND $this->attributes['id'] == 1) || $this->attributes['url'] == '' ) {
            $this->attributes['url'] = '/';
        }
    }

    public function setPermalinkAttribute($value) {
        $page_id = (isset($this->attributes['id'])) ? $this->attributes['id'] : NULL;

        $this->attributes['permalink'] = ($page_id == 1) ? '/' : PagesHelper::build_slug($value, $this->attributes['parentpage_id'], $page_id);
    }

    public function getWeightAttribute()
    {
        if(null !== $this->searcher )
            return $this->searcher->weight;

        return 1;
    }

    /**
     * Return the breadcrumb path for each page
     * 
     * @return String
     */
    public function getBreadcrumbPathAttribute()
    {
        $page       = $this;
        $page_names = [];

        while ( null !== $page->parentpage ):
            $page_names[] = $page->url;

            $page = $page->parentpage;
        endwhile;

        return implode(".", array_reverse($page_names));     
    }

    /**
     * Return the breadcrumb title for this page
     * Default is page title
     * Overridden by custom menu name
     * 
     * @return String
     */
    public function getBreadcrumbTitleAttribute() {
        if( $this->custom_menu_name )
            return trim($this->custom_menu_name);

        return trim($this->title);
    }

    public static function get_selection($emptyitem_text = 'Select a page', $excludes = array())
    {
        $selection = array(0 => $emptyitem_text);
        
        $pages = Page::all();
        
        foreach($pages as $page) {
            if(in_array($page->id, $excludes)) continue;
            $selection[$page->id] = $page->title;
        }
        
        return $selection;
    }
    
    public static function get_blog_feed_styles()
    {
        return array();
    }

    public static function get_statuses()
    {
        return array('draft' => 'Draft', 'public' => 'Public');
    }
    
    public function getPermissionType()
    {
        return $this->permission_type;
    }

    public function deletedPages($count = false)
    {
        $pages = Page::whereNotNull('deleted_at');

        if($count) return $pages->count();

        return $pages->get();
    }

    public function cms_list_pages($ignore_id = false)
    {
        $pages = (!$ignore_id) ? Page::objectPermissions()->whereNull('deleted_at')->where('status', '!=', 'trash')->orderby('parentpage_id', 'ASC')->orderby('position', 'ASC')->get() : Page::where('id', '!=', $ignore_id)->whereNull('deleted_at')->where('status', '!=', 'trash')->orderby('parentpage_id', 'ASC')->orderby('position', 'ASC')->get();

        return $this->buildFlatTree($pages);
    }

    public function list_all_pages_new($page, $args = array(), $return = false) {
        if(null === $page) return;

        $max_levels     = (isset($args['levels']) && is_numeric($args['levels'])) ? $args['levels']:100;
        $counted_levels = 1;

        $pages[$page->id]['page'] = $page;

        // to be returned
        foreach($page->getActiveChildren() as $ChildPage) {
            $pages[$page->id]['children'][$ChildPage->id]['page'] = $ChildPage;
        }

        $parent_page = $page->active_parent()->first();

        while($parent_page !== NULL && $counted_levels <= $max_levels) {
            $pages[$parent_page->id]['page'] = $parent_page;

            if($parent_page->parentpage_id != NULL) {
                foreach($parent_page->getActiveChildren() as $ChildPage) {
                    $pages[$parent_page->id]['children'][$ChildPage->id]['page'] = $ChildPage;

                    if(isset($pages[$ChildPage->id])) {
                        $pages[$parent_page->id]['children'][$ChildPage->id] = $pages[$ChildPage->id];
                        unset($pages[$ChildPage->id]);
                    }
                }
            }

            $parent_page    = $parent_page->active_parent()->first();
            $counted_levels++;
        }

        if(count($pages) > 0):
            return View::make( config()->get('hb3-templates.side-navigation.template') )->with('Pages', $pages)->with('CurrentPage', $page)->with('PageArguments', $args)->withShortcodes()->render();
        endif;
    }


    public function list_all_pages($page, $args = array(), $return = false)
    {
        if(null === $page) return;
        // to be returned
        $pages = array();

        // default args
        if(empty($args))
        {
        }

        /* get ids */
        $parent_ids = array($page->id);
        $active_parent_id = $page->parentpage_id;


        // about us > home = 2 > 1 - COMPLETE
        // partners > about us > home = 4 > 2 > 1 - 

        while($active_parent_id !== NULL)
        {
            $parent_page = Page::whereNull('deleted_at')
                ->where('status', '=', 'public')
                ->where('id', '=', $active_parent_id)
                ->get()
                ->first();

            if(null !== $parent_page)
            {
                $parent_ids[] = $parent_page->id;
                $active_parent_id = $parent_page->parentpage_id;
            }
            else
            {
                $active_parent_id = NULL;
            }
        }

        if(!in_array(1, $parent_ids)) $parent_ids[] = 1;

        // dd($parent_ids);

        $these_pages = Page::whereNull('deleted_at')->where('status', '=', 'public')
            ->where(function($query) use($parent_ids)
            {
                return $query->whereIn('id', $parent_ids)->orWhereIn('parentpage_id', $parent_ids);
            })
            ->orderByRaw('position ASC')
            ->get();

        $selected_pages = array_flatten($these_pages);

        foreach($selected_pages as $key => $element)
        {
            if(count($parent_ids)-2 >= 0 AND $element->id != $parent_ids[count($parent_ids)-2] AND $element->parentpage_id === end($parent_ids))
            {
                unset($these_pages[$key]);
            }
        }

        $selected_pages = array_flatten($these_pages);
        $pages = $this->newBuildTree2($selected_pages, $page);

        if(count($pages) > 0)
        {
            ob_start();?>

            <ul id="sidenav">
                <?php $this->returnTree($pages, $page);?>
            </ul>

            <?php $content = ob_get_clean();
            
            if($return) return $content;
            echo $content;
        }
    }

    public function get_all_pages($page, $args = array(), $return = false)
    {
        if(null === $page) return;
        // to be returned
        $pages = array();

        // default args
        if(empty($args))
        {
        }

        /* get ids */
        $parent_ids = array($page->id);
        $active_parent_id = $page->parentpage_id;


        // about us > home = 2 > 1 - COMPLETE
        // partners > about us > home = 4 > 2 > 1 - 

        while($active_parent_id !== NULL)
        {
            $parent_page = Page::whereNull('deleted_at')
                ->where('status', '=', 'public')
                ->where('id', '=', $active_parent_id)
                ->get()
                ->first();

            if(null !== $parent_page)
            {
                $parent_ids[] = $parent_page->id;
                $active_parent_id = $parent_page->parentpage_id;
            }
            else
            {
                $active_parent_id = NULL;
            }
        }

        // dd($parent_ids);

        $these_pages = Page::whereNull('deleted_at')->where('status', '=', 'public')
            ->where(function($query) use($parent_ids)
            {
                return $query->whereIn('id', $parent_ids)->orWhereIn('parentpage_id', $parent_ids);
            })
            ->orderByRaw('position ASC')
            ->get();

        $selected_pages = array_flatten($these_pages);

        foreach($selected_pages as $key => $element)
        {
            if($element->id != $parent_ids[count($parent_ids)-2] AND $element->parentpage_id === end($parent_ids))
            {
                unset($these_pages[$key]);
            }
        }

        $selected_pages = array_flatten($these_pages);
        return $this->newBuildTree2($selected_pages, $page);
    }


    public function loopTest($pages)
    {
        foreach ($pages as $key => $element)
        {
            General::pp($element['item']->title);

            if(isset($element['children']))
            {
                $this->loopTest($element['children']);
            }
        }        
    }

    public function buildFlatTree($elements, $parentId = null, $level = 0, &$pages = array()) 
    {
        // $pages = array();

        foreach ($elements as $key => $element) {
            /* Top level parent */
            if ($element->parentpage_id !== $parentId) continue;

            unset($elements[$key]);

            $element['title'] = str_repeat("&nbsp;", $level*2).$element['title']; //append arrow to name
            $pages[$element->id] = $element->title;
            $this->buildFlatTree($elements, $element->id, $level + 1, $pages);
        }

        return $pages;
    }

    public function newBuildTree2($elements, $current_page, $parentId = null, $pages = array()) 
    {
        foreach ($elements as $key => $element)
        {
            /* Top level parent */
            if ($element->parentpage_id !== $parentId) continue;//

            unset($elements[$key]);
            
            $pages[$element->id]['item'] = $element;
            $pages[$element->id]['children'] = $this->newBuildTree2($elements, $current_page, $element->id);
        }

        return $pages;
    }

// select * from `pages` where `deleted_at` is null and `status` = 'public' or (`id` = 3 and `parentpage_id` = 3 and `id` = 2)
// SELECT * FROM pages WHERE (id = 3 or parentpage_id = 3 or id = 2) AND deleted_at IS NULL AND status = 'public'


    public function newBuildTree(&$elements, $current_page, $parentId = null, $level = 0, $max_level = 1, $pages = array()) 
    {
        foreach ($elements as $key => $element)
        {
            /* Top level parent */
            // echo $element;
            // die();
            if(($element->id == $parentId AND $level == 0) OR ($element->parentpage_id == $parentId OR ($element->id == $current_page AND $element->id == $parentId)))
            {
                unset($elements[$key]);

                $pages[$element->id]['item'] = $element;
                $pages[$element->id]['children'] = $this->newBuildTree($elements, $element->id, $element->id, $level + 1, $max_level);
            }
        }

        return $pages;

        // foreach ($elements as $key => $element) 
        // {
        //     unset($elements[$key]);
        //     if($element->id === $parentId) continue;

        //     if(($element->id == $parentId AND $level == 0) OR $element->parentpage_id == $parentId OR $element->id == $current_page)
        //     {
                

        //         $pages[$element->id] = $element;

        //         $pages[$element->id]['children'] = $this->buildTree($elements, $element->id, $level + 1, $max_level, $pages);
        //     }
        // }

        // return $pages;
    


// use wp_list_pages to display parent and all child pages all generations (a tree with parent)
// $parent = 93;
// $args=array(
//   'child_of' => $parent
// );
// // $pages = get_pages($args);  
// if ($pages) {
//   $pageids = array();
//   foreach ($pages as $page) {
//     $pageids[]= $page->ID;
//   }

//   $args=array(
//     'title_li' => 'Tree of Parent Page ' . $parent,
//     'include' =>  $parent . ',' . implode(",", $pageids)
//   );
//   // wp_list_pages($args);
// }

    }


    public function buildTree($elements, $parentId = null, $level = 0, $max_level = 1) 
    {
        /* pages to return */
        $pages = array();

        foreach ($elements as $key => $element) 
        {
            if ($element->id !== $parentId AND ($element->parentpage_id !== $parentId OR $element->parentpage_id === NULL)) continue;

            if ($level == 0 AND $element->id !== $parentId) continue;

            if(($element->id == $parentId AND $level == 0) OR $element->parentpage_id === $parentId)
            {
                $pages[$element->id] = $element;

                if($level + 1 <= $max_level) $pages[$element->id]['children'] = $this->buildTree($elements, $element->id, $level + 1, $max_level);
            }
        }

        return $pages;
    }

    /**
     * Output standard side navigation for pages.
     * 
     * @param  Array  $elements    
     * @param  boolean $current_page
     * @param  integer $count       
     * @return String
     */
    public function subNavigation($elements, $current_page = false, $count = 0)
    {
        foreach($elements as $element)
        {
            $element_children = (isset($element['children'])) ? $element['children']:false;
            $element = (isset($element['page'])) ? $element['page']:$element;

            if(is_object($element)) {
                $class = ($this->checkURL($element->permalink)) ? 'active':'';
                $page_title = (!empty($element->custom_menu_name)) ? $element->custom_menu_name : $element->title;

                if($element->show_in_nav OR (!$element->show_in_nav AND strpos(Request::url() .'/', $element->permalink) !== false AND $element->permalink != '/'))
                {?>
                    <li class="<?=$class?>"><a data-count="<?=$count?>" class="<?=$class?>" href="<?php echo $element->permalink;?>"><?php echo $page_title;?></a>
                        <?php if(is_array($element_children) AND count($element_children) > 0)
                        {?>
                            <ul>
                                <?php $this->showCategories($element);?>
                                <?php $this->subNavigation($element_children, $current_page, $count++);?>
                            </ul>
                        <?php }?>
                    </li>
                <?php }
            }
        }
    }

    /**
     * DEPRECATED - Output standard side navigation for pages.
     * 
     * @param  Array  $elements    
     * @param  boolean $current_page
     * @param  integer $count       
     * @return String
     */
    public function returnTree($elements, $current_page = false, $count = 0)
    {
        if(is_array($elements))
        {
            foreach($elements as $key => $element)
            {
                $element_children = (isset($element['children'])) ? $element['children']:false;
                $element = (isset($element['item'])) ? $element['item']:$element;

                $class = ($this->checkURL($element->permalink)) ? 'active':'';
                $page_title = ($element['show_in_nav'] AND $element['custom_menu_name'] != '') ? $element['custom_menu_name']:$element['title'];

                if($element->show_in_nav OR (!$element->show_in_nav AND strpos(Request::url() .'/', $element->permalink) !== false AND $element->permalink != '/'))
                {?>
                    <li class="<?=$class?>"><a data-count="<?=$count?>" class="<?=$class?>" href="<?php echo $element->permalink;?>"><?php echo $page_title;?></a>
                        <?php if(is_array($element_children) AND count($element_children) > 0)
                        {?>
                            <ul>
                                <?php $this->showCategories($element);?>
                                <?php $this->returnTree($element_children, $current_page, $count++);?>
                            </ul>
                        <?php }?>
                    </li>
                <?php }
                else
                {
                    if(is_array($element_children) AND count($element_children) > 0) $this->returnTree($element_children, $current_page);
                }
            }
        }
    }

    public function checkURL($page_url)
    {
        $state              = true;
        $current_segments   = Request::segments();
        $page_segments      = ArrayHelper::cleanExplodedArray(explode("/", $page_url));

        if(!empty($current_segments) AND !empty($page_segments))
        {
            foreach($page_segments as $key => $segment)
            {
                if(!isset($current_segments[$key]) OR $current_segments[$key] !== $segment) return false;
            }

            return true;
        }

        return false;
    }

    public function showCategories($current_page)
    {
        $segments = Request::segments();

        if($current_page['permalink'] == implode('/', $segments))
        {
            $top_cat = Categories::where('slug', '=', 'shop')->whereNull('parent')->whereNull('deleted_at')->first();

            if(null !== $top_cat)
            {
                $categories = Categories::where('parent', $top_cat->id)->whereNull('deleted_at')->get();

                if(count($categories) > 0)
                {?>
                    <li><ul>

                    <?php $url_segments = Request::segments();

                    foreach($categories as $category)
                    {
                        $category_url = "/shop/" . $category->slug . '/';
                        $class = (strpos(Request::url(), rtrim($category_url, "/")) !== FALSE) ? 'active':'';?>

                        <li>
                            <a class="<?=$class?>" href="<?=$category_url?>"><?=$category->name?></a>

                            <?php if(in_array($category->slug, $url_segments))
                            {?>
                                <ul>
                                    <?=$this->getSubCategories($category->id, $url_segments, $category_url)?>
                                </ul>
                            <?php }?>
                        </li>
                    <?php }?>

                    </ul></li>
                <?php }
            }
        }
    }

    public function getSubCategories($cat_id, $url, &$cat_url = '')
    {
        $categories = Categories::where('parent', $cat_id)->whereNull('deleted_at')->get();
        
        if(count($categories) > 0)
        {
            foreach($categories as $category)
            {

                $new_cat_url = $cat_url . $category->slug . '/';
                $class = (strpos(Request::url(), rtrim($new_cat_url, "/") ) !== FALSE) ? 'active':'';?>

                <li>
                    <a class="<?=$class?>" href="<?=$new_cat_url?>"><?=$category->name?></a>

                    <?php 
                        if(in_array($category->slug, $url))
                        {?>
                            <ul>
                                <?php $this->getSubCategories($category->id, $url, $new_cat_url);?>
                            </ul>
                        <?php }
                    ?>
                </li>
            <?php }
        }
    }

/*
function buildTree(array &$elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[$element['id']] = $element;
            unset($elements[$element['id']]);
        }
    }
    return $branch;
}

*/



// function wp_list_pages( $args = '' ) {
// 1028            $defaults = array(
// 1029                    'depth' => 0, 'show_date' => '',
// 1030                    'date_format' => get_option( 'date_format' ),
// 1031                    'child_of' => 0, 'exclude' => '',
// 1032                    'title_li' => __( 'Pages' ), 'echo' => 1,
// 1033                    'authors' => '', 'sort_column' => 'menu_order, post_title',
// 1034                    'link_before' => '', 'link_after' => '', 'walker' => '',
// 1035            );
// 1036    
// 1037            $r = wp_parse_args( $args, $defaults );
// 1038    
// 1039            $output = '';
// 1040            $current_page = 0;
// 1041    
// 1042            // sanitize, mostly to keep spaces out
// 1043            $r['exclude'] = preg_replace( '/[^0-9,]/', '', $r['exclude'] );
// 1044    
// 1045            // Allow plugins to filter an array of excluded pages (but don't put a nullstring into the array)
// 1046            $exclude_array = ( $r['exclude'] ) ? explode( ',', $r['exclude'] ) : array();
// 1047    
// 1048            /**
// 1049             * Filter the array of pages to exclude from the pages list.
// 1050             *
// 1051             * @since 2.1.0
// 1052             *
// 1053             * @param array $exclude_array An array of page IDs to exclude.
// 1054             */
// 1055            $r['exclude'] = implode( ',', apply_filters( 'wp_list_pages_excludes', $exclude_array ) );
// 1056    
// 1057            // Query pages.
// 1058            $r['hierarchical'] = 0;
// 1059            $pages = get_pages( $r );
// 1060    
// 1061            if ( ! empty( $pages ) ) {
// 1062                    if ( $r['title_li'] ) {
// 1063                            $output .= '<li class="pagenav">' . $r['title_li'] . '<ul>';
// 1064                    }
// 1065                    global $wp_query;
// 1066                    if ( is_page() || is_attachment() || $wp_query->is_posts_page ) {
// 1067                            $current_page = get_queried_object_id();
// 1068                    } elseif ( is_singular() ) {
// 1069                            $queried_object = get_queried_object();
// 1070                            if ( is_post_type_hierarchical( $queried_object->post_type ) ) {
// 1071                                    $current_page = $queried_object->ID;
// 1072                            }
// 1073                    }
// 1074    
// 1075                    $output .= walk_page_tree( $pages, $r['depth'], $current_page, $r );
// 1076    
// 1077                    if ( $r['title_li'] ) {
// 1078                            $output .= '</ul></li>';
// 1079                    }
// 1080            }
// 1081    
// 1082            /**
// 1083             * Filter the HTML output of the pages to list.
// 1084             *
// 1085             * @since 1.5.1
// 1086             *
// 1087             * @see wp_list_pages()
// 1088             *
// 1089             * @param string $output HTML output of the pages list.
// 1090             * @param array  $r      An array of page-listing arguments.
// 1091             */
// 1092            $html = apply_filters( 'wp_list_pages', $output, $r );
// 1093    
// 1094            if ( $r['echo'] ) {
// 1095                    echo $html;
// 1096            } else {
// 1097                    return $html;
// 1098            }
// 1099    }


    public function scopeTrashedOnly($query)
    {
        return $query->onlyTrashed();
    }


    public function scopeNotTrashed($query)
    {
        return $query->whereNull('deleted_at');
    }

    /**
     * Return whether this page is searchable and live
     *
     * @return Boolean
     */
    public function isSearchable() {
        return ( $this->searchable && $this->isLive() );
    }


    /**
     * Return the id of the searchable subject.
     *
     * @return string
     */
    public function getSearchableId() {
        return $this->id;
    }


    /**
     * Return the title of the searchable subject
     *
     * @return string
     */
    public function getSearchableTitle() {
        return $this->title;
    }


    /**
     * Return the slug of the searchable subject
     *
     * @return string
     */
    public function getSearchableSlug() {
        return $this->permalink;
    }


    /**
     * Returns the article summary that must be indexed
     *
     * @return array
     */
    public function getSearchableSummary() {
        return $this->summary ?: NULL;
    }


    /**
     * Returns the article body that must be indexed
     *
     * @return array
     */
    public function getSearchableBody() {
        return $this->content;
    }


    /**
     * Return the type of the searchable subject.
     *
     * @return string
     */
    public function getSearchableType() {
        return get_class($this);
    }


    /**
     * Return the type of the searchable subject.
     *
     * @return string
     */
    public function getSearchableWeight() {
        if(Request::filled('weight'))
            return (int) Request::get('weight');

        return 1;
    }


    /**
     * Return the searchable_image if there is one set
     *
     * @return String
     */
    public function getSearchableImage() {
        if(!empty($this->searchable_image))
            return $this->searchable_image;
    }


    /**
     * Check if the resource has a searchable_image
     *
     * @return Boolean
     */
    public function hasSearchableImage($fields_to_search = array('search_image', 'featured_image', 'content')) {
        if(null === $this->searchable_image)
        {
            $this->searchable_image = General::findImage($this, $fields_to_search);
        }

        return !empty($this->searchable_image);
    }

    public function getMenuName() {
        if(!empty($this->attributes['custom_menu_name'])) {
            return $this->attributes['custom_menu_name'];
        }

        return $this->attributes['title'];
    }
    
    public function scopeCmsSearch($query, $term) {
        if( $term ) {
            $query->where(function($query) use($term) {
                $query->where('title', 'like', "%$term%")
                    ->orWhere('url', 'like', "%$term%");
            });
        }

        return $query;
    }


    public function hasActiveChildren() {
        return ($this->active_children()->count() > 0);
    }


    public function getActiveChildren() {
        return $this->active_children()
                ->orderBy('position')
                ->get();
    }


    public function scopeSearch($query, $search_term = NULL) {
        if(empty($search_term)) {
            return $query;
        }

        $table = DB::getTablePrefix() . $this->getTable();

        return $query->select(
            DB::raw("$table.*, 
                (
                    (
                        1 * (
                                MATCH(title) AGAINST(".DB::connection()->getPdo()->quote($search_term).")
                            )
                    )
                    +
                    (
                        1 * (
                                MATCH(url) AGAINST(".DB::connection()->getPdo()->quote($search_term).")
                            )
                    )
                    +
                    (
                        1 * (
                                MATCH(content) AGAINST(".DB::connection()->getPdo()->quote($search_term).")
                            )
                    )
                ) AS `score`"
            )
        )
        ->whereRaw("MATCH(`title`, `url`,`content`) AGAINST(".DB::connection()->getPdo()->quote($search_term).")")
        ->orderBy('score', 'DESC');
    }

    /**
     * New Scope query to get pages where they're showing in the navigation (or not)
     * @param  QueryBuilder $query
     * @param  boolean $show  Yes | No | Null (all)
     */
    public function scopeShowInNav($query, $show = true) {
        if( !is_null($show) ) {
            $query->where('show_in_nav', $show);
        }

        return $query;
    }

    /**
     * Filter pages by the parentpage_id
     */
    public function scopeParent($query, $parent = NULL) {
        if( !empty($parent) && $parent > 0 ) {
            $query->where('parentpage_id', $parent);
        }

        return $query;
    }

    /**
     * Filter pages their status
     */
    public function scopeStatus($query, $status = NULL) {
        if( !empty($status) && in_array($status, ['public', 'draft']) ) {
            $query->where('status', $status);
        }

        return $query;
    }

    public function get_templates() {
        $theme = Themes::activeTheme();
        $themePath = base_path()."/themes/public/".$theme."/*.blade.php";
        $themefiles = glob($themePath);
        $themeTemplate = array();
        foreach($themefiles as $themefile) {
            $filebasename = str_replace('.blade.php', "", basename($themefile));
            $themeTemplate[$filebasename] = ucfirst($filebasename);
        }
        asort($themeTemplate);
        return $themeTemplate;
    }
}
