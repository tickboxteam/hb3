<?php namespace Hummingbird\Models;

use App;
use Auth;
use Request;
use Illuminate\Database\Eloquent\Model;

class Activitylog extends Model 
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activitylogs';

    protected $guarded = array();
    public $search_field = 'action';
    public static $permission_type = 'logging';

    const CREATED = 'CREATED';
    const UPDATED = 'UPDATED';
    const DELETED = 'DELETED';
    
    /**
     * Get the user that the activity belongs to.
     *
     * @return object
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Create an activity log entry.
     *
     * @param  mixed
     * @return boolean
     */
    public static function log($data = array())
    {
        if (is_object($data)) $data = (array) $data;
        if (is_string($data)) $data = array('action' => $data);

        $activity = new static;
        $activity->user_id      = ( Auth::user() ) ? Auth::user()->id : NULL;
        $activity->ip_address   = Request::getClientIp();
        $activity->action       = $data['action'];
        $activity->type         = isset($data['type'])   ? $data['type']   : null;
        $activity->link_id      = isset($data['link_id']) ? $data['link_id'] : null;
        $activity->url          = isset($data['url'])      ? $data['url']      : null;
        $activity->description  = isset($data['description']) ? $data['description'] : null;
        $activity->notes        = isset($data['notes'])     ? $data['notes']     : null;
        $activity->user_agent   = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'No UserAgent';

        if( isset($data['user_id']) && is_numeric($data['user_id']) ):
            $activity->user_id = $data['user_id'];
        endif;

        if( isset($data['created_at']) || isset($data['updated_at']) ):
            $activity->timestamps = false;

            if( isset($data['created_at']) ):
                $activity->created_at = $data['created_at'];

                if( !isset($data['updated_at']) ):
                    $activity->updated_at = $data['created_at'];
                endif;
            endif;

            if( isset($data['updated_at']) ):
                $activity->updated_at = $data['updated_at'];

                if( !isset($data['created_at']) ):
                    $activity->created_at = $data['updated_at'];
                endif;
            endif;
        endif;

        $activity->save();

        return true;
    }

    /**
     * Get a shortened version of the user agent with title text of the full user agent.
     *
     * @return string
     */
    public function getUserAgentPreview()
    {
        return substr($this->user_agent, 0, 42) . (strlen($this->user_agent) > 42 ? '<strong title="'.$this->user_agent.'">...</strong>' : '');
    }

    
    public function getItemUrlAttribute($value)
    {
        // get unique field from table
        # select name from db_table
        
        return "/".App::make('backend_url')."/$this->type/edit/$this->link_id";
    }
    
    public function getItemAttribute($value)
    {
        // get unique field from table
        # select name from db_table
        
        // return substr($this->type, 0, -1);
        return $this->type;
    }

    /**
     * Query scope to return results based on an action
     * @param  QueryBuilder $query
     * @param  Mixed $action
     */
    public function scopeByaction($query, $action = null)
    {
        if( !empty($action) ) {
            return $query->where('action', strtoupper($action));
        }

        return $query;
    }

    /**
     * Query scope to return results based on an type
     * @param  QueryBuilder $query
     * @param  Mixed $type
     */
    public function scopeBytype($query, $type = null)
    {
        if( !empty($type) ) {
            return $query->where('type', $type);
        }

        return $query;
    }

    /**
     * Query scope to return results based on an individual user
     * @param  QueryBuilder $query
     * @param  Mixed $user_id
     */
    public function scopeByuser($query, $user_id = null)
    {
        if( !empty($user_id) && is_numeric($user_id) ) {
            return $query->where('user_id', $user_id);
        }

        return $query;
    }
    
    /**
     * Get a list of distinct users who've registered an activity
     * @return Array
     */
    public static function getDistinctUsers() {
        $Logs = Activitylog::select('user_id')->whereHas('user', function($query) {
            $query->whereNotNull('user_id')
                ->where('user_id', '>', 0);
        })->groupBy(['user_id'])->get();

        $Users = [];

        foreach($Logs as $Log) {
            $Users[ $Log->user->id ] = trim($Log->user->name);
        }

        asort($Users);

        return $Users;
    }

    /**
     * Get a list of distinct actions
     * @return Array
     */
    public static function getDistinctActions() {
        return Activitylog::groupBy('action')->pluck('action', 'action')->all();
    }

    /**
     * Get a list of distinct action types
     * @return Array
     */ 
    public static function getDistinctTypes() {
        return Activitylog::groupBy('type')->pluck('type', 'type')->all();
    }
}