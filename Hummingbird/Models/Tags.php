<?php namespace Hummingbird\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Hummingbird\Traits\TaxonomyHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tags extends Model {
    use Sluggable, SoftDeletes, TaxonomyHelper;

    public $table       = 'taxonomy';
    protected $fillable = array('name', 'slug', 'display_name', 'description');
    protected $guarded  = array();
    protected $type     = 'tag';

    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    public static function boot() {
        parent::boot();

        static::creating(function($Taxonomy) {
            $Taxonomy->setAttribute('type', $Taxonomy->getType() );

            if( !isset($Taxonomy->image) || $Taxonomy->image == '' ):
                $Taxonomy->image = NULL;
            endif;

            if( !isset($Taxonomy->description) || $Taxonomy->description == '' ):
                $Taxonomy->description = NULL;
            endif;
        });

        static::saving(function($Taxonomy) {
            $Taxonomy->setAttribute('type', $Taxonomy->getType() );

            if( !isset($Taxonomy->image) || $Taxonomy->image == '' ):
                $Taxonomy->image = NULL;
            endif;

            if( !isset($Taxonomy->description) || $Taxonomy->description == '' ):
                $Taxonomy->description = NULL;
            endif;
        });

        Tags::registerModelEvent('slugging', function($model) {
            if( isset($model->force_clean_url) ) {
                unset($model->force_clean_url);
            }
            
            if( !isset($model->force_clean_url) ) {
                // before we save, make sure we have a title
                if( empty($model->name) ) {
                    $model->name = $model->getRawOriginal('name');
                }
            }
        });

        static::deleted(function($Taxonomy) {
            // On purge, delete all relationships
            if ($Taxonomy->forceDeleting) {
                $Taxonomy->truncate_relationships();
            }
        });
    }

    /**
     * Does this tag exist?
     * @param  Name $name
     * @return Boolean
     */
    public function exists($name) {
        return (Tags::type()->where('name', $name)->count() > 0);
    }

    /**
     * How many times has this Tag been referenced?
     */
    public function counts() {
        return DB::table('taxonomy_relationships')->where('term_id', $this->id)->count();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array {
        $source = 'name';
        $onUpdate = true;

        if( isset($this->force_clean_url) ) {
            if( !empty($this->getRawOriginal('slug') ) ) {
                $source = 'slug';
            }
        }

        if( !isset($this->force_clean_url) ) {
            // before we save, make sure we have a name
            if( empty($this->name) ) {
                $this->name = $this->getRawOriginal('name');
            }

            // Only re-sluggify if we've changed or emptied the URL
            if( !is_null($this->getRawOriginal('slug')) && $this->getRawOriginal('slug') != $this->attributes['slug'] ) {
                $source = 'name';

                // We have changed the URL and it's not empty
                if( !empty($this->attributes['slug']) ) {
                    $source = 'slug';
                }
            }
        }

        if( isset($this->attributes['slug']) && $this->getRawOriginal('slug') == $this->attributes['slug'] ) {
            $onUpdate = false;
        }

        return [
            'slug' => [
                'source'    => $source,
                'includeTrashed' => true,
                'onUpdate'  => $onUpdate
            ]
        ];
    }
}
