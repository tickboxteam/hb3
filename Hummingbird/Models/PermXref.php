<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class PermXref extends Model
{
    public $table = 'perm_types_actions_xref';
    public $fillable = array('perm_type', 'perm_action');
    
    
    public function permType()
    {
        return $this->belongsTo('PermissionType', 'perm_type');
    }
    
    public function permAction()
    {
        return $this->belongsTo('PermissionAction', 'perm_action');
    }
}

?>
