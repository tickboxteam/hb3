<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model {
    protected $table    = 'failed_jobs';

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'failed_at' => 'datetime',
    ];

    /**
     * Get the Payload attribute (decoded)
     * @return Object
     */
    public function getPayloadAttribute() {
        return json_decode( $this->attributes['payload'] );
    }

    /**
     * Get the job attribute stored within the payload
     * @return Mixed
     */
    public function getPayloadJobAttribute() {
        $PayLoad = $this->payload;
        
        return (!empty($PayLoad->job)) ? $PayLoad->job : NULL;
    }

    /**
     * Get the data attribute stored within the payload
     * @return Mixed
     */
    public function getPayloadDataAttribute() {
        $PayLoad = $this->payload;

        return (!empty($PayLoad->data)) ? htmlentities( json_encode($PayLoad->data), true) : NULL;
    }
}
