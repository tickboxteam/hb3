<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class MetaData extends Model {
    public $table       = 'meta_content';
    public $fillable    = array('item_type', 'key', 'value');
    public $timestamps  = false;
}