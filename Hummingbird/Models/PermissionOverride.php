<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionOverride extends Model {

    public $table = 'permission_overrides';
    protected $fillable = array();
    
    protected $guarded = array();
    
    
}
