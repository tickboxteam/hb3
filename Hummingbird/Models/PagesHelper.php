<?php namespace Hummingbird\Models;

class PagesHelper
{
    public static function build_slug($url, $parent_id = NULL, $page_id = NULL) 
    {
        $permalinkFound = false;
        $counter = 0;
        $permalink = $_new_permalink = PagesHelper::get_parent_url($url, $parent_id);

        while( !$permalinkFound ) {
            $PageQuery = Page::where('permalink', "/{$_new_permalink}/")->withTrashed();

            if( !is_null($page_id) ) {
                $PageQuery->whereNotIn('id', [$page_id]);
            }

            if( $PageQuery->count() == 0 ) {
                $permalinkFound = true;
                break;
            }

            $counter++;
            $_new_permalink = trim($permalink, '/') . "-{$counter}";            
        }

        return "/{$_new_permalink}/";
    }

    public static function get_parent_url($slug = '/', $parent_id = 0)
    {
        // echo "parentid: $parent_id, slug: $slug \n";exit;
        if(null === $parent_id || $parent_id == 1) return $slug;


        
        // echo "parentid: $parent_id, slug: $slug \n";exit;
        $parent = Page::find($parent_id);

        if(null === $parent) {
            return $slug;
        }

        $slug = ($parent->id == 1) ? "/$slug":"$parent->url/$slug";

        // - ///support-us//support-us///help//
        // - ///support-us//support-us//help/
        // - support-us/support-us/help


        return PagesHelper::get_parent_url($slug, $parent->parentpage_id);
        //return $slug;
    }
    
    public static function get_url_from_title($title)
    {    
        $url = str_replace(" ", "-", $title);
        $url = preg_replace('/[^A-Za-z0-9\-]/', '', $url);

        return strtolower($url);
    }
}
