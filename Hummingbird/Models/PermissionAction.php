<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionAction extends Model
{
    public $table = 'permission_actions';

    protected $guarded = array();

    public $timestamps = false;

    /**
     * 
     * @param 
     * @return 
     */
	public function insert($action, $return = false)
	{
		$action = $this->format($action);

		if($action != '')
		{
	        $perm_action = \PermissionAction::where('action', '=', $action)->get();

	        if(!$perm_action->count() > 0)
	        {
	            $perm_action = new \PermissionAction;

	            $perm_action->action = $action;

	            $perm_action->save();
	        }
	        else
	        {
	        	$perm_action = $perm_action->first();
	        }

	        if($return === TRUE)
	        {
	        	return $perm_action;
	        }
        }		
	}

    /**
     * 
     * @param 
     * @return 
     */
	public function format($action)
	{
		return ucfirst(strtolower(trim($action)));
	}

    /**
     * 
     * @param 
     * @return 
     */
	public function scopeAction($query, $value)
	{
		return $query->where('action', '=', $this->format($value));
	}
}

?>
