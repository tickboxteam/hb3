<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class Downloads extends Model
{
    public $table = 'media_xref';

    protected $fillable = array('media_id', 'item_id', 'item_type', 'position', 'media_type');

    protected $guarded = array();
    
    public $timestamps = false;

    /**
     * Creates a new instance of the model.
     *
     * @param array $attributes
     */
    public function __construct(array $downloads = [])
    {
        parent::__construct($downloads);
    }

    /**
     * Relationship between OptionAttributes and OptionGroup
     *
     */
    public function media()
    {
        return $this->belongsTo(Media::class, 'media_id', 'id');
    }
}?>