<?php namespace Hummingbird\Models;

use ArrayHelper;
use Auth;
use Config;
use File;
use General;
use Hash;
use Log;
use Mail;
use Request;
use Session;
use Validator;
use View;
use App\Models\User as AppUser;
use Cviebrock\EloquentSluggable\Sluggable;
use Hummingbird\Mail\Users\ResetPasswordNotification;
use Hummingbird\Mail\Users\Admin\ResetPasswordNotification as AdminResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Validation\Rules\Password;

class User extends AppUser {
    const PENDING = 'pending';
    const APPROVED = 'active';
    const INACTIVE = 'inactive';

    use Sluggable, EntrustUserTrait, SoftDeletes {
        EntrustUserTrait::restore insteadof SoftDeletes;
    }

    public $table = 'users';
    public $cms_url = 'users';

    // password confirmation added here for validation. beforeSave() removes this before calling the db
    protected $fillable = array('firstname', 'surname', 'name', 'username', 'email', 'status', 'role_id', 'password', 'password_confirmation', 'last_login', 'force_password_reset', 'remember_token', 'avatar');
        
    public $export_fields = array('forename', 'surname', 'username', 'email', 'status');
    
    public static $permission_type = 'users';
    
    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login' => 'datetime',
        'deleted_at' => 'datetime'
    ];
    
    protected $sluggable = [
        'build_from'      => 'name',
        'save_to'         => 'username',
        'separator'       => '.',
        'include_trashed' => true
    ];

    /**
     * Initialize model events
     */
    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            if( !isset($model->uuid) ):
                $model->uuid = (string) \Illuminate\Support\Str::uuid();
            endif;

            if( isset($User->password_confirmation) ):
                unset($User->password_confirmation);
            endif;

            if( !isset($model->preferences) ):
                $model->preferences = NULL;
            endif;
        });

        static::updating(function($user) {
            if( isset($user->password_confirmation) ):
                unset($user->password_confirmation);
            endif;
        });

        static::saving(function ($model) {
            if( !isset($model->preferences) ):
                $model->preferences = NULL;
            endif;

            if( isset($User->password_confirmation) ):
                unset($User->password_confirmation);
            endif;
        });
    }

    public function objectPermissions() {
        return $this->hasMany(ObjectPermission::class, 'reference_id')
            ->where('reference_type', static::class);
    }

    // need to do here because can't make static variable using function username_regex()
    public static function getRules($action = 'update') {
        switch($action) {
            
            case 'create':
                return array(
                    'name' => 'required',
                    'username' => 'required|unique:users',
                    'email' => 'required|email|unique:users',
                    'password' => ( !Request::is('hummingbird/users/add') ) ? 'required|confirmed' : NULL 
                );
            default:
                return array(
                    'name' => 'required',
                    'email' => 'required|email',
                    'password' => ( Request::is('hummingbird/new-account*') ) ? ['required', Password::defaults(), 'confirmed'] : ['sometimes', Password::defaults(), 'confirmed']
                );                
        }
    }
    
    public static function getStatusAttribute($value)
    {
        return strtolower($value);
    }

    /**
     * Return the correct name for the Membership - in this the user name
     * 
     * @return String
     */
    public function getSystemNameAttribute() {
        if( !empty($this->attributes['name']) )
            return $this->attributes['name'];

        if( !empty($this->attributes['firstname']) && !empty($this->attributes['surname']) )
             return $this->attributes['firstname'] . ' ' . $this->attributes['surname'];

        return 'System';
    }


    /**
     * Return the correct name for the Membership - in this the user name
     * 
     * @return String
     */
    public function getNameAttribute() {
        if( !empty($this->attributes['name']) )
            return $this->attributes['name'];

        return $this->attributes['firstname'] . ' ' . $this->attributes['surname'];
    }
    

    /**
     * Return the users firstname
     * 
     * @return String
     */
    public function getFirstnameAttribute() {
        if( trim($this->attributes['firstname']) != '' ):
            return trim($this->attributes['firstname']);
        endif;

        if( trim($this->attributes['name']) != '' ):
            $SplitName = ArrayHelper::cleanExplodedArray( explode(" ", trim($this->attributes['name'])));

            return $SplitName[0];
        endif;
    }

    /**
     * Accessor to get the user's initials
     * @return String
     */
    public function getInitialsAttribute() {
        $Name = strtoupper( $this->attributes['name'] );
        $NamePart = ArrayHelper::cleanExplodedArray( explode(" ", $Name));

        if( count($NamePart) > 1 ) {
            return $NamePart[0][0] . $NamePart[ count($NamePart)-1 ][0];
        }

        return $Name[0];
    }


    // to match above structure
    public static function getMessages()
    {
        return array(
            'username.bad_usernames' => 'That username is not allowed.',
            'password.bad_passwords' => 'That password is not allowed.',
            'password' => 'Password does not match desired requirements'
        );
    }
    
    public static function get_bad_usernames()
    {
        return config()->get('hb3-hummingbird.bad_usernames') ?: array();
    }
    
    public static function get_bad_passwords()
    {
        return config()->get('hb3-hummingbird.bad_passwords') ?: array();
    }
    
    public static function get_statuses()
    {
        return array('inactive' => 'Inactive', 'active' => 'Active', 'banned' => 'Banned');
    }

    /**
     * Allow the user to return a list of activities
     */
    public function activity() {
        return $this->hasMany(Activitylog::class, 'user_id', 'id');
    }

    public function formatActivity($data)
    {
        if($data !== NULL)
        {
            return $data->action . ' ' . $data->db_table;
        }

        return '-';
    }
    
    // ensure we use the defined rules and messages for validation in ardent
    public function updateSave(
            array $rules = array(),
            array $customMessages = array(),
            array $options = array(),
            \Closure $beforeSave = null,
            \Closure $afterSave = null
        )
    {       

        Validator::extend('bad_usernames', function ($attribute, $value, $parameters){
            return !in_array(strtolower($value), User::get_bad_usernames());
        });
        
        Validator::extend('bad_passwords', function ($attribute, $value, $parameters){
            return !in_array(strtolower($value), User::get_bad_passwords());
        });
        
        $rules = $rules ?: static::getRules();
        $customMessages = $customMessages ?: static::getMessages();

        return parent::save($rules, $customMessages, $options, $beforeSave, $afterSave);
    }
    
    
    public function beforeSave() {
        if($this->isDirty('password')) {
            $this->password = Hash::make($this->password);
        }

        $this->is_admin = ( !empty($this->role_id) ) ? 1:NULL;
        
        unset($this->password_confirmation);

        $this->name = "{$this->firstname} {$this->surname}";


        if( $this->username == '' || ( $this->username != '' && $this->username != $this->getRawOriginal('username') ) ):
            if( $this->username != '' ) {
                if( strpos($this->username, '@') !== FALSE ):
                    $UsernameExp = explode("@", $this->username);
                    $this->username = $UsernameExp[0];
                endif;

                $this->sluggable['build_from'] = 'username';
            }

            $this->resluggify();
        endif;

        return true;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function getEmail()
    {
        return $this->email;
    }

    public static function returnActiveLabel($data)
    {
        $states = array('active' => 'success', 'inactive' => 'warning', 'banned' => 'danger');

        return '<span class="label label-' . $states[$data->status] . '">' . ucfirst($data->status) . '</span>';
    }

    public function isSuperUser()
    {
        return ($this->role && $this->role->isSuperUser());
    }

    public function previewMode()
    {
        return (null !== Session::get('view_user_perms'));
    }

    public function getPreference($item)
    {
        switch($item)
        {
            case 'cover-image':
                if($this->email == 'darylp@tickboxmarketing.co.uk')
                {
                    $image = array(
                        'https://s.yimg.com/uy/build/images/sohp/hero/havasu-falls-at-night2.jpg',
                        'https://s.yimg.com/uy/build/images/sohp/hero/artic-sky2.jpg',
                        'https://s.yimg.com/uy/build/images/sohp/hero/shy-boy2.jpg',
                        'https://s.yimg.com/uy/build/images/sohp/hero/wildsee-pizol2.jpg',

                    );
                    return $image[array_rand($image)];
                }
                break;
        }
    }
    
    /**
     * Return the profile image for this user
     * @return String
     */
    public function get_profile_image() {
        return View::make('HummingbirdBase::public.users.profile_image')->with('User', $this)->render();
    }


    public function scopeNotAdmin($query) {
        return $query->whereNull('is_admin')->whereNull('role_id');   
    }

    public function scopeAdmin($query) {
        
        return $query->whereNotNull('is_admin')
            ->whereNotNull('role_id');
        
    }
    
    public function scopeCmsSearch($query, $term) {
        if(!empty($term)) {
            $query->where(function($query) use ($term) {
                $query->where('firstname', 'like', "%$term%")
                    ->orWhere('surname', 'like', "%$term%")
                    ->orWhere('email', 'like', "%$term%");
            });
        }

        return $query;
    }


    /**
     * We need a way of setting the standard web user
     * @return Void
     */
    public function standardUser()
    {
        $this->attributes['role_id'] = $this->attributes['is_admin'] = NULL;
    }


    public function isActive()
    {
        return (null === $this->deleted_at && $this->status == self::APPROVED);
    }


    public function userAbility($permission) {
        if($this->isSuperUser()) return true; // always give superadmin access

        $permission = trim($permission);

        // Any permission that is set will do.
        if (substr(rtrim($permission), -1) == "*") {
            foreach ($this->permissions as $value) {
                if (false !== stripos($value, rtrim($permission, "*"))) {
                    return true;
                }
            }
        }

        // Checks whether the user has the ability in overrides.
        foreach ($this->permissions as $perm) {
            if ($perm->name == $permission) {
                return true;
            }
        }

        return false;
    }

    public function hasPermissionAction($permission) { 
        if(!$this->isSuperUser()) {
            if(is_object($permission)) {
                return in_array($permission->name, $this->permissions->pluck('name')->all());
            }

            return in_array(trim($permission), $this->permissions->pluck('name')->all());
        }

        return true;
    }

    public function isAdmin() {
        return ($this->is_admin == 1);
    }


    /**
     * Many-to-Many relations with Permission named perms as permissions is already taken.
     *
     * @return void
     */
    public function permissions(){
        return $this->belongsToMany(config()->get('entrust.permission'), 'permissions_users');
    }

    /**
     * Detach permission form current role.
     *
     * @param object|array $permission
     *
     * @return void
     */
    public function detachPermission($permission)
    {
        if (is_object($permission))
            $permission = $permission->getKey();

        if (is_array($permission))
            $permission = $permission['id'];

        $this->permissions()->detach( $permission );
    }

    public function role() {
        return $this->belongsTo(Role::class);
    }

    public function hasRole($name)
    {
        if ($this->role->name == $name) {
            return true;
        }

        return $this->userHasRole($name);
    }

    /**
     * Clear all of the permissions for this user
     */
    public function resetPermissions() {
        $this->permissions()->sync( [] );
    }




    public function scopeExceptMe($query) {
        return $query->whereNotIn('id', [Auth::user()->id]);
    }

    public function scopeHidden($query, $hidden = 0) {
        return $query->where('hidden', $hidden);
    }

    public function scopeByRoles($query, $roles = array()) {
        if(!empty($roles)) {
            return $query->whereIn('role_id', $roles);
        }
    }

    /**
     * Filter users by their status
     */
    public function scopeStatus($query, $status = NULL) {
        if( !empty($status) && in_array($status, ['active', 'inactive', 'banned']) ) {
            $query->where('status', $status);
        }

        return $query;
    }

    public function lastLoginDate() {
        if(null !== $this->last_login) {
            return $this->last_login->diffForHumans();
        }

        return '-';
    }

    /**
     * Reinstate this user (if soft deleted)
     */
    public function reinstate() {
        if( !is_null($this->deleted_at) ) {
            $this->restore();
        }

        return $this;
    }

    /**
     * Check whether the user has uploaded an avatar (and if it exists)
     * @return boolean
     */
    public function hasAvatar() {
        return ( $this->attributes['avatar'] != '' && File::exists( base_path() . $this->attributes['avatar'] ) );
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = ( $value != '' && Hash::needsRehash($value) ) ? Hash::make( $value ) : $value;
    }

    /**
     * Get all meta data attributes for this user
     */
    public function meta() {
        return $this->morphMany(MetaData::class, 'item', 'item_type');
    }

    /**
     * Does the user have this meta option
     */
    public function hasMetaOption($key) {
        $meta = $this->load('meta')->meta;

        foreach ($meta as $item):
            if( $item->key == $key ) return true;
        endforeach;

        return false;
    }

    /**
     * Get the user have this meta option
     */
    public function getMetaOption($key) {
        if( $this->hasMetaOption( $key) ):
            foreach ($this->meta as $item):
                if( $item->key == $key ) return $item->value;
            endforeach;
        endif;

        return false;
    }

    /**
     * Add a new meta option
     */
    public function addMetaOption($key, $value) {
        if( trim($key) == '' ) {
            throw new \Exception('Invalid key provided for meta data');
        }

        if( $this->hasMetaOption($key) ):
            return $this->updateMetaOption( $key, $value );
        endif;

        $this->meta()->create([
            'key' => trim($key),
            'value' => trim($value)
        ]);
    }

    /**
     * Update the current meta option. If we have a meta option, 
     * then update it, rather than create a new one.
     */
    public function updateMetaOption($key, $value) {
        if( trim($key) == '' ) {
            throw new \Exception('Invalid key provided for meta data');
        }

        foreach ($this->meta()->get() as $item):
            if( $item->key == $key ) {
                $item->value = $value;
                $item->save();
            }
        endforeach;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $Mailer = ( Request::is('hummingbird/*') ) ? new AdminResetPasswordNotification($this, $token) : new ResetPasswordNotification($this, $token);

        Mail::to($this)->send( $Mailer );
    }

    /**
     * Archive this user
     * @return User
     */
    public function archive() {
        $this->attributes['status'] = self::ARCHIVED;
        $this->remember_token = NULL;
        $this->save();

        return $this;
    }

    /**
     * Does this user have 2FA enabled?
     * @return Boolean
     */
    public function has2FAEnabled(): bool {
        return ( $this->two_factor_secret && $this->two_factor_confirmed_at );
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array {
        return [
            'username' => [
                'source'    => 'name',
                'includeTrashed' => true,
                'onUpdate'  => false
            ]
        ];
    }

    /**
     * Toggle the user status
     * 
     * @return Hummingbird\Models\User
     */
    public function toggleStatus():User {
        switch( $this->status )
        {
            case self::APPROVED:
                $this->status = self::INACTIVE;
                break;
            case self::INACTIVE:
                $this->status = self::APPROVED;
                break;
            default:
                $this->status = self::INACTIVE;
                break;
        }

        $this->save();

        return $this;
    }
}