<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Redirect stat model to help store, process and look after data
 * related to all website redirects
 *
 * @since 1.1.31
 * @version 1
 */
class RedirectStat extends Model {
    public $table       = 'redirections_stats';
    protected $fillable = ['visited_at', 'referrer'];
    public $timestamps  = false;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'visited_at' => 'datetime',
    ];

    /**
     * Belongs to a Redirect
     */
    public function redirect() {
        $this->belongsTo(Redirections::class, 'id', 'redirect_id');
    }
}
