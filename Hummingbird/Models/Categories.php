<?php namespace Hummingbird\Models;

use DB;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hummingbird\Models\Tags;
use Hummingbird\Traits\ObjectPermissionTrait;
use Hummingbird\Traits\TaxonomyHelper;

class Categories extends Model {
    use Sluggable, SoftDeletes, TaxonomyHelper, ObjectPermissionTrait;
    
    public $table       = 'taxonomy';
    protected $fillable = array('name', 'slug', 'display_name', 'parent', 'description', 'SortOrder');
    protected $guarded  = array();
    protected $type     = 'category';

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'deleted_at' => 'datetime',
    ];


    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    public static function boot() {
        parent::boot();

        static::creating(function($Taxonomy) {
            $Taxonomy->setAttribute('type', $Taxonomy->getType() );
            $Taxonomy->slug = ( !empty($Taxonomy->slug) ) ? str_slug($Taxonomy->slug) : str_slug($Taxonomy->name);

            if( !isset($Taxonomy->image) || $Taxonomy->image == '' ):
                $Taxonomy->image = NULL;
            endif;

            if( !isset($Taxonomy->description) || $Taxonomy->description == '' ):
                $Taxonomy->description = NULL;
            endif;
        });

        static::saving(function($Taxonomy) {
            $Taxonomy->setAttribute('type', $Taxonomy->getType() );

            if( !isset($Taxonomy->image) || $Taxonomy->image == '' ):
                $Taxonomy->image = NULL;
            endif;

            if( !isset($Taxonomy->description) || $Taxonomy->description == '' ):
                $Taxonomy->description = NULL;
            endif;
        });

        Categories::registerModelEvent('slugging', function($model) {
            if( isset($model->force_clean_url) ) {
                unset($model->force_clean_url);
            }

            if( !isset($model->force_clean_url) ) {
                // before we save, make sure we have a title
                if( empty($model->name) ) {
                    $model->name = $model->getRawOriginal('name');
                }
            }
        });

        static::saved(function($Taxonomy) {
            if( empty($Taxonomy->type) ) {
                $Taxonomy->type = $Taxonomy->getType();
            }
        });
        
        static::restoring(function($model) {
            $model->parent = ( $model->parentCategory ) ? $model->parent : NULL;
        });

        static::deleted(function($Taxonomy) {
            if( $Taxonomy->children->count() ) {
                foreach( $Taxonomy->children()->get() as $ChildCategory ) {
                    $ChildCategory->parent = ( !empty($Taxonomy->parent) ) ? $Taxonomy->parent : NULL;
                    $ChildCategory->save();
                }
            }

            // On purge, delete all relationships
            if ($Taxonomy->forceDeleting) {
                $Taxonomy->truncate_relationships();
            }
        });
    }

    /**
     * Get the parent category
     * 
     */
    public function parentCategory()
    {
        return $this->belongsTo(Categories::class, 'parent');
    }


    /**
     * Get all the children of parent category
     * 
     */
    public function children($namespace = NULL)
    {
        if( null !== $namespace && class_exists($namespace))
            return $this->hasMany($namespace, 'parent');

        return $this->hasMany(Categories::class, 'parent');
    }


    /**
     * Retreive relationships for any items attached to this category
     *
     * @param  string  $type
     * Get all the children of parent category
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @return Void
     */
    public function items($type = NULL) {
        if(null !== $type || class_exists($type))
            return $this->belongsToMany($type, 'taxonomy_relationships', 'term_id', 'item_id');

        return;
    }

    

    /**
     *
     *
     * @return 
     */
    public function setParentAttribute($value)
    {
        $this->attributes['parent'] = ($value == '' OR $value < 1) ? NULL:$value;
    }

    /**
     * Return elements in their indexed position
     *
     * @param Eloquent $query
     * @param String $order [Default is ASC]
     */
    public function scopeOrdered($query, $order = 'ASC')
    {
        return $query->orderBy('SortOrder', $order);
    }

    /**
     * Scope the parent categories
     *
     * @param Eloquent $query
     * @param Integer|NULL $parent_id
     */
    public function scopeParent($query, $parent_id = NULL)
    {
        return $query->where('parent', $parent_id);
    }

    public function hasChildren()
    {
        return (Categories::objectPermissions()->where('parent', $this->id)->whereNull('deleted_at')->count() > 0) ? true:false;
    }

    public function exists($name)
    {
        return (Categories::objectPermissions()->whereNull("deleted_at")->type()->where('name', $name)->count() > 0);
    }

    public static function deletedCategories($return_count = false)
    {
        $categories = Categories::objectPermissions()->onlyTrashed()->orderBy('name')->type();

        return ($return_count) ? $categories->count() : $categories->get();
    }

    public function usedByBreakdown()
    {
        return DB::table('taxonomy_relationships')
                    ->where('term_id', '=', $this->id)
                    ->groupBy('tax_type')
                    ->orderBy('results', 'DESC')
                    ->having('results', '>', 0)
                    ->select(DB::raw('count(term_id) AS results, tax_type'))
                    ->get(); 
    }

    public function scopeSearch($query, $value)
    {
        return $query;
        return $query->whereRaw('MATCH (name, slug, description) AGAINST (?)', array($value));
    }

    public function assignImage()
    {
        return ($this->attributes['name'] == 'Events') ? true:false;
    }


    /**
     * Query scope for getting elements where they belong to a parent
     *
     * @param 
     * @param String $value
     * @return 
     */
    public function scopeChildOf($query, $value)
    {
        return $query->where('parent', $value);
    }


    public function scopeOnly($query, $show_categories = [])
    {
        if ( empty($show_categories) )
            return $query;

        return $query->whereIn('id', $show_categories);
    }


    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array {
        $source = 'name';
        $onUpdate = true;

        if( isset($this->force_clean_url) ) {
            if( !empty($this->getRawOriginal('slug') ) ) {
                $source = 'slug';
            }
        }

        if( !isset($this->force_clean_url) ) {
            // before we save, make sure we have a name
            if( empty($this->name) ) {
                $this->name = $this->getRawOriginal('name');
            }

            // Only re-sluggify if we've changed or emptied the URL
            if( !is_null($this->getRawOriginal('slug')) && $this->getRawOriginal('slug') != $this->attributes['slug'] ) {
                $source = 'name';

                // We have changed the URL and it's not empty
                if( !empty($this->attributes['slug']) ) {
                    $source = 'slug';
                }
            }
        }

        if( isset($this->attributes['slug']) && $this->getRawOriginal('slug') == $this->attributes['slug'] ) {
            $onUpdate = false;
        }

        return [
            'slug' => [
                'source'    => $source,
                'includeTrashed' => true,
                'onUpdate'  => $onUpdate
            ]
        ];
    }
}
