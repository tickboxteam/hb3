<?php namespace Hummingbird\Models;

use Auth;
use Session;
use Validator;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {
    public $table = 'roles';
    public $cms_url = 'roles';
    public $preview_role = false;
    public static $permission_type = 'roles';
    protected $fillable = array('name', 'parentrole_id');
    
    /**
     * Boot the role model
     * Attach event listener to remove the many-to-many records when trying to delete
     * Will NOT delete any records if the role model uses soft deletes.
     *
     * @return void|bool
     */
    public static function boot()
    {
        static::bootTraits();

        static::deleting(function($role) {
            // Remove user permissions if they have that role
            foreach( $role->users as $UserWithRole ):
                $UserWithRole->permissions()->sync([]);
            endforeach;

            // Detach all permissions for this role
            $role->perms()->sync([]);
        });
    }


    public function objectPermissions() {
        return $this->hasMany(ObjectPermission::class, 'reference_id')
            ->where('reference_type', static::class);
    }

    public function parentrole()
    {
        return $this->belongsTo(Role::class);
    }

    public static function get_selection($roles) {
        $selection = array();

        foreach ($roles as $role) {
            $selection[$role->id] = str_repeat('&nbsp;', $role->level) . $role->name;
        }

        return $selection;
    }

    public static function setPreviewRole($val = false) {
        $this->preview_role = $val;
    }

    public function delete(array $options = []) {
        // don't delete superadmin!
        if ($this->name == 'superadmin' OR $this->id == 1 OR $this->locked) {
            return false;
        }
        
        parent::delete();
    }

    public static function getUserRoleID()
    {
        return Auth::user()->role->id;
    }

    public static function getRoleParentID()
    {
        return Auth::user()->role->parentrole_id;
    }

    public static function getUserRoleName()
    {
        return Auth::user()->role->name;
    }

    public static function showPreviewLink($role_id)
    {
        if(Role::getUserRoleID() != $role_id) return true;
    }

    public function parent()
    {
        return $this->belongsTo(Role::class, 'parentrole_id');
    }

    public function children()
    {
        return $this->hasMany(Role::class, 'parentrole_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'role_id');
    }

    public function previewMode()
    {
        return (null !== Session::get('view_role_perms'));
    }

    public function roleAbility($permission) {
        // Checks whether the user has the ability in overrides.
        foreach ($this->perms as $perm) {
            if ($perm->id == $permission->id) {
                return true;
            }
        }
    }

    public function hasPermissionAction($permission) {
        if(!$this->isSuperUser()) {
            if(is_object($permission)) 
                return in_array($permission->name, $this->perms->pluck('name')->all());

            return in_array(trim($permission), $this->perms->pluck('name')->all());
        }

        return true;
    }

    /**
     * Detach permission form current role.
     *
     * @param object|array $permission
     *
     * @return void
     */
    public function detachPermission($permission = NULL)
    {
        if(is_null($permission)) {
            return;
        }

        parent::detachPermission($permission);
    }



    public function isSuperUser() {
        return ($this->id == 1);
    }
}
