<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class MigratedMediaItem extends Eloquent {

    public $table       = 'media_xref_migration';
    protected $fillable = array('source', 'destination');
    public $timestamps  = false;


    public function media() {
        return $this->hasOne(Media::class, 'location', 'destination');
    }


    /**
     * Find by source
     * @param  QueryBuilder $query
     * @param  String $filename
     * @return QueryBuilder
     */
    public function scopebySource($query, $filename) {
        return $query->where('source', $filename);
    }
}
