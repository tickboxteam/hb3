<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class Moduletemplate extends Model {
    public $table = 'moduletemplates';
    public $cms_url = 'module-templates';

    protected $fillable = array('name', 'html', 'css', 'js', 'notes', 'type');
    
    protected $guarded = array();


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        // saving is fired on both creating and updating
        Moduletemplate::deleting(function($Moduletemplate) {
            if( $Moduletemplate->modules()->count() > 0 ) {
                $Moduletemplate->modules()->forceDelete();
            }
        });
    }

    /**
     * Each template can have many modules associated.
     *
     */
    public function modules() {
        return $this->hasMany(Module::class, 'moduletemplate_id', 'id');
    }

    /**
     * Setting the value of type to be NULL if empty
     * @param Mixed $value
     */
    public function setTypeAttribute($value) {
        $newValue = trim($value);

        $this->attributes['type'] = ( $value != '' ) ? $value : NULL;
    }




    /**
     * Search the table for anything close to a given term
     * 
     * @param  QueryBuilder $quer
     * @param  String|NULL $term
     * 
     * @return Query
     */
    public function scopeSearch($query, $term = NULL)
    {
        if(!empty($term)) {
            $query->where('name', 'LIKE', "%$term%");
        }

        return $query;
    }

    public function scopeMicrosite($query)
    {
        if(class_exists('Tickbox\Microsites\Microsites'))
        {
            if(Session::has('microsite-builder'))
            {
                return $query->where(function($query)
                    {
                        $query->where('microsite_id', Session::get('microsite-builder'))->orWhere('microsite_id', NULL);
                    });
            }

            return $query->where('microsite_id', NULL);
        }
    }

    /**
     * Get all module template files and order by name
     * @param   string $emptyitem_text
     * @return  Array
     * @version 2
     */
    public static function get_selection($emptyitem_text = 'Select a template') {
        $selection = array();
        
        foreach(Moduletemplate::microsite()->orderBy('name')->get() as $moduletemplate) {
            $selection[$moduletemplate->id] = $moduletemplate->name;
        }

        return ( count($selection) > 0 ) ? $selection : [];
    }
    
    public function module()
    {
        return $this->belongsTo('Module');
    }


        
    public function data()
    {
        $data = array();

        $templates = Moduletemplate::all();

        foreach($templates as $template)
        {
            $data['css'][] = $template->css;
            $data['js'][] = $template->js;
        }

        return $data;
    }

    public function getNotes($default = '-')
    {
        return ($this->notes != '') ? $this->notes:$default;
    }

    public function getText()
    {
        return 'Lorem ipsum nulla potenti suspendisse nisl massa venenatis aliquam, ad condimentum senectus tempus risus dictum etiam tempor fames, ipsum quam condimentum platea pulvinar aenean dapibus.';
    }
}
