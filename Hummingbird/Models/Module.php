<?php namespace Hummingbird\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Module extends Model {
    public $table = 'modules';
    public $cms_url = 'modules';
    protected $fillable = array('name', 'moduletemplate_id', 'notes');

    protected $shortcode = '[HB::MODULE(id)]';
    
    protected $guarded = array();


    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    public static function boot() {
        parent::boot();

        static::deleting(function($model) {
            $model->storeTaxonomy([]);
        });
    }

    public function shortcode()
    {
        return str_replace("id", $this->attributes['id'], $this->shortcode);
    }

    public function moduletemplate()
    {
        return $this->belongsTo(Moduletemplate::class);
    }

    /**
     * Modules can belong to many categories and vice versa
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Categories::class, 'taxonomy_relationships', 'item_id', 'term_id')
            ->where('type', 'category')
            ->where('tax_type', get_class($this));
    }

    /**
     * Modules can belong can have many tags and vice versa
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tags::class, 'taxonomy_relationships', 'item_id', 'term_id')
            ->where('type', 'tag')
            ->where('tax_type', get_class($this));
    }

    /**
     * Setting the note attribute
     * @param String $value
     */
    public function setNotesAttribute($value) {
        $value = trim($value);
        $this->attributes['notes'] = ( $value != '' ) ? $value : NULL;
    }

    /**
     * Search the table for anything close to a given term
     * 
     * @param  QueryBuilder $quer
     * @param  String|NULL $term
     * 
     * @return Query
     */
    public function scopeSearch($query, $term = NULL)
    {
        if(!empty($term)) {
            $query->where('name', 'LIKE', "%$term%");
        }

        return $query;
    }

    public function scopeMicrosite($query)
    {
        if(class_exists('Tickbox\Microsites\Microsites'))
        {
            if(Session::has('microsite-builder'))
            {
                return $query->where(function($query)
                    {
                        $query->where('microsite_id', Session::get('microsite-builder'))->orWhere('microsite_id', NULL);
                    });
            }

            return $query->where('microsite_id', NULL);
        }
    }
    
    public static function get_selection($emptyitem_text = 'Select a module')
    {
        $selection = array(0 => $emptyitem_text);
        
        $modules = Module::all();
        
        foreach($modules as $module) {
            $selection[$module->id] = $module->name;
        }
        
        return $selection;
    }

    public function setModuleTemplateIdAttribute($value)
    {
        $this->attributes['moduletemplate_id'] = ($value == '' OR !is_numeric($value) OR $value <= 0) ? NULL:$value;
    }

    public function getModuleDataAttribute($value)
    {
        if(is_array($value)) return;
        
        if($value != '') return unserialize($value);

        return;
    }

    public function getTemplateName()
    {
        if($this->template())
        {
            return $this->moduletemplate->name;
        }

        return 'No template';
    }

    public function storeTaxonomy($tags)
    {
        $class = get_class($this);

        DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $this->id)->delete();
        
        if ($tags)
        {
            $input = array();
            foreach($tags as $tag)
            {
                $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $this->id);
            }

            DB::table('taxonomy_relationships')->insert($input);
        }
    }
    
    public function taxonomy()
    {
        $data = array('category' => array(), 'tag' => array());
        $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->get();

        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }

    public function template()
    {
        if(null !== $this->moduletemplate_id) return true;
        return;
    }
    
    public function render() {
        return $this->moduletemplate->html;
    }
}
