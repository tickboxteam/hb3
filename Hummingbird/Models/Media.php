<?php namespace Hummingbird\Models;

use DB;
use Event;
use File;
use Hummingbird\Libraries\FileHelper;
use Illuminate\Database\Eloquent\Model;

class Media extends Model {

    public $table = 'media-items';
    public $fillable = array('title', 'filename', 'location', 'caption', 'alt', 'description', 'parent', 'collection');
    
    protected $sizes = array(
        'file-list'
    );


    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see     \Illuminate\Database\Eloquent\Model::boot()
     * @return  void
     * @since   1.1.37
     */
    public static function boot() {
        parent::boot();

        static::deleting(function($model) {
            foreach($model->children as $ChildItem) {
                if( File::exists( storage_path('app/public') . $ChildItem->location ) ) {
                    Event::dispatch('Tickbox.Media.AfterDelete', ['MediaItemFilename' => $ChildItem->location]);

                    File::delete( storage_path('app/public') . $ChildItem->location );
                }

                $ChildItem->delete();
            }

            if( File::exists( storage_path('app/public') . $model->location ) ) {
                Event::dispatch('Tickbox.Media.AfterDelete', ['MediaItemFilename' => $model->location]);

                File::delete( storage_path('app/public') . $model->location );
            }
        });
    }


    /**
     * Checking the value we should store for alt tags
     */
    public function setAltAttribute( $value ) {
        $value = trim( $value );
        $this->attributes['alt'] = ( $value != '' ) ? $value : NULL;
    }

    /**
     * Checking the value we should store for caption
     */
    public function setCaptionAttribute( $value ) {
        $value = trim( $value );
        $this->attributes['caption'] = ( $value != '' ) ? $value : NULL;
    }

    /**
     * Checking the value we should store for collection
     */
    public function setCollectionAttribute($value) {
        $this->attributes['collection'] = ($value == '' OR !is_numeric($value)) ? NULL:$value;
    }

    /**
     * Checking the value we should store for description
     */
    public function setDescriptionAttribute( $value ) {
        $value = trim( $value );
        $this->attributes['description'] = ( $value != '' ) ? $value : NULL;
    }

    public function getFileLocations($items)
    {
        $data = array();

        foreach($items as $item)
        {
            $data[] = base_path() . $item->location;
        }

        return $data;
    }

    /**
     * Get all children associated to this resource
     *
     * @since 1.1.37
     */
    public function children() {
        return $this->hasMany(Media::class, 'parent', 'id');
    }

    public function scopeLocked($query, $locked = NULL)
    {
        return $query->where('locked', '=', $locked);
    }

    /**
     * Search the table for anything close to a given term
     * 
     * @param  QueryBuilder $quer
     * @param  String|NULL $term
     * 
     * @return Query
     */
    public function scopeSearch($query, $term = NULL)
    {
        if(!empty($term)) {
            $query->where(function($query) use($term) {
                $query->where('title', 'LIKE', "%$term%")
                    ->orWhere('filename', 'LIKE', "%$term%")
                    ->orWhere('location', 'LIKE', "%$term%")
                    ->orWhere('caption', 'LIKE', "%$term%")
                    ->orWhere('alt', 'LIKE', "%$term%")
                    ->orWhere('description', 'LIKE', "%$term%");
            });
        }

        return $query;
    }


    public function scopeType($query, $type = '')
    {
        if($type != '')
        {
            return $query->where('type', '=', $type);//return images only
        }
    }

    public function scopeInCollection($query, $id)
    {
        if(!$id) return $query->whereNull('collection');

        return $query->where('collection', '=', $id);
    }

    public function scopeParent($query, $has_parent = true)
    {
        if($has_parent)
        {
            return $query->whereNull('parent');
        }
    }

    public function collection()
    {
        $collection = DB::table("media-collections")->where('id', '=', $this->collection)->first();

        if(count($collection) < 1)
        {
            return (object) array('name' => 'Root media library');
        }

        return $collection;
    }

    public function collectionLink()
    {
        if(null !== $this->collection)
        {
            return 'view/'.$this->collection;
        }
    }


    public function storeTaxonomy($tags)
    {
        DB::table('taxonomy_relationships')->where('tax_type', '=', self::class)->where('item_id', '=', $this->id)->delete();

        $input = array();

        if($tags) {
            foreach($tags as $tag) {
                $input[] = array('term_id' => $tag, 'tax_type' => self::class, 'item_id' => $this->id);
            }

            DB::table('taxonomy_relationships')->insert($input);
        }
    }

    public function taxonomy()
    {
        $data = array('category' => array(), 'tag' => array());
        $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->get();

        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }

    public function replicateTaxonomy()
    {
        $tax_items = DB::table('taxonomy_relationships')->where('tax_type', '=', get_class($this))->where('item_id', '=', $this->parent)->get();

        $new_taxonomy = array();

        if(count($tax_items) > 0)
        {
            foreach($tax_items as $item)
            {
                $new_taxonomy[] = array('term_id' => $item->term_id, 'tax_type' => get_class($this), 'item_id' => $this->id);
            }

            if(count($new_taxonomy) > 0) DB::table('taxonomy_relationships')->insert($new_taxonomy);
        }
    }

    public function mediaCentreThumb()
    {
        $image = Media::where('parent', '=', $this->id)->where('mc_only', '=', '1')->first();

        if(null === $image) return $this->location;

        return $image->location;
    }

    public function getParentId()
    {
        if($this->parent === NULL) return $this->id;

        return Media::find($this->parent)->id;
    }


    public function isType($type, FileHelper $FileHelper, $items)
    {
        if($type !== false)
        {
            $data = array();
            $extensions = $FileHelper->getExtensions($type);

            foreach($items as $item)
            {
                if($FileHelper->validateExtension($item->location, $extensions, false))
                {
                    $data[] = $item;
                }
            }

            return $data;
        }

        return;
    }
    
    public function download() 
    {
        return $this->hasMany('Downloads', 'id', 'media_id');
    }
}