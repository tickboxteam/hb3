<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class ObjectPermission extends Model 
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table    = 'object_permissions';
    public $fillable    = ['item_id', 'item_type', 'reference_id', 'reference_type'];
    public $timestamps  = false;

    public function scopeType($query, $Type = NULL) {
        if(!is_null($Type)) {
            $query->where('item_type', $Type);
        }

        return $query;
    }
}
