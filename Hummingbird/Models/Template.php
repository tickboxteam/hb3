<?php namespace Hummingbird\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model {
    use SoftDeletes;

    public $table = 'templates';
    public $cms_url = '';
    protected $fillable = array('name', 'description', 'html', 'locked');
    
    protected $guarded = array();

    public static $rules = array(
        'name' => 'required|min:2'
    );

    /**
     * Setting the HTML value (cleaning)
     * @param String $value
     */
    public function setHtmlAttribute($value) {
        $value = trim($value);
        $this->attributes['html'] = ( $value != '' ) ? $value : NULL;
    }

    /**
     * Setting the Description value (cleaning)
     * @param String $value
     */
    public function setDescriptionAttribute($value) {
        $value = trim($value);
        $this->attributes['description'] = ( $value != '' ) ? $value : NULL;
    }

    public function scopeMicrosite($query)
    {
        if(class_exists('Tickbox\Microsites\Microsites') && Session::has('microsite-builder'))
        {
            return $query->where(function($query){
                $query->where('microsite_id', Session::get('microsite-builder'))->orWhere('microsite_id', NULL);
            });

            return $query->whereNull('microsite_id');
        }
    }

    public static function get_selection($add_new = true) {
        return Template::microsite()->orderBy('name', 'ASC')->pluck('name', 'id')->all();
    }
    
    public static function get_types()
    {
        return array ('page' => 'Page');
    }
    
    public static function get_statuses()
    {
        return array (0 => 'Inactive', 1 => 'Active');
    }
    
    private function get_relational_data($path = null)
    {            
        $path = ($path != null) ? $path : __DIR__ . '/../views/templates/emails/';
        $templates = scandir($path);
        $all_templates = array();

        foreach ($templates as $template) {

            if ($template == '.' || $template == '..')
                continue;
            
            $view = str_replace(".blade.php", "", $template);            
            $all_templates[$view] = $view;
        }
        
        $this->data['views'] = $all_templates;    
    }
    
    public static function get_layouts($path = null)
    {
        $available_layouts = array();
        
        $path = ($path != null) ? $path : (__DIR__ . '/../../themes/public/'.config()->get('hb3-hummingbird.activeTheme').'/templates/');
        $layouts = scandir($path);
        foreach ($layouts as $layout) {

            if ($layout == '.' || $layout == '..')
                continue;
            
            $view = str_replace(".blade.php", "", $layout);            
            $available_layouts[strtolower($view)] = ucfirst($view);
        }
        
        return $available_layouts;
    }
    
    public function get_module_areas()
    {
        $html = $this->html;
        echo $html;
        
        // regex on {{$modules[area name]}}
        return array('AREA-ONE', 'AREA-TWO');
    }
    
    /*
     * html to be rendered for a page on the frontend
     */
    public function getHtmlFromStructure()
    {
        // coming from frontend, so won't have path
        View::addLocation(app_path() . "/hummingbird/pages/views");
        
        $data['structure'] = json_decode($this->structure);
        $data['elements'] = array();
        
        return View::make('HummingbirdBase::cms.template-structure', $data);
    }

    public function scopeFind($query, $value)
    {
        return $query->where('id', '=', $value);
    }

    /**
     * Finding out whether a template is locked
     */
    public function scopeLocked($query)
    {
        if( !Auth::user()->isSuperUser() ) {
            $query->whereNull('locked');
        }

        return $query;
    }
}
