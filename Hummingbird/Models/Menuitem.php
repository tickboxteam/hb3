<?php namespace Hummingbird\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Menuitem extends Model {
    public $table       = 'menuitems';
    protected $fillable = array('menu_id', 'menu_item_name', 'parent', 'order', 'data', 'url', 'title', 'classes', 'menu_item_id', 'description', 'new_window');

    protected $guarded  = array();
    

    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    public static function boot() {
        parent::boot();
        
        static::saving(function($Menuitem) {
            if( $Menuitem->parent == 0 ) {
                $Menuitem->parent = NULL;
            }
        });
    }

    public function children() {
        return $this->hasMany(Menuitem::class, 'parent', 'id');
    }

    public function menu() {
        return $this->belongsTo(Menu::class);
    }   

    public static function childPages($menuitem_url = NULL) {
    	$menuitem_url = str_replace('/', '', $menuitem_url);

    	$parent_page = DB::table('pages')->whereNull('deleted_at')->where('status', '=', 'public')->where('url', '=', $menuitem_url)->first();

    	if(null !== $parent_page)
        {
    		$children = DB::table('pages')->where('status', '=', 'public')->where('parentpage_id', '=', $parent_page->id)->orderby('position', 'ASC')->get();
    		return $children;
    	}       
    }

    /**
     * Here we are going to check the link for being external
     * If it is, just trim it.
     * If not, clean and sluggify
     */
    public function setUrlAttribute($menu_url) {
        $this->attributes['url'] = clean_and_sluggify_url( $menu_url );
    }

    /**
     * Set the classes attributes
     */
    public function setClassesAttribute($value) {
        $value = trim($value);

        $this->attributes['classes'] = ( $value != '' ) ? $value : NULL;
    }

    /**
     * Do we have any classes for this menuitem
     * @return boolean
     */
    public function hasClasses() {
        return !empty($this->classes);
    }
}