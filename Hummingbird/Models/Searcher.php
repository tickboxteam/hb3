<?php namespace Hummingbird\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Searcher extends Model
{
    public $table       = 'search';
    protected $fillable = array('item_id', 'title', 'url', 'summary', 'content', 'type', 'weight');

    public static function boot() {
        parent::boot();
        
        static::saving(function ($model) {
            $model->url = (substr($model->url, 0, 1) !== '/') ? "/{$model->url}": $model->url;
        });
    }

    /**
     * Retreive the item that is related
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo($this->type, 'item_id', 'id');
    }



    /**
     * Remove any HTML tags from the data we are saving.
     *
     * @param String $value
     */  
    public function setSummaryAttribute($value)
    {
        $this->attributes['summary'] = clean_html_and_shortcodes_from_string($value);
    }

    /**
     * Remove any HTML tags from the data we are saving.
     *
     * @param String $value
     */  
    public function setContentAttribute($value)
    {
        $this->attributes['content'] = clean_html_and_shortcodes_from_string($value);
    }


    /**
     * Check the URL is stored correctly and return it in the correct format
     *
     * @param String $value
     * @return String
     */  
    public function setUrlAttribute($value) {
        $Permalink = parse_url( $value );

        if( isset($Permalink['path']) ) {
            $this->attributes['url'] = $Permalink['path'];
        }

        $this->attributes['url'] = ( isset($Permalink['path']) ) ? $Permalink['path'] : $Permalink;
    }


    /**
     * Check the URL is stored correctly and return it in the correct format
     *
     * @param String $value
     * @return String
     */  
    public function getUrlAttribute() {
        $Permalink = $this->attributes['url'];

        if (substr($Permalink, 0, 1) !== '/') {
            $Permalink = "/{$Permalink}";
        }

        return $Permalink;
    }

    /**
     * Scope searching the table with weights added accordingly
     * 
     * UPDATED-20190906: Bindings cause some failures with correct orders and values, so we've escaped
     * the values by using DB rather than using bindings until we fully get to the bottom of why bindings
     * cause the pagination issues. We also removed the bindings because they were not calculating the
     * results properly. :searchterm_ bindings were not being executed, resulting in 0 calculations from 
     * Eloquent. Yet MySQL and Raw DB queries worked perfectly fine.
     *
     * @param Search
     */  
    public function scopeSearch($query, $search_term) {
        // return $query;
        $table_prefix = DB::getTablePrefix();
        $SearchTable = $this->getTable();
        $table = DB::getTablePrefix() . $this->getTable();

        $Cleanedterm = trim(DB::connection()->getPdo()->quote($search_term), "'");

        return $query->select(
            DB::raw("$table.*, 
                (
                    $table.weight +
                    (
                        1 * (
                                MATCH(title) AGAINST(".DB::connection()->getPdo()->quote($search_term).")
                            )
                    )
                    +
                    (
                        1 * (
                                MATCH(url) AGAINST(".DB::connection()->getPdo()->quote($search_term).")
                            )
                    )
                    +
                    (
                        1 * (
                                MATCH(summary) AGAINST(".DB::connection()->getPdo()->quote($search_term).")
                            )
                    )
                    +
                    (
                        5 * (
                                MATCH(content) AGAINST(".DB::connection()->getPdo()->quote($search_term).")
                            )
                    )
                ) AS score,
                +
                    CASE 
                        WHEN DATE_ADD(updated_at, INTERVAL 12 month) < NOW() THEN 0
                        WHEN DATE_ADD(updated_at, INTERVAL 6 month) < NOW() THEN 1
                        WHEN DATE_ADD(updated_at, INTERVAL 1 month) > NOW() THEN 10
                        WHEN DATE_ADD(updated_at, INTERVAL 3 month) > NOW() THEN 5
                        WHEN DATE_ADD(updated_at, INTERVAL 6 month) > NOW() THEN 2.5
                    END
                AS score_freshness,
                +
                    CASE 
                        WHEN title = " . DB::connection()->getPdo()->quote($Cleanedterm) . " THEN 50
                        WHEN title REGEXP " . DB::connection()->getPdo()->quote( "[[:<:]]" . $Cleanedterm . "[[:>:]]") . " THEN 25
                    END
                AS score_title,
                +
                    CASE 
                        WHEN url = " . DB::connection()->getPdo()->quote( "/" . str_slug($Cleanedterm) . "/" ) . " THEN 50
                        WHEN url REGEXP " . DB::connection()->getPdo()->quote( "[[:<:]]" . str_slug($Cleanedterm) . "[[:>:]]" ) . " THEN 25
                    END
                AS score_url, TaxCount"
            )
        )
        ->whereRaw("MATCH(title, url, summary, content) AGAINST(".DB::connection()->getPdo()->quote($search_term).")")
        ->leftJoin( DB::raw("
            (SELECT 
                tax_type,
                item_id,
             SUM(
              CASE 
                 WHEN {$table_prefix}taxonomy.type = 'category' THEN 2.5
                 WHEN {$table_prefix}taxonomy.type = 'tag' THEN 1
                 ELSE 0
              END
          )  AS TaxCount
          FROM {$table_prefix}taxonomy_relationships 
          LEFT JOIN {$table_prefix}taxonomy ON {$table_prefix}taxonomy_relationships.term_id = {$table_prefix}taxonomy.id
          WHERE {$table_prefix}taxonomy.deleted_at IS NULL AND ({$table_prefix}taxonomy.name LIKE '%{$Cleanedterm}%' OR {$table_prefix}taxonomy.display_name LIKE '%{$Cleanedterm}%' OR {$table_prefix}taxonomy.slug LIKE '%{$Cleanedterm}%')
          GROUP BY {$table_prefix}taxonomy_relationships.item_id, tax_type
        ) {$table_prefix}t_rel"), function ($join) use($SearchTable) {
            $join->on($SearchTable . '.item_id', '=', 't_rel.item_id')->on($SearchTable . '.type', '=', 't_rel.tax_type');
        });
    }


    /**
     * Ordering by format and position
     * @param  QueryBuilder $query
     * @param  String $order_by     Default is the score
     * @param  String $order        Order of results
     */
    public function scopeOrdering($query, $order_by = 'relevance', $order = 'DESC') {
        switch( $order_by ):
            case 'created':
                $query->orderBy('created_at', $order);
                break;
            case 'last_updated':
            case 'newest':
            case 'oldest':
                if( $order_by == 'newest_first' ) {
                    $order = 'DESC';
                }

                if( $order_by == 'oldest' ) {
                    $order = 'ASC';
                }

                $query->orderBy('updated_at', $order);
                break;
            case 'a-z':
            case 'z-a':
            case 'title':
                if( $order_by == 'a-z' ) {
                    $order = 'ASC';
                }

                if( $order_by == 'z-a') {
                    $order = 'DESC';
                }

                $query->orderBy('title', $order);
                break;
            default:
                $query->orderByRaw("score+IFNULL(TaxCount,0)+IFNULL(score_freshness,0)+IFNULL(score_title,0)+IFNULL(score_url,0) DESC");
                break;
        endswitch;

        return $query;
    }

    /**
     * Ability to include certain items from the search
     * @param  QueryBuilder $query
     * @param  Array        $included_types
     */
    public function scopeInclude($query, $included_types = []) {
        if( count($included_types) > 0 ) {
            $CleanedIncludedTypes = [];
            foreach($included_types as $included_type_id => $included_type):
                $CleanedIncludedTypes[ $included_type_id ] = urldecode($included_type);
            endforeach;

            $query->whereIn('type', $CleanedIncludedTypes);
        }

        return $query;
    }

    /**
     * Ability to exclude certain items from the search
     * @param  QueryBuilder $query
     * @param  Array        $excluded_types
     */
    public function scopeExclude($query, $excluded_types = []) {
        if( count($excluded_types) > 0 ) {
            $CleanedExcludedTypes = [];
            foreach($excluded_types as $excluded_type_id => $excluded_type):
                $CleanedExcludedTypes[ $excluded_type_id ] = urldecode($excluded_type);
            endforeach;

            $query->whereNotIn('type', $CleanedExcludedTypes);
        }

        return $query;
    }

    /**
     * We're looking to filter results based on this page
     * @param  QueryBuilder $query
     * @param  String $section Page locations
     */
    public function scopeSection($query, $section = NULL) {
        if( !empty($section) ) {
            $section = trim($section, "/");

            $query->where(function($query) use($section) {
                $query->where('url', "/{$section}/")
                    ->orWhere('url', 'LIKE', "/{$section}/%");
            });
        }

        return $query;
    }

    /**
     * Force clean the content and summaries
     * 
     * @return Searcher
     */
    public function forceClean()
    {
        $this->summary = $this->summary;
        $this->content = $this->content;

        return $this;
    }
}
