<?php namespace Hummingbird\Models;

use DB;
use General;
use Hash;
use Illuminate\Database\Eloquent\Model;
use Hummingbird\Models\Activitylog;

class PageVersion extends Model {

    public $table = 'pages_versions';
    public $cms_url = 'pages';
    public $cms_type = 'Page - Version';
    
    protected $fillable = array('title', 'summary', 'url', 'parentpage_id', 'content', 'enable_comments', 'hide_from_search', 'breadcrumb', 'status', 'protected', 'username', 'password', 'show_in_nav', 'searchable', 'search_image', 'featured_image', 'custom_menu_name', 'template');
    
    public $search_field = 'content';
    public $export_fields = array('title', 'url', 'content', 'enable_comments', 'hide_from_search', 'status', 'protected');

    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see     \Illuminate\Database\Eloquent\Model::boot()
     * @return  void
     * @since   1.1.37
     */
    public static function boot() {
        parent::boot();

        static::deleting(function($PageVersion) { 
            if( $PageVersion->forceDeleting ) {
                DB::table('taxonomy_relationships')->where([
                    'tax_type' => get_class($this),
                    'item_id' => $PageVersion->id
                ])->delete();
                
                DB::table('meta_content')->where([
                    'item_type' => get_class($PageVersion),
                    'item_id' => $PageVersion->id
                ])->delete();
                
                Activitylog::where([
                    'type' => get_class($this),
                    'link_id' => $PageVersion->id
                ])->where()->forceDelete();
            }
        });
    }

    /**
     * Posts can belong to many categories and vice versa
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Categories::class, 'taxonomy_relationships', 'item_id', 'term_id')
            ->where('tax_type', get_class($this));
    }

    /**
     * Posts can belong can have many tags and vice versa
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tags::class, 'taxonomy_relationships', 'item_id', 'term_id')
            ->where('tax_type', get_class($this));
    }
    
    public function page()
    {
        return $this->belongsTo(Page::class);
    }
    
    public function parentpage()
    {
        return $this->belongsTo(Page::class);
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }      
    
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function setUrlAttribute($value)
    {
        $this->attributes['url'] = ($value == '') ? PagesHelper::get_url_from_title($this->attributes['title']) : $value;
    }

    public function setPasswordAttribute($value)
    {
        if(strlen($value) == 0) return $this->attributes['password'] = '';
        
        $this->attributes['password'] = Hash::make($value);
    }

    public function setHashAttribute($value)
    {
        $this->attributes['hash'] = Hash::make($value);
    }
    
    public function getBackendURL()
    {
        return '/' . General::backend_url() . '/' . $this->cms_url . '/edit/' . $this->attributes['page_id'] . '/?version=' . $this->attributes['id'];
    }

    public static function get_selection($emptyitem_text = 'Select a page', $excludes = array())
    {
        $selection = array(0 => $emptyitem_text);
        
        $pages = Page::all();
        
        foreach($pages as $page) {
            if(in_array($page->id, $excludes)) continue;
            $selection[$page->id] = $page->title;
        }
        
        return $selection;
    }
    
    public static function get_blog_feed_styles()
    {
        return array();
    }

    public static function get_statuses()
    {
        return array('draft' => 'Draft', 'public' => 'Public');
    }

    
    public function taxonomy($Microsites = null)
    {
        $data = array('category' => array(), 'tag' => array());

        if(null !== $Microsites AND null !== $Microsites->microsite and $Microsites->microsite->type == 'standard')
        {
            $Microsites->purgeDBConnection();

            $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->where('taxonomy_relationships.microsite_id', $Microsites->microsite->id)->get();

            $Microsites->purgeDBConnection($Microsites->new_prefix);
        }
        else
        {
            $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type');

            if(null !== $Microsites AND null === $Microsites->microsite) $terms = $terms->whereNull("taxonomy_relationships.microsite_id");

            $terms = $terms->get();
        }
        
        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }

    public function storeTaxonomy($tags, $page, $class, $Microsites = NULL)
    {
        if(null !== $Microsites AND null !== $Microsites->microsite and $Microsites->microsite->type == 'standard')
        {
            $Microsites->purgeDBConnection();

            DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $page->id)->where('microsite_id', $Microsites->microsite->id)->delete();

            $input = array();
            foreach($tags as $tag)
            {
                $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $page->id, 'microsite_id' => $Microsites->microsite->id);
            }

            DB::table('taxonomy_relationships')->insert($input);
            
            $Microsites->purgeDBConnection($Microsites->new_prefix);

            return;
        }

        /* No microsites */
        DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $page->id)->delete();

        $input = array();
        foreach($tags as $tag)
        {
            $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $page->id);
        }

        if(count($input) > 0)
        {
            DB::table('taxonomy_relationships')->insert($input);
        }
    }

    /**
     * Set the summary attribute (clean it where necessary)
     */
    public function setSummaryAttribute($value) {
        $this->attributes['summary'] = ( !empty($value) ) ? $value : NULL;
    }

    /**
     * Return the breadcrumb path for each page
     * 
     * @return String
     */
    public function getBreadcrumbPathAttribute()
    {
        $page       = $this;
        $page_names = [];

        while ( null !== $page->parentpage ):
            $page_names[] = str_slug(trim(strtolower($page->title)));

            $page = $page->parentpage;
        endwhile;

        return implode(".", array_reverse($page_names));     
    }
}
