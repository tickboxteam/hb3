<?php namespace Hummingbird\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Redirections extends Model {
    use SoftDeletes;

    public $table       = 'redirections';
    public $cms_url     = 'redirections';
    public $table_stats = 'redirections_stats';
    protected $fillable = array('from', 'to', 'comments', 'type', 'category', 'function');

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'last_used' => 'datetime',
    ];

    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    public static function boot() {
        parent::boot();

        // On force delete, delete all the stats
        static::creating(function($Redirect) {
            if( !isset($Redirect->comments) ):
                $Redirect->comments = '';
            endif;

            if( !isset($Redirect->category) ):
                $Redirect->category = '';
            endif;
        });

        static::saving(function($Redirect) {
            if( !isset($Redirect->comments) ):
                $Redirect->comments = '';
            endif;

            if( !isset($Redirect->category) ):
                $Redirect->category = '';
            endif;
        });

        static::deleting(function($Redirect) {
            if( $Redirect->forceDeleting ) {
                $Redirect->statistics()->delete();
            }
        });

        static::deleted(function($Redirect) {
            $NoteString = ( Auth::user() ) ? Auth::user()->username : "System";

            Activitylog::log([
                'action' => 'DELETED',
                'type' => get_class($Redirect),
                'link_id' => $Redirect->id,
                'description' => 'Deleted redirect',
                'notes' => "{$NoteString} has removed the website redirect from &quot;$Redirect->from&quot; to &quot;$Redirect->to&quot;"
            ]);
        });
    }

    /**
    * 
    */    
    public static function get_types_selection()
    {
        return array(
            '301' => 'Permanent (301)', 
            '302' => 'Temporary (302)', 
            'meta' => 'Visible'
        );
    }

    /**
     * Belongs to a Redirect
     */
    public function statistics() {
        return $this->hasMany(RedirectStat::class, 'redirect_id', 'id');
    }

    /**
     * Is this a meta-refresh type redirec
     * @return boolean
     */
    public function isMeta() {
        return ( $this->type == 'meta' );
    }

    /**
    * When setting the category, if empty, set to NULL
    */
    public function setCategoryAttribute($value) {
        $value = trim($value);

        $this->attributes['category'] = ( $value != '' ) ? $value : '';
    }

    /**
    * When setting the comment, if empty, set to NULL
    */
    public function setCommentsAttribute($value) {
        $value = trim($value);

        $this->attributes['comments'] = ( $value != '' ) ? $value : '';
    }

    /**
    *
    */
    public function setFromAttribute($value) {
        $this->attributes['from'] = clean_and_sluggify_url( $value );
    }


    /**
     * Scope query for retrieving categories of redirects only
     */
    public function scopeCategoriesOnly($query)
    {
        return $query->distinct()
            ->where('category', '!=', '')
            ->orderBy('category')
            ->get(['category']);
    }

    /**
     * Try and find any resource by the 'from', 'to' or 'comments' fields
     */
    public function scopeSearch($query, $term)
    {
        if ($term) {
            $query->where(function($q) use ($term) {
                $q->where('from', 'LIKE', "%$term%")
                  ->orWhere('to', 'LIKE', "%$term%")
                  ->orWhere('comments', 'LIKE', "%$term%");
            });
        }

        return $query;
    }

    /**
     * CMS Filters
     */
    public function scopeCmsFilter($query, Array $Inputs) {
        if(!empty($Inputs)) {

            // Filter by categories
            if( !empty($Inputs['category']) ) {
                $query->category( $Inputs['category'] );
            }

            // Filter by search term
            if( !empty($Inputs['s']) ) {
                $query->search( trim($Inputs['s']) );
            }

            // Filter by redirection type
            if( !empty($Inputs['type']) ) {
                $query->type( $Inputs['type'] );
            }

            if( !empty($Inputs['function']) ) {
                $query->where('function', $Inputs['function'] );   
            }
        }

        return $query;
    }

    /**
     * Filter by a category
     */
    public function scopeCategory($query, $category) {
        if($category == 'uncategorised') {
            return $query->where(function($query) {
                $query->whereNull('category')
                    ->orWhere('category', '');
            });
        }

        return $query->where('category', $category);
    }

    /**
     * Filter by a type of redirect
     */
    public function scopeType($query, $type) {
        return $query->where('type', $type);
    }
}
