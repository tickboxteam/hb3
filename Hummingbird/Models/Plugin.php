<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class Plugin extends Model
{
    public $table = 'plugins';

    protected $fillable = array('namespace', 'description', 'version');    
    protected $guarded = array();
}
