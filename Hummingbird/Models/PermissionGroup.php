<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model 
{
    public $table = 'groups_permissions';
    protected $fillable = array('name', 'description');


    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    public static function boot() {
        parent::boot();

        static::deleted(function($Model) {
            if( $Model->permissions->count() > 0 ) {
                $Model->permissions()->delete();
            }
        });
    }

    /**
     * One to Many relationship for Groups > Permissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions() {
        return $this->hasMany(Permission::class, 'group_id', 'id');
    }

    /**
     * Scope filter by name
     */
    public function scopeName($query, $value) {
        return $query->where('name', $value);
    }

    /**
     * Scope query to find permissions where they're attached to a role.
     */
    public function scopeFilterPermissionsByRole($query, $role_id = NULL) {
        $PermissionsQuery = Permission::select('permissions.group_id')->orderBy('permissions.group_id')->groupBy('permissions.group_id');

        if( !empty($role_id) ):
            $PermissionsQuery->join('permission_role', 'permissions.id', '=', 'permission_role.permission_id')
                ->where('permission_role.role_id', $role_id);
        endif;

        return $query->joinSub($PermissionsQuery, 'permissions', function($join) {
            $join->on('groups_permissions.id', '=', 'permissions.group_id');
        });
    }

    /**
     * Scope query to find permissions where they're attached to a user
     */
    public function scopeFilterPermissionsByUser($query, $user_id = NULL) {
        $PermissionsQuery = Permission::select('permissions.group_id')->orderBy('permissions.group_id')->groupBy('permissions.group_id');

        if( !empty($user_id) ):
            $PermissionsQuery->join('permissions_users', 'permissions.id', '=', 'permissions_users.permission_id')
                ->where('permissions_users.user_id', $user_id);
        endif;

        return $query->joinSub($PermissionsQuery, 'permissions', function($join) {
            $join->on('groups_permissions.id', '=', 'permissions.group_id');
        });
    }
}