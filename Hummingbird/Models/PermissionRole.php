<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
    public $table = 'perm_role';
    
    public $fillable = array('permission_id', 'role_id');
    
    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }
    
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}

?>
