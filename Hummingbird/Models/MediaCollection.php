<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class MediaCollection extends Model {
    public $table = 'media-collections';

    public $fillable = ['name', 'description', 'permalink', 'parent_collection'];


    /**
     * The "booting" method of the model.
     * Overrided to attach before/after method hooks into the model events.
     *
     * @see     \Illuminate\Database\Eloquent\Model::boot()
     * @return  void
     * @since   1.1.37
     */
    public static function boot() {
        parent::boot();

        static::deleting(function($model) {
            foreach($model->children as $ChildCollection) {
                foreach( $ChildCollection->resources as $ChildCollectionResource ) {
                    $ChildCollectionResource->delete();
                }

                $ChildCollection->delete();
            }

            foreach( $model->resources as $ModelResource ) {
                $ModelResource->delete();
            }
        });
    }

    public function parent() {
        return $this->belongsTo(MediaCollection::class, 'parent_collection');
    }

    public function children() {
        return $this->hasMany(MediaCollection::class, 'parent_collection');
    }

    /**
     * Get the resources associated with this collection
     *
     * @since 1.1.37
     */
    public function resources() {
        return $this->hasMany(Media::class, 'collection', 'id');
    }

    /**
     * Checking the value we should store for descriptions
     */
    public function setDescriptionAttribute( $value ) {
        $value = trim( $value );
        $this->attributes['description'] = ( $value != '' ) ? $value : NULL;
    }

    /**
     * Reset the parent_collection if it's not a valid integer
     */
    public function setParentCollectionAttribute( $value ) {
        $this->attributes['parent_collection'] = ( $value != '' && is_numeric($value) ) ? $value : NULL;
    }

    /**
     * Get only the top level Collections which do not belong to anything
     * @param  QueryBuilder $query
     * @return QueryBuilder
     */
    public function scopeTopLevelOnly($query) {
        return $query->whereNull('parent_collection');
    }
}