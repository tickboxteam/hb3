<?php namespace Hummingbird\Models;

use ArrayHelper;
use Auth;
use Request;
use Illuminate\Database\Eloquent\Model;

class RequestNotFound extends Model  {
	public $table      = 'errors';

    public static function boot() {
        parent::boot();

        static::deleted(function($RequestNotFound) {
            Activitylog::log(array(
                'action' => 'DELETED',
                'type' => get_class(new RequestNotFound),
                'link_id' => NULL,
                'description' => "Error removed &quot;{$RequestNotFound->url}&quot;",
                'notes' => Auth::user()->name . " removed &quot;{$RequestNotFound->url}&quot; from the error log"
            ));
        });
    }

    /**
     * Scope: Filter by the date they were last updated
     */
    public function scopeDate_Search($query, $from_date = NULL, $to_date = NULL) {
        if( !is_null($from_date) && !is_null($to_date) ) {
            return $query->where('updated_at', '>=', $from_date->format('Y-m-d 00:00:00'))
                ->where('updated_at', '<=', $to_date->format('Y-m-d 23:59:59'));
        }

        if( !is_null($from_date) ) {
            return $query->where('updated_at', '>=', $from_date->format('Y-m-d 00:00:00'));
        }

        if( !is_null($to_date) ) {
            return $query->where('updated_at', '<=', $to_date->format('Y-m-d 23:59:59'));
        }

        return $query;
    }

    /**
     * Scope: filter by those that match a particular string
     */
    public function scopeSearch($query) {
        if( Request::filled('s') ) {
            $query->where('url', 'LIKE', "%" . Request::get('s') . "%");
        }

        return $query;
    }

    /**
     * Scope: Filter by the type of error
     */
    public function scopeType($query) {
        $Types = [];

        if( Request::filled('type') ) {
            $Types = ArrayHelper::cleanExplodedArray( explode(",", Request::get('type')) );
        }

        return ( count($Types) > 0 ) ? $query->whereIn('type', $Types) : $query;   
    }
}
