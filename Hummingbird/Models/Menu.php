<?php namespace Hummingbird\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $table       = 'menus';
    public $cms_url     = 'menus';
    protected $fillable = array('name', 'location', 'description');

    public static $rules = array(
        'name' => 'required|min:2'
    );


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        Menu::deleting(function($Menu) {
            $Menu->items()->delete();
        });
    }

    /**
     * Get all the menuitems associated wth this Menu
     */
    public function items()
    {
        return $this->hasMany(Menuitem::class, 'menu_id', 'id')->orderBy('order');
    }

    public function topLevelItems() {
        return $this->hasMany(Menuitem::class, 'menu_id', 'id')->whereNull('parent')->orderBy('order');
    }

    /**
     * Get all the menuitems associated wth this Menu
     */
    public function menuitems()
    {
        return $this->items()->orderBy('order');
    }

    public static function get_selection()
    {
        $selection = array();
        
        $menus = Menu::all();
        
        foreach($menus as $menu) {
            $selection[$menu->id] = $menu->title;
        }
        
        return $selection;
    }

    /**
     * Checking the value we should store for descriptions
     */
    public function setDescriptionAttribute( $value ) {
        $value = trim( $value );
        $this->attributes['description'] = ( $value != '' ) ? $value : NULL;
    }
}
