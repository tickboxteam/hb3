<?php namespace Hummingbird;

use File;
use View;
use Illuminate\Support\ServiceProvider;

/**
 *
 */
abstract class PluginServiceProvider extends ServiceProvider {
    protected $base_path    = __DIR__;
    protected $files        = [];
    protected $pluginName;
    protected $packageName;
    protected $views;



    /**
     * Create a new service provider instance.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    public function __construct($app)
    {
        parent::__construct($app);

        $this->PluginManager = $this->app->make('plugin.manager');

        $this->setPluginName();
        $this->setPackageName();
        $this->setViewDirectory();
    }


    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {
        $Vendor     = trim( explode("/", $this->packageName)[0] );
        $VendorName = trim( explode("/", $this->packageName)[1] );
        $Package    = $this->PluginManager->getPackageInformation($Vendor, $VendorName);

        if( isset($this->configPath) ) {
            foreach (glob( "{$this->configPath}*.php" ) as $CoreConfig ) {
                $ConfigInfo = pathinfo($CoreConfig);

                $ConfigVendorPath = strtolower($Vendor) . "-" . strtolower($VendorName);
                $ConfigVendorNamespace = strtolower($Vendor) . "." . strtolower($VendorName);

                $this->publishes([$ConfigInfo['dirname'] . "/{$ConfigInfo['basename']}" => config_path("{$ConfigVendorPath}-{$ConfigInfo['basename']}")]);
                $this->mergeConfigFrom($ConfigInfo['dirname'] . "/{$ConfigInfo['basename']}", "{$ConfigVendorPath}-{$ConfigInfo['filename']}");
            }
        }

        $this->loadViewsFrom( $this->views, $this->packageName );

        if( isset($this->pluginPath) && file_exists( $this->pluginPath . '/Lang' ) ) {
            $this->loadTranslationsFrom($this->pluginPath . '/Lang', strtolower($Vendor) . "-" . strtolower($VendorName));
        }

        if( isset($this->app[$this->pluginName]) ):
            $this->app[$this->pluginName]->boot();
        endif;
    }

    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->registerPlugin();
        $this->registerFiles();
        $this->registerHelpers();
        $this->registerViews();
    }


    /**
     * Registering the main Plugin file used for Plugin Services
     *
     * @return void
     */
    public function registerPlugin() {}


    /**
     * Register Plugin Views (if directory exists)
     *
     * @return void
     */
    public function registerViews() {
        if(File::isDirectory($this->views)) {
            View::addNamespace($this->packageName, $this->views);
        }
    }

    /**
     * Register the files which are needed for booting
     * @return Void
     */
    protected function registerFiles() {
        foreach($this->files as $file) {
            if( File::exists( $file ) ) {
                include $file;
            }
        }
    }

    /**
     * If you have a directory of helpers, they can be 
     * registered here in one go.
     */
    public function registerHelpers() {
        foreach( glob( $this->base_path . '/Helpers/*.php') as $file ):
            require_once($file);
        endforeach;
    }
}
