<?php namespace Hummingbird\Listeners\Redirects;

use Request;
use Carbon\Carbon;
use Hummingbird\Jobs\Redirects\RedirectTriggered;
use Hummingbird\Models\RedirectStat;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RedirectTriggerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RedirectTriggered  $event
     * @return void
     */
    public function handle(RedirectTriggered $event)
    {
        $event->redirect->no_used += 1;
        $event->redirect->last_used = Carbon::now();
        $event->redirect->save();

        $RedirectStat = new RedirectStat;
        $RedirectStat->redirect_id  = $event->redirect->id;
        $RedirectStat->visited_at   = Carbon::now();
        $RedirectStat->referrer     = Request::server('HTTP_REFERER') ?: '';
        $RedirectStat->save();
    }
}
