<?php namespace Hummingbird\Listeners\Auth;

use Illuminate\Auth\Events\PasswordReset;

class PasswordResetListener
{
    /**
     * Create the event listener.
     */
    public function __construct() {}

    /**
     * Handle the event.
     *
     * @param  PasswordReset  $event
     * @return void
     */
    public function handle(PasswordReset $event): void
    {
        $event->user->force_password_reset = NULL;
        $event->user->save();
    }
}
