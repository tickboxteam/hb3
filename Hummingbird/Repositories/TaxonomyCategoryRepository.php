<?php namespace Hummingbird\Repositories;

use Hummingbird\Models\Categories;

class TaxonomyCategoryRepository {
    /**
     * Get all deleted categories.
     * @throws Exception
     */
    public function getAllDeletedCategories() {
        $Categories = Categories::objectPermissions()->type()->onlyTrashed()->get();

        if( $Categories->isEmpty() ) {
            throw new \Exception("No deleted categories found");
        }

        return $Categories;
    }

    /**
     * Get a deleted category
     * @param  Integer|Nullable $category_id
     * @throws ModelNotFoundException
     */
    public function getDeletedCategory($category_id) {
        return Categories::objectPermissions()->type()->onlyTrashed()->findOrFail($category_id);
    }
}