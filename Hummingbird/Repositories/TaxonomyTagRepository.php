<?php namespace Hummingbird\Repositories;

use Hummingbird\Models\Tags;

class TaxonomyTagRepository {
    /**
     * Get all deleted tags.
     */
    public function getAllDeletedTags() {
        return Tags::type()->onlyTrashed()->orderBy('name', 'ASC')->get();
    }

    /**
     * Get all deleted tags
     * @throws Exception
     */
    public function getAllDeletedTagsOrFail() {
        $Tags = $this->getAllDeletedTags();

        if( $Tags->isEmpty() ) {
            throw new \Exception("No deleted tags found");
        }

        return $Tags;
    }

    /**
     * Get a deleted tag
     * @param  Integer|Nullable $tag_id
     * @throws ModelNotFoundException
     */
    public function getDeletedTag($tag_id) {
        return Tags::type()->onlyTrashed()->findOrFail($tag_id);
    }
}