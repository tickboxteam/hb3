<?php namespace Hummingbird\Repositories;

use Hummingbird\Models\Job;

class JobRepository {
    /**
     * Get all failed jobs
     * @return Mixed
     */
    public function getFailedJobs() {
        return Job::orderBy('failed_at', 'DESC');
    }

    /**
     * Find a failed job by it's ID
     * @param  integer $id
     * @return Job
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findFailedJob( $id ) {
        return Job::findOrFail( $id );
    }

    /**
     * Remove all jobs
     */
    public function flush() {
        Job::truncate();
    }
}