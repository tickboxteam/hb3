<?php namespace Hummingbird\Shortcodes;

use ArrayHelper;
use Webwizo\Shortcodes\Facades\Shortcode;

/**
 * System shortcode registration class that will handle the registration of shortcodes
 * Shortcodes can be used to apply functionality to pages and content.
 * Shortcodes can be filtered too.
 *
 * @since   1.1.33
 * @version 1
 */
class SystemShortcodes {
    /**
     * @var array Shortcodes registered
     */
    public $shortcodes = [];

    /**
     * @var array Filters
     */
    public $filters = [];

    /**
     * Register multiple shortcodes
     * @param  array  $shortcodes Array of shortcodes to register
     */
    public function registerShortcodes($shortcodes = []) {
        foreach($shortcodes as $shortcode) {
            $this->registerShortcode($shortcode['name'], $shortcode['action']);
        }
    }

    /**
     * Registration of a single shortcode
     * @param  String $shortcode Shortcode name
     * @param  String $action    Shortcode action
     */
    public function registerShortcode($shortcode, $action) {
        Shortcode::register($shortcode, $action);

        $this->shortcodes[$shortcode] = $shortcode;
    }

    /**
     * Store filters that will be used to manipulate those shortcodes that are registered/output
     * @param Mixed $filters
     */
    public function add_filters($filters = NULL) {
        $new_filters = (is_array($filters)) ? $filters : [];

        if( !empty($filters) && is_string($filters) ) {
            $new_filters = ArrayHelper::cleanExplodedArray( explode(",", $filters) );
        }

        $this->filters = array_unique(array_merge($this->filters, $new_filters));
    }

    /**
     * Filtering shortcodes
     * @param  array  $shortcodes Array of shortcodes to register
     */
    public function filter_shortcodes($filters = NULL) {
        $NewlyFilteredShortcodes = array();

        // Filter out the shortcodes we do need
        if( count($this->filters) > 0 ) {
            foreach($this->filters as $filterShortcode) {
                foreach($this->shortcodes as $FilteredShortcodeKey => $FilteredShortcodeVal) {
                    if( strpos($FilteredShortcodeKey, $filterShortcode) === 0 ) {
                        $NewlyFilteredShortcodes[ $FilteredShortcodeKey ] = $FilteredShortcodeVal;
                    }
                }
            }

            $this->shortcodes = $NewlyFilteredShortcodes;
        }

        ksort($this->shortcodes);
    }

    /**
     * Get the shortcodes registered
     * Also anything that has been stripped, we are going to remove it.
     */
    public function get_active_shortcodes() {
        if( count($this->shortcodes) > 0 ):
            $this->filter_shortcodes();
        endif;

        return $this->shortcodes;
    }
}
