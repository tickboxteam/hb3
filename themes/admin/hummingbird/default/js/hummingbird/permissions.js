$(document).ready(function() {
    if($("#PermissionTabs").length > 0) {
        $("#PermissionTabs li:first a").trigger('click');
    }

    if($(".perms .permission").length > 0) {
        $(".perms .permission").click(function() {
            if($(this).prop('checked')) {
                $('.perms[data-perm-required="' + $(this).data('perm') + '"').show();
            }
            else {
                $('.perms[data-perm-required="' + $(this).data('perm') + '"').hide();
            }
        });
    }

    $(".checkbox-filter").click(function(e)
    {
        e.preventDefault();

        var el_type = $(this).data('type');

        switch(el_type)
        {
            case 'all':
                $(".permission").prop('checked', false);
                $(".permission").trigger('click');
                break;
            case 'deselect':
                $(".permission").prop('checked', true);
                $(".permission").trigger('click');
                break;
            case 'invert':
                $(".permission").trigger('click');
                break;
        }
    });
});