// https://dev.to/themodernweb/here-is-how-i-made-a-strong-password-checker-using-javascript-3m9o
let password = document.getElementById("password");
let password_confirmation = document.getElementById("password_confirmation");
let passwordChecklist = document.querySelectorAll('.list-item');
let passwordStrengthProgress = document.getElementById("password-strength--progress");

password.addEventListener('keyup', (e) => {
    if( passwordStrengthProgress && e.target.value.length > 0 ) {
        passwordStrengthProgress.classList.remove('hide');
        password_confirmation.parentNode.parentNode.parentNode.classList.remove('hide');
    }
    else {
        passwordStrengthProgress.classList.add('hide');
        password_confirmation.parentNode.parentNode.parentNode.classList.add('hide');
    }

    passwordChecklist.forEach((item, i) => {
        let icon = item.querySelector('i');
        let regexVal = new RegExp( item.getAttribute('data-regex') );
        let isValid = regexVal.test(password.value);

        if(isValid) {
            icon.classList.remove('fa-circle');
            icon.classList.add('fa-check');
        } else {
            icon.classList.add('fa-circle');
            icon.classList.remove('fa-check');
        }
    })
});

password_confirmation.addEventListener("keyup", function(){
    let pass_cnf_parent = password_confirmation.parentNode.parentNode.parentNode; //.form-group
    let newEl = document.createElement('p');
    newEl.innerHTML = 'Passwords do not match';
    newEl.classList.add('text-danger');

    pass_cnf_parent.classList.remove('has-error');
    if( pass_cnf_parent.querySelector('.text-danger') ) {
        pass_cnf_parent.querySelector('.text-danger').remove();
    }

    if( password.value != password_confirmation.value ) {
        pass_cnf_parent.classList.add('has-error');
        password_confirmation.parentNode.parentNode.insertBefore(newEl, null);
        return;
    }
});