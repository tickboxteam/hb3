/*!
 *
 * Hummingbird CMS (http://hummingbirdcms.com)
 * Copyright 2014-2021 Tickbox Marketing Ltd.
 *
 * @since     v1.0.0
 * @version   v1.0.1
 */

var width = $(window).width();
var height = $(window).height();

var redactor = false;
var plugins_arr = ['video', 'textexpander', 'textdirection', 'table', 'limiter', 'imagemanager', 'hb_image_manager', 'formatter', 'undo_redo', 'underline', 'super_subscript', 'fullscreen', 'fontsize', 'fontfamily', 'fontcolor', 'filemanager', 'definedlinks', 'counter', 'clips', 'shortcodes'];
var RedactorShortcodes = {};

function init_redactor()
{
    if(RedactorPlugins && plugins_arr.length == 0)
    {
        $.each(RedactorPlugins, function(key, value)
        {
            plugins_arr.push(key);
        });
    }

    if($('.redactor-content').length > 0)
    {
        $('.redactor-content').each(function()
        {
            if( $.data($(this)[0], 'redactor') === undefined) {
                $(this).attr('id', randString());
                $(this).redactor({
                    plugins: plugins_arr,
                    replaceDivs: false,
                    toolbarFixed: false,
                    paragraphize: false
                });
            }
        });

        redactor = true;
    
        if($('.redactor-toolbar .re-fullscreen').length > 0)
        {
            $('.redactor-toolbar .re-fullscreen').click(function()
            {
                $('.wrapper').toggleClass('addOpacity');
            })
        }
    }
}

function destroy_redactor()
{
    $('.redactor-content').each(function()
    {
        $("#"+$(this).attr('id')).redactor('core.destroy');
    });

    redactor = false;
}

$(document).ready(function()
{   
    /* Initialise */

    $(".preview-emails").click(function(e) {
        e.preventDefault();
        var params = $(this).data();
        delete params.route;
        var formRoute = false;

        $.ajax({
            type: "GET",
            url: $(this).data('route'),
            data: $(this).data(),
            success: function(response) {
                $('#emailContent .modal-title').text('Previewing email');
                
                if( params.modalTitle !== undefined ) {
                     $('#emailContent .modal-title').text('Previewing email: ' + params.modalTitle);
                }

                $('#emailContent .modal-body').html(response);
                $('#emailContent').modal('show');
            },
            error: function(response) {
            },
            complete: function(response) {
            }
        });
    });

    $(".switch-screen-size").click(function(e)
    {
        e.preventDefault();

        $('.wrapper').css('width', $(this).data('width'));
        $('.wrapper').css('height', $(this).data('height'));
    });

    $("#nav-toggle").click(function(e)
    {
        e.preventDefault();

        updateViewportDims();

        if(width > 768)
        {
            // we are looking on desktops
            if($("#sidebar").hasClass("collapsed"))
            {
                //the name is removed until the transition has finished
                $("#sidebar .brand span").addClass('hide');
            }

            $(".menu ul:not(.submenu)").slideDown();
            $("li.option, .hasSubMenu").removeClass('active');

            $("#sidebar, .main-content").toggleClass('collapsed');

            if(!$("#sidebar").hasClass("collapsed"))
            {
                //the name is shown once the transition has ended
                setTimeout(function()
                {
                    $("#sidebar .brand span").removeClass('hide');
                }, 500); //delay of CSS transition
            }
        }
        else
        {
            // iPad/iPhone?
            $('body').toggleClass('open-sidebar');
        }
    });

    // $(".content-section, .search-box input, .profile").click(function(e)
    // {
    //     e.preventDefault();
    //     updateViewportDims();
        
    //     if(width <= 768 && $('body').hasClass('open-sidebar'))
    //     {
    //         // iPad/iPhone?
    //         $('body').removeClass('open-sidebar');
    //     }
    // });

    if($(".hasSubMenu").length > 0)
    {
        $(".hasSubMenu").click(function(e)
        {
            e.preventDefault();

            if(!$("#sidebar").hasClass("collapsed"))
            {
                var el = $(this);
                var el_li = el.closest('li.option');
                var el_sub = el_li.find('ul');
                var el_first_child = el_li.children(':first');

                el_li.toggleClass('active');
                el_first_child.toggleClass('active');

                if(el_sub.is(':visible'))
                {
                    el_sub.slideUp();
                    el_first_child.find('i.fa-chevron-right').removeClass('rotate');
                }
                else
                {
                    el_sub.slideDown();
                    el_first_child.find('i.fa-chevron-right').addClass('rotate');
                }
            }
        });
    }

    if($("#sidebar .nav ul li").length > 0)
    {
        $( "#sidebar .nav ul li" ).hover(function() 
        {
            if(!$(this).parent().hasClass('submenu'))
            {
                $(this).addClass("nav-hover");

                var height = $("#sidebar").outerHeight();
                var el_height = $(this).parent().height();
                var el_sub_height = $(this).parent().find('.submenu').height();
                var el_position = $(this).parent().offset();
                var top = height - el_position.top - (el_height + el_sub_height) - $("#sidebar .brand").outerHeight();

                if(top < 0)
                {
                    $(this).find('span.title').css('top', top + 'px');
                    $(this).parent().find('.submenu').css('top', top + 'px');
                }
            }
        }, function () {
            if(!$(this).parent().hasClass('submenu'))
            {
                $(this).closest('li').removeClass("nav-hover");
            }
        });
    }

    $('.btn-confirm-delete').on('click', function(e) {
        e.preventDefault();
        $del_modal = $('#delete-modal');
        $del_modal.data('id', '');

        if(typeof $(this).data('id') !== 'undefined')
            $del_modal.data('id', $(this).data('id'));

        if($(this).is('a'))
            $del_modal.find("#btn-delete-confirmed").attr('href', $(this).attr('href'));

        if(typeof $(this).data('target') !== 'undefined') {
            $del_modal.data('target', $(this).data('target'));
        }

        $del_modal.find('.modal-body').hide();

        if(typeof $(this).data('title') !== 'undefined') {
            $del_modal.find('.delete-title').html($(this).data('title'));
            $del_modal.find('.modal-body:not(.modal-body-standard):not(.modal-body-custom)').show();
        }
        else if(typeof $(this).data('delete-body') !== 'undefined') {
            $del_modal.find('.modal-body-custom-data').html($(this).data('delete-body'));
            $del_modal.find('.modal-body-custom').show();
        }
        else {
            $del_modal.find('.modal-body-standard').show();
        }

        $del_modal.modal('show');
    });

    $("#btn-delete-confirmed").on('click', function(e) {
        if(typeof $(this).closest('.modal').data('target') !== 'undefined') {
            if( $( $(this).closest('.modal').data('target') ).is('button') ) {
                $( $(this).closest('.modal').data('target') ).unbind().trigger('click');
            }

            if( $( $(this).closest('.modal').data('target') ).is('form') ) {
                $( $(this).closest('.modal').data('target') ).closest('form').submit();
            }
        }

        return true;
    });

    if($("form.validate-taxonomy").length > 0) {
        var form_el = null;
        var form_btn = null;

        //trigger when form is submitted
        $("form.validate-taxonomy button").click(function(e){
            form_el = $(this).closest('form');
            form_btn = $(this);
            e.preventDefault();

            if( !form_el.find('.taxonomy-holder').length || form_el.find('input[name="taxonomy[]"]').length ) {
                if( form_btn && form_btn.attr('type') == 'submit' ) {
                    form_btn.unbind('click');
                    form_btn.trigger('click');
                    return;
                }

                return form_el.submit();
            }

            $('#taxonomy-confirm').modal('show');
        });

        $("#modal-taxonomy-confirmed").on('click', function(e) {
            e.preventDefault();
            $('#taxonomy-confirm').modal('hide');

            if( form_btn && form_btn.attr('type') == 'submit' ) {
                form_btn.unbind('click');
                form_btn.trigger('click');
                return;
            }
            
            $("form.validate-taxonomy").submit();
        });

        $("#modal-taxonomy-close").on('click', function() {
            $("body,html").animate({
                scrollTop : $(".taxonomy-group").offset().top
            },1000);
        });
    }
    
    /* All redactor based content areas */
    if($(".redactor-content").length > 0)
    {
        init_redactor();
    }

    if($("#clock").length > 0)
    {
        updateClock();
        setInterval('updateClock()', 1000);
    }

    if($("#return-to-top").length > 0)
    {
        var content = $('html,body').outerHeight();

        if (content < window) 
        {
            $('#return-to-top').fadeIn();
        }
        else 
        {
            $('#return-to-top').fadeOut();
        }

        $('#return-to-top').click(function(e)
        {
            e.preventDefault();
            $('body,html').animate({
                scrollTop : 0
            },1000);

            return false;
        });

        $(window).scroll(function()
        {
            if ($(this).scrollTop() > (content/4)) 
            {
                $('#return-to-top').fadeIn('slow');
            } 
            else 
            {
                $('#return-to-top').fadeOut('slow');
            }
        });
    }


    function addTaxonomy(tax_group, _select, html)
    {
        if(tax_group.find('.taxonomy-holder .no-taxonomy').length > 0)
        {
            tax_group.find('.taxonomy-holder .no-taxonomy').addClass('hide');
        }

        tax_group.find('.taxonomy-holder').append(html);
        _select.selectpicker('refresh');
    }

    if($(".add-taxonomy").length > 0)
    {
        $(".add-taxonomy").click(function(e)
        {
            e.preventDefault();

            var html = '<div class="taxonomy relative"><input type="hidden" name="taxonomy[]" value="{item-id}" /><span class="badge badge--hummingbird badge-sm">{item-name}</span><a class="taxonomy-remove" title="Remove Option" href="javascript:void(0);" style="color:red;">x</a></div>';

            /* Elements */
            var tax_group = $(this).closest('.taxonomy-group');
            var _select = tax_group.find('select');

            /* Variables */
            var _select_val = _select.val(); //remove white space
            var type = _select.data('type'); //type of taxonomy

            if($.isArray(_select_val))
            {
                var _html = '';

                $.each(_select_val, function( index, value )
                {
                    var _select_text = _select.find('option[value="'+value+'"]').text();
                    _select_text     = _select_text.replace("&nbsp;", "");
                    _select_text     = _select_text.replace(" ", "");

                    var _this_html = html;
                    _this_html = _this_html.replace("{item-id}", value);
                    _this_html = _this_html.replace("{item-name}", $.trim(_select_text));

                    _html += _this_html;

                    // _select.find('option[value="'+value+'"]').remove(); //remove array of items
                    _select.find('option[value="'+value+'"]').attr("disabled", true); //remove array of items
                });

                html = _html;

                addTaxonomy(tax_group, _select, $.trim(html));
            }
            else if(_select_val !== null)
            {
                var _select_text = _select.find('option:selected').text();
                _select_text     = _select_text.replace("&nbsp;", "");
                _select_text     = _select_text.replace(" ", "");

                html = html.replace("{item-id}", _select_val);
                html = html.replace("{item-name}", $.trim(_select_text));

                // _select.find('option:selected').remove(); //remove single item
                _select.find('option:selected').attr('disabled', true); //remove single item

                addTaxonomy(tax_group, _select, html);
            }
            else
            {
                var search_val = $.trim(tax_group.find('.bs-searchbox input').val());
                    
                var data = {};
                data.ajax = true;
                data.name = search_val;

                $.ajax({
                    type: "POST",
                    url: '/hummingbird/' + type,
                    data: data,
                    success: function(response)
                    {
                        //success
                        html = html.replace("{item-id}", response.data.id);
                        html = html.replace("{item-name}", response.data.name);
                    },
                    error: function(response)
                    {
                        //error
                        html = '';
                    },
                    complete: function(response)
                    {
                        //complete now insert taxonomy
                        addTaxonomy(tax_group, _select, html);
                    }
                });
            }
        });
    }

    $(document).on('click', '.taxonomy-remove', function(e)
    {
        e.preventDefault();

        var parent = $(this).closest('.taxonomy-group'); //parent element
        var select = parent.find('select'); // get the select element
        var _this = $(this).closest('.taxonomy'); //this element

        /* Variables */
        var id = _this.find('input').val();
        var text = _this.find('.badge').text();

        /* Add back to select */
        // select.append($("<option></option>")
        //     .attr("value",id)
        //     .text(text));        
        parent.find('select option[value="' + id + '"]').removeAttr('disabled');

        /* Remove 'tick' */
        select.find('option[value="' + id + '"]').prop("selected", false);

        /* remove from view */
        _this.remove();

        /* We have nothing left - add the "nothing to view" back in */
        if(parent.find('.taxonomy-holder .taxonomy').length == 0)
        {
            parent.find('.no-taxonomy').removeClass('hide');
        }

        /* re-initialise select */
        select.selectpicker('refresh');
    });

    $(document).on('hidden.bs.modal', '#media-lib.modal', function (e)
    {
        $(this).remove();
    });

    setContentHeight();

    $('[data-confirm]').click(function (e) {
        return confirm($(this).data('confirm'));
    });
       
    $(".form-control.textareas").attr('rows', 3).autogrow();

    $(".summary-truncate").on('keyup change', function() {
        truncate($(this));
    });

    $(".summary-truncate").each(function() {
        truncate($(this));
    });
});

function updateViewportDims()
{
    width = $(window).width();
    height = $(window).height();
}

function setContentHeight()
{
    var header_section = $(".header-section").outerHeight();
    var content_section = $(".content-section").outerHeight();
    var footer_section = $("#footer").outerHeight();

    // console.log(header_section);
    // console.log(content_section);
    // console.log(footer_section);

    var content_height = header_section + content_section;

    /* Side bar min-height */
    // var new_height = (height < content_height) ? content_height:height;
    // if(height < new_height) 
    // {
        // $("#sidebar").css('min-height', new_height + 'px');
    // }

    /* Content area min-height */
    var sidebar_height = $("#sidebar").outerHeight();
    var new_content_height = sidebar_height - header_section - footer_section;

    if($(".header-flash").length > 0)
    {
        new_content_height -= $(".header-flash").outerHeight();
    }

    $(".content-section").css('min-height', new_content_height + 'px');
}




function updateClock ( )
    {
    var currentTime = new Date ( );
    var currentHours = currentTime.getHours ( );
    var currentMinutes = currentTime.getMinutes ( );
    var currentSeconds = currentTime.getSeconds ( );
 
    // Pad the minutes and seconds with leading zeros, if required
    currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
    currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
 
    // Choose either "AM" or "PM" as appropriate
    var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";
 
    // Convert the hours component to 12-hour format if needed
    // currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;
 
    // Convert an hours component of "0" to "12"
    // currentHours = ( currentHours == 0 ) ? 12 : currentHours;
 
    // Compose the string for display
    var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
     
     
    $("#clock").html(currentTimeString);
         
 }


 /**
 * Function generates a random string for use in unique IDs, etc
 *
 * @param <int> n - The length of the string
 */
function randString(n)
{
    if(!n)
    {
        n = 10;
    }

    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for(var i=0; i < n; i++)
    {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

function truncate(el)
{
    var parent = el.parent();
    var item = el.val();
    var item_length = item.length;
    var limit = (parseInt(el.data('summary-limit')) > 0) ? parseInt(el.data('summary-limit')):255;
    var limit_area = parent.find('.character-count');
    var remaining = parseInt(limit-item_length);
    remaining = (remaining < 0) ? 0:remaining;

    var limit_text = (remaining != 1) ? ' characters remaining':' character remaining';

    if(limit_area.length < 1)
    {
        parent.append('<div class="character-count" />');
        limit_area = parent.find('.character-count');
    }

    limit_area.text(remaining + limit_text);

    if (remaining <= 0)
    {
        el.val((item).substring(0, limit - 1));
        return false;
    }
}

$(document).ready(function()
{ 
    $('.clipboard_copy').on('click', function(e) {
        e.preventDefault();
        var val = $(this).attr('data-clipboard-value');

        var aux = document.createElement("input");

        // Assign it the value of the specified element
        aux.setAttribute("value", val);

        // Append it to the body
        document.body.appendChild(aux);

        // Highlight its content
        aux.select();

        // Copy the highlighted text
        document.execCommand("copy");
        $("#copyToClipbrdModal .modal-body").text(val + ' has been copied to your clipboard');
        $("#copyToClipbrdModal").modal('show');

        setTimeout( function() {
            $("#copyToClipbrdModal").modal('hide');
        }, 5000);
    

        // Remove it from the body
        document.body.removeChild(aux);
    });    
});

