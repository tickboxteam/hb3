if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
	RedactorPlugins.hb_image_manager = function()
	{
		return {
	        getTemplate: function()
	        {
	            return String()
	            + '<section id="redactor-modal-advanced">'
	            + '<label>Enter a text</label>'
	            + '<textarea id="mymodal-textarea" rows="6"></textarea>'
	            + '</section>';
	        },
			init: function()
			{
				var button = this.button.add('hb_image_manager', 'Image Manager');
				this.button.addCallback(button, this.hb_image_manager.load);

	            // make your added button as Font Awesome's icon
	            this.button.setAwesome('hb_image_manager', 'fa-image');
			},
			load: function()
			{
				var _this = this;

				/* Get modal */
		        var data = {};
		        data.collection = $(this).data('collection-id');
		        data.title = $(this).data('page-title');
	            data.type = 'images';

		        $.ajax({
		            dataType: "json",
		            cache: false,
		            url: '/hummingbird/media/globalMediaLibrary',
		            data: data,
		            success: $.proxy(function(data)
		            {  
		                if($("#media-lib.redactor").length <= 0)
		                {
		                    $("body").append(data.html);
		                }

		                $(".modal").modal('hide');
		                $("#media-lib").addClass('redactor').modal('show');
		            }, this)
		        });

		        $(document).on('keypress', '#media-lib.redactor #library .search-box input[name=s]', function(e) {
		        	if( e.which == 13 ) {
		        		$("#media-lib .breadcrumb li:last-child a").trigger('click');
		        	}
		        });

		        $(document).on('click', '#media-lib.redactor #library .search-box button', function(e) {
		        	$("#media-lib .breadcrumb li:last-child a").trigger('click');
		        });

		        /* Media library - images only */
		        $(document).on('click', '#media-lib.redactor #library .view-collection', function(e)
		        {
		            e.preventDefault();
		            var _search_term = $("#media-lib.redactor #library").find('input[name=s]').val();

		            $("#library").html('<h3>Loading...</h3>');

		            var data = {};
		            data.action = 'library';
		            data.collection = $(this).data('id');
		            data.type = 'images';
		            data.s = _search_term;

		            $.ajax({
		                dataType: "json",
		                data: data,
		                cache: false,
		                url: '/hummingbird/media/globalMediaLibrary',
		                success: $.proxy(function(data)
		                {
		                    data = $(data.html);
		                    html = data.find('#library').html();

		                    $("#media-lib.redactor #library").html(html);
		                    $("#media-lib.redactor .modal-footer").html("");
		                }, this)
		            });
		        });

		        $(document).on('click', '#media-lib.redactor .gallery .thumb', function(e)
		        {
		            e.preventDefault();

		            $("#library").html('<h3>Loading...</h3>');

		            var data = {};
		            data.action = 'item';
		            data.item = $(this).data('item-id');
		            data.featured = true;

		            $.ajax({
		                dataType: "json",
		                data: data,
		                cache: false,
		                url: '/hummingbird/media/globalMediaLibrary',
		                success: $.proxy(function(data)
		                {
		                    data = $(data.html);
		                    html = data.html();

		                    $("#media-lib.redactor #library").html(html);

		                    if($("#media-lib.redactor .modal-footer").length <= 0)
		                    {
		                    	$("#media-lib.redactor .modal-content").append('<div class="modal-footer"><footer><button type="submit" class="create-image btn btn-primary">Insert Image</button></footer></div>');
		                    }
		                }, this)
		            });
		        });

		        $(document).on('click', '#media-lib.redactor .create-image', function(e)
		        {
		        	e.stopImmediatePropagation();
		            e.preventDefault();

		            _this.hb_image_manager.insert();

		            $(".modal").modal('hide');
		        });

		        $(document).on('change', '#media-lib.redactor #url', function(e)
		        {
		            e.preventDefault();

		            $(".url-show").toggleClass('hide');
		        });

		        $("#media-lib.redactor .create-image").unbind('click');
			},
			insert: function(e)
			{
	            var html = '';

	            var src 	= $("#media-lib.redactor #library #src").val();
	            var title 	= $("#media-lib.redactor #library #title").val();
	            var alt 	= $("#media-lib.redactor #library #alt").val();
	            var caption = $("#media-lib.redactor #library #caption").val();
	            var url 	= $("#media-lib.redactor #library #url").val();
	            var target_window = $("#media-lib.redactor #library #target_window").val();
	            var link 	= $("#media-lib.redactor #library #link").val();

	            var insert_img 		= document.createElement('img');
	            insert_img.src 		= src;
	            insert_img.alt 		= ( alt.length > 0 ) ? alt : title;
	            html = insert_img.outerHTML;

	            if( url == 'url' && link.length > 0 ) {
	            	var insert_link 	= document.createElement('a');
		            insert_link.href	= link;
		            insert_link.title 	= title;
		            insert_link.target 	= (target_window != '') ? target_window : '_self';
		            insert_link.append( insert_img );
		            html = insert_link.outerHTML;
	            }

	            if( caption.length > 0 ) {
	            	var caption_html = document.createElement('figure');

	            	if( url == 'url' && link.length > 0 ) {
	            		caption_html.append( insert_link );
	            	}
	            	else {
	            		caption_html.append( insert_img );	
	            	}

	            	var caption_desc = document.createElement('figcaption');
	            	caption_desc.innerHTML = caption;
	            	caption_html.append( caption_desc );

	            	html = caption_html.outerHTML;
	            }

	            this.selection.restore();
	            this.insert.html(html);
			}
		};
	};
})(jQuery);