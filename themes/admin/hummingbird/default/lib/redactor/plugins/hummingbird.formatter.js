if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
	RedactorPlugins.formatter = function()
	{
		return {
			init: function()
			{
				var button = this.button.add('formatter', 'Remove Formatting');
	            
	            // make your added button as Font Awesome's icon
	            this.button.setAwesome('formatter', 'fa-magic');

				var dropdown = {
				    item1: { title: 'Inline styles', func: this.formatter.format },
				    item2: { title: 'All formatting', func: this.formatter.remove }
				};
				 
				this.button.addDropdown(button, dropdown);
			},
	        format: function()
	        {
	        	// var html = $(this.code.get());
	        	var html = this.code.get();
	        	// var formatted_html = '';
	        	
	        	// $.each(html, function(index, value)
	        	// {
	        	// 	if(value.nodeName != '#text') 
        		// 	{
        		// 		$(this).removeAttr('style');
		        // 		$(this).removeData();
		        // 		$(this).removeAttr('rel');
		        // 		formatted_html += $(this).prop('outerHTML');
		        // 	}
	        	// });

				// removes MS Office generated guff
				// function cleanHTML(input) {
				// 1. remove line breaks / Mso classes
				var stringStripper = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g; 
				var output = html.replace(stringStripper, ' ');

				// 2. strip Word generated HTML comments
				var commentSripper = new RegExp('<!--(.*?)-->','g');
				var output = output.replace(commentSripper, '');
				var tagStripper = new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi');
				
				// 3. remove tags leave content if any
				output = output.replace(tagStripper, '');
				
				// 4. Remove everything in between and including tags '<style(.)style(.)>'
				var badTags = ['style', 'script','applet','embed','noframes','noscript'];

				for (var i=0; i< badTags.length; i++) {
					tagStripper = new RegExp('<'+badTags[i]+'.*?'+badTags[i]+'(.*?)>', 'gi');
					output = output.replace(tagStripper, '');
				}
				
				// 5. remove attributes ' style="..."'
				var badAttributes = ['style', 'start'];
				for (var i=0; i< badAttributes.length; i++) {
					var attributeStripper = new RegExp(' ' + badAttributes[i] + '="(.*?)"','gi');
					output = output.replace(attributeStripper, '');
				}
				// return output;

	        	this.code.set(output);
	        },
	        remove: function()
	        {
	        	var html_txt = $(this.code.get()).text();

	        	this.code.set(html_txt);
	        }
		};
	};
})(jQuery);