if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
    RedactorPlugins.shortcodes = function()
    {
        return {
            init: function()
            {
                var NewShortcodes = RedactorShortcodes;
                var redactor_el = this;
                var dropdowns = {};

                if( this.$element.context.classList.contains('redactor-shortcodes') && Object.keys(RedactorShortcodes).length > 0 ) {
                    var dataset = ( typeof this.$element.context.dataset.shortcodesOnly !== 'undefined' ) ? this.$element.context.dataset.shortcodesOnly : '';
                    dataset = redactor_el.shortcodes._ar_split_clean( dataset );

                    var exclude_shortcodes = ( typeof this.$element.context.dataset.shortcodesExclude !== 'undefined' ) ? this.$element.context.dataset.shortcodesExclude : '';
                    exclude_shortcodes = redactor_el.shortcodes._ar_split_clean( exclude_shortcodes );

                    if( dataset.length > 0 ) {
                        NewShortcodes = {}; //reset this and then loop over the filters

                        dataset.forEach(element => {
                            Object.keys(RedactorShortcodes).sort().forEach(function(Shortcode) {
                                if( Shortcode.indexOf(element) != -1 ) {
                                    NewShortcodes[Shortcode] = Shortcode;
                                }
                            });
                        });
                    }

                    Object.keys(NewShortcodes).sort().forEach(function(RedactorShortcode) {
                        let shouldSkip = false;

                        if( exclude_shortcodes.length > 0 ) {
                            exclude_shortcodes.forEach(excludeElement => {
                                if (shouldSkip) {
                                    return;
                                }

                                if ( RedactorShortcode.includes(excludeElement) ) {
                                    shouldSkip = true;
                                    return;
                                }
                            });
                        }

                        if( !shouldSkip ) {
                            dropdowns[RedactorShortcode] = { title: RedactorShortcode, func: redactor_el.shortcodes._insert };
                        }
                    });

                    if( Object.keys(dropdowns).length > 0 ) {
                        var button = this.button.add('shortcodes', 'Shortcodes');
                        this.button.setAwesome('shortcodes', 'fa-terminal'); // make your added button as Font Awesome's icon
                        this.button.addDropdown(button, dropdowns);
                    }
                }
            },
            _insert: function(e)
            {
                this.selection.restore();
                this.insert.html('[' + e.trim() + ']');
            },
            _ar_split_clean: function(arr_data) {
                arr_data = arr_data.split(",").map(function(item) {
                    return item.trim();
                });

                return arr_data.filter(item => item);
            }
        };
    };
})(jQuery);