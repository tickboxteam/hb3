var holder; //library holder for search and featured images

/* Media library - images only */
$(document).on('click', '#media-lib.browse #library .view-collection', function(e)
{
    e.preventDefault();

    $("#library").html('<h3>Loading...</h3>');
	downloads = $(this).data('downloads');
	
    var data = {};
    data.action = 'library';
    data.collection = $(this).data('id');
	if(downloads == 1) {
    	data.is_download = 'true';
    }
	
	additional = $(this).data('additional')
	
	if(additional == 1) {
		data.is_additional = 'true';
	}

    $.ajax({
        dataType: "json",
        data: data,
        cache: false,
        url: '/hummingbird/media/globalMediaLibrary',
        success: $.proxy(function(data)
        {
            data = $(data.html);
            html = data.find('#library').html();

            $("#media-lib.browse #library").html(html);
        }, this)
    });
});

$(document).on('click', '#media-lib.browse .gallery .thumb', function(e)
{
    e.preventDefault();

    $("#library").html('<h3>Loading...</h3>');
    $("#media-lib.browse .modal-footer").remove();
    downloads = $(this).data('downloads');
	additional = $(this).data('additional');
	
    var data = {};
    data.action = 'item';
    data.item = $(this).data('item-id');
    data.featured = true;
    
    if(downloads == 1) {
    	data.is_download = 'true';
    }
	
	if(additional == 1) {
		data.is_additional = 'true';
	}

    $.ajax({
        dataType: "json",
        data: data,
        cache: false,
        url: '/hummingbird/media/globalMediaLibrary',
        success: $.proxy(function(data)
        {
        	type = data.type;
        	dl = data.is_dl;
			additional = data.is_additional;
            data = $(data.html);
            html = data.html();

            $("#media-lib.browse #library").html(html);
			
			if(dl == true || dl == 'true') {
				if(type == 'image') {
		            $("#media-lib.browse #library").closest('.modal-content').append('<div class="modal-footer"><footer><button type="submit" class="create-file btn btn-primary">Insert Image</button></footer></div>');
		        } else if(type == 'file') {
		        	$("#media-lib.browse #library").closest('.modal-content').append('<div class="modal-footer"><footer><button type="submit" class="create-file btn btn-primary">Insert file</button></footer></div>');
		        }
			} else if(additional == true || additional == 'true') {
				if(type == 'image') {
		            $("#media-lib.browse #library").closest('.modal-content').append('<div class="modal-footer"><footer><button type="submit" class="add-additional btn btn-primary">Insert Image</button></footer></div>');
		        }
			} else {
				if(type == 'image') {
		            $("#media-lib.browse #library").closest('.modal-content').append('<div class="modal-footer"><footer><button type="submit" class="create-image btn btn-primary">Insert Image</button></footer></div>');
		        } else if(type == 'file') {
		        	$("#media-lib.browse #library").closest('.modal-content').append('<div class="modal-footer"><footer><button type="submit" class="create-file btn btn-primary">Insert file</button></footer></div>');
		        }
		    }
        }, this)
    });
});

$(document).on('click', '#media-lib.browse .create-image', function(e)
{
    e.preventDefault();

    holder.find('.remove-media-library').parent().removeClass('hide');
    holder.find('.media-image').val($("#media-lib.browse #library #src").val()); //reset value
    holder.find('.image-holder').removeClass('hide').find('img').attr('src', $("#media-lib.browse #library #src").val());
    holder.find('.browse-media-library').data('collection-id', $("#media-lib.browse #library #collection").val());
	
	downloads = $('#current-downloads').find('.downloads');
	
	if(downloads.length > 0) {
		var file = $('#media-lib.browse #library #title').val();
		var media_id = $('#media-lib.browse #library .pull-right').attr('data-id');
		$('.download-field').val(file);
		$('.media-id').val(media_id);
	}

    $("#media-lib.browse").modal('hide');
});

$(document).on('change', '#media-lib.browse #url', function(e)
{
    e.preventDefault();

    $(".url-show").toggleClass('hide');
});

// if($(".browse-media-library").length > 0)
// {
    $(document).on('click', '.browse-media-library', function(e)
    {
        e.preventDefault();

        holder = $(this).closest('.media-library-holder'); //library holder
		downloads = $(this).closest('.downloads');
		additional = $(this).closest('.additional');
		
		
		
        var data = {};
        data.collection = $(this).data('collection-id');
        data.title = $(this).data('page-title');
        data.type = 'all';
        
        if(downloads.length > 0) {
        	data.is_download = 'true';
        }

        if (additional.length > 0) {
            data.is_additional = 'true';
        }

        $.ajax({
            dataType: "json",
            cache: false,
            url: '/hummingbird/media/globalMediaLibrary',
            data: data,
            success: $.proxy(function(data)
            {  
                if($("#media-lib.browse").length <= 0)
                {
                    $("body").append(data.html);
                }

                $("#media-lib").modal('hide');
                $("#media-lib").addClass('browse').modal('show');
            }, this)
        });
    });
// }

// if($(".remove-media-library").length > 0)
// {
    $(document).on('click', '.remove-media-library', function(e)
    {
        e.preventDefault();

        $(this).parent().addClass('hide'); //hide remove button

        holder = $(this).closest('.media-library-holder'); //library holder
        holder.find('.media-image').val(''); //reset value
        holder.find('.browse-media-library').data('collection-id', '');
        holder.find('.image-holder').addClass('hide');
    });
// }

$(document).on('click', '#media-lib.browse .create-file', function(e)
{
	e.preventDefault();
	var file = $('#media-lib.browse #library #title').val();
	var media_id = $('#media-lib.browse #library .pull-right').attr('data-id');
	if($('.download-field[name="download_name"]').val() == '') {
		//only put filename to the name field if there isn't already a name specified
		$('.download-field[name="download_name"]').val(file);
	}
	$('.download-field[name="download_file"]').val(file);
	$('.media-id').val(media_id);
	$("#media-lib.browse").modal('hide');
	
});

$(document).on('click', '.add-additional', function(e)
{
	e.preventDefault();
	var file = $('#media-lib.browse #library .image-holder #target').attr('src');
	var media_id = $('#media-lib.browse #library .pull-right').attr('data-id');
	var html = '<div class="col-sm-6 col-md-2 thumb"> <input type="hidden" name="additional_images['+media_id+'][media_id]" value="'+media_id+'"/>';
		html += '<input type="hidden" name="additional_images['+media_id+'][filename]" value="'+file+'"  />';
		html += '<div class="images link-click"><a href="#"><img src="'+file+'" />'+'</a></div>';
			html += '<div class="options">'
				html += '<span class="option delete btn-danger">';
				html += '<a href="#" class="media-delete" data-id="#">';
				html += '<i class="fa fa-trash"></i>';
				html += '</a></span></div></div>';
	$('#media-lib2').append(html);
	$("#media-lib.browse").modal('hide');
});

$(document).on('click', '.media-delete', function(e)
{
    e.preventDefault();
    $(this).closest('.thumb').remove();
});
