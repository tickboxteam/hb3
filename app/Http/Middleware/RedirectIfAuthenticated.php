<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if( $request->headers->has('referer') ):
                    return redirect()->back();
                endif;

                // If this is a HB3 route, then redirect to HB3.
                if( request()->is('hummingbird', 'hummingbird/*') || strpos(request()->headers->get('referer'), 'hummingbird') !== FALSE ) {
                    return redirect()->route('hummingbird.dashboard');
                }

                return redirect('/');
            }
        }

        return $next($request);
    }
}
