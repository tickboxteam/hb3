<?php

namespace App\Http\Middleware;

use ArrayHelper;
use Illuminate\Foundation\Application;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        //
    ];

    /**
     * Add the CSRF Exception routes to the verifier when we load the request, from our .env
     */

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @param  \Illuminate\Contracts\Encryption\Encrypter  $encrypter
     * @return void
     */
    public function __construct(Application $app, Encrypter $encrypter)
    {
        parent::__construct( $app, $encrypter );
        
        if( env('CSRF_EXCEPT') ) {
            $this->except = ArrayHelper::cleanExplodedArray( explode(",", env('CSRF_EXCEPT')) );
        }
    }
}
