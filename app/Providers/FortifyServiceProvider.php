<?php

namespace App\Providers;

use Hash;
use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use Hummingbird\Models\User;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Laravel\Fortify\Fortify;
use Laravel\Fortify\Contracts\PasswordConfirmedResponse;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        Fortify::ignoreRoutes();
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Fortify::authenticateUsing(function (Request $request) {
            $field = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

            $user = User::where($field, $request->username)->first();
     
            if ($user &&
                Hash::check($request->password, $user->password)) {
                return $user;
            }
        });
        
        Fortify::loginView(function () {
            return view('HummingbirdBase::cms.login');
        });

        Fortify::twoFactorChallengeView(function () {
            return view('HummingbirdBase::cms.users.auth.two-factor-challenge');
        });

        Fortify::confirmPasswordView(function () {
            return view('HummingbirdBase::cms.users.auth.confirm-password');
        });

        if( request()->is('hummingbird', 'hummingbird/*') || strpos(request()->headers->get('referer'), 'hummingbird') !== FALSE ) {
            config()->set('fortify.home', url('/hummingbird') );
            config()->set('fortify.redirects.logout', url('/hummingbird') );
        }

        RateLimiter::for('login', function (Request $request) {
            $throttleKey = Str::transliterate(Str::lower($request->input(Fortify::username())).'|'.$request->ip());

            return Limit::perMinute(10)->by($throttleKey);
        });

        RateLimiter::for('two-factor', function (Request $request) {
            return Limit::perMinute(10)->by($request->session()->get('login.id'));
        });
    }
}
