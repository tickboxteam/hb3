<?php namespace App\Providers;

use Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        if( Request::is('hummingbird/*') && env('APP_PAGINATION_CMS') == 'bootstrap' ) {
            Paginator::useBootstrap();
        }
        
        if( !Request::is('hummingbird/*') && env('APP_PAGINATION') == 'bootstrap' ) {
            Paginator::useBootstrap();
        }
        
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
